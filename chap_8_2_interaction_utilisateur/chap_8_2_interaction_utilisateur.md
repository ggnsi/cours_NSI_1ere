# Interaction avec l'utilisateur (côté client), initiation au Javascript

(source du TP : M.Willm avec quelques modifications)

>Le but n'est pas ici de maitriser le Javascript mais de savoir analyser, comprendre, modifier, adapter un ensemble code HTML/CSS/ script Javascript afin d'offrir une interaction avec l'utilisateur dans une page HTML.

## Bref aperçu

En _1995_ , __Brendn Aich__ travaille chez Netscape Communication Corporation, la société qui éditait le célèbre navigateur Netscape Navigator, alors principal concurrent d’Internet Explorer. Il y développe le LiveScript, un langage destiné à être installé sur les serveurs développés par Netscape. Netscape se met à développer une version client du LiveScript, qui sera renommée __JavaScript__ en hommage au langage Java créé par la société Sun Microsystems.    

![logo JS](./img/logo_js.png)

- Javascript est un __langage de programmation interprété__. (Il faut un navigateur web qui dispose d’un interpréteur JavaScript)
-	Il est utilisé principalement dans la conception de pages web et permet d’agir sur les éléments HTML, rendant ainsi la page web interactive.

> Le code Javascript est donc chargé en même temps que la page HTML (inclus dans la page ou sous forme de fichier séparé téléchargé avec la page) et est exécuté par le navigateur sur la machine client (c'est à dire sur la machine de celui qui regarde la page. Ce code n'est pas exécuté sur le serveur (la machine qui met à disposition les pages HTML).

---
## Analyse du code n°1
---

### Analyse :

* Télécharger puis ouvrir la page suivante et la tester : [exemple n° 1.](fichiers/exemple1.html)

* Visualiser le code de la page et l'analyser : quels éléments du code ont permis d'ajouter de l'interactivité avec l'utilisateur ?

### Application n° 1 : 

* Créer une page Html avec trois boutons qui permettent chacun de mettre le fond de la page dans une couleur différente.

---
## Analyse du code n°2
---

### Analyse :

* Télécharger puis ouvrir la page suivante et la tester : [exemple n° 2](fichiers/exemple2.html)

* Visualiser le code de la page.

_Information_ : la propriété `innerHTML` correspond au code HTML contenu entre les balises de l'élément concerné.  
Ainsi, `document.getElementById("texte").innerHTML = "Au revoir"` définit le code HTML contenu entre la balise `<div>` et celle `</div>` dont l'`id` est `"texte"`

### Application n° 2

Reprendre l'application n° 1 et faire en sorte que chaque bouton permette non seulement de changer la couleur de fond de la page, mais en plus affiche cette couleur en toutes lettres.

---
## Analyse du code n°3
---

### Analyse :

* Télécharger puis ouvrir la page suivante et la tester : [exemple n° 3](fichiers/exemple3.html)

* Visualiser le code de la page.

### Application n° 3

* Proposer une page dans laquelle l'utilisateur peut, dans une zone de texte, choisir la couleur de fond de la page (avec un champ de texte et un bouton de validation). On pourra par exemple regarder [cette page](http://www.bernardquevillier.fr/toposnew/coulstand.htm) pour les couleurs standards.
* Modifiez afin que la zone de texte redevienne vide lorsque l'on appuie sur le bouton 

---
## D'autres événements
--

> `onclick` n'est pas le seul événement qui existe pour les différentes balises. On peut citer `onmouseover`, `onmouseout`, `onchange`, ...

### Application n°4

 _A faire_ : Créer une page avec un carré, initialement rouge, qui devient bleu lorsque la souris passe dessus et redevient rouge lorsque la souris en sort.
 
 _Aide_ : s'inspirer de l'exemple 1 pour la création du carré.

### Application n°5

>Certaines balises disposent d'événements particuliers. Ainsi, les balises `input`, quel que soit leur type, disposent de l'événement `oninput`.

_A faire_ : créer une page avec une zone de texte. La page doit afficher "Bonjour ..." au fur et à mesure que l'on écrit son prénom dans la zone de texte.

![Fenetre du navigateur avec la page attendue](img/exemple.png)

_A faire_ : Faites disparaître le texte "Bonjour ..." lorsque le contenu de la zone de texte est vide.  
_Aide_ : la structure `if ... then...` existe aussi en javascript. Cherchez la synthaxe.

---
## Bien d'autres choses possibles :
---

* Ouvrir la page suivante et visualiser son code : [exemple n° 6](fichiers/exemple6.html)
* Des jeux complets sont programmés en javascript.

---
## Pour ceux qui veulent aller plus loin
---

* [Apprenez à programmer avec JavaScript](https://openclassrooms.com/fr/courses/6175841-apprenez-a-programmer-avec-javascript) sur le site [openclassroom](https://openclassrooms.com/fr/)

* Des exemples d'utilisation du Javascript sur [ostralo.net](ostralo.net) : [Petits exemples en JavaScript](http://numerique.ostralo.net/javascriptexemples/)

* Quelques défis pour apprendre le Javascript sur [ostralo.net](ostralo.net) :  [Apprentissage du JavaScript](http://numerique.ostralo.net/javascriptapprentissage/)
