def est_trie(tab):
    '''prédicat qui indique si tab est trié par ordre croissant

    param tab (list)
    valeur retournée : (booléen)
    '''
    
    for i in range(0, len(tab) - 1):
        if tab[i] > tab[i + 1]:
            return False
    return True

# autre version

def est_trie_2(tab):
    
    i = 0
    while (i + 1 < len(tab)) and (tab[i + 1] >= tab[i]):
        i +=1
    return i == len(tab) - 1 # on teste si on est allé jusqu'au bout