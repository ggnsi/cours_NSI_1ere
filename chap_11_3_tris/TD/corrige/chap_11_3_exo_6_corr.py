import random

def liste_alea(n):
    '''retourne une liste d'entiers de taille n dont les éléments sont des entiers choisis
    au hasard entre 0 et n-1
    '''
    
    return [random.randint(0, 100000) for _ in range(n)]

def echange(tab, ind_1, ind_2):
    temp = tab[ind_2]
    tab[ind_2] = tab[ind_1]
    tab[ind_1] = temp
    

def tri_selection(tab):
    
    for i in range(len(tab) - 1):
        ind_maxi = i
        for ind in range(i + 1, len(tab)):
            if tab[ind] > tab[ind_maxi]:
                ind_maxi = ind
        echange(tab, i, ind_maxi)

def tri_insertion(tab):
    
    for i in range(1, len(tab)):
        val = tab[i]
        pos = i
        while (pos - 1 >= 0) and (tab[pos - 1] < val):
            tab[pos] = tab[pos - 1]
            pos = pos - 1
        tab[pos] = val

