def fusion_listes(liste_1, liste_2):
    '''fusionne les listes triées liste_1 et liste_2 en une liste triée

    param liste_1 et liste_2 : list
    valeur retournée : list
    
    CU : liste1 et liste_2 triées par ordre croissant
    
    Exemple :
    >>> liste_1 = [2, 4, 5, 7, 11, 15]
    >>> liste_2 = [1, 5, 6]
    >>> fusion_listes(liste_1, liste_2)
    [1, 2, 4, 5, 5, 6, 7, 11, 15]
    '''
    
    ind_1 = 0
    ind_2 = 0
    l_fusion = []
    
    while (ind_1 < len(liste_1)) and (ind_2 < len(liste_2)):                                 
        if liste_1[ind_1] <= liste_2[ind_2]:
            l_fusion.append(liste_1[ind_1])
            ind_1 += 1
        else:
            l_fusion.append(liste_2[ind_2])
            ind_2 += 1
    
    if ind_1 == len(liste_1):
        for k in range(ind_2, len(liste_2)):
            l_fusion.append(liste_2[k])
    else:
        for k in range(ind_1, len(liste_1)):
            l_fusion.append(liste_1[k])
    return l_fusion

if __name__ == '__main__':
    import doctest
    doctest.testmod()