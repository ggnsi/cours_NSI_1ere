import random
from timeit import timeit
from matplotlib import pyplot as plt

def liste_alea(n):
    '''retourne une liste d'entiers de taille n dont les éléments sont des entiers choisis
    au hasard entre 0 et 100000
    '''
    
    return [random.randint(0, 100000) for _ in range(n)]

def echange(tab, ind_1, ind_2):
    temp = tab[ind_2]
    tab[ind_2] = tab[ind_1]
    tab[ind_1] = temp
    

def tri_selection(tab):
    
    for i in range(len(tab) - 1):
        ind_mini = i
        for ind in range(i + 1, len(tab)):
            if tab[ind] < tab[ind_mini]:
                ind_mini = ind
        echange(tab, i, ind_mini)

def tri_insertion(tab):
    
    for i in range(1, len(tab)):
        val = tab[i]
        pos = i
        while (pos - 1 >= 0) and (tab[pos - 1] > val):
            tab[pos] = tab[pos - 1]
            pos = pos - 1
        tab[pos] = val

temps_selection, temps_insertion = [], []

for taille in range(1, 200):
    liste = liste_alea(taille)
    tps_selection = timeit(lambda: tri_selection(liste), number = 1)
    tps_insertion = timeit(lambda: tri_insertion(liste), number = 1)
    
    temps_selection.append(tps_selection)
    temps_insertion.append(tps_insertion)

# tracé des graphiques
plt.plot(temps_selection, '-r', label = "tri par selection")
plt.plot(temps_insertion, '-b', label = "tri par insertion")
plt.legend(loc="upper left")
plt.xlabel('taille de la liste')
plt.ylabel('temps de tri')
plt.show()