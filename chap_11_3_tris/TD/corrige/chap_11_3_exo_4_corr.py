def effectifs(tab):
    compteur = 0
    valeur = tab[0]
    for elt in tab:
        if elt == valeur:
            compteur += 1
        else:
            print(compteur, 'fois', valeur)
            valeur = elt
            compteur = 1
    
    # pour la dernière valeur
    print(compteur, 'fois', valeur)

def effectifs_2a(tab):
    compteur = 0
    valeur = tab[0]
    val_max, eff_max = valeur, compteur
    
    for elt in tab:
        if elt == valeur:
            compteur += 1
        else:
            if compteur > eff_max:
                val_max, eff_max = valeur, compteur
            valeur = elt
            compteur = 1
    # pour la dernière valeur
    if compteur > eff_max:
                val_max, eff_max = valeur, compteur
    return val_max, eff_max

def effectifs_2b(tab):
    compteur = 0
    valeur = tab[0]
    val_max, eff_max = valeur, compteur
    
    for elt in tab:
        if elt == valeur:
            compteur += 1
        else:
            if compteur >= eff_max:
                val_max, eff_max = valeur, compteur
            valeur = elt
            compteur = 1
    # pour la dernière valeur
    if compteur > eff_max:
                val_max, eff_max = valeur, compteur
    return val_max, eff_max
