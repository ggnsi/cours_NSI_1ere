def tri_bulles(tab):
    
    for i in range(len(tab) - 1):
        for k in range(len(tab) - 1 -i):
            if tab[k] > tab[k + 1]:
                temp = tab[k + 1]
                tab[k + 1] = tab[k]
                tab[k] = temp
        print(tab)
        
    
def tri_bulles_2(tab):
    
    nb_inversion = -1 # pour le premier tour de boucle while
    i = 0
    while (nb_inversion != 0) and (i < (len(tab) - 1)):
        nb_inversion = 0
        for k in range(len(tab) - 1 -i):
            if tab[k] > tab[k + 1]:
                temp = tab[k + 1]
                tab[k + 1] = tab[k]
                tab[k] = temp
                nb_inversion +=1
        print(tab)

# Lors du premier parcours de la liste, on effectue n - 1 comparaisons
# lors du 2eme parcours de la liste, on effectue n - 2 comparaisons
# ...
# lors du dernier parcours de la liste, on effectue 1 comparaison (le 1er élt et le deuxième)
# donc T(n) = (n - 1) + (n - 2) + ... + 1
# T(n) = 1 + 2 + 3 + ... + (n - 1)
# T(n) = n(n-1)/2 et T(n) = O(n^2)