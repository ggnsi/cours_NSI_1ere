# TD - Algorithmes de tri

### Exercice 1 : je sais re-programmer les deux algorithmes

Le but de l'exercice est d'essayer de re-programmer (sans aller faire du copier/coller du cours) les algorithmes de tri par sélection et par insertion.

1. Tri par sélection
  * programmer une fonction `echange(liste, ind_1, ind2)` qui échange __en place__ les éléments de `liste` d'indice `ind_1` et `ind_2`.
  * programmer une fonction `tri_selection(tab)` qui exécute __en place__ le tri par sélection de `tab`.
  

2. Tri par insertion

   Programmer une fonction `tri_selection(tab)` qui exécute __en place__ le tri par sélection de `tab`.

### Exercice 2

Modifier les algorithmes des tris par sélection et par insertion afin que les listes soient triées en place par ordre décroissant.


### Exercice 3

Ecrire un prédicat `est_trie(tab)` qui renvoie `True` si le tableau passé en paramètre est trié par ordre croissant.

_Exemples_ :

```Python
>>> est_trie([1, 2, 3, 5, 12, 18])
True
>>> est_trie([1, 2, 3, 5, 15, 12, 18])
False
```

### Exercice 4

1. Ecrire une fonction `effectifs(tab)` qui prend en argument un tableau d'entiers **trié** et affiche son contenu sous la forme d'un histogramme c'est à dire quelque chose comme :  
  *1 fois 0*  
  *2 fois 2*  
  *1 fois 3*  
  *4 fois 7*  

    La complexité de l'algorithme doit être en `O(n)` (c'est à dire que la liste ne doit être parcourue qu'une seule fois).
  
   _Exemple_ :

  ```Python
>>> liste = [0, 0, 0, 4, 4, 4, 4, 4, 5, 24, 24, 24, 24, 24, 24, 24, 78]
>>> effectifs(liste)
4 fois 0
5 fois 4
1 fois 5
7 fois 24
1 fois 78
```

2. Modifier la fonction précédente pour qu'elle renvoie le tuple `(valeur, effectif)` correspondant à l'effectif la valeur ayant le plus grand effectif dans la liste avec deux variantes :
  * variante 1 : en cas d'égalité d'effectif, c'est la première valeur rencontrée qui est renvoyée ;
  * variante 2 : en cas d'égalité d'effectif, c'est la dernière valeur rencontrée qui est renvoyée.
  
  _Exemples_ :
  
  ```Python
>>> liste = [0, 0, 0, 4, 4, 4, 4, 4, 5, 24, 24, 24, 24, 24, 78]
>>> effectifs_2a(liste)
(4, 5)
>>> effectifs_2b(liste)
(24, 5)
  ```

### Exercice 5 : mesure des temps d'exécution

1. créer une fonction `liste_alea(n)` qui retourne une liste de taille `n` composée d'entiers choisis au hasards entre `0` et `n - 1`.
2. Insérez dans le script les fonctions `tri_selection(tab)` et `tri_selection(tab)`
3. En vous inspirant des codes réalisés lors de la recherche dichotomique, complétez le script afin que :
* soient crées 2 listes (vides au départ) `temps_selection` et `temps_insertion` ;
* soit créée des listes de nombre aléatoires taille comprises entre 1 et 200 ;
* pour chacune des listes précédente soit mesuré les temps de tri à l'aide du tri par selection et du tri par insertion et que ces temps soient ajoutés aux listes temps_selection` et `temps_insertion`;
* Soit tracé le graphique représentant les temps de tri en fonction des tailles de liste.

### Exercice 6 : Tri à bulles

Regarder la vidéo expliquant le principe du tri à bulles : [vidéo](./video/tri_a_bulles_ilham_oumaira.3gp) (extrait de vidéo de Mme ilham_oumaira)

[Autre vidéo](https://www.youtube.com/watch?v=NdFQRqSY2Is)

1. Programmer une fonction `tri_bulles(tab)` qui tri __en place__ la liste suivant le tri à bulles (_remarque_ vous pourrez faire afficher le tableau à chaque fin de parcours afin de visualiser l'évolution du tri).
2. Quel est le nombre de comparaisons effectuées avec ce tri pour une liste de taille `n` ? Quelle est l'ordre de grandeur de la complexité ?
3. On peut améliorer l'algorithme en arrêtant le tri lorsqu'aucune inversion n'est faite lors du passage (c'est alors que la liste est totalement triée).  
  Modifiez votre algorithme pour y inclure cette amélioration (on pourra utiliser une variable `inversion` mise à `True` si il y a inversion)
  
  On pourra tester cette dernière fonction avec la liste `liste = [15, 12, 7, 1, 2, 3, 4]` et faire afficher les états du tableau après chaque parcours afin de visualiser l'arrêt des parcours.

### Exercice 7 : fusion de listes triées (plus dur)

Ecrire une fonction `fusion_listes(liste_1, liste_2)` qui, à partir de deux listes `liste_1` et `liste_2` qui sont chacune triées par ordre croissant, renvoie la liste triée par ordre croissant composée des éléments de `liste_1` et `liste_2`.

Attention : `liste_1` et `liste_2` n'ont pas forcément le même nombre d'éléments

_Exemple_ :
```Python
>>> liste_1 = [2, 4, 5, 7, 11, 15]
>>> liste_2 = [1, 5, 6]
>>> fusion_listes(liste_1, liste_2)
[1, 2, 4, 5, 5, 6, 7, 11, 15]
```

_Remarque_ : cette fonction est une partie de l'algorithme du tri fusion étudié en terminale.
