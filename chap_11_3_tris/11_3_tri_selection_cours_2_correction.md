# Le algorithmes de tris - tri par sélection
---

Rappel de l'algorithme ci-dessous :


```python
def tri_selection(liste):
    '''Trie la liste par ordre croissant en utilisant le tri par sélection
    param liste : (list)
    valeur retournée : aucune
    
    effet de bord : modifie en place la liste'''
    
    for i_deb in range(len(liste) - 1):
        
        # recherche du minimum dans la zone liste[i_deb : fin_liste]
        min = liste[i_deb]
        i_min = i_deb
        for ind in range(i_deb + 1, len(liste)):
            if liste[ind] < min:
                min = liste[ind]
                i_min = ind
        
        # échange des termes de rang i_deb et i_min
        temp = liste[i_deb]
        liste[i_deb] = min # ou liste[i_min]
        liste[i_min] = temp
```

## Preuve de correction de l'algorithme

Il s'agit ici de montrer que l'algorithme réalise bien le résultat recherché.

Soit __l'invariant de boucle__ : `P(i) : "liste([0 : i[) est triée et tous les éléments de liste([i : n[) sont >= à ceux de liste([0 : i[)"`

__Initialisation__ : 

Démontrons ici que l'invariant sera vrai __en fin d'exécution du premier tour de boucle__ (`i_deb = 0`), cad que `P(1)` sera vrai.

`P(1) : "liste([0 : 1[) est triée et tous les éléments de liste([1 : n[) sont >= à ceux de liste([0 : 1[)"`

Or : au premier tour de boucle, on détermine le plus petit élément de la liste et on le positionne en première position (indice `0`).  
On a donc bien :
* `liste([0 : 1[) triée` (un seul élément dans cette sous-liste)
* `tous les éléments de liste([1 : n[) sont >= à ceux de liste([0 : 1[)` puisqu'on a mis le plus petit en `liste[0]`.

Ainsi, `P(1)` sera vrai

__Propagation__ :

Supposons que `P(i)` soit vraie (`i_deb = i`) en début du corps de boucle et démontrons alors que `P(i + 1)` sera vraie en fin d'exécution du corps de boucle.

On suppose donc que `"liste([0 : i[) est triée et tous les éléments de liste([i : n[) sont >= à ceux de liste([0 : i[)"`

Démontrons que `"liste([0 : i + 1[) est triée et tous les éléments de liste([i + 1 : n[) sont >= à ceux de liste([0 : i + 1[)"`


Lors de l'exécution du corps de boucle (`i_deb = i`), on détermine le plus petit élément de `liste([i_deb : n[)` et on le place en indice `i_deb`.

Ainsi :
* puisque `liste([0 : i[) était triée` et que `tous les éléments de liste([i : n[) sont >= à ceux de liste([0 : i[)`, la valeur minimum trouvée dans `liste([i : n[)` est supérieure à tous les éléments de `liste([0 : i[)`.  
  Cette valeur ayant été placée en dernier, on a donc `liste([0 : i + 1[) triée`
* tous les éléments de `liste([i : n[) sont >= à ceux de liste([0 : i[)`.  
  On a pris le minimum de `liste([i : n[)` pour le placer en position d'indice `i`.  
  Ainsi, tous les éléments de `liste([i + 1 : n[)` seront supérieurs ou égaux à ceux de `liste([0 : i + 1[)`
  
Conclusion : `P(i)` vraie en début d'exécution de la boucle entraine que `P(i + 1)` sera vraie en fin d'exécution du corps de boucle.

__Terminaison__ :

On effectue le dernier tour de boucle pour `i_deb = n - 2`.  
On pourra donc en déduire que `P(n - 1)` sera vraie, c'est à dire :

`"liste([0 : n - 1[) est triée et tous les éléments de liste([n - 1 : n[) sont >= à ceux de liste([0 : n - 1[)"`

autrement dit :

`"liste([0 : n - 2]) est triée et liste[n - 1] (le dernier élément) est >= à tous ceux de liste([0 : n - 2])"`

Conclusion : __la liste est triée__
