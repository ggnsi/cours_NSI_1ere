# Algorithmes de tri

## Problématique

> Avoir des données triées permet d'accélérer le traitement des données ultérieurs, de faciliter des recherches (ex : recherche dichotomique)

_Exemple_ :

* Dans le dictionnaire, les mots sont rangés par ordre alphabétique afin de faciliter leur recherche ;
* La détermination de la médiane d'une série  statistique nécessite le tri des données.

---
### Ressources

* [Feuille d'activité qui sera utilisée pendant le cours](./21_22_chap_11_3_tris_activites.pdf) et son [corrige](./corriges/21_22_chap_11_3_tris_activites_corrigé.pdf)
* [vidéo Lumni sur les tris](https://www.lumni.fr/video/les-algorithmes-de-tri)
---

### Tri par sélection

1. [Principe et travail à faire avec la fiche d'activité](./11_3_tri_selection_principe.md)
2. [Résumé de cours sur le tri par sélection](./11_3_tri_selection_cours_1.md) avec preuve de complexité et de terminaison.

---

### Tri par insertion

1. [Principe et travail à faire avec la fiche d'activité](./11_3_tri_insertion_principe.md)
2. [Activité : écrire l'algorithme du tri par insertion](./11_3_tri_insertion_ecriture_algo.md) et (à ne regarder qu'après !)  [Le code du tri par insertion](./11_3_tri_insertion_algo_correction.md)
3. [Résumé de cours sur le tri par insertion avec complexité et terminaison](./11_3_tri_insertion_cours_complexite_terminaison.md)

---

### Preuve de correction d'algorithmes

1. [Activité sur les invariants de boucle](./11_3_activite_en_cours_invariant_de_boucles.md)
2. [Preuve de la correction des tris par sélection et par insertion](./11_3_les_tris_correction_des_tris.md)

---

### Résumés complets des cours

* [Cours complet sur le tri par sélection](./11_3_tri_selection_cours_complet.md)
* [Cours complet sur le tri par insertion](./11_3_tri_insertion_cours_complet.md)

---

### TD

* [TD du chapitre](./TD/11_3_TD_les_tris.md)

---

### Compléments à lire

* [Compléments sur les tris](./11_3_tris_complements.md)


