# Le algorithmes de tris - tri par sélection

### Principe du tri par sélection :

Le principe de ce tri est de trier progressivement la liste en cherchant le plus petit élément de la partie restant à trier et de l'échanger avec le premier élément de cette partie.

_Un exemple illustré_ :

Soit la liste de départ ci-dessous :
![liste_initiale](img/tri_selection_img_1_etape_0.png)


* première étape : on recherche la plus petite valeur (1 ici) et on l'échange avec celle d'indice 0

  ![liste_initiale](img/tri_selection_img_1_etape_1.png)  
  

  * La partie du tableau allant jusqu'à l'indice 0 est trié
  * celle au delà de l'indice 1 reste à trier et tous ses éléments sont plus grands que celui de la partie gauche.
  
  
* deuxième étape : on recherche la plus petite valeur dans la partie restant à trier (3 ici) et on l'échange avec celle d'indice 1

  ![liste_initiale](img/tri_selection_img_1_etape_2.png)
  
  
  * La partie du tableau allant jusqu'à l'indice 1 est trié
  * celle au delà de l'indice 2 reste à trier et tous ses éléments sont plus grands que c'eux de la partie gauche.


* troisième étape : on recherche la plus petite valeur dans la partie restant à trier (6 ici) et on l'échange avec celle d'indice 1

  ![liste_initiale](img/tri_selection_img_1_etape_3.png)
  
  
  * La partie du tableau allant jusqu'à l'indice 1 est trié
  * celle au delà de l'indice 2 reste à trier et tous ses éléments sont plus grands que c'eux de la partie gauche mais vu qu'il n'en reste qu'un, la liste est triée.

Le principe général à chaque étape est donc le suivant :

  ![liste_initiale](img/tri_selection_img_2_modif.png)

Tous les éléments de la partie non triée étant plus grand que ceux de la partie triée.

### A faire :

1. Ecrire en langage naturel (2 à 3 lignes) le principe du tri par sélection en considérant une liste de taille `n`.
2. Trier à la main les listes proposées sur la [feuille d'activité](./20_21_chap_11_3_tris_activites.pdf).  
Pour chaque tri de liste, compter le nombre de comparaisons effectuées.  
3. On considére comme opération prépondérant la comparaison entre deux éléments. La complexité `T(n)` est donc égale au nombre de comparaisons effectuées.
    * Y-a-t-il un type de liste minimisant le nombre de comparaisons lors du tri par sélection ? Si oui, donner une telle liste.
    * Y-a-t-il un type de liste maximisant le nombre de comparaisons lors du tri par sélection ? Si oui, donner une telle liste.
    * Déterminer le nombre `T(n)` de comparaisons effectuées lors du tri de la liste en fonction du nombre `n` d'éléments de la liste.
4. Ecrire une fonction `tri_selection(liste)` qui trie _en place_ `liste` en utilisant le tri par sélection.
