# Algorithmes de tri : le tri par insertion

## Principe du tri par insertion

Le principe de ce tri par insertion est de trier progressivement la liste en maintenant une partie triée sur la gauche et en prenant chaque élément suivant pour aller le positionner à sa place.

_Un exemple illustré_ :

Soit la liste de départ ci-dessous :

![etape0](img/tri_insertion_expl_img_1_etape_0_redimensionner.png)

* La sous-liste constituée de `9` est triée.  
On prend l'élément suivant : `2` et on va l'insére à sa place dans la sous liste de gauche (donc ici avant le 9)

![etape1](img/tri_insertion_expl_img_1_etape_1_redimensionner.png)

* La sous liste constituée de `2, 9` est triée.  
On prend l'élément suivant : `5` et on va l'insére à sa place dans la sous liste de gauche (donc ici entre le `2` et le `9`)

![etape2](img/tri_insertion_expl_img_1_etape_2_redimensionner.png)

* La sous liste constituée de `2, 5, 9` est triée.  
On prend l'élément suivant : `4` et on va l'insére à sa place dans la sous liste de gauche.

![etape3](img/tri_insertion_expl_img_1_etape_3_redimensionner.png)

* La sous liste constituée de `2, 4, 5, 9` est triée.  
On prend l'élément suivant : `3` et on va l'insére à sa place dans la sous liste de gauche.

![etape4](img/tri_insertion_expl_img_1_etape_4_redimensionner.png)

* `3` était le dernier élément de la liste initiale : la liste est triée.

![etape4](img/tri_insertion_expl_img_1_etape_5_redimensionner.png)

__Principe de l'algorithme de tri par insertion__ (`n = len(liste)`)


```python
Pour i allant de 1 à n - 1: # on commence bien à l'indice 1
    insérer l'élément d'indice i à sa place dans la liste triée de gauche
```

## Comment insérer l'élément suivant à sa place ?

L'idée est de décaler progressivement vers le droites toutes les valeurs supérieures à `élément suivant` puis d'insérer celui-ci.

_Un exemple pour bien comprendre_ :

Repartons de la situation ci-dessous (4ème étape de l'exemple précédent) :

![etape0](img/tri_insertion_expl_img_1_etape_4_redimensionner.png)

* on sauvegarde la valeur que l'on veut insérer à sa place (ici `3`, élément d'indice `i = 4`) dans une variable `val` (par exemple)

![etape1](img/tri_insertion_expl_img_2_etape_0_redimensionner.png)

* on a `3 < 9` : on décale `9` en le recopiant dans la case de droite

![etape2](img/tri_insertion_expl_img_2_etape_1_redimensionner.png)

* on a `3 < 5` : on décale `5` en le recopiant dans la case de droite

![etape3](img/tri_insertion_expl_img_2_etape_2_redimensionner.png)

* on a `3 < 4` : on décale `4` en le recopiant dans la case de droite

![etape4](img/tri_insertion_expl_img_2_etape_3_redimensionner.png)

* on a `3 >= 2` : on insère `3` à sa place

![etape5](img/tri_insertion_expl_img_2_etape_4_redimensionner.png)

__Important__ : si l'on était arrivé en début de tableau, on s'arrêterait et on insèrerait la valeur en début de tableau.

### A faire :

1. Effectuer à la main les tris des listes sur la feuille d'activité et dans chacun des cas, déterminer le nombre de comparaisons effectuées.

> Faire l'effort de réaliser toutes les étapes pour bien comprendre le fonctionnement du tri.

2. Quel type de liste correspond au meilleur des cas pour le tri par insertion (cad celui nécessitant le moins de comparaisons) ?  
    Si la liste est de longueur `n`, quelle sera alors la complexité `T(n)` du tri par insertion (cad le nombre de comparaisons effectuées)
3. Quel type de liste correspond au pire des cas pour le tri par insertion (cad celui nécessitant le moins de comparaisons) ?  
    Si la liste est de longueur `n`, quelle sera alors la complexité `T(n)` du tri par insertion (cad le nombre de comparaisons effectuées) ?  
    Quel sera l'ordre de grandeur de la complexité dans le pire des cas pour le tri par insertion ?
