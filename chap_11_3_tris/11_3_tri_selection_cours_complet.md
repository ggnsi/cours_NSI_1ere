# Le algorithmes de tris - tri par sélection
---

## Principe du tri par sélection :

Le principe de ce tri est de trier progressivement la liste en cherchant le plus petit élément de la partie restant à trier et de l'échanger avec le premier élément de cette partie.

_Un exemple illustré_ :

Soit la liste de départ ci-dessous :
![liste_initiale](img/tri_selection_img_1_etape_0.png)


* première étape : on recherche la plus petite valeur (1 ici) et on l'échange avec celle d'indice 0

  ![liste_initiale](img/tri_selection_img_1_etape_1.png)  
  

  * La partie du tableau allant jusqu'à l'indice 0 est trié
  * celle au delà de l'indice 1 reste à trier et tous ses éléments sont plus grands que celui de la partie gauche.
  
  
* deuxième étape : on recherche la plus petite valeur dans la partie restant à trier (3 ici) et on l'échange avec celle d'indice 1

  ![liste_initiale](img/tri_selection_img_1_etape_2.png)
  
  
  * La partie du tableau allant jusqu'à l'indice 1 est trié
  * celle au delà de l'indice 2 reste à trier et tous ses éléments sont plus grands que c'eux de la partie gauche.


* troisième étape : on recherche la plus petite valeur dans la partie restant à trier (6 ici) et on l'échange avec celle d'indice 1

  ![liste_initiale](img/tri_selection_img_1_etape_3.png)
  
  
  * La partie du tableau allant jusqu'à l'indice 1 est trié
  * celle au delà de l'indice 2 reste à trier et tous ses éléments sont plus grands que c'eux de la partie gauche mais vu qu'il n'en reste qu'un, la liste est triée.

Le principe général à chaque étape est donc le suivant :

  ![liste_initiale](img/tri_selection_img_2_modif.png)

__Important__ : Tous les éléments de la partie non triée étant plus grands que ceux de la partie triée.

__Algorithme en langage naturel avec une liste de taille n (indices allant de 0 à n - 1)__


```python
Pour i_debut allant de 0 à n - 2:
    Rechercher le plus petit élément dans liste([i_debut : fin_liste])
    l'échanger avec l'élément d'indice i_debut
```

__Remarque__ : une fois liste([0 : n - 2]) triée, la liste est triée puisque l'élément restant sera plus grand que tous ceux de la partie triée.


## Ecriture du code

### Recherche du rang du plus petit élément de la zone liste([i_debut : fin_liste])

Algorithme classique déjà vu :


```python
min = liste[i_deb]
i_min = i_deb
for ind in range(i_deb + 1, len(liste)):
    if liste[ind] < min:
        min = liste[ind]
        i_min = ind
```

### Echange des valeurs des termes de rang `i_deb` et  `i_min`

Classiquement, il faut commence par sauvegarde la valeur de `liste[i_deb]` dans une variable auxiliaire avant d'écraser la valeur de `liste[i_deb]`.


```python
temp = liste[i_deb]
liste[i_deb] = min # ou liste[i_min]
liste[i_min] = temp
```

En regroupant les deux étapes, on aboutit donc au code suivant :


```python
def tri_selection(liste):
    '''Trie la liste par ordre croissant en utilisant le tri par sélection
    param liste : (list)
    valeur retournée : aucune
    
    effet de bord : modifie en place la liste'''
    
    for i_deb in range(len(liste) - 1):
        
        # recherche du minimum dans la zone liste[i_deb : fin_liste]
        min = liste[i_deb]
        i_min = i_deb
        for ind in range(i_deb + 1, len(liste)):
            if liste[ind] < min:
                min = liste[ind]
                i_min = ind
        
        # échange des termes de rang i_deb et i_min
        temp = liste[i_deb]
        liste[i_deb] = min # ou liste[i_min]
        liste[i_min] = temp
```


```python
l = [5, 9, 4, 2, 6]
tri_selection(l)
print(l)
```

    [2, 4, 5, 6, 9]
    

__Remarques__ :

* on pourrait tester si `i_min = i_deb` pour éviter l'échange d'un élément avec lui-même ;
* `i_deb` pourrait parcourir tous les indices de la liste : la boucle secondaire ne tournerait juste pas pour le dernier indice.

### Version plus modulaire

On crée une fonction `échange(liste, i_1, i_2)` qui échange _en place_ les termes de rang `i_1` et `i_2` de la liste


```python
def echange(liste, i_1, i_2):
    '''échange les termes de rang i_1 et i_2 de la liste
    param liste : (list)
    param i_1, i_2 : (int)
    CU : 0 <= i_1 <= len(liste)-1 et 0 <= i_2 <= len(liste)-1
    Effet de bord : liste modifiée en place'''
    
    temp = liste[i_1]
    liste[i_1] = liste[i_2]
    liste[i_2] = temp

def tri_selection(liste):
    '''Trie la liste par ordre croissant en utilisant le tri par sélection
    param liste : (list)
    valeur retournée : aucune
    
    effet de bord : modifie en place la liste'''
    
    for i_deb in range(len(liste) - 1):
        
        i_min = i_deb
        for ind in range(i_deb + 1, len(liste)):
            if liste[ind] < liste[i_min]:
                i_min = ind
        
        echange(liste, i_deb, i_min)
```

## Etude de la complexité du tri par sélection

Nous ne prendrons ici en compte que le nombre de comparaisons effectuées.

> Il n'y a pas de meilleur ni de pire des cas : le nombre de comparaison effectuées est toujours le même quelque soit la liste

Décompte du nombre de comparaisons (on note ici `n` le nombre d'élément de `liste`:

* `i_deb = 0` : `n - 1` comparaisons effectuées (on compare `liste[0]` avec tous les autres éléments de liste) ;
* `i_deb = 1` : `n - 2` comparaisons effectuées (on compare `liste[1]` avec tous les autres éléments de liste) ;
* `i_deb = 2` : `n - 3` comparaisons effectuées (on compare `liste[1]` avec tous les autres éléments de liste) ;
* ...
* `i_deb = n - 2` : `1` comparaison effectuée (on compare `liste[n - 2]` avec `liste[n - 1]`)

Ainsi : `T(n) = 1 + 2 + 3 + ... + (n-1)`

On obtient la somme des termes consécutifs d'une suite arithmétique de premier terme `u_1 = 1` et de raison `r = 1`.

Donc `T(n)= (n - 1) * (1 + (n - 1))/2 = (n - 1) * n / 2 = (n^2 - n) / 2`

ou encore `T(n) = 1/2 * n^2 - 1/2 n`

Lorsque `n` devient grand, `n` devient négligeable devant `n^2` et `T(n) = O(n^2)`.

> La complexité du de tri par sélection est donc quadratique puisque `T(n) = O(n^2)`

## Preuve de terminaison

L'algorithme comporte deux boucles `for` qui sont donc nécessairement finies. On est donc sur que cet algorithme se termine.

## Preuve de correction de l'algorithme

Il s'agit ici de montrer que l'algorithme réalise bien le résultat recherché.

Soit __l'invariant de boucle__ : `P(i) : "liste([0 : i[) est triée et tous les éléments de liste([i : n[) sont >= à ceux de liste([0 : i[)"`

__Initialisation__ : 

Démontrons ici que l'invariant sera vrai __en fin d'exécution du premier tour de boucle__ (`i_deb = 0`), cad que `P(1)` sera vrai.

`P(1) : "liste([0 : 1[) est triée et tous les éléments de liste([1 : n[) sont >= à ceux de liste([0 : 1[)"`

Or : au premier tour de boucle, on détermine le plus petit élément de la liste et on le positionne en première position (indice `0`).  
On a donc bien :
* `liste([0 : 1[) triée` (un seul élément dans cette sous-liste)
* `tous les éléments de liste([1 : n[) sont >= à ceux de liste([0 : 1[)` puisqu'on a mis le plus petit en `liste[0]`.

Ainsi, `P(1)` sera vrai

__Propagation__ :

Supposons que `P(i)` soit vraie (`i_deb = i`) en début du corps de boucle et démontrons alors que `P(i + 1)` sera vraie en fin d'exécution du corps de boucle.

On suppose donc que `"liste([0 : i[) est triée et tous les éléments de liste([i : n[) sont >= à ceux de liste([0 : i[)"`

Démontrons que `"liste([0 : i + 1[) est triée et tous les éléments de liste([i + 1 : n[) sont >= à ceux de liste([0 : i + 1[)"`


Lors de l'exécution du corps de boucle (`i_deb = i`), on détermine le plus petit élément de `liste([i_deb : n[)` et on le place en indice `i_deb`.

Ainsi :
* puisque `liste([0 : i[) était triée` et que `tous les éléments de liste([i : n[) sont >= à ceux de liste([0 : i[)`, la valeur minimum trouvée dans `liste([i : n[)` est supérieure à tous les éléments de `liste([0 : i[)`.  
  Cette valeur ayant été placée en dernier, on a donc `liste([0 : i + 1[) triée`
* tous les éléments de `liste([i : n[) sont >= à ceux de liste([0 : i[)`.  
  On a pris le minimum de `liste([i : n[)` pour le placer en position d'indice `i`.  
  Ainsi, tous les éléments de `liste([i + 1 : n[)` seront supérieurs ou égaux à ceux de `liste([0 : i + 1[)`
  
Conclusion : `P(i)` vraie en début d'exécution de la boucle entraine que `P(i + 1)` sera vraie en fin d'exécution du corps de boucle.

__Terminaison__ :

On effectue le dernier tour de boucle pour `i_deb = n - 2`.  
On pourra donc en déduire que `P(n - 1)` sera vraie, c'est à dire :

`"liste([0 : n - 1[) est triée et tous les éléments de liste([n - 1 : n[) sont >= à ceux de liste([0 : n - 1[)"`

autrement dit :

`"liste([0 : n - 2]) est triée et liste[n - 1] (le dernier élément) est >= à tous ceux de liste([0 : n - 2])"`

Conclusion : __la liste est triée__

Sources :

* images : [iznbreith](https://www.isnbreizh.fr/nsi/activity/algoRefTri/tri_selection1/index.html), https://rmdiscala.developpez.com, 
