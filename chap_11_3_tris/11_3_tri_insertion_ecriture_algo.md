# Tri par insertion : écriture de l'algorithme

_Rappel : principe général_ :


```python
Pour i allant de 1 à n - 1: # on commence bien à l'indice 1
    insérer l'élément d'indice i à sa place dans la liste triée de gauche
```

### Ecriture de l'algorithme

>Reprenons en détail la partie insertion sur l'exemple d'introduction:

L'idée est de décaler progressivement vers le droites toutes les valeurs supérieures à `élément suivant` puis d'insérer celui-ci.

_Un exemple pour bien comprendre_ :

Repartons de la situation ci-dessous (4ème étape de l'exemple précédent) :

![etape0](img/tri_insertion_expl_img_1_etape_4_redimensionner.png)

* on sauvegarde la valeur que l'on veut insérer à sa place (ici `3`, élément d'indice `i = 4`) dans une variable `val`.

    Si `3` (`val`) était bien placé, sa place actuelle serait `i = 4`.

![etape1](img/tri_insertion_expl_img_2_etape_0_redimensionner.png)

* mais on a `3 < 9` (`val < L[i - 1] `) alors :
    * on décale `9` en le recopiant dans la case de droite (`L[3]` copié dans `L[4]`, cad `L[i - 1]` copié dans `L[i]`)
    * on décrémente `i` de `1` car si `3` (`val`) était bien placé, sa place actuelle serait `i = 3`.

![etape2](img/tri_insertion_expl_img_2_etape_1_redimensionner.png)

* mais on a `3 < 5` (`val < L[i - 1] `) alors :
    * on décale `5` en le recopiant dans la case de droite (`L[2]` copié dans `L[3]`, cad `L[i - 1]` copié dans `L[i]`)
    * on décrémente `i` de `1` car si `3` (`val`) était bien placé, sa place actuelle serait `i = 2`.

![etape3](img/tri_insertion_expl_img_2_etape_2_redimensionner.png)

* mais on a `3 < 4` (`val < L[i - 1] `) alors :
    * on décale `4` en le recopiant dans la case de droite (`L[1]` copié dans `L[2]`, cad `L[i - 1]` copié dans `L[i]`)
    * on décrémente `i` de `1` car si `3` (`val`) était bien placé, sa place actuelle serait `i = 1`.

![etape4](img/tri_insertion_expl_img_2_etape_3_redimensionner.png)

* on a `3 >= 2` (`val >= L[i - 1] `) :
    * c'est donc que l'on a trouvé la place de `3` (`val`) ;
    * cette place est à l'indice `i = 1` ;
    * on insère `3` à sa place (`L[i] = val`)

![etape5](img/tri_insertion_expl_img_2_etape_4_redimensionner.png)


**Résumé** :

```Python
Tant que val < L[i - 1]:
    On copie L[i - 1] dans L[i]
    on décrémente i de 1

Insertion de val à l'indice i

```

__Important__ : 
> On doit aussi tester à chaque étape si l'on est **pas arrivé en bout de tableau**. 
>
> **Ne PAS être en bout de tableau (et donc continuer le processus) c'est avoir `i >= 1`** (ou `i - 1 >= 0`)

Si on arrive en début de tableau (`i = 0`), on arrête le processus et on insèrerait la valeur à l'indice `i` (`L[i] = val`).

### A faire :

Ecrire l'algorithme en Python du tri par insertion


```python
def tri_insertion(tab):
    '''réalise en place le tri par insertion de tab
    param tab : (list)
    valeur retournée : aucune
    Effet de bord : liste triée en place
    
    Exemples :
    >>> liste = [9, 2, 5, 4, 3]
    >>> tri_insertion(liste)
    >>> liste
    [2, 3, 4, 5, 9]
    >>> liste = [1, 2, 3, 4, 5]
    >>> tri_insertion(liste)
    >>> liste
    [1, 2, 3, 4, 5]
    >>> liste = [5, 4, 3, 2, 1]
    >>> tri_insertion(liste)
    >>> liste
    [1, 2, 3, 4, 5]
    >>> liste = []
    >>> tri_insertion(liste)
    >>> liste
    []
    '''
    
    # a compléter
```
