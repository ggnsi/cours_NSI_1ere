# Tri par insertion : écriture de l'algorithme

_Rappel : principe général_ :


```python
Pour i allant de 1 à n - 1: # on commence bien à l'indice 1
    insérer l'élément d'indice i à sa place dans la liste triée de gauche
```

### Ecriture de l'algorithme

>Reprenons en détail la partie insertion sur l'exemple d'introduction:

**Résumé de la partie "insertion de l'élément"** :

```Python
Tant que i >= 1 et val < L[i - 1]:
    On copie L[i - 1] dans L[i]
    on décrémente i de 1

Insertion de val à l'indice i

```

### Algorithme du tri par insertion en Python


```python
def tri_insertion(tab):
    '''réalise en place le tri par insertion de tab
    param tab : (list)
    valeur retournée : aucune
    Effet de bord : liste triée en place
    
    Exemples :
    >>> liste = [9, 2, 5, 4, 3]
    >>> tri_insertion(liste)
    >>> liste
    [2, 3, 4, 5, 9]
    >>> liste = [1, 2, 3, 4, 5]
    >>> tri_insertion(liste)
    >>> liste
    [1, 2, 3, 4, 5]
    >>> liste = [5, 4, 3, 2, 1]
    >>> tri_insertion(liste)
    >>> liste
    [1, 2, 3, 4, 5]
    >>> liste = []
    >>> tri_insertion(liste)
    >>> liste
    []
    '''
    
    for ind in range(1, len(tab)):
        val = tab[ind]
        i = ind
        while (i - 1 >= 0) and (tab[i - 1] > val):
            tab[i] = tab[i - 1]
            i = i - 1
        tab[i] = val
```

**Remarque** :

Si `(i - 1 >= 0)` est faux (c'est à dire qu'on est en début de tableau), le caractère "fainéant" de Python vis à vis de la condition `and` (si la première condition est fausse, pas la peine de regarder la deuxième car le `and` donnera `False`) fait que la condition `tab[i - 1] > val` ne sera pas évaluée (donc pas d'erreur)
