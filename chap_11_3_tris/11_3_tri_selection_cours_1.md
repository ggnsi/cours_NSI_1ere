# Le algorithmes de tris - tri par sélection
---

## Principe du tri par sélection :

Le principe de ce tri est de trier progressivement la liste en cherchant le plus petit élément de la partie restant à trier et de l'échanger avec le premier élément de cette partie.

_Un exemple illustré_ :

Soit la liste de départ ci-dessous :
![liste_initiale](img/tri_selection_img_1_etape_0.png)


* première étape : on recherche la plus petite valeur (1 ici) et on l'échange avec celle d'indice 0

  ![liste_initiale](img/tri_selection_img_1_etape_1.png)  
  

  * La partie du tableau allant jusqu'à l'indice 0 est trié
  * celle au delà de l'indice 1 reste à trier et tous ses éléments sont plus grands que celui de la partie gauche.
  
  
* deuxième étape : on recherche la plus petite valeur dans la partie restant à trier (3 ici) et on l'échange avec celle d'indice 1

  ![liste_initiale](img/tri_selection_img_1_etape_2.png)
  
  
  * La partie du tableau allant jusqu'à l'indice 1 est trié
  * celle au delà de l'indice 2 reste à trier et tous ses éléments sont plus grands que c'eux de la partie gauche.


* troisième étape : on recherche la plus petite valeur dans la partie restant à trier (6 ici) et on l'échange avec celle d'indice 1

  ![liste_initiale](img/tri_selection_img_1_etape_3.png)
  
  
  * La partie du tableau allant jusqu'à l'indice 1 est trié
  * celle au delà de l'indice 2 reste à trier et tous ses éléments sont plus grands que c'eux de la partie gauche mais vu qu'il n'en reste qu'un, la liste est triée.

Le principe général à chaque étape est donc le suivant :

  ![liste_initiale](img/tri_selection_img_2_modif.png)

__Important__ : Tous les éléments de la partie non triée étant plus grands que ceux de la partie triée.

__Algorithme en langage naturel avec une liste de taille n (indices allant de 0 à n - 1)__


```python
Pour i_debut allant de 0 à n - 2:
    Rechercher le plus petit élément dans liste([i_debut : fin_liste])
    l'échanger avec l'élément d'indice i_debut
```

__Remarque__ : une fois liste([0 : n - 2]) triée, la liste est triée puisque l'élément restant sera plus grand que tous ceux de la partie triée.


## Ecriture du code

### Recherche du rang du plus petit élément de la zone liste([i_debut : fin_liste])

Algorithme classique déjà vu :


```python
mini = liste[i_deb]
i_mini = i_deb
for ind in range(i_deb + 1, len(liste)):
    if liste[ind] < mini:
        mini = liste[ind]
        i_mini = ind
```

### Echange des valeurs des termes de rang `i_deb` et  `i_min`

Classiquement, il faut commence par sauvegarde la valeur de `liste[i_deb]` dans une variable auxiliaire avant d'écraser la valeur de `liste[i_deb]`.


```python
temp = liste[i_deb]
liste[i_deb] = mini # ou liste[i_min]
liste[i_mini] = temp
```

En regroupant les deux étapes, on aboutit donc au code suivant :


```python
def tri_selection(liste):
    '''Trie la liste par ordre croissant en utilisant le tri par sélection
    param liste : (list)
    valeur retournée : aucune
    
    effet de bord : modifie en place la liste'''
    
    for i_deb in range(len(liste) - 1):
        
        # recherche du minimum dans la zone liste[i_deb : fin_liste]
        mini = liste[i_deb]
        i_mini = i_deb
        for ind in range(i_deb + 1, len(liste)):
            if liste[ind] < mini:
                mini = liste[ind]
                i_mini = ind
        
        # échange des termes de rang i_deb et i_min
        temp = liste[i_deb]
        liste[i_deb] = mini # ou liste[i_min]
        liste[i_mini] = temp
```


```python
l = [5, 9, 4, 2, 6]
tri_selection(l)
print(l)
```

    [2, 4, 5, 6, 9]
    

__Remarques__ :

* on pourrait tester si `i_min = i_deb` pour éviter l'échange d'un élément avec lui-même ;
* `i_deb` pourrait parcourir tous les indices de la liste : la boucle secondaire ne tournerait juste pas pour le dernier indice.

### Version plus modulaire

On crée une fonction `échange(liste, i_1, i_2)` qui échange _en place_ les termes de rang `i_1` et `i_2` de la liste


```python
def echange(liste, i_1, i_2):
    '''échange les termes de rang i_1 et i_2 de la liste
    param liste : (list)
    param i_1, i_2 : (int)
    CU : 0 <= i_1 <= len(liste)-1 et 0 <= i_2 <= len(liste)-1
    Effet de bord : liste modifiée en place'''
    
    temp = liste[i_1]
    liste[i_1] = liste[i_2]
    liste[i_2] = temp

def tri_selection(liste):
    '''Trie la liste par ordre croissant en utilisant le tri par sélection
    param liste : (list)
    valeur retournée : aucune
    
    effet de bord : modifie en place la liste'''
    
    for i_deb in range(len(liste) - 1):
        
        i_mini = i_deb
        for ind in range(i_deb + 1, len(liste)):
            if liste[ind] < liste[i_mini]:
                i_mini = ind
        
        echange(liste, i_deb, i_mini)
```

## Etude de la complexité du tri par sélection

Nous ne prendrons ici en compte que le nombre de comparaisons effectuées.

> Il n'y a pas de meilleur ni de pire des cas : le nombre de comparaison effectuées est toujours le même quelque soit la liste

Décompte du nombre de comparaisons (on note ici `n` le nombre d'élément de `liste`:

* `i_deb = 0` : `n - 1` comparaisons effectuées (on compare `liste[0]` avec tous les autres éléments de liste) ;
* `i_deb = 1` : `n - 2` comparaisons effectuées (on compare `liste[1]` avec tous les autres éléments de liste) ;
* `i_deb = 2` : `n - 3` comparaisons effectuées (on compare `liste[1]` avec tous les autres éléments de liste) ;
* ...
* `i_deb = n - 2` : `1` comparaison effectuée (on compare `liste[n - 2]` avec `liste[n - 1]`)

Ainsi : `T(n) = 1 + 2 + 3 + ... + (n-1)`

On obtient la somme des termes consécutifs d'une suite arithmétique de premier terme `u_1 = 1` et de raison `r = 1`.

Donc `T(n)= (n - 1) * (1 + (n - 1))/2 = (n - 1) * n / 2 = (n^2 - n) / 2`

ou encore `T(n) = 1/2 * n^2 - 1/2 n`

Lorsque `n` devient grand, `n` devient négligeable devant `n^2` et `T(n) = O(n^2)`.

> La complexité du de tri par sélection est donc quadratique puisque `T(n) = O(n^2)`

## Preuve de terminaison

L'algorithme comporte deux boucles `for` qui sont donc nécessairement finies. On est donc sur que cet algorithme se termine.


Sources :

* images : [iznbreith](https://www.isnbreizh.fr/nsi/activity/algoRefTri/tri_selection1/index.html), https://rmdiscala.developpez.com, 
