# Tri par insertion : complexité et terminaison
---

## Complexité du tri par insertion

### Dans le meilleur des cas

__Le meilleur des cas est celui où la liste est déjà triée__

Alors, pour chaque tout de la boucle `for`, on effectue une seule comparaison (avec l'élément précédent).  
Il y a `n - 1` itérations de la boucle `for`.

Donc __T(n) = n - 1__ et __T(n) = O(n)__


### Dans le pire des cas

__Le meilleur des cas est celui où la liste est déjà triée en ordre décroissant__

Par exemple : (avec `a_0 > a_1 > a_2 > ... > a_{n-1}`)

![illustration](img/tri_insertion_expl_pire_cas_redimensionner.png)

Chaque élément doit donc être ramené en première position à chaque étape.

* Pour placer l'élément d'indice `1`, on effectue `1` comparaison ;
* Pour placer l'élément d'indice `2`, on effectue `2` comparaisons ;
* Pour placer l'élément d'indice `3`, on effectue `3` comparaisons ;
* ...
* Pour placer l'élément d'indice `n - 1`, on effectue `n - 1` comparaisons ;

Donc __T(n) = 1 + 2 + 3 + ... + (n - 1) = 1/2 n^2 - 1/2 n__ (déjà vu dans le tri par sélection)

et donc __T(n) = O(n^2)__

> La complexité dans le pire des cas du tri par insertion est quadratique.

### Conclusion sur la complexité

>Dans le pire des cas, le tri par insertion a une complexité quadratique identique à celle du tri par sélection.
>
>Cependant il sera beaucoup plus rapide si le tableau est "presque trié"

## Terminaison du tri par insertion

> Démontrons que cet algorithme ce termine systèmatiquement.

```Python
def tri_insertion(tab):
    
    for ind in range(1, len(tab)):
        val = tab[ind]
        i = ind
        while (i - 1 >= 0) and (tab[i - 1] > val):
            tab[i] = tab[i - 1]
            i = i - 1
        tab[i] = val
```

La boucle `for` n'effectuera de manière certaine qu'un nombre fini d'itération.  
Il nous faut donc justifier que pour chaque valeur de `i`, la boucle `while` se termine.

Utilisons le __variant de boucle__ : `i`

Démontrons que :
1. `i` est toujours un entier ;
2. qu'on a toujours `i >= 0` ;
3. qu'à chaque fin d'exécution du corps de boucle, soit on sort de la boucle, soit `i` a au moins diminué d'une unité

__A l'entrée dans la boucle__ : `i = ind` et `ind` entier donc `i` entier.

__A chaque entrée dans la boucle__ : `i - 1 >= 0` donc `i >= 1` et donc `i >= 0`

__A chaque sortie du corps de boucle__ : on a effectué `i = i - 1` donc `i` reste entier et a diminué d'une unité.

Ainsi, la boucle `while` se termine toujours et l'algorithme se termine nécessairement.
