# Comment prouver la correction d'un algorithme

> Prouver la correction d'un algorithme, c'est démontrer que l'algorithme réalise bien le résultat recherché

Nous avons besoin d'une méthode rigoureuse pour cela.  
Il n'est en effet pas toujours facile de décrire facilement le contenu des variables en sortie d'une boucle.

## La méthode

> Lorsqu'une boucle est présente, on cherche un __invariant__ de boucle.  

> Un __invariant__ de boucle est une propriété qui, malgré la modification des variables de boucles, reste vraie après chaque itération de la boucle.

_Remarque_ : 
* Pour exprimer un invariant de boucle faisant intervenir des variables _a_, _b_ , ..., on pourra si besoin noter _a(k)_ , _b(k)_ , ..., les valeurs contenues dans les variables _a_ , _b_ après la _k_-ième itération de la boucle. (On peut aussi utiliser des notations avec des indices.)
* De même, on pourra noter _P(k)_ l'invariant de boucle.

__Comment prouver qu'une assertion (une affirmation) est un invariant de boucle ?__

On réalise en fait une démonstration par récurrence (type de démonstration vu en spécialité maths en terminale).

On doit réaliser deux étapes :

* __L'initialisation__ : c'est à dire montrer que l'invariant est vrai avant la première itération (parfois après la première itération)
* __L'hérédité__ : c'est montrer que le fait que _P(k)_ soit vrai entraine que _P(k+1)_ sera encore vrai.  
  En général pour nous il faut montrer que si l'invariant _P(k)_ est vrai en début d'exécution du corps de boucle, _P(k+1)_ sera  vrai en fin d'exécution du corps de boucle (et donc au début du tour de boucle suivant si il a lieu).
  
On pourra alors conclure que l'invariant sera encore vrai en sortie de la dernière itération.

En effet (si `k` commence à `0`):
* `P(0)` vraie (grâce à l'initialisation)
* `P(0)` vraie => `P(1)` vraie (hérédité) ;
* `P(1)` vraie => `P(2)` vraie (hérédité) ;
* `P(2)` vraie => `P(3)` vraie (hérédité) ;
* etc
* `P(k)` sera donc vraie en sortie de la boucle.

## Premier exemple simple

Soit la fonction suivante :


```python
def somme(n):
    s = 0
    for i in range(1, n + 1):
        s = s + i
    return s
```

On veut prouver que l'algorithme réalise bien la somme des _n + 1_ premiers entiers : `0 + 1 + 2 + ... + n`

Considérons l'invariant `P(i) : " s = 0 + 1 + ... + (i - 1)`


```python
def somme(n):
    s = 0
    for i in range(1, n + 1):
        # invariant : P(i) : " s = 0 + 1 + ... + (i - 1)
        s = s + i
    return s
```



## Deuxième exemple simple

Soit la fonction suivante :


```python
def max(tab):
    max = tab[0]
    for i in range(1, len(tab)):
        if tab[i] > max:
            max = tab[i]
    return max
```

Nous voulons prouver en sortie de boucle que `max = maximum(tab[0 : n - 1])` (en notant `n = len(table)`)

Considérons l'invariant `P(i) : max = maximum( tab[0 : i -1])`



## Comment savoir quel invariant de boucle choisir ?

L'invariant de boucle le plus simple est, comme dans les exemples précédents, celui qui prédit le contenu de variables de la boucle.

Ce n'est cependant pas toujours possible de définir un tel invariant en pratique et il n'y a pas de règle générale.  
On peut cependant se demander si en appliquant en sortie un invariant il peut nous permettre de prouver la validité de notre programme.

_sources utilisées_ : 

* [developpement-informatique.com](https://developpement-informatique.com/article/335/invariant-de-boucle)
* Cours MPSI
