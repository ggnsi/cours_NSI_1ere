# Algorithmes de tri : le tri par insertion

## Principe du tri par insertion

Le principe de ce tri par insertion est de trier progressivement la liste en maintenant une partie triée sur la gauche et en prenant chaque élément suivant pour aller le positionner à sa place.

_Un exemple illustré_ :

Soit la liste de départ ci-dessous :

![etape0](img/tri_insertion_expl_img_1_etape_0_redimensionner.png)

* La sous-liste constituée de `9` est triée.  
On prend l'élément suivant : `2` et on va l'insére à sa place dans la sous liste de gauche (donc ici avant le 9)

![etape1](img/tri_insertion_expl_img_1_etape_1_redimensionner.png)

* La sous liste constituée de `2, 9` est triée.  
On prend l'élément suivant : `5` et on va l'insére à sa place dans la sous liste de gauche (donc ici entre le `2` et le `9`)

![etape2](img/tri_insertion_expl_img_1_etape_2_redimensionner.png)

* La sous liste constituée de `2, 5, 9` est triée.  
On prend l'élément suivant : `4` et on va l'insére à sa place dans la sous liste de gauche.

![etape3](img/tri_insertion_expl_img_1_etape_3_redimensionner.png)

* La sous liste constituée de `2, 4, 5, 9` est triée.  
On prend l'élément suivant : `3` et on va l'insére à sa place dans la sous liste de gauche.

![etape4](img/tri_insertion_expl_img_1_etape_4_redimensionner.png)

* `3` était le dernier élément de la liste initiale : la liste est triée.

![etape4](img/tri_insertion_expl_img_1_etape_5_redimensionner.png)

__Principe de l'algorithme de tri par insertion__ (`n = len(liste)`)


```python
Pour i allant de 1 à n - 1:
    insérer l'élément d'indice i à sa place dans la liste triée de gauche
```

## Comment insérer l'élément suivant à sa place ?

L'idée est de décaler progressivement vers le droites toutes les valeurs supérieures à `élément suivant` puis d'insérer celui-ci.

_Un exemple pour bien comprendre_ :

Repartons de la situation ci-dessous (4ème étape de l'exemple précédent) :

![etape0](img/tri_insertion_expl_img_1_etape_4_redimensionner.png)

* on sauvegarde la valeur que l'on veut insérer à sa place (ici `3`) dans une variable `val` (par exemple)

![etape1](img/tri_insertion_expl_img_2_etape_0_redimensionner.png)

* on a `3 < 9` : on décale `9` en le recopiant dans la case de droite

![etape2](img/tri_insertion_expl_img_2_etape_1_redimensionner.png)

* on a `3 < 5` : on décale `5` en le recopiant dans la case de droite

![etape3](img/tri_insertion_expl_img_2_etape_2_redimensionner.png)

* on a `3 < 4` : on décale `4` en le recopiant dans la case de droite

![etape4](img/tri_insertion_expl_img_2_etape_3_redimensionner.png)

* on a `3 >= 2` : on insère `3` à sa place

![etape5](img/tri_insertion_expl_img_2_etape_4_redimensionner.png)

__Important__ : si l'on était arrivé en début de tableau, on s'arrêterait et on insèrerait la valeur en début de tableau.

## Algorithme du tri par insertion


```python
def tri_insertion(tab):
    '''réalise en place le tri par insertion de tab
    param tab : (list)
    valeur retournée : aucune
    Effet de bord : liste triée en place'''
    
    for ind in range(1, len(tab)):
        val = tab[ind]
        i = ind
        while (i - 1 >= 0) and (tab[i - 1] > val):
            tab[i] = tab[i - 1]
            i = i - 1
        tab[i] = val
```


```python
liste = [9, 2, 5, 4, 3]
tri_insertion(liste)
print(liste)
```

    [2, 3, 4, 5, 9]
    

## Complexité du tri par insertion

### Dans le meilleur des cas

__Le meilleur des cas est celui où la liste est déjà triée__

Alors, pour chaque tout de la boucle `for`, on effectue une seule comparaison (avec l'élément précédent).  
Il y a `n - 1` itérations de la boucle `for`.

Donc __T(n) = n - 1__ et __T(n) = O(n)__


### Dans le pire des cas

__Le pire des cas est celui où la liste est triée en ordre décroissant__

![illustration](img/tri_insertion_expl_img_3_redimensionner.png)

* Pour placer l'élément d'indice `1`, on effectue `1` comparaison ;
* Pour placer l'élément d'indice `2`, on effectue `2` comparaisons ;
* Pour placer l'élément d'indice `3`, on effectue `3` comparaisons ;
* ...
* Pour placer l'élément d'indice `n - 1`, on effectue `n - 1` comparaisons ;

Donc __T(n) = 1 + 2 + 3 + ... + (n - 1) = 1/2 n^2 - 1/2 n__ (déjà vu dans le tri par sélection)
et donc  __T(n) = O(n^2)__

### Conclusion sur la complexité

Dans le pire des cas, le tri par insertion a une complexité quadratique identique à celle du tri par sélection.

Cependant il sera beaucoup plus rapide si le tableau est "presque trié"

## Terminaison du tri par insertion

La boucle `for` n'effectuera de manière certaine qu'un nombre fini d'itération.  
Il nous faut donc justifier que pour chaque valeur de `i`, la boucle `while` se termine.

Utilisons le __variant de boucle__ : `pos`

Démontrons que :
1. `pos` est toujours un entier ;
2. qu'on a toujours `pos >= 0` ;
3. qu'à chaque fin d'exécution du corps de boucle, soit on sort de la boucle, soit `pos` a au moins diminué d'une unité

__A l'entrée dans la boucle__ : `pos = i` et `i` entier donc `pos` entier.

__A chaque entrée dans la boucle__ : `pos - 1 >= 0` donc `pos >= 1` et donc `pos >= 0`

__A chaque sortie du corps de boucle__ : on a effectué `pos = pos - 1` donc `pos` reste entier et a diminué d'une unité.

Ainsi, la boucle `while` se termine toujours et l'algorithme se termine nécessairement.

## Correction du tri par insertion

__Invariant utilisé__ : Pour tout `i >= 1`, soit `P(i) : "tab([0, i[) est trié"`

__Initialisation__ : à la première itération, `i = 1` et `tab([0, 1[) = [tab[0]]` est triée

__Hérédité__ : 

Supposons que `P(i)` soit vraie en début de boucle `for` (après `i - 1` itérations ici) et démontrons que `P(i + 1)` sera alors vraie en sortie de corps de boucle.

`P(i) : "tab([0, i[) est trié"`

`P(i + 1) : "tab([0, i + 1[) est trié"`, soit `tab([0, i]) est trié`.

Or, lors de cette itération, on va prendre l'élément `tab[i]` et aller l'insérer à sa place en décalant progressivement les éléments plus grands (on devrait en toute rigueur le prouver avec un autre invariant de boucle dans la boucle `while`)

Ainsi, en fin d'itération, `tab([0, i]) est trié` et `P(i + 1)` sera alors vraie.

__Conclusion__ : la dernière itération s'effectue pour `i = len(tab) - 1`.

Ainsi `P(len(tab) - 1)` est vraie en début d'itération et `P(len(tab))` sera vraie en fin d'itération.

Donc `tab([0, len(tab)[) est trié"` ce qui signifie que tout le tableau est trié.
