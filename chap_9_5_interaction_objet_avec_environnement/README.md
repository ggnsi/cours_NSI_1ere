# Interaction d'un objet numérique avec son environnement

* [Prise en main de la carte microbit](./9_5_carte_microbit_prise_en_main.md)
* [Défis suivants](./9_5_carte_microbit_defis.md)
