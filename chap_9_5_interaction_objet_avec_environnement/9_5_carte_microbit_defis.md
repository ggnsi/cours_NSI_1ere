# Carte Micro:bit - autres défis

### Défi 1

Vous documenter sur l'utilisation de l'accéléromètre intégré à la carte.

Réaliser un projet faisant en sorte que lorsque :
* au départ la led du milieu est allumée ;
* lorsque l'on incline la carte vers la gauche, la led allumée se déplace vers la gauche (et reste bloquée si besoin sur la colonne de gauche) ;
* lorsque l'on incline la carte vers la droite, la led allumée se déplace vers la droite (et reste bloquée si besoin sur la colonne de droite) ;

_Remarques_ :
* on pourra intégrer une pause de 100ms à chaque tour de boucle pour éviter des déplacements trop rapides
* penser à considérer une position "milieu" où l'on n'est ni à gauche ni à droite.


### Défi 2

* Faire s'allumer au hasard sur la carte une led ;
* Faire se déplacer en fonction de l'inclinaison de la carte une led initialement placée au centre de la matrice de led ;
* lorsque l'on arrive à se positionner sur la led, le tour de l'écran s'allume en clignotant 5 fois et le jeu s'arrête.
* Evolution : faire que la led à atteindre clignote

_Remarque_ : On peut créer et afficher sa propre image de la manière suivante :

```Python
boat = Image("05050:05050:05050:99999:09990")
mb.display(boat)
```

### Défi 3 : La pluie

Programmer une averse que l'on considère de la manière suivante :

A chaque étape :

* les leds de gouttes de pluie déjà présentes sur le plateau descendent d'un cran ;
* au maximum 3 nouvelles gouttes de pluie apparaissent sur la première rangée ;

On fournit ci-dessous une fonction permettant de transformer une liste de liste représentant une matrice de taille 5x5 en une image microbit pouvant ensuite être affichée :

```Python
def matrice_to_image(plateau):
    
    liste = []
    for ligne in plateau:
        for elt in ligne:
            liste.append(str(elt))
        liste.append(':')
    image = ''.join(liste)
    return mb.Image(image)
```

### Défi 4

* Demander à l'enseignant une carte d'interface `Grove` et un potentiomètre.
* Brancher le potentiomètre sur l'entrée `P0/P12` de la carte d'interface

Eléments de documentation :

* [Sur Ostralo](http://numerique.ostralo.net/microbit/partie5_grove/5a_presentation.htm)
* [Sur le site de référence](https://microbit-micropython.readthedocs.io/fr/latest/pin.html)
* Le potentiomètre envoie en sortie (fil jaune) une tension comprise entre 0 et 5 volts.  
L'utilisation de `mb.pin0.read_analog()` permet (lorsque le potantiomètre est connecté en `P0`) de lire la valeur obtenue via le connecteur `P0` de la carte Microbit.  
**Attention** : la carte microbit _mappe_ la valeur de tension sur la plage `[0; 1000]` : c'est à dire que :
    * une tension de 0 V donne une valeur lue de 0 ;
    * une tension de 5 V donne une valeur lue de 1000 ;
    * une tensino intermédiaire donne une valeur lue par proportionnalité entre 0 et 1000 (1 Volt donnerait une valeur lue de `1/ 5 * 1000 = 200`


1. Programmer un script qui affiche dans la console la valeur lue par la carte microbit sur le port `pin0`.
2. Programmer un script qui fasse s'afficher la led centrale de la carte avec une intensité contrôlée par  le potentiomètre ;
3. Programmer un script qui fasse que les lignes de led s'allument progressivement en fonction de la position du potentiomètre  (potentimètre à 0 : rien d'allumé et potentiomètre au maximum : toutes les leds allumées);

### Défi 5

A vous de trouver des idées de défi.

D'autres modules existent : me demander.
