# Prise en main de la carte micro:bit 
---

![image](./img/microbit-front.png)
![image](./img/microbit-back.png)

---
## Mise en route
---

### Quelques liens

* Détails des fonctionnalités de la carte microbit (sur ostralo.net) : [numerique.ostralo.net/microbit/](http://numerique.ostralo.net/microbit/)

    Vous y trouverez des informations sur le langage de programmation et les instructions propres au module microbit ainsi que des exemples.


* [Documentation officielle du module microbit](https://microbit-micropython.readthedocs.io/fr/latest/) 


* Pour vous entrainer chez vous ou en cas de problème, un simulateur en ligne de la programmation de la carte microbit : [https://create.withcode.uk/](https://create.withcode.uk/)

### Le logiciel utilisé

Pour programmer et inter-agir avec la carte microbit, nous allons utiliser **Thonny** ou le logiciel **Mu**.

On connecte la carte à l'ordinateur via le cable USB -> micro-USB.

Pour vous familiariser avec les paramétrages et les interfaces [lire la partie correspondante ici](http://numerique.ostralo.net/microbit/partie2_mu/2a_presentation.htm)

### La première ligne de code indispensable

La carte **se programme en python**. Vous pouvez donc utiliser toutes les notions connues (variables, boucles, structures conditionnelles, fonctions, listes, etc.), utiliser les modules (random, ...).

Cependant, **il faut aussi importer le module microbit pour les fonctionnalités spécifiques.**

**Première ligne de code indispensable** :

```Python
import microbit as mb
```
---
## Jouer avec l'affichage
---

### Afficher un texte 

> L'affichage du texte se fait à l'aide de la fonction microbit display.show().
>
>`mb.display.show('texte')`

**Travail à faire :**

Afficher le texte 'bonjour' sur la carte.

### Afficher un nombre 

>**L'affichage d'un nombre** se fait à l'aide de la fonction microbit display.show().
>
>`mb.display.show(nombre)`

**Travail à faire :**

Afficher un nombre sur la carte.


### Afficher une image préprogrammée


>Le module microbit propose une vingtaine d'images prédéfinies.
>
> Quelques exemples :
>
> `Image.HEART` : ![Image.HEART](./img/image_HEART.png)
>
> `Image.ARROW_E` : ![Image.Image_ARROW_E](./img/image_ARROW_E.png)
>
> `Image.CLOCK6` : ![Image.Image_CLOCK6](./img/image_CLOCK6.png)
>
> `Image.YES` : ![Image.Image_YES](./img/image_YES.png)


> **L'affichage d'une image prédéfinie** se fait à l'aide de la fonction `display.show()`.
>
> `mb.display.show(mb.Image.nom_image)`
>
> [Liste des images intégrées](https://microbit-micropython.readthedocs.io/fr/latest/tutorials/images.html)

**Travail à faire :**

Afficher une image prédéfinie sur la carte.

### Choisir les leds que l'on allume

> **Éteindre toutes les leds**
>
> `mb.display.clear()`

> **Allumer une led**
>
> Le controle de l'allumage de l'une des leds se fait à l'aide de la fonction `display.set_pixel(x, y, val)` où `x` est la colonne (de 0 à 4), `y` la ligne (de 0 à 4) et `va`l l'intensité (de 0 à 9).
>
>`mb.display.set_pixel(x, y, val)`

**Travail à faire :**

Faire s'allumer les leds de la diagonale principale (qui va de en haut à gauche à en bas à droite) avec une intensité croissante.

Cela peut se faire "à la main" mais utiliser une boucle est aussi plus joli.

---
## Jouer avec le temps
---

###  Faire faire une pause au programme

>Pour faire faire une pause au programme, il faut utiliser la fonction sleep en indiquant la durée de la pause souhaitée en millisecondes.
>
>`mb.sleep(nb_millisecondes)`

**Travail à faire :**

1. Faire s'allumer **progressivement** la led de la première colonne, deuxième ligne ;
2. Faire **s'allumer puis s'éteindre** progressivement la led de la première colonne, deuxième ligne

### Répéter à l'infini

> **Boucle infinie**
>
```Python
while True :
    # block d'instructions que l'on souhaite répéter
```

**Travail à faire :**

1. Afficher alternativement deux images à intervalle de temps régulier.
2. Faire clignoter 1 pixel (à choisir).
3. Afficher un nombre aléatoire entre 1 et 6 puis l'éteindre, au rythme de la seconde.
4. Allumer puis éteindre une led choisie aléatoirement par le programme chaque seconde.
5. Programmer un chenillard : le résultat doit donner l'impression qu'une led se déplace horizontalement de la gauche vers la droite (sur la troisième ligne par exemple), et recommence ainsi (en repartant de la gauche une fois arrivée à droite) ...
    * proposer une première solution ;
    * proposer une autre solution qui utilise une voucle `while True` à l'intérieur de laquelle on modifiera l'indice de colonne à l'aide d'une instruction du type (à compléter) : `i_col = (i_col + 1) % ....`
6. Si vous êtes en avance : Programmer un chenillard comme celui présenté [au début de cette vidéo](https://www.youtube.com/watch?v=iK1y_rr9pYs)

---
## Jouer avec les boutons
---

La carte microbit dispose de deux boutons notés A et B. Voici comment les programmer.

### Tester si l'un des boutons est appuyé : méthode `is_pressed()`

>Pour tester si l'un des boutons est appuyé, il faut utiliser la méthode `is_pressed()` du bouton. Cette méthode renvoie `True` ou `False`, on peut donc l'utiliser dans une instruction conditionnelle.

_Exemple avec le bouton A :_

```Python
while True:
    if mb.button_a.is_pressed():
        # block des instructions qui s'exécuteront si le bouton est appuyé
    else:
        # block des instructions qui s'exécuteront si le bouton n'est pas appuyé 
```

**Travail à faire :**

1. Faire en sorte que l'image HAPPY soit affichée quand le bouton est appuyé et que l'image SAD le soit quand le bouton n'est pas appuyé.
2. Faire en sorte que lorsque le bouton A est appuyé la colonne de leds de gauche s'allume, lorsque le bouton B est appuyé la colonne de leds de droite s'allume et lorsque qu'aucun bouton n'est appuyé rien ne s'affiche (on supposera ici qu'on n'appuie pas sur les deux boutons en même temps).
3. Reprendre le travail de la question précédente en faisant en sorte que si les deux boutons sont pressés alors les colonnes de leds de gauche et de droite s'allument.

### Tester si l'un des boutons a été appuyé : méthode `was_pressed()`

>La méthode `was_pressed()` permet de tester si le bouton a été appuyé depuis le dernier appel à cette fonction. Cette méthode renvoie `True` ou `False`, on peut donc l'utiliser dans une instruction conditionnelle.

_Exemple avec le bouton A :_

```Python
if mb.button_a.was_pressed():
    # block des instructions qui s'exécuteront si le bouton a été appuyé
else:
    # block des instructions qui s'exécuteront si le bouton n'a pas été appuyé
```

**Travail à faire :**

1. Faire un compteur de clic, autrement dit, faire en sorte que la carte augmente de 1 le nombre affiché lorsqu'on clique sur le bouton A et le diminue de 1 lorsqu'on clique sur le bouton B.
2. Faire en sorte que chaque fois que l'on appuie sur le bouton A, l'image affichée passe de HAPPY à SAD puis à de nouveau à HAPPY et ainsi de suite.
3. Reprendre le code du 1.  
    Que se passe-t-il lorsqu'on appuie rapidement plusieurs fois sur l'un des boutons ?  
    Documentez-vous sur la méthode `get_presses()` et modifier votre code pour supprimer le phénomène.




