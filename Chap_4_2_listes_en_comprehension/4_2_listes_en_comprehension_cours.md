# Les listes en compréhension

### Idée de départ

>L'idée est simple: simplifier le code pour le rendre plus lisible et donc plus rapide à écrire et plus simple à maintenir.
En un mot : avoir la possibilité de créer des listes de manière à la fois concise et élégante.

>Une application classique est la construction de nouvelles listes où chaque élément est le résultat d'une opération appliquée à chaque élément d'une autre séquence ; ou de créer une sous-séquence des éléments satisfaisant une condition spécifique.


### _Un exemple pour comprendre_ :

Supposons que l'on souhaite créer une liste `l` contenant les carrés des enties allant de $0$ à $9$ (inclus).

* _avec une boucle **for**_ :



```python
l = []
for k in range(10):
    l.append(k**2)
print(l)
```

    [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
    

* avec une **liste définie en compréhension** :


```python
l = [k**2 for k in range(10)]
print(l)
```

    [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
    

**C'est plus léger, plus lisible, ...., plus beau :-)**

--- 
### Syntaxe (sans test)


> new_list = [function(elt) for elt in iterable]
>
> L'itérable peut être une liste, une chaîne de caractères, l'itéragle obtenue par la fonction `range`, ...

>L'écriture `liste = [function(elt) for elt in iterable]`
>
>**est équivalente à**
>
>```Python
>liste = []
>for elt in interable:
>    liste.append(function(elt))
>```

---

_Exemple_ : 

Si `l_1 = [4, 7, -2, 3]`,

**En compréhension** `l_2 = [elt ** 2 for elt in l_1]` 

**est équivalent à la structure for ci-dessous**

```Python
l_2 = []
for elt in l_1:
    l_2.append(elt ** 2)
```

Ici, l'itérable utilisé est une liste et `l_2` contient les carrés des éléments de `l_1`.

---

### Possiblité d'avoir un filtrage par un test

>Il est possible de n'inclure `function(item)` dans la liste que si le résultat d'un test est vrai.
>
>La syntaxe devient alors :
>
> `new_list = [function(elt) for elt in iterable if condition(elt)]`
>
> On n'ajoute `function(elt)` à `new_list` uniquement si `condition(elt)` est vrai.

Cette structure est équivalente à 

```Python
new_list = []
for elt in l_1:
    if condition(elt):
        l_2.append(elt ** 2)
```

>`[x for x in iterable if condition(x)]` peut se lire «la liste des x quand x parcourt l'itérable `iterable`, tels que >`condition(x)` est vrai».

>`[f(x) for x in iterable if test(x)]` peut se lire «la liste des $f(x)$ quand x parcourt l'itérable `iterable`, tels que >`condition(x)` est vrai».


_Exemple_ : 

`l_1 = [4, 7, -2, 3, 9]`

On veut créer une liste `l_2` contenant tous les éléments de `l_1` supérieurs ou égaux à 5.

`l_2 = [elt for elt in l_1 if elt >= 5]`

C'est l'équivalent du code :

```Python
l_2 = []
for elt in l_1:
    if elt >= 5:
        l_2.append(elt)
```

_Exemple_ : 

Par exemple `l_1 = [32, 5, 12, 8, 3, 75, 2, 15]`

Pour obtenir la liste des éléments de `l_1` qui sont pairs : `l_pair = [x for x in l_1 if (x % 2 == 0)]`.  
Pour obtenir la liste des éléments de `l_1` qui sont impairs : `l_impair = [x for x in l_1 if (x % 2 == 1)]`.

### Une application classique : exécuter une fonction sur chaque item d'une liste.

_exemple_ :

Prenons l'exemple d'une conversion de string en integer de plusieurs items:


```python
items = ["5", "10", "15"]
items = [int(x) for x in items]
print(items)
```

    [5, 10, 15]
    

On peut, dans la même idée, appliquer une fonction pré-définie avant à tous les éléments d'une liste :


```python
def modif(x):
    if x % 2 == 0: # si x est pair on le multiplie par 2
        return 2 * x
    else:            # sinon, on le multiplie par 5
        return 5 * x
    
liste_1 = [4, 7, 3, 2, 1]
liste_2 = [modif(x) for x in liste_1]
print(liste_2)
```

    [8, 35, 15, 4, 5]
    

_Remarque_ :

On peut aussi obtenir le résulat précédent de la manière suivante :


```python
liste_1 = [4, 7, 3, 2, 1]
liste_2 = [(2 * x if (x % 2 == 0) else 5 * x) for x in liste_1]
print(liste_2)
```

    [8, 35, 15, 4, 5]
    

Les listes en compréhension permettent de condenser le code.  
Il faut cependant **garder à l'esprit la bonne pratique qui consiste à avoir un code lisible et facilement compréhensible par quelqu'un d'autre.**  
**Le choix de la liste en  compréhension n'est donc pas toujours le bon**.

_autre exemple_ :


```python
liste =  [(x**2 if (x % 2 == 0) else 5) for x in range(5)]
print(liste)
```

    [0, 5, 4, 5, 16]
    
