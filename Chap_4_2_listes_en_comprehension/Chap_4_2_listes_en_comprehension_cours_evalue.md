# Les listes en compréhension

### Idée de départ

>L'idée est simple: simplifier le code pour le rendre plus lisible et donc plus rapide à écrire et plus simple à maintenir.
En un mot : avoir la possibilité de créer des listes de manière à la fois concise et élégante.

>Une application classique est la construction de nouvelles listes où chaque élément est le résultat d'une opération appliquée à chaque élément d'une autre séquence ; ou de créer une sous-séquence des éléments satisfaisant une condition spécifique.


### _Un exemple pour comprendre_ :

Supposons que l'on souhaite créer une liste `l` contenant les carrés des enties allant de $`0`$ à $`9`$ (inclus).

*  _avec une boucle __for__ _ :

    Une variable `nombre` va parcourir les entiers de $`0`$ à $`9`$ (inclus) et pour chacun, on va ajouter  son carré dans une liste.


```python
l=[]
for k in range(10):
    l.append(k**2)
print(l)
```

    [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
    

Obtenir la même chose avec une liste en compréhension s'écrit :


```python
l = [k**2 for k in range(10)]
print(l)
```

    [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
    

C'est plus léger, plus lisible, ...., plus beau :-)

### Syntaxe (sans test)


> new_list = [function(item) for item in iterable]


L'itérable peut lui même être une liste.

_Exemple_ :

Supposons que l'on veuille mettre au carré tous les éléments d'une liste.

_avec une boucle __for__ _ :


```python
l_1 = [4, 7, -2, 3]
l_2 = []
for k in range(len(l_1)):
    l_2.append(l_1[k]**2)
print(l_2)
```

    [16, 49, 4, 9]
    

_en utilisant la création d'une liste en compréhension_ :


```python
l_2 = [k**2 for k in l_1]
print(l_2)
```

    [16, 49, 4, 9]
    

### Possiblité d'avoir un filtrage par un test

Il est possible de n'inclure `function(item)` dans la liste que si le résultat d'un test est vrai.

La syntaxe devient alors :

> new_list = [function(item) for item in iterable if condition(item)]

_Exemple_ : 

Reprenons un des exercices du chapitre précédent :

On dispose d'une liste de nombre entiers positifs.

Par exemple `L1 = [32, 5, 12, 8, 3, 75, 2, 15]`

Écrire une fonction `separe(liste)` qui analyse un par un tous les éléments de la liste pour générer puis afficher deux nouvelles listes.  
L’une contiendra seulement les nombres pairs de la liste initiale, et l’autre les nombres impairs.

Par exemple, si la liste initiale est L1, le programme devra construire puis afficher les listes `[32, 12, 8, 2]` d'une part et `[5, 3, 75, 15]` d'autre part.

Une manière en compréhension de définir la première liste est :


```python
L1 = [32, 5, 12, 8, 3, 75, 2, 15]
l_pair = [x for x in L1 if (x % 2 == 0)]
print(l_pair)
```

    [32, 12, 8, 2]
    

pour la 2ème liste demandée :


```python
l_impair = [x for x in L1 if (x % 2 == 1)]
print(l_impair)
```

    [5, 3, 75, 15]
    

`[x for x in iterable if condition(x)]` peut se lire «la liste des x quand x parcourt l'itérable `iterable`, tels que `condition(x)` est vrai».

`[f(x) for x in iterable if test(x)]` peut se lire «la liste des $f(x)$ quand x parcourt l'itérable `iterable`, tels que `condition(x)` est vrai».

### Une application classique : exécuter une fonction sur chaque item d'une liste.

_exemple_ :

Prenons l'exemple d'une conversion de string en integer de plusieurs items:


```python
items = ["5", "10", "15"]
items = [int(x) for x in items]
print(items)
```

    [5, 10, 15]
    

On peut, dans la même idée, appliquer une fonction pré-définie avant à tous les éléments d'une liste :


```python
def modif(x):
    if x % 2 == 0: # si x est pair
        return 2 * x
    else:
        return 5 * x
    
liste_1 = [4, 7, 3, 2, 1]
liste_2 = [modif(x) for x in liste_1]
print(liste_2)
```

    [8, 35, 15, 4, 5]
    

_Remarque_ :

On peut aussi obtenir le résulat précédent de la manière suivante :


```python
liste_1 = [4, 7, 3, 2, 1]
liste_2 = [(2 * x if (x % 2 == 0) else 5 * x) for x in liste_1]
print(liste_2)
```

    [8, 35, 15, 4, 5]
    

Si les listes en compréhension permettent de condenser le code, il faut cependant garder à l'esprit la bonne pratique qui consiste à avoir un code lisible et facilement compréhensible par quelqu'un d'autre.  
Le choix de la liste en  compréhension n'est donc pas toujours le bon.

_autre exemple_ :


```python
liste =  [(x**2 if (x % 2 == 0) else 5) for x in range(5)]
print(liste)
```

    [0, 5, 4, 5, 16]
    
