# TD : Listes en compréhension - corrigé exercices 3 à 5


### Exercice 3 [^1]

Quel est le contenu du tableau référencé par la variable mon_tab après l'exécution du programme ci-dessous ? (__après avoir répondu__, utilisez la console pour vérifier.)

```Python
>>> liste = [1, 7, 9, 15, 5, 20, 10, 8]
>>> mon_tab = [p for p in liste if p > 10]
```

`mon_tab = [15, 20]`

### Exercice 4 [^1]

Quel est le contenu du tableau référencé par la variable mon_tab après l'exécution du programme ci-dessous ? 

```Python
>>> liste = [1, 7, 9, 15, 5, 20, 10, 8]
>>> mon_tab = [p**2 for p in liste if p < 10]
```

>`mon_tab = [1, 49, 81, 25, 64]`

### Exercice 5 : epreuves communes

__Question 1__ :
On exécute le code suivant :

```Python
t = [1,2,3,4,5,6,7,8,9]
v = [c for c in t if c%3 == 0]
```

> On ne conserve que les éléments de `t` qui sont des multiples de 3.

Quelle est la valeur de la variable v à la fin de cette exécution ?

>Réponses : C- `[3,6,9]`


__Question 2__ :
Laquelle des expressions suivantes a-t-elle pour valeur la liste des carrés des premiers entiers qui ne sont pas multiples de 5 ?

>Réponses : D- `[x*x for x in range (11) if x%5 != 0]`

__Question 3__ :

Quelle est l'expression qui a pour valeur la liste [1,4,9,16,25,36] ?

Réponses :

A- { n*n for n in range(1,7) } # n'est pas une liste

B- { n*n for n in range(6) } # n'est pas une liste

> C- [ n*n for n in range(1,7) ]

D- [ n*n for n in range(6) ] # c'est la liste `[0, 1, 4, 9, 16, 25]`


__Question 4__ :

On dispose d'une liste L :


`L = [6, 2, 8, 24, 3, 6, 7, 8]`
		

Quelle est la valeur de M après exécution du code suivant ?

```Python
p = 8
M = [x for x in L if x<p] + [x for x in L if x==p] + [x for x in L if x>p]
```

> On concatène les listes suivantes :
> * celle qui comporte les éléments de `L` strictement inférieurs à 8 ;
> * celle qui comporte les éléments de `L` égaux à 8 ;
> * celle qui comporte les éléments de `L` strictement supérieurs à 8 ;

> Réponses : B- `[6,2,3,6,7,8,8,24]`

__Question 5__ :

Parmi les propositions suivantes, laquelle permet de créer en Python la liste des nombres impairs de 1 à 399 (inclus) ?

>Réponses : A- `impairs = [1 + nb*2 for nb in range(200)]`
		

B-
```Python
for nb in range(400) :
		impairs = 1 + 2 * nb
```
> On obtient `[1, 3, 5, ..., 799]` car `799 = 1 + 2 * 399`
		

C- `impairs = [i + 2 for i in range(1,200)]`

> On obtient `[3, 5, 7, 9, 201]`
		

D- `impairs = [1, 3, 5, 7, 9] * 40`

> On obtient `[1, 3, 5, 7, 9, 1, 3, 5, 7, 9, ...., 1, 3, 5, 7, 9]` (40 concaténations)
		

__Question 6__ :


On considère le code suivant :

```Python
def f(L):
    return [x*x for x in L if x%2 == 1]

carre = f([0,1,2,3,4,5,6,7,8,9])
```

_Rappel_ : L'opérateur % calcule le reste de la division euclidienne de l'opérande de gauche par l'opérande de droite. Par exemple : 7 % 3 vaut 1, 15 % 5 vaut 0 et 18 % 4 vaut 2. 

Que vaut carre à la fin de son exécution ?

> On obtient les carrés des éléments de `L` qui sont impairs

Réponses : C- `[1, 9, 25, 49, 81]`



[^1] https://pixees.fr/informatiquelycee/n_site/nsi_prem_pythonSequence.html
