# TD : Listes en compréhension - corrigé

### Exercice 1

__Question 1 :__  
Que va retourner l'expression suivante: [2*i+1 for i in range(5)]

* `[1, 3, 5, 7, 9]` 

`i` va décrire les entiers compris entre 0 et 4 inclus et ajoute à chaque fois à la liste l'élément `2*i+1` donc : `2*0+1`, `2*1+1`, ...

__Question 2 :__

Quelle instruction permet d’affecter la liste `[0,1,4,9,16]` à la variable tableau ? 

 Réponses :

B- `tableau = [ i**2 for i in range(5) ]`

On veut la liste des carrés des entiers compris entre 0 et 4.


