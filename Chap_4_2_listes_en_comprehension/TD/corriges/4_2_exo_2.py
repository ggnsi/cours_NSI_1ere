# 1

l_1 = [i for i in range(10)]

# 2

l_2 = [elt * 3 for elt in l_1]

#3
l_3 = [elt for elt in l_1 if elt >= 5]

# 3bis
l_3bis = [2 * elt for elt in l_1 if ((elt ** 2 >= 20) and (elt **2 <= 70))]

# 4

liste = [3 * k for k in range(1,7)]

#5
l_depart = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
l_5 = [elt for elt in l_depart if elt % 3 == 0]

#6
liste_1 = [i for i in range(5, 16)]
# ou liste_1 = list(range(5, 16))

def transform(nbr):
    if nbr % 2 == 0:
        return 3 * nbr
    else:
        return 5 * nbr

liste_2 = [transform(elt) for elt in liste_1]

#7
l_jours = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]

l_jours_maj = [jour.upper() for jour in l_jours]






