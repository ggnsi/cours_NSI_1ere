# Corrigés TD listes en compréhension

* [Exercice 1](./4_2_exo_1.py)
* [Exercice 2](./4_2_exo_2.py)
* [Exercice 3, 4 et 5](./chap_4_2_TD_exo_3_4_5_corr_listes_en_comprehension.md)
