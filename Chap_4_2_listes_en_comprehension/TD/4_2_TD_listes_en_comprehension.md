# TD : Listes en compréhension

### Exercice 1

__Question 1 :__  

Que va retourner l'expression suivante: `[2 * i + 1 for i in range(5)]`

*  `[5, 5, 5, 5, 5]`  
*  `[1, 3, 5, 7, 9]`  
*  `[2*i+1, 2*i+1, 2*i+1, 2*i+1]`
*  `[1, 3, 5, 7]`

__Question 2 :__

Quelle instruction permet d’affecter la liste `[0,1,4,9,16]` à la variable tableau ? 

 Réponses :

A. `tableau = [i**2 for i in range(4)]`

B. `tableau = [i**2 for i in range(5)]`

C. `tableau = [i**2 for i in range(16)]`

D. `tableau = [i**2 for i in range(17)]`



### Exercice 2

1) Créer une liste `l_1` par compréhension qui contient  les 10 premiers entiers en commençant par 0 (donc ceux compris entre 0 et 9 inclus), soit `l_1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]`

2) Définissez, à partir de `l_1` et par compréhension, une liste `l_2` contenant le triple de tous les éléments de `l_1` : `l_2 = [0, 3, 6, 9, 12, 15, 18, 21, 24, 27]` 


3) 
* Définissez, à partir de `l_1` et par compréhension, une liste `l_3` contenant uniquement les éléments de `l_1` supérieurs ou égaux à 5 : `l_3 = [5, 6, 7, 8, 9]`

* Définissez, à partir de `l_1` et par compréhension, une liste `l_3bis` contenant le double des éléments de `l_1` dont le carré est compris entre 20 et 70 : `l_3bis = [10, 12, 14, 16]`

4) Définissez en compréhension la la liste suivante ? : `liste = [3, 6, 9, 12, 15, 18]`

5) Soit `l_depart = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]`  
Comment obtenir en compréhension la liste de la question 4 à partir de `l_depart`.

6)
 * Créez la `liste_1` contenant les entiers compris entre 5 et 15 (inclus),
 * définissez une fonction `transform(nbr)` qui retourne `3 * nbr` si `nbr` est pair et qui retourne `5 * nbr` si `nbr` est impair
 * appliquez la fonction `transform()` à chaque élément de `liste_1` à l'aide d'une liste en compréhension. Le résultat sera affecté à une liste `liste_2`.
 
7) On considère la liste `l_jours = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]`.

Définissez par compréhension la liste `l_jours_maj` composée de tous les jours de la semaine écrits en majuscule.

### Exercice 3 [^1]

Quel est le contenu du tableau référencé par la variable mon_tab après l'exécution du programme ci-dessous ? (__après avoir répondu__, utilisez la console pour vérifier.)

```Python
>>> liste = [1, 7, 9, 15, 5, 20, 10, 8]
>>> mon_tab = [p for p in liste if p > 10]
```

### Exercice 4 [^1]

Quel est le contenu du tableau référencé par la variable mon_tab après l'exécution du programme ci-dessous ? 

```Python
>>> liste = [1, 7, 9, 15, 5, 20, 10, 8]
>>> mon_tab = [p**2 for p in liste if p < 10]
```

### Exercice 5 : epreuves communes

__Question 1__ :
On exécute le code suivant :

```Python
t = [1,2,3,4,5,6,7,8,9]
v = [c for c in t if c%3 == 0]
```


Quelle est la valeur de la variable v à la fin de cette exécution ?

Réponses :

A- 18

B- `[1,4,7]`

C- `[3,6,9]`

D- `[1,2,3,4,5,6,7,8,9]` 


__Question 2__ :

Laquelle des expressions suivantes a-t-elle pour valeur la liste des carrés des premiers entiers qui ne sont pas multiples de 5 ?

Réponses :

A- `[x*x for x in range (11) if x//5 != 0]`

B- `[x*x if x%5 != 0 for x in range (11)]`

C- `[x*x if x//5 != 0 for x in range (11)]`

D- `[x*x for x in range (11) if x%5 != 0]`

__Question 3__ :

Quelle est l'expression qui a pour valeur la liste `[1,4,9,16,25,36]` ?

Réponses :

A- `{ n*n for n in range(1,7) }`

B- `{ n*n for n in range(6) }`

C- `[ n*n for n in range(1,7) ]`

D- `[ n*n for n in range(6) ]`


__Question 4__ :

On dispose d'une liste L :


`L = [6, 2, 8, 24, 3, 6, 7, 8]`
		

Quelle est la valeur de M après exécution du code suivant ?

```Python
p = 8
M = [x for x in L if x<p] + [x for x in L if x==p] + [x for x in L if x>p]
```
	

Réponses :

A- `[2,3,6,6,7,8,8,24]`

B- `[6,2,3,6,7,8,8,24]`

C- `[6,2,8,24,3,6,7,8]`

D- `[[6,2,3,6,7],[8,8],[24]]`

__Question 5__ :

Parmi les propositions suivantes, laquelle permet de créer en Python la liste des nombres impairs de 1 à 399 (inclus) ?

Réponses :

A- `impairs = [1 + nb*2 for nb in range(200)]`
		

B-
```Python
for nb in range(400) :
		impairs = 1 + 2 * nb
```

		

C- `impairs = [i + 2 for i in range(1,200)]`
		

D- `impairs = [1, 3, 5, 7, 9] * 40`
		

__Question 6__ :


On considère le code suivant :

```Python
def f(L):
    return [x*x for x in L if x%2 == 1]

carre = f([0,1,2,3,4,5,6,7,8,9])
```

_Rappel_ : L'opérateur `%` calcule le reste de la division euclidienne de l'opérande de gauche par l'opérande de droite. Par exemple : `7 % 3` vaut `1`, `15 % 5` vaut `0` et `18 % 4` vaut `2`. 

Que vaut carre à la fin de son exécution ?

Réponses :

A- `[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]`

B- `[0, 4, 16, 36, 64]`

C- `[1, 9, 25, 49, 81]` 

D- `[0, 2, 4, 6, 8, 10, 12, 14, 16, 19]` 




Références utilisées :

* [^1] https://pixees.fr/informatiquelycee/n_site/nsi_prem_pythonSequence.html
