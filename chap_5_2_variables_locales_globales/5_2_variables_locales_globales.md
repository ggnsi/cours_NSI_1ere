# Variables locales, globales, portée des variables

## Qu'est-ce qu'une variable ?

Utilisez Thonny en ayant activé (menu Affichage), l'affichage des _Allocations mémoires_.

Dans la console, exécutons :

```Python
>>> a = 5
```

Lors de la définition d'une variable, Python crée un __espace de noms__, c'est à dire un tableau associant au _nom de la variable_ la _zone mémoire_ (repérée par son adresse (ou __ID__) ici en hexadécimal) où la valeur affectée est stockée.

![fig_1](./img/chap_5_2_fig_1.jpg)

Entrer successivement les instructions suivantes et observer les adresses mémoires associées à `b`, à `c`, à `a` :

```Python
>>> b = a
>>> c = 5
>>> a = 70
```
> Une table des noms est créée. Elle fait le lien entre le nom de la variable et l'adresse mémoire correspondant à sa valeur.
> On observe ici que Python ne crée qu'une seule fois l'entier 5 en mémoire et associe cette adresse aux différentes variables contenant 5.
> Réaliser une nouvelle affectation change si nécessaire l'adresse mémoire associée à la variable.

## Portée des variables

> _Définition_
>
> On appelle **portée** (en anglais **scope** ) d'une variable les zones (espaces) dans lesquelles cette variable est connue et accessible.

_Exemple 1_ :

Copier dans un script puis exécutez le code ci-dessous.


```python
def test():
    a = 7
    
test()
print(a)
```

Python génère une erreur nous indiquant que le nom 'a' n'est pas défini (`name 'a' is not defined`).

**visualiser l'exécution pas à pas dans PythonTutot** : [Exécution dans PythonTutor](http://pythontutor.com/visualize.html#code=def%20test%28%29%3A%0A%20%20%20%20a%20%3D%207%0A%20%20%20%20%0Atest%28%29%0Aprint%28a%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) (on utilisera la flèche next pour exécuter une à une les instructions)

> A droite, vous pouvez observer les espaces des noms ainsi que les références pour chaque variable.

Détail de ce qui se passe : 

>* Lors du lancement du script, Pytho crée un _espace de nom **global** (Global Frame);
>* le nom **test** est crée et associé à la fonction **test()** mise en mémoire.
>* Lors du lancement de la fonction, un **nouvel espace de nom propre à la fonction** est crée. On dit que c'est **l'espace de nom local à la fonction**  ;
>* Lors de l'affectation `a = 7` : une **variable locale** `a` est crée **dans l'espace des noms local de la fonction test**.
>* En sortie de fonction, cet **espace de nom local est détruit**.
>* Lors de `print(a)`, Python examine si une variable `a` existe dans l'espace de nom courant (l'espace global ici), ce qui n'est pas le cas. Il provoque une erreur.

On dit que la variable `a` est une **variable locale** à la fonction. Elle n'est pas accessible à l'extérieur de la fonction.

_Exemple 1bis_ :

Copier dans un script puis exécutez le code ci-dessous.


```python
def test():
    a = 7
    print(a)
     
test()
```

**Visualiser l'[exécution dans PythonTutor](https://pythontutor.com/visualize.html#code=def%20test%28%29%3A%0A%20%20%20%20a%20%3D%207%0A%20%20%20%20print%28a%29%0A%20%20%20%20%20%0Atest%28%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

> Ici pas d'erreur générée : le `print(a)` est effectué à l'intérieur de la fonction `test()` où la variable **locale** `a` est définie dans **l'espace des noms local** de la fonction.

_Exemple 2_ :

Copier dans un script puis exécutez le code ci-dessous.


```python
def fonction1():
    a = 7
    
def fonction2():
    a = 9
    fonction1()
    print(a)

fonction2()
```

Visualisation de l'exécution avec PythonTutor : [lien direct exemple 2](https://pythontutor.com/visualize.html#code=def%20fonction1%28%29%3A%0A%20%20%20%20a%20%3D%207%0A%20%20%20%20%0Adef%20fonction2%28%29%3A%0A%20%20%20%20a%20%3D%209%0A%20%20%20%20fonction1%28%29%0A%20%20%20%20print%28a%29%0A%0Afonction2%28%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

* Les fonctions `fonction1()` et `fonction2()` sont mises en mémoire et associées aux noms `fonction1` et `fonction2` définis **dans l'espace des noms global**.
* lors de l'appel de `fonction2()` :
     * `a = 9` crée une variable **locale* `a` **dans l'espace des noms local à fonction2**
     * on appelle `fonction1()` :
         * `a = 7` crée une variable **locale* `a` **dans l'espace des noms local à fonction1**.  
         Il n'y a donc pas de modification de la variable `a` existant dans l'espace des noms de `fonction2`.
         * En sortie de `fonction1()`, l'espace des noms local est détruit et l'on retourne dans `fonction2()`
    * lors de `print(a)`, on utilise donc la variable `a` locale à `fonction2()` et c'est 9 qui est affiché.
    * En sortie de `fonction2()`, l'espace des noms local est détruit

Il n'y a pas de téléscopage des variables `a` utilisées dans chacune des fonctions : chacune n'existant que dans son espace local et étant supprimées en sortie de fonction.

_Exemple 3_ :


```python
def imc(taille, poids):
    imc = poids / taille ** 2 # variable imc pas indispensable
    return imc

imc(1.85, 70)
```

Visualisation de l'[exécution dans PythonTutor](https://pythontutor.com/visualize.html#code=def%20imc%28taille,%20poids%29%3A%0A%20%20%20%20imc%20%3D%20poids%20/%20taille%20**%202%20%23%20variable%20imc%20pas%20indispensable%0A%20%20%20%20return%20imc%0A%0Aimc%281.85,%2070%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

La fonction possède deux paramètres : `taille` et `poids`.  
Lors de l'appel via `imc(1.85, 70)`, deux variables **locales** `taille` et `poids` sont crées et les valeurs `1.85` et `70` passées en argument leur sont affectées.

> Si l'on passe en paramètres des arguments à une fonction, lors de l'appel de la fonction, Python crée automatiquement des variables _locales_ avec les noms et leur affecte les valeurs passées en paramètre.

> Il est ainsi possible d'utiliser les mêmes noms pour des variables ou des paramètres dans différentes fonctions (heureusement !) puisqu'à chaque fois une variable locale est créée.

L'exemple 4 illustre cela :

_Exemple 4_ :


```python
def conversion(taille):
    """convertie taille de cm en m ...(docstring non complète)   
    """
    
    taille = taille / 100
    return taille

def imc(taille, poids):
    """calcule l'imc à partir d'une taille en cm et d'un poids en kg ...(docstring non complète) 
    """
    
    taille = conversion(taille)
    imc = poids / taille ** 2
    return imc

imc(185, 70)
```

Visualisation de l'[exécution dans PythonTutor](https://pythontutor.com/visualize.html#code=def%20conversion%28taille%29%3A%0A%20%20%20%20%22%22%22convertie%20taille%20de%20cm%20en%20m%20...%28docstring%20non%20compl%C3%A8te%29%20%20%20%0A%20%20%20%20%22%22%22%0A%20%20%20%20%0A%20%20%20%20taille%20%3D%20taille%20/%20100%0A%20%20%20%20return%20taille%0A%0Adef%20imc%28taille,%20poids%29%3A%0A%20%20%20%20%22%22%22calcule%20l'imc%20%C3%A0%20partir%20d'une%20taille%20en%20cm%20et%20d'un%20poids%20en%20kg%20...%28docstring%20non%20compl%C3%A8te%29%20%0A%20%20%20%20%22%22%22%0A%20%20%20%20%0A%20%20%20%20taille%20%3D%20conversion%28taille%29%0A%20%20%20%20imc%20%3D%20poids%20/%20taille%20**%202%0A%20%20%20%20return%20imc%0A%0Aimc%28185,%2070%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

> Cet exemple illustre le fait que Python commence par chercher la variable dans l'espace local de la fontion. Il n'y a ainsi pas de téléscopage lors de l'utilisation du même nom de variable dans des fonctions différentes, et même si comme ici l'une des fonction appelle l'autre.

__Petit bilan intermédiaire__ :

* Les variables définies dans des fonctions sont donc appelées __variables locales__ (car définies dans l'espace des noms local à la fonction) ;
* Si une variable est défini dans le script principal, elle est appelée __variable globales__ (car définie dans l'espace des noms global).

_Exemple 5_ :



```python
a = 7

def double(nombre):
    resultat = 2 * nombre
    
    return resultat

```

Dans le code précédent :

* la variable `a` est une __variable globale__ ;
* les variables `nombre` et `resultat` sont des variables __locales__ (définies dans l'espace des noms local de la fonction)


## Cas des données mutables passées en argument

Que se passe-t-il si l'on passe en argument une donnée mutable (pour l'instant pour nous : **une liste**, mais à l'avenir aussi des dictionnaires)

_Exemple 8_ :


```python
def ajoute_0(liste):
    liste.append(0)

l = [1, 2, 3]
ajoute_0(l)
print(l)
```

[Exécution dans Python Tutor](https://pythontutor.com/visualize.html#code=def%20ajoute_0%28liste%29%3A%0A%20%20%20%20liste.append%280%29%0A%0Al%20%3D%20%5B1,%202,%203%5D%0Aajoute_0%28l%29%0Aprint%28l%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

On constate que lors du passage de `l` en argument, une variable locale `liste` est créée mais qu'elle pointe en mémoire vers le même élément que celui contenu dans `l`.

**Toute modification effectuée sur `liste` est donc aussi effectuée sur `l` car les deux variables référencent le même élément en mémoire.**

**On dit que liste est modifiée en place**

> Ce fonctionnement est très pratique dans de nombreuses situations mais il faut bien en avoir conscience pour éviter  des erreurs.

## Compléments

__Comment faire pour pouvoir modifier à l'intérieur d'une fonction la valeur d'une variable globale ?__

>Pour modifier une variable __globale__ dans une fonction, il faut la déclarer comme globale en début de fonction avec l'instruction `global`.

_Exemple 6_ :


```python
a = 7

def test():
    global a   # on déclare ici la variable a comme globale
    a = a + 1
    return a
    
test()
```

__MAIS__ : on évite au maximum l'utilisation de `global` car cela entraine des _effets de bords_.

> Définition :
>
> On parle d'effet de bord quand une fonction modifie l'état de l'environnement extérieur à cette fonction.

Le problème est que cela modifie la valeur d'une variable sans qu'on le voit explicitement dans le corps principal du programme.

Cela rend le programme peu lisible et peu compréhensible.

Dans un programme complexe, cela peut provoquer des comportements non désirés et difficile à détecter.

__quelques subtilités__


```python
a = 7

def test():
    print(a)
    
test()
```

Que se passe-t-il si l'on exécute le code précédent ?


> On est surpris que l'appel de la fonction `test` provoque l'affichage du nombre 7 et non pas une erreur puisque la variable `a` n'existe pas dans l'espace local de la fonction.
> Voir l'explication ci-dessous :

Lors de l'exécution de la fonction :

* Python cherche si une variable `a` existe dans _l'espace local à la fonction_ (cad dans l'espace des noms local à la fonction) ;
* si ce n'est pas le cas, il cherche ensuite si la variable `a` existe dans _l'espace global_.

> Cela fonctionne mais c'est une chose à ne pas faire car dangereux, peu lisible et propice à des erreurs difficilement compréhensibles.

_Exemple 7_ :

Que provoquera le code suivant ?


```python
a = 7

def test():
    a = a + 1
    return a
    
test()
```

> Ici, nous avons une erreur du type `UnboundLocalError: local variable 'a' referenced before assignment`

L'interpréteur détecte une affectation de la variable `a` lors de sa lecture de la fonction. Il considère alors automatiquement cette variable comme locale à la fonction.  
Or, lors de l'évaluation du membre de droite, la variable `a`, considérée comme locale, n'est pas définie, d'où l'erreur.

