# TD sur la portée des variables corrigé

### Exercice 1

Déterminer dans chacun des cas suivant si une erreur se produit et, le cas échéant, les affichages obtenus

_code 1_ :


```python
def fonction(n):
    a = n + 1
fonction(3)
print(a)
```

Provoque une erreur :

* pas d'erreur lors de l'éxécution de la fonction : on affecte à la variable `a` la valeur `n + 1` ;
* par contre, une erreur lors de l'évaluation de `print(a)`.  
En effet, la variable `a` était définie localement lors de l'exécution de la fonction et a été supprimée. Il n'existe pas de variable `a` définie dans l'espace des noms global.

[lien python tutor](http://pythontutor.com/live.html#code=def%20fonction%28n%29%3A%0A%20%20%20%20a%3Dn%2B1%0Afonction%283%29%0Aprint%28a%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-live.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) pour visualiser l'exécution pas à pas (cliquer si besoin sur le bouton `first` pour remettre l'exécution au début)

_code 2_ : 


```python
a=18
def fonction(n):
    a = n + 1
    print(a)
    
fonction(3)
print(a)
```

Pas d'erreur ici.

Lors de l'appel de la fonction :
* la variable locale `a` est créée et on lui affecte la valeur `n + 1`, soit 6 ;
* cette valeur est affichée ;
* la variable locale `a` est supprimée en sortie de fonction.

Puis, lors de `print(a)`, c'est la variable globale `a` qui est utilisée et qui contient ici la valeur 18.

Donc, affichage obtenu :

6
18

[lien python tutor](http://pythontutor.com/live.html#code=a%3D18%0Adef%20fonction%28n%29%3A%0A%20%20%20%20a%3Dn%2B1%0A%20%20%20%20print%28a%29%0A%20%20%20%20%0Afonction%283%29%0Aprint%28a%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-live.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) pour visualiser l'exécution pas à pas

_code 3_ : 


```python
def fonc(p):
    p = 20
    print(p, q)

p = 15
q = 38
fonc(p)
print(p, q)   
```

Ici :

* les variables `globales` `p` et `q` sont définies ;
* lors de l'exécution de la fonction, une variable `locale` `p` est créée et affectée de la valeur 20 ;
* l'affichage réalisé est donc `(20, 38)` (pas de variable `q` locale -> on cherche si une variable globale de ce nom existe, ce qui est le cas ici) ;
* en sortie de fontion, la variable locale `p` est supprimée ;
* lors de l'affichage final, les variables gobales sont utilisées.

Affichages :

20 38  
15 38

[lien python tutor](http://pythontutor.com/live.html#code=def%20fonc%28p%29%3A%0A%20%20%20%20p%20%3D%2020%0A%20%20%20%20print%28p,%20q%29%0A%0Ap%20%3D%2015%0Aq%20%3D%2038%0Afonc%28p%29%0Aprint%28p,%20q%29%20&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-live.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) pour visualiser l'exécution pas à pas

_code 4_ : 


```python
def fonc():
    global p
    q = 20
    p = p + q
    print(p, q)

p = 15
q = 38
fonc()
print(p, q)
```

Ici on est dans un des cas des _subtilités_ :

* des variables `p` et `q` sont définies dans l'espace de nom global ;
* lors de l'entrée dans la fonction, il est précisé que la variable `p` est globale ;
* on affecte une valeur à la variable `q`. Une variable locale `q` est donc définie.
* à la ligne suivante, la valeur de `p` de l'espace global est utilisée et celle de `q` de l'espace local utilisée. Le résultat est affecté à la variable `p` dans l'espace global.
* Le premier affichage réalisé est donc : `35 20`
* on sort ensuite de la fonction et l'espace local des noms est supprimé.
* le deuxième affichage réalisé est donc `35 38`


[lien python tutor](https://pythontutor.com/visualize.html#code=def%20fonc%28%29%3A%0A%20%20%20%20global%20p%0A%20%20%20%20q%20%3D%2020%0A%20%20%20%20p%20%3D%20p%20%2B%20q%0A%20%20%20%20print%28p,%20q%29%0A%0Ap%20%3D%2015%0Aq%20%3D%2038%0Afonc%28%29%0Aprint%28p,%20q%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) pour visualiser l'exécution pas à pas

_code 5_ : 


```python
def fonc():
    global p
    p = p + 5
    q = 20
    print(p, q)

p = 15
q = 38
fonc()
print(p, q)
```

Ici :

* des variables `globales` `p` et `q` sont définies ;
* lors de l'appel de la fonction, il est précisé que la variable `p` est globale ;
* la valeur de cette variable globale est donc modifiée lorsque l'on effectue `p = p + 5` (la variable globale `p` contient 20) ;
* une variable `locale` `q` est définie ;
* on affiche alors `20 20`
* En sortie de fonction, la variable locale `q` est supprimée ;
* lors de l'affichage final, les variables `globales` sont utilisée et on affiche `20 38`.

Affichages :

20 20  
20 38

[lien python tutor](http://pythontutor.com/live.html#code=def%20fonc%28%29%3A%0A%20%20%20%20global%20p%0A%20%20%20%20p%20%3D%20p%20%2B%205%0A%20%20%20%20q%20%3D%2020%0A%20%20%20%20print%28p,%20q%29%0A%0Ap%20%3D%2015%0Aq%20%3D%2038%0Afonc%28%29%0Aprint%28p,%20q%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-live.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) pour visualiser l'exécution pas à pas

### Exercice 2

_code 1_


```python
def supprime(liste):
    liste.pop()

l = [1, 2, 3]
supprime(l)
print(l)
```

[Visualisation Python Tutor](https://pythontutor.com/visualize.html#code=def%20supprime%28liste%29%3A%0A%20%20%20%20liste.pop%28%29%0A%0Al%20%3D%20%5B1,%202,%203%5D%0Asupprime%28l%29%0Aprint%28l%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

L'affichage sera de `[1, 2]`.  
En effet, la variable locale `liste` de la fonction référence en mémoire vers le même objet que `l`.  

_code 2_


```python
def ajout(liste):
    l = liste
    l.append(4)

l = [1, 2, 3]
ajout(l)
print(l)
```

[Visualisation Python Tutor](https://pythontutor.com/visualize.html#code=def%20ajout%28liste%29%3A%0A%20%20%20%20l%20%3D%20liste%0A%20%20%20%20l.append%284%29%0A%0Al%20%3D%20%5B1,%202,%203%5D%0Aajout%28l%29%0Aprint%28l%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

L'affichage sera de `[1, 2, 3, 4]`.  
En effet, la variable locale `liste` de la fonction référence en mémoire vers le même objet que `l`.  

_code 3_


```python
def modif(liste):
    l = liste + [4]
    return l

l = [1, 2, 3]
modif(l)
print(l)
```

[Visualisation Python Tutor](https://pythontutor.com/visualize.html#code=def%20modif%28liste%29%3A%0A%20%20%20%20l%20%3D%20liste%20%2B%20%5B4%5D%0A%20%20%20%20return%20l%0A%0Al%20%3D%20%5B1,%202,%203%5D%0Amodif%28l%29%0Aprint%28l%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)

L'affichage sera de `[1, 2, 3]`.  
En effet, dans la fonction :
* une nouvelle liste est créée lors de `liste + [4]`
* le résultat est affecté à la variable locale `l` dont le contenue est retourné
* ce contenu n'est pas affecté.
* on affiche l'élément référencé par `l`, soit la liste de départ.

### Exercice 3


```python
def fonc1(t, q):
    global p
    q = q + p
    p = p +1
    print(p)
    print(q)
    return(q)
    
    

def fonc2(p, q):
    p = p + 3
    fonc1(p, q)
    print(p)
    print(q)
    return p
    
    

p = 4
q = 7
r = 2
print(fonc2(p, r))


```

Ici il est conseillé de construire (à l'image de ce que propose Python Tutor) des petits tableaux correspondants aux divers espaces de noms.

* on définit des variables globales `p`, `q` et `r` ;
* lors de l'appel de la fonction `fonc2(p, q)`, des variables locales de noms `p` et `q` sont crées puisque `p` et `q` sont les noms des paramètres de la fonction. `p` contient la valeur `4` et `q` contient `2` (l'appel de la fonction est fait avec `fonc2(p, r)` ;
* la valeur de `p` est modifiée et `p` contient la valeur `7` ;
* On appelle `fonc1(p, q)`. Des variables locales `t` et `q` sont crées dans l'espace des noms de `fonc1` contenant respectivement les valeurs `7` et `2`.
* Pour cette fonction, il est précisé que la variable `p` est globale, c'est à dire que pour `p`, les valeurs prises et les modifications effectuées seront faites sur la variable globale `p`.
* On effectue les opérations avec la variable locale `q` et la variable globale `p` qui contiennent lors des affichages les valeurs `p = 5` et `q = 6` ;
* la valeur `6` est retournée mais elle n'est pas utilisée (juste ` fonc1(p, q)` lors de l'appel : pas d'affectation ni d'affichage de la valeur retournée) ;
* la fonction `fonc1` est terminée, son espace des noms a été supprimé. On revient donc dans l'espace des noms local de la fonction `fonc2` ;
* dans cet espace, la variable `p` contient `7` et `q` contient `2`. Ces valeurs sont affichées ;
* la valeur de `p` (soit `7` est retournée et affichée (focntion `print` dans `print(fonc2(p, r))`

ainsi, en résumé, les affichages sont :

5  
6  
7  
2  
7

Si problème, je vous conseille de suivre pas à pas l'exécution du lien Python Tutor ci-dessous :

[lien python tutor](http://pythontutor.com/live.html#code=def%20fonc1%28t,%20q%29%3A%0A%20%20%20%20global%20p%0A%20%20%20%20q%20%3D%20q%20%2B%20p%0A%20%20%20%20p%20%3D%20p%20%2B1%0A%20%20%20%20print%28p%29%0A%20%20%20%20print%28q%29%0A%20%20%20%20return%28q%29%0A%20%20%20%20%0A%20%20%20%20%0A%0Adef%20fonc2%28p,%20q%29%3A%0A%20%20%20%20p%20%3D%20p%20%2B%203%0A%20%20%20%20fonc1%28p,%20q%29%0A%20%20%20%20print%28p%29%0A%20%20%20%20print%28q%29%0A%20%20%20%20return%20p%0A%20%20%20%20%0A%20%20%20%20%0A%0Ap%20%3D%204%0Aq%20%3D%207%0Ar%20%3D%202%0Aprint%28fonc2%28p,%20r%29%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-live.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) pour visualiser l'exécution pas à pas
