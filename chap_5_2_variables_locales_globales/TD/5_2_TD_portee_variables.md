# TD sur la portée des variables

_Conseil_ : utilisez [Python Tutor](https://pythontutor.com/visualize.html#mode=edit) pour confirmer (ou pas !) votre réponse en visualisant le fonctionnement.

### Exercice 1

Déterminer dans chacun des cas suivant si une erreur se produit et, le cas échéant, les affichages obtenus

_code 1_ :


```python
def fonction(n):
    a = n + 1
fonction(3)
print(a)
```

_code 2_ : 


```python
a=18
def fonction(n):
    a = n +1
    print(a)
    
fonction(3)
print(a)

```

_code 3_ : 


```python
def fonc(p):
    p = 20
    print(p, q)

p = 15
q = 38
fonc(p)
print(p, q)   
```

_code 4_ : 


```python
def fonc():
    global p
    q = 20
    p = p + q
    print(p, q)

p = 15
q = 38
fonc()
print(p, q)
```

_code 5_ : 


```python
def fonc():
    global p
    p = p + 5
    q = 20
    print(p, q)

p = 15
q = 38
fonc()
print(p, q)
```

### Exercice 2

Déterminer dans chacun des cas ci-dessous l'affichage réalisé :

_code 1_


```python
def supprime(liste):
    liste.pop()

l = [1, 2, 3]
supprime(l)
print(l)
```

_code 2_


```python
def ajout(liste):
    l = liste
    l.append(4)

l = [1, 2, 3]
ajout(l)
print(l)
```

_code 3_


```python
def modif(liste):
    l = liste + [4]
    return l

l = [1, 2, 3]
modif(l)
print(l)
```

### Exercice 3 (plus dur)

Déterminer si une erreur se produit et, le cas échéant, les affichages obtenus

_code 6_ : 


```python
def fonc1(t, q):
    global p
    q = q + p
    p = p +1
    print(p)
    print(q)
    return(q)
    
    

def fonc2(p, q):
    p = p + 3
    fonc1(p, q)
    print(p)
    print(q)
    return p
    
    

p = 4
q = 7
r = 2
print(fonc2(p, r))


```
