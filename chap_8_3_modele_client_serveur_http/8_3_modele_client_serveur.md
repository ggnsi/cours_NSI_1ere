# Le modèle client-serveur

## Des petites vidéos pour commencer

Ces vidéos illustrent le fonctionnement client/serveur dans le cadre de sites web.

* [vidéo 1](https://vimeo.com/138623558) (université de Lille) : client/serveur
* [vidéo 2](https://www.youtube.com/watch?v=GqD6AiaRo3U&list=PLWvGMqXvyJAPvxp2IPUAbKmr_ULtR8Nbh&index=3) (pixees) : site internet ou site web ?

## Le modèle client-serveur

_D'après [https://fr.wikipedia.org/wiki/Client-serveur](https://fr.wikipedia.org/wiki/Client-serveur)_

>L'environnement client/serveur désigne un mode de communication à travers un réseau entre plusieurs programmes :
>
>* l'un, qualifié de **client**, envoie des requêtes ;
>* l'autre, qualifié de **serveur**, attend les requêtes des clients et y répond.

Par extension, le client désigne également l'ordinateur sur lequel est exécuté le logiciel client, et le serveur l'ordinateur sur lequel est exécuté le logiciel serveur.

![image_client_serveur](./img/clients-server.png)

### Les clients

Les clients sont souvent des ordinateurs personnels ou des appareils individuels (téléphone, tablette).

### Les serveurs

N'importe quel ordinateur peut jouer le rôle de serveur mais les serveurs sont généralement des ordinateurs dédiés et qui sont dotés de capacités supérieures à celles des ordinateurs personnels en ce qui concerne la puissance de calcul, la capacité de stockage, les entrées/sorties et les connexions réseau.  
Ce sont souvent des machines conçues pour fonctionner 24h/24.

Un serveur peut répondre aux requêtes d'un grand nombre de clients.

Il existe une grande variété de serveurs en fonction des besoins à servir :

* un **serveur Web** publie des **pages Web** demandées par des navigateurs Web ;
* un **serveur de fichiers** met à disposition, permet de recevoir, stocker et envoyer des fichiers ;
* un **serveur de messagerie électronique** stocke et **envoie du courriel** à des clients de messagerie ;
* un **serveur de base de données** permet de récupérer des données stockées dans une base de données, etc.

## La communication entre le client et le serveur

### Les protocoles

> Un protocole est un ensemble de règles définissant le mode de communication entre deux ordinateurs.
> Il définit le format et l'enchaînement des messages qui doivent être échangés, ainsi que les actions à réaliser lors de la réception de ces messages.

Un protocole définit le format et l’enchaînement des messages qui doivent être échangés, ainsi que les actions à réaliser lors de la réception de ces messages.

Des protocoles différents sont utilisés selon l'objectif de la communication entre les machines. 

_Exemples_ :

* le protocole **HTTP** est destiné au transfert de ressources (souvent à destination de navigateur pour l'affichage de pages web) ;
* le protocole **FTP** (**F**ile **T**ransfert **P**rotocol) est destiné au transfert de fichiers par internet.
* le protocole **IP** (**I**nternet **P**rotocol) est destiné à l'acheminement des paquets de données sur le réseau internet

### Actions du serveur

Actions du programme serveur :

* il attend une demande de connexion entrante sur un ou plusieurs ports réseaux ;
* il établit la connexion avec le client ;
* à la suite de la connexion, le processus serveur communique avec le client en suivant le protocole prévu.

### Actions du client

Actions du programme client :

* il réalise une demande de connexion au serveur ;
* il établit la connexion au serveur ;
* lorsque la connexion est établie, il communique en suivant le protocole prévu.

Le client et le serveur doivent bien sûr utiliser le même protocole de communication.. On parle souvent d'un service pour désigner la fonctionnalité offerte par un processus serveur.

## Quelques spécificités d'un serveur web

Il y a quelques années, le web était dit « statique » : le concepteur d'un site web écrivait son code HTML et ce code était simplement envoyé par le serveur web au client. Les personnes qui consultaient le site recevaient tous la même page.

Les choses ont ensuite évolué : les serveurs sont aujourd'hui capables de générer eux-mêmes du code HTML. Les requêtes faites par les clients sont plus complexes que de simples liens vers des pages. Les résultats qui sont renvoyés par le serveur diffèrent d'un client à l'autre en fonction des requêtes : le web est devenu dynamique.

Différents langages de programmation peuvent être utilisés « côté serveur » afin de permettre au serveur de générer lui-même le code HTML à renvoyer. Le plus utilisé encore aujourd'hui se nomme PHP, mais d'autres langages sont utilisables comme Java, Python...

**Exemple simple de code en PHP** [^1]:

````html
<?php
$heure = date("H:i");
echo '<h1>Bienvenue sur mon site</h1>
      <p>Il est '.$heure.'</p>';
?>
```

Sans entrer dans les détails, si un client se connecte à un serveur web qui exécute ce code à 18h23, le serveur enverra au client le code HTML ci-dessous :

```html
<h1>Bienvenue sur mon site</h1>
<p>Il est 18h23</p>
```

En revanche si un client se connecte à ce même serveur à 9h12, le serveur enverra au client le code HTML ci-dessous :

```html
<h1>Bienvenue sur mon site</h1>
<p>Il est 9h12</p>
```

L'exemple est très simple et il est bien sur possible de générer des pages HTML bien plus complexes.

---
* [1] : [pixees](https://pixees.fr/informatiquelycee/n_site/nsi_prem_clsv.html)
