# Le protocole HTTP
---

## Introduction
---

Le _Web_ est à l'origine une collection de ressources (pages html, images, fichiers vidéo, texte, ...) reliés entre eux par des liens hypertextes.
Il est maintenant bien plus que cela avec des sites Web qui sont dynamiques et proposent l'accès à des applications et des traitements complexes effectués côté serveur.

**Le protocole HTTP** est celui régissant les échanges entre client (navigateur) et serveur Web.

* les messages envoyés par le client sont appelés des **requêtes** ;
* ceux envoyés pas le serveur sont appelés des **réponses**.

![image requette_reponse](./img/schema_requete_http.png)

---

## URL (Uniforme Resource Locator)
---

Pour accéder à une ressource (page Web, image, fichier,...), on saisit son _adresse_ (souvent dans la barre d'adresse du navigateur).

Une telle adresse s'appelle une **URL**.

> La syntaxe (simplifiée) d'une URL est de la forme :
>
> **protocole://nom-ou-adresse/chemin_vers_la_ressource**

_Exemple_ : `http://fr.wikipedia.org/wiki/Grace_Hopper.html`

Il y a 3 grandes parties :

* Le protocole (http, https, ftp, ...),
* l'adresse IP (ex : 216.58.198.195) ou le domaine précédé éventuellement du sous-domaine (ex : fr.wikipedia.org),
* le chemin vers la ressource sur le serveur

## Déroulé d'une connexion à un serveur

Si un utilisateur saisit une adresse dans un navigateur (ex : `http://fr.wikipedia.org/wiki/Grace_Hopper.html`) :

* Le navigateur Web isole le nom du serveur `fr.wikipedia.org`

* Le navigateur Web effectue une requête DNS pour obtenir l'adresse IP du serveur Web hébergeant le site ;

* Le navigateur Web se connecte à la machine dont l'adresse IP est maintenant connue, en utilisant sur le port 80 (par défaut pour un serveur Web).

* Une fois la connexion établie, le navigateur Web envoie un certain nombre de messages en se conformant au protocole HTTP, pour demander la ressource `Grace_Hopper.html` contenue dans le dossier `wiki`.

* Le serveur Web envoie en réponse le contenu du fichier au navigateur Web

* Le navigateur Web de l'utilisateur peut ensuite parcourir le fichier, et afficher la page correspondante.

Toutes ces actions, qui sont effectuées en quelques milli-secondes, se répètent à chaque fois que la navigation de l'utilisateur nécessite une nouvelle ressource :

* clic sur un lien ;

* présence d'une image, d'une vidéo...

* lorsque l'utilisateur soumet des informations au moyen d'un formulaire.


**Rappel de 2de** :

Le **nom de domaine** est un moyen simple pour nous humain de nous souvenir de l'adresse de site.  
La première chose que fait le navigateur est de _résoudre le nom de domaine_, c'est à dire d'effectuer une **requette DNS** afin de connaître l'adresse IP du serveur hébergeant ce site.

Pour ceux qui ont oublié, une [vidéo de Cookie connecté](https://www.youtube.com/watch?v=qzWdzAvfBoo)

---

## Détail du protocole HTTP
---

### Requête du client

Les requêtes du client concernent le plus souvent la demande de ressources. Mais elles peuvent également être des données d'identification (cookie par exemple) ou autre.

Il existe plusieurs types de requêtes :
* `GET`
* `POST`
* `DELETE`
* ...

Les informations nécessaires à la requête sont placées dans **des en-têtes** un peu comme si les messages étaient placés dans une enveloppe.  

_Exemple_ :

Voici un extrait de la requête envoyée pour demander la page dont l'URL est `https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol`

La première ligne indique le type de requête(GET, POST...), l'adresse de la ressource, et la version du protocole.

```html
GET /wiki/Hypertext_Transfer_Protocol HTTP/2
Host: fr.wikipedia.org
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3
...
Cookie: WMF-Last-Access=14-Sep-2020; WMF-Last-Access-Global=14-Sep-2020; GeoIP=FR:PAC:Nice:43.71:7.26:v4;
...
```

Chaque entête permet de transmettre des informations :
* `GET /wiki/Hypertext_Transfer_Protocol HTTP/2` :
    * la requête est de type `GET`
    * l'adresse de la ressource demandée sur le serveur est `/wiki/Hypertext_Transfer_Protocol`
    * la version du protocole utilisée est `HTTP/2`.
* `Host: fr.wikipedia.org` : l'Hôte de destinatin est `fr.wikipedia.org`
* `Firefox/80.0` : la version du navigateur utilisé (le serveur peut s'adapter au navigateur car tous n'ont pas les mêmes fonctionnalités implentées)
* `Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8` : le type de documents acceptés
* les langues acceptées ;
* `Cookie:...` : les informations émanant du cookie stocké sur le client
* ...

### Réponse du serveur

La réponse du serveur comprend elle aussi un certain nombre d'entêtes puis la ressource demandée.

La réponse commence par un code d'état:
* 200: succès de la requête
* ...
* 301 ou 302: redirection vers une autre page;
* 403 : accès non autorisé
* 404: page non trouvée
* ...
* 500: erreur du serveur
* ...

_Exemple_ : 

Voici une partie de la réponse du serveur à la requête précedente :

```html
HTTP/2 200 OK
date: Sun, 13 Sep 2020 09:14:27 GMT
...
content-language: fr
...
last-modified: Sun, 06 Sep 2020 18:23:06 GMT
content-type: text/html; charset=UTF-8
...
content-length: 33988
...

<!DOCTYPE html>
<html class="client-nojs" lang="fr" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Hypertext Transfer Protocol — Wikipédia</title>
...
</html>
```

La réponse comprend donc entre autre la version du protocole, le code d'état, la date, la langue utilisée, la date de dernière modification, le type de fichier (si html, le navigateur la compose, si fichier compressé : il propose l'ouverture ou la sauvegarde, etc.), ... puis la ressource demandée (ici le code HTML de la page web).

### Syntaxe complète d'une URL

La syntaxe des URL est plus complexe que celle présentée plus haut.  
Dans le cas de pages dynamiques, il est possible d'ajouter des paramètres aux URL en les plaçant après le chemin de la ressource.
Une URL peut être de la forme suivante :

> `protocole://nom_ou_adresse:port/chemin_ressource?mot_cle_1=valeur_1&mot_cle_2=valeur_2...#id`

Par exemple, en utilisant le moteur de recherche de Wikipédia :

> [https://fr.wikipedia.org/w/index.php?search=Johnny&fulltext=1&ns0=1](https://fr.wikipedia.org/w/index.php?search=Johnny&fulltext=1&ns0=1)

* port : pour http, le port TCP par défaut est 80 (443 pour https) mais il est possible de configurer cela, par exemple si on veut avoir plusieurs serveurs sur la même machine.

* document : le document est écrit comme chemin relatif à partir du répertoire où est stocké le site sur le serveur (généralement /var/www/html/). Cependant, le serveur peut réécrire l'URL en transformant l'adresse en un chemin n'ayant rien à voir. Par exemple, avec des pages web personnelles sur des serveurs mutualisés : `https://www.mon-site.fr/perso/alice` correspond en réalité à `/home/alice/public_html` et le dossier perso n'existe pas vraiment sur le serveur...

* `?` : la partie située après le document et marqué par un "?" est la liste des paramètres de requête. Ces paramètres sont des couples pi=vi ou pi est le nom du paramètre et vi sa valeur.

* `&` : les paires précédentes sont séparées par un &, cela permet donc de passer des valeurs au serveur avec la requête.

* `#id` : cette portion est appelée un signet et permet de cibler un élément particulier dans la ressource. Il s'agit donc de l'identifiant d'un élément HTML.


### Les échanges sécurisés avec le protocole HTTPS

Un inconvénient du protocole HTTP est qu'il s'agit d'un protocole _en clair_.  
N'importe qui ayant les droits suffisants peut intercepter les paquets réseaux échangés entre le client et le serveur et lire le contenu des messages (par exemple, il suffit d'avoir des droits d'administration sur une machine située sur le chemin entre client et serveur).

C'est un problème en terme de sécurité et de confidentialité (par exemple transmission d'identifiant et de mot de passe).

> L'HyperText Transfer Protocol Secure (HTTPS : « protocole de transfert hypertextuel sécurisé ») est **la combinaison du HTTP avec une couche de chiffrement**.

HTTPS permet au visiteur de vérifier l'identité du site web auquel il accède, grâce à un certificat d'authentification émis par une autorité tierce, réputée fiable (et faisant généralement partie de la liste blanche des navigateurs internet). Il garantit théoriquement la confidentialité et l'intégrité des données envoyées par l'utilisateur (notamment des informations entrées dans les formulaires) et reçues du serveur. Il peut permettre de valider l'identité du visiteur si celui-ci utilise également un certificat d'authentification client.

Par défaut, les serveurs HTTPS sont connectés au port TCP 443.  
On reconnait le chiffrement grâce au symbole affiché dans la barre du navigateur (cadenas sur Firefox par exemple).

**Principe**

Le protocole est identique au protocole web HTTP, mais avec un ingrédient supplémentaire dit **TLS** qui fonctionne à peu près comme suit :

* le client — par exemple le navigateur Web — contacte un serveur — par exemple Wikipédia — et demande une connexion sécurisée, en lui présentant un certain nombre de méthodes de chiffrement de la connexion ;

* le serveur répond en confirmant pouvoir dialoguer de manière sécurisée et en choisissant dans cette liste une méthode de chiffrement et surtout, en produisant un certificat garantissant qu'il est bien le serveur en question et pas un serveur pirate déguisé. Ces certificats électroniques sont délivrés par une autorité tierce dans laquelle tout le monde a confiance (un peu comme un notaire dans la vie courante).

* Le certificat contient aussi une clef publique qui permet de crypter un message pour le rendre uniquement déchiffrable par le serveur qui a émis cette clef (grâce à une clé privée, que seul le serveur détient) ; cela permet au client d'envoyer de manière secrète une clef de chiffrement symétrique qui permettra le chiffrement des échanges entre le serveur et le client.

En bref : serveur et client se sont reconnus, ont choisi une manière de chiffrer la communication et se sont passés de manière chiffrée une clef de chiffrement symétrique pour dialoguer de manière secrète.

![shéma requete https](./img/schema-requete-https.jpg)

### Le protocole HTTP : un protocole sans mémoire

Le protocole HTTP est un protocole **textuel, sans état**: 

* Textuel : toutes les commandes qui sont échangées sont du texte pouvant être lu par un humain.

* Sans état : ni le serveur, ni le client ne se souviennent des communications précédentes. Par exemple, si on utilisait uniquement HTTP, un serveur ne pourrait pas se souvenir si un mot de passe a été saisi ou si une transaction est en cours (pour gérer cela, il faut utiliser un serveur d'applications).

Pour se souvenir de données concernant le client, on utilise des éléments supplémentaires, par exemple :
* **Cookies** stoqués **sur la machine cliente** ;
* **des données de session** stoquées sur **le serveur**.

Cela peut poser des problèmes concernant la vie privée.

---
Sources :
* [lyceum.fr](http:lyceum.fr)
* [lycée la Martinière](http://portail.lyc-la-martiniere-diderot.ac-lyon.fr/srv1/co/_Module_NSI.html)
* livre NSI édition Ellipses
* source des images : [https://wpmarmite.com/wordpress-https/](https://wpmarmite.com/wordpress-https/)


