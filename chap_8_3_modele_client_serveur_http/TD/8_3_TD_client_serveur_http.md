# TD Interaction client-serveur, requêtes HTTP

### Exercice 1

Dans une nouvelle fenêtre de Firefox, naviguez dans le menu `outils -> outils du navigateur -> outils de développement Web` (ou appuyez sur `F12`) et sélectionnez (si ce n'est pas déjà fait) l'onglet `Réseau`.

Ouvrez la page `http://perdus.com` via la barre d'adresse.

1. En consultant les requêtes (ne pas hésiter à explorer la zone de droite), déterminer :

    * Combien de requêtes ont été effectuées  ? de quel type ?
    * Quelle est l'adresse ip du site `perdus.com` ?
    * Quelles sont les valeurs de l'entête `Accept-Encoding` dans la requête ?
    * Trouvez à quoi correspondent les valeurs indiqués dans cette entête.
    * Quelle est la valeur envoyée dans l'entête `Connection` de la requête ?
    * Que signifie-t-elle ?
    * Quelle est la longueur du contenu de la réponse du serveur ?
    * Quel est le type du contenu de la réponse du serveur ?
    * Toutes les requêtes ont-elle abouti ?

2. Utilisez la moteur de recherche [Qwant](https://www.qwant.com/) et faites-y une recherche de `perdus.com` puis cliquez sur le lien proposé.

Le site `perdus.com` peut-il savoir que vous y accédez via Qwant ? (indice : consultez la requête)

3. Accédez au site du [Lycée Beaupré](https://lycee-beaupre.fr/).

    * Une seule requête est-elle effectuée ?
    * Toutes les ressources sont-elles stockées sur le même serveur ?
    * Rechargez la page et observez la colonne 'Transfert' : que constatez-vous ? Expliquez.
    * Quel est alors le code d'état obtenu pour les ressources concernées ? A quoi correspond-il ?
    
4. Accédez au site [www.ostralo.net](http://ostralo.net)

    * Quel est la taille de l'image `titre.png` ?

### Exercice 2

Pour que le protocole https fonctionne, il faut impérativement **un certificat sur le serveur web*, délivré par une autorité de certification**.

Connectez vous sur la page du portail de connexion à l'ENT : [https://enthdf.fr](https://enthdf.fr) puis, en cliquant sur le cadenas, déterminez :

* Quel est l'organisme qui a émis le certificat de sécurité (affichez le certificat) ;
* Quelle est la date d'expiration de ce certificat ?
* Quel algorithme de chiffrement est utilisé et quelle est la taille de la clé publique ?

### Exercice 3 (à faire)

* page dynamique PHP à importer sur serveur web

---
Sources : [lycée la Martinière](http://portail.lyc-la-martiniere-diderot.ac-lyon.fr/srv1/co/_Module_NSI.html)
