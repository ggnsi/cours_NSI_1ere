# Les fonctions

## I. Les fonctions natives et leur documentation

En Python, un certain nombre de fonctions sont déjà disponibles. Elles sont dites **natives** ou **Built-in**.

Par exemple __type()__ est une fonction _native_.

Il en existe beaucoup d'autres. Par exemple, la fonction __round()__.


```python
round(1.2)
```

On devine bien son intérêt mais il serait intéressant de savoir exactement comment elle fonctionne et voir si elle admet des arguments facultatifs. Il suffit alors de regarder dans la __documentation__ de cette fonction.

Cela est possible avec la fonction native __help()__.


```python
help(round)
```

    Help on built-in function round in module builtins:
    
    round(number, ndigits=None)
        Round a number to a given precision in decimal digits.
        
        The return value is an integer if ndigits is omitted or None.  Otherwise
        the return value has the same type as the number.  ndigits may be negative.
    
    

Cette fonction permet d'accéder à la documentation de n'importe quelle fonction dont le nom est passé en __argument__ ( _entre parenthèses_ )

Ici par exemple on peut lire que :

* `round()` est une fonction native du langage Python : `function....in module builtins`  
  
* Elle peut accepter au maximum 2 arguments : `(number, ndigits=None)`  
  
* Elle arrondit à un certain nombre de décimales : `Round a number to a given precision in decimal digits.`  
  
* Si le 2ème argument n'est pas précisé, l'arrondi est entier : `The return value is an integer if ndigits is omitted or None`

Lors de la conception d'une fonction les programmeurs ont donc veillé à bien documenté leur code : une __pratique indispensable__ !

La liste compléte des fonctions natives est disponible [ici](https://docs.python.org/fr/3.5/library/functions.html)

## II Fonctions définies par l'utilisateur

### 1. Définition et appel d'une fonction

Nous avons vu à la fin du TP précédent (calcul de l'IMC) qu'il était pénible de devoir répéter les mêmes instructions plusieurs fois à la main.

Il serait donc intéressant de disposer facilement d'une séquence d'instruction que l'on pourrait utiliser plusieurs fois en l'appelant avec des valeurs d'entrées distinctes.

C'est exactement le concept de fonction.

Une fonction est une séquence d'instructions, dépendant de paramètres d'entrées (les valeurs utilisées étant appelées _arguments_ ) et retournant (le plus souvent) un résultat.

Un fonction peut être vue de deux points de vue complémentaires :

- c'est une séquence d'instruction qui permet de réaliser un calcul précis que l'on peut utiliser plusieurs fois ;

- c'est une brique de base d'un problème plus complexe.

__Définissons la fonction__ `imc(taille, masse)` **via le shell**  :

Ecrivons (dans le shell) puis validons :


```python
def imc(taille, masse):
    valeur_imc = masse / (taille ** 2)
    return valeur_imc
```

La fonction est mise en mémoire (voir dans la fenêtre variables).

Pour __appeler la fonction__, tapons dans le _Shell_ : `imc(1.5, 50)` puis validons.  
La valeur retournée par la fonction s'affiche dans le **shell** (c'est une spécificité de Python).  
Ne pas confondre cet affichage dans le shell avec un affichage à l'écran (fonction `print()`)

**Structure d'une fonction** :

>- Une fonction est définie par une _entête_ : le mot clé __def__ suivi du nom de la fonction ;
>
>- entre parenthèses, la liste des arguments attendus par la fonction. Si une fonction ne nécessite pas d'argument, >les parenthèses restent obligatoires ;
>
>- les : à la fin de cette première ligne définissent un bloc d'instruction.
>Ce bloc est défini dans Python par son __indentation__ (décalage avec des espaces par rapport au début de ligne). >Cela permet de délimiter le bloc de code correspondant au __corps de la fonction__.
>
>- l'instruction __return__
>
>Le corps de la fonction peut comprendre une ou plusieurs instructions __return resultat__, où resultat est une >expression.  
>Lors du déroulement de la fonction, si une telle instruction est rencontrée, alors l'expression _résultat_ est >renvoyée et l'éxécution de la fonction interrompue.

_Remarque_ : une fonction peut ne renvoyer aucun élément.

_Exemple_ : (au passage, cette fonction ne nécessite aucune valeur à passer en paramètre)


```python
def bonjour():
    print('Hello World !')
```

Cette fonction affichera le message mais ne renvoiera aucune valeur.

Exécuter dans le shell les commandes suivantes :


```python
print("l'imc est de " + imc(1.5, 50))
```


```python
print(bonjour())
```

### 2. Sauvegarde des fonctions dans un script

Taper ses fonctions dans le __shell__ entraine qu'à la fermeture du logiciel, ces  fonctions disparaissent.

Afin, entre-autre de pouvoir les sauvegarder, il suffit d'entrer ses fonctions dans la partie _éditeur_ de _Thonny_. On dira que l'on a crée un __script__.

Ce script peut être alors exécuté (flèche verte). Python déroule alors le script et met en mémoire les fonctions définies. Les fonctions définies peuvent être alors appelées depuis le __shell__ (ou dans le script lui même).  
La valeur **retournée** par la fonction est affichée dans la console en dessous de l'appel.

> _Remarque_ : c'est une erreur courante d'oublie de relire le script quand on a fait des modifications.

**Travail à faire vous même** : 

Vous testerez chacune des fonction en l'appelant via la console sur plusieurs exemples (exemple pour la fonction `double(x)` , taper dans la console `double(2)`, `double(0)`, ...)

* Définir une fonction `double(x)` qui renvoie le double de la valeur `x` (int ou float) passée en paramètre ;

* définir une fonction `somme(a,b)` qui retourne la somme des deux valeurs (int ou float) passées en paramètre ;

* définir une fonction `bonjour(nom)` qui **affiche** le texte `'Bonjour nom'`.

_Remarque_ : tester `somme("biba", "beloula")`

## III. Importer des fonctions : les modules

Les __modules__ sont des __scripts__ Python qui contiennent notamment un ensemble de fonctions que l'on est amené à réutiliser souvent (on les appelle aussi bibliothèques ou librairies). 

L'installation de Python fournit en plus des fonctions natives (module `builtins`) une bibliothéque standard contenant plusieurs modules pouvant répondre à des besoins divers :

* le module `math` contenant des fonctions mathématiques courantes
* le module `random` utilisant des nombres aléatoires
* le module `Tkinter` permettant de réaliser des interfaces graphiques
* le module `turtle` permettant de dessiner
et bien d'autres .....

Toutes ces fonctionnalités n'étant pas utiles dans tous les programmes, les modules correspondants ne sont donc pas chargés au démarrage. 
Il faut le réaliser manuellement.

## Importer un module / importer certaines fonctions d'un module

On peut importer globalement un module ou n'importer que quelques fonctions (ou constantes) d'un module

### Importer un module complet globalement

On importe le module (en **début de script**) en utilisant la commande `import module_truc`.
L'utilisation d'une fonction (ou constante) se fait alors avec d'une** notation pointée**

_Exemple_ :

```Python
import math # on importe le module math
rac = math.sqrt(9) # on utilise la fonction sqrt (racine carrée) du module math

rayon = 5
surface_disque = math.pi * rayon ** 2 #on utilise la constante pi du module math
```

### Importer certaines fonctions (ou constantes) d'un module

Si on ne souhaite importer **QUE** la fonction `sqrt` du module `math`, on écrira :

```Python
from math import sqrt
```

L'utilisation de la fonction se fait alors directement (**sans** notation pointée) :

```Python
from math import sqrt
rac = sqrt(9)
```

On peut importer plusieurs fonctions d'un coup :

```Python
from math import sqrt, pi, cos
```

On peut importer d'un coup **toutes** les fonctions d'un module mais cela peut encombrer inutilement la mémoire si on en utilise que quelques une :

```Python
from math import *
```


### Le module math 


Quelques fonctions définies dans le module `math` :

| | |
|:--:|:--:|
|`sin(x)`, `cos(x)`, ...| Les fonctions de trigonométrie en radian|
|`floor(x)` et `ceil(x)`| arrondis de `x` par défaut et par excès|
|`pi` et `e` | Les constantes `pi` et `e`|
|`sqrt(x)`| La racine carrée de `x`|

_Remarque_ :
* `dir(math)` va lister toutes les fonctions du module
* `help(math)` affichera toutes les fonctions ainsi que les aides correspondantes


## Le module random

Ce module regroupe toutes les fonctions liées à la génération de nombres aléatoires, la documentation est disponible [ici](https://docs.python.org/fr/3/library/random.html?highlight=random#module-random)

Quelques fonctions définies dans le module `random` :

| | |
|:--:|:--:|
| `randint(a,b)` | Renvoie un nombre entier aléatoire entre a et b inclus |
| `random()` | Renvoie un nombre à virgule aléatoire entre 0 inclu et 1 exclu|

_Exemple_ : `randint(1, 6)` permet de simuler le lancer d'un dé à 6 faces.


## Complements 1 : les commentaires

Vous allez être emmener à manipuler de très nombreuses fonctionnalités du langage Python et de ses modules, il est inenvisageable d'acquérir une connaissance exhaustive de toutes celles-ci.  
Dans les scripts que vous utiliserez vous pouvez déposer des commentaires à l'aide du symbole `#` pour expliquer certaines parties de codes ou certains choix.    
Ils ne seront pas interprétés et aideront le programmeur(vous) à mieux comprendre le code.

```python
>>> x = 3  # x est une variable correspondant à une abscisse
>>> x
3
```

## Complément 2 :  Générer la documentation associée à une fonction : le docstring

Cette notion sera reprise plus tard dans l'année mais pour ceux en avance, il peut être intéressant de lire ce paragraphe.

La **docstring** est une chaine de caractère encadrée par un couple de 3 guillements doubles \"\"\" et placée juste après l'en-tête de la fonction.


```python
def produit(a, b):
    """Renvoie le produit des nombres a et b
    
    param : a,b : int ou float
    return : a*b : int ou float"""
    
    return a * b
```

On y indique  :

- ce que fait la fonction ; 
- le type des paramètres ;
- le résultat retourné et son type.

On peut accéder à la documentation de la fonction :


```python
help(produit)
```

    Help on function produit in module __main__:
    
    produit(a, b)
        Renvoie le produit des nombres a et b
        param : a,b : int ou float
        return : a*b
    
    
