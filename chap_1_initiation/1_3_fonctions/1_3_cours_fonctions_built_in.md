# Les fonctions

## I. Les fonctions natives et leur documentation

En Python, un certain nombre de fonctions sont déjà disponibles. Elles sont dites **natives** ou **Built-in**.

Par exemple __type()__ est une fonction _native_.

Il en existe beaucoup d'autres. Par exemple, la fonction __round()__.


```python
round(1.2)
```

On devine bien son intérêt mais il serait intéressant de savoir exactement comment elle fonctionne et voir si elle admet des arguments facultatifs. Il suffit alors de regarder dans la __documentation__ de cette fonction.

Cela est possible avec la fonction native __help()__.


```python
help(round)
```

    Help on built-in function round in module builtins:
    
    round(number, ndigits=None)
        Round a number to a given precision in decimal digits.
        
        The return value is an integer if ndigits is omitted or None.  Otherwise
        the return value has the same type as the number.  ndigits may be negative.
    
    

Cette fonction permet d'accéder à la documentation de n'importe quelle fonction dont le nom est passé en __argument__ ( _entre parenthèses_ )

Ici par exemple on peut lire que :

* `round()` est une fonction native du langage Python : `function....in module builtins`  
  
* Elle peut accepter au maximum 2 arguments : `(number, ndigits=None)`  
  
* Elle arrondit à un certain nombre de décimales : `Round a number to a given precision in decimal digits.`  
  
* Si le 2ème argument n'est pas précisé, l'arrondi est entier : `The return value is an integer if ndigits is omitted or None`

Lors de la conception d'une fonction les programmeurs ont donc veillé à bien documenté leur code : une __pratique indispensable__ !

La liste compléte des fonctions natives est disponible [ici](https://docs.python.org/fr/3.5/library/functions.html)

> Si cette documentation est insuffisante pour notre compréhension, on n'hésite pas à faire une recherche (avec par exemple comme mots clés "Python round" et/ou à aller directement consulter la [documentation en ligne sur la fonction](https://docs.python.org/fr/3.7/library/functions.html?highlight=round#round)
