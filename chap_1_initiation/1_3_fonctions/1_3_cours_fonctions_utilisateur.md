# Les fonctions

## II Fonctions définies par l'utilisateur

### 1. Définition et appel d'une fonction

Nous avons vu à la fin du TP précédent (calcul de l'IMC) qu'il était pénible de devoir répéter les mêmes instructions plusieurs fois à la main.

Il serait donc intéressant de disposer facilement d'une séquence d'instruction que l'on pourrait utiliser plusieurs fois en l'appelant avec des valeurs d'entrées distinctes.

C'est exactement le concept de fonction.

> Une fonction est une séquence d'instructions, dépendant de paramètres d'entrées (les valeurs utilisées étant appelées _arguments_ ) et retournant (le plus souvent) un résultat.

Un fonction peut être vue de deux points de vue complémentaires :

- c'est une séquence d'instruction qui permet de réaliser un calcul précis que l'on peut utiliser plusieurs fois ;

- c'est une brique de base d'un problème plus complexe.

__Définissons la fonction__ `imc(taille, masse)` **via le shell**  :

Ecrivons (dans le shell) puis validons :


```python
def imc(taille, masse):
    valeur_imc = masse / (taille ** 2)
    return valeur_imc
```

La fonction est mise en mémoire (voir dans la fenêtre variables).

Pour __appeler la fonction__, tapons dans le _Shell_ : `imc(1.5, 50)` puis validons.  
La valeur retournée par la fonction s'affiche dans le **shell** (c'est une spécificité de Python).  
Ne pas confondre cet affichage dans le shell avec un affichage à l'écran (fonction `print()`)

**Structure/Synthaxe d'une fonction** :

>- Une fonction est définie par une _entête_ : le mot clé __def__ suivi du nom de la fonction ;
>
>- entre parenthèses, la liste des arguments attendus par la fonction. Si une fonction ne nécessite pas d'argument, >les parenthèses restent obligatoires ;
>
>- les : à la fin de cette première ligne définissent un bloc d'instruction.
>Ce bloc est défini dans Python par son __indentation__ (décalage avec des espaces par rapport au début de ligne). >Cela permet de délimiter le bloc de code correspondant au __corps de la fonction__.
>
>- l'instruction __return__
>
>Le corps de la fonction peut comprendre une ou plusieurs instructions __return resultat__, où resultat est une >expression.  
>Lors du déroulement de la fonction, si une telle instruction est rencontrée, alors l'expression _résultat_ est >renvoyée et l'éxécution de la fonction interrompue.

_Remarque_ : une fonction peut ne renvoyer aucun élément.

_Exemple_ : (au passage, la fonction ci-dessous ne nécessite aucune valeur à passer en paramètre)


```python
def bonjour():
    print('Hello World !')
```

Cette fonction affichera le message mais ne renvoiera aucune valeur.

Exécuter dans le shell les commandes suivantes :


```python
print("l'imc est de " + imc(1.5, 50))
```


```python
print(bonjour())
```

### 2. Sauvegarde des fonctions dans un script

Taper ses fonctions dans le __shell__ entraine qu'à la fermeture du logiciel, ces  fonctions disparaissent.

Afin, entre-autre de pouvoir les sauvegarder, il suffit d'entrer ses fonctions dans la partie _éditeur_ de _Thonny_. On dira que l'on a crée un __script__.

Ce script peut être alors exécuté (flèche verte). Python déroule alors le script et met en mémoire les fonctions définies. Les fonctions définies peuvent être alors appelées depuis le __shell__ (ou dans le script lui même).  
La valeur **retournée** par la fonction est affichée dans la console en dessous de l'appel.  
Le script peut bien sûr être sauvegardé (c'est l'intérêt).

> _Remarque_ : c'est une erreur courante d'oublie de relire le script quand on a fait des modifications.

**Travail à faire vous même** : 

Vous testerez chacune des fonction en l'appelant via la console sur plusieurs exemples (exemple pour la fonction `double(x)` , taper dans la console `double(2)`, `double(0)`, ...)

* Définir une fonction `double(x)` qui renvoie le double de la valeur `x` (int ou float) passée en paramètre ;

* définir une fonction `somme(a,b)` qui retourne la somme des deux valeurs (int ou float) passées en paramètre ;

* définir une fonction `bonjour(nom)` où nom est une chaîne de caractères qui **affiche** le texte `'Bonjour nom'` (on utilisera donc la fonction `print` et la fonction ne renverra rien (pas de `return` ici).

_Remarque_ : tester `somme("biba", "beloula")`
