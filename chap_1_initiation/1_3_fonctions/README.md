# Les fonctions (première approche)

---
* [Cours sur la fonction print](./3_0_la_fonction_print.md)
* Faire l'exercice 0 [du TD](./TD/1_3_TD_fonctions.md)

---

* [Les fonctions built-in et leur documentation](./1_3_cours_fonctions_built_in.md)
* Faire l'exercice 1 [du TD](./TD/1_3_TD_fonctions.md)


* [Les fonctions définies par l'utilisateur](./1_3_cours_fonctions_utilisateur.md)
* Faire les exercices 2 à 4 [du TD](./TD/1_3_TD_fonctions.md)


* [Importation de modules ou de bibliothèques](./1_3_cours_fonctions_modules.md)
* Faire les exercices 5 à 9 [du TD](./TD/1_3_TD_fonctions.md)


* Faire le [QCM : exo 10 TD](./TD/1_3_TD_fonctions.md)

* [Complément (revu plus tard) : Documenter sa fonction](./1_3_cours_fonctions_complements.md)

* [TD du chapitre](./TD/1_3_TD_fonctions.md)
* Corrigés dans le répertoire correspondant

* [Notebook du cours sur les fonctions](./1_3_cours_fonctions.ipynb) et la [version complète statique](./1_3_cours_fonctions.md)
---

* [Notebook du cours sur les fonctions](./1_3_cours_fonctions.ipynb) et la [version statique](./1_3_cours_fonctions.md)
* [TD du chapitre](./TD/1_3_TD_fonctions.md)
* Corrigés dans le répertoire correspondant
