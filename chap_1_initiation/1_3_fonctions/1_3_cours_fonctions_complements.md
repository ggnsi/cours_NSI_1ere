# Les fonctions

## Complements 1 : les commentaires

Vous allez être emmener à manipuler de très nombreuses fonctionnalités du langage Python et de ses modules, il est inenvisageable d'acquérir une connaissance exhaustive de toutes celles-ci.  
Dans les scripts que vous utiliserez vous pouvez déposer des commentaires à l'aide du symbole `#` pour expliquer certaines parties de codes ou certains choix.    
Ils ne seront pas interprétés et aideront le programmeur(vous) à mieux comprendre le code.

```python
>>> x = 3  # x est une variable correspondant à une abscisse
>>> x
3
```

## Complément 2 (revu plus tard) :  Générer la documentation associée à une fonction : le docstring

Cette notion sera reprise plus tard dans l'année mais pour ceux en avance, il peut être intéressant de lire ce paragraphe.

La **docstring** est une chaine de caractère encadrée par un couple de 3 guillements doubles \"\"\" et placée juste après l'en-tête de la fonction.


```python
def produit(a, b):
    """Renvoie le produit des nombres a et b
    
    param : a,b : int ou float
    return : a*b : int ou float"""
    
    return a * b
```

On y indique  :

- ce que fait la fonction ; 
- le type des paramètres ;
- le résultat retourné et son type.

On peut accéder à la documentation de la fonction :


```python
help(produit)
```

    Help on function produit in module __main__:
    
    produit(a, b)
        Renvoie le produit des nombres a et b
        param : a,b : int ou float
        return : a*b
    
    
