N'hésitez pas à revenir sur cette page en cas de doute ou pour vérifier une synthaxe.

# La fonction `print()`

La fonction `print()` permet d'afficher des données dans la console.

## Utilisation avec un seul paramètre

Testez si besoin par vous même.

```Python
>>> print("Bonjour")
Bonjour
>>>  print(8.5)
8.5
>>> a = 7
>>> print(a)
7
```

**A noter** : L'argument (c'est à dire la valeur écrite dans la parenthèse) est automatiquement convertie en chaine de caractère avant d'être affichée.

**A noter** : Par défaut, à l'affichage, un retour à la ligne est automatiquement ajouté à la fin de ligne.

### Complément :

Il est possible de concaténer des chaines de caractères en une seule chaine pour l'afficher dans la console. Dans ce cas, les variables qui ne sont pas des chaines de caractères devront être converties en chaines de caractères à l'aide de la fonction `str()` (erreur sinon).

```Python
>>> prenom = "Simone"
>>> age = 23
>>> print("Voici " + prenom + ", elle a " + str(age) + " ans !")
Voici Simone, elle a 23 ans
```

## Utilisation avec plusieurs paramètres (c'est à dire des valeurs à afficher séparées par des virgules)

Les valeurs passées en paramètres sont affichées les unes derrière les autres **séparées par défaut par un espace** (et si nécessaires sont automatiquement converties en chaines de caractères).

Ci-dessous une autre manière de réaliser le même affichage que juste avant (bien observer).

```Python
>>> prenom = "Simone"
>>> age = 23
>>> print("Voici", prenom, ", elle a", age, "ans !")
```

D'autres exemples :

```Python
>>> print("La", "température", "est", "de", 20, "°C")
Affiche La température est de 20 °C
```

```Python
>>> prenom = "Albert"
>>> age = 26
>>> print(prenom, "a", age, "ans !")
Affiche Albert a 26 ans !
```

On peut même inclure des opérations puisque l'expression sera évaluée avant d'être convertie.

```Python
>>> a = 5
>>> b = 4
>>> print("a - b vaut", a - b)
```

## Utilisation des paramètres optionnels `sep` et `end`

* Il est possible de choisir la chaine de caractère de séparation des valeurs :

```Python
print("a", "b", "c", "d", "e", sep="**")  #Affiche a**b**c**d**e
print("a", "b", "c", "d", "e", sep="")  #Affiche abcde
```

* Il est possible de choisir la chaine de caractère de fin d'affichage : 

```Python
print("Bonjour", end="")
print(" et à bientôt...")  #Affiche 'Bonjour et à bientôt' sur une seule ligne.
```

---
_Sources_ :
* documentation Python
* [site de M. Willm](http://nsi.ostralo.net)
