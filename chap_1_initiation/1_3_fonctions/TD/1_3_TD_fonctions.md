# TD Les fonctions

_Remarques_ :
* Créez un répertoire `chap_1_3` ou `1_3` dans votre dossier NSI ;
* Vous pouvez sauvegarder vos exercices dans des scripts différents dans ce répertoire (nommés par exemple : `1_3_exo_1.py`, `1_3_exo_2.py`, ...)

### Exercice n°0 : utilisation de la fonction `print()`

A chaque fois, vous pouvez copier/coller le code proposé dans un script, faire les 


1. Proposez **deux manières** de compléter la deuxième ligne afin que s'affiche `Tu as 5 ans` :

```Python
age = 5
print(...)
```

2. Proposez **deux manières** de compléter la quatrième ligne afin que s'affiche `Il fait beau` (attention aux espaces entre les mots). Inspirez vous des exemples du cours et adaptez-vous !

```Python
a = "Il"
b = "fait"
c = "beau"
print(...)
```

3. Compléter la troisième ligne afin que s'affiche `La somme de a et b vaut 12` (et que le résultat change si les valeurs de `a` et `b` sont modifiées.

```Python
a = 5
b = 7
print(...)
```

4. Compléter la troisième ligne afin que s'affiche `Pierre tu as 14 points` (et que le résultat change si les valeurs de `nom` et `score` sont modifiées.

```Python
nom = "Pierre"
score = 14
print(...)
```

5. Complétez la première ligne afin qu'en fin d'exécution soit affiché sur une seule ligne `Vive la NSI`. Obligation ici de garder les deux `print` comme imposé ci-dessous (aide : retourner dans le cours voir les options de la fonction `print()`).

```Python
print(...)
print("la NSI")
```

_Question complémentaire_ : et si on voulait `J'aime la NSI` ?


### Exercice n°1 : Fonctions natives et documentation ;

1. Faites apparaitre la documentation de la fonction `pow()`
     1. A quoi sert-elle ? Testez-la avec une exemple de votre choix pour le vérifier.
     2. Combien de paramètres au minimum doivent être utilisés ?  

2. Faites apparaitre la documentation de la fonction `abs()`  
     1. A quoi sert-elle ? Testez-la avec une exemple de votre choix pour le vérifier.  
     2. Combien de paramètre(s) doivent être utilisés ?


### Exercice n°2 : Quelques fonctions personnalisées ;

Pour chacun des cas suivants vous définirez correctement une fonction en n'oubliant pas de :

* choisir un nombre de paramètres adaptés
* nommer les fonctions et les paramètres de manière non ambigüe
* de tester votre fonction via la console

**Attention** : pour certaines fonctions on demande de **renvoyer** une valeur (donc utilisation de `return`) et pour d'autre **un affichage** (donc utilisation de `print()`).


1. Définir une fonction **renvoyant** le carré d'un nombre  
2. Définir une fonction **renvoyant** le perimètre d'un rectangle  
3. Définir une fonction **renvoyant** la moyenne de 3 notes  
4. Définir une fonction `div_euclid(a,b)`, où `a` et `b` sont entiers et qui **affiche** la division euclidienne de `a` par `b`.

  _Exemple_ : `div_euclid(14,3)` affiche `14 = 4 * 3 + 2`
  
  
4. 🥇Définir une fonction retournant la moyenne pondérée de 2 notes

**Complément pour ceux qui ont fini le TD** : lire la partie sur la _docstring_ dans le cours et, pour chaque fonction, créer une docstring associée

### Exercice n°3 : Vitesse ;


L'energie cinétique d'un solide de masse `m` et de vitesse `v` est définie par :

![energie cinétique](./fig/energie_cinetique.png)

1. Créez la fonction `calcul_energie` en choisissant le nombre de paramètres adapté et des noms de paramètres explicites.

_Elements de vérification_ (à tester via la console) :

```python
>>> calcul_energie(3, 4)
24.0
>>> calcul_energie(5.2, 1.5)
5.8500000000000005
```

2. Modifiez votre fonction afin que la valeur retournée soir arrondie au dixième (rappel : utiliser la fonction `round` et reprenez la documentation associée si nécessaire)

_Elements de vérification_ (à tester via la console) :

```python
>>> calcul_energie(5.2, 1.5)
5.9
>>> calcul_energie(32.125, 4.75)
362.4
```

_Compléments si TD terminé_ : Inclure les docstring

### Exercice n°4 : année-lumière ;
> source de l'exercice : [cours info L1S1 FIL](http://www.fil.univ-lille1.fr/~L1S1Info/Doc/New/exercices2_fr.html#Exercice-1-(ann%C3%A9e-lumi%C3%A8re))

L'__année-lumière__ ( _al_ ) est définie comme la distance parcourue par un photon (ou plus simplement la lumière) dans le vide, en une année julienne (soit 365,25 jours, ou 31 557 600 secondes).  

1. Sachant que la vitesse de la lumière dans le vide est une constante fixée à 299 792 458 m/s, calculez à combien de kilomètres correspondent une année-lumière à l'aide d'instructions Python **effectuées dans le shell**.

2. Écrivez une fonction `km_to_al(dist_km)` qui **renvoie** la valeur en année-lumières d'une distance donnée en kilomètres. `dist_km` sera un entier ou un flottant.

3. Écrivez une fonction `al_to_km(dist_al)` qui réalise la conversion inverse et **renvoie** le résultat.

4. Écrivez une fonction `al_to_km2(dist_al)` qui réalise la conversion _al_ en _km_ et **affiche** une phrase type "... al correspondent à ... km".

5. Sachant que Proxima Centauri, l'étoile la plus proche du Système solaire, se trouve à 4,22 années-lumière de la Terre, à combien de km de la Terre cette étoile se trouve-t'elle ?

---

UTILISATION DE MODULES

---
### Exercice n°5 :

**But** : travailler ici sur les deux manières d'importer des fonctions d'un module.  
Regardez bien les syntaxes utilisées pour savoir quelle première ligne vous devez mettre.

**Consigne** : Dans chacun des cas suivants, que mettre à la place des pointillés ?

    a. 
    
```Python
....

quantite = math.floor(9.4)
```
    b.
    
```Python
....
quantite = floor(9.4)
    
```

    c.
    
```Python
.....
quantite = 2 * pi
```
    
    d.
    
```Python
.....
quantite = random.randint(2,8)
```


### Exercice n°6 : Théorème de Pythagore ;
  
1. Définissez la fonction `hypotenuse(a,b)`, où `a` et `b` sont deux paramètres entiers correspondant aux longueurs des 2 plus petits côtés d'un triangle rectangle (exprimées avec la même unité) et qui retourne la longueur de l'hypothénuse de ce triangle.

_Elements de vérification_ :

```python
>>> hypotenuse(2, 3)
3.605551275463989
>>> hypotenuse(5, 2)
5.385164807134504
```

2. Le module math possède une fonction nommée `hypot`.  
**Via le Shell** importez-la, puis faites apparaitre sa documentation.  

### Exercice n°7 : Autour du cercle ;

1. Définissez la fonction `perimetre(rayon)` qui retourne le périmètre d'un cercle de rayon `rayon`.  
  
  _Aide_ : Vous utiliserez la constante `pi` de la bibliothèque `math`

  _Elements de vérification_ :

```python
>>> perimetre(2)
12.566370614359172
>>> perimetre(100)
628.3185307179587
```

2. Toujours dans le même script, définissez la fonction `aire(rayon)` qui retourne l'aire du disque de rayon `rayon`. 

  _Elements de vérification_ :
```python
>>> aire(1)
3.141592653589793
>>> aire(5)
78.53981633974483
```


### Exercice n°8 :  Une fonction peut appeler une autre fonction ;

Une fonction peut appeler d'autres fonctions :  
1. Définir une fonction `cube(x)` retournant le cube du nombre `x` passé en paramètre

2. Définir une fonction `volume_sphere(rayon)` qui retourne le volume d'une sphère de rayon `rayon`.  
Vous **ré-utilisez** la fonction `cube` définie précédemment dans le corps de la fonction `volume_sphere`.


### Exercice n°9 : Lancer de dés ;

1. Beaucoup de jeu de rôles (JDR) utilisent des dés spéciaux avec un nombre de faces adapté. 

![Dé 20 faces](./fig/20-sided_dice_250.jpg)[^2]


Créez une fonction `de_20()` sans paramètre simulant le lancer d'un dé à 20 faces (les faces étant numérotées de 1 à 20).

_rappel_ : lors de la définition d'une fonction sans paramètre les parenthèses sont tout de même obligatoires:
```python
def fonction_sans_parametre():
......
```

__2)__ Créez une fonction `lancer_3_de_20()` (sans paramètre également) qui **renvoie** la somme des résultats obtenus lorsque l'on lance 3 fois de suite un dé de 20.  
__Vous utiliserez bien évidemment la fonction `de_20()` dans la fonction `lancer_3_de_20()`__.  


### Exercice n° 10 : QCM
Indiquez à chaque fois __la bonne réponse__ (l'utilisation de l'ordinateur est interdit):

__Question 1__  
On considère la fonction suivante :
```python
def fonction_mystere(a, b):
    return a // b
```
Quelle est la valeur de retour pour `fonction_mystere(4, 2)`?  
* [ ] 0    
* [ ] 2.0   
* [ ] 4  
* [ ] 2  


__Question 2__  
On considère la fonction suivante :
```python
def fonction_mystere_bis(a):
    b = 3
    return b + a / 3
```
Quelle est la valeur de retour pour `fonction_mystere_bis(9)`?  
* [ ] 4   
* [ ] 4.0   
* [ ] 6.0  
* [ ] 6  


__Question 3__  
On considère le code suivant :
```python
from math import sqrt

def pythagore(a, b):
    """ Renvoie la longueur de l'hypoténuse"""
    return nom_fonction(a * a + b * b)
```

Par quoi remplacer `nom_fonction` ?  
* [ ] math.sqrt    
* [ ] sqrt   
* [ ] sqrt.math  
* [ ] math  


__Question 4__  
On considère la fonction `f()` suivante:
```python
def f(a, b, c, d):
    a = b
    c = d
    return b ** b + c * d
```
Quelle est la valeur de retour pour `f(3, 2, 1, 4)`?  
* [ ] 20    
* [ ] 28   
* [ ] 8  
* [ ] 9  


---
_Sources_

[^1]: [Light year by Bob King](http://www.skyandtelescope.com/observing/how-to-see-the-farthest-thing-you-can-see090920150909/)

[^2]: [20-sided dice, Photo by Fantasy on the English Wikipedia project](https://commons.wikimedia.org/wiki/File:20-sided_dice_250.jpg?uselang=fr)
