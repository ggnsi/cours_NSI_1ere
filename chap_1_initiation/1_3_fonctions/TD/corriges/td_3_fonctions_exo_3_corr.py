def calcul_energie(masse, vitesse):
    return 0.5 * masse * vitesse ** 2

def calcul_energie2(masse, vitesse):
    return round(0.5 * masse * vitesse ** 2, 1)