# A écrire dans le shell comme le demande l'énoncé
#
# 31557600 * 299792458 / 1000
#
# réponse obtenue : 9460730472580.8

def km_to_al(dist_km):
    return dist_km / 9460730472580.8

def al_to_km(dist_al):
    return dist_al * 9460730472580.8

def al_to_km2(dist_al):
    dist_km = dist_al * 9460730472580.8
    print(dist_al, "al correspondent à", dist_km, "km")

# Question 5 :
# on utilise la fonction al_to_km depuis la console :
#
# >>> al_to_km(4.22)
# 39924282594290.98
# environ : 39924 milliards de km