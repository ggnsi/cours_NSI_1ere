### Exercice n° 9 : QCM
Indiquez à chaque fois __la bonne réponse__ (l'utilisation de l'ordinateur est interdit):

__Question 1__  
On considère la fonction suivante :
```python
def fonction_mystere(a, b):
    return a // b
```
Quelle est la valeur de retour pour `fonction_mystere(4, 2)`?

Réponse : 2  

_En effet, `a // b` renvoie le quotient dans la division euclidienne de a par b_


__Question 2__  
On considère la fonction suivante :
```python
def fonction_mystere_bis(a):
    b = 3
    return b + a / 3
```
Quelle est la valeur de retour pour `fonction_mystere_bis(9)`? 

Réponse : 6.0  

_En effet, : 3 + 9 / 3 = 3 + 3.0 = 6.0_

__Question 3__  
On considère le code suivant :
```python
from math import sqrt

def pythagore(a, b):
    """ Renvoie la longueur de l'hypoténuse"""
    return nom_fonction(a * a + b * b)
```

Par quoi remplacer `nom_fonction` ?  

Réponse : sqrt   

_En effet, la fonction a bien été importée en mémoire_


__Question 4__  
On considère la fonction `f()` suivante:
```python
def f(a, b, c, d):
    a = b
    c = d
    return b ** b + c * d
```
Quelle est la valeur de retour pour `f(3, 2, 1, 4)`?  
Réponse : 20

_En effet, b = 2, et c = d = 4. Donc le calcul effectué est `2 **2 + 4 **2`, soit `20`._





```python

```
