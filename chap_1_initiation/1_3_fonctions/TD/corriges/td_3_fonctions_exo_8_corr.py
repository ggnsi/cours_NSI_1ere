import math

def cube(valeur):
    
    return valeur ** 3


def volume_sphere(rayon) :
    
    return 4 / 3 * math.pi * cube(rayon)

