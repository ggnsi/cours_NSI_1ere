import math

def hypothenuse(cote_1, cote_2):
    hypoth = math.sqrt(cote_1 ** 2 + cote_2 **2)
    return hypoth

# ou :

from math import sqrt

def hypothenuse(cote_1, cote_2):
    hypoth = sqrt(cote_1 ** 2 + cote_2 **2)
    return hypoth


