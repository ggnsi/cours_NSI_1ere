# Les fonctions

## III. Importer des fonctions : les modules

Les __modules__ sont des __scripts__ Python qui contiennent notamment un ensemble de fonctions que l'on est amené à réutiliser souvent (on les appelle aussi bibliothèques ou librairies). 

L'installation de Python fournit en plus des fonctions natives (module `builtins`) une bibliothéque standard contenant plusieurs modules pouvant répondre à des besoins divers :

* le module `math` contenant des fonctions mathématiques courantes
* le module `random` utilisant des nombres aléatoires
* le module `Tkinter` permettant de réaliser des interfaces graphiques
* le module `turtle` permettant de dessiner
et bien d'autres .....

Toutes ces fonctionnalités n'étant pas utiles dans tous les programmes, les modules correspondants ne sont donc pas chargés au démarrage. 
Il faut le réaliser manuellement.

## Importer un module / importer certaines fonctions d'un module

On peut importer globalement un module ou n'importer que quelques fonctions (ou constantes) d'un module

### Importer un module complet globalement

> On importe le module (en **début de script**) en utilisant la commande `import module_truc`.  
> L'utilisation d'une fonction (ou constante) se fait alors avec d'une **notation pointée** : `module_truc.fonction(...)`

_Exemple_ :

>```Python
>import math # on importe le module math
>rac = math.sqrt(9) # on utilise la fonction sqrt (racine carrée) du module math
>```

```Python
rayon = 5
surface_disque = math.pi * rayon ** 2 #on utilise la constante pi du module math
```

### Importer certaines fonctions (ou constantes) d'un module

> Si on ne souhaite importer **QUE** la fonction `sqrt` du module `math`, on écrira :
>
>```Python
>from math import sqrt
>```

L'utilisation de la fonction se fait alors directement (**sans** notation pointée) :

```Python
from math import sqrt
rac = sqrt(9)
```

On peut importer plusieurs fonctions d'un coup :

```Python
from math import sqrt, pi, cos
```

On peut importer d'un coup **toutes** les fonctions d'un module mais cela peut encombrer inutilement la mémoire si on en utilise que quelques une :

```Python
from math import *
```


### Le module math 


Quelques fonctions définies dans le module `math` :

| | |
|:--:|:--:|
|`sin(x)`, `cos(x)`, ...| Les fonctions de trigonométrie en radian|
|`floor(x)` et `ceil(x)`| arrondis de `x` par défaut et par excès|
|`pi` et `e` | Les constantes `pi` et `e`|
|`sqrt(x)`| La racine carrée de `x`|

_Remarque_ :
* `dir(math)` va lister toutes les fonctions du module
* `help(math)` affichera toutes les fonctions ainsi que les aides correspondantes


## Le module random

Ce module regroupe toutes les fonctions liées à la génération de nombres aléatoires, la documentation est disponible [ici](https://docs.python.org/fr/3/library/random.html?highlight=random#module-random)

Quelques fonctions définies dans le module `random` :

| | |
|:--:|:--:|
| `randint(a,b)` | Renvoie un nombre entier aléatoire entre a et b inclus |
| `random()` | Renvoie un nombre à virgule aléatoire entre 0 inclu et 1 exclu|

_Exemple_ : `randint(1, 6)` permet de simuler le lancer d'un dé à 6 faces.

