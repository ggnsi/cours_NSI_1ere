## I. La fonction `input()`

>La fonction `input()` permet à l'utilisateur d'__entrer un donnée__. Le traitement du programme est **arrêté** tant que la donnée n'a pas été entrée.

__Attention__ : la donnée entrée par l'utilisateur est **renvoyée sous forme de chaîne de caractères**.  
Des conversions peuvent donc être nécessaires pour utiliser la donnée.

_Exemple_ : 


```python
a = input()
```

    bonjour
    


```python
a
```




    'bonjour'



Si une chaîne de caractère est mise en argument, elle s'affiche avant d'attendre l'entrée de l'utilisateur.


```python
nom = input('Quel est votre nom : ')
```

    Quel est votre nom : Toto
    


```python
nom
```




    'Toto'



> **Attention** : Ne pas oublier qu'`input()` retourne **une chaîne de caractères** (c'est une erreur classique).
>
> Vous devrez peut être convertir la valeur renvoyée avant de l'utiliser dans la suite de votre programme.

_Exemple de code incorrect_ :


```python
age = input('Quel est votre age : ')
age = age + 3
```

    Quel est votre age : 5
    


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-6-1a61383bc020> in <module>
          1 age = input('Quel est votre age : ')
    ----> 2 age = age + 3
    

    TypeError: can only concatenate str (not "int") to str


Une erreur est générée car `age` est de type `str` et il n'est pas possible d'ajouter une chaîne de caractère et un entier.

Il faudra d'abord ici convertir la chaîne de caractères en nombre :

* soit entier en utilisant la fonction `int()` ;
* soit en flottant en utilisant la fonction `float()`.

_Le code corrigé_ : 
> on convertir le résultat renvoyé par `input` à l'aide de `int` : `int(input(...))`


```python
age = int(input('Quel est votre age : '))
age = age + 3
print(age)
```
