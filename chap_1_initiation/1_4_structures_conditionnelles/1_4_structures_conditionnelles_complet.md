# Structures conditionnelles, algèbre de Boole

## I. La fonction `input()`

>La fonction `input()` permet à l'utilisateur d'__entrer un donnée__. Le traitement du programme est **arrêté** tant que la donnée n'a pas été entrée.

__Attention__ : la donnée entrée par l'utilisateur est **renvoyée sous forme de chaîne de caractères**.  
Des conversions peuvent donc être nécessaires pour utiliser la donnée.

_Exemple_ : 


```python
a = input()
```

    bonjour
    


```python
a
```




    'bonjour'



Si une chaîne de caractère est mise en argument, elle s'affiche avant d'attendre l'entrée de l'utilisateur.


```python
nom = input('Quel est votre nom : ')
```

    Quel est votre nom : Toto
    


```python
nom
```




    'Toto'



> **Attention** : Ne pas oublier qu'`input()` retourne **une chaîne de caractères** (c'est une erreur classique).
>
> Vous devrez peut être convertir la valeur renvoyée avant de l'utiliser dans la suite de votre programme.

_Exemple de code incorrect_ :


```python
age = input('Quel est votre age : ')
age = age + 3
```

    Quel est votre age : 5
    


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-6-1a61383bc020> in <module>
          1 age = input('Quel est votre age : ')
    ----> 2 age = age + 3
    

    TypeError: can only concatenate str (not "int") to str


Une erreur est générée car `age` est de type `str` et il n'est pas possible d'ajouter une chaîne de caractère et un entier.

Il faudra d'abord ici convertir la chaîne de caractères en nombre :

* soit entier en utilisant la fonction `int()` ;
* soit en flottant en utilisant la fonction `float()`.

_Le code corrigé_ : 
> on convertir le résultat renvoyé par `input` à l'aide de `int` : `int(input(...))`


```python
age = int(input('Quel est votre age : '))
age = age + 3
print(age)
```

## II. Structure conditionnelles

### 1. Structure du SI (`if`) "seul"

Synthaxe :
> ```Python
if condition:
    bloc à exécuter
```

>Si l'évaluation de la condition écrite après `if` donne `True` (cad si condition est `Vraie`), alors le bloc d'instruction suivant est exécuté.  
Sinon il ne l'est pas et le script continue.


```python
age = 19
if age >= 18:
    print('vous êtes majeur.')
```


```python
age = 15
if age >= 18:
    print('vous êtes majeur.')
```

On fera bien attention à l'_indentation_ définissant le bloc d'instruction à exécuter ou pas.



```python
age = 19
if age >= 18:
    print('vous êtes majeur.')
    print('votre âge est', age)
```


```python
age = 15
if age >= 18:
    print('vous êtes majeur.')
    print('votre âge est', age)
```


```python
age = 15
if age >= 18:
    print('vous êtes majeur.')
print('votre âge est', age)
```

### 2. Structure du SI ... SINON (`if` `else`)

_Syntaxe_ :

>
```Python
if condition:
    bloc_1
else:
    bloc_2

suite_du_programme
```

* Si `condition` est vraie, alors on exécute le `bloc_1` puis éventuellement `suite_du_programme` ;
* Sinon, on exécute `bloc_2` puis éventuellement `suite_du_programme`.


```python
age = 15
if age >= 18:
    print('vous êtes majeur.')
else:
    print('vous êtes mineur')
```

Il n'y a pas de condition à inscrire après `else`.

### 3. Structure du SI ... SINON-SI ... SINON (`if` `elif` `else`)

Si l'on doit procéder à des tester à des tests successifs pour lequelles au maximum un seul bloc d'instructions sera traité, on utilise cette fonction

Exemple :



```python
choix = int(input('Quel est votre choix (entre 1 et 3): '))
if choix == 1:
    print('votre choix est le 1')
elif choix == 2:
    print('votre choix est le 2')
elif choix == 3:
    print('votre choix est le 3')
else:
    print("vous n'avez pas respecté la consigne")
```

_Remarque_ : le `else` est faculatatif.

Les structures conditionnelles précédentes peuvent être combinées entre elles et répétées.

### 4. Opérateurs de comparaison sur les nombres

`<`, `>`, `>=`, `<=` ne nécessitent pas d'explication.

* `==` permet de tester si les deux nombres sont égaux (à n'utiliser qu'avec des entiers);
* `!=` permet de tester si les deux nombres sont différents (à n'utiliser qu'avec des entiers).

_Remarque_ : ces opérateurs permettent aussi de comparer des chaînes de caractères (faire une recherche sur _ordre lexicographique_ )

_Exemples_ :


```python
'a' < 'c'
```




    True




```python
'bon' < 'bot'
```




    True




```python
'a' < 'A'
```




    False



### 5. Des tests parfois inutiles dans certaines fonctions

> Vocabulaire : On appelle __prédicat__ un fonction qui renvoie un booléen.

Supposons que l'on veuille écrire un _prédicat_ `est_majeur(age)` qui renvoie `True` si c'est le cas et `False` sinon.

Une **mauvaise** manière de faire (mais qui fonctionne) est :



```python
def est_majeur(age):
    if age >= 18:
        return True
    else:
        return False
```


```python
est_majeur(15)
```


```python
est_majeur(19)
```

Cependant, la structure conditionnelle est inutile.

En effet, il suffit de retourner le résultat du test `age >= 18` :



```python
def est_majeur(age):
    return (age >= 18)  # Bonne manière de faire ... et pluslégère
```


```python
est_majeur(19)
```


```python
est_majeur(15)
```
