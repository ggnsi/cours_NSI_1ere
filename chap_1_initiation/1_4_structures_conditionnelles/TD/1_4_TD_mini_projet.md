 # TD structures conditionnelles : mini-projet
 
---

## Projet
---

### Exercice 1

On souhaite pouvoir jouer contre l'ordinateur à une partie de pierre, feuille, ciseaux.
Programmez une fonction `chifoumi()` qui :

* demande à l'utilisateur son choix (on utilisera donc la fonction `input()` avec un texte affiché précisant les possibilités attentues (qui seront `p`, `f` et `c`).  
On supposera que l'utilisateur entre bien l'une de ces trois lettres et rien d'autre (on ne cherchera pas ici à tester les éventuelles erreurs d'entrées)
* choisit pour l'ordinateur au hasard une des trois possibilités ;
* affichera en fonction de la situation obtenue `égalité`, `Le joueur gagne` ou `Le programme gagne` ainsi qu'en dessous les choix effectués par chacun (utilisateur et ordinateur) ;
* ne retourne rien.

## pour continuer


### Exercice 

Une séance de cinéma est interdite aux moins de 16 ans.

Le prix du billet varie avec l’age : les seniors (plus de 65 ans) et les mineurs (moins de 18 ans) paient un tarif réduit, les autres un tarif plein.

Écrire un programme qui demande à un utilisateur de saisir son age et qui lui donne une réponse quant à son autorisation de voir le film ainsi que le tarif du billet.

Exemple : si l’utilisateur saisit 22, le programme doit afficher "Vous pouvez-voir ce film, le billet est au plein tarif." 

### Exercice  : Machine à sous  

![machine à sous](./fig/machine_sous.jpg)[^1]  

Il existe différents modèles de machines à sous, aussi appelées bandits manchots.   
Un modèle simple consiste en 3 rouleaux comportant 5 symboles (représentés ici par les chiffres de 1 à 5). 	
Les combinaisons gagnantes et les gains correspondants sont indiqués ci-dessous :  

|Combinaisons|gain(en euros)|  
|:----:|:---:|  
|1 1 1|300|  
|2 2 2 |100|  
|3 3 3 |70|  
|4 4 4|30|  
|5 5 5 |10|  
|deux 1 parmi les 3 rouleaux|5|  
|un seul 1 parmi les 3 rouleaux|2|   
  

__Définir une fonction `calcul_gain(r1, r2, r3)` acceptant trois paramètres entiers correspondant aux symboles sur les rouleaux et dont la valeur de retour est la somme gagnée.__  



