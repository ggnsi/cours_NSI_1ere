def tarif(age_1, age_2):
    
    if age_1 < 18:  # la première est mineure
        if age_2 < 18: # la deuxième aussi
           return 6
        else:        # sinon : une mineure et une majeure
            return 7
    else:            # la première est majeure
        if age_2 < 18: # la deuxième est mineure
            return 7
        else:          # les deux sont majeures
            return 9
