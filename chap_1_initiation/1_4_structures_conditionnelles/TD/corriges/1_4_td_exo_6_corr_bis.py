def de_plus(heure, minute):
    
    minute = minute + 1
    if minute == 60:
        minute = 0
        heure = heure + 1
        if heure == 24:
            heure = 0
    print("Il est", heure, "heures et", minute, "minutes")

#ou

def de_plus_bis(heure, minute):
    if minute != 59:
        minute +=1
    else:
        minute = 0
        if heure !=23:
            heure +=1
        else:
            heure = 0
    print("Il est", heure, "heures et", minute, "minutes")

#ou
            
def de_plus_ter(heure, minute):
    if minute != 59:
        minute +=1
    elif heure != 23:
        heure +=1
        minute = 0
    else:
        heure = 0
        minute = 0
    print("Il est", heure, "heures et", minute, "minutes")
