 # TD structures conditionnelles et operations booleennes
 
---

## Premiers exercices
---


### Exercice 1
 
Créez une fonction `permis(score)` où `score` est un entier positif, qui affiche `reçu` si le score est supérieur ou égal à 35 et `refusé` sinon.

### Exercice 2

Soit `f` la fonction définie sur R par morceaux par : 
* $`f(x) = -x+2`$ si  $`x <= 3`$ et
* $`f(x) = x^2+1`$ si $`x > 3`$.

Créez une fonction `image(nombre)` qui **renvoie** l'image de nombre par la fonction `f`.

_Elements de vérifiation_ :

```Python
>>> image(5)
26
>>> image(0)
2
>>> image(3)
-1
```

### Exercice 3

Un club sportif formant des jeunes de moins de 14 ans vous a demandé de créer une fonction pour informer les familles de la catégorie à laquelle appartiendra leur enfant en fonction de leur âge.

Le club sportif vous a donné ces informations :

    "Poussin" de 6 à 7 ans
    "Pupille" de 8 à 9 ans
    "Minime" de 10 à 11 ans
    "Cadet" après 12 ans

Écrire une fonction `categorie()` (sans paramètre) qui :

* **Demande** l’âge d’un enfant à l’utilisateur (donc utiliser la fonction `input()`),
* informe  l'utilisateur de la catégorie de l'enfant (en l'affichant),
* affiche "trop petit" si l'enfant ne peut être accueilli,
* ne renvoie rien.

### Exercice 4

1. Proposer une fonction, nommée `mini2(a, b)` qui prend comme arguments deux nombres entiers `a` et `b` et qui renvoie le minimum de ces deux nombres.


2. _Approfondissement_ (si le reste est terminé) : 
 
   Proposer une fonction, nommée `mini4(a, b, c, d)` qui :

 * prend comme arguments quatre nombres entiers `a`, `b`, `c` et `d`,
 * fait appel plusieurs fois à la fonction `mini2` pour finalement trouver le minimum des quatre nombres ;
 * renvoie le minimum trouvé.
 

### Exercice 5

Pour chaque cas, indiquez __quelle est la valeur de x__ après exécution du code fourni.  

Une __seule réponse est correcte__ (l'utilisation de l'ordinateur est interdit).

__Question 1__  

```python
x = 2
if x >= 2:
    x = x + 1
else:
    x = x - 1
```
Entourez la bonne réponse :  1 - 2  - 3 - 4

__Question 2__  

```python
x = 4
if x < 5:
    x = x % 2
else:
    x = 2
if x == 0:
    x = 1
else:
    x = 2
```
Entourez la bonne réponse :  1 - 2  - 3 - 4 

__Question 3__  

```python
x = 1
y = 2
if x < y and x > 2:
    x = 3
elif x > y or x >= 1:
    x = 2
else:
    x = 0
```
Entourez la bonne réponse :  0 - 1 - 2  - 3

__Question 4__  

```python
x = 0
if x < 2:
    x = x + 1
if x < 3:
    x = x * 4
if x < 4:
    x = x + 1
```
Entourez la bonne réponse :  0 - 1  - 4 - 5

### Exercice 6 : Pour une minute de plus...

Ecrire une fonction `de_plus(heure, minute)` qui affiche l’heure une minute après celle déterminée par les deux variables :

```python
>>> de_plus(10, 20)
Il est 10 heures et 21 minutes
>>> de_plus(10, 59)
Il est 11 heures et 0 minutes
>>> de_plus(23, 59)
Il est 0 heures et 0 minutes
```
**Attention** : Ne pas oublier le cas de minuit.

### Exercice 7 :

Un cinéma pratique trois types de tarifs pour deux personnes :

- si les deux personnes sont mineures, elles payent 6€ chacune
- si l'une seulement est mineure, elles payent un tarif de groupe de 14€
- si les deux personnes sont majeures, elles payent 18 € en tout

Écrire une fonction `tarif(age_1, age_2)` qui **renvoie** le prix à payer par chaque personne (elles partagent équitablement si besoin le tarif de groupe)

_Elements de vérifiation_ :

```Python
>>> tarif(12, 16)
6
>>> tarif(12, 19)
7
>>> tarif(21, 14)
7
>>> tarif(21, 25)
9
```

