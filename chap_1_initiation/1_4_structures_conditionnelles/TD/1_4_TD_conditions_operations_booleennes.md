 # TD structures conditionnelles et operations booleennes
 
---

## Premiers exercices
---


### Exercice 1
 
Créez une fonction `permis(score)` où `score` est un entier positif, qui affiche `reçu` si le score est supérieur ou égal à 35 et `refusé` sinon.

### Exercice 2

Soit `f` la fonction définie sur R par morceaux par : 
* `f(x) = -x+2` si  `x <= 3 et
* `f(x) = x^2+1` si `x > 3`.

Créez une fonction `image(nombre)` qui retourn l'image de nombre par la fonction `f`.

_Elements de vérifiation_ :

```Python
>>> image(5)
26
>>> image(0)
2
>>> image(3)
-1
```

### Exercice 3

Un club sportif formant des jeunes de moins de 14 ans vous a demandé de créer une fonction pour informer les familles de la catégorie à laquelle appartiendra leur enfant en fonction de leur âge.

Le club sportif vous a donné ces informations :

    "Poussin" de 6 à 7 ans
    "Pupille" de 8 à 9 ans
    "Minime" de 10 à 11 ans
    "Cadet" après 12 ans

Écrire une fonction `categorie()` qui :

* **Demande** l’âge d’un enfant à l’utilisateur (fonction `input()`,
* informe  l'utilisateur de la catégorie de l'enfant (en l'affichant),
* affiche "trop petit" si l'enfant ne peut être accueilli,
* ne renvoie rien.

### Exercice 4

1. Proposer une fonction, nommée `mini2(a, b)` qui prend comme arguments deux nombres entiers `a` et `b` et qui renvoie le minimum de ces deux nombres.


2. _Approfondissement_ (si le reste est terminé) : 
 
   Proposer une fonction, nommée `mini4(a, b, c, d)` qui :

 * prend comme arguments quatre nombres entiers `a`, `b`, `c` et `d`,
 * fait appel plusieurs fois à la fonction `mini2` pour finalement trouver le minimum des quatre nombres ;
 * renvoie le minimum trouvé.
 

### Exercice 5

Pour chaque cas, indiquez __quelle est la valeur de x__ après exécution du code fourni.  

Une __seule réponse est correcte__ (l'utilisation de l'ordinateur est interdit).

__Question 1__  

```python
x = 2
if x >= 2:
    x = x + 1
else:
    x = x - 1
```
Entourez la bonne réponse :  1 - 2  - 3 - 4

__Question 2__  

```python
x = 4
if x < 5:
    x = x % 2
else:
    x = 2
if x == 0:
    x = 1
else:
    x = 2
```
Entourez la bonne réponse :  1 - 2  - 3 - 4 

__Question 3__  

```python
x = 1
y = 2
if x < y and x > 2:
    x = 3
elif x > y or x >= 1:
    x = 2
else:
    x = 0
```
Entourez la bonne réponse :  0 - 1 - 2  - 3

__Question 4__  

```python
x = 0
if x < 2:
    x = x + 1
if x < 3:
    x = x * 4
if x < 4:
    x = x + 1
```
Entourez la bonne réponse :  0 - 1  - 4 - 5

### Exercice 6 : Pour une minute de plus...

Ecrire une fonction `de_plus(heure, minute)` qui affiche l’heure une minute après celle déterminée par les deux variables :

```python
>>> de_plus(10, 20)
Il est 10 heures et 21 minutes
>>> de_plus(10, 59)
Il est 11 heures et 0 minutes
>>> de_plus(11, 59)
Il est 0 heures et 0 minutes
```
**Attention** : Ne pas oublier le cas de minuit.

### Exercice 7 :

Un cinéma pratique trois types de tarifs pour deux personnes :

- si les deux personnes sont mineures, elles payent 6€ chacune
- si l'une seulement est mineure, elles payent un tarif de groupe de 14€
- si les deux personnes sont majeures, elles payent 18 € en tout

Écrire une fonction `tarif(age_1, age_2)` qui **renvoie** le prix à payer par chaque personne (elles partagent équitablement si besoin le tarif de groupe)

_Elements de vérifiation_ :

```Python
>>> tarif(12, 16)
6
>>> tarif(12, 19)
7
>>> tarif(21, 14)
7
>>> tarif(21, 25)
9
```

---
## Expressions booléennes - tables de vérité
---

### Exercice 1

1. `P` et `Q` sont deux valeurs booléennes. Construire la table de vérité de `(NON P) ou Q`

2. Comment s'écrit en Python cette condition (en considérant que `P` et `Q` sont des variables)?

### Exercice 2

1. `x`, `y` et `z` sont trois valeus booléennes.  
Construire la table de vérité de l'expression `((NON x) ET y) OU (x ET z)`.

_indications_ :
* combien de triplets (x ,y, z) différents existe-il ?
* On pourra commencer dans le tableau par remplir les différentes colonnes : `NON x`, `(NON x) ET y` et `x ET z` avant de conclure.


2. Comment s'écrit en Python cette condition (en considérant que `x`, `y` et `z` sont des variables)?


### Exercice 3

`x`, `y` et `z` sont trois valeus booléennes.

Construire la table de vérité de l'expression `(x ou y) et ((non y) ou z)`

### Exercice 4

1. Démontrer que `NON (A ET B)` est équivalent à `(NON A) OU (NON B)`. Pour cela on réalisera les tables de vérité de chacune des expressions et on montrera qu'elles sont idendiques.

2. De la même manière, démontrer que `NON (A OU B)` est équivalent à `(NON A) ET (NON B)`.


### Exercice 5

**Sachant que l'expression `not(a or b)` a la valeur `True`, quelles peuvent être les valeurs des variables booléennes `a` et `b` ?**

**Réponses :**

A- True et True

B- False et True

C- True et False

D- False et False

### Exercice 6 : (plus dur) si le reste est fini

Trouver à l’aide des opérateurs booléens l’expression de la fonction `ssi (x,y)` dont on donne la table de vérité ci-dessous.

| x | y | ssi(x,y) |
|:---:|:---:|:---:|
| 0 | 0 | 1 | 
| 0 | 1 | 0 |
| 1 | 0 | 0 | 
| 1 | 1 | 1 |

---
## Expressions booléennes
---

### Exercice 1

Evaluez les expressions suivantes et expliquez leur résultat.

**Attention** à **être fainéants** comme le sont les opérateurs Python

|Expression|Evaluation &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;|  
|:---------|:--------:| 
| 15 <= 20 or 1 > 150   |          |           
| 2 < 4  and 2 < 3 |          |           
| "A" == "A" and "B"=="B"  |          |            
| not (1 < 3)|          |           
| not (15 <= 20) or (1 < 150)  |          |           
| not ((15 <= 20) or (1 < 150))  |          |            
| (3 < 5) and ((7 < 5) or (2 < 3))  |          |           
| ((3 < 5) and (7 < 5)) or (2 < 3))  |          |           

### Exercice 2

On a saisi le code suivant :

```Python
a = 5
b = 10
c = (a > 50 and b == 10)
d = (a == 3 or not(b != 15))
print("c vaut", c,".  Et d vaut", d)
```

Qu'affiche ce programme ?

Réponses :

    c vaut True . Et d vaut True
    c vaut False . Et d vaut True
    c vaut True . Et d vaut False
    c vaut False . Et d vaut False


---
## Structures conditionnelles : prédicats
---


### Exercice 1

> Un __predicat__ est une fonction dont la valeur de retour est un booléen

__Ecrire les prédicats suivants et testez-les.__

__1)__   Définissez un prédicat `est_nul(entier)` renvoyant `True` si `entier` est égal à zero, `False` sinon.  

__2)__  Définissez un prédicat `est_obtus(mes_angle)` renvoyant `True` si l'angle dont on donne la mesure exprimée en degré est obtus, `False` sinon.  

__3)__  Définissez un prédicat `est_voyelle(lettre)` renvoyant `True` si le caractère passé en paramètre (en minuscule) est une voyelle, `False` sinon.  

__4)__  Sans utiliser d'opérateurs logiques (and, or, not ...) mais avec uniquement des suites d'instructions conditionnelles : définissez un prédicat `is_liquid_water(temperature)` renvoyant `True` si le nombre passé en paramètre est une température exprimée en °C à laquelle l'eau est liquide, `False` sinon(_on considérera une pression atmosphérique normale_).  

__5)__  Définissez un prédicat `is_even(entier)` renvoyant `True` si le nombre entier passé en paramètre est pair, `False` sinon. 

### Exercice 2

Depuis l'ajustement du calendrier grégorien en 1582, les années sont bisextilles si l'année est divisible par 4 et non divisible par 100 __ou encore__ si l'annnée est divisible par 400  

Définissez un prédicat `est_bisextille(annee)` renvoyant `True` si l'année passé en paramètre est une année bisextille  et `False` sinon.

**Contrainte** : La fonction **ne doit comporter aucun `if` : n'utiliser QUE des opérateurs booléens.**

Dans un premier temps, considérer que l'année est forcément supérieure à 1582.  
Dans un deuxième temps, ajouter cette contrainte dans votre fonction.

_Indications_ : Quelques années bisextilles à tester : 2012, 2016, 2020 et quelques années ne l'étant pas : 1900, 2100.


## Projet

### Exercice 1

On souhaite pouvoir jouer contre l'ordinateur à une partie de pierre, feuille, ciseaux.
Programmez une fonction `chifoumi()` qui :

* demande à l'utilisateur son choix (on utilisera `p`, `f`, `c`) ;
* choisira pour lui au hasard une des trois possibilités ;
* affichera `égalité`, `Le joueur gagne` ou `Le programme gagne` ainsi qu'en dessous les choix de chacun ;
* ne retourne rien.

## pour continuer


### Exercice 

Une séance de cinéma est interdite aux moins de 16 ans.

Le prix du billet varie avec l’age : les seniors (plus de 65 ans) et les mineurs (moins de 18 ans) paient un tarif réduit, les autres un tarif plein.

Écrire un programme qui demande à un utilisateur de saisir son age et qui lui donne une réponse quant à son autorisation de voir le film ainsi que le tarif du billet.

Exemple : si l’utilisateur saisit 22, le programme doit afficher "Vous pouvez-voir ce film, le billet est au plein tarif." 

### Exercice  : Machine à sous  

![machine à sous](./fig/machine_sous.jpg)[^1]  

Il existe différents modèles de machines à sous, aussi appelées bandits manchots.   
Un modèle simple consiste en 3 rouleaux comportant 5 symboles (représentés ici par les chiffres de 1 à 5). 	
Les combinaisons gagnantes et les gains correspondants sont indiqués ci-dessous :  

|Combinaisons|gain(en euros)|  
|:----:|:---:|  
|1 1 1|300|  
|2 2 2 |100|  
|3 3 3 |70|  
|4 4 4|30|  
|5 5 5 |10|  
|deux 1 parmi les 3 rouleaux|5|  
|un seul 1 parmi les 3 rouleaux|2|   
  

__Définir une fonction `calcul_gain(r1, r2, r3)` acceptant trois paramètres entiers correspondant aux symboles sur les rouleaux et dont la valeur de retour est la somme gagnée.__  



