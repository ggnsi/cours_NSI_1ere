 # TD prédicats
 
---
### Exercice 1

> Un __predicat__ est une fonction dont la valeur de retour est un booléen

__Ecrire les prédicats suivants et testez-les.__

__1)__   Définissez un prédicat `est_nul(entier)` renvoyant `True` si `entier` est égal à zero, `False` sinon.  

__2)__  Définissez un prédicat `est_obtus(mes_angle)` renvoyant `True` si l'angle dont on donne la mesure exprimée en degré est obtus, `False` sinon.  

__3)__  Définissez un prédicat `est_voyelle(lettre)` renvoyant `True` si le caractère passé en paramètre (en minuscule) est une voyelle, `False` sinon.  

__4)__  Sans utiliser d'opérateurs logiques (and, or, not ...) mais avec uniquement des suites d'instructions conditionnelles : définissez un prédicat `is_liquid_water(temperature)` renvoyant `True` si le nombre passé en paramètre est une température exprimée en °C à laquelle l'eau est liquide, `False` sinon(_on considérera une pression atmosphérique normale_).  

__5)__  Définissez un prédicat `is_even(entier)` renvoyant `True` si le nombre entier passé en paramètre est pair, `False` sinon. 

### Exercice 2

Depuis l'ajustement du calendrier grégorien en 1582, les années sont bisextilles si l'année est divisible par 4 et non divisible par 100 __ou encore__ si l'annnée est divisible par 400  

Définissez un prédicat `est_bisextille(annee)` renvoyant `True` si l'année passé en paramètre est une année bisextille  et `False` sinon.

**Contrainte** : La fonction **ne doit comporter aucun `if` : n'utiliser QUE des opérateurs booléens.**

Dans un premier temps, considérer que l'année est forcément supérieure à 1582.  
Dans un deuxième temps, ajouter cette contrainte dans votre fonction.

_Indications_ : Quelques années bisextilles à tester : 2012, 2016, 2020 et quelques années ne l'étant pas : 1900, 2100.

