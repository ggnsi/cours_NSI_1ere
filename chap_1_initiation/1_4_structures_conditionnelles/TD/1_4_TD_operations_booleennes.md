 # TD Expressions booléennes - tables de vérité
 
---

### Exercice 1

1. `P` et `Q` sont deux valeurs booléennes. Construire la table de vérité de `(NON P) ou Q`

2. Comment s'écrit en Python cette condition (en considérant que `P` et `Q` sont des variables)?

### Exercice 2

1. `x`, `y` et `z` sont trois valeus booléennes.  
Construire la table de vérité de l'expression `((NON x) ET y) OU (x ET z)`.

_indications_ :
* combien de triplets (x ,y, z) différents existe-il ?
* On pourra commencer dans le tableau par remplir les différentes colonnes : `NON x`, `(NON x) ET y` et `x ET z` avant de conclure.


2. Comment s'écrit en Python cette condition (en considérant que `x`, `y` et `z` sont des variables)?


### Exercice 3

`x`, `y` et `z` sont trois valeus booléennes.

Construire la table de vérité de l'expression `(x ou y) et ((non y) ou z)`

### Exercice 4

1. Démontrer que `NON (A ET B)` est équivalent à `(NON A) OU (NON B)`. Pour cela on réalisera les tables de vérité de chacune des expressions et on montrera qu'elles sont idendiques.

2. De la même manière, démontrer que `NON (A OU B)` est équivalent à `(NON A) ET (NON B)`.


### Exercice 5

**Sachant que l'expression `not(a or b)` a la valeur `True`, quelles peuvent être les valeurs des variables booléennes `a` et `b` ?**

**Réponses :**

A- True et True

B- False et True

C- True et False

D- False et False

### Exercice 6 : (plus dur) si le reste est fini

Trouver à l’aide des opérateurs booléens l’expression de la fonction `ssi (x,y)` dont on donne la table de vérité ci-dessous.

| x | y | ssi(x,y) |
|:---:|:---:|:---:|
| 0 | 0 | 1 | 
| 0 | 1 | 0 |
| 1 | 0 | 0 | 
| 1 | 1 | 1 |

---
## Expressions booléennes
---

### Exercice 7

Evaluez les expressions suivantes et expliquez leur résultat.

**Attention** à **être fainéants** comme le sont les opérateurs Python

|Expression|Evaluation &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;|  
|:---------|:--------:| 
| 15 <= 20 or 1 > 150   |          |           
| 2 < 4  and 2 < 3 |          |           
| "A" == "A" and "B"=="B"  |          |            
| not (1 < 3)|          |           
| not (15 <= 20) or (1 < 150)  |          |           
| not ((15 <= 20) or (1 < 150))  |          |            
| (3 < 5) and ((7 < 5) or (2 < 3))  |          |           
| ((3 < 5) and (7 < 5)) or (2 < 3))  |          |           

### Exercice 8

On a saisi le code suivant :

```Python
a = 5
b = 10
c = (a > 50 and b == 10)
d = (a == 3 or not(b != 15))
print("c vaut", c,".  Et d vaut", d)
```

Qu'affiche ce programme ?

Réponses :

    c vaut True . Et d vaut True
    c vaut False . Et d vaut True
    c vaut True . Et d vaut False
    c vaut False . Et d vaut False


