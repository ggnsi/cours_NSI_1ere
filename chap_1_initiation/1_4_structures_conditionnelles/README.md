# Chap 1_4 : Structures conditionnelles, opérateurs booléens

---

Objectifs :

* savoir choisir une structures conditionnelles adaptée et la rédiger correctement
* savoir écrire des prédicats
* connaître les définitions et les tables de vérité des opérateurs booléens
* savoir construire la table de vérité d'un expression booléenne
* connaître l'aspect séquentiel des opérateurs booléens
* savoir construire des expressions booléennes adaptées à la situation

----

* [Cours sur la fonction `input()`](./1_4_la_fonction_input.md)
* [Cours sur les structures conditionnelles](./1_4_structures_conditionnelles.md)


* Faire les exercices 1 à 7 du [TD structures conditionnelles](./TD/1_4_TD_structures_conditionnelles.md)


* [Cours sur les opérateurs booléens (poly)](./1_4_booleens_et_operateurs.pdf)
* [TD sur les opérateurs booléens](./TD/1_4_TD_operations_booleennes.md)


* [TD sur les prédicats](./TD/1_4_TD_predicats.md)


* [mini projet et exercices supplémentaires](./TD/1_4_TD_mini_projet.md)

-------

* [Notebook du début du cours](./1_4_structures_conditionnelles_boole.ipynb) et sa [version statique](./1_4_structures_conditionnelles_boole.md)
* [Poly de la suite du cours sur les opérateurs booléens](./1_4_booleens_et_operateurs.pdf)

* [TD](./TD/1_4_TD_conditions_operations_booleennes.md) et son corrigé