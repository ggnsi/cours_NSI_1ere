# Structures conditionnelles, algèbre de Boole

## II. Structure conditionnelles

### 1. Structure du SI (`if`) "seul"

Synthaxe :

> ```Python
>if condition:
>    bloc à exécuter
>```

> `condition` est évaluée.  
> Si `condition` équivaut à `True` (cad si condition est `Vraie`), alors le bloc d'instruction suivant est exécuté.  
Sinon il ne l'est pas et le script continue.

_Des exemples_ :


```python
age = 19
if age >= 18:
    print('vous êtes majeur.')
```


```python
age = 15
if age >= 18:
    print('vous êtes majeur.')
```

>  **Attention** à l'**indentation** définissant le bloc d'instruction à exécuter ou pas.
> C'est une cause classique d'erreur

Déterminez les affichages réalisés pour chacun des scripts suivants :


```python
age = 19
if age >= 18:
    print('vous êtes majeur.')
    print('votre âge est', age)
```


```python
age = 15
if age >= 18:
    print('vous êtes majeur.')
    print('votre âge est', age)
```


```python
age = 15
if age >= 18:
    print('vous êtes majeur.')
print('votre âge est', age)
```

### 2. Structure du SI ... SINON (`if` `else`)

_Syntaxe_ :

>
>```Python
>if condition:
>    bloc_1
>else:
>    bloc_2
>
>suite_du_programme
>```
>
>* Si `condition` est vraie, alors on exécute le `bloc_1` puis la `suite_du_programme` (si elle existe) ;
>* Sinon, on exécute `bloc_2` puis éventuellement `suite_du_programme` (si elle existe).

_Exemple_ :


```python
age = 15
if age >= 18:
    print('vous êtes majeur.')
else:
    print('vous êtes mineur')
```

> _Remarque_ : Il n'y a pas de condition à inscrire après `else`.

_Autre exemple_ (incluant un `and`)


```python
age = 19
sexe = "F"
if (age >= 18) and (sexe == 'F'):
    print('vous êtes une fille majeure.')
else:
    print('vous êtes une fille mineure')
```

### 3. Structure du SI ... SINON-SI ... SINON (`if` `elif` `else`)

Si l'on doit procéder à des tester à des tests successifs pour lequelles au maximum un seul bloc d'instructions sera traité, on utilise cette fonction

Exemple :



```python
choix = int(input('Quel est votre choix (entre 1 et 3): '))
if choix == 1:
    print('votre choix est le 1')
elif choix == 2:
    print('votre choix est le 2')
elif choix == 3:
    print('votre choix est le 3')
else:
    print("vous n'avez pas respecté la consigne")
```

> _Remarque_ : le `else` est facultatif. Il n'est exécuté que si aucune des conditions précédentes n'est réalisée.

> _Remarque_ : On peut bien sur **inclure une structure conditionnelle à l'intérieur d'une autre structure conditionnelle**

### 4. Opérateurs de comparaison

**Sur les nombres** : 

* `<`, `>`, `>=`, `<=` ne nécessitent pas d'explication.
* `==` permet de tester si les deux nombres sont égaux ;
* `!=` permet de tester si les deux nombres sont différents.

**Remarque 1** :

Il est souvnet **dangereux** d'utiliser `==` et `!=` sur des flottants (à cause de leur représentation en mémoire qui peut être une approximation du nombre de départ)

**Remarque 2** : ces opérateurs permettent aussi de **comparer des chaînes de caractères** (faire une recherche sur _ordre lexicographique_ )

_Exemples_ :

L'égalité de deux chaines :


```python
nom = "toto"
nom == "toto"
```




    True




```python
nom == "Toto"
```




    False



> La comparaison de deux chaînes utilise l'**ordre lexicographique**.

Quelques exemples :


```python
'a' < 'c'
```




    True




```python
'a' < 'A'
```




    False




```python
'bon' < 'bot'
```




    True




```python
'bonnet' < 'bot'
```




    True



**Exemple d'utilisation** : 

Tester si un utilisateur a bien entré une lettre minuscule (j'anticipe encore ici en utilisant le `and` mais ça se comprend bien):


```python
choix_utilisateur = 'A'
if (choix_utilisateur >=  'a') and (choix_utilisateur <=  'z'):
    .....
else:
    print("vous n'avez pas respecté la consigne")
    ...
```

### 5. Des tests parfois inutiles dans certaines fonctions

> Vocabulaire : On appelle __prédicat__ un fonction qui **renvoie un booléen**.

Supposons que l'on veuille écrire un _prédicat_ `est_majeur(age)` qui renvoie `True` si c'est le cas et `False` sinon.

Une **mauvaise** manière de faire (mais qui fonctionne) est :



```python
def est_majeur(age):
    if age >= 18:
        return True
    else:
        return False
```


```python
est_majeur(15)
```


```python
est_majeur(19)
```

Cependant, la structure conditionnelle est inutile.

En effet, il suffit de retourner le résultat du test `age >= 18` :



```python
def est_majeur(age):
    return (age >= 18)  # Bonne manière de faire ... et pluslégère
```


```python
est_majeur(19)
```


```python
est_majeur(15)
```
