# TD : variables et affectation

### _Exercice 1_

__1)__ Pour chacune des lignes de code suivantes indiquez l’erreur commise


```python
>>> moyenne eleve = 10 
```

Il ne peut y avoir d'espace dans un nom de variable.

Mettre par exemple `moyenne_eleve`


```python
>>> 12 = moyenne  
```

* moyenne est une variable qui n'existe pas
* 12 n'est pas un nom de variable possible (il commence par un chiffre)


```python
>>> a = 3 + s  
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-4-645c30dfa2d0> in <module>
    ----> 1 a = 3 + s
    

    NameError: name 's' is not defined


`s` est une variable qui n'a pas été définie auparavant. Il n'est donc pas possible de l'évaluer


```python
>>> y - 1 = 8
```

`y - 1` n'est pas un nom de variable.

Python l'interprète comme un opérateur (la soustraction) et il n'est pas possible d'affecter un nombre à un opérateur


```python
>>> if = 50  
```

`if` est un mot réservé du langage Python. On ne peut l'utiliser comme nom de variable

### _Exercice 2_

On considère l'algorithme suivant. 

1. En utilisant un tableau d'état des variables, faire fonctionner l'algorithme suivant avec $x=3$ puis avec $x=4$.
2. Exprimer en fonction de $x$ la valeur contenue dans la variable $b$ en fin d'éxécution.

![algo](./fig/1_2_td_fig_1.jpg)

|a|b|x|
|:--:|:--:|:--:|
|10|17|3|

### _Exercice 3_

On considère le code ci-dessous.

1. En réalisant un tableau d'état des variables, déterminer les valeurs contenues dans les variables a et b en fin d'éxécution :


```python
a = 5
b = 3
b = a + 2b
a = b - a
b = a - b
```

|a|b|
|:--:|:--:|
|5|3|
|6|11|
| |-5|

2. Exprimer en fonction des valeurs initiales a et b de départ le contenu des variables a et b en fin d'éxécution

|etape|a|b|
|:--:|:--:|:--:|
|1|a|a + 2b|
|2|a + 2b - a (= 2b)|a + 2b|
|3|2b|2b - (a + 2b) = - a |

### _Exercice 4_

Déterminer le type et la valeur de chaque variable à l'issue de l'exécution du programme :

1. ```Python
a = 8
b = 4 * 2
c = a == b
```

2. ```Python
a = 5
b = 12
r1 = a + b
r2 = "a" + "b"
r3 = str(a) + str(b)
```

1. `a` contient 8, de type `int`  
   `b` contient 8, de type `int`  
   `c` contient le booléen `True`
 
2. `r1` contient 17, de type `int`  
    `r2` contient `"ab"`, de type `str`  
    `r3` contient également `"ab"` de type `str`

### _Exercice  5_

On considère le code ci-dessous.

1. En réalisant un tableau d'état des variables, déterminer les valeurs contenues dans les variables a et b en fin d'éxécution :


```python
a = 2
b = -1
a += 5
b *=4
a += a + b
b -= a - b
```

Traduction sans l'écriture condensée :


```python
a = 2
b = -1
a = a + 5
b = b * 4
a = a + (a + b)
b = b - (a - b)
```

|a|b|
|:--:|:--:|
|2|-1|
|7|-4|
|10|-18|

### _Exercice 6_ 

Quel est le rôle de l'algorithme suivant (réaliser un tableau d'état des variables) ?

![algo](./fig/1_2_td_fig_2.jpg)

<!-- $x \leftarrow x - y$

$y \leftarrow x + y$

$x \leftarrow y - x$ -->

|x|y|
|:--:|:--:|
|x - y|y|
|x - y|x - y + y = x|
|x - (x - y) = y| x|

On échange les valeurs contenues dans les variables `x` et `y`.

### _Exercice 7_ : méthode à connaître

Quel est le rôle de l'algorithme suivant (deux variables $a$ et $b$ étant précédemment définies) :

![algo](./fig/1_2_td_fig_3.jpg)

<!-- $c \leftarrow a$

$a \leftarrow b$

$b \leftarrow c$ -->

|a|b|c|
|:--:|:--:|:--:|
|a|b|a|
|b|b|a|
|b|a|a|

On a échangé les valeurs contenues dans les variables `a` et `b`.
On a commencé par mettre "au frigo" dans `c` la valeur contenue dans la variable `a`.

Reprendre l'algorithme sans la première ligne pour voir ce qui se passe


### _Exercice 8_

__L'indice de masse corporelle__ est une grandeur permettant d'estimer la corpulence d'une personne.  

__a-__ On considère les variables _taille_ et _poids_ définies.

Après avoir consulté l'[article de Wikipedia](https://fr.wikipedia.org/wiki/Indice_de_masse_corporelle), exprimer la variable imc en fonction des variables précédentes.
 
__b-__Testez l'expression établie avec les couples de valeurs suivants correspondant à une partie des [exemples donnés](https://fr.wikipedia.org/wiki/Indice_de_masse_corporelle#Exemples)  

Vous travaillerez dans le shell en définissant d'abord les valeurs des variables taille et poids puis celle de la variable imc.

|taille (m)|poids (kg)|  
|:---:|:--:|  
|1,50 |  50|    
|1,70 | 70 |  
|1,90 |90  | 

### Exercice 9 :

Ecrire dans la zone de script un programme qui définit une variable `R` puis calcule et stocke le volume d'une sphère de rayon `R` dans une variable puis enfin l'affiche (utiliser l'instruction `print()` dont vous pouvez chercher la synthaxe).

Utiliser le mode débogueur de Thonny pour observer pas à pas les opérations effectuées. 
