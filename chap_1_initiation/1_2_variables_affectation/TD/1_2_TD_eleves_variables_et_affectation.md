# TD : variables et affectation

### _Exercice 1_

__1)__ Pour chacune des lignes de code suivantes indiquez (**sans les exécuter dans la fenêtre shell**) l’erreur commise


```python
>>> moyenne eleve = 10 
```


```python
>>> 12 = moyenne  
```


```python
>>> a = 3 + s  
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-4-645c30dfa2d0> in <module>
    ----> 1 a = 3 + s
    

    NameError: name 's' is not defined



```python
>>> y - 1 = 8
```


```python
>>> if = 50  
```

### _Exercice 2_

On considère l'algorithme suivant. 

1. En utilisant un tableau d'état des variables, faire fonctionner l'algorithme suivant avec $`x=3`$ puis avec $`x=4`$ (construire des petits tableaux d'états des variables).
2. Exprimer en fonction de $`x`$ la valeur contenue dans la variable $`b`$ en fin d'éxécution.

![algo](./fig/1_2_td_fig_1.jpg)

### _Exercice 3_

On considère le code ci-dessous.

1. En réalisant un tableau d'état des variables, déterminer les valeurs contenues dans les variables a et b en fin d'éxécution :


```python
a = 5
b = 3
b = a + 2b
a = b - a
b = a - b
```

2. Exprimer en fonction des valeurs initiales a et b de départ le contenu des variables a et b en fin d'éxécution

### _Exercice 4_

Sans faire exécuter le code, déterminer le type et la valeur de chaque variable à l'issue de l'exécution du programme :

1.

  ```Python
a = 8
b = 4 * 2
c = a == b
```

2.

  ```Python
a = 5
b = 12
r1 = a + b
r2 = "a" + "b"
r3 = str(a) + str(b)
```

### _Exercice  5_

On considère le code ci-dessous.

1. En réalisant un tableau d'état des variables, déterminer les valeurs contenues dans les variables a et b en fin d'éxécution :


```python
a = 2
b = -1
a += 5
b *=4
a += a + b
b -= a - b
```

### _Exercice 6_ 

Quel est le rôle de l'algorithme suivant (réaliser un tableau d'état des variables) ?

![algo](./fig/1_2_td_fig_2.jpg)

<!-- $x \leftarrow x - y$

$y \leftarrow x + y$

$x \leftarrow y - x$ -->

### _Exercice 7_ : méthode à connaître

Quel est le rôle de l'algorithme suivant (deux variables $`a`$ et $`b`$ étant précédemment définies) :

<!--![algo](./fig/1_2_td_fig_3.jpg)-->

 $`c \leftarrow a`$

$`a \leftarrow b`$

$`b \leftarrow c`$ 

### _Exercice 8_

__L'indice de masse corporelle__ est une grandeur permettant d'estimer la corpulence d'une personne.  

__a-__ On considère les variables `taille` et `poids` définies.

Après avoir consulté l'[article de Wikipedia](https://fr.wikipedia.org/wiki/Indice_de_masse_corporelle), exprimer la variable imc en fonction des variables `taille` et `poids`.
 
__b-__ **En travaillant dans la fenêtre shell**, pour chacun des exemples suivants, affectez les valeurs aux variables `taille` et `poids` puis affectez la valeur de l'imc à la variable `imc` et affichez la valeur obtenue.

|taille (m)|poids (kg)|  
|:---:|:--:|  
|1,50 |  50|    
|1,70 | 70 |  
|1,90 |90  | 

### Exercice 9 :

Ecrire dans la zone de script un programme qui définit une variable `rayon` (y affecter initialement `4` comme valeur) puis calcule et stocke le volume d'une sphère de rayon `rayon` dans une variable (à vous de choisir un nom) puis enfin affiche la valeur du volume.

Pour l'affichage demandé, vous utiliserez l'instruction `print()`.  
Vous ferez une recherche "python print" afin de trouver la documentation de cette fonction.

Utiliser le mode débogueur de Thonny pour observer pas à pas les opérations effectuées. 

_Remarque_ : on prendra ici $`\pi = 3.14`$ (utiliser une variable `pi` pour stocker ce nombre.

