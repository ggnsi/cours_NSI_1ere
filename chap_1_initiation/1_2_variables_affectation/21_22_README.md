# Chap 1_2 : variables et affectations

### Cours

* [Le notebook](./2_cours_variables_et_affectation.ipynb) et sa  [version ouverte avec Basthon](https://notebook.basthon.fr/?from=2_cours_variables_et_affectation.ipynb) ou la [version statique](./2_cours_variables_et_affectation.md)
* [version complétée du notebook](./2_cours_variables_et_affectation_complete.md)

### TD

* [L'énoncé](./TD/1_2_TD_eleves_variables_et_affectation.md)

