# Variables

## Définition

Une variable est l'association d'une valeur (ou plutôt d'un emplacement réservé dans la mémoire) avec un _nom\_de\_variable_ (c'est à dire un ensemble de caractères autorisés).

## Affectation

On peut lui __affecter__ une __valeur__ en utilisant : `nom_de_variable = valeur`.

Si la variable n'existe pas, alors Python la crée.

_Exemples_ :



```python
a = 5
```


```python
print(a)
```

    5
    


```python
type(a)
```




    int




```python
texte = 'bonjour'
```


```python
type(texte)
```




    str




```python
a = 5.0
```


```python
type(a)
```




    float



En Python, on dit que le **typage est dynamique**, c'est à dire qu'il n'y pas besoin de définir le type d'une variable au préalable et que Python adapte le type au contenu.

Visualisation dans Thonny : on peut afficher la fenêtre des variables (Affichage -> variables)

## Nom de variable


```python
v lumiere = 300
```


      File "<ipython-input-8-66625e0bf50d>", line 1
        v lumiere = 300
          ^
    SyntaxError: invalid syntax
    



```python
v_lumiere = 300
```


```python
1_variable = 300
```


      File "<ipython-input-10-327da6759f9e>", line 1
        1_variable = 300
         ^
    SyntaxError: invalid decimal literal
    





Le choix du nom de variable est assez libre. Cependant, quelques règles :

* Un nom de variable peut être composé de lettres, de chiffres et du caractère \_ mais doit __obligatoirement__ _commencer_ par une lettre ou par le symbole \_ ;

* on __ne peut pas__ mettre d'espace dans un nom de variable. On utilise le caractère souligné \_ pour séparer les mots ;

* Donnez à vos variables un nom qui décrit leur rôle plutôt que leur nature : somme, produit, prix, pt_de_vie sont mieux que nombre ou entier ;

* Même si Python 3 les accepte, interdisez-vous les caractères accentués. Contentez-vous de symboles simples. Il vaut mieux nommer une variable _prenom_ plutôt que _prénom_ , ou _coeur_ plutôt que _coeur_ ;

* À quelques exceptions courantes près (du style _x_ et _y_ pour des coordonnées), il est préférable de choisir des noms qui font au moins 3 caractères. _nom_ est préférable à _nm_ , _option_ est préférable à _op_.

La norme [PEP-08](https://openclassrooms.com/fr/courses/4425111-perfectionnez-vous-en-python/4464230-assimilez-les-bonnes-pratiques-de-la-pep-8) précise un ensemble de comportements de bonne conduite à avoir afin d'avoir un code lisible et bien ordonné.

Certains noms sont réservés par le langage :

| | | | | | | | | |
|:---: |:---: |:---: |:---: |:---: |:---: |:---: |:---: |:---: |
| False |    None |    True |    and |    as |    assert |    async |    await|    break|
| class |   continue |   def |   del |    elif |   else |   except |   finally |    for |
| from |    global |    if |    import |    in |    is |    lambda |    nonlocal |    not |
| or|    pass |    raise |     return |    try |    while |    with |    yield | |

On évitera d'utiliser des noms de fonctions déjà définies, comme par exemple _print_ ce qui reviendrait à les écraser.


Une variable évidemment être ré-utilisée dans d'autres expressions.

Exemple :


```python
pi = 3.14159
rayon = 4
```


```python
pi * rayon ** 2
```




    50.26544




```python
phrase = 'Bonjour'
nom = 'Jean'
```


```python
phrase + ' ' + nom
```




    'Bonjour Jean'



### Retour sur le mécanisme d'affectation

Regardons dans Thonny le détail de ce qui se passe lorsque l'on évalue les instructions suivantes (utilisons le mode débuggage) :


```python
a = 4
b = 7
c = a + b
a = a ** 2
```

Lorsque Python doit réaliser une affectation, il commence par _évaluer le contenu de l'expression de droite_ avant de _l'affecter_ à la variable nommée à gauche.

Cela s'illustre parfaitement lors de l'affectation simultanée permise en Python (la notion de tuples est derrière mais nous y reviendrons plus tard).

Examinons sous Thonny ce qui se passe lorsque nous exécutons le code suivant :


```python
a = 3
b = 2
a,b = b,a
```

__Attention__ à ne pas confondre l'affectation `=` et l'opérateur de comparaison  `==`

_Exemple_ : 

Affectation


```python
a = 3
```

(la variable a contient maintenant la valeur 3)

comparaison (qui renvoie une booléen) :


```python
a == 2
```




    False



### Affectation augmentée

Il arrive régulièrement d'avoir à réaliser des opérations du type :


```python
a = a + 2
a = a - 3
a = a * 2
phrase = phrase + nom
```

Afin de réduire l'écriture de ce type d'opération, on peut procéder comme suit :


```python
a = 5
chaine = 'Bonjour'
nom = ' Pierre'
a += 2 #équivalent de a = a + 2
a -= 3 #équivalent de a = a - 3
a *= 2 #équivalent de a = a * 2
phrase += nom # équivalent de phrase = phrase + nom
```

### Utilisation d'un tableau d'état des variables

Afin de comprendre le fonctionnement d'un algorithme, il est souvent utile d'utiliser un tableau d'état des variables.

Cela consiste à réaliser un tableau dans lequel on met en entête les différentes variables utilisées et sur les lignes les valeurs successives prises par chacune des variable.

Exemple : 

Considérons l'algorithme suivant (écrit en pseudo-code) : 

a $\leftarrow$ a - b

b $\leftarrow$ b - a

a $\leftarrow$ b - a

et cherchons à savoir ce qu'il réalise.

Pour cela, réalisons un tableau d'état des variables : 

$\begin{array}{|c{5cm}|c{5cm}|}
\hline
a & b \\
\hline
 & \\
 \hline
  & \\
 \hline
  & \\
 \hline
 \end{array}$

## Retour sur la norme PEP-8

Elle indique entre autre (ce qui nous concerne pour l'instant) :

* Une ligne doit contenir 80 caractères maximum.
* Les noms (variable, fonction, classe, ...) ne doivent pas contenir d'accent. Que des lettres ou des chiffres !
* Opérateurs : un espace avant et un après. Exemple : `i = 1 + 1`
* Pour les noms de variables, on utilise uniquement des lettres minuscules et tiret du bas `ma_variable`

