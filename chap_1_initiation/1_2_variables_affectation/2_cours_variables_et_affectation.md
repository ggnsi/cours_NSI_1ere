# Variables

## Définition

Une variable est l'association d'une valeur (ou plutôt d'un emplacement réservé dans la mémoire) avec un _nom\_de\_variable_ (c'est à dire un ensemble de caractères autorisés).

## Affectation

On peut lui __affecter__ une __valeur__ en utilisant : `nom_de_variable = valeur`.

Si la variable n'existe pas, alors Python la crée.

_Exemples_ (entrez les expressions suivantes dans la fenêtre **Shell**) :



```python
a = 5
```


```python
print(a)
```


```python
type(a)
```


```python
texte = 'bonjour'
```


```python
type(texte)
```


```python
a = 5.0
```


```python
type(a)
```


```python
a
```

En Python, on dit que le **typage est dynamique**, c'est à dire qu'il n'y pas besoin de définir le type d'une variable au préalable et que Python adapte le type au contenu.

Vous voyez d'ailleurs que vous avez pu changer le type de la variable `a` en lui affectant un flottant.

Dans Thonny : on peut afficher la fenêtre des variables (Affichage -> variables).

## Nom de variable

Testez dans la fenêtre **Shell** :


```python
v lumiere = 300
```


```python
v_lumiere = 300
```


```python
1_variable = 300
```




Le choix du nom de variable est assez libre. Cependant, quelques règles :

* Un nom de variable peut être composé de lettres, de chiffres et du caractère \_ (underscore ou "tiret du 8") mais doit __obligatoirement__ _commencer_ par une lettre ou par le symbole \_ ;

* on __ne peut pas__ mettre d'espace dans un nom de variable. **On utilise le caractère souligné \_ pour séparer les mots** ;

* **Donnez à vos variables un nom qui décrit leur rôle plutôt que leur nature** : somme, produit, prix, pt_de_vie sont mieux que nombre ou entier ;

* Même si Python 3 les accepte, **interdisez-vous les caractères accentués**. Contentez-vous de symboles simples. Il vaut mieux nommer une variable _prenom_ plutôt que _prénom_ ;

* À quelques exceptions courantes près (du style _x_ et _y_ pour des coordonnées), **il est préférable de choisir des noms qui font au moins 3 caractères**. _nom_ est préférable à _nm_ , _option_ est préférable à _op_.

La norme [PEP-08](https://openclassrooms.com/fr/courses/4425111-perfectionnez-vous-en-python/4464230-assimilez-les-bonnes-pratiques-de-la-pep-8) précise un ensemble de comportements de bonne conduite à avoir afin d'avoir un code lisible et bien ordonné.

Certains noms sont réservés par le langage :

| | | | | | | | | |
|:---: |:---: |:---: |:---: |:---: |:---: |:---: |:---: |:---: |
| False |    None |    True |    and |    as |    assert |    async |    await|    break|
| class |   continue |   def |   del |    elif |   else |   except |   finally |    for |
| from |    global |    if |    import |    in |    is |    lambda |    nonlocal |    not |
| or|    pass |    raise |     return |    try |    while |    with |    yield | |

On évitera d'utiliser des noms de fonctions déjà définies, comme par exemple _print_ ce qui reviendrait à les écraser.


Une variable évidemment être ré-utilisée dans d'autres expressions.

Exemple (Testez toujours dans le **Shell**) :


```python
pi = 3.14159
rayon = 4
```


```python
pi * rayon ** 2
```


```python
phrase = 'Bonjour'
nom = 'Jean'
```


```python
phrase + ' ' + nom
```

### Retour sur le mécanisme d'affectation

**Préalable** : avoir activé dans Thonny l'affichage de la zone variable (voir menu Affichage)

* Copier/coller dans la zone script les instructions suivantes :


```python
a = 4
b = 7
c = a + b
a = a ** 2
```

* Sauvegardez ce script (ensemble d'instructions) sur votre espace personnel (pensez à l'organiser en dossiers différents en fonction des chapitres) en lui donnant un nom (par exemple `2_test_1.py`)

* Puis lancez le mode dé-bogage en cliquant sur l'icone du petit insecte (voir image ci-dessous)

![image thonny](./img/affectation_1.jpg)

* Exécutez pas à pas en utilisant à chaque fois "entrer dans le noeud) et observer comment Python analyse et exécute chacune des expressions d'affectation (penser à regarder l'évolution de la zone variable)

![image thonny](./img/affectation_2.jpg)

**Bilan** : 

>Lorsque Python doit réaliser une affectation, il commence par **évaluer le contenu de l'expression de droite** avant de **l'affecter_ à la variable nommée à gauche**.



> **Attention** à ne pas confondre l'affectation `=` et l'opérateur de comparaison  `==`

_Exemple_ : 

Affectation


```python
a = 3
```

(la variable a contient maintenant la valeur 3)

comparaison (qui renvoie une booléen) :


```python
a == 2
```

### Affectation augmentée

Il arrive régulièrement d'avoir à réaliser des opérations du type :


```python
a = a + 2
a = a - 3
a = a * 2
chaine = chaine + nom
```

C'est à dire que l'on souhaite effectuer une opération sur le contenu d'une variable et ré-affecter le résultat à cette variable.

Quand on a l'habitude (n'utilisez cette synthaxe que lorsque vous vous sentirez à l'aise), afin de réduire l'écriture de ce type d'opération, on peut procéder comme suit (testez dans la zone de Shell et obervez (ou faites afficher) au fur et à mesure le contenu des variables) :


```python
a = 5
chaine = 'Bonjour'
nom = ' Pierre'
a += 2 #équivalent de a = a + 2
a -= 3 #équivalent de a = a - 3
a *= 2 #équivalent de a = a * 2
chaine += nom # équivalent de chaine = chaine + nom
```

### Utilisation d'un tableau d'état des variables (à faire avec l'enseignant)

Afin de comprendre le fonctionnement d'un algorithme, il est souvent utile d'utiliser un tableau d'état des variables.

Cela consiste à réaliser un tableau dans lequel on met en entête les différentes variables utilisées et sur les lignes les valeurs successives prises par chacune des variable.

Exemple : 

Considérons l'algorithme suivant (écrit en pseudo-code, la flèche $`\leftarrow`$ signifiant `prend la valeur`) : 

a $`\leftarrow`$ a - b

b $`\leftarrow`$ b - a

a $`\leftarrow`$ b - a

et cherchons à savoir ce qu'il réalise.

Pour cela, réalisons un tableau d'état des variables : 

* en commençant par considérer qu'au départ `a` contient `4` et `b` contient `7` ;
* puis de manière générale

|a|b|
|:---:|:---:|
| | |
| | |
| | |

## Retour sur la norme PEP-8

Elle indique entre autre (ce qui nous concerne pour l'instant) :

* Une ligne doit contenir 80 caractères maximum.
* **Les noms (variable, fonction, classe, ...) ne doivent pas contenir d'accent**. Que des lettres ou des chiffres !
* **Opérateurs : un espace avant et un après**. Exemple : `i = 1 + i` et pas `i=1+i`
* Pour **les noms de variables**, on utilise **uniquement des lettres minuscules et tiret du bas** `ma_variable`

> Votre code doit être rendu le plus lisible possible pour un humain !

## Des commentaires

Tout code est destiné à être relu/modifié ... par des humains !

Ainsi, au delà des conseils déjà donnés (utiliser des noms de variables parlants, le PEP-8, ...), il est courant d'insérer des commentaires pour expliquer ce que réalise le code.

En python, on utilise le symbole `#` pour insérer un commentaire sur une ligne

![commentaire](./img/affectation_3.jpg)

Pour insérer un commentaire sur plusieurs lignes , on le commence avec trois guillemets `"""` et on le finit de la même façon. C'est une façon aisée d'écrire un long commentaire qui court sur plusieurs lignes : 


```python
"""
Nous rappelons la formule du produit scalaire de deux
vecteurs que nous utilisons ci-dessous :
Soit U(x;y) et V(x';y'), U.V = x * x' + y * y'
"""
```

En réalité, il ne s'agit pas d'un commentaire mais d'un texte du programme s'étendant sur plusieurs lignes. Sans instruction pour l'utiliser, il est cependant ignoré à l'exécution. 
