# Structure itérative : boucles non bornées -- la boucle `while`

## principe et syntaxe

>La boucle répétitive conditionnelle Tant que / `while` est utilisée quand on ne connait pas le nombre de fois où l'on doit répéter une suite d'instructions. 

>Contrairement à la boucle `for`, il n'y a pas de variable de boucle et donc pas d'incrépmentation automatique.

_Syntaxe_ : 

```Python
#Instructions qui précèdent
while Condition :
    # Instructions répétées tant que la Condition est vérifiée
# Instructions qui suivent
```

`Condition` est le plus souvent un test de comparaison qui, une fois évalué se transforme en valeur booléenne : `True` ou `False`.  
Et donc tant que la `condition` est `vraie`, le corps de boucle est exécuté.

En pratique :

* la condition est évaluée ;

* si la condition est Fausse, on n'entre pas dans la boucle et on continue après le bloc des instructions du while ;

* si la condition est Vraie, on entre dans la boucle :
    * les instructions du corps de boucle sont exécutées ;
    * on retourne à l'évaluation de la condition.

On répète ainsi les instructions du corps de boucle jusqu'à ce nécessaire pour que la condition corresponde à `Faux`.

_Conséquence_ : 

La condition doit devenir fausse à un moment. Sinon, on se retrouve avec une boucle infinie.

### exemples et remarques :

**_Exemple 1_** :
    
```Python
a = 0
while (a < 3):        # (n’oubliez pas le double point !)
    print(a)
    a = a + 1          # (n’oubliez pas l’indentation !)    
```

Compléter le tableau d'état des variables :

|&nbsp;&nbsp; a &nbsp;&nbsp; |
|:--:|
| &nbsp;&nbsp;&nbsp; |
|  |
|  |
|  |
|  |

_Remarques_ :

On introduit une variable auxiliaire `a` dont la valeur est modifiée à l'intérieur du corps de boucle  et qu'on utilise dans la condition d'arrêt (heureusement puisque la condition fait intervenir cette variable `a`)


**_Exemple 2_**

```Python
a = 0
while (a < 5):       
    print(a)
a = a + 1            
```

Compléter le tableau d'état des variables :

|&nbsp;&nbsp; a &nbsp;&nbsp; |
|:--:|
| &nbsp;&nbsp;&nbsp; |
|  |
|  |
|  |
|  |


La variable `a` ne sera jamais modifiée dans le corps de boucle et conservera toujours la valeur 0.  
Ainsi la condition `(a < 5)` sera toujours vraie ... et la boucle sera infinie.

**_Exemple 3_** : un menu


```python
entree = 'z'
while (entree != 'a') and (entree != 'b') and (entree != 'c'):
    entree = input('Faites votre choix (a,b ou c) :')
```

Une autre manière de coder cela :


```python
recommencer = True
while recommencer:
    entree = input('Faites votre choix (a,b ou c) :')
    if (entree = 'a') or (entree = 'b') or (entree = 'c'):
        recommencer = False

#suite des instructions
```

## `continue` et `break`

Ces instructions déjà vues avec les boucles `for` peuvent aussi être utilisées avec les boucles `while`.

__continue__

L'instruction `continue` dans la boucle while en Python renvoie à la condition en l'entrée de la boucle. Toutes les actions suivant `continue` dans le corps de boucle ne sont pas réalisées.


```python
var = 10
while var>0 : 
    var = var-1
    if var == 4 :
        continue
    print (var, end = " ")
```

    9 8 7 6 5 3 2 1 0 

__break__

L'instruction `break` fait sortir immédiatement du corps boucle et de la boucle.


```python
var = 10
while var>0 : 
    var = var-1
    if var == 4 :
        break
    print (var, end = " ")
```

    9 8 7 6 5 
