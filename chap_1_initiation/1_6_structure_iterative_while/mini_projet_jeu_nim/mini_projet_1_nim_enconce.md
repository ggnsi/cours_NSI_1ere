# Projet : le jeu de Nim

__Ce travail est individuel__. Le script finalisé sera remis via Capytale (je vous enverrai le code par mail). 

**Seule les fonctions vues en cours sont autorisées**

# Régles du jeu

> On dispose un tas de 16 allumettes au milieu de la table.  
> Les deux joueurs ramassent à tour de rôle 1,2 ou 3 allumettes.  
> **Celui qui prend la dernière a gagné** .  


# Ressources disponibles

* L'énoncé ici présent   
* Le script `nim_sujet.py` à télécharger [ici](./nim_sujet.py)  


# Travail à réaliser

__Vous devrez compléter le script fourni en suivant les spécifications demandées pour chaque fonction dans l'énoncé afin d'implémenter en Python le jeu de NIM__

Réalisez les étapes qui suivent dans l'ordre et vérifier au fur et à mesure votre code.    

## Etude du script fourni

__a)__ Ouvrez avec Thonny le fichier nommé `nim_sujet.py` et renommez-le en indiquant notre nom et votre classe sous la forme `nim_nom_prenom_classe.py`.    

__b)__ Observez la structuration du code en plusieurs zones délimitées par des commentaires :   

* Importation des modules   
* Fonctions d'affichage   
* Fonctions relatives aux joueurs   
* Fonctions liées aux coups joués    
* Fonction principale    

__c)__ Les noms des fonctions et des paramètres associés sont imposés, respectez-les! 
Les noms de paramètres réutilisés dans plusieurs fonctions différentes correspondent volontairement aux mêmes données (__leurs significations respectives ne seront donc fournies qu'une fois dans l'énoncé à traiter__)  

__d)__ Remarquez le mot-clé `pass` utilisé dans chaque fonction. Il permet de réserver un espace pour la définition d'une fonction à venir. Autrement dit, tant qu'aucun code n'est écrit dans une fonction, aucune erreur ne sera levée. __Veillez donc, à n'enlever les mot-clés `pass` qu'au fur et à mesure de votre travail.__  

__e)__ N'oubliez pas d'écrire au fur et à mesure une __docstring__ adaptée à chaque fonction

## Définitions des fonctions (procédures) d'affichage

### Affichage du joueur courant

Compléter le code de la procédure `affiche_joueur(joueur_actuel)`, où  `joueur_actuel` est une chaîne de caractères  (correpondant au nom du joueur).  
Elle **affichera** un message indiquant au joueur en cours de jouer.  

Par exemple :
```python
>>> affiche_joueur('Bob')
 --> A Bob de jouer
```

### Affichage de la situation

Compléter le code de la procédure `affiche_situation(situation)` où `situation` est un entier positif (correpondant au nombre d'allumettes encore présentes dans le tas).  
Elle affichera ces allumettes en utilisant pour chacune d'entre-elles le symbole `|` (Réaliser pour l'instant un affichage dans lequel les allumettes seront alignées horizontalement)

Par exemple :
```python
>>> affiche_situation(5)
 Il reste 5 allumette(s).
 | | | | | 
>>> affiche_situation(0)
 Il reste 0 allumette(s).

```

### Affichage du choix du joueur

Compléter le code de la procédure `affiche_choix(joueur_actuel, choix)` où :  
* `joueur_actuel` est une chaîne de caractères ; 
* `choix` est un entier correpondant au nombre d'allumettes que le joueur souhaite prendre.    
    
Elle **affichera** une phrase présentant le nombre d'allumettes prises par le joueur en cours.   

Par exemple :
```python
>>> affiche_choix(2, 'Lea')
 Lea prend 2 allumette(s)
```

### Affichage des coups possibles

Compléter le code de la procédure `affiche_coups(coups_max)`, où `coups_max` est un entier **compris entre 1 et 3** et correspondant au nombre maximal d'allumettes pouvant être prises par un joueur lors de son tour.
    
Elle affichera une phrase présentant les choix possibles : 

Par exemple :
```python
>>> affiche_coups(3)
Vous pouvez prendre 1, 2 ou 3 allumettes.

>>> affiche_coups(1)
Vous ne pouvez prendre qu'une seule allumette.

>>> affiche_coups(2)
Vous pouvez prendre 1 ou 2 allumettes.
```

### Affichage du résultat de la partie

Compléter le code de la procédure `affiche_fin_partie(joueur_actuel)`, où `joueur_actuel` est une chaîne de caractères.

Elle affichera une phrase présentant indiquant que le tas d'allumettes est vide et présentant le gagnant (il s'agira du joueur en cours : celui ayant pris la dernière allumette). 

Par exemple :
```python
>>> affiche_fin_partie('Enzo')
Il n'y a plus d'allumette disponible, Enzo a gagné !
```

## Définitions des fonctions relatives aux joueurs

### Tirage au sort initial

Compléter le code de la fonction `premier_joueur(joueur1, joueur2)` où `joueur1` et `joueur2` sont des chaînes de caractères représentant les noms des deux joueurs.

La fonction devra __renvoyer__ le nom d'un joueur tiré au sort pour être le premier à jouer.  

Vous utiliserez la fonction `random()` du module `random` (pensez à l'importer dans la zone réservée et étudiez la documentation correspondante).  
Si le nombre tiré au sort est inférieur ou égal à __0,5__ choisissez `joueur1`, dans le cas contraire choisissez `joueur2`.


Par exemple :
```python
>>> premier_joueur('Mike', 'Cindy')
 'Cindy'
```

### Changement de joueur tour après tour

Compléter le code de la fonction `change_joueur(joueur_actuel, joueur1, joueur2)` où `joueur_actuel`, `joueur1` et `joueur2` sont des chaînes de caractères.

La fonction devra __renvoyer__ le nom du joueur, qui n'est pas le joueur en cours.

Par exemple :
```python
>>> change_joueur('Leo', 'Bob', 'Leo')
 'Bob'
>>> change_joueur('Bob', 'Bob', 'Leo')
 'Leo'
```

## Définitions des fonctions liées aux coups joués

### Existe-t-il un coup possible?

Compléter le code du **prédicat** `existe_coup(situation)`, où situation est un entier représentant le nombre d'allumettes restantes en jeu.

Le **booléen renvoyé** indiquera si un coup est-encore possible pour le joueur (s'il reste donc au moins une allumette dans le tas)

Par exemple :
```python
>>> existe_coup(8)
 True

>>> existe_coup(0)
 False
```

### Quels sont les coups possibles?

Compléter le code de la fonction `coups_possibles(situation)`, où `situation` est un entier supérieur ou égal à 1.

L'entier renvoyé par la fonction correspondra aux nombre maximum d'allumettes pouvant être prises par le joueur (attention ce nombre dépend du nombre d'allumettes encore restantes)

Par exemple :
```python
>>> coups_possibles(9)
 3

>>> coups_possibles(2)
 2
```


### Choisir le nombre d'allumettes à prendre

Compléter le code de la fonction `choix_coups(coups_max)` où `coups_max` est un entier .

La fonction devra demander à l'utilisateur le nombre d'allumettes qu'il souhaite prendre. Attention ce nombre devra être compatible avec la valeur de `coups_max` (on le rappelle : le nombre maximal d'allumettes pouvant être prises par un joueur lors de son tour). 
**Cette demande s'effectuera tant que la valeur choisie ne sera pas possible.**
La fonction **retournera** cette valeur.


### Mettre à jour le nombre d'allumettes après le choix d'un joueur

Compléter le code de la fonction `msj_situation(choix, situation)` où `choix` et `situation` sont deux entiers (et `choix` $`\leqslant`$ `situation`)

La fonction doit renvoyer le nombre d'allumettes restantes après le choix du joueur en cours.

Par exemple :
```python
>>> msj_situation(2, 15)
13
>>> msj_situation(3, 8)
5
>>> msj_situation(3, 3)
0
```

## Définition de la fonction principale du jeu

La fonction `jouer()` est la seule fonction directement appelée par l'utilisateur pour exécuter le jeu. 

> Son rôle est d'**appeler les fonctions précédentes** afin de réaliser une partie compléte de jeu de NIM entre deux joueurs humains.

> Vous devez donc **ré-utiliser au maximum les fonctions précédentes et ne pas écrire du code dans la fonction `jouer` si vous pouvez utiliser une des fonctions précédentes**.
>
> **Cela permet d'améliorer la lisibilité et la maintenance des codes**.  
> D'une part grâce à des noms de variables parlants, d'autre part car il est plus facile de comprendre/modifier des fonctions de petites tailles.



Voici l'algorithme écrit en pseudo-code décrivant les instructions réalisées par cette fonction.

```
1:  situation ← 16
2:  joueur1 ← Demandez le nom d'un joueur
3:  joueur2 ← Demandez le nom de l'autre joueur
4:  joueur_actuel ← tirage au sort du premier joueur
5:  Afficher le nombre d'allumettes encore présentes
6:  TANT QUE un coup est encore possible faire :
7:     joueur_actuel ← nouveau joueur
8:     Afficher le nom du joueur qui doit jouer
9:     coups_max ← Nombre d'allumettes maximum pouvant être prises par le joueur en cours
10:    Afficher le nombre précédent
11:    choix ← Demander le nombre d'allumettes à prendre
12:    situation ← nombre d'allumettes restantes après le choix du joueur
13:    Afficher le choix réalisé par le joueur
14:    Afficher le nombre d'allumettes restantes
15:  fin TANT QUE
16:  Afficher le gagnant
```

_Remarque_ : On change de joueur à chaque tour lors de la ligne 7. Cela entraine qu'au premier tour le premier joueur ne sera pas celui choisi ligne 4 mais l'autre (ce qui ne pose pas de souci puisque c'est le premier tour).

Ecrivez la définition de la fonction `jouer` en réutilisant toutes les fonctions précédentes.  
Puis, tester le fonctionnement du jeu en l'appelant.  


## Approfondissements (optionnels) :  

__Si vous décidez de réaliser tout ou partie de ces approfondissements rendez en plus du script précédent un deuxième script nommé différemment__

__1)__ Réaliser un affichage de la situation en vous inspirant de l'image ci-dessous, soit en pyramide avec une base de sept allumettes, puis un étage de 5 allumettes, surmonté d’un avant-dernier étage de 3 allumettes, pour finir avec au sommet une seule allumette. (on enlève les allumettes à partir du haut de la pyramide)

![img](./jeu_nim_en_etages.png)


__2)__ Sans changer les régles du jeu, proposez une ou plusieurs fonctionnalités supplémentaires au jeu.


---
**Sources** :
* image du jeu de nim en pyramide : [créateur MOtty](https://fr.wikiversity.org/wiki/Jeux_d%27allumettes/Jeu_de_Marienbad#/media/Fichier:Pyramidal_matches.svg)
* projet initial : Patrice Thibaud modifié par Gaëtan Guillon
