# Structures itératives : la boucle `while`

## Les débuts ...

### Exercice 1

Pour chacune des situations suivantes, dresser un tableau de suivi des valeurs des variables utilisées et précisez le ou les affichages réalisés.

__1)__

```python
i = 0
j = 1
while i < 3:
    i = i + 1
    print(i + j)
```

__2)__

```python
i = 1
j = 1
while i < 3:
    i = i + 1
while j < 1:
    j = j + 1
print(i + j)
```

__3)__
```python
x = 5
while x != 2:
    x = x - 1
print(x)
```

__4)__
```python
x = 12
while x > 12:
    x = x + 1
print(x)
```

### Exercice 2

1. Ecrire une fonction `ent1()` utilisant une boucle `while` qui affiche, séparés par un espace, les entiers allant de 1 à 15.

```Python
>>> ent1()
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
```

2. Ecrire une fonction `ent2()` utilisant une boucle `while` qui affiche, séparés par un espace, les entiers _impairs_ compris entre 1 et 15 (inclus).

```Python
>>> ent2()
1 3 5 7 9 11 13 15
```

3. Ecrire une fonction `ent2_bis(n)`, avec `n` entier utilisant, une boucle `while` qui affiche, séparés par un espace, les entiers _impairs_ compris entre 1 et n (inclus).


```Python
>>> ent2_bis(23)
1 3 5 7 9 11 13 15 17 19 21 23
>>> ent2_bis(14)
1 3 5 7 9 11 13
```

4. Ecrire une fonction `ent3()` utilisant une boucle `while` qui affiche, séparés par un espace, les entiers pairs allant de 18 à 0.

```Python
>>> ent3()
18 16 14 12 10 8 6 4 2 0
```

### Exercice 3

Ecrire à l'aide d'une boucle `while` une fonction `carre()` qui renvoie le premier entier `k` pour lequel `$k^2$` est supérieur ou égal à `50`.

### Exercice 4

Ecrire en utilisant une boucle `while` une fonction `somme(n)` qui renvoie la somme des entiers allant de 0 à `n` (inclus)

```Python
>>> somme(4)  # 0 + 1 + 2 + 3 + 4 = 10
10   
```

### Exercice 5

Ecrire un programme qui affiche le plus petit entier n tel que `1 + 2 + 3 + ... +  n` dépasse `16000`.

### Exercice 6

Polo est un peu obsessionnel : quand il est stressé, il se met à lancer un dé et ne s'arrête que lorsque le 6 apparaît. Alors seulement il se détend.  
Cela lui demande beaucoup d'énergie.

1. Aidez donc Polo en lui créant une fonction `polo()` qui va lancer le dé à sa place et affichera les uns en dessous des autres les numéros obtenus (le dernier étant donc un 6).

2. Modifiez votre fonction afin que soit écrit en plus sur chaque ligne le numéro du lancer effectué :

```Python
>>> polo()
Lancer 1 : 4
Lancer 2 : 1
Lancer 3 : 5
Lancer 4 : 6
```

### Exercice 7 : des lapins

Polo (qui est détendu aujourdh'ui) aime arriver tôt le matin au lycée, s'installer sur un banc et contempler le lapins qui s'amusent sur l'herbe.  
La semaine n°1, il constante qu'il y a 7 lapins.

Il constate ensuite que chaque semaine, pour trouver le nouveau nombre de lapins, il lui suffit de multiplier par 2 le nombre de lapins de la semaine précédente et d'ajouter 3.

Quel sera le numéro de la semaine où Polo apercevra pour la première fois un nombre supérieur ou égal à 1 000 lapins sur la pelouse ?

### Exercice 8 : ce fameux Michel !

Michel Strogonoff était veuf, mais il était beau.
Pour des raisons obscures, il décida de partir à l'aventure, sans un kopek en poche, à travers les étendues infinies de la Sibérie.

Las, à peine avait-il parcouru une verste [1] qu'il rencontra un ermite qui lui dit comme ça : "Michel Strogonoff, donne-moi un rouble, ou tu t'en repentiras."
- Mais, mon pauvre ermite, je suis trop pauvre, je ne peux pas te donner un rouble."
- Puisque c'est comme ça, répliqua l'ermite, c'est moi qui vais te donner un rouble ! Tiens !"
Michel Strogonoff était un peu surpris, mais content, et il reprit sa route, avec un rouble dans la poche.

Une verste plus loin, nouvel ermite, même tableau :
"Michel Strogonoff, donne-moi deux roubles, ou tu t'en repentiras."
- Mais, mon pauvre ermite, je suis trop pauvre, je ne peux pas te donner deux roubles."
- Puisque c'est comme ça, répliqua l'ermite, c'est moi qui vais te donner deux roubles ! Tiens !"
Michel Strogonoff était toujours un peu surpris, mais de plus en plus content, et il reprit sa route, avec maintenant trois roubles dans la poche.

Et à la fin de la troisième verste, ça recommence avec un troisième ermite :
"Michel Strogonoff, donne-moi trois roubles, ou tu t'en repentiras."
- Tiens, mon pauvre ermite, je me réjouis de pouvoir soulager ta misère !"
Et Michel Strogonoff lui donna ses trois roubles, et reprit sa route, la bourse vide, à la fois surpris et content, car un rien l'étonnait et c'était un heureux caractère.

Et ça continue comme ça, à la fin de la n-ième verste, un ermite lui demande n roubles. Si Michel Strogonoff les possède, il les lui donne, sinon c'est l'ermite qui lui donne n roubles.

[^1] Unité de longueur utilisée dans la Russie des Tsars, équivalant à un peu plus d'un kilomètre.

1. On cherche à savoir combien Michel Strogonoff possède après avoir quitté le dixième ermite.  
Réaliser à la main l'évolution de la `somme` détenue par Michel en fonction du nombre d'ermites rencontrés.

2. Ecrire une fonction `michel(n)`, où `n` est un entier supérieur ou égal à 1, qui renvoie la somme détenue par Michel après avoir rencontré `n` ermites.

3. Ecrire une fonction `rencontres(total)` qui renvoie le nombre de rencontres que Michel devra faire pour détenir pour la première fois une somme supérieure ou égale à `total`.

---
## Projet

### Exercice : Jeu du plus ou moins

Le jeu du plus/moins consiste à deviner un nombre entier compris entre 0 et 100 choisi au hasard par l'ordinateur.  
A chaque essai de l'utilisateur, l'ordinateur indique si le nombre proposé est trop petit, trop grand jusqu'à ce que l'utilisateur trouve.

1. Créer une fonction `plus_moins()` qui permette de jouer à ce jeu.

```Python
>>>plus_moins()
J'ai choisi mon nombre.
Votre proposition : 4
Trop petit
Votre proposition : 84
Trop grand
Votre proposition : 25
Trop petit
....
Votre proposition : 51
Gagné
```

2. Modifiez votre fonction afin que soit écrit en plus à la fin le nombre d'éssais effectués.

---
## Conversion Décimal -> Binaire : algorithme à connaître par coeur
---

### Exercice

1. Déterminer à la main l'écriture binaire du nombre 107.
2. Compléter l'algorithme suivant écrit en pseudo-code (<-- signifie "prend la valeur") qui doit retourner l'écriture binaire de `nbr` **sous forme de chaîne de caractères** (attention donc aux conversions nécessaires).

```Python
resultat <-- ''  # on initialise le résultat à une chaîne de caractères vide
Tant que nbr différent de 0:
    Effectuer ...
    resultat <-- 
    nbr <--
    
retourner ...
```

3. Ecrire une fonction `dec2bin(nbr)` qui renvoie l'écriture binaire de `nbr` sous forme de chaîne de caractères.
