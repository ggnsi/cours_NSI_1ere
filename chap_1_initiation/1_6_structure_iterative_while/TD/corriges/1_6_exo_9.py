# resultat <-- ''  # on initialise le résultat à une chaîne de caractères vide
# Tant que nbr différent de 0:
#     Effectuer la division euclidienne de nbr par 2 => quotient et reste
#     resultat <-- str(reste) + resultat
#     nbr <-- quotient
#     
# retourner resultat

def dec2bin(nbr):
    
    resultat = ''
    while nbr != 0:
        quotient = nbr // 2
        reste = nbr % 2
        resultat = str(reste) + resultat
        nbr = quotient
    return resultat
    
    
    
    