# Types et opérations

### Cours

* [cours](./1_1_types_et_operations_cours.md)
* [fiche bilan  au cas où](./22_23_types_operation_fiche_bilan_eleve.pdf)

### TD

* [enoncé](./TD/1_1_TD_type_operations.md)
* [énoncé du QCM](./TD/1_1_qcm_type_operations.pdf)
