# Exercice 1

Déterminer le type de chacun des éléments suivants (vérifier si nécessaire dans le _shell_ ) :

1. `4`
2. `4.0`
3. `False`
4. `"True"`
5. `True`
6. `6 / 3`
7. `6 // 3`
8. `5 + 7 % 2`
9. `3 ** 2 + 4 / 2`
10. `3.43e3`

# Exercice 2

Evaluez à la main les expressions suivantes puis vérifiez à l'aide du _shell_ (certaines peuvent provoquer des erreurs) :

1. `5 ** 2 - 3`
3. `5 / 2`
4. `4 / 2`
7. `'quatre' / 'deux'`
5. `14 // 6`
6. `14 % 5`
7. `5 % 2`
8. `7 + 7 % 2`
9. `2 ** 2 ** 3`
10. `'ba' + 'be' + 'bi'`
11. `'ba' * 2 + "bi"`
12. `"4" // 2`


# Exercice 3

1. Donnez les résutats obtenus par les commandes suivantes (certaines peuvent provoquer des erreurs) :

    * a) `int(3)`
    * b) `float(2.4)`
    * c) `int(4.7)`
    * d) `float("3")`
    * e) `str(2.35)`
    * f) `int("5.2")`
    * g) `str('test')`
    * h) `str('3.2')`

# Exercice 4 : QCM

[fichier pdf du QCM](./1_1_qcm_type_operations.pdf) (à ouvrir dans un nouvel onglet)


# Pour les curieux

Tapez les instructions suivantes

```python
>>> import sys
>>> sys.float_info
```
On accéde ainsi aux limitations du langage lors de la manipulation de nombres à virgule flottante.  

__a-__ Notez la valeur maximale pouvant être atteinte par un flottant en python 
__b-__  Testez ce qui se passe au delà de cette valeur.  
__c-__ Que signifie l'indication renvoyée ?

