# Exercice 1

Déterminer le type de chacun des éléments suivants (vérifier si nécessaire dans le _shell_ ) :

1. `4` : int
2. `4.0` : float
3. `False` : bool
4. `"True"` : str
5. `True` : bool
6. `6 / 3` : float
7. `6 // 3` : int
8. `5 + 7 % 2` : int
9. `3 ** 2 + 4 / 2` : float
10. `3.43e3` : float

# Exercice 2

Evaluez à la main les expressions suivantes puis vérifiez à l'aide du _shell_ (certaines peuvent provoquer des erreurs) :

1. `5 ** 2 - 3` : 22
3. `5 / 2` : 2.5
4. `4 / 2` : 2.0
7. `'quatre' / 'deux'` : Erreur
5. `4 // 2` : 2
6. `4 % 2` : 0
7. `5 % 2` : 1
8. `7 + 7 % 2` : 8 (7 + 1)
9. `3 ** 2 ** 3` : 3 puissance 8 (pour la mise à la puissance, évaluation de droite à gauche quand même priorité)
10. `'ba' + 'be' + 'bi'` : 'babebi'
11. `'ba' * 2 + 'bi'` : `bababi`
12. `'4' // 2` : erreur


# Exercice 3

1. Donnez les résutats obtenus par les commandes suivantes (certaines peuvent provoquer des erreurs) :

    * a) `int(3)` : 3
    * b) `float(2.4)` : 2.4
    * c) `int(4.7)` : 4
    * d) `float("3")` : erreur
    * e) `str(2.35)` : 2.35
    * f) `int("5.2")` : erreur
    * g) `str('test')` : 'test'
    * h) `str('3.2')` : 3.2

# Exercice 4 : QCM

[corrigé du QCM](./1_1_qcm_type_operations_corr.pdf) (à ouvrir dans un nouvel onglet)

