# Python : Types et opérations

Les documents suivants sont construits de telle manière à ce que vous observiez des exemples simples commentés puis que vous réalisiez les exercices proposés au fur et à mesure.  
Il est important de tester vous même les petits morceaux de code proposés dans les exemples avant de passer aux exercices

__Python__

![1_1_logo_python.png](./fig/1_1_logo_python.png)

Python est un language de programmation créé en 1989 par le néerlandais Guido van Rossum. Il continue d'évoluer depuis grâce à une communauté de contributeurs.

![van_Rossum.png](./fig/1_1_van_Rossum.png)

Lien : [Entretien avec Guido van Rossum dans le Journal LeMonde.fr](https://www.lemonde.fr/pixels/article/2018/07/25/je-n-imaginais-pas-que-python-connaitrait-un-tel-succes_5335917_4408996.html)

## Thonny

Nous utiliserons l'IDE (Interface de développement) _Thonny_ cette année.  
La fenêtre de _Thonny_ se compose d'une zone de script(en haut) et d'une zone _Shell_ qui permet d'inter-agir directement avec l'interpréteur Python.

![shell_thonny.jpg](./fig/shell_thonny.jpg)

Une invite de commande aussi appelée prompt et symbolisée par 3 chevrons `>>>` vous permet de tester directement du code qui sera immédiatement interprété après l'appui sur la touche `entrée`

## Les types de base

L'instruction **type** permet d'obtenir le type d'un objet.
Dans le shell, tester les types des différents objets suivants :



```python
type(8)
type(-5)
type(8.0)
type(-5.4)
type('Bonjour !')
type("bleu")
type("143")
type(True)
type(False)
type('True')
```

Compléter le tableau résumé (reprenez les exemples précédents pour le compléter) :

| Type python | correspond aux | Exemples |
| :--: | :-- | :--: |
| | entiers | |
| | flottants | |
| | chaînes de caractères | | 
| | booléens | |

Le type détermine les propriétés de la valeur (par exemple les opérations qu’elle peut subir) et matérielles (par exemple, la façon dont elle est représentée en mémoire et la place qu’elle occupe)

En faisant une recherche sur le net, déterminer quelles sont les différentes valeurs possibles pour un _booleen_ (_boolean_ en anglais)

## Nombres entiers : premières opérations

Effectuer, via le shell, les calculs suivants et compléter au fur et à mesure le tableau résumé :



```python
60 - 7
2 * 3 + 4
(-2 + 3) * 7 - 1
6 / 3
1 / 3
2 ** 2
2 ** 3
2 ** 4
10 // 3
11 // 3
12 // 3
13 // 3
14 // 3
10 % 3
11 % 3
12 % 3
13 % 3
14 % 3
2 < 7
2 < 2
5 >= 5
10 ** 50
10 ** 2000
```

Avec `a` et `b` entiers :

| Opérateur | opération réalisée/élément obtenu | type de l'objet obtenu |
| :--: | :-- | :--: |
| `a ** b` | | |
| `a // b` | | |
| `a % b`| | |
| `a / b` | | |
| `a < b` | | |

En Python, les priorités opératoires sont-elles respectées : Oui / Non

En Python, la taille des entiers est limitée : Oui / Non

Effectuez :


```python
100 * 59 / 60
100 / 59 * 60
```

La multiplication `*` et `/` sont deux opérateurs de même priorité.  

Lorsque Python rencontre cette situation, dans quel sens effectue-t-il les opérations ?

...

Effectuez :


```python
2 ** 3 ** 4
```

Dans quel sens Python effectue-t-il les opéations lorsqu'il y a deux mises à la puissance successives ?

....


**Pour éviter les erreurs** : ...


## Les flottants (ou nombres à virgule flottante)

Les nombres à virgule flottante sont des nombres comportant une partie entière et une partie décimale après la virgule  marquée par un point (notation anglo-saxonne).  

Entrez dans la console :



```python
351.4 ** 6
15.1 ** 8
351.4 ** 24
0.157 ** 12
```

Lorsque la partie entière d'un nombre flottant devient trop grande ou que le nombre commence par une grande suite de zéros (y compris la partie entière) : le nombre est donnée en notation scientifique (comme sur vos calculatrices).  
Ici **e** signifie « fois dix puissance ».

Exemple : `1.35e4` correspond au nombre 13 500

On peut d'ailleurs directement écrire les nombres flottants sous cette forme :

_Exemple_ : tapez : `6.02e21`

### Valeur exacte ou approximation ?

Entrez (vous pouvez faire du copier/coller (ah, j'aurais pu le dire plus tôt ...)) :



```python
0.20000000000000000000000458
0.000000000024586525412556124
0.000000000024586525412556124654564564564564545645
```

_Attention donc_ : la valeur retournée (et donc éventuellement stockée) par Python peut être une approximation du réel entré.

Enfin, il est intéressant de noter que Python convertit implicitement les entiers en flottants si nécessaire :

_Exemple_ : entrez : `4 + 5.2`

**Pour approfondir** : Chaque opérateur existe sous forme d'une fonction (cf. [documentation officielle](https://docs.python.org/fr/3.7/library/operator.html?highlight=op%25C3%25A9rateurs%20affectation#mapping-operators-to-functions))


## Chaînes de caractères  : premières opérations

Testez dans le shell les opérations suivantes :


```python
'abc' + 'def'
'abc' * 2
'abc' ** 3
'abc' / 2
'abc' // 2 
```

L'opérateur `+` appliqué à deux chaînes de caractères réalise leur **concaténation**.
L'utilisation du produit permet de réaliser une **concaténation multiple**.

Tous les opérateurs peuvent-ils être appliqués à deux chaînes de caractères ?

...

## Changer le type d'un objet : conversion de type

Effectuez les opérations suivantes 


```python
"12" + 4
"A" + 310
```

L'addition n'est donc dans Python pas autorisée entre une chaîne de caractères (`str`) et un entier (`int`).

On peut cependant avoir "récupéré" l'entier `12` stocké dans une chaîne de caractère et vouloir le convertir en `int` afin de pouvoir réaliser l'addition.  
On peut aussi souhaiter obtenir la chaine `A310`.

Testez :


```python
int('457')
int(3.145)
int(-2.25)
int('4.25')
int('a')

float('135')
float('13.25')
float(45)
float('a')

str(3.1456)
str(12)
str('a')
```

## Pour approfondir

[Types de base](https://docs.python.org/fr/3.7/library/stdtypes.html#numeric-types-int-float-complex) dans la documentation officielle

[Priorité des opérateurs](https://docs.python.org/fr/3.7/reference/expressions.html?highlight=bool%C3%A9en#operator-precedence) dans la documentation officielle
