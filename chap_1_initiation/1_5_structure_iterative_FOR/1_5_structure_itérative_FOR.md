# Structure itérative : la boucle __FOR__

## I. `range()` : une fonction un peu particulière


La fonction `range()` nous renvoie un _itérateur_ , c'est à dire un _objet_ qui va égréner des valeurs les unes après les autres en les retournant successivement.

_Définition_ ([www3schools](https://www.w3schools.com/python/ref_func_range.asp)):

> The 'range()' function returns a sequence of numbers, starting from 0 by default and increments by 1 (by default), and stops before a specified number.

_Syntax_ :

> `range(_start_, _stop_, _step_)`

_Exemples_ :

_remarque : l'instruction `list()` est uniquement présente pour nous permettre de visualiser les différents entiers qui seront retournés successivement_


```python
list(range(5))
```




    [0, 1, 2, 3, 4]




```python
list(range(7))
```




    [0, 1, 2, 3, 4, 5, 6]




```python
list(range(3,9))
```




    [3, 4, 5, 6, 7, 8]




```python
list(range(0,5,2))
```




    [0, 2, 4]




```python
list(range(5,0))
```




    []




```python
list(range(5,0,-1))
```




    [5, 4, 3, 2, 1]




```python
list(range(5,0,-2))
```




    [5, 3, 1]



## II.  La boucle __FOR__

### 1. généralité sur les boucles

_Exemple_ : Vous avez hérité de la punition qui consiste à écrire 500 fois la phrase "Je dois écouter en cours de maths et de NSI" et vous souhaitez réaliser cette punition avec un script Python.

Deux options s'offrent à vous :

1. option 1 : Ecrire 500 fois l'instruction `print("Je dois ... NSI")`
2. option 2 : Itérer (répéter) 500 fois l'instruction `print("Je dois ... NSI")`

L'option 2 consiste à utiliser une boucle.

Il existe deux types de structures itératives :
* celles où le nombre d'itération est connu dès le début : la boucle `for` (on parle de boucle _bornée_ ) ;
* celles où l'arrêt de l'itération est déterminé par un test : ce sera la boucle `while` reprise plus tard.

### 2. synthaxe de la boucle for



```python
for variable in itérable: #itérable = range(..) pour l'instant
    bloc d'instruction du corps de boucle
```

Donc pour nous à l'heure actuelle :


```python
for variable in range(..):
    bloc d'instruction du corps de boucle
```

_Remarques_ :
* la variable est appelé la **variable de la boucle**. Elle va ici prendre successivement les valeurs retournées par l'instruction `range`
* **Ne pas oublier les deux points qui indiquent qu'un bloc d'instruction suit** ;
* le corps de boucle est défini par **l'indentation** (le décalage par rapport au bord gauche).

_Exemple_ :


```python
for k in range(1,5):
    print(k)
```

**Fonctionnement détaille** :

`range(1,5)` va retourner successivement les entiers 1, 2, 3, 4

* _1er tour de boucle_ :  
    `k` prend la valeur 1  
    On execute le corps de boucle : on affiche 1
    On arrive à la fin du corps de boucle  
    
    
* _2eme tour de boucle_ :  
    `k` prend la valeur 2  
    On execute le corps de boucle : on affiche 2
    On arrive à la fin du corps de boucle  
    
    
* etc.  


* _4ème tour de boucle_ :  
    `k` prend la valeur 4  
    On execute le corps de boucle : on affiche 4
    On arrive à la fin du corps de boucle  
    
    

* _Il n'y a plus de valeur à prendre pour `k`_ : On passe à la suite du script

**Retour sur notre punition** (ici seulement 5 répétitions faites)


```python
for k in range(5): # k prendra les valeurs 0, 1, .., 4
    print("Je dois écouter en cours de maths et de NSI")
```

    Je dois écouter en cours de maths et de NSI
    Je dois écouter en cours de maths et de NSI
    Je dois écouter en cours de maths et de NSI
    Je dois écouter en cours de maths et de NSI
    Je dois écouter en cours de maths et de NSI
    

>**remarque** : lorsqu'elle la variable de boucle pas utilisée, on peut la remplacer par \_ (underscore). 

Cela montre à tout lecteur du code qu'elle ne sera pas utilisée et que le but est de répéter (5 fois dans l'exemple ci-dessous) le corps de boucle.


```python
for _ in range(5):
    print("Je dois écouter en cours de maths et de NSI")
```

### 3. Quelques exemples

Utiliser un tableau d'état des variables afin de visualiser le fonctionnement.

_Exemple 1_


```python
for k in range(1,6):
    carre = k * k
    print(carre)
```

| &nbsp;&nbsp;&nbsp; k &nbsp; &nbsp;&nbsp;| &nbsp;&nbsp; carre &nbsp;&nbsp; |&nbsp;&nbsp; affichage &nbsp;&nbsp;|
|:--:|:--:|:--:|
| &nbsp; | &nbsp; | &nbsp;|
| &nbsp; | &nbsp; | &nbsp;|
| &nbsp; | &nbsp; | &nbsp;|
| &nbsp; | &nbsp; | &nbsp;|
| &nbsp; | &nbsp; | &nbsp;|
| &nbsp; | &nbsp; | &nbsp;|

Remarque : On aurait bien sur pu aussi écrire :


```python
for k in range(1,6):
    print(k**2)
```

_Exemple 2_


```python
k = 1       
for u in range(5):  
    print(k**2)
    k = k + 1
```

| &nbsp;&nbsp;&nbsp; k &nbsp; &nbsp;&nbsp;| &nbsp;&nbsp; u &nbsp;&nbsp; |&nbsp;&nbsp; affichage &nbsp;&nbsp;|
|:--:|:--:|:--:|
| &nbsp; | &nbsp; | &nbsp;|
| &nbsp; | &nbsp; | &nbsp;|
| &nbsp; | &nbsp; | &nbsp;|
| &nbsp; | &nbsp; | &nbsp;|
| &nbsp; | &nbsp; | &nbsp;|
| &nbsp; | &nbsp; | &nbsp;|

_Remarques_ :

* Cela fait la même chose que le script précédent
* On n'utilise pas ici la variable de boucle (on aurait pu mettre `_`) ;
* on utilise une variable auxiliaire (appelée accumulateur)
* Je ne dis pas que c'est mieux : c'est un exemple de cours !

_Exemple 3_ :


```python
somme = 0
for k in range(1,5):
    somme = somme + k
print(somme)
```

| &nbsp;&nbsp;&nbsp; k &nbsp; &nbsp;&nbsp;| &nbsp;&nbsp;&nbsp; somme &nbsp; &nbsp;&nbsp; |
|:--:|:--:|
| &nbsp; | &nbsp; |
| &nbsp; | &nbsp; | 
| &nbsp; | &nbsp; | 
| &nbsp; | &nbsp; | 
| &nbsp; | &nbsp; | 
| &nbsp; | &nbsp; |

Que réalise ce script ?

_exemple 4_ : **boucles imbriquées**


```python
for num_l in range(3):
    for num_c in range(2):
        print("coordonnées", num_l, num_c)
```

| &nbsp;&nbsp;&nbsp; num_l &nbsp; &nbsp;&nbsp;| &nbsp;&nbsp;&nbsp; num_c &nbsp; &nbsp;&nbsp; |
|:--:|:--:|
| &nbsp; | &nbsp; |
| &nbsp; | &nbsp; | 
| &nbsp; | &nbsp; | 
| &nbsp; | &nbsp; | 
| &nbsp; | &nbsp; | 
| &nbsp; | &nbsp; | 

### 4. Quelques erreurs classiques

**Erreur d'indentation (et donc de définition du corps de boucle)**


```python
for k in range(3):
    print("Je suis dans la boucle")
print("Je n'y suis plus")
```

    Je suis dans la boucle
    Je suis dans la boucle
    Je suis dans la boucle
    Je n'y suis plus
    


```python
for k in range(3):
    print("Je suis dans la boucle")
    print("Je n'y suis plus")
```

    Je suis dans la boucle
    Je n'y suis plus
    Je suis dans la boucle
    Je n'y suis plus
    Je suis dans la boucle
    Je n'y suis plus
    

**Return mal placé**

Examiner ce qui se passe dans les deux algorithmes suivants (utiliser des tableaux d'états)


```python
def terme():
    u = 0
    for k in range(3):
        u = u + 1
    return(u)
```


```python
def terme():
    u = 0
    for k in range(3):
        u = u + 1
        return(u)
```

| &nbsp;&nbsp;&nbsp; u &nbsp; &nbsp;&nbsp;| &nbsp;&nbsp;&nbsp; k &nbsp; &nbsp;&nbsp; |
|:--:|:--:|
| &nbsp; | &nbsp; |
| &nbsp; | &nbsp; | 
| &nbsp; | &nbsp; | 
| &nbsp; | &nbsp; | 

| &nbsp;&nbsp;&nbsp; u &nbsp; &nbsp;&nbsp;| &nbsp;&nbsp;&nbsp; k &nbsp; &nbsp;&nbsp; |
|:--:|:--:|
| &nbsp; | &nbsp; |
| &nbsp; | &nbsp; | 
| &nbsp; | &nbsp; | 
| &nbsp; | &nbsp; | 

### 5. Les instructions 'continue' et 'break'

L'instruction `continue` dans une boucle en Python termine l'exécution du corps de boucle (on entame alors le "tour" de boucle suivant)

Exemples :



```python
for k in range(5):
    print(k)
    continue
    print('NSI')
```

    0
    1
    2
    3
    4
    


```python
for k in range(4):
    print(k)
    if k == 2 :
        continue
    print('NSI')
```

    0
    NSI
    1
    NSI
    2
    3
    NSI
    

L'instruction `break` dans une boucle en python termine immédiatement la boucle (On sort directement de la boucle)

Exemple :


```python
for k in range(4):
    print(k)
    break

```

    0
    


```python
for k in range(4):
    print(k)
    if k == 2 :
        break
    print('NSI')
```

    0
    NSI
    1
    NSI
    2
    

## III. Pour m'entrainer seul sur les boucles

Vous pouvez travailler les exercices du site [France IOI](http://www.france-ioi.org/)

Par exemple ceux du Niveau 1, chapitre 2 : [ici](http://www.france-ioi.org/algo/chapter.php?idChapter=643)

