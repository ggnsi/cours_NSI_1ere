# DM à faire sur France-IOI 

Voici  l'[adresse du site](http://www.france-ioi.org/).

**Il faudra aller sur l'ancien site** (cliquer à l'endroit indiqué en haut de la page d'accueil)

### Inscription

Ensuite :

* vous devez vous créer un compte : il suffit d'un identifiant et d'un mot de passe (notez les pour ne pas les perdre sinon vous devrez tout recommencer)
* Aller ensuite dans "mon profil" en haut à gauche
* décocher la case "Afficher seulement les champs requis ou recommandés"
* **Entrez votre prénom et l'initiale de votre nom** que je puisse ensuite vous identifier. (Côté données personnelles, pas la peine de mettre votre nom en entier. Personne n'a besoin de vous identifier hors moi.)
* Valider en bas de la page

### Rejoindre le groupe

* revenir à la page d'accueil de France-IOI
* aller dans "groupes et classes" (menu à gauche)
* rejoindre le groupe "nsibeaupre" (je devrai valider vos inscriptions dans le groupe mais cela ne vous empêchera pas de commencer les exercices).

### Travail à faire

* Les exercices 1 à 10 du [chapitre 2 du niveau 1 : "Répétitions d'instructions"](http://www.france-ioi.org/algo/chapter.php?idChapter=643)


### Remarque

* Les exercices de France-ioi dans cette section là n'utilisent pas la variable de boucle (même si des corrections mettent `for loop in range(...)` plutôt que `for _ in range(...)`)

* Essayer évidemment de trouver les solutions les plus concises.