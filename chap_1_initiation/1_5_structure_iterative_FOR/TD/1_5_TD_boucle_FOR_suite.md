# TD boucle `For` suite
---

### Exercice : Tortue 2

On peut également contrôler le crayon à l'aide de plusieurs instructions

|Instructions |description|   
|:---:|:---:|     
|`up()`|relève le crayon|  
|`down()`|abaisse le crayon (_le dessin peut reprendre_)|  
|`goto`_(x, y)_|se déplace au point de coordonnées données (`x` et `y` : `int` ou `float`| 

Testez le code suivant:

```python
forward(100)
up()
forward(50)
down()
forward(100)
```

__En utilisant les fonctions précédentes et une `for`, définissez les fonctions suivantes__


__a)__ `trois_carres(a)` dessinant trois carres d'arête `a` alignés horizontalement et séparés d'une distance `a`  

_Exemple_ : `trois_carres(100)`

![trois_carres_100](./fig/fig_tortue_trois_carres.jpg)

__b)__ `grille_horizontale(x, a)` dessinant `x` carres d'arête $a$ accolés horizontalement.  

_Exemple_ :`grille_horizontale(7, 80)`

![grille_horizontale](./fig/fig_tortue_9.jpg)  

_Conseils_: _utilisez des petites distances de l'ordre de 10 à 20 pixels, et peu d'alternances pour que le dessin "reste" dans la fenêtre_ 
  
__c)__ `grille_verticale(y, a)` dessinant `y` carres accolés verticalement.  

_Exemple_ :`grille_verticale(4, 80)`

![grille_verticale](./fig/fig_tortue_10.jpg)  

### Exercice : tortue 3

1. tracer cette forme (longueur du trait 100px, côté du carré 30px):

<img src="fig/fig_tortue_5.jpg" height="200">


2. puis celle-ci (on pourra régler le nombre de motifs utilisés ainsi que le rayon du cercle extérieur):

<img src="fig/fig_tortue_6.jpg" height="350">

3. Réaliser celle-ci (on pourra régler le nombre de motifs utilisés).

Vous chercherez dans la documentation comment tracer des cercles.

On pourra également penser à utiliser une fonction auxiliaire pour augmenter la lisibilité du code.

<img src="fig/fig_tortue_7.jpg" height="350"> 

4. Chaque point est relié aux autres (on pourra faire varier le nombre de points et la distance des points au centre):

<img src="fig/fig_tortue_8.jpg" height="350">

6.

![Challenge 1](./fig/challenge_1.jpg)


7.

![Challenge 2](./fig/challenge_2.jpg)


8.  

![Challenge 3](./fig/challenge_3.jpg)

_AIDE_ : vous utiliserez une fonction annexe permettant de dessiner un rectangle

9.  

![Challenge 4](./fig/challenge_4.jpg)

10.  🥇🥇

![Challenge 5](./fig/challenge_5.jpg)






### Exercice : suite géométrique  *avec ou sans accumulateur*

> Une suite de réels $(u_n)$ est dite géométrique lorsque pour passer d'un terme au suivant, on multiplie toujours par le même nombre (appelé raison de la suite).

_On considèrera ici que toutes les suites commencent au terme de rang 0_ .

>*Exemple* : Soit $(u_n)$ la suite géométrique de premier terme $u_0 = 2$ et de raison $q = 3$.

> Alors $u_0 = 2$, $u_1 = 2 \times 3 = 6$, $u_2 = 6 \times 3 = 18$, $u_3 = 18 \times 3 = 54$, $u_4 = 54 \times 3 = 162$, etc.

__1.__ Ecrire une fonction `suite_geo(premterme,raison,rang)` qui renvoie la valeur du terme de rang $n$ de la suite géométrique de premier terme `premterme` et de raison `raison`.

*Exemple* : l'appel `suite_geo(2,3,4)` doit renvoyer 162.

__3.__ Modifier la fonction précédente afin qu'elle ne renvoie rien mais affiche les termes allant de $u_0$ à $u_n$ (inclus) séparés par une virgule.

__2.__ On *admet* que pour une suite géométrique de premier terme $u_0$ et de raison $q$, on a : $u_n = u_0 \times q^n$.

Ecrire une fonction `suite_geo_2(premterme,raison,rang)` qui fait la même chose que la précédente mais sans l'utilisation d'accumulateur.



### Exercice : suite mathématique  *avec accumulateur*

Soit la suite $(u_n)$, définie par son premier terme (ici terme de rang 0), noté $u_0 = 1$

et par la *relation de récurrence suivante* : $\forall n\in\mathbb{N},\quad u_{n+1} = 2u_n + 3$.

Cette relation indique que pour calculer le terme suivant de la suite, il faut multiplier le terme précédent par 2 puis lui ajouter 3.

__1.__ Vérifier à la main que $u_1 = 5$, $u_2 = 13$ et $u_3 = 29$.

__2.__ Ecrire une fonction `terme()' qui renvoie $u_5$, terme de rang 5 de cette suite.

__3.__ Modifier la fontion pour qu'elle renvoie le terme de rang $n$ de cette suite ($n$ étant passé en paramètre).



## V. Plus dur

### Exercice n°1 : la suite de Fibonacci

La suite de Fibonacci est définie par ses premiers termes $u_0 = 1$, $u_1 = 1$ et par la relation de récurrence suivante : $\forall n\in \mathbb{N}$,\quad $u_{n+2} = u_{n+1} + u_{n}$.

Ainsi, pour calculer le terme suivant, on fait la somme des deux termes précédents.

*Exemples :*

$u_2 = u_1 + u_0 = 1 + 1 = 2$  
$u_3 = u_2 + u_1 = 2 + 1 = 3$  
$u_4 = u_3 + u_2 = 3 + 2 = 5$

__1.__ Ecrire une fonction `terme(n)` (où $n\geqslant 2$) qui affiche les valeurs des termes de la suite du rang $0$ au rang $n$ (inclus).

__2.__ Ecrire une fonction `terme_rang(n)` (où $n\geqslant 2$) qui ne *renvoie* que la valeur du terme de rang $n$. 

__3.__ Ecrire une fonction `quotient(n)` qui retourne la valeur du quotient $\dfrac{u_{n}}{u_{n-1}}$, autrement dit le rapport du termer de rang $n$ et du terme précédent.

__4.__ Faire une recherche sur le nombre d'or et comparer sa valeur avec celle retournée par `quotient(n)` quand $n$ est grand.
