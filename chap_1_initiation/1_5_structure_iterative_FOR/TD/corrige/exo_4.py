def algo1():
    
    for k in range(0, 19, 2): #compléter ici
        print(k)

def algo2():
    for k in range(10):
        print(2 * k)     #compléter ici

def algo3():  #utilisation d'un accumulateur
    
    k = 0
    for _ in range(10):
        print(k)     #compléter ici
        k = k + 2       #compléter ici

def algo3_2(n):  #utilisation d'un accumulateur
    
    k = 0
    for _ in range(n):
        print(k)     #compléter ici
        k = k + 2       #compléter ici
        