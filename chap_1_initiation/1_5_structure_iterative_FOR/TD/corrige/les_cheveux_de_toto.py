def toto_1(nb_jours):
    longueur = 17
    for num_jour in range(1, nb_jours + 1):
        longueur = longueur * 1.3
        print("jour n° :", num_jour, ":", longueur)

def toto_2(nb_jours):
    longueur = 17
    for num_jour in range(1, nb_jours + 1):
        longueur = longueur * 1.3
        if longueur >=50:
            longueur = longueur / 2
        print("jour n° :", num_jour, ":", longueur)