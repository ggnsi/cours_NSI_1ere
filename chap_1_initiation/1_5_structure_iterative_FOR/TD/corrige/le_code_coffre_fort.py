import math

def coffre_1(code_depart, num_jour):
    code = code_depart
    for _ in range(num_jour):
        code = 2 * code + 3
    return code

def coffre_2(code_depart, num_jour):
    code = code_depart
    print("code jour 0 :", code)
    for jour in range(1, num_jour + 1):
        code = 2 * code + 3
        print("code jour", jour,":", code)
    
def coffre_3(code_depart, num_jour):
    code = code_depart
    print("code jour 0 :", code)
    for jour in range(1, num_jour + 1):
        code = 2 * code + 3
        if code >= 10000:
            code = code // 1000
        print("code jour", jour,":", code)
        
def coffre_4(code_depart, num_jour):
    code = code_depart
    print("code jour 0 :", code)
    for jour in range(1, num_jour + 1):
        code = 2 * code + 3
        if code >= 10000:  # faire les calculs suivants sur un exemple, type 13309 pour voir le fonctionnement
            code_temp = code / 100
            partie_entiere = code // 100
            code = int((code_temp - partie_entiere) * 100)
        print("code jour", jour,":", code)