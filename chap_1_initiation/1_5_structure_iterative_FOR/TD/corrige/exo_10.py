from turtle import *

def carre(cote):
    for _ in range(4):
        forward(cote)
        left(90)

def carres_tournants(n):
    for _ in range(n):
        carre(100)
        left(360 / n)

def carres_imbriques(n):
    lgr_cote = 60
    for _ in range(n):
        carre(lgr_cote)
        lgr_cote = lgr_cote + 40
        
def spirale_carre(nb_cotes):
    speed(10)
    lgr = 10
    for _ in range(nb_cotes):
        forward(lgr)
        right(90)
        lgr = lgr + 10

def grille(x, y, a):
    
    for _ in range(y):
        for _ in range(x):
            carre(a)
            forward(a)
        left(180)
        forward(x * a)
        right(90)
        forward(a)
        right(90)

    
    
    
    
    
    