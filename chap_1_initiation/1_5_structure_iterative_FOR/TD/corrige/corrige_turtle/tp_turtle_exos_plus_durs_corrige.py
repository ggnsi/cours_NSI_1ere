from turtle import *
import math

width(3)
speed("fast")

def fig_1():
    right(45)
    forward(100)
    left(45)
    color("red")
    for _ in range(4):
        forward(30)
        right(90)
    color("black")

def fig_2(n):
    for _ in range(n):
        figure_1()
        left(360/n)

def fig_3(n, rayon): #première manière de faire
    up()
    goto(0,-rayon)
    down()
    circle(rayon)
    up()
    goto(0,-rayon/2)
    down()
    circle(rayon/2)
    up()
    goto(0,0)
    down()
    for _ in range(n):
        circle(rayon/2)
        left(360/n)
    
# autre manière de faire : avec une fonction cercle(distance, rayon)

def cercle(distance, rayon): #trace un cercle de rayon après avoir avancé devant de distance
    forward(distance)        #revient à l'origine à la fin, crayon levé
    right(90)                #le crayon doit être levé avant d'appeler la fonction
    forward(rayon)
    left(90)
    down()
    circle(rayon)
    up()
    goto(0,0)
    
def fig_3_bis(n, rayon):
    up()
    cercle(0,rayon)
    cercle(0,rayon/2)
    for _ in range(n):
        cercle(rayon/2, rayon/2)
        left(360/n)

def fig_4(rayon, nb_points):
    up()
    cercle(0,rayon) #pour le cercle extérieur
    
    for k in range(nb_points - 1): #le dernier point aura déjà été relié à tout le monde donc nb_points-1 répétition
        angle_depart = k * 2 *math.pi/nb_points +  math.pi/nb_points
        x_depart = rayon * math.cos(angle_depart)
        y_depart = rayon * math.sin(angle_depart)
        
        for j in range(1, nb_points-k):
            x_arrivee = rayon * math.cos(angle_depart + j * 2 *math.pi / nb_points)
            y_arrivee = rayon * math.sin(angle_depart + j * 2 *math.pi / nb_points)
            goto(x_depart,y_depart)
            down()
            goto(x_arrivee, y_arrivee)
            up()
                
                
                
                
                