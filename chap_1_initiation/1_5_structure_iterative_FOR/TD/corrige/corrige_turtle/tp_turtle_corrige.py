from turtle import *

width(3)
speed("fast")

def carre(cote):
    for _ in range(4):
        forward(cote)
        left(90)

def carres_tournants(n):
    for _ in range(n):
        carre(100)
        left(360/n)

def carres_imbriques(n):
    longueur = 60
    for _ in range(n):
        carre(longueur)
        longueur +=40

def spirale_carre(n):
    longueur = 10
    for _ in range(n):
        forward(longueur)
        right(90)
        longueur +=10

def trois_carres(a):
    for _ in range(3):
        carre(a)
        up()
        forward(2*a)
        down()

def grille_horizontale(x, a):
    for _ in range(x):
        carre(a)
        forward(a)

def grille_verticale(y, a):
    for _ in range(y):
        carre(a)
        left(90)
        forward(a)
        right(90)

def grille(x, y, a):
    for _ in range(y):
        grille_horizontale(x, a)
        left(90)
        forward(a)
        left(90)
        forward(x*a)
        left(180)
        
grille_verticale(4, 80)