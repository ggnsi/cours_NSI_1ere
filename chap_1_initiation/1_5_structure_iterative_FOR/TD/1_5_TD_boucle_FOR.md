# TD boucle `For`

### Exercice 1 : l'instruction `range(...)`

Déterminer à chaque fois l'instruction `range(...)` permettant de générer la liste des chiffres inscrits.
Vous pouvez vérifier en tapant dans la console `list(range(...))`

_Exemple_ : 0, 1, 2, 3, 4 : `range(5)`

1. `4, 5, 6, 7, 8` :
2. `1, 2, 3, 4, 5, 6` : 
3. `1, 3, 5, 7, 9` : 
4. `6, 5, 4, 3, 2, 1, 0` : 
5. `12, 10, 8, 6, 4` :

### Exercice 2

1. Ecrivez une fonction `manege()` qui fait faire 10 tours de manège en affichant un message à chaque tour: `"C'est le tour n°..."`

2. Ecrivez une fonction `manege_2(n)`, où `n` est un entier positif qui fait faire `n` tours de manège en affichant un message à chaque tour: `"C'est le tour n°..."`

### Exercice 3

1. Ecrire une fonction `fonc_1()` qui **affiche** les uns en dessous des autres et par ordre croissant de tous les entiers compris entre 1 à 10 (inclus).

2. Ecrivez une fonction `fonc_2()` qui **affiche** les uns en dessous des autres et par ordre croissant de tous les entiers compris entre 1 à 10 (inclus) et indique pour chacun si celui-ci est pair ou impair:

```Python
"1 est impair"
"2 est pair"
...
"10 est pair"
```

3. Remplacez les **pointillés** de la fonction `plus()` ci-dessous afin qu'elle réalise la même chose que `fonc_1()` (interdiction de modifier le reste) 

```Python
nombre = 0
for _ in range(10):
    nombre = ...
    print(nombre)
```

4. En vous inspirant de ce qui a déjà été fait, créez deux fonctions différentes `moins()` et `moins_2()` qui réalisent de deux manières différentes l'**affichage** les uns en dessous des autres et par ordre décroissant de tous les entiers compris entre 15 à 0 (inclus).


### Exercice 4

1. Compléter chacun des algorithmes suivants afin que la fonction affiche les 10 premiers entiers pairs positifs : `0, 2, 4, 6, ..., 18`

```Python
def algo1():
    
    for k in range(.....): #compléter ici
        print(k)
```

```Python
def algo2():
    for k in range(10):
        print(.....)     #compléter ici
```

```Python
def algo3():  #utilisation d'un accumulateur
    
    k = 0
    for _ in range(10):
        print(.....)     #compléter ici
        k .....          #compléter ici
```

2. Modifier la fonction `algo3()` afin d'obtenir l'affichage des `n` premiers entiers pairs (`n` étant un entier positif)

3. (Si reste fini)  Modifier la fonction `algo3()` afin  que les nombres soient affichés séparés par le caractère `-`.


### Exercice 5

1. Compléter la fonction suivante afin qu'elle renvoie la moyenne des 3 nombres entrés par l'utilisateur.

```Python
def moyenne():
        
    s = 0
    for k in range(1,4):
        
        nbr = float(input("votre nombre : "))  # l'utilisateur entre son nombre converti en flottant 
        s ...... 
        
    return .....  
```


2. Modifier la fonction afin qu'elle renvoie la moyenne de _n_ nombes entrés (_n_ étant un paramètre).



### Exercice 6 : les cheveux de Toto

Au jour `0`, la longueur des cheveux de Toto est de `17 cm` et chaque jour, la longueur de ses cheveux est multipliée par `1.3` (Toto met de l'engrais sur ses cheveux).

1. Créez une fonction `toto_1(nb_jours)` qui affiche comme indiqué ci-dessous la longueur des cheveux de Toto les jours numéros 1, 2, ..., `nb_jours`

_Elements de vérification_ :

```Python
>>> toto_1(5)
jour n° : 1 : 22.1
jour n° : 2 : 28.730000000000004
jour n° : 3 : 37.349000000000004
jour n° : 4 : 48.553700000000006
jour n° : 5 : 63.11981000000001
```

2. Sa maman ne supporte pas les cheveux de plus de `50 cm` de long. Dès que leur longueur fini un jour par dépasser  ou être égale à `50 cm`, elle les coupe de la moitié de leur longueur (Si par exemple les cheveux font 64 cm de long, elle les coupe à 32 cm).

    Créez une fonction `toto_2(nb_jours)` qui affiche en tenant compte de cette contrainte la longueur des cheveux de Toto les jours numéros 1, 2, ..., `nb_jours`
    
    _Elements de vérification_ :
    
```Python
>>> toto_2(8)
jour n° : 1 : 22.1
jour n° : 2 : 28.730000000000004
jour n° : 3 : 37.349000000000004
jour n° : 4 : 48.553700000000006
jour n° : 5 : 31.559905000000004
jour n° : 6 : 41.027876500000005
jour n° : 7 : 26.668119725000004
jour n° : 8 : 34.6685556425
```

### Exercice 7

Créez deux fonctions différentes `somme_carres_1()`  et `somme_carres_2()`qui **retournent** la somme des carrés des entiers compris entre `0` et `10` :

* Dans la première, vous avez le droit d'utiliser la variable de boucle ;
* Dans la deuxième, vous n'avez PAS le droit d'utiliser la variable de boucle.

### Exercice 9: plus dur

> Définition du nombre `factoriel(n)`, pour `n` entier positif :

> `factoriel(0) = 1`

> Si `n >= 1`, `factoriel(n) = 1 x 2 x 3 x ... x n`
>
> *Exemples* :

> `factoriel(4)= 1 x 2 x 3 x 4 = 24`

> `factoriel(5)= 1 x 2 x 3 x 4 x 5 = 120`  


Ecrire une fonction `facto(n)` qui renvoie la valeur de `factoriel(n)`.

On pourra commencer par supposer que `n >= 1` puis s'intéresser ensuite au cas `n = 0`.

### Exercice 10 :  Franklin la tortue ...

La module [`turtle`](https://docs.python.org/3.3/library/turtle.html?highlight=turtle) intégré à la bibliothéque standard de python permet de réaliser des dessins géométriques à l'aide d'une _tortue virtuelle_.  
Après avoir présenté quelques fonctionnalités de base, nous utiliserons ce module pour illustrer les boucles bornées vues en cours.

|Mouvements|méthode associée|nature du paramètre| type de paramètre|  
|:---:|:---:|:---:|:---:|    
|__Avancer__|`forward`_(d)_|distance parcourue en pixels| `int` ou `float`|  
|__pivoter à droite__|`right`_(a)_|angle de rotation de la tête en degré| `int` ou `float`|  
|__pivoter à gauche__|`left`_(a)_|angle de rotation de la tête en degré| `int` ou `float`|  
 

1. Tester l'exemple suivant : 

```python
from turtle import *
forward(100)
left(90)
forward(100)
right(90)
forward(50)
right(90)
forward(150)
```

_Remarques_ :  

*  La tortue commence toujours au point de coordonnées(0,0) situé au centre de la fenêtre `Python Turtle Graphics`.  
  
*  Pour réaliser un nouveau dessin, fermer d'abord cette fenêtre sinon la tortue repartira de l'état final précédent.  

* la fonction `reset()` efface l'écran et recentre la tortue.
  

__a)__ Définissez une fonction `carre(cote)`, où `cote` est un entier positif, permettant de dessiner un carré de longueur `cote` (la longueur de son arête en pixels). 
Codez cette fonction :
* d'abord sans boucle 
* puis avec une boucle `for`


__b)__ À l'aide de votre fonction `carre()`, définissez une fonction nommée `carres_tournants(n)` qui dessine `n` carrés de côté `100` pivotant autour d'un sommet commun (l'origine) pour faire un tour complet. La figure suivante montre l'exemple pour n=5.

![carres tournants](./fig/fig_tortue_2.jpg)  

[^2] 

__c)__ À l'aide de votre fonction `carre()`, définissez une fonction nommée `carres_imbriques(n)` qui dessine `n` carrés comme indiqués sur la figure ci-dessous.  
La longueur du côté du premier carré sera de `60 px` et les longueurs des côtés des carrés augmenteront à chaque fois de `40 px`.


*Exemple* : carres_imbriques(4)

![carres_imbriques](./fig/fig_tortue_1.jpg)


__d)__ Definissez une fonction `spirale_carre()` permettant de réaliser une spirale comme celle ci-dessous.


![spirale carré](./fig/fig_tortue_3.jpg)  


A chaque tour le trait est plus long de 10 pixels. Vous pouvez commencer par ne faire tracer que 20 segments au départ puis modifier votre fonction en `spirale_carre(n)` permettant de tracer `n` segments.

*Remarque* : Vous pouvez réglez la vitesse de la tortue à l'aide de la méthode [`speed`](https://docs.python.org/3.3/library/turtle.html?highlight=turtle#turtle.speed)

__e)__  `grille(x, y, a)` dessinant une grille de x sur y carrés d'arête $a$.

*Exemple*: `grille(5,3,60)`

![grille](./fig/fig_tortue_4.jpg)  



### Exercice 11 : de jolis motifs :-)

1. Ecrire une fonction `motif()` qui affiche la figure suivante composée de `15` lignes comportant chacune `50` fois le chiffre `1`.

Il est **interdit** d'écrire plus d'une fois le chiffre `1` dans une fonction `print()` (donc de faire écrire d'un coup toute la ligne !)  
Il est **interdit** d'utiliser `"1" * 50`.

_Aide_ : pensez à utiliser le paramètre `end=` de la fonction `print()`

_Aide_ : l'instruction `print()` seule permet de passer à la ligne

```Python
>>> motif()
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
11111111111111111111111111111111111111111111111111
```

2. Ecrire une fonction `damier()` qui affiche la figure suivante comportant 20 lignes (pas d'espace entre les lignes), chaque ligne étant composée de 30 caractères.

Vous n'avez le droit **qu'à 2 éléments affichés au maximum par fonction `print()`** 

```Python
101010101010101010101010101010       
010101010101010101010101010101          
101010101010101010101010101010       
010101010101010101010101010101          
101010101010101010101010101010       
010101010101010101010101010101          
101010101010101010101010101010       
010101010101010101010101010101          
101010101010101010101010101010       
010101010101010101010101010101          
101010101010101010101010101010       
010101010101010101010101010101          
101010101010101010101010101010       
010101010101010101010101010101          
101010101010101010101010101010       
010101010101010101010101010101          
101010101010101010101010101010       
010101010101010101010101010101          
101010101010101010101010101010       
010101010101010101010101010101          
```


### Exercice 12 : 

1. Ecrire une fonction `triangle(n)` qui affiche la figure suivante comportant n lignes.  
   Il est **interdit** d'utiliser la concaténation multiple de chaînes de caractères (ex : `5 * 'x'`).

```Python
x   
xx   
xxx  
xxxx  
xxxxx  
xxxxxx  
xxxxxxx
```

2. Ecrire des fonctions utilisant des boucles permettant d'obtenir l'affichage suivant.
   Il est **interdit** d'utiliser la concaténation multiple de chaînes de caractères (ex : `5 * 'x'`).

``` Python
OOOOOOOOO
OOOOOOOO
OOOOOOO
OOOOOO
OOOOO
OOOO
OOO
OO
O
```

3. Ecrire des fonctions utilisant des boucles permettant d'obtenir l'affichage suivant.
   Il est **autorisé** d'utiliser la concaténation multiple de chaînes de caractères.

```Python
0000000000
111111111
22222222
3333333
444444
55555
6666
777
88
9
```

### Exercice 13 : 

Ecrire une fonction `table()` qui affiche les tables de multiplication des dix premiers entiers non nuls, chaque table étant sur une ligne.

```Python
0 1 2 3 4 5 6 7 8 9  
0 2 4 6 8 10 12 14 16 18  
0 3 6 9 12 15 18 21 24 27  
...  
0 9 18 27 36 45 54 63 72 81
```

_Aide_ : pensez à utiliser le paramètre `end=` de la fonction `print()`


### Exercice 14

Ecrivez un programme qui affiche tous les nombres entre 1 et 100 avec les exceptions suivantes :

* Il affiche "Fizz" à la place du nombre si celui-ci est divisible par 3.
* Il affiche "Buzz" à la place du nombre si celui-ci est divisible par 5 et non par 3.

```Python
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11

```

Ensuite, améliorez votre programme pour qu'il affiche "FizzBuzz" à la place des nombres divisibles à la fois par 3 et par 5.

Attention: Cet exercice a de nombreuses solutions possibles et constitue un test d'entretien d'embauche classique qui élimine un nombre significatif de candidats. Accrochez-vous pour le réussir !

```Python
13
14
FizzBuzz
16
17
Fizz
19
Buzz
```

Les deux exercices qui suivent ont été traduits à partir du [cours d'initiation à la programmation du MIT](https://www.edx.org/course/introduction-to-computer-science-and-programming-7).

### Exercice 15 : Le code du coffre fort

Toto possède un coffre fort évolué dont le code change chaque jour.  
Le jour numéro `0`, il choisit un chiffre entre `0` et `10` appelé `code_depart`.

Puis, chaque jour le nouveau code est déterminé en multipliant par 2 le code du jour précédent et en lui ajoutant 3.

_Exemple_ : si le code le jour `0` est `4`, le code le jour `1` sera `2 * 4 + 3 = 11`.

1. Toto n'est pas très fort en calcul alors pour l'aider, écrire une fonction `coffre_1(code_depart, num_jour)` qui **renvoie** le code du coffre au bout de `n` jours.

_Elements de vérification_ : 

```Python
>>> coffre_1(4, 10)
7165
>>> coffre_1(2, 15)
163837
```

2. Créez une fonction `coffre_2(code_depart, num_jour)` qui **affiche** tous les codes du jour `0` au jour `num_jour`.

_Elements de vérification_ : 

```Python
>>> coffre_2(4, 8)
code jour 0 : 4
code jour 1 : 11
code jour 2 : 25
code jour 3 : 53
code jour 4 : 109
code jour 5 : 221
code jour 6 : 445
code jour 7 : 893
code jour 8 : 1789
```

3. Si le code du jour devient supérieur ou égal à `10000`, on le remplace par le code obtenu en en gardant que ses deux premiers chiffres.  
    Exemple : si le code devait être `12543`, on le remplace par `12`.
    
    Créez une fonction `coffre_3(code_depart, num_jour)` qui **affiche** tous les codes du jour `0` au jour `num_jour` en tenant compte de cette modification.   

_Elements de vérification_ : 

```Python
>>> coffre_3(10, 10)
code jour 0 : 10
code jour 1 : 23
code jour 2 : 49
code jour 3 : 101
code jour 4 : 205
code jour 5 : 413
code jour 6 : 829
code jour 7 : 1661
code jour 8 : 3325
code jour 9 : 6653
code jour 10 : 13
```

4. (** dur : si tout le reste fini**) : Cette fois ci, si le code du jour devient supérieur ou égal à `10000`, on le remplace par le code obtenu en en gardant que ses deux derniers chiffres.

    Modifiez votre fonction `coffre_3(code_depart, num_jour)` en conséquence pour créer une fonction `coffre_4(code_depart, num_jour)`

### Exercice 16 : années bissextiles le retour

_Rappel_ : Depuis l'ajustement du calendrier grégorien en 1582, les années sont bisextilles si l'année est divisible par 4 et non divisible par 100 ou encore si l'annnée est divisible par 400.

Créer une fonction `annees_bi()` qui, pour la période allant de 1600 à 2021 (inclus tous les deux) :

* Affiche séparées par un espace la liste de toutes les années bissextiles ;
* affiche le nombre d'année bissextiles (avec un petit texte avant) ;
* affiche le nombre d'année non-bissextiles (avec un petit texte avant);

_Prolongement_ :

* Modifier la fonction précédente afin qu'elle admette deux paramètres `annee_debut`, `annee_fin` définissant la période étudiée : `annees_bi(annee_debut, annee_fin)` 
* Tester dans la fonction si `annee_debut` est bien inférieure ou égale à `annee_fin`. Afficher un message d'erreur correspondant si ce n'est pas le cas et quitter la fonction
* Si `annee_debut` est strictement inférieure à 1582, vous ne tiendrez compte QUE des années à partir de 1582

_Elements de vérification_ :

```Python
annees_bi()
1600 1604 1608 1612 1616 1620 1624 1628 1632 1636 1640 1644 1648 1652 1656 1660 1664 1668 1672 1676 1680 1684 1688 1692 1696 1704 1708 1712 1716 1720 1724 1728 1732 1736 1740 1744 1748 1752 1756 1760 1764 1768 1772 1776 1780 1784 1788 1792 1796 1804 1808 1812 1816 1820 1824 1828 1832 1836 1840 1844 1848 1852 1856 1860 1864 1868 1872 1876 1880 1884 1888 1892 1896 1904 1908 1912 1916 1920 1924 1928 1932 1936 1940 1944 1948 1952 1956 1960 1964 1968 1972 1976 1980 1984 1988 1992 1996 2000 2004 2008 2012 2016 2020 
Nombre d'années bissextiles 103
Nombre d'années non bissextiles 319
```

_Pour  le premier prolongement_

```Python
>>> annees_bi(1650, 1750)
1652 1656 1660 1664 1668 1672 1676 1680 1684 1688 1692 1696 1704 1708 1712 1716 1720 1724 1728 1732 1736 1740 1744 1748 
Nombre d'années bissextiles 24
Nombre d'années non bissextiles 77
```

_Pour  le deuxième prolongement_

```Python
>>> annees_bi(1650, 1550)
l'année de début doit être inférieure à celle de fin
```

```Python
>>> annees_bi(1500, 1650)
1584 1588 1592 1596 1600 1604 1608 1612 1616 1620 1624 1628 1632 1636 1640 1644 1648 
Nombre d'années bissextiles 17
Nombre d'années non bissextiles 52
```
