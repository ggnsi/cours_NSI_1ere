# TP Routage - protocole RIP

## Simulation à la main du protocole RIP

On utilisera ici la version "simplifiée" du protocole RIP vue en cours (sans se soucier de l'existence es timers définis dans le protocole).

On utilisera la [feuille tableur `tables_reseau_1` à télécharger](./docs/tables_reseau_1.xls) pour représenter les évolutions des tables de routage.  
Pour la suite, n'oubliez pas que le copier/coller existe.

On considère la topologie réseau présentée ci-dessous.  

![image du reseau](./img/topo_reseau.jpg)

Pour les interfaces en lien avec les deux réseaux locaux, les tables de routage des routeurs A et E ont été paramétrées.

1. On considère qu'à l'allumage, les routeurs s'initialisent avec les réseaux auxquels ils sont connectés de manière immédiate.    
Compléter les tables de routage sur la feuille tableur.

2. Le routeur D envoie sa table de routage à ses voisins. Actualiser les tables des routeurs C et E.

3. Le routeur C envoie sa table de routage à ses voisins. Actualiser les tables des routeurs A, B et D.

4. Le routeur B envoie sa table de routage à ses voisins. Actualiser les tables des routeurs A, C et E.

5. Quelle serait la table de routage de A si le routeur B avait envoyé la sienne avant C ?

6. Le routeur E envoie sa table de routage à ses voisins. Actualiser les tables des routeurs A, B et D.

7. Remplir les tables de routages des différents routeurs après convergence.

## Utilisation d'une simulation

Lancier **Filius** et charger le fichier [reseau_1](./fichiers/reseau_1.fls).
Lancer la simulation, afficher les bureaux de `pc_1` et `pc_2` et y lancer une console.

1. A l'aide de la console, déterminer les adresse ip de `pc_1` et `pc_2`. Faire afficher leurs tables `ARP`.
2.  
    * Exécuter une commande ping depuis `pc_1` vers `pc_2`.
    * Faire afficher les tables `ARP` de `pc_1` et `pc_2`.
    * En observant les données échangées par `pc_1` et `pc_2`, expliquez les différentes étapes réalisées.  A quel moment les tables `ARP` des `pc` ont-elles été actualisées?

3. L'adresse `IP` de `pc_3` est `192.168.1.1`.  
Effectuez un ping de `pc_1` vers `pc_3`.  
Quelle est la réponse obtenue ? Pourquoi ?
4. Quittez le mode simulation et paramétrez `pc_1` pour résoudre le problème (**corrigé : etape_1**) puis relancez la simulation.
5. 
    * Effectuez un ping de `pc_1` vers `pc_3`. 
    * Affichez la table `ARP` de `pc_1`.
    * Utilisez la commande `traceroute` pour visualiser la route utilisée par les paquets lors d'une communication entre `pc_1` et `pc_3`.

## Panne d'un lien (situation similaire si panne d'un routeur)

On utilisera la [feuille tableur `tables_reseau_2` à télécharger](./docs/tables_reseau_2.xls). Les tables intiales des routeurs après convergence y sont écrites.

Le lien entre les routeurs B et E est rompu et le routeur B se rend compte du fait que le routeur E lui est inaccessible.

1. Comment les routeurs B et E modifient-t-ils leurs tables de routage ?
2. B transmet sa table à A et B et E transmet sa table à D. Actualiser les tables.
3. D transmets sa table à C. Actualiser la table de C.
4. C transmets sa table à A et à B. Actualiser les table de A et B.
5. Dans votre simulation Filius, supprimez le lien entre B et E et vérifiez la nouvelle route utilisez par les paquets échangés entre `pc_1` et `pc_3`.


