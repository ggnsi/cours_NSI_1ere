## Routage sur internet

![routage_internet](./img/reseau_ip_routage.png)

### Acheminement de proche en proche

Pour envoyer un paquet d'une machine source à une machine de destination, la machine source commence par envoyer les trames au routeur auquel elle est connectée. Ce routeur, envoie à son tour les trames au routeur suivant, et ainsi de suite jusqu'au dernier routeur qui envoie les trames à l'ordinateur de destination.

### Choisir la bonne direction ?

Chaque routeur dispose d'une table de routage, c'est à dire d'un ensemble d'informations permettant au routeur de savoir à quel autre routeur transmettre les trames en fonction de l'adresse IP de destination.
timbre

> Analogie postale
>
> Un courrier envoyé depuis le Japon en France, arrivera d'abord au centre de tri national, puis sera envoyé au centre de tri régional (en fonction du département), puis au bureau de poste de la ville et enfin, il sera acheminé par le facteur à la bonne rue et au bon numéro.
>
>Le centre de tri national doit disposer de la liste des centres de tri régionaux et sait comment leur envoyer le courrier. Il n'a pas besoin d'autres informations. De même, chaque centre de tri régional dispose de la liste des bureaux de poste des ville de sa région et sait comment leur envoyer les courriers. Ils n'ont besoin d'autres informations.

Les tables de routage sont régulièrement mises à jour par des échanges d'informations entre les routeurs. Ils s'agit ici de protocoles des routage.

### Quelle fiabilité ?

Le routage est un procédé très souple. En effet, si un routeur tombe en panne, les tables de routages des routeurs du réseau seront mises à jour et les données seront transférées par d'autres routes.

Mais cette souplesse a une contrepartie inévitable, le manque de fiabilité. Par exemple, si un routeur est saturé, il va détruire les trames qu'il reçoit ; si une trame circule trop longtemps sans atteindre sa destination, elle sera détruite...

S'il est utilisé pour le transport, le protocole **TCP** s'assurera alors du renvoi des trames manquantes.