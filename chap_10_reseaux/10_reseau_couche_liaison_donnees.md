# Couhe de liaison des données : transmission d'un paquet dans un réseau local

## Problématique

> **Nous allons ici expliquer comment les données issues de la couche supérieure (couche réseau) vont être véhiculées entre deux ordinateur d'un même sous-réseau..**
>
> **Les mécanismes expliqués ici sont donc situés dans la couche _Liaison des données/Physique_** et on concidère utiliser le protocole **Ethernet**.

![modele_tcp_ip](./img/OSI_TCP_IP_partiel.png)

On considèrera une situation simplifiée où :
* tous les hôtes (ici des ordinateurs) qui font partie d'un même sous-réseau ;
* ils sont connectés entre eux via un switch (commutateur);
* chaque ordinateur ne possède qu'une carte réseau.

![image réseau local](./img/liaison_donnees_img1_sans_mac.png)


## Adresse Mac

Chaque carte réseau possède un identifiant unique au monde déterminé lors de sa fabrication. 

> Cet identifiant est appelé **adresse MAC** (**M**édia **A**ccess **C**ontrol)
>
> Elle est constituée de 6 octets (48 bits) et on la note le plus souvent en écrivant la valeur de chaque octet en hexadécimal (sur 2 digits) séparés par deux points.
>
> _Exemple_ : `01:23:C5:B7:89:AB`

C'est cette adresse qui est utilisée pour l'adressage des paquets de données dans le sous-réseau.

Quelques [compléments sur les adresses MAC](./10_adresse_mac.md)

### A faire : Déterminer l'adresse mac de la carte réseau de votre ordinateur

Sous Windows :
* aller dans 'Démarrer' puis 'exécuter', taper 'cmd' puis valider ;
* entrer la commande 'ipconfig /all' ou 'getmac /v'

Sous Linux, entrer la commande 'ifconfig'

_Remarque_ : en tapant la commande `arp -a`, vous affichez la `table arp` constituée de l'ensemble des adresses mac des machines connues par votre pc (je simplifie ici)

## Trame ethernet

Considérons notre sous-réseau (complété avec les adresses mac des cartes réseau de chaque ordinateur) :

![image réseau local](./img/liaison_donnees_img1_avec_noms.png)

Si **PC A** souhaite envoyer les données à **PC B** :
* il ajoute :
  * en-tête son adresse MAC (celle de la source) ainsi que celle du destinataire (celle du **PC B**) ;
  * il ajoute également à la fin un **CRC** (code permettant de vérifier s'il n'y a pas eu d'erreur de transmission).

  On obtient une **trame ethernet**.
  
  ![trame ethernet](./img/Trame_Ethernet.png)
  

* Il envoie cette trame au **switch**. Celui-ci possède en interne une table à l'intérieur de laquelle il stoque l'adresse MAC de chaque ordinateur qui lui  est connecté ainsi que le port qui lui correspond (à quel endroit est branché le câble menant à cet ordinateur).

* le switch lit l'adresse MAC du destinataire, regarde dans sa table interne sur quel port celui-ci est connecté et lui transfère (à lui seul) la trame ;

* A l'arrivée, **PC B** vérifie avec l'adresse MAC que le message lui est bien destiné et si c'est le cas :

  * il désencapsule les données en supprimant l'entête et le CRC ;
  * il transmet les données à la couche supérieure (couche réseau).

## Compléments

A faire : adresse broadcast / construction des tables arp
