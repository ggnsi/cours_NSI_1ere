# Principe de base des protocoles TCP et IP

## Description du principe des protocoles TCP et IP

> Le protocole TCP se situe dans la couche _Transport_ (celui UDP évoqué également) ;
> Le protocole IP se situe dans la couche _Réseau_.

Quand un ordinateur A "désire" envoyer des données à un ordinateur B, l'ordinateur A "utilise" le **protocole TCP **(Transmission Control Protocol) pour mettre en forme les données à envoyer. On parle alors de **segments TCP**.

Ensuite le **protocole IP** (Internet Protocol) prend le relai et utilise les données mises en forme par le protocole TCP afin de créer des **paquets** des données.  
Après quelques autres opérations qui ne seront pas évoquées ici, les paquets de données pourront commencer leur voyage sur le réseau jusqu'à l'ordinateur B. 

>Il est important de bien comprendre que le protocole IP "encapsule" les données issues du protocole TCP afin de constituer des paquets de données. On parle alors de paquets IP ou de datagrammes

![tcp_ip_1](./img/tcp_ip_1.jpg)

Le protocole IP s'occupe uniquement de faire arriver à destination les paquets en utilisant l'adresse IP de l'ordinateur de destination. Les adresses IP de l'ordinateur de départ (ordinateur A) et de l'ordinateur destination (ordinateur B) sont ajoutées aux paquets de données.

![tcp_ip_2](./img/tcp_ip_2.jpg)

Une fois arrivées à destination (ordinateur B), les données sont "désencapsulées" : on récupère les données TCP contenues dans les paquets afin de pouvoir les utiliser.

![tcp_ip_3](./img/tcp_ip_3.jpg)

Le protocole TCP permet de s'assurer qu'un paquet est bien arrivé à destination. En effet quand l'ordinateur B reçoit un paquet de données en provenance de l'ordinateur A, l'ordinateur B envoie un accusé de réception à l'ordinateur A (un peu dans le genre "OK, j'ai bien reçu le paquet"). Si l'ordinateur A ne reçoit pas cet accusé de réception en provenance de B, après un temps prédéfini, l'ordinateur A renverra le paquet de données vers l'ordinateur B.

![tcp_ip_4](./img/tcp_ip_4.jpg)

À noter qu'il existe aussi le protocole UDP qui ressemble beaucoup au protocole TCP. La grande différence entre UDP et TCP est que le protocole UDP ne gère pas les accusés de réception. Les échanges de données avec UDP sont donc moins fiables qu'avec TCP (un paquet "perdu" est définitivement "perdu" et ne sera pas renvoyé) mais beaucoup plus rapides (puisqu' il n'y a pas d'accusé de réception à transmettre). UDP est donc très souvent utilisé pour les échanges de données qui doivent être rapides, mais où la perte d'un paquet de données de temps en temps n'est pas un gros problème (par exemple le streaming vidéo).

>Il est très important de bien comprendre que TCP/IP repose sur la notion de paquets de données.

Si par exemple on désire envoyer un fichier (son, photo, vidéo ou texte, peu importe, dans tous les cas on envoie une succession de bits) en utilisant TCP/IP, les données qui constituent ce fichier ne seront pas envoyées d'un seul tenant, ces données vont être "découpées" en plusieurs morceaux et chaque morceau sera envoyé dans un paquet différent. Une fois tous les paquets arrivés à destination, le fichier d'origine pourra être reconstitué. Pour aller d'un ordinateur A à un ordinateur B, les différents paquets contenant les données qui constituent notre fichier, ne passeront pas forcement par la même route (cette notion de route sera abordée plus tard), ils pourront emprunter des chemins très différents : en exagérant à peine, pour faire le trajet Paris-Los Angeles, certains paquets pourront passer par l'atlantique alors que d'autres passeront par le pacifique. Si un des paquets n'arrive pas à destination, le fichier ne pourra pas être reconstitué, le paquet "perdu" devra être renvoyé par l'émetteur (voir le système d'accusé de réception décrit ci-dessus).

![tcp_ip_5](./img/tcp_ip_5.jpg)

## Fiabilité

> TCP est un protocole fiable. On entend par fiable **le fait de faire en sorte que tout ce qui arrive est exactement ce qui a été envoyé**.

On a donc les 4 critères suivants :

* Sans perte : les données ne doivent pas être perdues
* Sans erreur : les données ne doivent pas subir d’erreurs (une erreur correspond à un bit qui change pendant le transport)
* Dans l’ordre : les données doivent arriver dans l’ordre
* Sans duplication : les données ne doivent pas arriver en double

Dans la pratique :
* En cas de perte ou erreur, il faut retransmettre si on n’a pas reçu d’acquittement (ACK) au bout d’un certain temps
* Pour détecter une perte ou une duplication, il faut des ACK et numéroter les messages et les ACK
* Pour détecter les erreurs, on utilise les checksum
* Pour retransmettre, il faut conserver les messages envoyés qui n’ont pas encore été acquittés

Beaucoup d’applications utilisent le protocole TCP car elles ne peuvent pas se passer de fiabilité :
* Le web
* La connexion à distance
* Le courrier électronique
* Le transfert de fichiers
* ...

En revanche, les applications multimédias et le DNS n’utilisent pas TCP.

## La trame Ethernet

Nous avons vu avec les protocoles TCP et IP le processus d'encapsulation des données : "IP encapsule TCP". Les paquets IP ne peuvent pas transiter sur un réseau tel quel, ils vont eux aussi être encapsulés avant de pouvoir "voyager" sur le réseau. L'encapsulation des paquets IP produit ce que l'on appelle **une trame**. Il n'est pas question d'étudier en détail ce qu'est une trame, vous devez juste savoir qu'il existe de nombreux types de trames : ATM, token ring, PPP, Ethernet, Wifi...

Si vous utilisez un réseau filaire avec des câbles Ethernet (avec des prises RJ45), la trame sera de type Ethernet (ce qui est le cas pour le réseau du lycée). Si vous utilisez un réseau sans fil Wifi, la trame sera de type Wifi. En fait, la trame Wifi ressemble beaucoup à la trame Ethernet, on peut même dire que la trame Wifi est la variante sans-fil de la trame Ethernet, afin de simplifier les choses, dans la suite, nous évoquerons uniquement la trame Ethernet en ayant à l'esprit que ce qui est dit sur la trame Ethernet et aussi valable pour la trame Wifi.

Nous avons vu que le paquet IP contient les adresses IP de l'émetteur et du récepteur :

![tcp_ip_6](./img/tcp_ip_6.jpg)

Le paquet IP étant encapsulé par la trame Ethernet, les adresses IP ne sont plus directement disponibles (il faut désencapsuler le paquet IP pour pouvoir lire ces adresses IP), nous allons donc trouver un autre type d'adresse qui permet d'identifier l'émetteur et le récepteur : l'adresse MAC (Media Access Control) aussi appelée adresse physique.

![tcp_ip_7](./img/tcp_ip_7.jpg)

Nous avons déjà vu que l'adresse MAC est liée au matériel, chaque carte réseau (Ethernet ou Wifi) possède sa propre adresse MAC.

Au moment de l'encapsulation d'un paquet IP, l'ordinateur "émetteur" va utiliser un protocole nommé ARP (Address Resolution Protocol) qui va permettre de déterminer l'adresse MAC de l'ordinateur "destination", en effectuant une requête "broadcast" (requête destinée à tous les ordinateurs du réseau) du type : "j'aimerais connaitre l'adresse MAC de l'ordinateur ayant pour IP XXX.XXX.XXX.XXX". Une fois qu'il a obtenu une réponse à cette requête ARP, l'ordinateur "émetteur" encapsule le paquet IP dans une trame Ethernet et envoie cette trame sur le réseau.

# Complément : Initiation et terminaison d'une connection TCP

A faire ...

### sources

* Cours de C.Bonnefoy
* A. Willm

