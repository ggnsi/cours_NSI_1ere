# La couche réseau : le protocole IP

## Nécessité d'un autre adressage : l'addresse IP

* les **adresses MAC** liées aux cartes réseaux lors de leurs construction ne permettent pas, de part leur structure, de définir ni d'organiser des sous-réseaux ;
* elles ne permettent donc pas de déterminer si des données sont destinées à un ordinateur auquel on est directement relié (dans le même sous-réseau donc) ou à une machine d'un autre sous-réseau (et donc de **router** les données vers l'extérieur du sous-réseau puis de la faire transiter dans des routeurs) ;
* une machine ou un service (un serveur web par exemple) doit pouvoir être identifié indépendemment du matériel utilisé. Le serveur webdoit rester localisable même si on change la machine qui l'héberge (et donc de carte réseau.

>Un type d'adressage supplémentaire est donc nécessaire :
>
>* pour constituer des sous-réseaux ;
>* pour pouvoir router les données d'un sous-réseau à un autre.
>
> C'est le rôle du protocole IP

![llustration_reseau_0](./img/illustration_reseau_0.jpg)


## Le protocole IP

### L'adressage IP

> Une adresse IP est un identifiant de réseau associée à une machine.

Chaque machine se voit attribuer une **adresse IP** constituée de :
* d'une adresse sur 4 octets (en IPV4) usuellement notés en écriture décimale et séparés par un point (_Exemple_ : `192.168.2.12`)

* d'un **masque** permettant de déterminer à quel sous-réseau elle appartient, représenté lui aussi sur 4 octets.

_Exemple_ : dans le sous-réseau 2, un ordinateur  a pour :
* adresse IP : `192.168.2.1` ;
* masque : `255.255.255.0`

> Le masque permet de déterminer 
>* l'_identifiant du sous-réseau_ auquel appartient l'ordinateur ;
>* l'_identifiant machine_ de l'ordinateur dans ce sous-réseau

### comment déterminer l'identifiant du sous-réseau et l'identifiant machine ?

On écrit d'abord l'adresse IP et le masque en binaire.

Le masque est focément constitué d'une suite de bits à `1` (représentant l'identifiant réseau) suivie d'une suite de bits à `0` (représentant l'identifiant machine).

Par exemple, un masque de sous-réseau valide est `255.255.255.0` parce que son écriture binaire (: `11111111.11111111.11111111.00000000`) est une suite de bits à `1` suivie d'une suite de bits à `0`.

Ici : 
* `192.168.2.1` -> `11000000.10101000.00000010.00000001` ;
* `255.255.255.0` -> `11111111.11111111.11111111.00000000`

>Pour obtenir l'adresse réseau du sous-réseau auquel appartient l'ordinateur, il suffit de réaliser un `ET` logique bit à bit entre l'adresse IP de la machine et son masque (se rappeler de sa table de vérité !) :

> L'identifiant machine dans le sous-réseau correspond à l'ensemble des bits de l'adresse IP non utilisés dans l'adresse du sous-réseau (le reste étant remplacé par des zéros.

_Exemple_ :

```
  11000000.10101000.00000010.00000001 -> 192.168.2.1
& 11111111.11111111.11111111.00000000 -> 255.255.255.0
  ___________________________________
= 11000000.10101000.00000010.00000000 -> 192.168.2.0
```

L'adresse du sous-réseau est donc `192.168.2.0`.

L'identifiant de la machine dans le sous-réseau sera `00000000.00000000.00000000.00000001`

> **Pour savoir si deux ordinateurs sont dans le même sous-réseau**, il suffit :
>
> * de déterminer pour chacun l'adresse du sous-réseau auquel ils appartiennent ;
> * de regarder si c'est la même

**Adresses IP réservées** :

* L'adresse réseau ne peut pas être donnée comme adresse IP à une machine ;
* L'adresse IP, ayant tous les bits de l'identifiant machine à `1`, ne peut également pas être donnée à une machine.

Ainsi, dans notre exemple, tous les ordinateurs dont l'adresse IP est de la forme `192.168.2.x` (avec `x` différent de 0 et 255) sont dans le même sous-réseau que notre ordinateur et lui sont donc directement accessibles sans avoir à passer par un routeur.

##### A faire

1. Pour chacun des adresses IP et masques suivants, retrouver l'identifiant réseau et l'identifiant machine :

   - IP : ``192.166.0.254`` ; Masque : ``255.255.255.0``

   - IP : ``192.168.0.2`` ; Masque : ``255.255.254.0``

   - IP : ``192.168.1.6`` ; Masque : ``255.255.254.0``

2. Si l'identifiant machine est codé sur `n` bits, combien d'identifants machine peut-on attribuer (cad combien de machines pourront appartenir à ce sous-réseau) ?

### Le routage des données

Considérons maintenant la configuration suivante :

![llustration_reseau_1](./img/illustration_reseau_1_serveur.jpg)

**Situation 1** : **les deux machines sont dans le même sous-réseau**

_Exemple_ : la machine 1 (IP : `192.168.0.2`) veut envoyer des données à la machine 3 (IP : `192.168.0.3`) :

* le protocole IP détermine grâce au masque que les deux machines appartiennent au même sous-réseau. ;
* le protocole IP ajoute au données une entête contenant entre autre les adresses IP sources et destination. Cela constitue un **paquet IP**.
  
  ![trame_ip](./img/trame_ip.png)
  
  
* Il regarde dans la table interne qu'il possède s'il connait l'adresse MAC de l'ordinateur destinataire (dans notre exemple `192.168.0.3`)

> Sur votre ordinateur, lancez la commande `arp -a` dans une console. Vous verrez le contenu de la table `arp` actuelle de votre machine.
  
* Si ce n'est pas le cas, il envoie à toutes les machines du réseau une `requête ARP` qui dit `que la machine qui possède l'adresse IP `192.168.0.3` me réponde afin que j'aie son adresse MAC) ;
  
* Il transfère la trame à la couche de liaison des données qui va encapsuler le paquet IP dans une trame ethernet (voir page précédente du cours).

**Situation 2** : **les machines ne sont pas dans le même sous-réseau**

_Exemple_ : la machine 1 (IP : `192.168.0.2`) veut envoyer des données au serveur (IP : `125.45.36.12`)

* le protocole IP détermine grâce au masque que les deux machines ne sont pas dans le même sous-réseau ;
* il va constituer le **paquet IP** comme précédemment ;
* mais il va alors envoyer les données à **la passerelle** qui lui a été définie (ici **le routeur A**) en utilisant l'adresse locale de ce routeur (en utilisant comme expliqué ci-dessus l'adresse MAC du routeur).
* Des **protocoles de routage** permettent aux routeurs de constituer des **tables de routage**.  
  Le routeur A utiliser sa table pour déterminer à qui il doit transmettre ensuite les données.  
  De proche en proche, les données arriveront au serveur.


_Compléments possible_ :
* [sur les adresses IP](./10_adresse_ip.md) : IPV4, IPV6, adresses publiques, privées ;
* masques de sous-réseaux : [vidéo 1](https://www.youtube.com/watch?v=dCWDq2Ty00g), [vidéo 2](https://www.youtube.com/watch?v=3p__ecHwezw)
* 
[Complément possible : masques de sous-réseaux](https://www.youtube.com/watch?v=3p__ecHwezw)
