# Parler la même langue : des protocoles

## Problématique - définition

Le transfert de données entre les équipements nécessite que ces équipements parlent la même langue, suivent les mêmes règles. En informatique on parle de **protocole**.

> Un protocole est un ensemble de règles qui régissent la transmission de l'information sur un réseau. Il existe de nombreux protocoles, chacun spécialisé dans une tâche bien précise.

Chaque protocole a une fonction bien précise et a été conçu pour être indépendant d'autres protocoles.

> Un **service** est une interface (un programme) qui permet de mettre en place un protocole.


## Quelques protocoles 

* *HTTP* : Il gère la communication entre un client et un serveur web pour la transmission de données hypertextes (pages web)
* **POP, SMTP, IMAP** : ce sont des protocoles de relève ou d'envoi de mails ;
* **FTP** (File Transfer Protocol) : protocole de transfert de fichiers ;
* **SSH** (Secure Shell) : protocole de communication sécurisé. C'est à la fois un protocole et un programme informatique ;
* **DHCP** (Dynamic Host Configuration Protocol ou protocole de configuration dynamique des hôtes en français) : protocole réseau dont le rôle est d’assurer la configuration automatique des paramètres IP d’une station ou d'une machine ;
* ...

Plus spécifiquement pour la partie transmission de données dans un réseau :

* **IP** : Internet Protocol ;
* [**TCP** : Transmission Control Protocol](https://fr.wikipedia.org/wiki/Transmission_Control_Protocol) : protocole de contrôle de transmissions ;
* [**UDP** : User Datagram Protocol](https://fr.wikipedia.org/wiki/User_Datagram_Protocol) : également un protocole de transmission de données mais plus simple que **TCP** ;
* [**Ethernet**](https://fr.wikipedia.org/wiki/Ethernet) : protocole de transmission des données dans la couche physique. ;
* **ARP** (Address Résolution Protocol (protocole de résolution d'adresse)) : il assurre la liaison entre l'adresse physique d'une carte réseau (MAC) et l'adresse IP correspondante. 

