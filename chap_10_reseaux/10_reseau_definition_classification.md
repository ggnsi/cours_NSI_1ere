# Réseau : définition, classification

> Un réseau désigne un ensemble de ressources, permettant la circulation de flux (eau, air, huile…) ou d’éléments finis (marchandises, informations, personnes…).

> Un réseau de communication peut être défini comme l’ensemble des ressources matérielles et logicielles liées à la transmission et l’échange d’informations entre différentes entités.

_Exemple_ : Réseau postal, réseau téléphonique. Plus anciennement : [Réseau de sémaphores](https://fr.wikipedia.org/wiki/T%C3%A9l%C3%A9graphe_Chappe)

## Réseau informatique ?

> Un réseau informatique (Data Communication Network ou DCN) est un ensemble d'équipements reliés entre eux et qui échangent des informations.
> Dans les grandes lignes, un réseau est intégralement composé :
> * d'équipements informatiques (ordinateurs, imprimantes, télévisions, objets connectés, ...) ;
> * de matériel réseau qui permet l'interconnection des équipements et le transfert de données entre eux (point d'accès-wifi, switch, routeur, ..)
> * et de liaisons point-à-point (cables, fibres, wifi, ... ) qui relient deux équipements entre eux.

_Exemples d'équipements pouvant faire partie d'un réseau_ :

* Un ordinateur bien sûr ;
* Une console de jeu ;
* Un téléphone ;
* Certaines Télévision et appareil domotique (réfrigérateur, thermostat, volets roulant, ) ;
* Enceintes connectés en permanence via wifi (pas bluetooth) ;
* Une imprimante wifi ou reliée au réseau ;
* Les appareils médicaux dans un hôpital sont presque toujours en réseau (alertes ...) ;



## Classification en fonction de la taille

![reseaux_taille](./img/wan_man_lan.png)

![reseaux_taille](./img/lan_wan_pan.png)

* **LAN : Local Area Network** (ou réseau **local**) fait communiquer des équipements informatiques dans un domaine géographique limité (de l’ordre de quelques kilomètres). Le LAN est très souvent propriété de l’utilisateur ou de la société.)

_Exemple_ : hôpital, lycée, entreprise, habitation

* **MAN : Metropolitan Area Network** fait communiquer des équipement à l'échelle d'une métropole ;
* **WAN : Wide Area Network** fait communiquer des ordinateurs sur de très grandes distances et à l’échelle mondiale. C’est un grand réseau, dont l’utilisateur n’est souvent pas propriétaire.

_Exemple_ : Internet, réseaux des opérateurs ADSL ou GAFAM

_Remarque_ : On peut aussi parler de **PAN : Personal Area Network** à une échelle plus petite.

_Remarque_ : Ne pas confondre **Internet** qui est le réseau et le **world wide web** qui est constitué de toutes les ressources reliées entre elles par des liens hypertextes.

## le réseau internet

![réseau internet](./img/reseaux_internet.png)