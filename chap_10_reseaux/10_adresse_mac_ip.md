# Adresse MAC, adresse IP

Des adresses sont nécessaires pour identifier les machines sur un réseau.

Localement, lorsque votre facteur trie le courrier puis vous le délivre, il utilise **que votre adresse physique** :
* le nom de la ville (si besoin) ;
* votre numéro de maison et le nom de la rue.

**Il n'a pas besoin de savoir quel est le nom de la personne pour le délivrer.**

> L'équivalent de l'adresse physique sera **l'adresse MAC** d'une machine (de son interface réseau)

**Cela ne suffit pas**

L'adresse MAC ne suffit pas pour identifier un service (serveur web, de mail, de fichiers ...) car on peut très bien faire un changement de machine qui l'héberge (et donc l'adresse mac de la machine qui l'héberge).

Il est donc nécessaire d'avoir une adresse dite **logique** qui permet de situer un service ou une machine pour pouvoir échanger des données avec elle.

> Ce sera l'adresse **IP** d'une machine




