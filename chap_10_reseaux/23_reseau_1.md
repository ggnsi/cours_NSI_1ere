# Réseau : définition, types, constituants

> Un réseau désigne un ensemble de ressources, permettant la circulation de flux (eau, air, huile…) ou d’éléments finis (marchandises, informations, personnes…).

> Un réseau de communication peut être défini comme l’ensemble des ressources matérielles et logicielles liées à la transmission et l’échange d’informations entre différentes entités.

_Exemple_ : Réseau postal, réseau téléphonique. Plus anciennement : [Réseau de sémaphores](https://fr.wikipedia.org/wiki/T%C3%A9l%C3%A9graphe_Chappe)

## Réseau informatique ?

> Un réseau informatique (Data Communication Network ou DCN) est un ensemble d'équipements reliés entre eux et qui échangent des informations.
> Dans les grandes lignes, un réseau est intégralement composé :
> * d'équipements informatiques (ordinateurs, imprimantes, télévisions, objets connectés, ...) ;
> * de matériel réseau qui permet l'interconnection des équipements et le transfert de données entre eux (point d'accès-wifi, switch, routeur, ..)
> * et de liaisons point-à-point (cables, fibres, wifi, ... ) qui relient deux équipements entre eux.

_Exemples d'équipements pouvant faire partie d'un réseau_ :

* Un ordinateur bien sûr ;
* Une console de jeu ;
* Un téléphone ;
* Certaines Télévision et appareil domotique (réfrigérateur, thermostat, volets roulant, ) ;
* Enceintes connectés en permanence via wifi (pas bluetooth) ;
* Une imprimante wifi ou reliée au réseau ;
* Les appareils médicaux dans un hôpital sont presque toujours en réseau (alertes ...) ;



## Classification en fonction de la taille

![reseaux_taille](./img/wan_man_lan.png)

![reseaux_taille](./img/lan_wan_pan.png)

* **LAN : Local Area Network** (ou réseau **local**) fait communiquer des équipements informatiques dans un domaine géographique limité (de l’ordre de quelques kilomètres). Le LAN est très souvent propriété de l’utilisateur ou de la société.)

_Exemple_ : hôpital, lycée, entreprise, habitation

* **MAN : Metropolitan Area Network** fait communiquer des équipement à l'échelle d'une métropole ;
* **WAN : Wide Area Network** fait communiquer des ordinateurs sur de très grandes distances et à l’échelle mondiale. C’est un grand réseau, dont l’utilisateur n’est souvent pas propriétaire.

_Exemple_ : Internet, réseaux des opérateurs ADSL ou GAFAM

_Remarque_ : On peut aussi parler de **PAN : Personal Area Network** à une échelle plus petite.

_Remarque_ : Ne pas confondre **Internet** qui est le réseau et le **world wide web** qui est constitué de toutes les ressources reliées entre elles par des liens hypertextes.

## Topologie d'un réseau local

Il existe plusieurs façons de relier des machines informatiques entre elles.  
L'arrangement physique des équipements communiquant entre eux , c’est-à-dire la configuration spatiale du réseau est appelé topologie physique.

### Réseau maillé

>Toutes les stations sont connectées les unes au autres par une liaison point à point.

[réseau maillé](reseau_maille.png)

* Avantages :
  * C’est la plus simple car elle relie deux stations par un câble unique ;
  * C’est la technologie la plus robuste car toute panne d'un matériel ou d'un câble n'empêche pas la transmission des informations sur le reste du réseau.
 
* Inconvénients :

  * Cela implique un coût extrêmement élevé dans le cas de réseaux à grand nombre de stations.
  * Il n'est pas toujours physiquement possible de relier chaque machine à toutes les autres ;
  * Tout machine ajoutée sur le réseau nécessite l'installation d'un nombre très important de nouveaux câbles, ce qui n'est pas toujours possible.
 
### Architecture en étoile

[topologie_étoile](topologie_etoile.png)

>Les équipements sont reliés entre eux par un matériel situé au noeud qui assure la transmission des informations entre les équipements.

**C'est cette topologie qui est le plus souvent utilisée dans nos réseaux domestiques et dans les LAN**

* **Avantage** : Si un équipement est défectueux, cela n'affecte pas le reste du réseau ;
* **Inconvénient** :  Si le matériel situé au noeud tombe en panne, l'ensemble du réseau devient inutilisable.

> Matériel utilisé :

L'équipement situé au noeud peut être :

* un **hub** (ne se rencontre quasi plus).  
  Chaque élément reçu d'un équipement est retransmis à **tous les autres équipements**.  
    
  **Inconvénients** :
    * Les équipements reçoivent de nombreuses informations qui ne les concernent pas ;
	* En cas d'un nombre d'équipement important, cela surchage fortement le réseau.

* un **switch**  
  
  Contrairement au hub, le switch analyse chaque information reçue par un équipement et la transmet **uniquement** à l'équipement concerné.  
  
  **Avantages** :
    * Il évite d'engorger le réseau ;
	* il peut adapter les débits aux possibilités des liaisons vers les équipements.

	
	
![topologies_reseaux](./img/topologies_reseaux.png]




### Sources

* [info Blaise Pascal](https://info.blaisepascal.fr/)
* [christophe Mieszczak](https://framagit.org/tofmzk/informatique_git/-/tree/master/premiere_nsi)
* [A. Willm]()