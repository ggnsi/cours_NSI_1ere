# La couche Transport : TCP (ou UDP)

> Nous nous situons ici dans la **couche transport** et allons parler du protocole **TCP** (et également **UDP)

![modele_tcp_ip](./img/OSI_TCP_IP_partiel.png)


## Il faut découper ...

Chaque utilisateur peut envoyer des données sur les réseau.  
Si ces dernières, qui peuvent représenter un volume important, étaient envoyées en une seule fois, cela pourrait provoquer :
* une occupation exclusive de certaines liaisons, empêchant les autres transmissions sur cette liaison ;
* des surcharges de serveurs/routeurs pleinement occupés avec ce flux et donc la perte de données.

![surcharge](./img/surcharge.gif)

**On va donc découper les données en segments de longueur plus petites**.

![decoupage](./img/decoupage.gif)

## mais pas seulement ...

A cause du routage, les paquets de données peuvent :
* arriver dans un ordre différent de celui d'envoi ;
* voir ne pas arriver du tout (et donc être perdues) ;
* être altérés.

>Ainsi, **TCP** va **lors de l'envoi du côté émetteur** :
>
>* découper les données envoyées en paquets ;
>* numéroter chaque paquet à l'envoi ;
>* ajouter des checksum pour vérifier que les données n'ont pas subies de modification pendant le transport ;
>
>**du côté réception** :
>
>* signaler à l'émetteur qu'il a bien reçu un paquet (en envoyant un accusé de réception comportant le numéro du paquet) ;
>* ré-ordonner les paquets reçus ;

Si l'émetteur constate qu'au bout d'un certain temps il n'a pas reçu l'accusé de réception pour un certain paquet (et que donc le récepteur ne l'a pas reçu), il le renvoie. (Remarque : il se peut bien sûr aussi que l'accusé de réception se soit perdu. Alors TCP supprimera le paquet reçu en double à la réception)

![illustration tcp](./img/tcp_ip_5_ter.jpg)

## TCP/UDP

**TCP** est un protocole fiable et bneaucoup d’applications utilisent le protocole TCP car elles ne peuvent pas se passer de fiabilité :
* Le web
* La connexion à distance
* Le courrier électronique
* Le transfert de fichiers
* ...

Il existe aussi le **protocole UDP** qui ressemble beaucoup au protocole TCP.

> La grande différence entre UDP et TCP est que **le protocole UDP ne gère pas les accusés de réception**.

Les échanges de données avec UDP sont donc moins fiables qu'avec TCP (un paquet "perdu" est définitivement "perdu" et ne sera pas renvoyé) mais beaucoup plus rapides (puisqu' il n'y a pas d'accusé de réception à transmettre).

UDP est donc très souvent utilisé pour les échanges de données qui doivent être rapides, mais où la perte d'un paquet de données de temps en temps n'est pas un gros problème (par exemple le streaming vidéo).



