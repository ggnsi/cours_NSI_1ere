# Protocole de bit alterné

> Le protocole de bit alterné est un protocole simple de fiabilisation de transfert de données.

## Principe du protocole

Le principe de ce protocole est simple, considérons 2 ordinateurs en réseau : un __ordinateur A qui sera l'émetteur des trames et un ordinateur B qui sera le destinataire des trames__. 

Au moment d'émettre une trame, __A va ajouter à cette trame un bit__ (1 ou 0) __appelé drapeau (flag en anglais)__.  
dès que B a reçu une trame en provenance de A, il lui renvoie un accusé de réception (acknowledge en anglais souvent noté __ACK__) en changeant la valeur du drapeau (il renvoie 1 s'il a reçu 0 et inversement).  
A la prochaine trame, B s'attend à recevoir le même drapeau que celui qu'il a renvoyé.

Concrètement, cela donne ça : 

- La première trame envoyée par A aura pour drapeau 0.
- Dès cette trame reçue par B, ce dernier va envoyer un accusé de réception avec le drapeau 1 (ce 1 signifie "la prochaine trame que A va m'envoyer devra avoir son drapeau à 1"). 
- Dès que A reçoit l'accusé de réception avec le drapeau à 1, il envoie la 2e trame avec un drapeau à 1, et ainsi de suite...

![Protocole de bit alterné](img/bitAlt_1.png)

Le __système de drapeau est complété avec un système d'horloge__ côté émetteur. Un "chronomètre" est déclenché à chaque envoi de trame, si au bout d'un certain temps, l'émetteur n'a pas reçu un acquittement correct (avec le bon drapeau), __la trame précédemment envoyée par l'émetteur est considérée comme perdue et est de nouveau envoyée__.

Examinons quelques cas :

- __La trame est perdue__ :

![Protocole de bit alterné : trame perdue](img/bitAlt_2.png)

Au bout d'un certain temps ("TIME OUT") A n'a pas reçu d'accusé de réception, la trame est considérée comme perdue, elle est donc renvoyée.

- __L'accusé de réception est perdu__ :

![Protocole de bit alterné : AR perdue](img/bitAlt_3.png)

A ne reçoit pas d'accusé de réception avec le drapeau à 1, il renvoie donc la trame 1 avec le drapeau 0. B reçoit donc cette trame avec un drapeau à 0 alors qu'il attend une trame avec un drapeau à 1 (puisqu'il a envoyé un accusé de réception avec un drapeau 1), il "en déduit" que l'accusé de réception précédent n'est pas arrivé à destination : il ne tient pas compte de la trame reçue et renvoie l'accusé de réception avec le drapeau à 1. Ensuite, le processus peut se poursuivre normalement.

> __Remarque :__ dans certaines situations, le protocole de bit alterné ne permet pas de récupérer les trames perdues ou de détecter certaines erreurs. 
>
>C'est pour cela que ce TCP utilise des mécanismes plus complexes mais aussi plus chers en ressources.

## Une autre explication illustrée si besoin

### 1. Contexte
- Alice veut envoyer à Bob un message M, qu'elle a prédécoupé en sous-messages M0, M1, M2,...
- Alice envoie ses sous-messages à une cadence Δt fixée (en pratique, les sous-messages partent quand leur acquittement a été reçu ou qu'on a attendu celui-ci trop longtemps : on parle alors de _timeout_)

### 2. Situation idéale

![](data/ideale.png) 

Dans cette situation, les sous-messages arrivent tous à destination dans le bon ordre. La transmission est correcte.

### 3. Situation réelle
Mais parfois, les choses ne se passent pas toujours aussi bien. Car si on maîtrise parfaitement le timing de l'envoi des sous-messages d'Alice, on ne sait pas combien de temps vont mettre ces sous-messages pour arriver, ni même (attention je vais passer dans un tunnel) s'ils ne vont pas être détruits en route.

![](data/realite.png) 

Le sous-message M0 est arrivé après le M1, le message M2 n'est jamais arrivé...

Que faire ?

Écartons l'idée de numéroter les sous-messages, afin que Bob puisse remettre dans l'ordre les messages arrivés, ou même redemander spécifiquement des sous-messages perdus. C'est ce que réalise le protocole TCP (couche 4 — transport), c'est très efficace, mais cher en ressources. Essayons de trouver une solution plus basique.

### 3. Solution naïve...

Pourquoi ne pas demander à Bob d'envoyer un signal pour dire à Alice qu'il vient bien de recevoir son sous-message ?
Nous appelerons ce signal ACK (comme _acknowledgement_, traduisible par «accusé de réception»).
Ce signal ACK permettra à Alice de renvoyer un message qu'elle considérera comme perdu :

![](data/naive.png) 

N'ayant pas reçu le ACK consécutif à son message M1, Alice suppose (avec raison) que ce message n'est pas parvenu jusqu'à Bob, et donc renvoie le message M1.

### 4. Mais peu efficace...

![](data/naivebad.png) 

Le deuxième ACK de Bob a mis trop de temps pour arriver (ou s'est perdu en route) et donc Alice a supposé que son sous-message M1 n'était pas arrivé. Elle l'a donc renvoyé, et Bob se retrouve avec deux fois le sous-message M1. La transmission est incorrecte. 
En faisant transiter un message entre Bob et Alice, nous multiplions par 2 la probabilité que des problèmes techniques de transmission interviennent. Et pour l'instant rien ne nous permet de les détecter.

### 5. Bob prend le contrôle

Bob va maintenant intégrer une méthode de validation du sous-message reçu. Il pourra décider de le garder ou de l'écarter. Le but est d'éviter les doublons.

Pour réaliser ceci, Alice va rajouter à chacun de ses sous-messages un bit de contrôle, que nous appelerons FLAG (drapeau). Au départ, ce FLAG vaut 0. 
Quand Bob reçoit un FLAG, il renvoie un ACK **égal au FLAG reçu**.

Alice va attendre ce ACK contenant le même bit que son dernier FLAG envoyé :
- tant qu'elle ne l'aura pas reçu, elle continuera à envoyer **le même sous-message, avec le même FLAG**.
- dès qu'elle l'a reçu, elle peut envoyer un nouveau sous-message en **inversant** («alternant») **le bit de son dernier FLAG** (d'où le nom de ce protocole).


Bob, de son côté, va contrôler la validité de ce qu'il reçoit : il ne gardera que **les sous-messages dont le FLAG est égal à l'inverse de son dernier ACK**. C'est cette méthode qui lui permettra d'écarter les doublons.

Observons ce protocole dans plusieurs cas :

##### 5.1 Cas où le sous-message est perdu

![](./img/alt2.png) 



##### 5.2 Cas où le ACK  est perdu
![](./img/alt1.png) 

Le protocole a bien détecté le doublon du sous-message M1.

##### 5.3 Cas où un sous-message est en retard

![](./img/alt3.png) 

Le protocole a bien détecté le doublon du sous-message M1... mais que se passerait-il si notre premier sous-message M1 était _encore plus_ en retard ?

Qu'en conclure sur ce protocole ?

---
Sources :
* [Gilles Lassus](https://github.com/glassus/nsi)
* [Pixees](https://pixees.fr)
