# Adresse IP

## Définition

>  Une adresse IP est un numéro d'identification qui est attribué de façon permanente ou provisoire à chaque appareil connecté à un réseau informatique utilisant l'Internet Protocol.

>Cette adresse est le numéro unique d'un ordinateur ou d'une machine sur un réseau qui lui permet de communiquer sur ce réseau.
>Ce numéro (en IP v4) est composé de 4 octets séparés par un point (notation décimale pointée) comme le montre la figure ci-dessous :



## IP v4 et IP v6

Actuellement, deux versions coexistent : `IPv4`  et `IPv6`.

### IP v4

Les  adresses IP, en version 4, prennent la forme d'une série de 4 nombres séparés par des  points :

* Ces 4 nombres sont des  entiers codés sur un octet (donc allant de 0 à 255), par exemple 192.168.0.1

* Une adresse `IPv4` se code donc sur 4 octets ou 32 bits.

![adresse ip v4](./img/addresse_ipv4.png)    

Un rapide calcul: $`256^4 = 4 294 967 296`$, nous donne le nombre d'adresses `IPv4` possibles. 

Compte-tenu du développement d'internet, ce nombre est rapidement devenu trop petit. Pour palier à ce problème deux solutions cohabitent : l'adressage privée, qui permet d'utiliser plusieurs fois la même adresse dans différents réseaux privés, et l'adressage IPv6 qui est en train de se déployer.

### IP v6

Afin d'augmenter le nombre d'adresses possibles, une  nouvelle version, `IPv6`, est entrée en service en 2010 et cohabite avec la norme `IPv4` : Les adresses IP ne  sont plus codées sur 4 octets mais sur 16 !

 Le nombre  d'adresses disponibles théoriquement est donc colossal : $`256^{16} = 340282366920938463463374607431768211456 `$
 
Les machines qui ont besoin d'une adresse IP unique au monde utilisent, aujourd'hui et pour la plupart, une adresse IPv6. 

## Adresse IP publique, adresse IP privée

>Les machines qui sont sur un réseau local ont-elle toutes besoin d'être identifiée de manière unique sur le réseau internet mondial (et donc d'avoir une adresse _IP publique_) ?

>Non. Mais il est nécessaire de les identifier localement dans le réseau pour qu'elles puissent communiquer avec les autres machines du réseau local. Elle possèderont alors une adresse _IP privée_.

### IP Privée

Dans un réseau local (un LAN pour Local Area Network), les machines utilisent une adresse `IP privée`  :

* Cette adresse permet de repérer de manière unique une machine dans un réseau local.

* Elle n'est pas unique au monde : deux machines de deux LAN distincts peuvent utiliser la même adresse privée. 

* Pour ne pas mélanger les adresses `IP privée` et `IP publique`, des plages leurs sont allouées. Dans un réseau local, les adresses à utiliser commencent :

    * soit par `10.` suivi de 3 octets. Par exemple `10.1.23.128`.
    * soit par `172.16` suivi de 2 octets. Par exemple `172.16.28.1`.
    * soit par `192.168` suivi de deux octest. Par exemple `192.168.0.2`.


**Avantage** : Il n'est pas nécessaire d'assigner une adresse unique à toutes les machines donc il n'est pas nécessaire d'avoir un nombre d'adresses uniques gigantesque. 

**Inconvénient** : On ne peut donc pas trouver une route vers ou depuis une machine d'un autre réseau uniquement grâce à cette adresse : on dit qu'une IP privée seule n'est pas `routable`. 

### IP publique

Pour qu'une machine puisse être repérer de manière unique sur internet,  il faut utiliser une adresse unique au monde : une `IP publique` .

* Seules les machines disposant d'une IP Publique peuvent être accessible directement sur internet. On dit alors qu'elles sont `routables`. C'est le cas des routeurs et des machines qui hébergent des serveurs.

*  Les plus anciennes machines disposant d'une IP publiques ont des adresses IPv4 mais, cette norme ne proposant plus assez d'adresses au vue du nombre exponentiellement croissant d'objets connectés, les plus récentes possèdent une adresse en IPv6?

* Chez les particuliers, seule la box possède une IP publique. Les autres machines font parties d'un réseau local et ont des IP privées.  
  
  Lorsqu'une machine de votre LAN envoie ou reçoit des données sur internet, la box fait le travail d'un concierge d'immeuble : elle est capable de remplacer l'adresse IP privée d'une machine de son réseau par son adresse publique ou de faire l'inverse. On parle alors de `NAT` pour `Network Address Translation`.  Ce procédé permet, par l'intermédiaire de la box qui joue alors le rôle de `routeur NAT`, de rendre, en quelque sorte, l'IP privée `routable`.

### Adresse IP peut être soit **fixe** ou **dynamique** :

* Une adresse fixe, comme  son nom l'indique, ne change jamais. On est donc dans ce cas aisément  traçable. 

* Une l'adresse IP dynamique est attribuée par une entité du réseau dont c'est le rôle :
	* Pour les box internet, c'est le FAI (**F**ournisseur d'**A**ccès **I**nternet) qui l'attribue à chaque nouvelle  connexion de la box à internet. Cette adresse est  attribuée sur des  plages d'adresses IP réservées. Le traçage n'est donc pas immédiat mais  le FAI est capable de fournir les informations permettant  l'identification.
	* pour les machines de votre réseau local (derrière votre box), c'est la box qui vous attribue une adresse IP locale.






** Sources** :
* [Christophe Mieszczak Licence CC BY SA](https://framagit.org/tofmzk/informatique_git/-/blob/master/premiere_nsi)
