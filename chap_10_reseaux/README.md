# Web - les réseaux

## Ressource Lumni

[Vidéo à regarder sur Lumni](http://www.lumni.fr/video/l-architecture-des-reseaux-et-les-protocoles-de-communications)

## Réseau,

* [Bref historique](./10_reseau_bref_historique.md)

## Relier des équipements

* [vidéo à regarder](https://www.youtube.com/watch?v=dCknqcjcItU)

* [Réseau : définition, classification](./10_reseau_definition_classification.md)
* [Différentes topologies de réseaux](./10_reseau_topologie.md)
* [Réseaux : le matériel utilisé](./10_reseau_materiel.md)


* A faire :  
[Compléter le schéma du pdf avec une légende la plus complète possible](./reseau.pdf)


## Transfert de données et protocoles

* [Notion de protocole](./10_protocoles.md)
* [Le modèle en couches](./10_modele_en_couches.md)
* Etude des protocoles des différentes couches :
  * [Couche liaison des données : Transmission de données dans un réseau local](./10_reseau_couche_liaison_donnees.md) et complément [sur les adresse MAC](./10_adresse_mac.md)
  * [La  couche réseau : le protocole IP](./10_reseaux_couche_reseau_ip.md) et complément [sur les adresses IP](./10_adresse_ip.md)
  * [La couche transport : TCP (ou UDP)](./10_reseaux_couche_transport_tcp_udp.md)
* [Retour sur l'encapsulation/décapsulation](./10_reseaux_retour_sur_encapsulation.md)



<!-- * [TCP/IP le principe](./10_principe_tcp_ip.md)
* [Principe de base des protocoles TCP et IP](./10_principe_tcp_ip.md) -->
* [IP et le routage sur internet](./10_ip_routage_internet.md)


## TP

* [TD sur le chapitre](./TD/10_reseau_TD.md)

## Fiabilisation des données

* [le protocole du bit alterné](./10_reseaux_protocole_bit_alterne.md)
 
### ancien cours

* [une vision plus détaillée](./10_reseau_et_protocoles_de_communication.md)

### Sources

* [info Blaise Pascal](https://info.blaisepascal.fr/)
* [christophe Mieszczak](https://framagit.org/tofmzk/informatique_git/-/tree/master/premiere_nsi)
* Cours de C.Bonnefoy
* [A. Willm]()
* [Comment relier des équipements ?](http://nsi2021.ostralo.net/3_documents/cours_reseaux/pages/B_relier_des_equipements.htm)
* [Quelques commandes liées au réseaux](http://nsi2021.ostralo.net/1_premiere/chap15_reseaux/C15.2_TD_commandes.htm)
