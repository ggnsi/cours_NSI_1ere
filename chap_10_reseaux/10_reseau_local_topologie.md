# Réseau : topologie


## Topologie d'un réseau local

Il existe plusieurs façons de relier des équipements terminaux entre eux.  
L'arrangement physique des équipements communiquant entre eux , c’est-à-dire la configuration spatiale du réseau est appelé topologie physique.

### Réseau maillé

>Toutes les équipements terminaux sont connectés les uns aux autres par une liaison point à point.

![réseau maillé](reseau_maille.png)

* Avantages :
  * C’est la plus simple car elle relie deux équipements par une liaison unique ;
  * C’est la technologie la plus robuste car toute panne d'un matériel ou d'une liaison n'empêche pas la transmission des informations sur le reste du réseau.
 
* Inconvénients :

  * Cela implique un coût extrêmement élevé dans le cas de réseaux à grand nombre de stations.
  * Il n'est pas toujours physiquement possible de relier chaque équipement terminal à touts les autres ;
  * Tout machine ajoutée sur le réseau nécessite l'installation d'un nombre très important de nouveaux câbles, ce qui n'est pas toujours possible.
 
### Architecture en étoile

![topologie_étoile](topologie_etoile.png)

>Les équipements sont reliés entre eux par un équipement intermédiaire situé au noeud qui assure la transmission des informations entre les équipements.

**C'est cette topologie qui est le plus souvent utilisée dans nos réseaux domestiques et dans les LAN**

* **Avantage** : Si un équipement est défectueux, cela n'affecte pas le reste du réseau ;
* **Inconvénient** :  Si le matériel situé au noeud tombe en panne, l'ensemble du réseau devient inutilisable.

### exemples d'autres topologies

Il existe également (mais plus vraiment utilisées) :

* la topologie en **bus**

  ![topologie_bus](./img/topologie_bus.png)

* la topologie en **anneau**

  ![topologie_anneau](./img/topologie_anneau.png)

### topologie hybride

> Une topologie hybride regroupe plusieurs topologies différentes.

Le réseau internet est un exemple de topologie hybride.

![topologie_hybride](./img/topologie_hybride.png)
