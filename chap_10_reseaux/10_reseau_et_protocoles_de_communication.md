# Architecture réseau et Protocoles de communication
## Transmission de données dans un réseau

Le but d'un réseau est de __transmettre des informations d'un ordinateur à un autre__.

Pour cela il faut, dans un premier temps, __décider du type de codage de la donnée à envoyer__, c'est-à-dire sa __représentation informatique__. Celle-ci sera différente selon le type de données (sons, texte, graphique, table, vidéos,...).

La __représentation de ces données__ peut se diviser en deux catégories :

- Une __représentation numérique__ : c'est-à-dire le codage de l'information en un ensemble de valeurs binaires, soit une suite de 0 et de 1.
- Une __représentation analogique__ : c'est-à-dire que la donnée sera représentée par la variation d'une grandeur physique continue, comme une tension.

Les réseaux que nous étudierons ne traitent que de __signaux numériques__.

Les données circulent sur internet sous forme de __datagrammes__. Les datagrammes sont des __données encapsulées__, c'est-à-dire __des données auxquelles on a ajouté des en-têtes__. Ces en-têtes sont des informations nécessaires à leur transport (telles que l'adresse IP de destination).

## Protocoles

Un protocole est une __procédure comprise par les deux machines qui doivent communiquer__.

> __Exemple :__ Lorsque vous rencontrez une personne, nous suivons naturellement un ensemble de protocoles :
- On commence par initier la communication par un bonjour, une poignée de main.
- Ensuite on s'identifie.
- Puis on échange des informations dans un codage adapté (le français).
- On vérifie que l'on s'est compris (oui, ok, j'ai compris).
- Et enfin on se quitte.

En informatique ils existent bon nombre de protocoles : HTTP, IP, TCP, UDP,...

> __Exemple :__ Pour récupérer sur son ordinateur un mail stocké sur un serveur gérant sa mesagerie, on utilise un protocole nommée POP (Post Office Protocol) ou un protocole IMAP (Internet Message Access Protocol).

Un __service__ est une interface (un programme) qui permet de mettre en place un protocole.

# Le modèle TCP/IP, intégré au modèle OSI
## Les couches du modèle TCP/IP

Pour communiquer nous avons besoin de définir des modèles qui constituent des normes de communication. Le modèle OSI est le modèle théorique qui encadre les échanges sur un réseau. Le __modèle OSI comporte 7 couches__ que nous ne détaillerons pas cette année.

Le modèle OSI est un peu complexe et pas forcément ancré dans la réalité. Il a été simplifié pour donner le __modèle TCP/IP, à 5 couches__.

![Modèles OSI et TCP/IP](img/OSI_TCP_IP.png)

Dans ce modèle par couches, __les données passent d'une couche à l'autre__, sans pouvoir en sauter une. Chaque couche ne peut "communiquer" qu'avec une __couche adjacente__.

TCP/IP désigne communément une architecture réseau, mais cet acronyme désigne en fait deux protocoles étroitement liés : un __protocole de transport, TCP__ (Transmission Control Protocol) qu’on utilise « par-dessus » un __protocole internet, IP__ (Internet Protocol).

Le __modèle TCP/IP est une architecture réseau en 5 couches__ :

- la couche __Application__
- la couche __Transport__
- la couche __Réseau__
- la couche __Liaison de données__
- la couche __Physique__

## La couche Application du modèle TCP/IP

La couche Application est la couche la plus haute du modèle TCP/IP. La couche application définit les protocoles d’application TCP/IP et la __manière dont les programmes hôtes se connectent aux services__ de la couche de transport pour utiliser le réseau. 

La couche d’application inclut tous les protocoles de niveau supérieur tels que :

- DNS (système de nommage de domaine),
- __HTTP (protocole de transfert hypertexte)__,
- SSH (protocole pour sécuriser la communication grâce à un cryptage),
- FTP (protocole de transfert de fichier),
- SNMP (protocole de gestion de réseau simple) ,
- SMTP (protocole de transfert de courrier simple),
- DHCP (protocole de configuration d’hôte dynamique),
- RDP (protocole de bureau à distance),
- IMAP (protocole pour accéder à ses courriers électroniques),

En première, nous ne nous intéressons qu'au protocole HTTP (revoir le cours sur les interactions Client / Serveur au besoin).

Cette couche fait appel à un ensemble de __services__ pour mettre en place des protocoles (Ex : le __navigateur web gère le protocole HTTP__).

La __couche application se situe au niveau des programmes applicatifs__ des ordinateurs (navigateur web, client de messagerie,...). Ce sont ces __applications qui ont besoin d'envoyer et de recevoir des données__.

![Couche application](img/Couche_application.png)

## La couche Transport du modèle TCP/IP

### Le protocole TCP au coeur de la couche Transport

La couche de Transport a __pour but de permettre aux machines (ex : un client et un serveur) d’engager une conversation__.

Les principaux protocoles inclus dans la couche Transport sont __TCP__ (protocole de contrôle de transmission) et __UDP__ (protocole de datagramme utilisateur).

En première, nous ne nous intéressons qu'au __protocole TCP__.

Le __protocole TCP__ définit et __organise l'envoi des données__ :

- TCP permet d'__assurer l'initialisation et la fin de la connexion__ entre deux machines par un système d'accusés de réception.
- TCP permet un __découpage des données puis un ajout d'en-tête pour obtenir des segments__, afin de les "remettre" au protocole IP.
- TCP permet de __remettre en ordre les datagrammes (paquets)__ en provenance du protocole IP,
- TCP permet de __vérifier__ le flot de données afin d'éviter une saturation du réseau.
- TCP permet de multiplexer les données, c'est-à-dire de faire circuler simultanément des informations provenant de sources distinctes sur une même ligne.

> __Remarque :__ Le protocole UDP encapsule moins d'informations sur le destinataire avec les données si bien qu'aucun accusé de réception n'assure la bonne réception de l'envoi. La fiabilité n'est plus garantie par le protocole UDP mais la mise en oeuvre est plus rapide (pas d'aller-retours). C'est par exemple ce __protocole UDP qu'on utilise lors d'un streaming__, où la vitesse est plus importante que la fiabilité.

### Le protocole TCP pour initialiser et mettre fin à la connexion

#### Etablir une connexion

L'établissement de la connexion entre deux applications se fait souvent selon le schéma suivant :

- Les __ports TCP__ doivent être ouverts
- L'application sur le __serveur est passive, c'est-à-dire que l'application est à l'écoute__, en attente d'une connexion
- L'application sur le client fait une requête de connexion sur le serveur dont l'application est en ouverture passive. 

Les deux machines doivent donc __synchroniser leurs séquences__ grâce à un mécanisme communément appelé __three ways handshake__ (poignée de main en trois temps). Ce dialogue permet d'initier la communication, il se déroule en trois temps, comme sa dénomination l'indique :

1. Dans un premier temps la machine émettrice (le client) transmet un segment avec le __drapeau SYN__, pour signaler qu'il s'agit d'un segment de __synchronisation__.
2. Dans un second temps la machine réceptrice (le serveur) reçoit le segment initial provenant du client, puis lui envoie un accusé de réception, c'est-à-dire un segment avec le __drapeau SYN-ACK__ (synchronize, acknowledge).
3. Enfin, le client transmet au serveur un accusé de réception, c'est-à-dire un segment avec le __drapeau ACK__.

Cela se résume schématiquement ainsi :

![Connexion TCP Three-way handshake](img/TCP_3Way.png)

Suite à cette séquence comportant trois échanges les deux machines sont synchronisées et la communication peut commencer !

#### Terminaison de communication

La phase de terminaison d'une connexion utilise un handshaking en quatre temps, chaque extrémité de la connexion effectuant sa terminaison de manière indépendante. Ainsi, la fin d'une connexion nécessite une paire de segments FIN et ACK pour chaque extrémité.

![Terminaison TCP](img/TCP_Terminaison.png)

Suite à cette séquence, la connexion TCP est close.

### La couche Transport TCP encapsule les données dans un segment

Dans cette couche Transport que les __données que l'on veut transmettre sont découpées /organisées en segments__ avant d'être envoyées.

Un __segment est un ensemble de données généré par la couche de Transport TCP__.

Cette __couche Transport rajoute des informations aux données à transmettre (dans l'en-tête)__ pour permettre, entre autre, la reconstitution des données complètes par le receveur une fois tous les segments reçus. 

Ces __données rajoutées en-tête s'ajoutent aux données de la couche Application pour former un segment__. On parle d'__encapsulation__.

![Encapsulation par la couche transport](img/Encapsulation_transport.png)

L'ensemble de ces données (en-tête + données découpées de la couche Application) forme le segment.

A l'aide des __en-têtes__, la couche TCP de la machine réceptrice se chargera :

- de rassembler les segments pour __reconstituer les données__ de la couche Transport. 
- d'__acheminer ces données vers la bonne application, à l'aide du port de destination__. L'en-tête comprend également le port de la source, au cas où une réponse est nécessaire.

Le protocole __TCP propose également un mécanisme d'accusé de réception afin de s'assurer qu'un paquet est bien arrivé à destination__. On parle plus généralement de __processus d'acquittement__. Ces processus d'acquittement permettent de détecter les pertes de paquets au sein d'un réseau, l'idée étant qu'en cas de perte, l'émetteur du paquet renvoie le paquet perdu au destinataire.

## La couche Réseau du modèle TCP/IP

### Le protocole IP au coeur de la couche Réseau

> __Remarque :__ l'ancien modèle TCP/IP nomme cette couche Internet plutôt que Réseau. J'ai choisi ce dernier modèle pour avoir une cohérence de terme avec le modèle OSI (revoir le schéma de début de chapitre au besoin). 

__La couche Réseau découpe les segments, y ajoute une entête pour créer des datagrammes__ (on peut aussi parler de paquet à la place de datagramme).

Cette couche __gère l'acheminement des segments issus de la couche Transport__ entre une source et un destinataire, en particulier permettant leur __routage à travers différents réseaux__.

Les principaux protocoles inclus dans la couche Réseau sont :

- __IP (Internet Protocol)__
- ICMP (Internet Control Message Protocol)
- ARP (Address Resolution Protocol)
- RARP (Reverse Address Resolution Protocol)
- IGMP (Internet Group Management Protocol)

En première, nous ne nous intéressons qu'au protocole IP.

### La couche réseau IP encapsule les segments TCP dans des datagrammes (paquets)

Un __datagramme, ou paquet,__ est le nom que l'on donne à la __capsule générée par la couche Réseau IP__.

Cette __couche Internet IP encapsule les segments__ générés par la couche transport TCP :
- elle __découpe les segments__ au besoin.
- elle __rajoute (dans une en-tête) des informations sur les adresses IP source et de destination__ utilisées pour le transfert entre les hôtes et entre les réseaux. On obtient __un datagramme__.
- elle __reconstitue les datagrammes__ s'ils ont été découpés en plusieurs paquets.

![Encapsulation par la couche Réseau](img/Encapsulation_Reseau.png)

> __Remarques :__ 
- La taille maximale par défaut d'un paquet transmis sur Ethernet est de 1500 octets.
- Sur ces 1500 octets, 40 sont réservés à l'en-tête de la couche Transport.
- Seulement 16 octets sont réservés à l'en-tête de la couche Internet.

## La couche Liaison de données du modèle TCP/IP

### Le protocole Ethernet au coeur de la couche Liaison de données

La couche Liaison de données définit en détail comment les __données sont adressées à travers le réseau local__.

Les protocoles inclus dans la couche d’accès au réseau sont :

- __Ethernet__
- Wifi
- Token Ring
- Frame Relay

En première, nous ne nous intéressons qu'au protocole Ethernet.

### La couche Liaison de données Ethernet encapscule les datagrammes IP dans des trames

Une __trame__ est le nom que l'on donne à la __capsule générée des données gérée par la couche Liaison de données__.

Cette __couche Réseau encapsule notamment les paquets__ générés par la couche Internet. La couche Réseau rajoute aux paquets issus de la courche Internet différentes informations dans une en-tête, dont les __adresses physiques MAC permettant de retrouver la machine destinataire sur le réseau local__. 

Une __adresse MAC est une adresse matérielle unique__ par carte réseau (Ethernet, Wifi,...), constituée de 6 octets, notés en hexadécimal, séparés par deux points (ex : 5E:FF:56:A2:AF:15)

![Encapsulation par la couche Liaison de données](img/Encapsulation_Liaison_de_donnees.png)

### Protocole de bit alterné

Le protocole de bit alterné est un protocole simple de récupération de perte de trames.

Le principe de ce protocole est simple, considérons 2 ordinateurs en réseau : un __ordinateur A qui sera l'émetteur des trames et un ordinateur B qui sera le destinataire des trames__. 

Au moment d'émettre une trame, __A va ajouter à cette trame un bit__ (1 ou 0) __appelé drapeau (flag en anglais)__. B va envoyer un accusé de réception (acknowledge en anglais souvent noté __ACK__) à destination de A dès qu'il a reçu une trame en provenance de A. À cet accusé de réception on associe aussi un bit drapeau (1 ou 0).

Concrètement, cela donne ça : 

- La première trame envoyée par A aura pour drapeau 0.
- Dès cette trame reçue par B, ce dernier va envoyer un accusé de réception avec le drapeau 1 (ce 1 signifie "la prochaine trame que A va m'envoyer devra avoir son drapeau à 1"). 
- Dès que A reçoit l'accusé de réception avec le drapeau à 1, il envoie la 2e trame avec un drapeau à 1, et ainsi de suite...

![Protocole de bit alterné](img/bitAlt_1.png)

Le __système de drapeau est complété avec un système d'horloge__ côté émetteur. Un "chronomètre" est déclenché à chaque envoi de trame, si au bout d'un certain temps, l'émetteur n'a pas reçu un acquittement correct (avec le bon drapeau), __la trame précédemment envoyée par l'émetteur est considérée comme perdue et est de nouveau envoyée__.

Examinons quelques cas :

- __La trame est perdue__ :

![Protocole de bit alterné : trame perdue](img/bitAlt_2.png)

Au bout d'un certain temps ("TIME OUT") A n'a pas reçu d'accusé de réception, la trame est considérée comme perdue, elle est donc renvoyée.

- __L'accusé de réception est perdu__ :

![Protocole de bit alterné : AR perdue](img/bitAlt_3.png)

A ne reçoit pas d'accusé de réception avec le drapeau à 1, il renvoie donc la trame 1 avec le drapeau 0. B reçoit donc cette trame avec un drapeau à 0 alors qu'il attend une trame avec un drapeau à 1 (puisqu'il a envoyé un accusé de réception avec un drapeau 1), il "en déduit" que l'accusé de réception précédent n'est pas arrivé à destination : il ne tient pas compte de la trame reçue et renvoie l'accusé de réception avec le drapeau à 1. Ensuite, le processus peut se poursuivre normalement.

> __Remarque :__ dans certaines situations, le protocole de bit alterné ne permet pas de récupérer les trames perdues, c'est pour cela que ce protocole est aujourd'hui remplacé par des protocoles plus efficaces, mais aussi plus complexes.

## La couche Physique du modèle TCP/IP

La couche Physique définit en détail comment les __données sont envoyées physiquement à travers le réseau local__, c'est à dire la manière dont les bits sont signalés électriquement ou optiquement par les périphériques matériels qui interfacent directement avec un support réseau, tel qu’un câble coaxial, une fibre optique ou un fil de cuivre à paire torsadée.

## Les équipements des réseaux

Il existe une grande variété de matériel réseau, à tous les niveaux des modèles OSI et TCP/IP (répéteurs, concentrateurs, ponts, commutateurs, routeurs, passerelles,...). Nous en retiendrons trois essentiels :

![Routeur](img/Routeur.jpg)

- Le __concentrateur, ou hub (Couche Physique, Niveau 1)__, est simplement capable de répéter les données reçues (sans les décoder), en les transmettant à toutes les machines connectées sur le LAN (réseau local), sans distinction. 
- Le __commutateur, ou switch (Couche Liaisons de données Ethernet, Niveau 2)__, est capable de déchiffrer les trames ethernet en lisant les adresses MAC de la source et du destinaire afin de lier des machines appartenant à un même réseau local. Ainsi, il n'envoie la trame qu'au bon correspondant. Il aura besoin de créer une table de correspondance entre les adresses MAC du réseau et ses interfaces physiques (connectique). 
- Le __routeur (Couche Réseau IP, Niveau 3)__, est capable de gérer des liaisons sur les trois premières couches du modèle TCP/IP. Il déchiffre les adresses MAC et IP et permet la liaisons de machines situées dans des réseaux différents. Il doit donc comporter plusieurs interfaces réseaux pour les lier entre eux. Si plusieurs routes sont disponibles, il va choisir la meilleure pour acheminer les paquets à l'aide d'une table de routage.

# Encapsulation / Décapsulation
## Principe de l'encapsulation

À l'envoi d'un paquet de données, on parle d'encapsulation (suivre les flèches) :

![Encapsulation](img/Encapsulation.png)

## Principe de la décapsulation

À la réception d'un paquet de données, on parle de décapsulation (suivre les flèches) :

![Decapsulation](img/Decapsulation.png)

Ces encapsulations et décapsulations sont utilisées pour transférer des données entre deux périphériques sur un réseau.

## Un exemple d'encapsulation / décapsulation

Considérons un ordinateur dont le navigateur veut accéder au contenu d'une page Web stocké dans un serveur distant. Pour cela, le __navigateur du client va envoyer une requête HTTP au serveur__. Une telle requête est un message. 

Pour simplifier l'exemple, on suppose que :

- le navigateur connaît l'IP du serveur : le protocole DNS (Domain Name System) n'est pas utilisé.
- l'acheminement de l'ordinateur client au serveur se fait en passant uniquement par deux routeurs : routeur 1 et routeur 2.

![Transmission_reseau_1](img/Transmission_reseau_1.png)

Les données de la requête vont suivre les modifications et cheminement suivant :

### 1. Au niveau de l'ordinateur client, il y a une encapsulation.

  - Le navigateur prépare la __requête HTTP__ : couche Application.
  - Cette requête est encapsulée dans un __segment__ (ou plusieurs) par le protocole TCP : couche Transport.
  - Ces segments sont encapsulés avec des adresses IP dans des __datagrammes (paquets)__ pour pouvoir circuler dans le réseau : couche Réseau.
  - Ces paquets sont encapsulés avec des adresses physiques MAC dans des __trames__ : couche Liaisons de données.

![Transmission_reseau_2](img/Transmission_reseau_2.png)

Une fois encapsulée dans une trame, le tout peut circuler et être orienté dans les réseaux.
La trame est transmise, par une succession de bits, au routeur 1.

### 2. Au niveau du routeur 1

  - Décapsulation pour lire une partie du contenu des en-têtes : lorsque la trame arrive au routeur 1, celui-ci a besoin de connaître l'expéditeur et le destinataire. Pour cela, il va décapsuler :

    - les trames pour récupérer les adresses physiques MAC,
    - les paquets pour récupérer les adresses logiques IP.

![Transmission_reseau_3](img/Transmission_reseau_3.png)

Avec ces adresses (et sa table de routage, à voir en classe de Terminale), le routeur va pouvoir acheminer les données à l'intermédiaire suivant : le routeur 2.

Cependant, pour pouvoir être renvoyées sur le réseau, les données doivent être encapsulées sous forme d'une trame. D'où :

  - encapsulation pour le renvoi sur le réseau : comme il y a eu deux niveaux de décapsulation, il faut encapsuler deux fois pour transformer les données en paquet puis trame.

![Transmission_reseau_4](img/Transmission_reseau_4.png)

Le routeur 1 envoie la trame vers le routeur 2.

### 3. Au niveau du routeur R2

Le même besoin des adresse MAC et IP conduit au même travail de décapsulation et d'encapsulation avant que ce routeur n'envoie la trame réencapsulée vers le serveur destinataire.

![Transmission_reseau_5](img/Transmission_reseau_5.png)

### 4. Au niveau du serveur

Le serveur doit récupérer la requête HTTP qu'il vient de recevoir encapsulée dans une trame.
Pour cela, il doit enlever les différentes en-têtes en décapsulant :

- d'abord la trame pour extraire le paquet IP.
- ensuite le paquet IP pour extraire le datagramme.
- enfin le segment pour extraire les données correspondant à la requête HTTP.

![Transmission_reseau_6](img/Transmission_reseau_6.png)

Une fois la requête HTTP reçue et extraite, le serveur peut traiter la demande.

Sa réponse contenant le code de la page Web suivra lui aussi une succession d'encapsulations et de décapsulations à travers son acheminement sur le réseau.

## Autre exemple d'encapsulation / décapsulation

Voic un autre exemple schématisé : l'__envoi et la réception d'un courriel__.

![Exemple TCP/IP lors d'un envoi de courriel. Source : Benoit Fourlegnie](img/TCP_IP_Exemple.png)


## Le modèle TCP/IP schématisé par ses protocoles

On présente en général le modèle TCP/IP de haut en bas comme dans l'illustration ci-dessous contenant différents noms de protocoles :

![Modèles OSI détaillé](img/osi_detail.png)

## Que retenir ?
### A minima...

- En informatique un protocole permet de faciliter la communication entre des ordinateurs.
- Internet est basé sur 2 protocoles : TCP et IP.
  - Le protocole TCP permet de s'assurer qu'un paquet est bien arrivé à destination grâce à un système d’accusé de réception.
  - Le protocole IP s'occupe uniquement de faire arriver à destination les paquets en utilisant l'adresse IP de l'ordinateur de destination.
- TCP/IP repose sur la notion de paquets de données : un fichier n’est pas envoyé en une seule fois, les données (bits) sont “découpées” en petits paquets. Ces paquets de données n’empruntant pas forcément tous le même “chemin” sur le réseau, l’ordinateur devra les remettre dans l’ordre avant de pouvoir reconstituer le fichier d’origine. Si un paquet se perd, le fichier ne pourra pas être reconstitué.
- Pour transférer des données entre 2 ordinateurs, le protocole TCP encapsule ces données. Les données issues de l’encapsulation de TCP sont elles-mêmes encapsulées par le protocole IP. On obtient alors un paquet IP qui pourra être transmis sur le réseau. 
- Une fois arrivées à destination, les données seront désencapsulées.
- Le protocole TCP gère les accusés de réception. Les accusés de réception permettent à l’ordinateur qui a envoyé les données, de s’assurer que ces données ont bien été réceptionnées par l’ordinateur destination. 

### Au mieux...

- Le modèle TCP/IP (4 ou 5 couches suivant les version) est un modèle simplifié du modèle OSI (7 couches).
- Les couches du modèle TCP/IP sont :
  - Application
  - Transport (Protocole TCP)
  - Réseau (Protocole IP)
  - Liaisons de données (Protocole Ethernet)
  - Physique
- Le protocole TCP gère la connexion et déconnexion d'une communication par un échange de SYN / ACK (connexion) ou FIN / ACK (déconnexion).
- Des protocoles sont mis en place pour limiter et palier aux pertes de données. Le protocole de bit alterné en est un.

---
[![Licence CC BY NC SA](https://licensebuttons.net/l/by-nc-sa/3.0/88x31.png "licence Creative Commons CC BY-NC-SA")](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

**Sources :**
* Auteur : David Landry, Lycée Clemenceau - Nantes
* <a  href=http://www.monlyceenumerique.fr/index_nsi.html#premiere>JC. Gérard, T. Lourdet, J. Monteillet, P. Thérèse, sur le site monlyceenumerique.fr</a>
* <a  href=http://bfourlegnie.com/nsi_2019/cours/chap%2011/TCP_IP.pdf>Benoît Fourlegnie</a>
* <a  href=https://pixees.fr/informatiquelycee/n_site/nsi_prem.html>Pixees, le site de David Roche</a>
