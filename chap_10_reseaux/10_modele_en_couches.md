# Le modèle en couches

## Introduction

 Au début des années 70, chaque constructeur commençait à développer sa propre solution réseau autour d'architecture et de protocoles privés (SNA d'IBM, DECnet de DEC, DSA de Bull, TCP/IP du DoD,...) **qui étaient incompatibles entre eux**.
 
Il s'est donc vite avéré qu'il serait impossible d'interconnecter ces différents réseaux «propriétaires» si une norme internationale n'était pas établie.

>Ainsi est née la norme **OSI (Open System Interconnection ou interconnexion de systèmes ouverts)**, établie par l'International Standard Organization (ISO).

> OSI définit le standard des communications entre des machines d'un réseau

## le modèle OSI

Le modèle OSI est le modèle théorique qui encadre les échanges sur un réseau. Le __modèle OSI comporte 7 couches__ que nous ne détaillerons pas cette année.  
Il décrit de manière précise les imbrications des différents éléments lorsque l'on veut concevoir un élément réseau.

Le modèle OSI est un peu complexe. C'est un modèle plus détaillé que celui simplifié, utilisé par l'**IETF** (Internet Engineering Task Force), aussi appelé __modèle TCP/IP, à 5 couches__.

> Nous utiliserons dans la suite le __modèle TCP/IP, à 5 couches (celui situé tout à droite sur l'image ci-dessous)__.

![Modèles OSI et TCP/IP](img/OSI_TCP_IP.png)

- la couche __Application__ : l'application (Navigateur, client mail, autre) génère **une donnée à transférer** ;
- la couche __Transport__ : Elle gère le découpage de la donnée en segments, leur numérotage, la reconstitution à l'arrivée, la vérification de la bonne transmission, le renvoi ou la demande de segments qui se seraient perdus ;  
  
  C'est ici qu'intervient le __protocole de transport TCP__ (Transmission Control Protocol) ou le __protocole de transport UDP__ (nous en reparlerons plus tard) ;  
  
- la couche __Réseau__ : Elle gère l'adressage des segments précédents et le routage, c'est à dire qu'elle permet que chaque machine ait des adresses pour être localisées et qu'on puisse ensuite déterminer quelle route doivent suivre les segments ;  
  
  C'est ici qu'intervient le __protocole internet, IP__ (Internet Protocol).  
  
- la couche __Liaison de données__ et celle __Physique__ : Elles gèrent le transfert des données entre équipements. 


> On désigne par **TCP/IP** ce modèle d'architecture réseau car cet acronyme désigne en fait deux protocoles étroitement liés et qui sont le plus souvent utilisés pour les transmissions sur internet : 
>
>D'autres protocoles pourraient être utilisés (voir image ci-dessous)

![osi model](./img/osi_model.png)


## Exemple pour bien comprendre et notion d'encapsulation 

### Pour bien comprendre le principe : analogie avec un envoi postal

* M. Dupont (qui joue le rôle de  **l'application**) a un document de 1000 pages à envoyer à M. Durand ;

**Chez M. Dupont (l'expéditeur)** :
  
* il donne son document **au service transport** de son entreprise.  
Comme le réseau postal n'accepte, pour des raisons d'encombrement que des colis contenant au maximum 100 pages, le service transport ( **protocole TCP**) va :
   * découper le document initial en blocs de 100 pages ;
   * numéroter ces blocs (pour que les blocs soient remis dans l'ordre ! En effet, rien ne dit que toutes les blocs arriveront en même temps au destinataire ni dans le bon ordre) ;
   * confier enfin chacune des bloc au service réseau.  
      
* le **service réseau** :
   * mets chaque bloc dans une enveloppe ;
   * écrit sur le devant de l'enveloppe l'adresse d u destinataire (M. Durand) et derrière l'enveloppe l'adresse de l'expéditeur (M. Dupont).  
  
  Ce sont ces adresses (**protocole IP**) qui vont permettre de savoir à quel endroit envoyer chaque enveloppe et permettre ensuite au transporteur de savoir par quelle route passer pour arriver chez M. Dupont.  
    
   * confie chaque enveloppe à un transporteur (couches Physique/Liaison des données).  
      
* le coursier (**les couches Physique/Liaison des données**) :
   * met l'enveloppe dans son camion (**protocole Ethernet**) qui va faire passer le colis camions en camions puis de centre de tri en centre de tri (**routeurs**) en utilisant l'adresse du destinataire (**adresse IP**) pour decider quel sera le centre de tri suivant.
   * va remettre l'enveloppe au destinataire.

> Nous venons de voir **2 choses importantes** :
>
> * la donnée initiale à transférer (le document de 1000 pages) **est passé de couche en couche en descendant le modèle** ;
> * chaque couche réceptionne un élément de la couche supérieure et le considère comme sa donnée à elle (sans se soucier de ce qu'elle contient) ;
> * chaque couche ne communique qu'avec les couches qui lui sont adjacentes ;
> * chaque couche a réceptionné un contenu de la couche supérieure et y **a ajouté des informations supplémentaires qui lui sont nécessaires**.

En effet :
* **la couche transport** a ajouté des numéros à chaque bloc, sans lire le document initial (et a fait du découpage) ;
* **la couche réseau** a mis chaque bloc dans une enveloppe et a ajouté des adresses (sans regarder ce que contenait chaque bloc) ;
* **la couche Physique/liaison des données** met des étiquettes pour indiquer à quel centre de tri il faut envoyer les enveloppes.

![illustration encapsulation](./img/encapsulation_gg_dessin_1.jpg)

###  processus d'**encapsulation**. 

> Chaque couche : 
> * récupère des données de la couche supérieure (sans se soucier de ce que c'est) ;
> * l' "enferme" dans une nouvelle bouteille en ajoutant **une entête** (des informations supplémentaires)
>
> On parle *d'encapsulation*.


![illustration encapsulation](./img/encapsulation_dessin_gg_3_720.jpg)

![illustration encapsulation](./img/encapsulation_illustration_4.jpeg)


### Analogie postale : que se passe-t-il à le destinataire ? : **décapsulation**

Chez M. Durand :

* (**couches Physique/Liaison des données, protocole Ethernet**) le dernier centre de tri oriente chaque enveloppe vers le bon camion pour la livraison.  
Le livreur arrache les étiquettes collées un peu partout et donne le colis à l'accueil ;
* **couche réseau, protocole IP** : à l'accueil, la personne vérifie que l'adresse sur l'enveloppe est bien celle de l'entreprise. Elle l'ouvre et transmets ensuite le contenu au service transport ;
* **couche transport, protocole TCP (ou UDP))** : 
   * le service transport vérifie que le bloc n'est pas arrivé abimé (si oui, il envoie un courrier à l'expéditeur pour lui demander de lui renvoyer) ;
   * Il attend d'avoir reçus toutes les enveloppes et les ouvre ;
   * Il remet les blocs reçus dans l'ordre (grâce aux numéros marqués sur les enveloppes) ;
   * Il transmet l'ensemble à M. Durand (**la couche application**).



> On voit ici qu'à la réception, les informations remontent les couches du modèle OSI.
>
> Chaque couche supprime son entête et transmets le reste à la couche supérieure.
>
> On parle de **processus de décapsulation**

![illustration encapsulation](./img/decapsulation.jpg)

 
### Schéma de synthèse

![shéma de synthèse](./img/2_schema_synthese.png)

<!-- ## Complément : parallèle du modèle OSI avec un envoi postal

![parallèle avec un envoi postal](./img/osi_parallele_lettre.png) -->

