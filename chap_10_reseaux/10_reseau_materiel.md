# Réseau : le matériel utilisé

Repartons de cette illustration :

![topologie_hybride](./img/topologie_hybride.png)

##  Les liaisons entre équipements

>Elles sont réalisées :
>
>* soit sans fil : wifi, bluetooth, réseaux 3G, 4G, ... ;
>* soit de manière **filaire** à l'aide :
>   * d'un câble *dit* **ethernet** ou **RJ45** ;
>   * de fibre optique

_Remarque_ :

* Ethernet est en fait le protocole de transmission utilisé pour échanger les données ;
* RJ45 est le nom de la prise terminale.
* Ces câbles sont constitués de 4 paires de fils torsadées (pour éviter les interférences) et souvent blindés (entourés par une fine feuille de métal conducteur pour l'isoler) ;
* Ces câbles existent en différentes catégories permettant de supporter diverses vitesses de transmission sur des distances différentes.

Câble "ethernet" ou "RJ45"

![cable_ethernet](./img/cable_ethernet.jpg)

![cable_ethernet](./img/cable_ethernet_2.jpg)

## Dans chaque équipement terminal : une carte réseau

> Elle permet de réaliser l'interface entre les données numériques qui arrivent et la transmission physique (gestion des tensions sur les brins d'un câble ethernet, gestion des impulsions lumineuses sur une fibre optique, gestion des ondes sur une liaison sans fil)

Elle se présente sous forme de carte d'extension ou est directement intégrée (sur la carte mère de l'équipement)

_Exemple_ : Une carte réseau "ethernet"

![carte_reseau](./img/carte_reseau_1.jpg)

_Exemple_ : Une carte réseau fibre optique

![carte_reseau](./img/carte_reseau_fibre.jpg)

## Les équipements intermédiaires

**Switch à 8 ports** :

![switch](./img/switch.jpg)

**Switch rackable** (pouvant être installé dans une armoire de brassage)

![switch](./img/switch_rack.jpg)

On rencontre :

* un **hub** (ne se rencontre quasi plus).  
  Chaque élément reçu d'un équipement est retransmis à **tous les autres équipements**.  
    
  **Inconvénients** :
    * Les équipements reçoivent de nombreuses informations qui ne les concernent pas ;
	* En cas d'un nombre d'équipement important, cela surchage fortement le réseau.

* un **switch**  
  
  Contrairement au hub, le switch analyse chaque information reçue par un équipement et la transmet **uniquement** à l'équipement concerné.  
  
  **Avantages** :
    * Il évite d'engorger le réseau ;
	* il peut adapter les débits aux possibilités des liaisons vers les équipements.

* un **routeur**
  Cet équipement permet la création de sous-réseaux et l'interconnexion des sous-réseaux.  
  Il participe également au routage des données, c'est à dire à l'acheminement des données de proche en proche dans le réseau.

_Remarques_ : 
* Chacune des "box" d'accès à internet est un routeur ;
* un routeur peut physiquement se présenter sous la même forme qu'un switch ou sous forme de machine plus imposante.
  

### Complément : les câbles sous-marins

Plus de 500 câbles sous-marins, parfois longs de plusieurs milliers ou dizaines de milliers de kilomètres sont déjà posés (le câble SEA-ME-WE 3 a une longueur de 39000 km !).
Plus de 95 % des communications mondiales transiteraient par ces câbles et ils assureraient plus de 99% des flux intercontinentaux.

Si la plupart des câbles étaient au départ propriétés des sociétés de communications nationales, les GAFAM sont maintenant les acteurs les plus présents dans l'installation de câbles afin  de maitriser le transit des données vers leurs serveurs.

Ces câbles composés maintenant de fibres optiques et sont équipés de répéteurs de signal environ tous les 100km.

Leur durée de vie théorique est d'environ 25 ans mais ils peuvent être renouvelés avant s'ils sont considérés comme technologiquement obsolètes.  
Ils intègrent à l'heure actuelle entre 12 et 16 paires de fibres optiques.

Allez voir [la carte des cables sous-marins](https://www.submarinecablemap.com/)

Vous pourrez voir que certains pays ne sont connectés que par un ou deux câbles.