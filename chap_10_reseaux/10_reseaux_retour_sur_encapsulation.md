# Retour sur l'encapsulation/décapsulation

## Schéma de synthèse

Chaque ensemble d'une couche devient les données pour la couche suivante

![synthese encapsulation](./img/encapsulation_600.png)

- La couche Transport ajoute aux données une en-tête TCP. On y retrouve les ports source et de destination, ces ports indiquent quelle application utiliser avec quel protocole (TCP ou UDP). Cela forme un *Segment TCP*.

- A ce segment, la couche Internet y ajoute l'en-tête IP. On y retrouve les adresses IP des machines source et de destination. Cela forme un *Paquet IP*.

- Enfin, la couche Accès réseau ajoute au paquet IP l'en-tête Ethernet. On y retrouve les adresses MAC des machines source et de destination. Cela forme une *Trame Ethernet*.

![encapsulation vue générale](./img/Encapsulation_Liaison_de_donnees_700.png)

------

## Un exemple d'encapsulation / décapsulation

Considérons un ordinateur dont le navigateur veut accéder au contenu d'une page Web stocké dans un serveur distant. Pour cela, le __navigateur du client va envoyer une requête HTTP au serveur__. Une telle requête est un message. 

Pour simplifier l'exemple, on suppose que :

- le navigateur connaît l'IP du serveur : le protocole DNS (Domain Name System) n'est pas utilisé.
- l'acheminement de l'ordinateur client au serveur se fait en passant uniquement par deux routeurs : routeur 1 et routeur 2.

![Transmission_reseau_1](img/Transmission_reseau_1.png)

Les données de la requête vont suivre les modifications et cheminement suivant :

### 1. Au niveau de l'ordinateur client, il y a une encapsulation.

- Le navigateur prépare la __requête HTTP__ : couche Application.
- Cette requête est encapsulée dans un __segment__ (ou plusieurs) par le protocole TCP : couche Transport.
- Ces segments sont encapsulés avec des adresses IP dans des __datagrammes (paquets)__ pour pouvoir circuler dans le réseau : couche Réseau.
- Ces paquets sont encapsulés avec des adresses physiques MAC dans des __trames__ : couche Liaisons de données.

![Transmission_reseau_2](img/Transmission_reseau_2.png)

Une fois encapsulée dans une trame, le tout peut circuler et être orienté dans les réseaux.
La trame est transmise, par une succession de bits, au routeur 1.

### 2. Au niveau du routeur 1

  - Décapsulation pour lire une partie du contenu des en-têtes : lorsque la trame arrive au routeur 1, celui-ci a besoin de connaître l'expéditeur et le destinataire. Pour cela, il va décapsuler :

    - les trames pour récupérer les adresses physiques MAC,
    - les paquets pour récupérer les adresses logiques IP.

![Transmission_reseau_3](img/Transmission_reseau_3.png)

Avec ces adresses (et sa table de routage, à voir en classe de Terminale), le routeur va pouvoir acheminer les données à l'intermédiaire suivant : le routeur 2.

Cependant, pour pouvoir être renvoyées sur le réseau, les données doivent être encapsulées sous forme d'une trame. D'où :

  - encapsulation pour le renvoi sur le réseau : comme il y a eu deux niveaux de décapsulation, il faut encapsuler deux fois pour transformer les données en paquet puis trame.

![Transmission_reseau_4](img/Transmission_reseau_4.png)

Le routeur 1 envoie la trame vers le routeur 2.

### 3. Au niveau du routeur R2

Le même besoin des adresse MAC et IP conduit au même travail de décapsulation et d'encapsulation avant que ce routeur n'envoie la trame réencapsulée vers le serveur destinataire.

![Transmission_reseau_5](img/Transmission_reseau_5.png)

### 4. Au niveau du serveur

Le serveur doit récupérer la requête HTTP qu'il vient de recevoir encapsulée dans une trame.
Pour cela, il doit enlever les différentes en-têtes en décapsulant :

- d'abord la trame pour extraire le paquet IP.
- ensuite le paquet IP pour extraire le datagramme.
- enfin le segment pour extraire les données correspondant à la requête HTTP.

![Transmission_reseau_6](img/Transmission_reseau_6.png)

Une fois la requête HTTP reçue et extraite, le serveur peut traiter la demande.

Sa réponse contenant le code de la page Web suivra lui aussi une succession d'encapsulations et de décapsulations à travers son acheminement sur le réseau.

## Autre exemple d'encapsulation / décapsulation

Voic un autre exemple schématisé : l'__envoi et la réception d'un courriel__.

![Exemple TCP/IP lors d'un envoi de courriel. Source : Benoit Fourlegnie](img/TCP_IP_Exemple.png)
