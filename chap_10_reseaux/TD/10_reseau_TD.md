# Réseau : TD

### Exercice 1

Dans cet exercice, nous allons manipuler l'interface en ligne de commande. Pour cela, il faut ouvrir l'application Windows **Invite de commandes**.

Exécuter la commande ``ipconfig`` et répondre aux questions suivantes :

a) Donner l'adresse MAC de votre machine.

b) Donner l'adresse IP (IPv4) de votre machine.

c) Donner le masque de sous-réseau et vérifier qu'il s'agit d'un masque valide.

d) Donner l'adresse réseau du lycée.

e) Combien de machines ce réseau peut-il contenir ?

f) Quelle est sa plage d'adresse possible ? (Première adresse à dernière adresse IP)


### Exercice 2

[Filius](https://www.lernsoftware-filius.de/Herunterladen) est un logiciel pédagogique pour aider à la compréhension et à l’apprentissage des concepts des réseaux informatiques.  

![Image logiciel Filius](./img/filius1.png)  

On y trouve :  

* _une zone centrale blanche_ où l’on construira le réseau 
* _sur la gauche_  : les différents composants du réseau 
* _en haut_ : des boutons pour charger ou enregistrer un fichier, des boutons de commande pour la simulation (le bouton play) ou la construction d’un réseau (le marteau) et enfin un glisseur pour contrôler la vitesse des simulations.

__1) On commence par créer un réseau élémentaire avec deux ordinateurs de bureau reliés par un cable__

* Cliquez/ glissez pour déposer un premier ordinateur dans la zone centrale
* Faites un double click sur lui pour observer en bas ses caractéristiques réseau
* Affectez l'adresse IP __192.168.0.10__ au premier ordinateur (en changeant éventuellement la valeur dans `Adresse IP`) puis cochez la case `Utiliser l'adresse IP comme nom`
* Faites la même chose avec un autre ordinateur d'IP: __192.168.0.11__
* Reliez-les par l'intermédiaire d'un cable

__2)__ Appuyez sur le bouton "play". Vous passez en mode simulation.  

* Double-cliquer sur l'ordinateur `192.168.0.10 `
* Cliquer sur l'icône `Installation des logiciels`
* Cliquer sur `ligne de commandes` et la fleche verte gauche pour mettre l'instruction dans `installés`.
* Cliquer sur `Appliquer les modifications`.
* Exécuter alors l'application `ligne de commandes` en double cliquant dessus.

Une invite de commandes vous permet d'utiliser quelques commandes sur cet ordinateur du réseau.
A tout moment vous pouver utiliser la commande 
```bash
help
```
pour accéder aux commandes disponibles

__3)__ Testons la connexion avec l'autre ordinateur

```bash
ping 192.168.0.11
```
La commande ``ping`` permet d’envoyer un paquet IP "vide" vers une adresse IP. Si la machine de destinatation reçoit ce paquet, elle répond avec un autre paquet IP comme accusé de réception. Cette commande permet de vérifier si deux machines peuvent communiquer entre-elles.

* Combien de paquets ont été envoyés?  ............. 
 
On voit la caractéristique `ttl` qui veut dire __Time To Live__  : chaque routeur diminue de une unité le `ttl`. Si après avoir enlevé un le routeur constate que le `ttl`est à zéro, il détruit le paquet. Ici, le TTL initial est fixé pour tous à 64.  

* Combien de routeurs a rencontré la paquet n°1 ?
* Combien de routeurs un paquet peut-il traverser si son `ttl`est de 1 ?
* Combien de routeurs un paquet peut-il traverser si son `ttl`est de 2 ?
* Combien de routeurs un paquet peut-il traverser si son `ttl`est de 64 ?

### Exercice 3

A l'aide de Filius, construire la situation ci-dessous (attention à bien attribuer les adresses ip et le masque de sous-réseau)

![réseau 4 machines et switch](./img/img_4_pc_switch.png)

1. Passez en mode simulation ;
2. Cliquer sur le switch : vous allez faire apparaître sa table MAC. Que contient-elle initialement ? Pourquoi ?
3. Faire un clic droit sur l'ordinateur d'adresse IP `192.168.0.1` et sélectionnez `afficher les échanges de données`.  
   Vous pourrez ainsi observer tous les paquets envoyés ou reçus par cet ordinateur.
4. Faire un clic gauche sur l'ordinateur `192.168.0.1` et ouvrir la console.
5. Envoyer un `ping` vers l'ordinateur d'adresse  IP `192.168.0.2`.
   * cliquer sur la première requête `ARP` dans les données échangées.
      * A quoi sert cette requête ?
      * A quelle adresse `MAC` est-elle destinée ? A quoi correspond cette adresse `MAC` (faites une recherche !)
      * observez les requêtes ICMP suivantes en regardant à chaque fois les adresse `ÌP` et `MAC` sources et destination.
   * Comment a évolué la table `MAC`du switch ?
 

### Exercice 4

![réseau 4 machines et switch](./img/exo_4.png)

a) En mode construction, reproduire le réseau de la figure ci-dessus dans un fichier nommé `reseau_ex3.fls`.

Les machines M1 et M2 ont respectivement les adresses IP ``192.168.10.1`` et ``192.168.30.5``.

Le serveur possède l'adresse IP ``8.8.8.1``.

b) Donner l'adresse réseau du réseau A et B.

c) Il faut désormais relier ces deux réseaux, pour cela configurer le routeur :

- L'adresse IP du routeur du port relié au réseau A est ``192.168.0.1``.

- L'adresse IP du routeur du port relié au réseau B est ``8.8.8.254``.

Attention à également modifier le masque de sous-réseau si cela est nécessaire.

d) Depuis M1, lancer un ``ping`` vers le serveur. Que se passe t-il ? Les machines sont-elles connectées ?

L'erreur survient du fait qu'il manque une information à M1, il ne sait pas où envoyer le paquet. La passerelle est l'interface qui permet aux paquets de sortir du réseau.

e) Dans la configuration de M1, ajouter dans Passerelle l'adresse IP du routeur relié au réseau A.

f) Dans la configuration du serveur, ajouter dans Passerelle l'adresse IP du routeur relié au réseau B.

g) Refaire la commande ``ping`` vers le serveur depuis M1. Les machines sont-elles connectées ?

La commande `traceroute`permet de visualiser quels équipements le paquet traverse.

h) Depuis M1, effectuer la commande ``traceroute`` suivie de l'adresse IP du serveur. A quoi correspondent les adresses affichées ?

On décide de faire héberger sur le serveur une page Web.

i) Pour cela, installer sur le serveur le logiciel **Serveur web** et sur M1 le logiciel **Navigateur web**.

S'assurer que le serveur web est bien démarré, puis faire clic droit sur M1 et choisir **Afficher les échanges de données**.

Enfin, sur M1, ouvrir le navigateur et à la suite de ``https://`` : écrire l'adresse IP du serveur.

j) En visualisant les échanges effectués entre le navigateur et le serveur web, donner en précisant les couches, les protocoles utilisés.

### Exercice 5

Téléchargez le fichier [`reseau-de-reseaux.fls`](./filius_exercice_5.fls) est ouvrez-le depuis Filius.  

1. Passer en mode simulation. Analyser les échanges de données l'ordinateur 192.168.0.10 et la table MAC du switch 0.  
  
   * Par qui ces requêtes ont-elles été émises ? 

> Les routeurs envoient des requêtes broadcast (cad à tout le monde) afin d'interroger les autres routeurs auquels ils serait connecté afin qu'ils se partagent les adresses des sous-réseaux auxquels ils sont connectés et que se construisent progressivement leurs tables de routage (En terminale vous verrez deux protocoles utilisés pour cela).

> La commande `traceroute` permet de visualiser quels équipements le paquet traverse.

2. Sur l'ordinateur 192.168.0.10, lancez la  commande `traceroute 192.168.0.11`.  
Combien d'équipements le paquet a-t-il traversé ? Est-normal ?

3. Sur l'ordinateur 192.168.0.10, lancez la  commande `traceroute 192.168.2.12`.  
   * Combien d'équipements le paquet a-t-il traversé ? 
   * Identifiez ces équipements en utilisant leurs adresses `IP`.
