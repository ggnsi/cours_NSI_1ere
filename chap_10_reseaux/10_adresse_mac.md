# Adresse MAC

>Dans un réseau informatique, l'adresse **MAC** (**M**édia **A**ccess **C**ontrol) d'une interface réseau est l'identifiant physique d'une carte réseau d'un périphérique.
>Cette adresse est stockée par le constructeur dans la carte réseau. Elle est **unique** au monde.

Cette adresse est constituée de 6 octets (48 bits) (potentiellement $'2^{48} = 281475'$ milliards d'adresses possibles).

On le note le plus souvent en écrivant la valeur de chaque octet en hexadécimal (sur 2 digits) séparés par deux points.

_Exemple_ : '01:23:C5:B7:89:AB'

## Une adresse particulière

L'adresse 'FF:FF:FF:FF:FF:FF' ne désigne par un équipement particulier. Elle est utilisée lorsque l'on veut que les données soient envoyés à toutes les machines du réseau.  
On parle **d'adresse broadcast** (_diffusion_ en français).

## Trouver les adresses mac des cartes réseaux d'un ordinateur

Sous Windows :
* aller dans 'Démarrer' puis 'exécuter', taper 'cmd' puis valider ;
* entrer la commande 'ipconfig /all' ou 'getmac /v'

Sous Linux, entrer la commande 'ifconfig'

## Complément : structure d'une adresse

![structure adresse MAC](./img/mac_adresse_constitution.png)

Les 3 premiers octets sont nommés Organisationnaly Unique Identifier (OUI) et sont affectés à un constructeur. Chaque constructeur peut donc disposer, par OUI, de 224 (environ 16 Millions) d'adresses MAC.

Les adresses MAC sont attribuées par l'IEEE (Institute of Electrical and Electronics Engineers).

Chaque constructeur de cartes réseau se voit attribuer une plage d'adresses qu'il affecte aux différentes cartes réseau.  
On pourra utiliser [ce site](http://www.coffer.com/mac_find/) pour consulter les constructeurs, fabricant de cartes réseaux.

