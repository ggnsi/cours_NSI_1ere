# Représentation des entiers naturels

---
**Objectifs du chapitre**

* Comprendre ce qu'est l'écriture d'un entier naturel dans une base _b_ ;
* Savoir déterminer l'écriture décimale d'un entier naturel à partir de son écriture dans une base _b_ (en particuler en binaire et en hexadécimal) ;
* A partir de l'écriture d'un entier naturel en base 10, savoir déterminer son écriture en bianaire, en hexadécimal (à la main, avec les divisions successives);
* Savoir passer directement de l'écriture binaire à l'écriture hexadécimale (et inversement)
* Savoir le nombre de valeurs possibles pouvant être codées sur _n_ bits ;
* Connaitre les entiers naturels dont l'écriture binaire peut s'écrire sur _n_ bits ;

---

* [pdf du cours version élève](./2_1_binaire_hexadecimal_eleve.pdf)
* [TD du chapitre](./TD/2_1_TD_representation_entiers_naturels.md)
* [corrigé du TD](./TD/