# 2_1 Représentations des entiers naturels 

---
## Conversions

---

### Exercice n°1 : Du binaire au décimal 

1. Convertir en décimal les entiers naturels dont on donne l'écriture binaire :

  a. 1010 

  b. 11001 

  c. 10001101   

  d. En plus si le reste du TD est fini : 1111010111 

2. Pour vérifier vos résultats :

* Entrez l'écriture binaire de votre entier naturel dans la console `0b1010` et validez, Python vous retournera son écriture en base 10 ;

* Il est également possible d'utiliser la fonction `int(chaine,base)` ou `chaine` représente une chaîne de caractère contenant l'écriture du nombre dans la `base` indiquée.  La valeur de retour sera le nombre entier exprimé en base 10.  
Exemple : `int("1010",2)`

### Exercice n°2 : Du décimal au binaire

Convertir en binaire les nombres suivants (écrits en base 10) :  

1. 37  
2. 78   
3. 189   
4. En plus si le reste du TD est fini : 205  

**Pour vérifier vos résultats**, utilisez la fonction `bin()` dont vous chercherez la documentation.
**Attention** : sous quelles forme est renvoyé le résultat de cette fonction ?

### Exercice n°3 :  De l'hexadécimal au décimal

Convertir en décimal les entiers naturels dont on donne l'écriture hexadécimale : 

1. 23  
2. A0   
3. 1010   
4. DD   
5. ABCD (si reste terminé)

**Pour vérifier vos résultats**, pensez à soit utiliser l'écriture du nombre en hexadécimal dans la console (`0xA05` par exemple), soit à utiliser la fonction `int()` vue plus haut.

### Exercice n°4 :  Du décimal à l'hexadécimal
En adaptant la méthode des divisions successives rencontrées en cours, convertir en hexadécimal les nombres décimaux suivants :

1. 512  
2. 2002   
3. 16012   
4. 12728

**Pour vérifier vos résultats**, vous pouvez utiliser la fonction `hex()` (doc à regarder), soit re-convertir en forme décimale l'écriture hexadécimale que vous avez trouvé.

### Exercice n°5 :  Du binaire à l'hexadécimal

Convertir en hexadécimal les entiers naturels dont on vous donne ci-après l'écriture binaire :

1. 11110000  
2. 10101101   
3. 101111010001   
4. 110000111100
5. 1100110100101

**Comment vérifier vos résultats** avec les outils précédents ?

### Exercice n°6:  De l'hexadécimal au binaire

Convertir en écriture binaire les entiers naturels donc on donne l'écriture hexadécimale :

1. A8C
2. 37E9


### Exercice n°7 :  Nombre d'entiers codés sur n bits;

Préciser combien d'entiers naturels peuvent être représentés lorsqu'on les code sur :

1. `3` bits ;
2. `5` bits ;
3. `12` bits.

Dans chacun des cas, préciser quelle est la valeur du plus grand entier dont l'écriture binaire s'écrira sur le nombre de bits donnés.

### Exercice n°8 : Nombre d'entiers codés sur n bits ;

Sur combien de bits au minimum faut-il travailler pour pouvoir écrire la forme binaire des entiers naturels suivants :

1. 350
2. 512
3. 3500

### Exercice n°9 : Représentations des nombres entiers en binaire

1. Sans ordinateur, évaluez si les expressions suivantes renvoient `True` ou `False` (détaillez les calculs). Vous effectuerez une vérification à la fin.

    1. `0b1111 >= 15`
    2. `0b1101 + 0b10 == 15`
    3. `0b101 + 12 > 17`
    4. `0b11111 * 2 < 60`

2. Idem 

    1. `bin(5) == '0b101'`
    2. `bin(5) == 0b101`
    3. `bin(256) == '0b100000000'`
    4. `bin(1+1) != '0b11'`
    5. `bin(35) == '0b100001'`


### Exercice n°10 :  Codage des couleurs

La restitution des couleurs par un écran est basé sur le principe de la synthèse additive de trois Lumières colorées: Rouge, Vert et Bleu.
On parle de codage RVB (_ou RGB en anglais_ )
Pour un codage RGB 24 bits, chacune des composante est codée sur 8 bits.  
On a donc au maximum 256 nuances par composante.

Soit la couleur 1 représentée par le triplet (r,g,b) (composantes données en binaire) : (01111101, 10110010, 01001111)

1. Donner le codage de cette couleur avec ses composantes données en écriture décimale.
2. Donner le codage de cette couleur avec ses composantes données en écriture hexadécimale.
3. En [utilisant par exemple ce site (ou un autre)](https://www.peko-step.com/fr/tool/tfcolor.html), déterminer à quelle couleur cela correspond-il ?


### Exercice n°11 : multiplication par des puissances de 2

1. Soit `x = 10110011_2`.

    A. En revenant à la définition de l'écriture en base 2, écrire une égalité entre `x` et des puissances de 2.
    
    B. Déterminer alors l'écriture binaire des entiers suivants : `2x`, `4x`, `8x`


2. Soit `y = 10110100_2`.
    a. $y$ est-il multiple de 2 ? de 4 ? de 8 ?
    b. Si oui, déterminer l'écriture binaire des entiers suivants `x/2`, `x/4`, `x/8`.


---
  
## Pour aller plus loin

---

### Exercice n°1 : Base 8  


Les avions possédent des transpondeurs pour aider à les identifier sur le contrôle du trafic aérien radar

![Horloge binaire](./fig/cessna_transponder.jpg)[^2]

Ces transpondeurs transmettent quatre nombres à un chiffre compris entre 0 et 7 : ce système est dit octal (__base 8__)

__1)__ Le code  `7500` signifie détournement d'avion.  
Quelle est la forme décimale du code `7500` (écriture en base 8) ? (Rép : 3904)

__2)__ On peut le vérifier en Python en utilisant le préfixe `0o`

```python
>>> 0o7500
3904
```
Donnez les valeurs décimales des entiers naturels suivant écrits en base 8 (__détaillez vos calculs et n'utilisez l'interpréteur qu'à la fin pour vérifier__)

* 0031
* 7700


__3)__ Quel est le code le plus grand utilsable par les transpondeurs en base 8. Donnez sont écriture en base 10.

__4)__ Combien de codes différents sont possibles avec ce système ?

---

### Exercice n°2 :  Horloge binaire (dur)

![Horloge binaire](./fig/binary_clock.png)[^1]

L'horloge binaire ci-dessus affiche __10:37:49__  
Expliquez.

---
### Exercice n°3 : Codage et taille des entiers (curiosité) 

__1)__ Quels entiers naturels peut on représenter avec 16, 32, 64 bits ?  
__2)__  Dans la plupart des langages de programmation les entiers sont codés sur un nombre fixe de bits.  
__a-__ En Python, il n'existe pas de taille maximale pour les entiers, on peut le vérifier.  
Evaluer grâce au Shell un entier de valeur supérieur à la valeur précédente.  
__b-__ On peut par contre imposer une représentation qui limite cette taille à l'aide d'un module dédié.  

* Tapez le code suivant

```python
>>> import numpy
>>> x = numpy.uint8(12)
```
Le module numpy permet de travailler avec des entiers de taille fixée ici `numpy.uint8()` impose un codage de l'entier 12 sur 8 bits.

* vérifiez-le en testant le type.

```python
>>> type(x)
```
* Que se passe-il si on dépasse la valeur maximale autorisée pour un codage 8 bits ? Testez-le avec les nombres suivants

```python
>>> numpy.uint8(256)
```

```python
>>> numpy.uint8(258)
```


```python
>>> numpy.uint8(-5)
```

Concluez.


---
_Sources_

[^1]: [Visual explanation of a binary clock by Alexander Jones & Eric Pierce](https://commons.wikimedia.org/wiki/File:Binary_clock.svg?uselang=fr)

[^2]: [Cessna ARC RT-359A transponder and Bendix/King KY197 VHF communication radio](https://commons.wikimedia.org/wiki/File:CessnaARC-RT-359ATransponder04.jpg?uselang=fr)


```python

```
