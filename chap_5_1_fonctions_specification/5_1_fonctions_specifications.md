# Fonctions : spécifications

## Spécifications

La **spécification**  d'une fonction consiste à décrire , à lister de manière explicite les exigences  de ce que doit faire cette fonction. 
Nous allons voir comment spécifier et mettre au point son programme pour être le plus rigoureux possible.

### Pré-conditions et post_conditions

#### Exemple

_Un exemple caricatural_ :


```python
from math import sqrt # on importe la fonction racine du module math

def racine(x):
    y = sqrt(x) # remarque : la variable y n'est pas nécessaire
    return y
```


```python
racine(-3)
```


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-2-e160b420e7cc> in <module>
    ----> 1 racine(-3)
    

    <ipython-input-1-f2cc688c5136> in racine(x)
          2 
          3 def racine(x):
    ----> 4     y = sqrt(x) # remarque : la variable y n'est pas nécessaire
          5     return y
    

    ValueError: math domain error


Une `ValueError` est soulevée car la fonction `sqrt()` n'accepte pas -3 comme argument.


```python
racine('toto')
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-4-53ed150fdf8c> in <module>
    ----> 1 racine('toto')
    

    <ipython-input-1-f2cc688c5136> in racine(x)
          2 
          3 def racine(x):
    ----> 4     y = sqrt(x) # remarque : la variable y n'est pas nécessaire
          5     return y
    

    TypeError: must be real number, not str


Une `TypeError` est soulevée car la fonction `sqrt()` n'accepte pas d'argument de type `str`.

#### Définitions

>On appelle __préconditions__ sur un paramètre les conditions que doit vérifier ce paramètre afin de garantir le bon fonctionnement de la fonction.

> On appelle __postconditions__ sur le résultat retourné par la fonction les conditions que doit vérifier le résultat retourné par rapport au fonctionnemnet attendu de la fonction.


Ici :
* `x` doit être un entier ou un flottant positif ou nul.
* Le résultat retourné doit être un entier ou flottant positif. 

La _postcondition_ est évidemment remplie ici puisque la fonction est simple et le nombre de ligne de code très réduit. Mais pour une fonction complexe, il est intéressant de vérifier que le résultat retourné a bien les caractéristiques attendues.

> Afin qu'une fonction écrite puisse être correctement utilisée, il est donc important que les pré-conditions et post-conditions y soient clairement décrites.
> Pour cela, on prendra soin d'y inclure une **docstring** (chaîne de documentation)

### La docstring

> Le docstring est une chaine de caractère encadrée par un couple de 3 guillements doubles \"\"\" et placée juste après l'en-tête de la fonction.

On y indique  :

- ce que fait la fonction ; 
- le type des paramètres (pré-conditions) ;
- éventuellement des conditions d'utilisations (CU) (pré-conditions) ;
- le résultat retourné et son type (post-conditions).

_Exemple de structure_ :

Dans le docstring, on trouve :
* une courte phrase explicative sur la fonction (moins de 80 caractères si on désire respecter PEP 8)
* une ligne vide
* les spécifications sur les paramètres (type et préconditions : les conditions qu'on attend des arguments envoyés à la fonction).
* les spécifications sur le retour de la fonction (type et postconditions : le type de retour qu'on garantit si les préconditions sont respectées).
* les effets de bord éventuels (modification de listes, dictionnaires ou autres objets transmis) (nous reviendrons là-dessus plus tard)
* une ligne vide

_Illustration_ :


```python
def produit(a, b):
    """Renvoie le produit des nombres a et b
    
    param : a,b : int ou float
    return : a*b : int ou float"""
    
    return a * b
```


```python
def racine(x):
    """Calcule la racine carré d'un nombre et retourne le résultat
    
    paramètre : x (int or float)
    retourne : (float) : la racine carrée de x
    
    CU : x >= 0
    
    """
    
    y = sqrt(x) # remarque : la variable y n'est pas nécessaire
    return y
```

## Comment vérifier pré-conditions et post-conditions quand on met au point un projet ?

Le but ici est de déclencher des erreurs si les pré-conditions ou si les post-conditions ne sont pas respectées.
On souhaite prévoir un garde-fous pour vérifier que les conditions sont bien respectées.
On parle de _programmation défensive_.

Cela se nomme une __assertion__ et python comporte une fonction particulière `assert`

>_Syntaxe_ :
>
> `assert condition_booléenne, string_a_afficher_si_false`

On va placer ces instructions directement dans le corps de la fonction.

>Pour tester les pré-conditions, on fait les test juste après avoir récupérés les arguments.
Pour tester les post-conditions, juste avant de retourner le résultat.

_Exemple pour notre fonction `racine`_ :

_Remarques_ :
* j'ai enlevé la docstring pour alléger la lecture ici
* la fonction `isinstance(objet, type)` retourne `True` si `objet` est de type `type` et `False` sinon


```python
def racine(x):
    
    # Assertions impératives sur les préconditions
    assert isinstance(x, int) or isinstance(x, float), 'le paramètre doit être un entier ou un flottant'
    assert x >= 0, "le paramètre doit être positif"
    
    
    y = sqrt(x) # remarque : la variable y n'est pas nécessaire
    
    #assertions impératives sur les postconditions
    assert isinstance(y, float), "le résultat retourné n'est pas un flottant"
    
    return y
```

Appelons la fonction `racine()` avec une chaine de caractères comme argument.

Une `AssertionError` est levée et le texte correspondant affiché.


```python
racine('toto')
```


    ---------------------------------------------------------------------------

    AssertionError                            Traceback (most recent call last)

    <ipython-input-7-53ed150fdf8c> in <module>
    ----> 1 racine('toto')
    

    <ipython-input-6-94c910ceba86> in racine(x)
         10 
         11     # Assertions impératives sur les préconditions
    ---> 12     assert isinstance(x, int) or isinstance(x, float), 'le paramètre doit être un entier ou un flottant'
         13     assert x >= 0, "le paramètre doit être positif"
         14 
    

    AssertionError: le paramètre doit être un entier ou un flottant


Appelons la fonction `racine()` avec un nombre négatif comme argument.

Une `AssertionError` est levée et le texte correspondant affiché.


```python
racine(-3)
```


    ---------------------------------------------------------------------------

    AssertionError                            Traceback (most recent call last)

    <ipython-input-8-e160b420e7cc> in <module>
    ----> 1 racine(-3)
    

    <ipython-input-6-94c910ceba86> in racine(x)
         11     # Assertions impératives sur les préconditions
         12     assert isinstance(x, int) or isinstance(x, float), 'le paramètre doit être un entier ou un flottant'
    ---> 13     assert x >= 0, "le paramètre doit être positif"
         14 
         15 
    

    AssertionError: le paramètre doit être positif



```python
racine(5)
```




    2.23606797749979



__Remarque__ : les assertions sur les pré-conditions et post-conditions sont surtout utilisées lors de la mise au point d'un programme. Elle n'ont plus lieu d'être lorsque le programme est en production.

#### Pour aller plus loin : La recommandation de type (Type hint) [pep 484](https://www.python.org/dev/peps/pep-0484/) 

La recommandation de type consiste à indiquer le type attendu pour la variable et pour la valeur retournée dans la déclaration des paramètres.

Un exemple valant mieux qu'un long discours :


```python
def racine(x : float) -> float:
    """Calcule la racine carré d'un nombre et retourne le résultat
    
    paramètre : x (int or float)
    retourne : (float) : la racine carrée de x
    
    CU : x >= 0
    
    """
    
    y = sqrt(x) # remarque : la variable y n'est pas nécessaire
    return y
```

_Remarques_ :

* Le moteur d'exécution python n'applique pas les annotations de type pour les fonctions et les variables.
  Par contre, des outils tiers (contrôleur de type, environnements de développement (IDE), analyseurs de codes, le module `typing`) peuvent utiliser ces informations ;

* on peut spécifier le fait que x appartienne à un ensemble regroupant int et float  (voir doc du module `typing`) mais ce n'est pas l'objet de notre cours. 
