# TD : fonctions : spécifications, tests

### Exercice 1

Considérons l'algorithme ci-dessous :


```python
def algo1(a, b):
    
    q = 0
    while a >= b:
        q = q + 1
        a = a - b
    
    return (q, a)
```

1. Faire fonctionner (tableau de suivi des variables) cet algorithme avec `a = 37` et `b = 7`.
2. Même question avec `a = 25` et `b = 5`.
<!-- 3. Même question avec `a = 7` et `b = 9`. -->
4. Même question avec `a = -2` et `b = 5`
5. Que réalise l'algorithme `algo1` ?
6. A partir de vos observations et en considérant que $b$ doit aussi être strictement positif, déterminer les pré-conditions et post-conditions pour cette fonction.
7. Inclure une docstring.
7. Inclure des assertions dans le code permettant de tester les pré-conditions.
8. Inclure un jeu de test.
9. Lancer les tests via la console
10. Inclure dans le script les instructions permettant d'automatiser le lancement des tests si le script est appelé en tant que module principal.

### Exercice 2

Polo a écrit l'algorithme ci-dessous :


```python
def puissance(x, n):
    valeur = 1
    for i in range(n):
        valeur = valeur*x
    return valeur
```

1. Faire fonctionner l'algorithme pour comprendre ce qu'il réalise.
2. Polo inclut le test ci-dessous et en déduit que son algorithme est correct.  
Qu'en penser ?


```python
def puissance(x, n):
    """
    >>> puissance(1, 1)
    1
    >>> puissance(1, 5)
    1
    """
    valeur = 1
    for i in range(n):
        valeur = valeur*x
    return valeur
```

3. Déterminer les pré-conditions, les post-conditions et inclure une docstring complète.
4. Déterminer et inclure un jeu de test

### Exercice 3

Polo (qui travaille beaucoup) a conçu l'algorithme ci-dessous qui attribue une lettre en fonction de la note obtenue à un devoir :

* `F` strictement en dessous de 10 ;
* `E` si 10 <= note < 12 ;
* `D` si 12 <= note < 14
* `C` si 14 <= note < 16;
* `B` si 16 <= note < 18;
* `A` si 18 <= note < 20.



```python
def resultat(note):
    """Converti une noté en lettre
    
    paramètre note : (int or float) la note
    valeur retournée : (string) la lettre correspondante allant de A à F
    
    CU : 0<= note <= 20
    
    Exemples :
    
    >>> resultat(10)
    'E'
    >>> resultat(13)
    'D'
    >>> resultat(15)
    'C'
    >>> resultat(19)
    'A'
    """
    
    assert isinstance(note, int) or isinstance(note, float), "la note n'est pas un entier ou un flottant"
    assert (note >= 0) and (note <= 20), "la note n'est pas entre 0 et 20"
    
    if note >= 18:
        return 'A'
    elif note >= 17:
        return 'B'
    elif note >= 14:
        return 'C'
    elif note > 12:
        return 'D'
    elif note >=10:
        return 'E'
```

Polo obtient les résultats des tests ci-dessous.
Quelle conclusion en déduire ?


```python
Trying:
    resultat(10)
Expecting:
    'E'
ok
Trying:
    resultat(13)
Expecting:
    'D'
ok
Trying:
    resultat(15)
Expecting:
    'C'
ok
Trying:
    resultat(19)
Expecting:
    'A'
ok
1 items had no tests:
    __main__
1 items passed all tests:
   4 tests in __main__.resultat
4 tests in 2 items.
4 passed and 0 failed.
Test passed.
TestResults(failed=0, attempted=4)
```

### Exercice 4

Voici une fonction `cherche_lettre(nom,lettre)` qui affiche si le caractère `lettre` est présent ou non dans `nom`.

```Python
def cherche_lettre(nom,lettre):
	if lettre in nom:
		print(lettre," est dans le nom ",nom)
	else:
		print(lettre,"n' est pas dans le nom ",nom)
```

1. Quelles sont les spécifications de cette fonction ?
2. Ecrire une docstring incluant un jeu de test.
3. Inclure des assertions permettant de vérifier les pré-conditions.
4. Inclure dans le script les instructions permettant d'automatiser le lancement des tests si le script est appelé en tant que module principal.

### Exercice 5

On prétend que la fonction suivante teste l'appartenance du caractère `car` à la chaîne `chaine`.


```python
def appartient(car, chaine):
    i = 0
    while i < len(chaine)- 1 and chaine[i] != car:
        i = i + 1
    return i < len(chaine)        
```


```python
>>> appartient('j', 'Bonjour')
True
```

1. Réaliser la chaîne de documentation
2. Donner des tests pour cette fonction et montrer en particulier un test montrant qu'elle est incorrecte.

