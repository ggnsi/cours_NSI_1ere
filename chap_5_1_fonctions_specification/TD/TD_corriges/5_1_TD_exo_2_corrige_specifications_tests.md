# TD : fonctions : spécifications, tests

### Exercice 2 corrigé

### Exercice 2

Polo a écrit l'algorithme ci-dessous :


```python
def puissance(x, n):
    valeur = 1
    for i in range(n):
        valeur = valeur*x
    return valeur
```

1. Faire fonctionner l'algorithme pour comprendre ce qu'il réalise.

> Il déterminer la valeur de `x` à la puissance `n` **lorsque `n` est positif**

2. Polo inclut le test ci-dessous et en déduit que son algorithme est correct.  
Qu'en penser ?

> Le jeu de test n'est clairement pas suffisant car il ne prend en compte que la valeur  `x = 1`.  

3. Déterminer les pré-conditions, les post-conditions et inclure une docstring complète.

> pré-conditions :
> * `x` doit être entier ou flottant
> * `n` doit être un entier positif ou nul

```Python
def puissance(x, n):
    """
    Retourne la puissance n-ième de x : x^n
    
    paramètre x : (int ou float)
    paramètre n : (int) la puissance
    
    valeur retournée : (int or float) : x^n
    
    CU : n >= 0
    """
    
    valeur = 1
    for i in range(n):
        valeur = valeur * x
    return valeur
```

4. Déterminer et inclure un jeu de test

> Il faut inclure d'autre situations ainsi que les situations particulières comme `x = 0`, où `n = 0`. On peut aussi tester avec des valeurs de `x` négatives.

```Python
def puissance(x, n):
    """
    Retourne la puissance n-ième de x : x^n
    
    paramètre x : (int ou float)
    paramètre n : (int) la puissance
    
    valeur retournée : (int or float) : x^n
    
    CU : n >= 0
    
    Exemples :
    
    >>> puissance(1, 1)
    1
    >>> puissance(1, 5)
    1
    >>> puissance(0, 5)
    0
    >>> puissance(2, 0)
    1
    >>> puissance(2, 4)
    16
    >>> puissance(-2, 5)
    -32
    """
    
    valeur = 1
    for i in range(n):
        valeur = valeur*x
    return valeur
```
