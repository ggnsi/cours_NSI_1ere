# TD : fonctions : spécifications, tests

### Exercice 3 corrigé


Polo obtient les résultats des tests ci-dessous.
Quelle conclusion en déduire ?

> Que tous les tests entrés ont été passés avec succès.
> Pour autant **cela ne garantit aucunement le bon fonctionnement de la fonction ni que l'algorithme utilisé est correct**.

> Par exemple ici, l'algorithme ne prend pas en compte le fait d'avoir une note strictement inférieure à 10.
> On ne détecte pas l'erreur car le jeu de test n'est pas assez complet et n'inclut pas cette situation. Le jeu de test doit être exhaustif.
