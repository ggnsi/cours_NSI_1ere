#exercice 2 : spécifications de fonctions

def puissance(x, n):
    """
    Retourne la puissance n-ième de x : x^n
    
    paramètre x : (int ou float)
    paramètre n : (int) la puissance
    
    valeur retournée : (int or float) : x^n
    
    CU : n >= 0

    Exemples :
    
    >>> puissance(1, 3)
    1
    >>> puissance(2, 4)
    16
    >>> puissance(-3, 4)
    81
    >>> puissance(4, 0)
    1

    """
    
    valeur = 1
    for i in range(n):
        valeur = valeur * x
    return valeur



