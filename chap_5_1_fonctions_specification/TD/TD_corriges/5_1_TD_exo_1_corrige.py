def algo1(a, b):
    """Effectue la division euclidienne de a par b, retourne le quotient et le reste

    param a : (int) : le dividende
    param b : (int) : le diviseur
    
    valeur retournée : (int, int) le quotient et le reste
    
    CU : a >= 0 et b > 0
    
    Exemples :
    
    >>> algo1(37, 7)
    (5, 2)
    >>> algo1(25, 5)
    (5, 0)
    >>> algo1(7, 9)
    (0, 7)

    """
    
    
    assert isinstance(a, int), "a n'est pas entier"
    assert isinstance(b, int), "b n'est pas entier"
    assert a >= 0, "a n'est pas positif"
    assert b > 0, "b n'est pas strictement positif"
    
    q = 0
    while a >= b:
        q = q + 1
        a = a - b
    
    assert isinstance(a, int) and isinstance(b, int), "a ou b ,'est pas entier"
    assert (a >= 0), "a ou b n'est pas positif"
    assert (a < b), "le reste n'est pas st plus petit que le diviseur"
    
    return (q, a)

if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose = True)