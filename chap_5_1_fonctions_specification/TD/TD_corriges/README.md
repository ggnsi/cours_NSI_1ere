## Fonctions : spécifications et tests - corrigés
---

Dans le répertoire, en fonction des exercices, soit des fichiers `.md` à lire qui comprennent des explications, soit des fichiers `.py` comprenant du code (soit les deux pour un même exo)

* [corrigé exercice 1](./5_1_TD_exo_1_corrige_specifications_tests.md) et [fichier python](./5_1_TD_exo_1_corrige.py)
* [corrigé exercice 2](./5_1_TD_exo_2_corrige_specifications_tests.md) et [fichier python](./5_1_TD_exo_2_corrige.py)
* [corrigé exercice 3](./5_1_TD_exo_3_corrige_specifications_tests.md)

