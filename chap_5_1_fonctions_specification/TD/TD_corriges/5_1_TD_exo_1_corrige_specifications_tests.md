# TD : fonctions : spécifications, tests

### Exercice 1 corrigé

Considérons l'algorithme ci-dessous :


```python
def algo1(a, b):
    
    q = 0
    while a >= b:
        q = q + 1
        a = a - b
    
    return (q, a)
```

1. Faire fonctionner (tableau de suivi des variables) cet algorithme avec `a = 37` et `b = 7`.

> Cela retourne (5, 2)

2. Même question avec `a = 25` et `b = 5`.

> Cela retourne (5, 0)

4. Même question avec `a = -2` et `b = 5`

> Cela retourne (0, -2)

5. Que réalise l'algorithme `algo1` ?

> Il réalise la division euclidienne de a par b

6. A partir de vos observations et en considérant que b doit aussi être strictement positif, déterminer les pré-conditions et post-conditions pour cette fonction.

> Préconditions :
> * a et b entiers
> * a positif et b strictement positif

> post-conditions :
> quotient (q)  et reste (a) entiers positifs
> reste (a) strictement inférieur au quotient (q)

7. Inclure une docstring.

> CU correspond aux conditions d'utilisations de la fonction.

```Python
"""Effectue la division euclidienne de a par b, retourne le quotient et le reste

    param a : (int) : le dividende
    param b : (int) : le diviseur
    
    valeur retournée : (int, int) le quotient et le reste
    
    CU : a >= 0 et b > 0
```

7. Inclure des assertions dans le code permettant de tester les pré-conditions.

**Remarque : j'ai aussi mis des assertions pour tester les post-conditions**

```Python
def algo1(a, b):
    """Effectue la division euclidienne de a par b, retourne le quotient et le reste

    param a : (int) : le dividende
    param b : (int) : le diviseur
    
    valeur retournée : (int, int) le quotient et le reste
    
    CU : a >= 0 et b > 0
    """
    
    
    assert isinstance(a, int), "a n'est pas entier"
    assert isinstance(b, int), "b n'est pas entier"
    assert a >= 0, "a n'est pas positif"
    assert b > 0, "b n'est pas strictement positif"
    
    q = 0
    while a >= b:
        q = q + 1
        a = a - b
        
    assert isinstance(a, int) and isinstance(b, int), "a ou b ,'est pas entier"
    assert (a >= 0), "a ou b n'est pas positif"
    assert (a < b), "le reste n'est pas st plus petit que le diviseur"
    
    return (q, a)
```
8. Inclure un jeu de test.

```Python
def algo1(a, b):
    """Effectue la division euclidienne de a par b, retourne le quotient et le reste

    param a : (int) : le dividende
    param b : (int) : le diviseur
    
    valeur retournée : (int, int) le quotient et le reste
    
    CU : a >= 0 et b > 0
    
    Exemples :
    
    >>> algo1(37, 7)
    (5, 2)
    >>> algo1(25, 5)
    (5, 0)
    >>> algo1(7, 9)
    (0, 7)
    """
    
    
    assert isinstance(a, int), "a n'est pas entier"
    assert isinstance(b, int), "b n'est pas entier"
    assert a >= 0, "a n'est pas positif"
    assert b > 0, "b n'est pas strictement positif"
    
    q = 0
    while a >= b:
        q = q + 1
        a = a - b
    
    assert isinstance(a, int) and isinstance(b, int), "a ou b ,'est pas entier"
    assert (a >= 0), "a ou b n'est pas positif"
    assert (a < b), "le reste n'est pas st plus petit que le diviseur"
    
    return (q, a)

```

9. Lancer les tests via la console

```Python
>>> import doctest
>>> doctest.testmod()
```

10. Inclure dans le script les instructions permettant d'automatiser le lancement des tests si le script est appelé en tant que module principal.

> On ajoute dans le script les lignes suivantes :
> ```Python
if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose = True)
```

> Remarque : j'ai ajouté l'option `verbose = True` qui donne plus de détail. Pas forcément nécessaire en général (cela dépend de ce que l'on veut)
