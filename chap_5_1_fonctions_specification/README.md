## Fonctions : spécifications et tests
---

* [Cours fonctions spécifications](./5_1_fonctions_specifications.md)
* [Cours tester ses fonctions](./5_1_tester_ses_fonctions.md)
* [TD](./TD/chap_5_1_TD_fonctions_specifications_tests.md) et [des corrigés](./TD/TD_corriges)




