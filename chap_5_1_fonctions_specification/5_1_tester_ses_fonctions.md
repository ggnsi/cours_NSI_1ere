# Tester ses fonctions

### Pourquoi tester ?

Lorsque l'on écrit un code, on pense qu'il est correct, mais comment être sur que l'algorithme est bon, que l'on a pas oublié des cas particulier, bien initialisé cerains éléments ?

Il existe des manières de __démontrer__ qu'un code est correct, mais cela peut être très lourd et compliqué. Certains langages ont même été développés pour prouver la validité d'algorithmes.

Sur des projets importants (plusieurs dizaines de développeurs, centaine de milliers de lignes dans des milliers de fichiers, projet étalé sur plusieurs mois, ...), il est important d'avoir des procédures de test essayant de couvrir un maximum de situations possibles.  
Il est évidemment impossible de faire une infinité de test mais il existe des méthodes (et même des développeurs spécialisés).  
On cherchera aussi les cas limites ou qui peuvent être problématiques.
Ces tests peuvent aussi permettre de vérifier qu'une modification mineure effectuée pour améliorer une fonctionnalité n'en a pas dégradé une autre.

A notre niveau, nous essaierons de tester les fonctions individuellement.

[Quelques exemples de bugs célèbres](./5_1_quelques_bugs.md)

### Première idée : utilisation d'assertions

>Une première idée possible est d'utiliser des assertions pour tester le fonctionnement d'une fonction en incluant dans le script principal des appels de la fonction et des résultats attendus.

_Détaillons le fonctionnement avec la scipt ci-dessous_ :

* Python va lire la définition de la fonction `dec2bin` et la mettre en mémoire
* Puis :
    * il va évaluer `dec2bin(5)`, ce qui va lui renvoyer `'101'` ;
    * effectuer le test `'101' == '101'` qui donne `True` ;
    * passer à la ligne suivante puisque la valeur est `True`.
* etc.


```python
def dec2bin(x):
    """Convertir l'écriture décimale en éciture binaire
    
    paramètre x : (int) un entier
    valeur renvoyée : (str) l'écriture binaire de x
    
    CU : x > 0
    """
    
    result = ''
    while x >0:
        #on effectue la division euclidienne de x par 2
        q = x // 2 
        r = x % 2
        
        #on ajoute à gauche le reste obtenue dans le résultat
        result = str(r) + result
        x = q
    
    return result

assert dec2bin(5) == '101', 'erreur sur la conversion de 5'
assert dec2bin(1) == '1', 'erreur sur la conversion de 1'
assert dec2bin(107) == '1101011', 'erreur sur la conversion de 107'
```

>Cette pratique n'est pas des plus lisible et alourdit le script.

### Deuxième idée : le module `doctest`

> le module `doctest` permet 
> * d'inclure les tests dans la docstring de la fonction (et donc de les rendre plus visibles) ;
> * d'automatiser les tests.

#### Comment inclure les tests dans la docstring ?

* On écrit les appels souhaités de la fonction **après trois chevrons** `>>>` **en laissant un espace après eux**
* On écrit en dessous le résultat attendu.

_Exemple_ :


```python
def dec2bin(x):
    """Convertir l'écriture décimale en éciture binaire
    
    paramètre x : (int) un entier
    valeur renvoyée : (str) l'écriture binaire de x
    
    CU : x > 0
    
    Exemples :
    
    >>> dec2bin(5)
    '101'
    >>> dec2bin(1)
    '1'
    >>> dec2bin(107)
    '1101011'
    """
    
    result = ''
    while x >0:
        #on effectue la division euclidienne de x par 2
        q = x // 2 
        r = x % 2
        
        #on ajoute à gauche le reste obtenue dans le résultat
        result = str(r) + result
        x = q
    
    return result
```

#### Comment lancer les tests ?

On importe (via la console) le module `doctest` et on lance la fonction `testmod()` de ce module :


```python
import doctest
doctest.testmod()
```




    TestResults(failed=0, attempted=3)



>La fonction `testmod` est allée chercher dans les docstring de toutes les fonctions du module actuellement chargé tous les exemples (reconnaissables à la présence des triples chevrons >>>), et a regardé si le résultat obtenu était celui attendu.

Dans le cas présent, il n'y a qu'une fonction dont la documentation contient trois exemples (`attempted=3`) a été testée, et il n’y a eu aucun échec (`failed=0`).

Incluons volontairement une erreur dans le résutat attendu de la conversion de 107 dans la fonction et relançons les tests :


```python
def dec2bin(x):
    """Convertir l'écriture décimale en éciture binaire
    
    paramètre x : (int) un entier
    valeur renvoyée : (str) l'écriture binaire de x
    
    CU : x > 0
    
    Exemples :
    
    >>> dec2bin(5)
    '101'
    >>> dec2bin(1)
    '1'
    >>> dec2bin(107)
    '1101010'        # Erreur volontaire : on a changé le dernier 1 en 0
    """
    
    result = ''
    while x >0:
        #on effectue la division euclidienne de x par 2
        q = x // 2 
        r = x % 2
        
        #on ajoute à gauche le reste obtenue dans le résultat
        result = str(r) + result
        x = q
    
    return result
```


```python
import doctest
doctest.testmod()
```

    **********************************************************************
    File "__main__", line 15, in __main__.dec2bin
    Failed example:
        dec2bin(107)
    Expected:
        '1101010'        # Erreur volontaire : on a changé le dernier 1 en 0
    Got:
        '1101011'
    **********************************************************************
    1 items had failures:
       1 of   3 in __main__.dec2bin
    ***Test Failed*** 1 failures.
    




    TestResults(failed=1, attempted=3)



`doctest` nous indique qu'il y a eu 1 test sur 3 d'échoué, nous indique lequel, ce qui été attendu (`Expected`) et ce que l'on a obtenu (`Got`)

__Remarque__ :  
Il est possible de rendre un peu plus 'causant' `doctest` même lorsque les test sont bon en utilisant le paramètre optionnel `verbose` :


```python
doctest.testmod(verbose = True)
```

    Trying:
        dec2bin(5)
    Expecting:
        '101'
    ok
    Trying:
        dec2bin(1)
    Expecting:
        '1'
    ok
    Trying:
        dec2bin(107)
    Expecting:
        '1101010'        # Erreur volontaire : on a changé le dernier 1 en 0
    **********************************************************************
    File "__main__", line 15, in __main__.dec2bin
    Failed example:
        dec2bin(107)
    Expected:
        '1101010'        # Erreur volontaire : on a changé le dernier 1 en 0
    Got:
        '1101011'
    1 items had no tests:
        __main__
    **********************************************************************
    1 items had failures:
       1 of   3 in __main__.dec2bin
    3 tests in 2 items.
    2 passed and 1 failed.
    ***Test Failed*** 1 failures.
    




    TestResults(failed=1, attempted=3)



__Automatisation du lancement des tests__

Il faut ici lancer à la main les tests, ce qui n'est pas pratique.

On peut inclure directement les commandes de lancement des tests dans le script :
```Python
def fonction1():
    ...

def fonction2():
    ...

import doctest
doctest.testmod()    
```

**MAIS** les tests seront alors **lancés à chaque lecture du script**.

Problème : un programme complexe est souvent constitué d'un script principal qui utilise des fonctions regroupées dans des scripts auxiliaires (modules).

>On souhaite en général ne lancer les tests **QUE si le script actuel est exécuté en tant que script principal** et non chargé (importé via `import`) ensuite par un autre.

**Comment faire** ?

On inclue dans le script une structure du type :

```Python
Si le script actuel est lancé comme script principal:
    Effectuer les tests
```

qui se traduit par 

```Python
if __name__ == '__main__':
    import doctest
    doctest.testmod()
```


```python
def dec2bin(x):
    """Convertir l'écriture décimale en éciture binaire
    
    paramètre x : (int) un entier
    valeur renvoyée : (str) l'écriture binaire de x
    
    CU : x > 0
    
    Exemples :
    
    >>> dec2bin(5)
    '101'
    >>> dec2bin(1)
    '1'
    >>> dec2bin(107)
    '1101010'        # Erreur volontaire : on a changé le dernier 1 en 0
    """
    
    result = ''
    while x >0:
        #on effectue la division euclidienne de x par 2
        q = x // 2 
        r = x % 2
        
        #on ajoute à gauche le reste obtenue dans le résultat
        result = str(r) + result
        x = q
    
    return result

if __name__ == '__main__': # Si le script est script principal
    import doctest
    doctest.testmod()
```

### Quels tests utiliser ??

Il est bien sur impossible de tester toutes les possibilités et la réussite d'un certain nombre de tests ne garantit pas que la fonction est correcte dans tous les cas.

On essaiera d'utiliser un jeu de test (ensemble de tests) qui couvre l'ensemble des situations pouvant être rencontrées, et particulièrement les cas particuliers.

Réfléchir à un jeu de test avant même d'écrire le code d'une fonction permet souvent de prendre conscience des spécifications et des cas particuliers.

__Compléments__ :

* [documentation du module `doctest`](https://docs.python.org/3/library/doctest.html)
* [Gestion d'erreur sur python.developpez.com](https://python.developpez.com/tutoriels/apprendre-programmation-python/notions-avancees/?page=gestion-d-erreurs#LIV-C-1)
* s'intéresser aux modules `unittest` (et [un guide ici)](http://sametmax.com/un-gros-guide-bien-gras-sur-les-tests-unitaires-en-python-partie-2/) et `pytest`
