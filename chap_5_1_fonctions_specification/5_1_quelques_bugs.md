# Quelques bugs informatiques ... couteux, en vie ou en argent

## Ariane 5, vol 501 : en fumée

![ariane 5](./img/ariane_5.jpg)

Nous sommes le 4 juin 1996, jour du vol inaugural du lanceur européen Ariane 5, le vol 501. La mauvaise météo fait prendre une heure de retard à l’opération mais la lancement est donné à 9 h 35.

Après 36,7 secondes de vol, les fortes accélérations produites par l’évolution de la fusée provoquent **un dépassement d’entier dans les registres mémoire des calculateurs électroniques du système de guidage inertiel principal, qui se met aussitôt hors service.**

Le système de guidage de secours, identique au système principal, subit les mêmes effets et s’arrête à la même seconde. Le pilote automatique, qui s’appuie justement sur les informations provenant de ces systèmes de guidage inertiels, n’a alors plus aucun moyen de contrôler la fusée. L’échec de la mission est inéluctable.

La fusée explose à une altitude de 4 000 mètres au-dessus du centre spatial de Kourou, en Guyane. On ne déplore aucune victime. Mais dans l’explosion, les quatre satellites de la mission Cluster, embarquée dans la fusée, partent en fumée.

**Valeur : 370 millions de dollars.**

## La sonde Mars Climate Orbiter : aux fraises

![Mars climate orbiter](./img/mars_climate_orbiter.jpg)

Nous sommes le 23 septembre 1999. Après un voyage de 286 jours, La sonde Mars Climate Orbiter s’apprête à se mettre en orbite autour de Mars.

Le moteur est lancé mais l’engin spatial est 100 km plus proche que prévu, à 60 km de la planète. Cela le positionne environ à 25 km au-dessous du niveau limite pour bien fonctionner.

Le système de propulsion surchauffe et il est désactivé. Cela empêche le moteur de finir de brûler et propulse la sonde en dehors de l’atmosphère martienne.

Pour l’anecdote, il se murmure que Climate Orbiter poursuit actuellement son voyage en orbite autour du soleil.

L’origine du problème ? Un accident métrique. En effet, les deux équipes techniques ayant collaboré à la construction ne s’étaient pas mises d’accord sur le système métriques à utiliser. L’une de Lockheed Martin à Denver utilisait le système métrique anglais, tandis que l’autre du Jet Propulsion Laboratory de la NASA à Pasadena, en Californie utilisait le système métrique américain, comme c’est l’usage depuis près de 10 ans. La boulette…

Les experts, pour le moins perspicaces, déclareront : “Il s’agit d’un problème de processus de bout en bout”.

**Coût : 125 millions de dollars.**

## La sonde spatiale Mariner 1 : dans le vent

![Mariner 1](./img/mariner.jpg)

La sonde spatiale Mariner 1 est lancée le 22 juillet 1962 à Cap Canaveral, en Floride. Quelques minutes après le début du vol, une antenne de guidage embarquée tombe en panne.

Cela entraîne le repli sur un système radar de secours qui aurait dû pouvoir guider le vaisseau spatial. Cependant, le logiciel de ce système de guidage présente une faille fatale

Les équations utilisées pour traiter et traduire les données de suivi en instructions de vol sont alors codées sur des cartes perforées et lors du codage, un symbole essentiel est manquant : un tiret haut, souvent confondu dans les années suivantes avec un trait d’union.

L’absence de tiret haut amène l’ordinateur de guidage à compenser de manière incorrecte certains mouvements légitimes de l’engin spatial.

La sonde essaye d’atterrir mais ses manœuvres anormales indiquent qu’elle est sur le point de s’écraser, potentiellement en zone habitée.

Pour des raisons de sécurité, 293 secondes plus tard, l’ordre est donné : la sonde doit être détruite. Boum !

**Coût : 18,2 millions de dollars s’envolent dans l’atmosphère.**

## Le missile Patriot : erreur 404, scud introuvable, 28 morts

![Patriot](./img/missile_patriot.jpg)

Nous sommes le 25 février 1991 sur le champ de bataille de la guerre du Golfe.

Un Scud iraquien frappe la caserne de Dhahran, en Arabie saoudite, tue 28 soldats du 14e détachement de quartier-maître de l’armée américaine.

L’attaque aurait dû être interceptée par le système radar des missiles Patriot en service. Mais un bug logiciel dans la gestion des horodatages par le système s’est mêlée à la partie.

Conséquence : le système a scruté dans la mauvaise partie du ciel et n’a trouvé aucune cible. En l’absence de cible, la détection initiale était supposée être une piste parasite et le missile avait été retiré du système.

En cause, la batterie de missiles Patriot qui était en service depuis 100 heures, au bout desquelles l’horloge interne du système avait dérivé d’un tiers de seconde. En raison de la vitesse du missile, cela équivalait à une distance manquante de 600 mètres.

Ironie du sort, deux semaines auparavant, le 11 février 1991, les Israéliens avaient identifié le problème et avaient informé l’armée américaine et le bureau du projet Patriot, le fabricant de logiciels.

Comme solution provisoire, les Israéliens avaient recommandé de redémarrer régulièrement les ordinateurs du système.

Le fabricant a fourni un logiciel mis à jour à l’armée le 26 février.

## Le système Multidata Systems : double dose, 8 morts

![Multidata Systems](./img/mulltidata_systems.jpg)
mulltidata_systems.jpg

Multidata Systems International est une société américaine qui édite un logiciel de planification de la thérapie qui calcule la dose de rayonnement appropriée pour les patients soumis à une radiothérapie.

Nous sommes en novembre 2000 à l’Institut national du cancer (Panama), client de ce logiciel.

Cet outil permet aux radiothérapeutes de tracer sur un écran d’ordinateur la mise en place de boucliers métalliques appelés “blocs” conçus pour protéger les tissus sains du rayonnement.

Mais il ne permet aux techniciens d’utiliser que quatre blocs de blindage, et les médecins panaméens souhaitent en utiliser cinq.

Les médecins découvrent qu’ils peuvent tromper le logiciel en dessinant les cinq blocs comme un seul gros bloc avec un trou au milieu. Ils ne se rendent pas compte que le logiciel Multidata donne différentes réponses dans cette configuration en fonction de la manière dont le trou est dessiné. Dessiné dans un sens, la dose correcte est calculée. Dessiné dans un autre sens, le logiciel recommande de doubler l’exposition nécessaire.

Au moins huit patients décèdent, tandis que 20 autres reçoivent une surdose susceptible de causer des problèmes de santé importants.

Les médecins, qui étaient légalement tenus de vérifier à la main les calculs de l’ordinateur, sont inculpés de meurtre.

Et le dédommagement des victimes a plutôt salé la note.

---
Source : [Stéphanie Binet](https://horustest.io/blog/les-10-bugs-informatiques-les-plus-couteux-de-l-histoire/#)
