# Introduction à la complexité et recherche séquentielle -

* [cours : Introduction à la notion de complexité](11_1_complexite_introduction.md) 
* [TD sur la complexité](./TD/chap_11_1_TD_complexite.md)
* [cours : La recherche séquentielle](./11_1_recherche_sequentielle.md)
* [cours : retrouver expérimentalement la complexité](./11_1_mesure_temps_execution.md)
* [TD sur le cours précédent](./TD/chap_11_1_TD_mesure_temps_execution.md)



