# Recherche séquentielle d'un élément dans une liste
---

## Objectifs et savoirs-faire
---

* écrire une fonction de recherche séquentielle documentée d'un élément dans une liste ;
* savoir déterminer la complexité d'un algorithme simple.

## Problématique
---

On considère une liste `liste` contenant `n` éléments et un élément `elt`.
On cherche à déterminer si `elt` appartient à cette liste :
* si oui, on retourne le rang der `elt` dans `liste` ;
* si non, on décide de retourner `None` (on pourrait aussi décider de retourner -1)

## Algorithme de recherche séquentielle dans une liste

L'idée de cet algorithme est simple et **vous l'avez déjà réalisé plusieurs fois** :

* parcourir tous les éléments de la liste du premier au dernier via les indices;
* si l'élément est celui cherché => on renvoie l'indice (et on sort donc de la fonction)
* si on a parcouru tout le tableau sans trouver l'élément cherché, c'est qu'il n'est pas là et alors en renvoie `None`.


```python
def rech_sequentielle(liste, elt):
    """Recherche si elt est un élément de liste. Si oui renvoie le rang de elt, si non renvoie None
    
    param liste : (list)
    param elt : (unknow)
    valeur retournée : (int or None), avec 0 <= int <= len(liste) -1
    
    
    Exemples :
    >>> rech_sequentielle([4, 2, 6, 7, 8, 9, 1], 7)
    3
    >>> rech_sequentielle([4, 2, 6, 7, 8, 9, 1], 5)
    None
    >>> rech_sequentielle([], 7)
    None
    """
    
    for indice in range(len(liste)):
        if liste[indice] == elt:
            return indice
    return None
```

### Quelle est la complexité de cet algorithme ?

#### La réponse n'est pas aussi automatique que dans les exemples précédents.

En effet, le nombre d'opérations effectuées va ici dépendre de la liste de départ elle-même :
* `rech_sequentielle([4, 2, 6, 7, 8, 9, 1], 4)` ne nécessitera q'une "étape" puisque `4` est le premier élément de la liste ;
* alors que `rech_sequentielle([4, 2, 6, 7, 8, 9, 1], 1)` en nécessitera `7` (soit la longueur de la liste) puisque `1` est en dernière position
* et que `rech_sequentielle([4, 2, 6, 7, 8, 9, 1], 5)` en nécessitera aussi `7` aussi (puisque `5` n'est pas présent dans la liste).

>La complexité dépend donc ici :
>* du nombre _n_ de données à traiter ;
>* **des données elles-mêmes**.

>On a donc nécessité de différencier différents types de complexité :
>* **complexité dans le meilleur des cas** ;
>* **complexité dans le pire des cas** ;
>* **complexité moyenne**.

>Seule la __complexité dans le pire des cas__ nous intéresse cette année.

Reprenons notre code de recherche séquentielle (sans la docstring) :

```Python
def rech_sequentielle(liste, elt):
    
    for indice in range(len(liste)):
        if liste[indice] == elt:
            return indice
    return None
```

Le __pire des cas__ correspond à la situation où la valeur recherchée n'est pas présente.

Dans cette situation, on réalise :

* une évaluation de la longueur de la liste (`len(liste)`) lors de la création de l'itérateur puis 
* on effectue __n__ tours de boucle et, pour chaque tour de boucle on réalise :
  * une évaluation de l'itérateur 
  * une affectation (à la variable de boucle `indice`) ;
  * une lecture d'un élément de la liste (`liste[indice]`);
  * une comparaison.
* un retour de fonction

A chaque tour de boucle, on effectue `4` opérations.

Dans __le pire des cas__, on a donc effectué `$T(n) = 1 + 4n + 1 = 4n + 2$` opérations 

La complexité est donc un `O(n)` est est **linéaire**.

> La complexité de l'algorithme de recherche séquentielle d'un élément dans une liste de longueur `n` est en `O(n)` est est **linéaire**.
