# Mesure du temps d'exécution d'un algorithme sur votre machine

## Objectif

Les calculs effectués précédemment étaient théorique.

Nous allons ici mesurer **sur votre machine** le temps d'exécution de l'algorithme de recherche séquentielle dans le pire des cas et tracer la courbe de la complexité $`T(n)`$ en fonction de `n` afin de retrouver qu'elle est linéaire.

## Principe général

Pour cela nous allons :
* créer des listes de taille `n` progressive comprise entre 0 et 3000 ;
* pour chaque liste, mesurer le temps de recherche d'un élément n'y appartenant pas et sauvegarder ces temps dans une liste ;
* tracer le graphique représentant le temps d'exécution `$T(n)$` de la recherche en fonction de la taille `$n$` de la liste.

## Comment mesurer un temps d'exécution ?

### Le principe

Il existe des fonctions spécialisés comme `timit` (voir compléments plus bas).

L'approche utilisée ici est un peu moins précise mais suffit à nos besoins.  
Nous allons utiliser la fonction `perf_counter_ns()` du module `time`.

> `perf_counter_ns()` renvoie le temps écoulé (en nano-secondes) depuis un certain moment de référence

On va :
* regarder la valeur `t0` obtenue via `perf_counter_ns()` avant de faire la recherche ;
* effectuer la recherche ;
* regarder la valeur `t1` obtenue via `perf_counter_ns()` après ;

> La différence $`t1 - t0`$ nous donnera le temps d'exécution de la recherche

**Principe illustré**

```Python
# mémorisation du temps de début
t0 = time.perf_counter_ns()
   
rech_sequentielle(liste, -1) #-1 ne sera pas dans la liste. On est bien dans le pire des cas.
   
# mémorisation du temps de fin
t1 = time.perf_counter_ns()
    
temps_execution = (t1 - t0) / 1e9 # il faut convertir en secondes donc diviser par 10^9
```

### pour avoir une valeur plus révélatrice

Le temps d'exécution obtenu pour une seule recherche peut être trés variable car il peut dépendre de divers facteurs inhérents à l'état de la machine à ce moment-là (occupation du processeur, gestion des processus actifs, ...).

Ainsi, on préfèrera réaliser plusieurs fois la même recherche (par exemple 10 fois) et calculer le temps moyen

**Principe illustré**

```Python
# mémorisation du temps de début
t0 = time.perf_counter_ns()
    
# on réalise 10 recherches
for _ in range(10):
   rech_sequentielle(liste, -1) #-1 n'est pas dans la liste. On est bien dans le pire des cas.
   
# mémorisation du temps de fin
t1 = time.perf_counter_ns()
    
temps_10_recherches = (t1 - t0) / 1e9 # il faut convertir en secondes donc diviser par 10^9
temps_moyen_une_recherche = temps_10_recherches / 10
```

## Allons-y !

Nous allons :
* créer des listes d'entiers **positifs** de taille `n` progressive comprise entre 0 et 3000 ;

    * Une variable `taille` variera de `0` à `3000` : `for taille in range(0,3001):`  
    * Pour chaque valeur de `taille`, on crée une liste contenant les `k` premiers entiers positifs : `liste = [k for k in range(taille)]`

* pour chaque liste, mesurer le temps de recherche d'un élément n'y appartenant (`-1`) pas et sauvegarder ces temps dans une liste ;

    **code correspondant** :

```Python
l_temps = [] # la liste qui va contenir les temps d'exécution

# mémorisation du temps de début
t0 = time.perf_counter_ns()
   
for _ in range(10):
    rech_sequentielle(liste, -1)
   
# mémorisation du temps de fin
t = time.perf_counter_ns()
   
# ajout des valeurs aux tableaux
l_temps.append(((t - t0) / 1e6)/10)
```

* on réalise le tracé des courbes.  

    Pour cela on utilisera le module `mathplotlib.pyplot` et sa fonction `plot(liste_1, liste_2)` qui permet de placer dans un graphique l'ensemble des points dont les abscisses sont les éléments de `liste_1` et les ordonnées les éléments de `liste_2`.
    
    _Exemple_ :    
```Python
liste_1 = [0, 1, 2, 3]
liste_2 = [5, -1, 7, 4]
pylab.plot(liste_1, liste_2)
``` 
    Cela placera dans un repère les points de coordonnées $`(0, 5)`$, $`(1, -1)`$, $`(2, 7)`$ et $`(3, 4)`$.
    
    Pour notre recherche séquentielle, nous utiliserons en abscisse la taille de la liste (`n`) et en ordonnée le temps d'éxécution $`T(n)`$ mesuré.

_Remarque_ :

En général, la méthode `show` de `pylab` ouvre une fenêtre pour présenter le graphique obtenu, et elle est bloquante, i.e. elle ne permet plus de dialoguer avec l'interpréteur Python. Il faut fermer cette fenêtre pour pouvoir poursuivre le dialogue. Avant la fermeture de cette fenêtre, certaines actions sont possibles sur
le graphique. L'une d'elles est la sauvegarde du graphique dans un fichier image.


```python
import matplotlib.pyplot as pylab
import time

def rech_sequentielle(l, elt):
    for indice in range(len(l)):
        if l[indice] == elt:
            return indice
    return None

l_taille = [k for k in range(3001)]
l_temps = []

for taille in range(0,3001):
    # création de la liste de longueur : taille
    liste = [k for k in range(taille)]
   
    # mémorisation du temps de début
    t0 = time.perf_counter_ns()
   
    for _ in range(10):
        rech_sequentielle(liste, -1)
   
    # mémorisation du temps de fin
    t = time.perf_counter_ns()
   
    # ajout des valeurs aux tableaux
    l_temps.append(((t - t0) / 1e6)/10)


pylab.title('Temps de la recherche séquentielle')
pylab.xlabel('taille des listes')
pylab.ylabel('temps en millisecondes')
pylab.plot(l_taille, l_temps)
pylab.grid()
pylab.show()
```


    
![png](output_1_0.png)
    


> **On observe bien que le temps d'exécution s'exprime comme une fonction linéaire en fonction du nombre de données et que la complexité est linéaire**.

Nous pouvons continuer à examiner les temps d'exécutions sur des listes plus grandes. Cepandant, pour limiter le temps global, nous ferons les recherches par exemple sur des tailles de listes allant de 0 à 10 000 par pas de 100.


```python
import matplotlib.pyplot as pylab
from timeit import timeit

def rech_sequentielle(liste, elt):
   
    for indice in range(len(liste)):
        if liste[indice] == elt:
            return indice
    return None

l_tailles = [k for k in range(0,10000,100)]
l_temps = []

for taille in range(0,10000,100):
    # création de la liste de longueur : taille
    liste = [k for k in range(taille)]
    
    # mémorisation du temps de début
    t0 = time.perf_counter_ns()
   
    for _ in range(10):
        rech_sequentielle(liste, -1)
   
    # mémorisation du temps de fin
    t = time.perf_counter_ns()
   
    # ajout des valeurs aux tableaux
    l_temps.append(((t - t0) / 1e6)/10)

pylab.title('Temps de la recherche séquentielle')
pylab.xlabel('taille des listes')
pylab.ylabel('temps en secondes')
pylab.plot(l_tailles, l_temps)
pylab.grid()
pylab.show()

```


    
![png](output_3_0.png)
    


## Complément : utilisation de la fonction `timit`

> Le module `timeit` permet de mesurer le temps en secondes mis pour trier chacune des listes.

_Exemple_ : Mesure du temps d'exécution d'un lancement (`number = 1`) de la fonction `rech_sequentielle(tab, -1)`


```python
def rech_sequentielle(liste, elt):
   
    for indice in range(len(liste)):
        if liste[indice] == elt:
            return indice
    return None

# import de la fonction timeit du module timeit

from timeit import timeit

# création d'un tableau contenant les entiers allant de 0 à 999
tab = [k for k in range(1000)] 

# mesure du temps nécessaire à la recherche séquentielle de -1 dans le tableau précédent
# -1 n'étant pas dans le tableau, on est bien dans le pire des cas

timeit(lambda: rech_sequentielle(tab, -1), number = 1)
```




    9.65000000001659e-05



En utilisant le même principe que précédement, on peut réaliser 10 exécutions de la recherches (`number = 10`) et mesurer le temps moyen en divisant par 10.

Le reste du code est identique.

**listes de longueur 3000 au maximum**


```python
import matplotlib.pyplot as pylab
from timeit import timeit

def rech_sequentielle(liste, elt):
   
    for indice in range(len(liste)):
        if liste[indice] == elt:
            return indice
    return None

l_tailles = [k for k in range(0,3001)]
l_temps = []

for taille in range(0,3001):
    # création de la liste de longueur : taille
    liste = [k for k in range(taille)]
    
    # calcul du temps  d'exécution moyen sur 10 exécutions
    temps = timeit(lambda: rech_sequentielle(liste, -1), number = 10)/10
    
    # on ajoute le temps à la liste l_temps
    l_temps.append(temps)

pylab.title('Temps de la recherche séquentielle')
pylab.xlabel('taille des listes')
pylab.ylabel('temps en secondes')
pylab.plot(l_tailles, l_temps)
pylab.grid()
pylab.show()

```


    
![png](output_8_0.png)
    


**Listes de longueur 9999 au maximum en allant de 100 en 100**


```python
import matplotlib.pyplot as pylab
from timeit import timeit

def rech_sequentielle(liste, elt):
   
    for indice in range(len(liste)):
        if liste[indice] == elt:
            return indice
    return None

l_tailles = [k for k in range(0, 10000, 100)]
l_temps = []

for taille in range(0, 10000, 100):
    # création de la liste de longueur : taille
    liste = [k for k in range(taille)]
    
    # calcul du temps  d'exécution moyen sur 10 exécutions
    temps = timeit(lambda: rech_sequentielle(liste, -1), number = 10)/10
    
    # on ajoute le temps à la liste l_temps
    l_temps.append(temps)

pylab.title('Temps de la recherche séquentielle')
pylab.xlabel('taille des listes')
pylab.ylabel('temps en secondes')
pylab.plot(l_tailles, l_temps)
pylab.grid()
pylab.show()

```


    
![png](output_10_0.png)
    


Sources utilisées :
* [mathweb](https://www.mathweb.fr/euclide/2019/03/19/initiation-a-la-complexite-algorithmique/)
* [site Blaise Pascal](https://info.blaisepascal.fr/cpge-tri-et-complexite)
* et bien d'autres.
