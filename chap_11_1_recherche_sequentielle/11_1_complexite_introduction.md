# Introduction à la complexité
---

## Problématique
---

>Plusieurs algorithmes peuvent réaliser la même tâche et il est naturel de vouloir comparer leur efficacité (cad leur performance).  
>Pour cela, nous allons la quantifier à travers une quantité : **sa complexité**.

Il existe deux types de complexité :

* la complexité spatiale qui permet de quantifier **l'utilisation de la mémoire** ;
* la  complexité temporelle qui permet de quantifier la **vitesse d'exécution**.

> Dans la suite, nous ne nous intéressons qu'à la complexité temporelle.

La mesure de l'efficacité temporelle doit se faire indépendemment :
* du processeur sur lequel on va faire tourner le programme ;
* du langage ;
* du compilateur employé.

En effet, c'est l'efficacité de l'algorithme qui nous intéresse et pas la vitesse d'exécution sur une machine à un moment du code binaire résultat de la compilation de la traduction de l'algorithme dans un langage donné :
* le temps d'exécution peut varier sur une même machine ;
* en fonction du langage ou du fonctionnement du compilateur, le code binaire obtenu peut s'exécuter plus ou moins rapidement.

>Ainsi, on désire disposer d’un procédé qui nous permettrait d’évaluer l’efficacité
d’un algorithme sans l’implémenter et indépendamment de l’architecture de la machine.

## Définition

> La __complexité__ correspond aux nombres d'opérations élémentaires effectuées.
>
> On la note `T`, ou `T(n)` en fonction des situations.

Il faut se mettre d'accord sur ce qu'on appelle **opération élémentaire**, le tout étant d'utiliser la même convention pour comparer deux algorithmes.

Par exemple, on pourra considérer comme opération élémentaire :
  * toute opération arithmétique (somme, produit, quotient, ...);
  * toute comparaison ;
  * toute affectation d'une valeur à une variable (ou dans une liste);
  * tout accés à un élément d'une liste, à une valeur d'un dictionnaire, ...;

> Chacune des opératin ci-dessus ne nécessiterait évidemment pas le même temps mais **par souci de simplification, on considère que chaque opérations coûte __1__ unité de temps.**

_Exemple_ : `a = a + 1` comprend :
* une opération algébrique ;
* une affectation

Donc : 2 opérations (Le temps d'accès à la valeur contenue dans la variable `a` est négligé ici car pas comptabilisé)..

_Exemple_ :

```Python
def convert(nb_secondes):
    h = nb_secondes // 3600
    m = (nb_secondes - 3600*h) // 60
    s = nb_secondes % 60
    return h,m,s
```

Quelque soit la valeur de `nb_secondes`, on effectue :
* 1ère ligne : 1 opération et une affectation
* 2ème ligne : 3 opérations (un produit, une différence, une division entière) et une affectation ;
* 3ème ligne : une opération et une affectation ;
* 4ème ligne : le `return`

La complexité de cet algorithme est donc de 9.

_Exemple_ :

```Python
def sommeEntiers(n):
    somme = 0
    for i in range(n+1):
        somme += i
    return somme
```

Décompte des opérations effectuées :
* une affectation (`somme = 0`)
* puis **pour chaque tout de boucle** (donc **n fois** ):
    * 1 évaluation de l'itérateur (`range(..)`)
    * 1 affectation de la variable de boucle `i` (`for i ..`).  
        Je néglige ici l'appel de l'itérateur
    * 1 addition (`somme + i`)
    * 1 affectation (`somme =`)
* un `return`
    
  Ainsi, en fin de boucle, nous avons effectué `n * 4` opérations (donc `4n` opérations)

La complexité de cet algorithme est donc `T(n) = 4n + 2`. **Elle dépend du paramètre `n` d'entrée.**

**Remarque** :

Par souci de simplification (ou parce que le nombre d'opération lui est proportionnel, comme ci-dessus), on peut aussi ne s'intéresser qu'aux opérations __significatives__ réalisées  (par exemple ne compter que les comparaisons effectuées, ou les tours de boucles).

---
## Notions d'ordre de grandeur de la complexité

Lorsque _n_ devient grand, 1 devient négligeable devant _3n_ et le nombre d'opération est donc de l'odre de grandeur de `3n`, soit égale à un multiple de `n`.

On dire qu'elle est **de l'ordre de grandeur de n** et on écrira que la complexité est en __O(n)__ (grand O de n), ou encore qu'elle est **linéaire**.

__Exemples__

* Si **T(n) = 5n + 7** :  
  lorsque n devient grand, 7 devient négligeable devant _5n_.  
  La complexité est donc de l'ordre de grandeur de _5n_ et donc de _n_.
  
  __T(n) = O(n)__ et on dit que la complexité est **linéaire** (voir tableau ci-dessous)
  
  
* Si __T(n) = 3n^2 + 20n + 4__ :  
  lorsque n devient grand, 4 et _20n_ deviennent négligeables devant _3n^2_.  
  La complexité est donc de l'ordre de grandeur de _3n^2_ et donc de _n^2_.  
  
  __T(n) = O(n^2)__ et on dit que la complexité est **quadratique**
  
  
* Si __T(n)__ ne dépend pas du nombre de données traitées __n__, on dira alors que __T(n) = O(1)__ et on dit que la complexité est **constante**.

__Un peu de vocabulaire__ :

| O(...) | Type de complexité |
|:--:|:--:|
| O(1) | constante|
| O(n) | linéaire |
| O(n^2) | quadratique |
| O(n^3) | cubique |
| O(ln(n)) | logarithmique |
| O(a^n) | exponentielle |

## Retour sur le dernier exemple

```Python
def sommeEntiers(n):
    somme = 0
    for i in range(n+1):
        somme += i
    return somme
```

Dans la boucle `for`, il y a ici un nombre fini d'opération. Le nombre d'opération sera donc proportionnel au nombre de tour de boucles et l'ordre de grandeur sera donc donné par le nombre de tour de boucles.

> C'est pour cela que lors de détermination de complexités, on compte parfois uniquement le nombre de tours de boucles.

Je ferai d'autres remarques lors des exercices du TD.

Sources utilisées :
* [mathweb](https://www.mathweb.fr/euclide/2019/03/19/initiation-a-la-complexite-algorithmique/)
* [site Blaise Pascal](https://info.blaisepascal.fr/cpge-tri-et-complexite)
* et bien d'autres.
