# TD mesure du temps d'exécution

### Exercice 1

En vous inspirant du cours, adapter les scripts proposer afin de retrouver expérimentalement par mesure du temps d'exécution la complexité de l'algorithme suivant (exercice 3 du :[TD sur la complexite](./chap_11_1_TD_complexite.md)) en traçant le courbe du temps d'exécution de l'algorithme (en millisecondes) en fonction de la taille `n` de la liste utilisée

```Python
def bizarre(n):
    L = [0] * n
    for i in range(n):
        for j in range(i):
            L[j] = i
```


### Exercice 2

Même chose avec les deux algorithmes suivants (qui reprennent l'exercice 4 du même TD).

Thonny étant très lent pour les affichages, je les ai transformés en ajouts dans des listes. La complexité reste cependant la même.

```Python
def liste_colonne(matrice, num_c):
    l_sortie = []
    nb_lignes = len(matrice)
    for num_l in range(nb_lignes):
        l_sortie.append(matrice[num_l][num_c]
    
```

```Python
def affiche_colonne_2(matrice, num_c):
    l_sortie = []
    nb_lignes = len(matrice)
    nb_colonnes = len(matrice[0]) #ici, la matrice étant carrée, c'est aussi égal à nb_lignes
    for num_l in range(nb_lignes):
        for num_col in range(nb_colonnes):
            if num_col == num_c:
                l_sortie.append(matrice[num_l][num_c])
```
