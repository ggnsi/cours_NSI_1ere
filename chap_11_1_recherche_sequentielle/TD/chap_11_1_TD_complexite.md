# TD complexité

### Exercice 1

Déterminer la complexité `$T$` de l'algorithme suivant:

```Python
def myFunction(n):
    if n%3 == 0:
        p = n/3 + 2
    else:
        p = n*2 + 1
    return p
```


### Exercice 2

Déterminer l'ordre de grandeur et le type de complexité dans chacun des cas suivants :

1. `$T(n) = 5n^3 + 3n^2 - 7$` ;
2. `$T(n) = 2n + 3$` ;
3. `$T(n) = 3n + 7n^2 - 9$` ;
4. `$T(n) = 7$` ;

### Exercice 3

Considérons l'algorithme suivant :

```Python
def bizarre(n):
    L = [0] * n
    for i in range(n):
        for j in range(i):
            L[j] = i
```

1. Déterminer la complexité _T(n)_ en fonction de _n_. On prendra en compte __uniquement__ les affectation de valeurs dans la liste.  

    _Aide_ : 
    * Lorsque `i` vaut `0`, combien d'affectations sont réalisées dans la boucle imbriquée ?
    * Lorsque `i` vaut `1`, combien d'affectations sont réalisées dans la boucle imbriquée ?
    * Lorsque `i` vaut `2`, combien d'affectations sont réalisées dans la boucle imbriquée ?
    * Lorsque `i` vaut `3`, combien d'affectations sont réalisées dans la boucle imbriquée ?
    * ...
    * Lorsque `i` vaut `n - 1`, combien d'affectations sont réalisées dans la boucle imbriquée ?
    
    En déduire le calcul à réaliser qui donne, en fonction de `n`, le nombre d'affectations réalisées.
    
    * si vous êtes en spé-Maths, vous pouvez simplifier l'expression avec vos connaissances ;
    * sinon, faire une recherche sur internet pour trouver la formule.
2. Déterminer l'ordre de grandeur de _T(n)_ et le type de complexité lorsque _n_ devient grand.

### Exercice 4

Affichage d'une colonne d'une matrice comportant `n` lignes et `n` colonnes.

On considère une matrice `n` lignes et `n` colonnes représentée par une liste de liste `matrice`
On souhaite afficher la colonne d'indice `num_c`.

Voici deux fonctions ci-dessous (reprises de ce que j'ai vu dans vos scripts en cours) :

```Python
def affiche_colonne(matrice, num_c):
    nb_lignes = len(matrice)
    for num_l in range(nb_lignes):
        print(matrice[num_l][num_c], end=" ")
    print()
```

```Python
def affiche_colonne_2(matrice, num_c):
    nb_lignes = len(matrice)
    nb_colonnes = len(matrice[0]) #ici, la matrice étant carrée, c'est aussi égal à nb_lignes
    for num_l in range(nb_lignes):
        for num_col in range(nb_colonnes):
            if num_col == num_c:
                print(matrice[num_l][num_c], end=" ")
    print()
```

**On comptabilisera pour la détermination de la complexité uniquement le nombre de tour de boucle réalisés**

1. Déterminer les complexités respectives `$T_1(n)$` et `$T_2(n)$` de chacun des deux algorithmes.
2. Si `$n = 100$`, combien de tours de boucles sont réalisés dans chacun des cas ?
3. Si `$n = 1000$`, combien de tours de boucles sont réalisés dans chacun des cas ?
4. Déterminer l'ordre de grandeur et le type de complexité dans chacun des cas.
5. Quel est l'algorithme que l'on choisira ?

### Exercice 5 (si je suis en avance)

Considérons l'algorithme suivant :


```python
def max_liste(liste):
    longueur = len(liste)
    maximum = liste[0]
    for i in range(longueur):
        if liste[i] > max:
            maximum = liste[i]
    return maximum
```

1. Déterminer la complexité _T(n)_ en fonction de _n_.
2. Déterminer l'ordre de grandeur de _T(n)_ lorsque _n_ devient grand.
