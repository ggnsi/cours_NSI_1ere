# Recherche dichotomique

* [Activité préparatoire à faire à la maison](chap_11_2_activite_recherche_dichotomique.md)
* [Feuille de la suite des activités](20_21_activites_eleve_recherche_dichotomique.pdf)
* [Cours sur la recherche dichotomique](chap_11_2_cours_recherche_dichotomique.md)

* [Premiers exercices](./TD/21_22_TD_eleve_recherche_dichotomique.pdf) et le corrigé
* [Exercices suivants](./TD/11_2_TD_recherche_dichotomique.md)
