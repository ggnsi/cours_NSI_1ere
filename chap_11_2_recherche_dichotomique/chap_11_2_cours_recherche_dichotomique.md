# Recherche dichotomique dans un tableau trié

## Pré-requis

>**Il faut que le tableau soit ordonné par ordre croissant pour réaliser une recherche dichotomique**

## Introduction
---
le fait qu'un tableau soit trié facilite de nombreuses opérations.

L'une d'entre elles est la recherche d'un élément. En effet il suffit de comparer la valeur recherchée à celle située au milieu du tableau, pour diviser l'intervalle de recherche par deux.

C'est le principe de recherche par dichotomie

  > Ethymologie (https://fr.wiktionary.org/wiki/dichotomie)
  >
  > Du grec ancien διχοτομία, dikhotomia (« division en deux parties »)

## Algorithme
---

Ci-dessous une version de l'algorithme de recherche par dichotomie où :
* une pré-condition est que la liste soit triée par ordre croissant ;
* une autre pré-condition est que `elt` soit comparable avec tous les éléments de la liste (impossible par exemple de comparer un `int` et une `string`)
* si `elt` est dans la liste, on retourne son rang ;
* sinon, on retourne `None`


```python
def dichotomie(liste, elt):
    '''Recherche par dichotomie si elt appartient à la liste.
    Si oui : retourne le rang de elt, si non retourne None
    
    param liste : (list)
    param elt : ()
    valeur retournée : (int or None)
    
    CU : liste est triée par ordre croissant
    CU : elt est comparable avec tous les éléments de liste
    '''
    
    ind_g = 0
    ind_d = len(liste) - 1
    while ind_g <= ind_d:
        ind_pivot = (ind_g + ind_d) // 2
        if elt > liste[ind_pivot]:
            ind_g = ind_pivot + 1
        elif elt < liste[ind_pivot]:
            ind_d = ind_pivot - 1
        else:
            return ind_pivot
    return None
```

_Exemples_ : 


```python
# en utilisant les liste de l'activité
liste = [6, 8, 13, 15, 24, 28, 36, 41, 42, 46]
print('36, indice : ',dichotomie(liste, 36))

liste = [5, 7, 12, 14, 23, 27, 35, 40, 41, 45]
print('9, indice : ',dichotomie(liste, 9))

liste = [5, 7, 12, 14, 23, 27, 35, 40, 41, 45]
print('37, indice : ',dichotomie(liste, 37))

# mais on peut aussi faire une recherche si la liste comporte des 
# chaînes de caractères

liste = ['a', 'ba', 'bo', 'but', 'maison', 'voiture', 'zoro']
print('bo, indice : ', dichotomie(liste, 'bo'))

liste = ['a', 'ba', 'bo', 'but', 'maison', 'voiture', 'zoro']
print('bac, indice : ',dichotomie(liste, 'bac'))
```

    36, indice :  6
    9, indice :  None
    37, indice :  None
    bo, indice :  2
    bac, indice :  None
    

__Remarque__ : on utilise ici l'ordre définie par Python sur les objets manipulés : 
* ordre lexicographique pour les chaines de caractères,
* relation d'ordre classique sur les entiers.

## Complexité

### Raisonnement

Le nombre d'opérations effectuées sera, à une constante près, proportionnelle au nombre de tour de boucle effectué.
A chaque tour de boucle, au pire, on divise le nombre d'éléments possible par 2.

Ainsi, si la liste comporte au départ _n_ éléments, au pire :

- après 1 tour de boucle on a  :   _0 <= nombre éléments possibles <= n/2_
- après 2 tours de boucle on a : _0 <= nombre éléments possibles <= n/ (2^2)_
- ....
- après _k_ tours de boucle on a : _0 <= nombre éléments possibles <= n / (2^k)_

Dans le pire des cas, on va arriver à _n / (2^k) = 1_ puis encore réaliser un tour de boucle.

Lors de ce dernier tour, soit on trouve l'élément, soit les indices (*`ind_g` ou `ind_d`) sont décalés et on obtient `ind_g > ind_d`

Ainsi, dans le pire des cas, on effectue _k_ tours de boucles avec _k_ le plus petit entier tel que _n / 2^k < 1_, ou encore _2^k > n_.

Ci-dessous, un tableau indiquant les valeurs de _k_ en fonction de _n_ :

| n | k |
|:--:|:--:|
| 10 | 4 |
| 100 | 7 |
| 1000 | 10 |
| 1 000 000 | 20 |
| 1 000 000 000 | 30 |

Pour un tableau comportant 1 milliard d'éléments, on peut donc déterminer au pire en 30 tours de boucle si un élément y est présent ou non.

__Complément__ : la solution de l'équation _2^k = n_ s'écrit en fait `log_2(n)` : le logarithme en base 2 de _n_ (fonction non connue en première)

Il faudra donc prendre le plus petit entier supérieur ou égal à `log_2(n)`

> La complexité de la recherche dichotomique est donc un `O(log_2(n))`

_Tracé de la courbe de la fonction `log_2(x)`_


```python
from matplotlib import pyplot as plt
from math import log2

def representationGraphiquesLog2():
    valeurs = [k for k in range(10, 1000010,10)]
        
    imagesLog=[log2(x) for x in valeurs]
       
    plt.plot(valeurs, imagesLog, '-y', label="fonction log2")    
    plt.legend(loc="upper left") 

representationGraphiquesLog2()    
plt.show()
```


![png](output_5_0.png)


### Comparaison recherche dichotomique / recherche séquentielle

__Rappel__ :
* la complexité dans le pire des cas de la recherche séquentielle est en `O(n)`
* la complexité dans le pire des cas de la recherche dichotomique est en `O(log_2(n))`

Courbes pour une taille de liste inférieure à 100


```python
from matplotlib import pyplot as plt
from math import log2, ceil
taillesListes=[10, 20, 30, 40, 50, 60, 80, 100]
images1 = [x for x in taillesListes]
images2 = [ceil(log2(x)) for x in taillesListes]
plt.xlabel('taille de la liste triée')
plt.ylabel('Nombre de tours de boucles dans le pire des cas')
plt.plot(taillesListes,images1, '-r', label="avec le parcours séquentiel")
plt.plot(taillesListes,images2, '-b', label="avec la méthode dichotomique")
plt.legend(loc="upper left") 
plt.show()

```


![png](output_7_0.png)


Courbes pour une taille de liste inférieure à 10000


```python
from matplotlib import pyplot as plt
from math import log2, ceil
taillesListes=[1, 1000, 2000, 3000, 4000, 5000, 6000, 8000, 10000]
images1 = [x for x in taillesListes]
images2 = [ceil(log2(x)) for x in taillesListes]
plt.xlabel('taille de la liste triée')
plt.ylabel('Nombre de tours de boucles dans le pire des cas')
plt.plot(taillesListes,images1, '-r', label="avec le parcours séquentiel")
plt.plot(taillesListes,images2, '-b', label="avec la méthode dichotomique")
plt.legend(loc="upper left") 
plt.show()
```


![png](output_9_0.png)


__La différence de performance dans le pire des cas entre les deux types de méthodes est très importante.__

## Terminaison de l'algorithme

Nous allons ici démontrer que l'algorithme se termine après un nombre fini d'opérations.

_Algorithme sans la docstring_ :


```python
def dichotomie(liste, elt):
    
    ind_g = 0
    ind_d = len(liste) - 1
    while ind_g <= ind_d:
        ind_pivot = (ind_g + ind_d) // 2
        if elt > liste[ind_pivot]:
            ind_g = ind_pivot + 1
        elif elt < liste[ind_pivot]:
            ind_d = ind_pivot - 1
        else:
            return ind_pivot
    return None
```

### Variant de boucle : généralité

L'algorithme comporte une boucle _while_. On ne connait donc pas à l'avance le nombre d'itérations.
Nous devons donc démontrer que __dans tous les cas__, la boucle va se terminer et ne pas tourner indéfiniment.

__Méthode__ : on détermine un __variant de boucle__.

> Un __variant__ de boucle est une quantité
> * _entière_
> * _positive_ au début de chaque itération
> * _qui décroit strictement_ à chaque itération de la boucle.

Trouver une telle quantité, c'est pouvoir affirmer que la boucle va se terminer.

En effet, sinon, cette suite de nombre entiers étant strictement décroissante, elle finirait par devenir négative, ce qui est impossible.

### pour notre algorithme

Considérons ici la quantité `v(n) = ind_d(n) - ind_g(n)`, où `ind_d(n)` et `ind_g(n)` sont les valeurs de `ind_d` et `ind_g` après _n_ itérations de la boucle.

Prouvons qu'elle est un variant de boucle.

* cette quantité est entière puisque tous les indices sont entiers ;
* plaçons nous au début de la _n_-ième itération.
  étant entré dans la boucle, la condition `ind_g <= ind_d` est vérifiée et `v(n) >= 0`.
  
  Prouvons alors que soit la boucle se termine, soit _v(n)_ aura diminué d'au moins une unité.
  
  3 cas se présentent :
    * Soit `elt > liste[ind_pivot]` : alors `ind_g(n+1) = ind_pivot + 1 > ind_pivot >= ind_g(n)` et `ind_d(n+1) = ind_d(n)`.  
    La différence `v(n+1) = ind_d(n+1) - ind_g(n+1)` aura donc diminué ;
    * Soit `elt < liste[ind_pivot]` : alors `ind_d(n+1) = ind_pivot - 1 < ind_pivot <= ind_d(n)` et `ind_g(n+1) = ind_g(n)`.  
    La différence `v(n+1) = ind_d(n+1) - ind_g(n+1)` aura donc diminué ;
    * Soit `elt = liste[ind_pivot]` et la boucle se termine.

On a ainsi prouvé que `v(n)` est un variant de boucle et donc que la boucle se termine.

## Correction de l'algorithme

> La correction de l'algorithme consiste à prouver qu'il renvoie bien le résultat voulu.

La correction n'est pas au programme, mais ceux intéressés peuvent se rapporter à [cette page](https://professeurb.github.io/articles/dichoto/)
