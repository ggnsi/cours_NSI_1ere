# TD - Recherche dichotomique

### Exercice 1

Etre capable d’écrire la fonction `recherche_dico(liste, elt`) qui recherche la présence de `elt` dans liste en utilisant la recherche dichotomoique.  
Cette fonction renvoie l’indice de `elt` si `elt` est présent dans la liste ou `None` sinon.

### Exercice 2

On recherche par dichotomie si une valeur est présente dans un tableau de
taille `n = 100 000`.

1.	Combien d’itérations seront au maximum nécessaire ?
2.	Comment évolue ce nombre d’itération si on mutiplie la taille du tableau par 2 ? par 8 ?

### Exercice 3

On suppose que l'on a un annuaire qui contient les huit milliards d'êtres humains dans l'ordre alphabétique de leurs nom et prénom.  
Pour simplifier, on suppose que tous les êtres humains ont des couples noms, prénoms différents.

Combien de comparaisons sont nécessaires pour retrouver une personne dans cet annuaire ?

### Exercice 4

La page suivante contient une liste de 336531 mots du français.

[https://chrplr.github.io/openlexicon/datasets-info/Liste-de-mots-francais-Gutenberg/liste.de.mots.francais.frgut.txt](https://chrplr.github.io/openlexicon/datasets-info/Liste-de-mots-francais-Gutenberg/liste.de.mots.francais.frgut.txt)

Après avoir téléchargé le fichier, vous pouvez importer les mots dans une liste avec le code python suivant (_Attention_ : ce fichier doit être sauvegardé dans le même répertoire que celui où l'est votre script).

Vous mettrez le code en début de script.  
Vous aurez alors une variable globale `mots` contenant la liste de tous les mots.

```Python
# initialisation de la liste de mots vide
mots = []
with open('liste.de.mots.francais.frgut.txt', encoding = 'utf-8') as f:
    mots = [line.rstrip() for line in f]

mots.sort() # trie la liste par ordre croissant    
```

Une fois le script évalué, vous pouvez vérifier que l'importation s'est bien passée en tapant dans la console :

```Python
len(mots) # doit renvoyer 336531
```

On souhaite rechercher si un mot appartient ou non à notre liste de mots à l'aide d'une recherche dichotomique.

1. Combien d'itérations seront nécessaires au minimum ? Pour quel mot ?
2. Combien d'itérations seront nécessaires au maximum ?
3. Ecrire un prédicat `est_dans_liste(mot)` qui retourne donc `True` si le mot fait partie de notre liste et `False` sinon.


