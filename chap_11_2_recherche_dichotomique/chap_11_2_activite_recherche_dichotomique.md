# Recherche Dichotomique

## Activité à préparer à la maison

* Jouer au jeu du [plus ou moins](http://guillonprof.free.fr/nsi/plus_moins.html) et essayer de déterminer la stratégie qui vous semble la plus efficace.

* Décrire cette stratégie de la manière la plus précise possible sous forme d'algorithme en langage naturel.

* En combien de coups peut-on gagner au minimum ?

* En combien de coups peut-on gagner au maximum ?


