# Introduction - données en table : réunion de tables, jointure

## Réunion de tables

> On parle de la réunion de deux tables lorsqu'on souhaite réunir les données de deux tables qui ont les mêmes attributs.

__Méthode : pour réunir deux tables ayant les mêmes structures (mêmes attributs), on utilise la concaténation `+`__

En effet, les tables étant des listes de dictionnaires, on peut concaténer des listes avec `+`.  

_Exemple_ :


```python
# on suppose les tables table1 et table2 déjà importées

table_reunion = table1 + table2
```

_Remarque_ : il est parfois nécessaire d'ajouter un attribut correspondant à la spécificité de chaque table (voir exercice ci-dessous).

### Exercice 1

On dispose de deux fichiers csv (__attention__ : dans ces deux fichiers, le séparateur est le point-virgule.)

* Le premier correspondant au calendrier des saisons des légumes : [legumes.csv](./legumes.csv)

|Nom |Janvier|	Février |	Mars 	|Avril |	Mai |	Juin |	Juillet 	|Août 	|Septembre |	Octobre |	Novembre |	Décembre|
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|Artichauts | | | | |1|	1 |	1| | | | |  |				
|Asperges 	| | | |1 |1|	1 |	1| | | | |  |			
Aubergines 	| | | | |1|	1 |	1|1 |1 | | |  |			
|... |	... |	... |	... |	... |... |	... |	... |	... |	... |	... |	... |	...|

* Le deuxième est similaire et correspond au calendrier des saisons des fruits : [fruits.csv](./fruits.csv)

On souhaite réunir les deux tables en ajoutant l'attribut `Nature` qui permet de savoir si on a un fruit ou un légume.

__A faire__ :

Concevoir un script qui permet :
* de charger les deux fichiers dans deux listes de dictionnaires.  
  On pourra à cet effet créer (ou ré-utiliser) une fontion `importcsv(nomfichier, separateur)` qui retourne la table importée du fichier csv sous forme de liste de dictionnaire.
* de réunir les deux listes en une seule qui corresponde à la table souhaitée.

## Jointure de tables

> On parlera ici de jointure de deux tables lorsqu'on souhaite combiner les données de deux tables sur le critère d'un attribut en commun.

### Exercice 2

On dispose de deux fichiers csv correspondant aux tables suivantes (séparateur : le point-virgule `;`):

* Population et superficie des Haut-de-France par département en 2017 : [population_hdf.csv](./population_hdf.csv)

|code |	population| superficie |
|:--:|:--:|:--:|
|02| 	534490| 7362 |
|59| 	2604361| 5743 |
|60| 	824503| 5860 |
|62| 	1468018| 6671 |
|80| 	572443| 6170 |

* Correspondance des numéros et des noms de départements : [departements.csv](./departements.csv)

|code |	nom|
|:--:|:--:|
|01 |	Ain|
|02 |	Aisne|
|03 |	Allier|
|... |	...|

Ces deux tables ont l'attribut `code` commun.  
La __jointure__ des deux tables sera la table obtenue en prenant toutes les valeurs des attribus des enregistrements des deux tables qui ont le même code.

On souhaite donc obtenir la table suivante :

|code |	nom |	population| superficie |
|:--:|:--:|:--:|:--:|
|02| 	Aisne 	|534490|7362 |
|59|	Nord 	|2604361|5743 |
|60|	Oise 	|824503|5860 |
|62| 	Pas-de-Calais| 	1468018|6671 |
|80|	Somme 	|572443|6170 |

__A faire__ :

Concevoir un script qui permet :

* de charger les deux tables dans deux listes de dictionnaires (utiliser la fonction `import_csv`  déjà codée et utilisée dans d'autres exercices) ;
* de réaliser la jointure des deux tables.  
  Pour cela: 
     * on commencera par créer une fonction `fusion(dict_pop, dict_nom)` qui retourne le dictionnaire correspondant à la fusion de `dict_pop` (de la table population) et `dict_nom` (de la table des noms) lorsque ceux-cis ont même valeur de `code`.
         Ce dictionnaire retourné aura donc comme clés : `code`, `population`, `superficie` et `nom` et les valeurs associées seront à reprendre de `dict_pop` ou de `dict_nom`.
         
        _Exemple_ :
         
        ```Python
        >>> enr_1 = {'code': '02', 'population': '534490', 'superficie': '7362'}
        >>> enr_2 = {'code': '01', 'nom': 'Ain'}
        >>> fusion(enr_1, enr_2)
        {'code': '02', 'nom': 'Ain', 'population': '534490', 'superficie': '7362'}
        ```

        Le code de la fonction `fusion` est de la forme (où vous devrez compléter !):
        
        ```Python
        def fusion(dict_pop, dict_nom):

            dict_fusion = { 'code' : ..., 'nom' : dict_nom[...], ...}

            return dict_fusion
        ```

     * puis on complètera le script afin de créer une liste de dictionnaires `table_joint` correspondant à la jointure des deux tables précédentes.
     
      _Aide_ : pour chaque enregistrement de la table_1, il faudra chercher l'enregistrement de la table_2 ayant le même `code` avant d'ajouter le résultat de la fusion des enregistrements à la table de sortie.
      
      _version expert_ : On pourra chercher une manière de  créer la jointure en utilisant une définition en compréhension.
