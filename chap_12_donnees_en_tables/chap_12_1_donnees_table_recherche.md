## IV Recherche dans une table

_importation de la table_ :


```python
import csv
table = []

with open('personnes.csv', "r", encoding='utf-8') as csv_file:
   it_dictreader = csv.DictReader(csv_file, delimiter=',') 
    
   table = [dict(enregistrement) for enregistrement in it_dictreader]
```


```python
for elt in table:
    print(elt)
```

    {'nom': 'Durand', 'prenom': 'Jean', 'age': '32', 'taille': '150', 'ville': 'Lille'}
    {'nom': 'Dupont', 'prenom': 'Christophe', 'age': '51', 'taille': '165', 'ville': 'Santes'}
    {'nom': 'Terta', 'prenom': 'Henry', 'age': '37', 'taille': '174', 'ville': 'Loos'}
    {'nom': 'Kapri', 'prenom': 'Leon', 'age': '45', 'taille': '145', 'ville': 'Lille'}
    {'nom': 'Lenard', 'prenom': 'Michel', 'age': '17', 'taille': '190', 'ville': 'Wavrin'}
    {'nom': 'Lenard', 'prenom': 'Georges', 'age': '15', 'taille': '160', 'ville': 'Loos'}
    {'nom': 'Herpan', 'prenom': 'stephan', 'age': '22', 'taille': '176', 'ville': 'Lille'}
    {'nom': 'Mastic', 'prenom': 'Brice', 'age': '61', 'taille': '155', 'ville': 'Santes'}
    {'nom': 'Pouyeau', 'prenom': 'Maxime', 'age': '11', 'taille': '130', 'ville': 'Loos'}
    {'nom': 'Arox', 'prenom': 'Gilles', 'age': '51', 'taille': '174', 'ville': 'Loos'}
    {'nom': 'Follin', 'prenom': 'Paul', 'age': '32', 'taille': '156', 'ville': 'Haubourdin'}
    

### 1. En fonction de la valeur d'un attribut

On souhaite retourner tous les enregistrements (ou parties d'enregistrements) pour lesquels un attribut prend une valeur donnée.

On parcourt la table et on ajoute les enregistrements (ou parties d'enregistrements) à la liste des résultats.

_Exemple_ :

On souhaite retourner tous les enregistrements pour lesquels l'attribut `ville` a la valeur `Loos`


```python
result = []

for elt in table:
    if elt['ville'] == 'Loos':
        result.append(elt)
```

une version plus élégante avec une liste en compréhension


```python
result = [elt for elt in table if elt['ville'] == 'Loos']
print(result)
```

    [{'nom': 'Terta', 'prenom': 'Henry', 'age': '37', 'taille': '174', 'ville': 'Loos'}, {'nom': 'Lenard', 'prenom': 'Georges', 'age': '15', 'taille': '160', 'ville': 'Loos'}, {'nom': 'Pouyeau', 'prenom': 'Maxime', 'age': '11', 'taille': '130', 'ville': 'Loos'}, {'nom': 'Arox', 'prenom': 'Gilles', 'age': '51', 'taille': '174', 'ville': 'Loos'}]
    

Si on ne souhaite extraire que les noms des personnes habitant Loos :


```python
result = [elt['nom'] for elt in table if elt['ville'] == 'Loos']
print(result)
```

    ['Terta', 'Lenard', 'Pouyeau', 'Arox']
    

Créer une liste des tuples `(nom, prénom)` des personnes habitant Loos


```python
result = [(elt['nom'], elt['prenom']) for elt in table if elt['ville'] == 'Loos']
print(result)
```

    [('Terta', 'Henry'), ('Lenard', 'Georges'), ('Pouyeau', 'Maxime'), ('Arox', 'Gilles')]
    

## 2. avec des contraintes multiples sur les valeurs d'attributs

On peut vouloir filtrer la table selon plusieurs contraintes : par exemple chercher tous les enregistrements :
* pour lesquels `ville` est `Loos` ;
* pour lesquels `taille` est supérieure ou égale à 170.

### 2.1 méthode 1


```python
result = []

for elt in table:
    if elt['ville'] == 'Loos' and int(elt['taille']) >= 170:
        result.append(elt)
```


```python
result = [elt for elt in table if (elt['ville'] == 'Loos' and int(elt['taille']) >= 170)]
print(result)
```

    [{'nom': 'Terta', 'prenom': 'Henry', 'age': '37', 'taille': '174', 'ville': 'Loos'}, {'nom': 'Arox', 'prenom': 'Gilles', 'age': '51', 'taille': '174', 'ville': 'Loos'}]
    

Si on ne veut extraire que les noms des personnes vérifiant les contraintes précédentes :


```python
result = [elt['nom'] for elt in table if (elt['ville'] == 'Loos' and \
                                                         
                                                         int(elt['taille']) >= 170)]
print(result)
```

    ['Terta', 'Arox']
    

### 2.2 méthode 2

On peut créer un _prédicat_ de validation des contraintes renvoyant `True` si l'enregistrement vérifie les contraintes et utiliser ce prédicat dans la construction de la liste ensuite :


```python
def est_valide(elt):
    return elt['ville'] == 'Loos' and int(elt['moyenne']) <= 10

result = [elt for elt in table if est_valide(elt)]
print(result)
```

    [{'nom': 'Terta', 'prenom': 'Henry', 'age': '37', 'moyenne': '8', 'ville': 'Loos'}, {'nom': 'Arox', 'prenom': 'Gilles', 'age': '51', 'moyenne': '9', 'ville': 'Loos'}]
    
