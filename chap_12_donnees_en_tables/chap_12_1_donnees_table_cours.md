# Traitement des données en table

## I Introduction

Avec le numérique, la collecte, le traitement et l'utilisation des données est devenu omniprésent :

* cartes de fidélité ;
* sites visités sur internet et comportements sur les sites ;
* données médicales ;
* position des moyens de transports en temps réel, disponibilité des vélos sur des stations de vélos partagés ;
* ...

De nombreuses institutions mettent à disposition des données de manière ouverte (c'est à dire des données dont l'accès est public et libre de droit, tout comme leur exploitation. Voir aussi [Wikipedia](https://fr.wikipedia.org/wiki/Donn%C3%A9es_ouvertes)).  
  _Exemple_ : [Site des données ouvertes de la MEL](https://opendata.lillemetropole.fr/pages/home/)

L'utilisation de ces données permet de rendre ou d'améliorer les services :

* temps d'attente avant le prochain bus dans des applications ;
* modélisation d'évolution d'épidémie (Exemple sur la covid) ;

mais sont aussi pour une bonne part utilisées à des fins commerciales (publicité ciblée).

Des questions éthiques se posent également :

* ma vie privée est-elle préservée ?
* comment protéger des données médicales ?
* espionnage des données
* ...

L'union européenne a mis en place une réglementation sur la protection des données personnelles : la RGPD :

* [RGPD : de quoi parle-t-on ?](https://www.cnil.fr/fr/rgpd-de-quoi-parle-t-on)
* [Affiche entreprise : adoptez les bons réflexes](./docs/affiche-adoptez-les-6-bons-reflexes.pdf)

## II Tables de données

### 1. Définition

Les données peuvent souvent être représentées sous forme de tableau.

> Une table est un ensemble de données organisées sous forme d'une tableau. On dit que c'est une __collection d'éléments (ou d'enregistrements)__.  
> Chaque _ligne_ du tableau correspond à un élément (ou enregistrement).
> Les _colonnes_ représentent les __attributs__ (ou __descripteurs__ ou encore __clés__) d'un élément.
> Chaque ligne comporte pour un enregistrement les différentes __valeurs__ des __attributs__.

Pour un attribut donné, toutes les valeurs sont de même type (toutes des entiers ou des chaînes de caractères, ...)

_Exemple_ :

Le tableau ci-dessous est une table comportant :
* 5 attributs : nom, prenom, age, moyenne, ville
* 11 enregistrements

|nom|prenom|age|taille|ville|
|:---:|:---:|:---:|:---:|:---:|
|Durand|Jean|32|150|Lille|
|Dupont|Christophe|51|165|Haubourdin|
|Terta|Henry|37|174|Loos|
|Kapri|Leon|45|145|Lille|
|Lenard|Michel|17|190|Wavrin|
|Lenard|Georges|15|160|Loos|
|Herpan|stephan|22|176|Lille|
|Mastic|Brice|61|155|Santes|
|Pouyeau|Maxime|11|130|Loos|
|Arox|Gilles|51|74|Loos|
|Follin|Paul|32|156|Haubourdin|

### 2. Format CSV

Pour stocker ces données, on utilise très souvent le format CSV.

>Le format CSV (Comma-separated values c’est à dire valeurs séparées par des
virgules) est un format texte permettant de représenter les données structurées.  
Chaque ligne de ce fichier texte correpond à un enregistrement. Chaque attribut 
est séparé par un délimiteur qui est la virgule.

_Remarque_ :  
Très souvent en pratique le séparateur est le point-virgule `;`.  
L'écriture des décimaux se faisant avec la virgule comme séparateur (ex : `12,4`), cela évite les confusions (c'est prévu par la définition du format CSV mais cela évite les lourdeurs et facilite la lecture humaine).

_Exemple_ :  
Ci-dessous, visualisation du fichier CSV correspondant à la table précédente.

> __Remarque importante__ la première ligne du fichier contient les noms des attributs


```python
nom,prenom,age,taille,ville
Durand,Jean,32,150,Lille
Dupont,Christophe,51,165,Santes
Terta,Henry,37,174,Loos
Kapri,Leon,45,145,Lille
Lenard,Michel,17,190,Wavrin
Lenard,Georges,15,160,Loos
Herpan,stephan,22,176,Lille
Mastic,Brice,61,155,Santes
Pouyeau,Maxime,11,130,Loos
Arox,Gilles,51,174,Loos
Follin,Paul,32,156,Haubourdin
```

## III Importation et représentation des données en Python

_Problématiques_ :
* Comment lire un fichier CSV avec Python ? 
* Comment stocker ces données de manière simple et efficace avec les structures connues en Python ?

Nous allons utiliser le module `csv` qui comporte déjà un certain nombre d'outils évitant de gérer toute la lecture du fichier csv à la main.

### 1. Première idée : utilisation d'une liste de liste

**Une première idée est de stocker la table dans une liste de liste** :
* la première sous-liste a pour éléments les attributs ;
* puis chaque sous-liste a pour éléments les valeurs d'un enregistrement

```Python
table = [ [ liste des attributs],
         [premier enregistrement],
         ['Dupont', 'Christophe', '51', '13', 'Haubourdin'],
         ...
        ]
```

La table prise en exemple serait donc représentée par la liste de liste suviante :

```Python
[['nom', 'prenom', 'age', 'taille', 'ville'],
 ['Durand', 'Jean', '32', '150', 'Lille'],
 ['Dupont', 'Christophe', '51', '165', 'Santes'],
 ['Terta', 'Henry', '37', '174', 'Loos'],
 ['Kapri', 'Leon', '45', '145', 'Lille'],
 ['Lenard', 'Michel', '17', '190', 'Wavrin'],
 ['Lenard', 'Georges', '15', '160', 'Loos'],
 ['Herpan', 'stephan', '22', '176', 'Lille'],
 ['Mastic', 'Brice', '61', '155', 'Santes'],
 ['Pouyeau', 'Maxime', '11', '130', 'Loos'],
 ['Arox', 'Gilles', '51', '174', 'Loos'],
 ['Follin', 'Paul', '32', '156', 'Haubourdin']]
```

__code d'importation du fichier CSV__


```python
import csv
table = []
csv_file = open('personnes.csv', "r", encoding='utf-8')
it_reader = csv.reader(csv_file, delimiter=',') 
for enregistrement in it_reader:
    table.append(enregistrement)
csv_file.close()
```

Explications ligne par ligne :

* on importe le module CSV ;
* on ouvre le fichier `personnes.csv` en lecture (fonction open avec le paramètre "r" pour read). `csv_file` est un objet
* on construit un objet `reader` grâce à la fonction `csv.reader`.  
  Celle ci génère un itérable sotcké dans la variable `it_reader` qui va successivement renvoyer sous forme de liste les valeurs de chaque enregistrement.
* on initialise la table avec une liste vide
* on parcours `it_reader` et on ajoute chaque enregistrement à la liste `table`
* on ferme l'accès au fichier sur le disque


```python
for ligne in table:
    print(ligne)
```

    ['nom', 'prenom', 'age', 'taille', 'ville']
    ['Durand', 'Jean', '32', '150', 'Lille']
    ['Dupont', 'Christophe', '51', '165', 'Santes']
    ['Terta', 'Henry', '37', '174', 'Loos']
    ['Kapri', 'Leon', '45', '145', 'Lille']
    ['Lenard', 'Michel', '17', '190', 'Wavrin']
    ['Lenard', 'Georges', '15', '160', 'Loos']
    ['Herpan', 'stephan', '22', '176', 'Lille']
    ['Mastic', 'Brice', '61', '155', 'Santes']
    ['Pouyeau', 'Maxime', '11', '130', 'Loos']
    ['Arox', 'Gilles', '51', '174', 'Loos']
    ['Follin', 'Paul', '32', '156', 'Haubourdin']
    


```python
print(table)
```

    [['nom', 'prenom', 'age', 'taille', 'ville'], ['Durand', 'Jean', '32', '150', 'Lille'], ['Dupont', 'Christophe', '51', '165', 'Santes'], ['Terta', 'Henry', '37', '174', 'Loos'], ['Kapri', 'Leon', '45', '145', 'Lille'], ['Lenard', 'Michel', '17', '190', 'Wavrin'], ['Lenard', 'Georges', '15', '160', 'Loos'], ['Herpan', 'stephan', '22', '176', 'Lille'], ['Mastic', 'Brice', '61', '155', 'Santes'], ['Pouyeau', 'Maxime', '11', '130', 'Loos'], ['Arox', 'Gilles', '51', '174', 'Loos'], ['Follin', 'Paul', '32', '156', 'Haubourdin'], []]
    

__ATTENTION__ : toutes les valeurs sont importées comme étant des chaînes de caractère

L'utilisation de `with` permet de simplifier un peu le code (indentation et suppression du `close` automatiquement fait).


```python
import csv
table = []

with open('personnes.csv', "r", encoding='utf-8') as csv_file:
    it_reader = csv.reader(csv_file, delimiter=',') 
    for enregistrement in it_reader:
        table.append(enregistrement)
```

Simplifions encore plus avec l'utilisation d'une liste en compréhension :


```python
import csv

with open('personnes.csv', "r", encoding='utf-8') as csv_file:
    it_reader = csv.reader(csv_file, delimiter=',') 
    
    table = [enregistrement for enregistrement in it_reader]
```

__Problèmes posés par cette représentation (liste de listes)__ :

* Les attributs sont stockés comme un enregistrement (le premier ici), il faudra donc faire attention de systèmatiquement l'exclure (donc ignorer la première sous-liste) ;
* Supposons que l'on rechercher toutes les personnes qui habitent `Wavrin` :
    Il faut alors se souvenir que l'attribut `ville` correspond à `sous_liste[4]` (et de manière générale à la correspondance entre attribut et indice), **ce qui n'est pas très pratique**.

### 2. Deuxième idée : utilisation d'une liste de dictionnaire

Il est plus intéressant de stocker un enregistrement sous forme de dictionnaire plutôt que sous forme de liste.  
En effet, accéder à la ville se fera alors simplement avec `enregistrement['ville']`

>Un enregistrement sera alors un dictionnaire de la forme `dict( 'attribut' : 'valeur')`  
et **la table sera une liste de dictionnaires**.

_Exemple_ le premier enregistrement sera stocké sous la forme :

```Python
{'nom': 'Durand', 'prenom': 'Jean', 'age': '32', 'moyenne': '12', 'ville': 'Lille'}
```

et la table serait stoquée ainsi sous forme d'une liste de dictionnaires :

```Python
[{'nom': 'Durand', 'prenom': 'Jean', 'age': '32', 'taille': '150', 'ville': 'Lille'}, 
 {'nom': 'Dupont', 'prenom': 'Christophe', 'age': '51', 'taille': '165', 'ville': 'Santes'},
 {'nom': 'Terta', 'prenom': 'Henry', 'age': '37', 'taille': '174', 'ville': 'Loos'},
 {'nom': 'Kapri', 'prenom': 'Leon', 'age': '45', 'taille': '145', 'ville': 'Lille'},
 {'nom': 'Lenard', 'prenom': 'Michel', 'age': '17', 'taille': '190', 'ville': 'Wavrin'},
 {'nom': 'Lenard', 'prenom': 'Georges', 'age': '15', 'taille': '160', 'ville': 'Loos'},
 {'nom': 'Herpan', 'prenom': 'stephan', 'age': '22', 'taille': '176', 'ville': 'Lille'},
 {'nom': 'Mastic', 'prenom': 'Brice', 'age': '61', 'taille': '155', 'ville': 'Santes'},
 {'nom': 'Pouyeau', 'prenom': 'Maxime', 'age': '11', 'taille': '130', 'ville': 'Loos'},
 {'nom': 'Arox', 'prenom': 'Gilles', 'age': '51', 'taille': '174', 'ville': 'Loos'}, 
 {'nom': 'Follin', 'prenom': 'Paul', 'age': '32', 'taille': '156', 'ville': 'Haubourdin'}]
```

__code d'importation du fichier CSV__

Le principe est le même que précédemment mais on utilise la fonction `DictReader` (attention aux majuscules) qui renvoie un itérable contenant des dictionnaires ordonnés que l'on convertit en dictionnaire "classique" avec la fonction `dict`.


```python
import csv
table = []

with open('personnes.csv', "r", encoding='utf-8') as csv_file:
    it_dictreader = csv.DictReader(csv_file, delimiter=',') 
    for enregistrement in it_dictreader:
        table.append(dict(enregistrement))
```

ou avec une liste en compréhension :


```python
import csv
table = []

with open('personnes.csv', "r", encoding='utf-8') as csv_file:
   it_dictreader = csv.DictReader(csv_file, delimiter=',') 
    
   table = [dict(enregistrement) for enregistrement in it_dictreader]
```


```python
print(table)
```

    [{'nom': 'Durand', 'prenom': 'Jean', 'age': '32', 'taille': '150', 'ville': 'Lille'}, {'nom': 'Dupont', 'prenom': 'Christophe', 'age': '51', 'taille': '165', 'ville': 'Santes'}, {'nom': 'Terta', 'prenom': 'Henry', 'age': '37', 'taille': '174', 'ville': 'Loos'}, {'nom': 'Kapri', 'prenom': 'Leon', 'age': '45', 'taille': '145', 'ville': 'Lille'}, {'nom': 'Lenard', 'prenom': 'Michel', 'age': '17', 'taille': '190', 'ville': 'Wavrin'}, {'nom': 'Lenard', 'prenom': 'Georges', 'age': '15', 'taille': '160', 'ville': 'Loos'}, {'nom': 'Herpan', 'prenom': 'stephan', 'age': '22', 'taille': '176', 'ville': 'Lille'}, {'nom': 'Mastic', 'prenom': 'Brice', 'age': '61', 'taille': '155', 'ville': 'Santes'}, {'nom': 'Pouyeau', 'prenom': 'Maxime', 'age': '11', 'taille': '130', 'ville': 'Loos'}, {'nom': 'Arox', 'prenom': 'Gilles', 'age': '51', 'taille': '174', 'ville': 'Loos'}, {'nom': 'Follin', 'prenom': 'Paul', 'age': '32', 'taille': '156', 'ville': 'Haubourdin'}]
    
