# données en table : TD2

### Exercice 1

Dans cet exercice, on s'intéresse aux __Pokémons__.

![image Pokémon](./img/pokemon.png)


On dispose d'une base de données au format csv (en anglais, donc le séparateur est la virgule) de l'ensemble des Pokémons et de leurs caractéristiques.

1. Écrire la fonction `importcsv(nom_fichier, separateur)` qui retourne sous la forme liste de dictionnaires les données extraites du fichier `num_fichier` .

  On utilisera dans le script cette fonction pour importer la table dans la variable `table_poke`.

2. Écrire une fonction `l_attrib(table)` qui renvoie la liste des _attributs_ de la `table` passée en paramètre.
3. Ecrire une fonction `aff_liste(liste)` qui permet d'afficher tous les éléments d'une liste les uns en dessous des autres. L'utiliser pour afficher la liste des attribus.

4. Ecrire une fonction `l_type1(table)` qui renvoie la liste de toutes les valeurs possibles de l'attribut `Type 1` (chaque valeur possible ne doit apparaître qu'une seule fois).

_Pour aller plus loin_ : Ecrire une fonction `l_type1(table, attribut)` qui renvoie la liste de toutes les valeurs possibles de l'attribut `attribut` (chaque valeur possible ne doit apparaître qu'une seule fois).

5. Écrire une fonction `l_attack(table, val_min)` qui renvoie la liste des tuples `(Name, Type2)` de tous les Pokémons dont l'attribut `Attack` est supérieur à une valeur passée en paramètre. 

6. Écrire une fonction `moy_hp_gen(table, num_generation)` qui retourne la moyenne des points de vie (attribut `HP`) des Pokémons d'une génération (attribut Generation) passée en paramètre.  
   Une condition d'utilisation de cette fonction est que `num_generation` existe.

7. Écrire une fonction `moy_hp_tot(table)` qui affiche les moyennes des points de vie des Pokémons pour chaque génération : 

```Python
La génération 1 a une moyenne de HP de ...
La génération 2 a une moyenne de HP de ...
...
```

_Réponses attendues_ :

```Python
La génération 1 a une moyenne de HP de 65.81927710843374
La génération 2 a une moyenne de HP de 71.20754716981132
La génération 3 a une moyenne de HP de 66.54375
La génération 4 a une moyenne de HP de 73.08264462809917
La génération 5 a une moyenne de HP de 71.78787878787878
La génération 6 a une moyenne de HP de 68.26829268292683
```


8. Écrire une fonction `project(table, type_2)` qui retourne la table (liste de dictionnaires) comportant les enregistrements des pokémons dont l'attribut `Type 2` correspond à celui `type_2` passé en paramètre.

9. Polo possède sa collection de pokémons qu'il a sauvegardée sous forme de table dans le fichier [pokemon_toto.csv](./pokemon_toto.csv).  
Sa table comporte le nom du pokémon, le nombre de cartes de ce personnage que possède Toto et le numéro du classeur où il l'a rangée.

_Exemple volontairement limité utilisé_ :

| nom | nombre | classeur |
|:--:|:--:|:--:
| Arbok | 2 | 4 |
| Mankey | 1 | 3 |
| Graveler | 3 | 5 |
| Gengar| 1 | 1 |
| Marowak | 3 | 2 |

Dans le script principal, importer la table de toto dans la variable `table_toto` (juste après avoir importé la table des pokémons.

Ecrire une fonction `joint(table_pok, table_tot)` qui retourne la table issue de la jointure entre la table des pokémons et la table de Toto en utilisant l'attribut commun `nom` (attention à la différence anglais/français ici).  
On conservera en sortie l'attribut `name` pour le nom.

```Python
>>> joint(table_poke, table_toto)
[{'#': '24', 'Name': 'Arbok', 'Type 1': 'Poison', 'Type 2': '', 'Total': '438', 'HP': '60', 'Attack': '85', 'Defense': '69', 'Sp. Atk': '65', 'Sp. Def': '79', 'Speed': '80', 'Generation': '1', 'Legendary': 'False', 'nombre': '2', 'classeur': '4'}, {'#': '56', 'Name': 'Mankey', 'Type 1': 'Fighting', 'Type 2': '', 'Total': '305', 'HP': '40', 'Attack': '80', 'Defense': '35', 'Sp. Atk': '35', 'Sp. Def': '45', 'Speed': '70', 'Generation': '1', 'Legendary': 'False', 'nombre': '1', 'classeur': '3'}, {'#': '75', 'Name': 'Graveler', 'Type 1': 'Rock', 'Type 2': 'Ground', 'Total': '390', 'HP': '55', 'Attack': '95', 'Defense': '115', 'Sp. Atk': '45', 'Sp. Def': '45', 'Speed': '35', 'Generation': '1', 'Legendary': 'False', 'nombre': '3', 'classeur': '5'}, {'#': '94', 'Name': 'Gengar', 'Type 1': 'Ghost', 'Type 2': 'Poison', 'Total': '500', 'HP': '60', 'Attack': '65', 'Defense': '60', 'Sp. Atk': '130', 'Sp. Def': '75', 'Speed': '110', 'Generation': '1', 'Legendary': 'False', 'nombre': '1', 'classeur': '1'}, {'#': '105', 'Name': 'Marowak', 'Type 1': 'Ground', 'Type 2': '', 'Total': '425', 'HP': '60', 'Attack': '80', 'Defense': '110', 'Sp. Atk': '50', 'Sp. Def': '80', 'Speed': '45', 'Generation': '1', 'Legendary': 'False', 'nombre': '3', 'classeur': '2'}]
```
