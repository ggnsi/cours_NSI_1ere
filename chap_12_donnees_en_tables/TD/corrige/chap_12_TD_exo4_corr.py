# Exercice 4
import csv

## code permettant d'importe la table

def import_csv(nom_fichier, separateur):
    '''Retourne sous forme de liste de dictionnaire la table de données
    sauvegardée dans le fichier csv nom_fichier avec le séparateur indiqué
    
    param nom_fichier : (string)
    param separateur : (string)
    valeur retournée : (list of dict(string : string))
    '''
   
    table_sortie = []
    with open(nom_fichier, 'r', encoding = 'utf-8') as fichier:
        reader = csv.DictReader(fichier, delimiter = separateur)
        for enregistrement in reader:
            table_sortie.append(dict(enregistrement))
    
    # ou
    # table_sortie = [dict(enregistrement) for enregistrement in reader]
    return table_sortie


def join(enr1, enr2):
    
    return { 'nom' : enr1['nom'], 'prenom' : enr1['prenom'], 'age' : enr1['age'], 'taille' : enr1['taille'],
             'profession' : enr2['profession'], 'voiture' : enr2['voiture']}


table_td = import_csv('personnes_td.csv', ',')
table_td_supp = import_csv('personnes_td_supp.csv', ',')

table_joint = []
for enr1 in table_td:
    for enr2 in table_td_supp:
        if enr1['nom'] == enr2['nom']:
            table_joint.append(join(enr1, enr2))
            continue

# ou
table_joint_bis = [join(enr1, enr2) for enr1 in table_td for enr2 in table_td_supp if enr1['nom'] == enr2['nom']]

# juste pour afficher la jointure pour vérification
for ligne in table_joint:
    print(ligne)