# Exercice 3
import csv

## code permettant d'importe la table

def import_csv(nom_fichier, separateur):
    '''Retourne sous forme de liste de dictionnaire la table de données
    sauvegardée dans le fichier csv nom_fichier avec le séparateur indiqué
    
    param nom_fichier : (string)
    param separateur : (string)
    valeur retournée : (list of dict(string : string))
    '''
   
    table_sortie = []
    with open(nom_fichier, 'r', encoding = 'utf-8') as fichier:
        reader = csv.DictReader(fichier, delimiter = separateur)
        for enregistrement in reader:
            table_sortie.append(dict(enregistrement))
    
    # ou
    # table_sortie = [dict(enregistrement) for enregistrement in reader]
    return table_sortie

def cle_tri_prenom(ligne):
    return ligne['prenom']

def tri_prenoms(table):
    
    return sorted(table, key = cle_tri_prenom, reverse = False)

def cle_tri_nom(ligne):
    return ligne['nom']

def tri_noms(table):

    return sorted(table, key = cle_tri_nom, reverse = True)

def cle_tri_taille(ligne):
    return int(ligne['taille'])

def plus_grands(table):
    
    table_triee = sorted(table, key = cle_tri_taille, reverse = True)
    
    liste_sortie = []
    for k in range(3):
        liste_sortie.append(table_triee[k]['nom'])
    return liste_sortie

table_td = import_csv('personnes_td.csv', ',')

