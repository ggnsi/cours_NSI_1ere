import csv

# fonction qui permet l'importation
def importcsv(nom_fichier, separateur):
    """Importe la table du fichier csv nom_fichier sous forme de dictionnaire

    param nomfichier : (string)
    param separateur : (string) : séparateur utilisé dans le fichier csv
    valeur retournée : (dict(string : string))
    """
    
    with open(nom_fichier, "r", encoding = 'utf-8') as csv_file:
        reader = csv.DictReader(csv_file, delimiter = separateur)
        
        table = [dict(enregistrement) for enregistrement in reader]
    return table

def affiche_table(table):
    for elt in table:
        print(elt)


table_legumes = importcsv('legumes.csv', ';')
table_fruits = importcsv('fruits.csv', ';')

table_sortie = []
for legume in table_legumes:
    legume['Nature'] = 'legume'
    table_sortie.append(legume)
    
for fruit in table_fruits:
    fruit['Nature'] = 'fruit'
    table_sortie.append(fruit)

#juste pour vérifier sur deux exemples que l'attribut Nature a bien été ajouté
print(table_sortie[0])
print(table_sortie[29])

# affichage de la tablea complète
affiche_table(table_sortie)

