import csv

# fonction qui permet l'importation
def importcsv(nom_fichier, separateur):
    """Importe la table du fichier csv nom_fichier sous forme de dictionnaire

    param nomfichier : (string)
    param separateur : (string) : séparateur utilisé dans le fichier csv
    valeur retournée : (dict(string : string))
    """
    
    with open(nom_fichier, 'r', encoding = 'utf-8') as fichier:
        reader = csv.DictReader(fichier, delimiter = separateur)
        
        table = [dict(enregistrement) for enregistrement in reader]
    return table

#juste pour vérifier facilement le résultat
def affiche_table(table):
    for elt in table:
        print(elt)

# je définis ici une fonction fusion qui me retourne le dictionnaire
# correspondant à la fusion des deux enregistrements
# cela clarifie le code de jointure en dessous
def fusion(dict_pop, dict_nom):
    
    dict_fusion = { 'code' : dict_pop['code'], 'nom' : dict_nom['nom'],
                    'population': dict_pop['population'],
                    'superficie' : dict_pop['superficie']}
    
    return dict_fusion

#importation des tables
table_population = importcsv('population_hdf.csv', separateur = ';')
table_noms = importcsv('departements.csv', separateur = ';')

table_joint = []

#on parcourt les enregistrements de la table population
for enr1 in table_population:
    # on parcourt les enregistrements de la table noms à la recherche du code commun (si il existe)
    for enr2 in table_noms:
        if enr1['code'] == enr2['code']:
            table_joint.append(fusion(enr1, enr2))
            break #pas la peine de continuer à tout parcourir

affiche_table(table_joint)

## autre méthode possible avec les listes en compréhension (mais
## on parcourt toute la deuxième table à chaque fois

# table_joint = [fusion(enr1,enr2) for enr1 in table_population   for enr2 in table_noms    if enr1['code']==enr2['code']