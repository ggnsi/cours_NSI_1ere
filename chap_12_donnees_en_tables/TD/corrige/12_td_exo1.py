# Exercice 1

## code permettant d'importe la table

import csv

def import_csv(nom_fichier, separateur):
    table = []

    with open(nom_fichier, "r", encoding='utf-8') as csv_file:
        it_dictreader = csv.DictReader(csv_file, delimiter = separateur) 
        for enregistrement in it_dictreader:
            table.append(dict(enregistrement))
            
    return table

table_td = import_csv('personnes_td.csv', ',')

#2 : len(table)

#3 : len(table[0])


def affiche_table(table):
    """affiche les uns en dessous des autres les éléments de table

    param table : (list)
    valeur retournée : aucune
    """
    
    for enregistrement in table:
        print(enregistrement)

def l_attributs(table):
    
    enregistrement = table[0]
    l_sortie = []
    for cle in enregistrement.keys():
        l_sortie.append(cle)
    return l_sortie

    # l_sortie = [cle for cle in enregistrement.keys()]

def proj_1(table):
    
    l_noms = []
    for enregistrement in table:
        l_noms.append(enregistrement['nom'])
    return l_noms
    
    
def hab_loos_1(table):
    l_sortie = []
    for enregistrement in table:
        if enregistrement['ville'] == 'Loos':
            l_sortie.append(enregistrement)
    return l_sortie

def hab_loos_2(table):
    
    l_sortie = [enregistrement for enregistrement in table if enregistrement['ville'] == 'Loos']
    return l_sortie

def hab_loos_3(table):
    l_noms = []
    for enregistrement in table:
        if enregistrement['ville'] == 'Loos':
            l_noms.append(enregistrement['nom'])
    return l_noms

def hab_loos_4(table):
    

def hab_age(table):
    pass

def rech_1(table):
    pass

def rech_2(table):
    pass

def est_valide(elt):
    """Prédicat qui indique si l'enregistrement donné par elt est
    une personne habitant Loos et dont la taille est supérieure ou
    égale à 170 cm
    
    param elt : (dict(string:string))
    valeur retournée : Booléan
    """
    pass

def rech_3(table):
    pass





