# Exercice 2
import csv

## code permettant d'importe la table

def import_csv(nom_fichier, separateur):
    '''Retourne sous forme de liste de dictionnaire la table de données
    sauvegardée dans le fichier csv nom_fichier avec le séparateur indiqué
    
    param nom_fichier : (string)
    param separateur : (string)
    valeur retournée : (list of dict(string : string))
    '''
   
    table_sortie = []
    with open(nom_fichier, 'r', encoding = 'utf-8') as fichier:
        reader = csv.DictReader(fichier, delimiter = separateur)
        for enregistrement in reader:
            table_sortie.append(dict(enregistrement))
    
    # ou
    # table_sortie = [dict(enregistrement) for enregistrement in reader]
    return table_sortie

def nb_1(table):
    
    compteur = 0
    for ligne in table:
        if ligne['ville'] == 'Loos':
            compteur += 1
    return compteur

def nb_2(table):
    
    compteur = 0
    for ligne in table:
        if ligne['ville'] == 'Loos' or ligne['ville'] == 'Lille':
            compteur += 1
    return compteur

def t_moy(table):
    
    somme_tailles = 0
    for ligne in table:
        somme_tailles += int(ligne['taille'])
    
    nb_hab = len(table)
    
    return somme_tailles / nb_hab




table_td = import_csv('personnes_td.csv', ',')

