# Exercice 1

# réponse question 2 : len(table_td)
# réponse question 3 : len(table_td[0]) (on prend un enregistrement au hasard : le premier !)


import csv

## code permettant d'importe la table

def import_csv(nom_fichier, separateur):
    '''Retourne sous forme de liste de dictionnaire la table de données
    sauvegardée dans le fichier csv nom_fichier avec le séparateur indiqué
    
    param nom_fichier : (string)
    param separateur : (string)
    valeur retournée : (list of dict(string : string))
    '''
   
    table_sortie = []
    with open(nom_fichier, 'r', encoding = 'utf-8') as fichier:
        reader = csv.DictReader(fichier, delimiter = separateur)
        for enregistrement in reader:
            table_sortie.append(dict(enregistrement))
    
    # ou
    # table_sortie = [dict(enregistrement) for enregistrement in reader]
    return table_sortie



def affiche_table(table):
    """affiche les uns en dessous des autres les éléments de table

    param table : (list)
    valeur retournée : aucune
    """
    
    for ligne in table:
        print(ligne)

def l_attributs(table):
    '''retourne la liste des attributs de la table

    param table : (list of dict(string : string))
    valeur retournée : (list of string)
    '''
    
    enregistrement = table[0] # on prend un enregistrement
    
    return list(enregistrement.keys()) # les attributs sont les clés des enregistrements

def proj_1(table):
    '''retourne la liste des valeurs de l'attribut 'nom' de la table

    param table : (list of dict(string : string))
    valeur retournée : (list of string)
    '''
    
    l_sortie = []
    for ligne in table:
        l_sortie.append(ligne['nom'])

    # ou en compréhension
    # l_sortie = [ligne['nom'] for ligne in table]
    
    return l_sortie

def hab_loos_1(table):
    """retourne la liste des enregistrements correspondant aux
    personnes habitant de Loos
    
    param table : (list of dict)
    valeur retournée : (list of dict)
    """
    
    l_sortie = []
    for ligne in table:
        if ligne['ville'] == 'Loos':
            l_sortie.append(ligne)
    
    return l_sortie
    

def hab_loos_2(table):
    """retourne la liste des enregistrements correspondant aux
    personnes habitant Loos
    
    param table : (list of dict)
    valeur retournée : (list of dict)
    """
    
    return [ligne for ligne in table if ligne['ville'] == 'Loos']

def hab_loos_3(table):
    
    l_sortie = []
    for ligne in table:
        if ligne['ville'] == 'Loos':
            l_sortie.append(ligne['nom'])
    
    # ou en compréhension
    # [ligne['nom'] for ligne in table if ligne['ville'] == 'Loos']
    
    return l_sortie

def hab_loos_4(table):
    
    l_sortie = []
    for ligne in table:
        if ligne['ville'] == 'Loos':
            l_sortie.append((ligne['nom'], ligne['prenom']))
    
    # ou en compréhension
    # [(ligne['nom'], ligne['prenom']) for ligne in table if ligne['ville'] == 'Loos']
    
    return l_sortie

def hab_age(table):
    
    l_sortie = []
    for ligne in table:
        if int(ligne['age']) >= 45:
            l_sortie.append(ligne['prenom'])
            
    # ou en compréhension
    # [ligne['prenom'] for ligne in table if ligne['age'] >= 45]
    
    return l_sortie

def rech_1(table):
    l_sortie = []
    for ligne in table:
        if ligne['prenom'][0] == 'M':
            l_sortie.append(ligne['nom'])
            
    # ou en compréhension
    # [ligne['nom'] for ligne in table if ligne['nom'][0] == 'M']
    
    return l_sortie

def rech_2(table):
    l_sortie = []
    for ligne in table:
        if (ligne['ville'] == 'Loos') and (int(ligne['taille']) >= 170):
            l_sortie.append(ligne)
    
    # ou en compréhension
    # [ligne for ligne in table if (ligne['ville'] == 'Loos') and (ligne['taille'] >= 170)]
    
    return l_sortie

def rech_3(table):
    l_sortie = []
    for ligne in table:
        if (ligne['ville'] == 'Loos' or ligne['ville'] == 'Lille') and (int(ligne['taille']) >= 170):
            l_sortie.append(ligne['nom'])
    
    # ou en compréhension
    # [ligne for ligne in table if (ligne['ville'] == 'Loos' or ligne['ville'] == 'Lille') and (int(ligne['taille']) >= 170)]
    
    return l_sortie

def est_valide(ligne):
    
    return (ligne['ville'] == 'Loos' or ligne['ville'] == 'Lille') and (int(ligne['taille']) >= 170)

def rech_4(table):
    
    return [ligne['nom'] for ligne in table if est_valide(ligne)]


table_td = import_csv('personnes_td.csv', ',')

