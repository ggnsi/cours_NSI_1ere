import csv

def importcsv(nom_fichier, separateur):
    
    with open(nom_fichier, 'r', encoding = 'utf-8') as fichier:
        
        d_read = csv.DictReader(fichier, delimiter = separateur)
        
        table = [dict(entree) for entree in d_read]
    
    return table

table_poke = importcsv('pokemon.csv', ',')
table_toto = importcsv('pokemon_toto.csv', ',')

def l_attrib(table):
    
    return list(table[0].keys())

    # l = []
    # entree = table[0]
    # for attribut in entree.keys():
    #   l.append(attribut)
    # return l
    
def aff_liste(liste):
    
    for elt in liste:
        print(elt)

def l_type1(table):
    
    l_val = []
    for entree in table:
        if entree['Type 1'] not in l_val:
            l_val.append(entree['Type 1'])
    return l_val

def l_type1plus(table, attribut):
    
    l_val = []
    for entree in table:
        if entree[attribut] not in l_val:
            l_val.append(entree[attribut])
    return l_val

def l_attack(table, val_min):
    
    return [(entree['Name'], entree['Type 2']) for entree in table if int(entree['Attack']) >= val_min]

    # l = []
    # for entree in table:
    #    if int(entree['Attack']) >= val_min:
    #       l.append((entree['Name'], entree['Type 2']))
    # return l

def l_attack_affiche(table, val_min):
    
    result = [(entree['Name'], entree['Type 2']) for entree in table if int(entree['Attack']) >= val_min]
    aff_liste(result)

def moy_hp_gen(table, num_generation):
    
    total = 0
    nb_pok = 0
    for entree in table:
        if int(entree['Generation']) == num_generation:
            total += int(entree['HP'])
            nb_pok +=1
    
    return total/nb_pok

# il existe beaucoup de moyens de réaliser la fonction suivante.
# cela dépend de la manière dont on décide de stocker et associer les informations nécéssaires,
# à savoir : la génération, l'effectif de cette génération et le total des points de vie.
# on pourrait aussi commencer par trier la table suivant la génération.

# il y a 6 générations de pokémon.
# on peut par exemple stocker l'effectif dans une liste où l'indice correspondra à la génération
# et le total des points de vie dans une autre de la même manière.
# les éléments d'indice 0 ne serviront pas
def moy_hp_tot(table):
    l_eff_generation = [0] * 7
    l_total_hp = [0] * 7
    for pokemon in table:
        gen = int(pokemon['Generation'])
        l_eff_generation[gen] +=1
        l_total_hp[gen] += int(pokemon['HP'])
    for num_gen in range(1, 7):
        print('La génération', num_gen, 'a une moyenne de HP de', l_total_hp[num_gen]/l_eff_generation[num_gen])

# ci-dessous, les informations sont stockées dans un dictionnaire de dictionnaire.
# Les clés du dictionnaire principal sont les numéros de génération (de 1 à 6) et chaque valeur est un dictionnaire de clés 'effectif', 'total_HP'

def moy_hp_tot_2(table):
    
    dico = { k: {'effectif' : 0, 'total_HP' : 0} for k in range(1, 7) }
    for pokemon in table:
        gen = int(pokemon['Generation'])
        HP =  int(pokemon['HP'])
        dico[gen]['effectif'] += 1
        dico[gen]['total_HP'] += HP
    
    for (gen, dico_gen) in dico.items():
        print('La génération', gen, 'a une moyenne de HP de', dico_gen['total_HP']/dico_gen['effectif'])

def project(table, type_2):
    
    l_sortie = []
    for pokemon in table:
        if pokemon['Type 2'] == type_2:
            l_sortie.append(pokemon)
    return l_sortie

    # return [pokemon for pokemon in table if pokemon['type_2'] == type_2]

def jointure(elt1, elt2):
    
    dico = elt1.copy() # pour ne pas modifier elt1 en place
    dico['nombre'] = elt2['nombre']
    dico['classeur'] = elt2['classeur']
    return dico

def joint(table_pok, table_tot):
    
    table_joint = []
    for pok1 in table_tot:
        for pok2 in table_pok:
            if pok2['Name'] == pok1['nom']:
                table_joint.append(jointure(pok2, pok1))
                continue
    return table_joint

table_j = joint(table_poke, table_toto)