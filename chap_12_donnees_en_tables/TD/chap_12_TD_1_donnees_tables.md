# TD 1 - données en table : recherches, agrégation, tris

Télécharger le fichier [personnes_td.csv](./personnes_td.csv) et le sauvegarder dans le répertoire où sera situé votre script Python.

### Exercice 1 : Recherche dans une table

On utilisera la trame définie dans le fichier [chap_12_TD_exo1.py](./chap_12_TD_exo1.py)

1. En vous inspirant du code proposé dans le cours, créer une fonction `import_csv(nom_fichier, separateur)` qui retourne sous sous forme d'une **liste de dictionnaires** la table de données sauvegardée dans le fichier `nom_fichier` avec le `séparateur` précisé (que l'on pourra donc faire varier en fonction du fichier CSV utilisé). Les paramètres `nom_fichier` et `séparateur` seront de stype `string`.

   Utiliser cette fonction dans le script principal pour importer la table sauvegardée dans le fichier `personnes_td.csv` (séparateur : la virgule) et la stocker dans la variable `table_td`. Exécuter le script.  
   Vous avez ainsi dans le script principal une ligne à mettre de la forme `table_td = ...`
  
2. Quelle instruction entrer dans la console afin de connaître le nombre d'enregistrements de la table ?

3. Quelle instruction entrer dans la console afin de connaître le nombre de descripteurs utilisés (le nombre de colonnes de la table) ?

4. Compléter la fonction `affiche_table(table)` afin qu'elle affiche les uns en dessous des autres les éléments de `table` (qui seront donc ici **les dictionnaires** correspondant à chaque enregistrement)

5. Créer la fonction `l_attributs(table)` qui retourne la liste des attributs utilisés dans cette table.

  ```Python
  >>> l_attributs(table_td)
  ['nom', 'prenom', 'age', 'taille', 'ville']
  ```

6. Créer la fonction `proj_1(table)` qui retourne la liste de tous les noms des personnes de la table.

    On pourra essayer de coder cette fonction de deux manières différentes dont l'une utilise une liste en compréhension.

  ```Python
  >>> proj_1(table_td)
  ['Durand', 'Dupont', 'Terta', 'Kapri', 'Lenard', 'Lenard', 'Herpan', 'Mastic', 'Pouyeau', 'Arox', 'Follin', 'Fourcade', 'Mbappe', 'Fonte', 'Rinner', 'Fritoi']
  ```

7. Créer la fonction `hab_loos_1(table)` afin qu'elle retourne la liste des **enregistrements** (donc des dictionnaires ici) des personnes de la table habitant à `Loos`.
8. Créer la fonction `hab_loos_2(table)` qui réalise la même chose que `hab_loos_1` mais en utilisant une liste en compréhension.

> **Dans la suite, on pourra régulièrement essayer de raccourcir son code en écrivant une version utilisant une liste en compréhension.**

   
9. Créer la fonction `hab_loos_3(table)` qui retourne la **liste des noms** des personnes de la table qui habitent Loos.

  ```Python
  >>> hab_loos_3(table_td)
  ['Terta', 'Lenard', 'Pouyeau', 'Arox']
  ```
10. Créer la fonction `hab_loos_4(table)` qui retourne la **liste des tuples** `(nom, prenom)` des personnes de la table qui habitent Loos.

  ```Python
  >>> hab_loos_4(table_td)
  [('Terta', 'Henry'), ('Lenard', 'Georges'), ('Pouyeau', 'Maxime'), ('Arox', 'Gilles')]
  ```

11. Créer la fonction `hab_age(table)` qui retourne la **liste des prénoms** des personnes dont l'âge est supérieur ou égal à 45 ans.

  ```Python
  >>> hab_age(table_td)
  ['Christophe', 'Leon', 'Brice', 'Gilles']
  ```
12. Créer la fonction `rech_1(table)` qui retourne la **liste des noms** des personnes dont la première lettre **du prénom** est un `M`.

  ```Python
  >>> rech_1(table_td)
  ['Lenard', 'Pouyeau', 'Fourcade', 'Fritoi']
  ```
13. Créer la fonction `rech_2(table)` qui retourne la **liste des noms** pour lesquels la ville est `'Loos'` et la taille supérieure ou égale à 170 cm.

  ```Python
  >>> rech_2(table_td)
  ['Terta', 'Arox']
  ```
14. Créer la fonction `rech_3(table)` qui retourne la **liste des noms** pour lesquels la ville est `'Loos'` ou `'Lille'` et dont la taille supérieure ou égale à 170 cm.

  ```Python
  >>> rech_3(table_td)
  ['Terta', 'Herpan', 'Arox', 'Mbappe', 'Fonte', 'Rinner', 'Fritoi']
  ```

__Pour aller plus loin (si vous avez fini le reste)__  

15. Compléter le _prédicat_ `est_valide(ligne)` qui renvoie `True` si l'enregistrement `ligne` vérifie les conditions de la question précédente.
16. Créer la fonction `rech_4(table)` qui réalise la même chose que la fonction `rech_2` mais en utilisant le prédicat `est_valide`.

### Exercice 2 : Agrégation de résultats

> Les opérations d'_agrégation_ consistent à combiner les données de plusieurs lignes afin de produire un résultat (par exemple statistique) sur les données.

On changera de fichier de script afin d'améliorer la lisibilité.  
Donc commencer par faire un copier/coller du code d'importation du fichier csv.

1. Créer une fonction `nb_1(table)` qui renvoie le nombre de personnes de la table habitant Loos.

  ```Python
  >>> nb_1(table_td)
  4
  ```
2. Créer une fonction `nb_2(table)` qui renvoie le nombre de personnes de la table habitant Loos ou Lille.

  ```Python
  >>> nb_2(table_td)
  11
  ```

3. Créer une fonction `t_moy(table)` qui renvoie la taille moyenne des personnes de la table.

  ```Python
  >>> t_moy(table_td)
  168.6875
  ```
>
> Revenir au [sommaire du cours](../README.md) pour lire **la première synthèse** puis lire la partie cours sur le [tri d'une table de données](../chap_12_1_donnees_table_tri.md)
>

### Exercice 3 : Trier les données en fonction d'une clé

On travaille toujours ici avec la table issue du fichier `personnes_td.csv` et on ne fera **PAS de tri en place**. On utilisera donc à chaque fois la fonction `sorted()` qui renvoie une nouvelle liste.

1. Créer une fonction `tri_prenoms(table)` qui retourne la `table` triée par ordre alphabétique croissant des prénoms.
2. Créer une fonction `tri_noms(table)` qui retourne la `table` triée par ordre alphabétique décroissant des noms.
3. Créer une fonction `plus_grands(table)` qui retourne la liste des _noms_ des 3 plus grandes personnes de la table (on suppose ici que la table comporte au moins 3 enregistrements et que 3 personnes seront bien plus grandes que les autres (pas d'égalités)).

  ```Python
  >>> plus_grands(table_td)
  ['Rinner', 'Lenard', 'Fritoi']
  ```

### Exercice 4 : Jointure

> **Avant cet exercice, effectuer [l'activité d'introduction sur la réunion-jointure](./chap_12_donnees_tables_intro_reunion_jointure.md)  puis lire [la partie cours correspondante](./chap_12_1_donnees_reunion_jointure.md) (voir le fichier [README](./README.md))**

Effectuer la jointure des tables situées dans les fichiers [personnes_td.csv](./personnes_td.csv) et [personnes_td_supp.csv](./personnes_td_supp.csv) en utilisant l'attribut commun `nom`.

Les attribus de la première table sont `nom`, `prenom`, `age`, `taille`, `ville` et ceux de la deuxième sont `nom`, `profession`, `voiture`.

Afficher la table résultante (une ligne par enregistrement).

  ```Python
{'nom': 'Dupont', 'prenom': 'Christophe', 'age': '51', 'taille': '165', 'profession': 'Enseignant', 'voiture': ' Citroën'}
{'nom': 'Lenard', 'prenom': 'Michel', 'age': '17', 'taille': '190', 'profession': 'garagiste', 'voiture': ' Porsche'}
{'nom': 'Mastic', 'prenom': 'Brice', 'age': '61', 'taille': '155', 'profession': 'commercial', 'voiture': ' Renault'}
{'nom': 'Pouyeau', 'prenom': 'Maxime', 'age': '11', 'taille': '130', 'profession': 'musicien', 'voiture': ' Dacia'}
{'nom': 'Fourcade', 'prenom': 'Martin', 'age': '36', 'taille': '178', 'profession': 'sportif', 'voiture': ' bmw'}

  ```

## Exercices complémentaires

### Exercice : conversion des valeurs de certains attributs

Créer un script qui :
* importe sous forme de liste de dictionnaires la table sauvergardée dans le fichier [personnes_td.csv](./personnes_td.csv) (séparateur : la virgule)
* convertit en place toutes les valeurs des attributs `age` et `taille` en type `int`.


### Exercice : test de cohérence de la table

Après conversion des valeurs des attributs (voir juste avant), une première opération peut consister en la vérification de la cohérence des données de la table.

Ainsi, on peut raisonnablement considérer :
* qu'une personne a un âge strictement positif et inférieur à 125 ans ;
* qu'une personne a une taille comprise entre 50 et 230 cm.



__a créer__
Test si valeurs sont cohérentes et ne retourner que celles ok => créer fichier avec des tailles négatives, des âges supérieurs à 130.


