# Chapitre traitement des données en tables

> Dans ce chapitre, la fonctionnalité d'**inspecteur d'objet** présente dans Thonny pourra vous être utile (à activer dans le menu Affichage, tout comme l'affichage des variables).
> En sélectionnant une variable, elle vous permet d'en inspecter plus facilement le contenu).

* [Première partie de cours](./chap_12_1_donnees_table_cours.md) : Définition d'une table, importation d'un fichier csv
* Exercices 1 et 2 du [TD 1](./TD/chap_12_TD_1_donnees_tables.md) à faire
* [Synthèse sur la recherche et l'agrégation des résultats](./chap_12_1_donnees_table_recherche.md)
* [Partie de cours sur le tri des données](./chap_12_1_donnees_table_tri.md) à lire
* Exerice 3 du [TD 1](./TD/chap_12_TD_1_donnees_tables.md) à faire
* [Activité d'introduction](./chap_12_donnees_tables_intro_reunion_jointure.md) sur les réunions/jointures à faire ([corrections ici](./TD/corrige/activite_union_jointure/))
* [Partie cours sur les réunions/jointures](./chap_12_1_donnees_reunion_jointure.md)
* Exercice 4 du [TD 1](./TD/chap_12_TD_1_donnees_tables.md) à faire
* Fin du [TD 1](./TD/chap_12_TD_1_donnees_tables.md) à faire

* pour s'entraîner : [TD 2](./TD/chap_12_TD_2_donnees_tables.md) (qui reprend les notions à maîtriser)

---
* [des corrigés des exercices (TD1 et TD2)](./TD/corrige)