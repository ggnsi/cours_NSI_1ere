# VI Fusion de tables : réunion et jointure

Lorsqu'on travaille sur des tables de données, on peut se retrouver avec plusieurs tables qu'on aimerait rassembler afin d'effectuer des opérations de filtration , tri,etc. ...
Il existe plusieurs façons de "fusionner" des données en tables. Selon l'opération que l'on souhaite effectuer, des précautions particulières sont à prendre pour ne pas introduire d'incohérence dans les données.

## Réunion de tables
Une première opération naturelle est de réunir dans une même table les données de deux (ou plusieurs)autres tables qui ont les même structures (même attributs, c'est à dire mêmes noms des colonnes).
>📢 Afin de réunir deux tables ayant les mêmes attributs, on utilise la concaténation `+`

Afin de réunir les deux tables suivantes dans une troisième, il va falloir réaliser différentes étapes :

1. Importer les données `table_1` et `table_2` à partir des fichiers csv ;
2. fusionner les deux dans une `table 3` par l'opération `table3 = table1 + table2` ;


## Opération de jointure

On peut aller plus loin avec des tables ayant des attributs différents, mais **au moins un attribut commun** en créant une nouvelle table constituée de la fusion des enregistrements des deux tables ayant la même valeur pour l'attribut commun.

_Exemple_ : la jointure des tables suivantes

|code |	population| superficie |
|:--:|:--:|:--:|
|02| 	534490| 7362 |
|59| 	2604361| 5743 |
|60| 	824503| 5860 |
|62| 	1468018| 6671 |
|80| 	572443| 6170 |

et

|code |	nom|
|:--:|:--:|
|01 |	Ain|
|02 |	Aisne|
|03 |	Allier|
|... |	...|

sera la table 

|code |	nom |	population| superficie |
|:--:|:--:|:--:|:--:|
|02| 	Aisne 	|534490|7362 |
|59|	Nord 	|2604361|5743 |
|60|	Oise 	|824503|5860 |
|62| 	Pas-de-Calais| 	1468018|6671 |
|80|	Somme 	|572443|6170 |

__Principe de l'opération de jointure__ :

1. **Importer les données** dans **table1** et **table2**
2. **Créer une fonction** fusion(enr1,enr2) qui va créer un nouveau dictionnaire représentant la ligne de la table fusionnée recherchée.

puis, deux méthodes sont possibles (même logique derrière).

__Méthode 1__

3. utiliser une double boucle pour :
  * parcourir tous les enregistrements de la _table1_ et pour chacun d'entre eux :
    * parcourir tous les enregistrements de la _table2_ à la recherche de celui ayant même valeur pour le critère commun
    * lorsqu'il est rencontré, fusionner les deux enregistrements et l'ajouter à la table de sortie


```python
def fusion(dict_pop, dict_nom):
    
    dict_fusion = { 'code' : dict_pop['code'], 'nom' : dict_nom['nom'],
                    'population': dict_pop['population'],
                    'superficie' : dict_pop['superficie']}
    
    return dict_fusion

table_joint = []

#on parcourt les enregistrements de la table population
for enr1 in table_population:
    # on parcourt les enregistrements de la table noms à la recherche du code commun (si il existe)
    for enr2 in table_noms:
        if enr1['code'] == enr2['code']:
            table_joint.append(fusion(enr1, enr2))
            break #pas la peine de continuer parcourir toute la deuxième table : on a trouvé
```

__Méthode 2__ :

On utilise une double compréhension 


```python
# Etape 3 b : double compréhension
table_joint= [fusion(enr1,enr2) for enr1 in table_population   for enr2 in table_noms    if enr1['code']==enr2['code']
```

> **A retenir**  
    La **fusion** de tables de données peut se faire de plusieurs façons. Si les tables ont exactement **les mêmes attributs (colonnes)**, alors on peut concaténer ces tables pour obtenir leur **réunion**. Si elles ont **au moins un attribut commun**, on peut effectuer une opération de **jointure** qui mettra en relation les données égales selon ces colonnes communes.

sources:
* Le petit Python Aide mémoire pour Python 3 Richard Gomez Ellipses
* NSI 30 leçons T Balabonski S Conchon JC Filliâtre K Nguyen
* www.math93.com / J. Courtois, F. Duffaud
* http://pascal.ortiz.free.fr/contents/python/tri/tri.html
* nsi.ostralo.net (M. Willm)
