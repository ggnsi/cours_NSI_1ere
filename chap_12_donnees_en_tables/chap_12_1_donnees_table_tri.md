# V : Tri d'une table de données
---

## Fonction sorted(liste) et méthode list.sort()

> En Python, la fonction `sorted(liste)` retourne une **nouvelle liste** correpondant au tri de `liste` avec l'ordre usuel.  
La liste initiale  n'est pas modifiée comme on peut le voir ci-dessous :


```python
liste = [51,87,47,87,12]
liste_trie = sorted(liste)
print("le tableau est : ",liste , "; le tableau trié est : ", liste_trie)
```

    le tableau est :  [51, 87, 47, 87, 12] ; le tableau trié est :  [12, 47, 51, 87, 87]
    

> `liste.sort()` trie __en place__ la liste `liste`.   
La liste initiale __est__ modifiée.


```python
liste = [51,87,47,87,12]
liste.sort()
print(liste)
```

    [12, 47, 51, 87, 87]
    

## Tri d'une table selon une clé

### Problème : _Python ne sait pas comparer deux objets qui sont des dictionnaires_. 


```python
table = [{'nom': 'Durand', 'prenom': 'Jean', 'age': '32', 'taille': '150', 'ville': 'Lille'},
         {'nom': 'Dupont', 'prenom': 'Christophe', 'age': '51', 'taille': '165', 'ville': 'Santes'},
         {'nom': 'Terta', 'prenom': 'Henry', 'age': '37', 'taille': '174', 'ville': 'Loos'},
         {'nom': 'Kapri', 'prenom': 'Leon', 'age': '45', 'taille': '145', 'ville': 'Lille'},
         {'nom': 'Lenard', 'prenom': 'Michel', 'age': '17', 'taille': '190', 'ville': 'Wavrin'}]
sorted(table)
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    Cell In [1], line 6
          1 table = [{'nom': 'Durand', 'prenom': 'Jean', 'age': '32', 'taille': '150', 'ville': 'Lille'},
          2          {'nom': 'Dupont', 'prenom': 'Christophe', 'age': '51', 'taille': '165', 'ville': 'Santes'},
          3          {'nom': 'Terta', 'prenom': 'Henry', 'age': '37', 'taille': '174', 'ville': 'Loos'},
          4          {'nom': 'Kapri', 'prenom': 'Leon', 'age': '45', 'taille': '145', 'ville': 'Lille'},
          5          {'nom': 'Lenard', 'prenom': 'Michel', 'age': '17', 'taille': '190', 'ville': 'Wavrin'}]
    ----> 6 sorted(table)
    

    TypeError: '<' not supported between instances of 'dict' and 'dict'


### Solution : utilisation de l'argument `key` de `sorted()` ou de `sort()`

### Tri selon une clé
> La fonction `sorted` et la méthode `sort()` disposent d’un argument nommé `key` permettant de préciser selon quel critère une liste doit être triée.  
>
> syntaxe : `sorted(liste, key = fonc)` ou `liste.sort(key = fonc)`
>
> `fonc` est une **fonction** (j'insiste : une fonction !) qui doit :
>
>* avoir un unique paramètre de même type que les éléments de la liste,
>* renvoyer un valeur qui servira de critère de tri.
>
> `fonc` est appelée fonction de clé de tri.
>
> Si `liste = [elt1, elt2, elt3]`:
>
>* Python va évaluer :
>    * `fonc(elt1)` (appelons le résultat `result1`) ;
>    * `fonc(elt2)` (appelons le résultat `result2`) ;
>    * `fonc(elt3)` (appelons le résultat `result3`) ;
>  
>* Puis va trier `liste` de telle manière que les éléments soient ordonnés par résultats croissants.
>
>  Si par exemple on a : `result3 < result1 < result2`, la liste triée sera `[elt3, elt1, elt2]`
>
> **Important** : En cas d'égalité, les éléments sont placés dans l'ordre initial de la liste de départ.
>
> Cette technique est rapide car la fonction servant de clef de tri est appelée exactement une seule fois pour chaque enregistrement en entrée.

**Principe de fonctionnement sur un exemple** :

Soit une liste dont les éléments sont des chaînes de caractères :

```Python
liste_de_chaine = ['bernard', 'jules', 'bertrand', 'arthur', 'claude']
```

L'utilisation de la méthode `sort` entrainerait le tri en place de cette liste suivant l'odre lexycographique et on aurait alors :
```Python
>>> liste_de_chaine
['arthur', 'bernard', 'bertrand', 'claude', 'jules']
```

> On souhaite cependant trier la liste initiale en fonction de la longueur de chaque élément (les chaînes les plus courtes au début, celles les plus longues à la fin).
>
> 'jules' qui est de longueur 5 serait alors au début de la liste et 'bertrand' de longueur 8 serait alors à la fin.

Pour cela :

* On  définit une fonction de tri `fonc_cle_de_tri(chaine)` (puisque les éléments de la liste sont de type `string`) qui, pour la chaîne ce caractère qui lui est passée en paramètre retourne la longueur de cette chaîne.

```Python
def fonc_cle_de_tri(chaine):
    # Fonction qui prend une chaine en paramètre et renvoie son nombre de caractères
    return len(chaine)
```

* On demande le tri en place en utilisant cette clé de tri :

```Python
liste_de_chaine.sort(key=fonc_cle_de_tri) # Le trie se fait en fonction du nombre de caractères
# Après tri, liste_de_chaine contient ['jules', 'arthur', 'claude', 'bernard', 'bertrand']
```

* **Détail du fonctionnement** :

   * Python a évalué :
      * `fonc_cle_de_tri(`bernard`)` qui vaut 7 ;
      * `fonc_cle_de_tri(`jules`)` qui vaut 5 ;
      * `fonc_cle_de_tri(`bertrand`)` qui vaut 8 ;
      * `fonc_cle_de_tri(`arthur`)` qui vaut 6 ;
      * `fonc_cle_de_tri(`claude`)` qui vaut 6 ;
      
      

   * Puis a trié la liste de telle manière que les éléments soient triés par ordre de valeur renvoyée croissante.
     'arthur' et 'claude' ont touts les deux pour longueur 6 et ont été laissé l'ordre de la liste de départ ;


```python
# ----- Tri sans clé -----
liste_de_chaine = ['bernard', 'jules', 'bertrand', 'arthur', 'claude']
liste_de_chaine.sort() # Le tri se fait en fonction du critère alphabétique
                       # et tient compte des majuscules.
# liste_de_chaine contient ['arthur', 'bernard', 'bertrand', 'claude', 'jules']

# ----- Tri avec une clé donnée par une fonction -----
def fonc_cle_de_tri(chaine):
    # Fonction qui prend une chaine en paramètre et renvoie son nombre de caractères
    return len(chaine)

liste_de_chaine = ['bernard', 'jules', 'bertrand', 'arthur', 'claude']
liste_de_chaine.sort(key=fonc_cle_de_tri) # Le trie se fait en fonction du nombre de caractères

# Après tri, liste_de_chaine contient ['jules', 'arthur', 'claude', 'bernard', 'bertrand']
```

__Exemple 2 : tri d'une liste de listes avec comme clé de tri le deuxième élément de chaque liste.__

On a ici une liste de film, chacun étant représenté par une liste du type `[ nom _du_film, année_de_sortie, réalisateur]`.

On souhaite trier la liste par année de sortie croissante.

On va donc définir une fonction de tri qui, pour un film (une liste), retourne l'année de sortie, c'est à dire `film[1]`.

Lors du tri :

* pour chaque élément de la liste, python appelle la fonction de tri et récupère donc l'année de sortie du film ;
* puis la liste est triée de manière à ce que les valeurs retournées précédemment soient en ordre croissant.



```python
liste = [['La ligne verte', '2000', 'Frank Darabont'],
         ['La liste de Schindler', '1994', 'Steven Spielberg'],
         ['Le voyage de Chihiro', '2002', 'Hayao Miyazaki']]

def fonc_cle_de_tri(film):
    return int(film[1])

liste.sort(key=fonc_cle_de_tri)

# Après tri, liste contient 
        [['La liste de Schindler', '1994', 'Steven Spielberg'],
         ['La ligne verte', '2000', 'Frank Darabont'],
         ['Le voyage de Chihiro', '2002', 'Hayao Miyazaki']]
```

__Exemple : tri d'une table (liste de dictionnaires) avec pour clé de tri l'attribut `Année`__

Reprenons l'exemple précédent mais notre collection de film est maintenant représentée par une liste de dictionnaires.

Nous allons créer une fonction de tri qui, pour un film (un dictionnaire donc) va retourner la valeur de la clé `Année`.

La reste du procédé est le même que précédemment.


```python
liste = [{'Titre': 'La ligne verte', 'Année': '2000', 'Réalisateur': 'Frank Darabont'},
         {'Titre': 'La liste de Schindler', 'Année': '1994', 'Réalisateur': 'Steven Spielberg'},
         {'Titre': 'Le voyage de Chihiro', 'Année': '2002', 'Réalisateur': 'Hayao Miyazaki'}]
                
def fonc_cle_de_tri(film):
    return int(film["Année"])
                
liste.sort(key=fonc_cle_de_tri)
```

> __A retenir__ Pour résumer, on considère que table est un tableau de dictionnaires. Alors la fonction f associée à l'argument de tri **key** de la table sera de la forme :  


```python
#fonction de tri f associée à l'argument key
    def fonc(enregistrement):
        return enregistrement[attribut] # attribut = clé du dictionnaire
    
    table_triee = sorted(table, key = fonc)
    # ou table.sort(key = fonc) si on veut un tri en place
```

### Ordre croissant et décroissant
> Un troisième argument `reverse` (un booléen) permet de préciser si on veut le résultat par ordre croissant par défaut) ou décroissant (`reverse = True`).



```python
table_triee = sorted(table, key = fonc, reverse = True)
```

## Compléments 

### Ordre lexicographique  

#### Définition  

__Pour comparer des *chaînes*__, Python utilise l’ordre lexicographique qui est lui-même basé sur l’ordre des caractères. Il se trouve que chaque caractère unicode a un numéro d’ordre (une valeur unicode) et c’est ce numéro qui est utilisé pour classer les caractères.

Ainsi il faudra être vigilant sur l'ordre alphabétique dans les chaînes de caractères : **une majuscule (haut de casse) est  classée avant une minuscule (bas de casse)** : la **sensibilité à la casse** (traduction de l'anglais case sensitivity) est une notion informatique signifiant que dans un contexte donné, le sens d'une chaîne de caractères (par exemple un mot, un nom de variable, un nom de fichier, un code alphanumérique etc) dépend de la casse (capitale  ou bas de casse) des lettres qu'elle contient.Les caractères a, b, etc reçoivent les valeurs consécutives 97, 98, etc.  Les caractères majuscules A, B, etc reçoivent les valeurs 65, 66, etc. Donc, un caractère majuscule est toujours classé avant un caractère minuscule. 

__Pour comparer des listes (ou des tuples)__, Python va comparer les premiers éléments, puis les seconds, etc.

_Exemple d'utilisation_ : un commerce de vente de masques FFP2 souhaite remercier ses meilleurs clients par un cadeau. La propriétaire de la boutique voudrait classer les clients par points obtenus grâce aux achats. Ainsi elle va trier selon deux critères, d'abord selon le nombre de points puis, à valeur égale selon l'ordre alphabétique afin que les clients se retrouvent plus facilement dans la liste de lots. Elle réalise un tri selon l'ordre lexicographique.  
Cependant, les deux critères ne pourront être l'un en ordre croissant, l'autre en ordre décroissant : 

La table ci-dessous sera triée suivant le nombre de poins puis, à nombre de points égal suivant le nom


```python
Fichier_clients = [{"nom":"Dark Vador" ,"points" :125}, 
           {"nom" :"Spiderman","points" : 68} ,
           {"nom" :"Ironman","points" : 54} , 
           {"nom" : "Kylo Ren", "points": 25}, 
           {"nom" :"Arya Stark","points" : 125},
           {"nom" :"Fantomas", "points" :54},
           {"nom" :"Sacha Pyjamasque","points": 68},
           {"nom" : "Zorro" ,"points" :68 } ,
           {"nom" :"Jason Vendreditreize", "points" : 56},
           {"nom" : "Patrick Pelloux", "points":54} ]
def points_puis_nom(x):
    return (x["points"], x["nom"]) # on renvoie un tuple

liste_a_afficher = sorted(Fichier_clients, key =points_puis_nom,)
print(liste_a_afficher)
```

    [{'nom': 'Kylo Ren', 'points': 25}, {'nom': 'Fantomas', 'points': 54}, {'nom': 'Ironman', 'points': 54}, {'nom': 'Patrick Pelloux', 'points': 54}, {'nom': 'Jason Vendreditreize', 'points': 56}, {'nom': 'Sacha Pyjamasque', 'points': 68}, {'nom': 'Spiderman', 'points': 68}, {'nom': 'Zorro', 'points': 68}, {'nom': 'Arya Stark', 'points': 125}, {'nom': 'Dark Vador', 'points': 125}]
    

### Stabilité du tri en Python

Lorsque la fonction `sorted` ou la méthode `sort()` effectue un tri à l’aide d’une clé, les éléments ayant des clés de même valeur sont placés dans la liste triée dans le même ordre que dans la liste initiale. On dit que le tri effectué en Python est un **tri stable**.

sources:
* Le petit Python Aide mémoire pour Python 3 Richard Gomez Ellipses
* NSI 30 leçons T Balabonski S Conchon JC Filliâtre K Nguyen
* www.math93.com / J. Courtois, F. Duffaud
* http://pascal.ortiz.free.fr/contents/python/tri/tri.html
* nsi.ostralo.net (site M.Willm)

On remarque que cette fois ce module n'est pas sensible à la casse puisque "Prune" est placé après "prune"
