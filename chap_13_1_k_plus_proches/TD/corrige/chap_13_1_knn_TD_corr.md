# TD - Algorithme des k plus proches voisins

### Exercice 1

Ci-dessous, on donne un échantillon constitué de 11 éléments réparti selon 2 classes (les carrés en bleu et les triangles en rouge)

<center><img src="./img/knn_exo_fig_1.jpg" width="500"></center>

Appliquer l’algorithme KNN pour le nouvel élément représenté par un cercle vert afin de déterminer dans quelle catégorie il sera classé

1. pour k = 3

> Pour k = 3, l'élément serait classé rouge (2 triangles et un carré)

<img src="./img/knn_exo_fig_1_corr1.png" width="400">

2. pour k = 5

>Pour k = 5, l'élément serait classé bleu (3 bleus et 2 rouges)

<img src="./img/knn_exo_fig_1_corr2.png" width="400">

3. pour k = 9

>Pour k = 9, l'élément serait classé bleu (4 rouges et 5 bleus)

<img src="./img/knn_exo_fig_1_corr3.png" width="400">

### Exercice 2 : Jeu de rôle

(Source : David Landry)

Considérons un jeu de rôle médiéval avec des chevaliers et des fantassins.

Le jeu génère des soldats aléatoirement et il faut leur donner une spécialité d'après deux de leurs caractéristiques (Courage et force) et celles de soldats déjà catégorisés en chevalier ou fantassins.

Voici la représentation des soldats pris pour référence :

<center><img src="./img/Role1.png" width="500"></center>

Nous introduisons une cible de force = 5 et de courage = 12,5.

On choisit k = 4 et la distance est schématisée par un disque :

<center><img src="./img/Role2.png" width="500"></center>

1. Quels sont les k plus proches voisins de notre donnée cible ? 

> Avec k = 4, les 4 plus proches voisins sont 2 chevaliers et 2 fantassins.

2. Quel est le problème ? Comment le résoudre ?

> Il n'y a pas de catégorie prioritaire parmi les 4 PPV. Il est donc impossible de déterminer la catégorie de notre nouveau soldat. Il faut donc soit décider au départ d'une catégorie prioritaire en cas d'égalité, soit modifier k.

3. Donner deux valeurs de k permettant de décider de la catégorie de notre donnée cible ?

>k = 1, 3 par exemple (9 fonctionne aussi)

4. Donner pour ces deux valeur de k la catégorie de la donnée cible.

> k = 1 : chevalier k = 3 : chevalier

5. On choisit maintenant k = 9. Pour la distance, on décide que les valeurs de la force n'ont pas d'importance. La distance dépend donc uniquement du courage. On peut représenter graphiquement cette condition ainsi  :

   <center><img src="./img/Role3.png" width="500"></center>

   Quelle est la catégorie de notre donnée cible ?

   > Fantassin

6. On choisit k = 5. Pour la distance, on décide que les valeurs du courage n'ont pas d'importance. La distance dépend donc uniquement de la force. 

   Quelle est la catégorie de notre donnée cible ?
   
   > On peut représenter graphiquement cette condition :
   
   <center><img src="./img/Role4.png" width="500"></center>
   
   > Fantassin
   
### Exercice 3 : Champ de bataille

(Source : David Landry)

Sur un champ de bataille de la Première Guerre Mondiale un mémorial a été construit. Afin de réaliser une extension, des fouilles préventives ont été réalisées par l'INRAP (Institut National de Recherches Archéologiques Préventives).
Au cours de ces fouilles, différents objets ou éléments de squelettes humains ont été trouvés. L'étude de ces découvertes a permis d'identifier la nationalité de nombreux artéfacts retrouvés : soit allemand, anglais ou français.
Le plan ci-dessous représente la zone de fouille et la position des éléments dont l'origine a été identifiée. L'unité est le mètre.

Un élément d'un squelette a été retrouvé en (10; 4). Il est représenté par un losange couleur magenta sur le plan.

L'objectif est de déterminer une origine probable pour cet élément de squelette avant de le déposer dans un ossuaire.

<center><img src="./img/Champ_bataille.png" width="600"></center>

La distance qui sera prise en compte est la distance dite de Tchebychev. Pour cet exercice, l'ensemble des points se trouvant à une distance r d'un point I correspond au pourtour du carré, de centre 
I, de côtés parallèles aux axes et de longueurs 2r.

Sur le graphique ci-dessus, le carré dessiné :

* en rouge correspond ainsi à l'ensemble des points se trouvant à 3 mètres.
* en noir correspond ainsi à l'ensemble des points se trouvant à 1 mètre.


1. A quelle valeur de k correspond le carré noir ?

> k = 3

2. Quelle serait l'origine du squelette en considérant cette valeur de k ?

> Français

3. On choisit k=9. Quelle serait l'origine du squelette en considérant cette valeur de k ?

> Anglais

4. On choisit k=11. Quelle serait l'origine du squelette en considérant cette valeur de k ?

> Allemand

5. Peut-on savoir à coup sûr, en prenant une valeur de k inférieure au égale à 11, si le combattant dont on a trouvé un squelette était un combattant de la Triple-Entente (France + Royaume-Uni + Russie) ou de la Triple-Alliance (Allemagne + Autriche-Hongrie + Italie) ? Répondre par plusieurs arguments.

> Non, car...

>* dans les conditions d'étude actuelle, si k = 7, la nationalité allemande est majoritaire.
>* si on change les données, tout peut changer :
>  * si on change le type de distance.
>  * si on change le nombre de données de référence.
>* dans tous les cas, ce n'est qu'un algorithme prédictif. En aucun cas, cet algorithme n'apporte de certitude.
