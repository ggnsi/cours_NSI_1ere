import csv
from matplotlib import pyplot as plt

def csv_import(nom_fichier):
    '''retourne sous forme de liste de dictionnaire le contenu
    du fichier csv nom_fichier
    
    param nomfichier : (string)
    valeur retournée : list of dict(string:string)
    '''
    
    with open(nom_fichier, 'r', encoding="utf-8") as fichier:
        dico_reader = csv.DictReader(fichier, delimiter = ",")
        l_sortie = [dict(entree) for entree in dico_reader]
    return l_sortie


def trace_iris():

    l_iris = csv_import('iris.csv')
    
    x_versicolor, y_versicolor, x_setosa, y_setosa, x_virginica, y_virginica = [], [], [], [], [], []
    
    for iris in l_iris:
        if iris['species'] == 'versicolor':
            x_versicolor.append(float(iris['petal_length']))
            y_versicolor.append(float(iris['petal_width']))
        elif iris['species'] == 'setosa':
            x_setosa.append(float(iris['petal_length']))
            y_setosa.append(float(iris['petal_width']))
        else:
            x_virginica.append(float(iris['petal_length']))
            y_virginica.append(float(iris['petal_width']))
            
    plt.plot(x_versicolor, y_versicolor, 'bo', label='Versicolor')
    plt.plot(x_setosa, y_setosa, 'go', label='Setosa')
    plt.plot(x_virginica, y_virginica, 'ro', label='Virginica')
    plt.legend(loc="upper left")
    plt.show()

def trace_iris_2(larg_petale, long_petale):

    l_iris = csv_import('iris.csv')
    
    x_versicolor, y_versicolor, x_setosa, y_setosa, x_virginica, y_virginica = [], [], [], [], [], []
    
    for iris in l_iris:
        if iris['species'] == 'versicolor':
            x_versicolor.append(float(iris['petal_length']))
            y_versicolor.append(float(iris['petal_width']))
        elif iris['species'] == 'setosa':
            x_setosa.append(float(iris['petal_length']))
            y_setosa.append(float(iris['petal_width']))
        else:
            x_virginica.append(float(iris['petal_length']))
            y_virginica.append(float(iris['petal_width']))
            
    plt.plot(x_versicolor, y_versicolor, 'bo', label='Versicolor')
    plt.plot(x_setosa, y_setosa, 'go', label='Setosa')
    plt.plot(x_virginica, y_virginica, 'ro', label='Virginica')
    plt.plot(long_petale, larg_petale, 'ko', label='Iris à classifier')
    plt.legend(loc="upper left")
    plt.show()



trace_iris_2(0.5,3)

                                

    
    
    
    
    
    
    
    
    
    
    
    
    