import csv
import math # pour la fonction sqrt

def csv_import(nom_fichier):
    '''retourne sous forme de liste de dictionnaire le contenu
    du fichier csv nom_fichier
    
    param nomfichier : (string)
    valeur retournée : list of dict(string:string)
    '''
    
    with open(nom_fichier, 'r', encoding="utf-8") as fichier:
        dico_reader = csv.DictReader(fichier, delimiter = ",")
        l_sortie = [dict(entree) for entree in dico_reader]
    return l_sortie

# Attention : il faut penser à convertir les valeurs en flottant pour faire les calculs
# Ne pas oublier que lors de l'importation du fichier csv, toutes les valeurs sont des chaines de caractères

def distance_petale(iris_1, larg_petale, long_petale):
    '''Retoure la distance euclidienne entre les points dont les coordonnées
    sont (largeur, longueur) des iris_1 et iris_2
    
    param iris_1, iris_2 : dict(string:string)
    valeur retournée : (float)
    
    Exemple :
    >>> iris_1 = {'sepal_length': '5.1', 'sepal_width': '3.5', 'petal_length': '1.4', 'petal_width': '0.2', 'species': 'setosa'}
    >>> iris_2 ={'sepal_length': '5.0', 'sepal_width': '3.5', 'petal_length': '1.6', 'petal_width': '0.6', 'species': 'setosa'}
    >>> distance_petale(iris_1, iris_2)
    0.6000000000000001
    '''
    
    return math.sqrt((larg_petale - float(iris_1['petal_width'])) **2 + \
                     (long_petale - float(iris_1['petal_length'])) ** 2)

def ajout_distance(l_iris, larg_petale, long_petale):
    '''Ajoute pour chaque iris contenue dans l_iris une clé 'distance' ayant pour valeur la distance
    obtenue entre les pétales de l'iris et de celle ayant (larg_petale, long_petale)
    
    param l_iris : list of dict(string:string)
    param larg_petale, long_petale : (float)
    '''
    
    for iris in l_iris:
        iris['distance'] = distance_petale(iris, larg_petale, long_petale)
    

def effectif_iris(l_iris):
    '''Retourne un dictionnaire dont les clés sont les espèces d'iris ( 'setosa', 'versicolor' et 'virginica')
    et dont les valeurs sont le nombre d'iris de `l_iris` de ces espèces.
    param l_iris : list of dict(string:string)
    valeur retournée : dict(string:int)
    '''
    
    dico_effectifs = { 'setosa' : 0, 'versicolor' : 0, 'virginica' : 0}
    
    # on compte le nombre d'iris de chaque type en récupérant le type de l'iris et en augmentant l'effectif
    # correspondant dans dico_effectifs
    for iris in l_iris:
        dico_effectifs[iris['species']] += 1
    
    return dico_effectifs

def cle_tri_distance(elt):
    
    return elt['distance']

# On va effectuer un parcours du dictionnaire par (clé, valeur)
# j'utilise une variable nb_egalite me donnant le nombre d'espèces ayant comme effectif le maximum
# afin de savoir si il y a égalité entre plusieurs espèces
# Remarque : il n'y a ici que 3 espèces et possibilité de gérer les tests différemment 

def preponderant(l_iris):
    '''retourne l'espèce d'iris prépondérante dans l_iris

    param l_iris : list of dict(string:string)
    valeur retournée : string
    '''
    
    dico_effectifs = effectif_iris(l_iris)
    
    # maintenant on recherche quel type d'iris est prépondérant
    maxi = -1
    nb_egalite = 0
    type_maxi = ''
    
    for (type_iris, effectif) in dico_effectifs.items():
        if effectif > maxi:
            type_maxi = type_iris
            maxi = effectif
            nb_egalite = 0
        elif effectif == maxi:
            nb_egalite += 1
    if nb_egalite == 0:
        return type_maxi
    else:
        return None

# autre idée : on recherche le maximum et un type d'iris ayant
# cet effectif maximum.
# puis on re-parcourt la liste en étudiant si on trouve un autre
# type d'iris ayant cet effectif maximum
# si oui -> on retourne None
# si non -> on retourne le type d'iris trouvé

def preponderant_2(l_iris):
    
    dico_effectifs = effectif_iris(l_iris)
    
    max = -1
    type_iris_max = ''
    for (type_iris, valeur) in dico_effectifs.items():
        if valeur > max:
            max = valeur
            type_iris_max = type_iris
    
    for (type_iris, valeur) in dico_effectifs.items():
        if (valeur == max) and (type_iris != type_iris_max):
            return None
    return type_iris_max
    

def k_voisins(k, larg_petale, long_petale):
    '''Retourne la prévision de type de l'iris obtenue avec l'algorithme
    des _k_ plus proches voisins si une espèce est prépondérante, et `None` sinon
    
    param k : (int) le nombre de plus proches voisins à utiliser
    param larg_petale, long_petale : (float)
    valeur retournée : (string or None)
    '''
    
    liste_iris = csv_import('iris.csv')
    
    #copie de l_iris pour ne pas modifier la liste d'origine
    
    lst_iris = liste_iris.copy()
    
    # ajout pour chaque iris d'une clé 'distance' ayant pour valeur la distance avec l'iris qui a
    # larg_petale et long_petale
    
    ajout_distance(lst_iris, larg_petale, long_petale)
    
    # tri de la liste des iris en utilisant la distance comme clé
    liste_iris.sort(key = cle_tri_distance, reverse = False)
    
    # On ne conserve que les k premiers
    
    l_k_premiers = []
    for ind in range(k):
        l_k_premiers.append(liste_iris[k])
    
    return preponderant(l_k_premiers)
    
    
    
    
    
    
    
    
    
    
    
    
    