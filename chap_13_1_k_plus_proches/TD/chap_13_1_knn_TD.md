# TD - Algorithme des k plus proches voisins

### Exercice 1

Ci-dessous, on donne un échantillon constitué de 11 éléments réparti selon 2 classes (les carrés en bleu et les triangles en rouge)

<center><img src="./img/knn_exo_fig_1.jpg" width="500"></center>

Appliquer l’algorithme KNN pour le nouvel élément représenté par un cercle vert afin de déterminer dans quelle catégorie il sera classé

1. pour k = 3
2. pour k = 5
3. pour k = 9

### Exercice 2 : Jeu de rôle

(Source : David Landry)

Considérons un jeu de rôle médiéval avec des chevaliers et des fantassins.

Le jeu génère des soldats aléatoirement et il faut leur donner une spécialité d'après deux de leurs caractéristiques (Courage et force) et celles de soldats déjà catégorisés en chevalier ou fantassins.

Voici la représentation des soldats pris pour référence :

<center><img src="./img/Role1.png" width="500"></center>

Nous introduisons une cible de force = 5 et de courage = 12,5.

On choisit k = 4 et la distance est schématisée par un disque :

<center><img src="./img/Role2.png" width="500"></center>

1. Quels sont les k plus proches voisins de notre donnée cible ? 


2. Quel est le problème ? Comment le résoudre ?


3. Donner deux valeurs de k permettant de décider de la catégorie de notre donnée cible ?


4. Donner pour ces deux valeur de k la catégorie de la donnée cible.



5. On choisit maintenant k = 9. Pour la distance, on décide que les valeurs de la force n'ont pas d'importance. La distance dépend donc uniquement du courage. On peut représenter graphiquement cette condition ainsi  :

   <center><img src="./img/Role3.png" width="500"></center>

   Quelle est la catégorie de notre donnée cible ?


6. On choisit k = 5. Pour la distance, on décide que les valeurs du courage n'ont pas d'importance. La distance dépend donc uniquement de la force. 

   Quelle est la catégorie de notre donnée cible ?

### Exercice 3 : Champ de bataille

(Source : David Landry)

Sur un champ de bataille de la Première Guerre Mondiale un mémorial a été construit. Afin de réaliser une extension, des fouilles préventives ont été réalisées par l'INRAP (Institut National de Recherches Archéologiques Préventives).
Au cours de ces fouilles, différents objets ou éléments de squelettes humains ont été trouvés. L'étude de ces découvertes a permis d'identifier la nationalité de nombreux artéfacts retrouvés : soit allemand, anglais ou français.
Le plan ci-dessous représente la zone de fouille et la position des éléments dont l'origine a été identifiée. L'unité est le mètre.

Un élément d'un squelette a été retrouvé en (10; 4). Il est représenté par un losange couleur magenta sur le plan.

L'objectif est de déterminer une origine probable pour cet élément de squelette avant de le déposer dans un ossuaire.

<center><img src="./img/Champ_bataille.png" width="600"></center>

La distance qui sera prise en compte est la distance dite de Tchebychev. Pour cet exercice, l'ensemble des points se trouvant à une distance r d'un point I correspond au pourtour du carré, de centre 
I, de côtés parallèles aux axes et de longueurs 2r.

Sur le graphique ci-dessus, le carré dessiné :

* en rouge correspond ainsi à l'ensemble des points se trouvant à 3 mètres.
* en noir correspond ainsi à l'ensemble des points se trouvant à 1 mètre.


1. A quelle valeur de k correspond le carré noir ?

2. Quelle serait l'origine du squelette en considérant cette valeur de k ?

3. On choisit k=9. Quelle serait l'origine du squelette en considérant cette valeur de k ?

4. On choisit k=11. Quelle serait l'origine du squelette en considérant cette valeur de k ?



5. Peut-on savoir à coup sûr, en prenant une valeur de k inférieure au égale à 11, si le combattant dont on a trouvé un squelette était un combattant de la Triple-Entente (France + Royaume-Uni + Russie) ou de la Triple-Alliance (Allemagne + Autriche-Hongrie + Italie) ? Répondre par plusieurs arguments.

### Exercice 4 : Classification des iris

#### Les iris

L’Iris est un genre de plantes vivaces à rhizomes ou à bulbes de la famille des Iridacées. Le genre Iris contient 210 espèces et d'innombrables variétés horticoles. (d'après l'article [Iris](https://fr.wikipedia.org/wiki/Iris_(genre_v%C3%A9g%C3%A9tal)) sur wikipédia.fr).

<center><img src="./img/iris.png" width="500"></center>

#### Des données pour de l'analyse prédictive

En 1936, Edgar Anderson a collecté des données sur 3 espèces d'iris : "iris setosa", "iris virginica" et "iris versicolor".

<center><img src="./img/iris_2.png" width="450"></center>

Pour chaque iris, quatre mesures on été faites (en cm) :

- la largeur des sépales

- la longueur des sépales

- la largeur des pétales

- la longueur des pétales

<center><img src="./img/iris_3.png" width="250"></center>

Le jeu de données (voir l'article [Iris de Fisher](https://fr.wikipedia.org/wiki/Iris_de_Fisher) sur wikipédia.fr) rassemble 150 enregistrements (50 de chaque espèce) dans un fichier csv : [iris.csv](./iris.csv).

#### But de l'exercice

On souhaite, en utilisant l'algorithme des _k_ plus proches voisins, pouvoir prédire à quelle espèce appartiendra une iris que l'on vient de trouver dans son jardin à partir des longueurs et largeurs de ses sépales et de ses pétales.

#### Travail à faire Méthode 1 :

1. Ecrire une fonction `csv_import(nom_fichier)` qui retourne sous la forme d'une liste de dictionnaires les données du fichier csv `nom_fichier`.

2. Ecrire une fonction `distance_petale(iris_1, larg_petale, long_petale))` (où `iris_1` est un dictionnaire  et `larg_petale, long_petale` des flottants) qui renvoie  la distance _d_ entre les iris calculée de la mariène suivante :

<center><img src="./img/iris_4.jpg" width="550"></center>

   _Exemple_ :
```Python
>>> iris_1 = {'sepal_length': '5.1', 'sepal_width': '3.5', 'petal_length': '1.4', 'petal_width': '0.2', 'species': 'setosa'}
>>> iris_2 ={'sepal_length': '5.0', 'sepal_width': '3.5', 'petal_length': '1.6', 'petal_width': '0.6', 'species': 'setosa'}
>>> distance_petale(iris_1, iris_2)
0.6000000000000001
```

3. Ecrire une fonction `l_dist(l_iris, larg_petale, long_petale)` qui retourne la liste constituée des dictionnaires de la forme  {'distance' : ..., 'indice': ...}, où
    `'distance'` est la distance entre l'iris ayant `'indice'` dans `l_iris` et celle ayant `larg_petale, long_petale`

    Cette liste nous permettra donc de déterminer les _k_ plus proches éléments de l'iris ayant `'indice'` dans `l_iris` et de connaître leur indice dans la liste globale d'iris.

4. Ecrire une fonction `k_plus_proches((l_iris, larg_petale, long_petale)` qui retourne une liste contenant les _k_ iris les plus proches de celle ayant `larg_petale, long_petale` (il sera nécessaire d'utiliser une fonction annexe à définir)

5. Ecrire une fonction `effectif_iris(l_iris)`  où `l_iris` est une liste d'iris (représentées par des dictionnaires) qui retourne un dictionnaire dont les clés sont les espèces d'iris ( 'setosa', 'versicolor' et 'virginica') et dont les valeurs sont le nombre d'iris de `l_iris` de ces espèces.

6. Ecrire une fonction `preponderant(l_iris)` où `l_iris` est une liste d'iris (représentées par des dictionnaires) qui :
   * retourne le type d'iris le plus présent dans la liste d'iris s'il y en a un
   * qui retourne la chaîne de caractère `None` sinon (cad si il y au moins deux types d'iris ayant l'effectif maximum trouvé)

7. Ecrire une fonction `k_voisins(k, larg_petale, long_petale)` où `k` est un entier positif non nul, `larg_petale` et `long_petale` sont respectivement la largeur et la longueur des pétales d'une iris et qui retourne la prévision de type de l'iris obtenue avec l'algorithme des _k_ plus proches voisins si une espèce est prépondérante, et `None` sinon.

   Pour cette fonction, il faudra :
   * importer le fichier csv `iris.csv` ;
   * obtenir la liste des _k_ iris les plus proches de celle qui nous intéresse ;
   * y déterminer le type prépondérant s'il existe. Si oui, on le retourne. Si non, on 
   retourne la chaîne de caractères `aucun type prépondérant`.

   _Elements de vérification_
```Python
>>> k_voisins(4, 0.25, 1.5)
'setosa'
>>> k_voisins(4, 2, 6)
'virginica'
>>> k_voisins(4, 1.5, 5)
'aucun type prépondérant'
>>> k_voisins(2, 1.75, 4.85)
'aucun type prépondérant'
```


#### Méthode 2 :

On peut ajouter à chaque iris de la liste une clé `distance` dont la valeur est la distance entre l'iris considérée et celle ayant `larg_petale` et `long_petale` comme largeur et longueur de pétale.  
Il faudra ensuite déterminer les _k_ plus proches voisins et le type prépondérant si il en existe un.



#### Approfondissement possible

1. Ecrire une fonction qui permet de visualiser la répartition des iris suivant les informations __sur les pétales__, autrement dit en abscisse la longueur des pétales et en ordonnées la largeur des pétales et une couleur différente pour chaque espèce. On utilisera le module Matplotlib.

<center><img src="./img/iris_graphique.png" width="400"></center>

2. Modifier la fonction précédente pour qu'elle représente également par un point noir l'iris qu'on cherche à classifier (il faudra inclure des paramètres à la fonction).

3. Ecrire une fonction `k_voisins(k, larg_petale, long_petale, larg_sepale, long_sepale)` qui prend également en compte les largeurs et longueurs des sépales.  
   Il faudra adapter la distance utilisée.
