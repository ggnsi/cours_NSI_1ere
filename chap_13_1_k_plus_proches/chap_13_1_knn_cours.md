# Algorithme KNN des plus proches voisins
---

## Principe de la méthode

>La méthode des __k plus proches voisins__ (ou _KNN__ pour _k Nearest Neighbours_) est un algorithme d'apprentissage qui, à partir d'un jeu de données connues va classifier (ou attribuer une valeur à) un élément en fonction de la nature de ses voisins (ou des valeurs possédées par ses voisins).  
>* Lorsque l'on classe l'élément dans une catégorie, on parle de problème de _classification_ ;
>* Lorsque l'on attribue une valeur à l'élément, on parle de problème de _régression_.

On va choisir les k données les plus proches du point étudié afin soit d'en prédire sa nature, soit d'en prédire une valeur.

On pourrait résumer cela par "Dis-moi qui sont tes amis, et je te dirais qui tu es".


### Exemple de problème de régression :

On connait les adresses et les prix des logements vendus dans une zone et on souhaite estimer le prix de vente d'un logement L connaissant son adresse (on suppose ici que le prix de vente
ne dépend que de l'emplacement).

On va déterminer les prix de vente des _k_ logements les plus proches de L et on en fera la moyenne pour obtenir une estimation plausible.


### Exemple de problème de classification

(source et crédit des illustations utilisées dans cet exemple [^1]):

La carte scolaire répartit les élèves dans des établissement scolaires en fonction de leur lieu d'habitation.
On suppose qu'on ne connait pas les limites de cette carte scolaire mais que pour un certain nombre d'élèves, on connait leur adresse et leur établissement d'affectation.

On cherche à essayer de prédire de manière _plausible_ , pour un nouvel élève, dans quelle établissement il serait.

On a représenté ci-dessous les données connues : localisation des élèves et établissement d'affectation (représenté par une couleur).

<img alt="carte_scolaire" src="./img/voisins_img_1.jpg" width="250">

Pour un nouvel élève (en utilisant ici 3 voisins), on détermine dans quelles catégories sont ses voisins et ont lui affecte la catégorie majoritaire.

<img alt="carte_scolaire" src="./img/voisins_img_2.jpg" width="600">

__Allez voir l'[animation sur ce site](http://mathartung.xyz/nsi/cours_algo_knn1.html#canvasTheorique)__ : Cliquez sur un point quelconque de la zone du dessous (pas un point vert ni rouge !).  
Vous aurez à droite les distances aux _k_ voisins les plus proches et la classification déduite.

### Méthodologie globale 

Supposons donc connues les données pour un certains nombre d'éléments et cherchons à classifier un nouvel élément (ou à effectuer une régression)

Pour ce nouvel élément, il faudra donc :
* Évaluer la distance qui le sépare de chacun des autres éléments de l’ensemble.
* Stocker ces valeurs de distance en mémoire.
* Trier ces valeurs de distances suivant l’ordre croissant.
* Restreindre cette liste de valeurs aux k premiers éléments.
* Assigner une classe à ce nouvel élément en fonction de la majorité des classes représentées parmi les k plus proches voisins (ou déterminer une valeur à lui associer
en fonction des valeurs connues des voisins).

### Résumé du principe en vidéo 

<a href="http://guillonprof.free.fr/nsi/videomachinelearning_coupe.ogv" target=blank>Regardez cette vidéo</a> (Source: [^2]) 


## Limites de l'algorithme

### influence de la valeur de k

La variation du nombre de voisins utilisés peut faire varier notre classification précédente comme le montre le dessin ci-dessous où la classification de _b_ et de _c_ change en fonction du nombre de voisins utilisés.

<img alt="carte_scolaire" src="./img/voisins_img_3.jpg" width="350">

Un petit nombre de voisins permet de ne tenir compte que des éléments les plus proches. Un trop petit nombre perturbe facilement la classification.
Un nombre plus important de voisins permet de gommer des perturbations locales mais peut aussi prendre en compte des éléments non significatifs.

__Allez voir l'[animation sur ce site](http://mathartung.xyz/nsi/cours_algo_knn1.html#curseur2)__ : Elle détermine en fonction du nombre _k_ de voisins utilisés la classification de chaque point.  
En faisant varier la valeur de _k_, vous pourrez constater que certains points changent de classification.

_Remarques_ : 
* la détermination d'une valeur pertinente de _k_ n'est pas toujours simple et varie en fonction du jeu de données ;
* dans le cas d'une classification où seules 2 catégories existent, on prendra une valeur impaire de _k_ pour éviter les égalités.



### influence du jeu de données utilisé

<img alt="carte_scolaire" src="./img/voisins_img_2.jpg" width="600">

Dans la sectorisation du point c, on peut voir que les données concernant l'établissement blanc ne sont pas localement assez nombreuses par rapport à l'établissement noir.
On est aussi dans une localisation proche de la limite entre les secteurs.
L'imprécision de la classification est alors intrinsèque à la méthode utilisée.


Il faut déjà que les données :
* soient suffisemment nombreuses ; 
* ne soient pas trop différentes de l'élément observé ;
* ne soient pas réparties de manières inégales entre les différentes classes possibles (pour ne pas donner trop d'importance à l'une ou l'autre)

### Quelles distances ?

Depuis le début, on indique qu'il faut déterminer les _k_ voisins __les plus proches__.

Cela implique que l'on définisse une distance entre les points considérés.

Il existe de nombreuses distances. 

####  Distances usuelles 

* Sur une droite graduée, la distance entre deux points est donnée par la valeur absolue de la différence de leurs abscisses

<center><img src="./img/distance_1.jpg" width="200"></center>

* Dans le plan muni d'un repère orthonormé, la distance euclidienne entre les points A(x_1,y_1) et B(x_2, y_2) est donnée par 

<center><img src="./img/distance_2.jpg" width="220"></center>

On peut généraliser cette formule dans l'espace : 

<center><img src="./img/distance_3.jpg" width="280"></center>

#### D'autres distances ?

* [distance de Manhattan](https://fr.wikipedia.org/wiki/Distance_de_Manhattan) ;
* [distance de Hamming](https://fr.wikipedia.org/wiki/Distance_de_Hamming) ;
* [distance de Levenshtein](https://fr.wikipedia.org/wiki/Distance_de_Levenshtein)

### Sources utilisées

* [^1] : Livre NSI Balabanski/Conchon/Filliâtre/Nguyen Ellipses
* [^2] isn-icn-ljm.pagesperso-orange.fr qui s'est appuyé sur cette vidéo https://www.youtube.com/watch?v=MDniRwXizWo)
* http://mathartung.xyz/nsi/cours_algo_knn1.html


