# Le langage CSS

__Sommaire__ :

1. <a href="#kesa">Kézako le CSS ?</a>
2. <a href="#exem">Des exemples</a>
3. <a href="#ptes">Propriétés CSS, ressources</a>
4. [Travail à faire](travail-a-faire)

-----

## <span id="kesa"></span>Kézako le CSS ?
----

> Le W3C a défini deux langages afin de bien séparer le fond et la forme. Le `html` décrit le __contenu__ et la __structure__ du document, et le `css` décrit sa __forme__. Le css permet de mettre en forme notre page web pour personnaliser son aspect (positionnement des éléments, décoration, couleurs, taille de texte, etc.).  
Ce langage est venu compléter le HTML avec une première version en 1996.

> __CSS__ signifie __Cascading Style Sheets__ ou __feuilles de style en cascade__.

### principe - intérêt

Le principe est d'adjoindre à la page html une feuille de style décrivant le mise en forme des documents.

Avantages (source : [wikipédia](https://fr.wikipedia.org/wiki/Feuilles_de_style_en_cascade#S%C3%A9paration_entre_la_structure_et_la_pr%C3%A9sentation)) :

* La structure du document et la présentation peuvent être gérées dans des fichiers séparés ;
* La conception d'un document se fait dans un premier temps sans se soucier de la présentation, ce qui permet d'être plus efficace ;
* Dans le cas d'un site web, la présentation est uniformisée : les documents (pages HTML) font référence aux mêmes feuilles de styles. Cette caractéristique permet de plus une remise en forme rapide de l'aspect visuel (par exemple en fonction du média sur lequel la page est affichée : écran ordinateur, portable, tablette) ;
* Un même document peut donner le choix entre plusieurs feuilles de style, par exemple une pour l'impression et une pour la lecture à l'écran. Certains navigateurs web permettent au visiteur de choisir un style parmi plusieurs ;
* Le code HTML est considérablement réduit en taille et en complexité, puisqu'il ne contient plus de balises ni d'attributs de présentation.

### Pourquoi le mot `en cascade` dans feuille de style _en cascade_ ?

Cela signifie :
* que certaines propriétés CSS d'un élément peuvent être héritées de celles de l'élément parent (celui à l'intérieur duquel est situé l'élément)
* que si deux règles sont applicables, c'est la dernière déclarée qui sera utilisée.

_Exemple_ :

Supposons une page dont la structure est celle ci-dessous :

<img src="img/emboitements.png" width="300">

Si l'on applique une couleur de fond à la balise `div` principale, tous les éléments contenus hériteront de cette couleur de fond, à moins qu'on n'en définisse une autre directement pour eux. Alors, la dernière régèle définie s'appliquera, c'est à dire celle de l'élément inclus.

_Compléments de lecture_ :

* [Cascade et héritage](https://developer.mozilla.org/fr/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance)
* [Héritages et cascades](http://www.css-faciles.com/heritage-cascade.php)

### Des exemples pour visualiser

Les images ci-desssous correspondent à trois versions __du même code HTML__ :

* la première sans aucune feuille de style ;
* les deux suivantes avec des feuilles de styles différentes.

Ces images ont été généré à partir du site [Zen Garden](http://www.csszengarden.com) que je vous invite à visiter

__Version brute__

<img src="img/zen_garden_1.jpg" height="400" style="border:solid 2px;">

__Version 1__

<img src="img/zen_garden_2.jpg" height="400" style="border:solid 2px;">

__Version 2__

<img src="img/zen_garden_3.jpg" height="400" style="border:solid 2px;">

----
## <span id="exem"></span>Des exemples
----

__Page brute__ :

<div style="text-align:center;">
<img src="img/css_exemple_1_html.jpg" width="350" style="display:inline;"> <img src="img/css_exemple_1.jpg" width="350" style="display:inline;"> 
</div>

__première inclusion de code CSS__:

<img src="img/css_exemple_2_html.jpg" width="500">

>La ligne `<link rel="stylesheet" type="text/css" href="style.css">` a été incluse dans le `<head>` afin de lier la feuille de style `style.css` (située dans le même répertoire) à la page.

__code css à droite__ :

<div style="text-align:center;">
<img src="img/css_exemple_2.jpg" width="600" style="display:inline; margin-right:40px;"> <img src="img/css_exemple_2_css.jpg" width="250" style="display:inline;"> 
</div>

Nous avons défini un style pour chaque type de balise. Le style s'applique alors à toutes les balises de même type.

_Remarques_ :

* La syntaxe : pour chaque élément sélectionné, les propriétés de style sont mises entre accodades 
* le nom de la propriété CSS est séparé de sa valeur par `:` et les propriétés sont déparées par des point-virgule ;
* La balise `<h1>` n'a pas de couleur spécifique définie : elle a donc hérité de celle de son parent (ici `<body>`)

__Problème__ : il n'est pas ainsi possible de différencier les mises en forme sur des paragraphes différents.

__Autre exemple__ :

<img src="img/css_exemple_3_html.jpg" width="500">

* Des `identifiants (id)` ont été attribués à certaines balises via l'attribut `id="..."` (deux balises ne peuvent pas avoir le même identifiant)
* D'autres dopteront le style défini par une classe (attribut `class="..."`) (plusieurs balises peuvent utiliser la même classe de style).

__code css ajouté au précédent à droite__ :
<br>

<div style="text-align:center;">
<img src="img/css_exemple_3.jpg" width="600" style="display:inline; margin-right:40px;"> <img src="img/css_exemple_3_css.jpg" width="250" style="display:inline;"> 
</div>

_Remarques_ :

* On attribue une style particulier à __chaque identifiant__ avec la synthaxe `#id` 
* On attribue une style particulier à __chaque classe__ avec la synthaxe `.id` 

__Résumé sur les sélecteurs__ ([source](https://www.lyceum.fr/1g/nsi/5-interactions-entre-lhomme-et-la-machine-sur-le-web/3-mise-en-forme-avec-css))

|sélecteur| Éléments visés|
|:-- | :--|
|p {...}| Tous les éléments avec la balise `<b>`|
|p, li{...}|Tous les éléments `<p>` et les éléments `<p>`|
|div p{...}|Tous les éléments `<p>` contenus dans(descendants d')une balise `<div>`|
|.centre {...}|Tous les éléments ayant un attribut `class="centre"`|
|#formulaire {...}|L'élément ayant un attribut `id="formulaire"`|
|p.centre {...}|Tous les éléments `<p>` ayant un attribut `class="centre"`|

---
## <span id="#ptes"></span>Propriétés CSS, ressources
---

Vous avez maintenant le principe. A vous d'explorer les différentes possibilités.
    

Des ressources :
* [site de M. Willm](http://numerique.ostralo.net/html_css/p3a_generalites_styles.htm) résumant les principales propriétés et les bases du CSS ;
* [CSS sur W3schools.com](https://www.w3schools.com/css/default.asp)
* [tutoriel CSS sur developper.mmozilla.org](https://developer.mozilla.org/fr/docs/Web/CSS)
* [CSS sur le site d'un collègue](https://www.silanus.fr/nsi/premiere/css/css.html)
* [CSS et images sur W3schools.com](https://www.w3schools.com/css/css3_images.asp)
* etc.

---
## Travail à faire
---

Vous devez réaliser dans un dossier un petit site html sur le thème que vous voulez (sport, cinéma, passion, ...) avec les contraintes suivantes :

* le site doit comporter au moins trois pages dont
  * une page sommaire (qui inclue donc des liens vers les autres pages du site)
  * les autres pages doivent comporter au moins un lien permettant de revenir au sommaire.
* les pages doivent être mises en forme en utilisant du CSS ;
* des images doivent être présentes

