# Le langage HTML

__Sommaire__ :

1. <a href ="#structure">Structure d'une page Web</a>
2. [Les balises](#les-balises)
3. [Les principales balises](#les-principales-balises)
4. [Travail à faire](#travail-a-faire)

---
__Rappel__ :

> Vous retrouvez une synthèse des informations sur le site de M.Willm : [HTML et CSS](http://numerique.ostralo.net/html_css/)


------

## <span id="structure"></span>Structure d'une page Web
--------

### structure générale

1. Enregistrer dans votre espace (bouton droit :  enregistrer la cible sous) le fichier [fichier_1_structure.html](fichiers/fichier_1_structure.html) puis l'ouvrir avec votre navigateur.
2. Utiliser `Ctrl + U` pour visualiser le code source de la page.

Une page HTML doit respecter une structure précise.

<img src="img/html5-basic.png" width="200">

La structure minimale se compose de :

* un `Doctype` qui est une ligne de code servant à indiquer le type de document (html ici) ;
* une __entête__ (`head`) comprise entre les balises `<head>` et `</head>`.  
  >L'élément HTML `<head>` fournit des informations générales (métadonnées) sur le document, incluant son titre et des liens ou des définitions vers des scripts et feuilles de style. 
    Il contient principalement des données destinées au traitement automatisé et pas nécessairement lisibles par des humains (source : [developer.mozilla.org](https://developer.mozilla.org/fr/docs/Web/HTML/Element/head))
  
  C'est également dans l'entête que l'on inclut des liens par exemple vers des fichiers `css` (pour la mise en forme) ou `javascript` (pour inclure des scripts).


* un __corps__ (`body`) qui représente le contenu principal du document HTML.
    
__Attention__ :  L'entête et le corps sont inclus dans des balises `<html>` et `</html>`.

### Etude du fichier ouvert    

<img src="img/fichier_1_structure.jpg" width="500">
    
_Bonne pratique_ : les lignes de code sont indentées afin d'avoir une structure lisible du document.

#### section `head` ([documentation](https://developer.mozilla.org/fr/docs/Web/HTML/Element/head)) 

* `<meta charset="utf-8">` Indique au navigateur le jeu de caractères utilisés : à mettre à chaque fois !


* `<meta name="author" content="Gg">` : Ajoute une métadonnée indiquant quel est l'auteur de la page. Non visible par le lecteur mais utile pour les robots qui indexent les pages par exemple.


* `<title>Ma première page</title>` : Indique ce qui sera affiché sur l'onglet du navigateur contenant la page .

#### section `body`

Elle est ici constituée d'un simple paragraphe (balises `<p>` et `</p>`) qui contient le texte `Bonjour`

### J'apprends à modifier une page

* Editer le fichier `fichier_1_structure.html` sauvegardé sur votre disque en utilisant `Notepad` (bouton droit : `Edit with Notepad++`).  

  Notepad ++ détecte l'extension `.hmtl` du fichier indiquand qu'il contient une page html et utilise une _coloration syntaxique_ adaptée.
  
  
* Modifiez le contenu de la page : 

```html
<p>Bonjour le monde</p>
<p>J'aime la NSI</p>
```
* puis sauvegardez (Ctrl + S)
* actualisez enfin la page dans votre navigateur (soit avec la touche F5, soit avec la flèche ronde)

--------
##  Les balises
----

### Définitions

> Une balise est un ensemble de caractères qui commence par < et fini par >.
>
> Elle indiquent la nature ou le sens du contenu

Il y a deux types de balises :

* les balises qui vont par paire

  Elles encadrent alors le contenu entre une balise ouvrant et une balise fermante.
  
  _Exemple_ : `<title>Ma première page</title>`
  
 
* les balises orphelines

  Elles servent à insérer un élément qu'il n'est pas nécessaire de délimiter.
  
  _Exemple_ : `<meta charset="utf-8">`
  

> Il existe une centaine de balises : voir une liste [sur le site de M. Willm](http://numerique.ostralo.net/html_css/p2g_liste_balises.htm).


### Règles d'écritures

>Les balises ouvrantes et fermantes doivent toujours être correctement imbriquées, c'est à dire que les balises fermantes doivent êtres écrites dans l'ordre inverse des balises d'ouverture. Une bonne imbrication des balises est une des règles à respecter afin d'avoir un code valide. [Source MDN](https://developer.mozilla.org/fr/docs/Learn/HTML/Introduction_to_HTML#Les_balises)

_Exemple de code valide_ :

```html
<em>Ceci est <strong>très</strong> important</em>.
```

_Exemple de code non-valide_ :

```html
<em>Ceci est <strong>très</em> important</strong>.
```

### Les attributs d'une balise

> Chaque élément HTML peut avoir un ou plusieurs attributs. Ces attributs sont des valeurs supplémentaires qui permettent de configurer les éléments ou d'adapter leur comportement. [Source MDN](https://developer.mozilla.org/fr/docs/Web/HTML/Attributes)

* Exemple : balise `<meta>`

    ```html
    <meta charset="utf-8">
    <meta name="author" content="Gg">
    ```
  
   `charset`, `name`, `content` sont des attributs de la balise `<meta>`
   

* Exemple avec la balise `<img>` (que nous utiliserons après)

    ```html
    <img src="img/mon_chat.jpg" alt="image de mon chat" width="300">
    ```
    
    * l'attribut `src` permet d'indiquer l'emplacement du fichier image ;
    * l'attribut `alt` permet de définir un texte de remplacement au cas où l'image ne puisse être affichée
    * l'attribut `width` permet de définir la largeur de l'image.

### Deux attributs particuliers 

Toutes les balises disposent, entre autre, des attributs suivants :

* style : permet de définir le style (mais on pourra utiliser un fichier css à part)
* id : permet d'associer un nom à une balise et donc de la repérer pour, entre autre, lui attribuer un style.

-----
## Les principales balises

### Un petit exemple

* Ouvrez le fichier [fichier_2_recette.html](fichiers/fichier_2_recette.html) ;
* observez son code source du `body`(remis-ci dessous) et observez le rôle et le rendu des différentes balises utilisées :

```html
<body>
	<div>
		<h1>Mini cakes au crabe</h1> <!-- titre de taille 1 (la plus grande possible) -->
		
		<p> Vous pouvez retrouver ma recette sur <a href="http://marmiton.org" target="_blank" >marmiton</a>
			</p>
		<h3> Visuel du plat fini</h3> <!-- titre de taille 3 -->
		
		<img src="mini-cakes-au-crabe.jpg" alt="image du plat terminé" width="300" /><br/>
		
		<h3>Ingrédients pour 6 personnes</h3>
		
		<ul>
			<li>2 gousses d'ail</li> 
			<li>3 oeufs </li>
			<li>130 g de farine </li>
			<li>90 g d'emmental râpé</li> 
			<li>50 g de beurre</li>
			<li>10 cl de lait </li>
			<li>1 sachet de levure chimique </li>
		</ul>
		
		<h3>Préparation</h3>
		
		<p>Chauffez le four à 200°C (thermostat 7). <em>Faites fondre le beurre</em>.<br> Dans un saladier, mélangez la farine et la levure. <strong>Faites un puits</strong>, versez-y les oeufs,le beurre et le lait. Salez, poivrez et mélangez. Ajoutez le fromage râpé, l'ail haché et le crabe émietté.</p>
		<p>Beurrez et farinez des petits moules à cake ou à brioches. Répartissez-y la préparation, enfournez et faites cuire de <mark>15 à 20 minutes</mark>.</p>
    </div>
 </body>
```

### Balises mettant en forme du texte

Quelques balises :

| balise | rôle | exemple |
|  :---  |  :---  |  :--- |
| `<em>` | Met en emphase (italique par défaut) | `<em>ceci est important</em>` |
| `<strong>` | Texte important (gras par défaut) | `<strong>ceci est important</strong>` |
| `<mark>` | Texte surligné (couleur de fond jaune par défaut) | `<mark>texte surligné</mark>` |
| `<cite>` | Pour une citation |  |
| `<code>` | Pour du code informatique |  |
| `<sup>` et `<sub>` | Pour les exposants et les indices |  |
    
_Remarque_ : Le CSS permettra de modifier les éléments par défaut (ou de définir vos propres éléments)

### Balises structurant le texte

| balise | rôle | exemple |
|  :---  |  :---  |  :--- |
| `<h1>` à `<h6>` | Titres et sous-titres du plus gros au plus petit| `<h1> Titre principal</h1>`<br>`<h3>Sous-Titre</h1>` |
| `<p>` | Définit un paragraphe<br> Retour chariot et espacement vertical<br>avec le paragraphe précédent | `<p>paragraphe</p>` |
| `<br>` | Passage à la ligne (orpheline) | `Bonjour <br>`<br>`Je suis content de vous voir` |
| `<ul>` et `<li>` | `<ul>` définit une liste __non numérotée__<br>et `<li>` chaque élément de la liste | `<ul>`<br>`<li> premier élément</li>`<br>`<li>Deuxième élément </li>`<br>`</ul>`|
| `<ol>` et `<li>` | `<ol>` définit une liste  __numérotée__<br>et `<ol>` chaque élément de la liste | `<ul>`<br>`<li> premier élément</li>`<br>`<li>Deuxième élément </li>`<br>`</ol>`|
| `<div>` | Crée un bloc.<br> Balise permettant de structure le contenu<br>et d'appliquer du style à différentes zones | |

### Insérer un lien hypertexte

__Syntaxe__ :

> `<a href="URL_visé"> Texte du lien </a>`

> Une URL est simplement l'adresse d'une ressource donnée, unique sur le Web.

L'URL peut être absolue ou relative selon que le chemin indiqué est fait en référence ou non à l'emplacement du fichier où il est situé.


__Exemples__ :

* `<a href="http://marmiton.org">marmiton</a>` :  __URL absolu__ lien hypertexte vers la racine du site `marmiton.org` :;
* `<a href="https://developer.mozilla.org/fr/docs/Web">Technos web</a>` : __URL absolu__ lien hypertexte vers le fichier `Web` du  site `developer.mozilla.org` et situé dans le sous-dossier `docs` du dossier `fr` de la racine du serveur ;
* `<a href="page2.html">autre page</a>` : __URL relative__ lien hypertexte vers le fichier `page2.html` situé dans le même répertoire que le fichier actuel ;
* `<a href="rep1/page2.html">autre page</a>` : __URL relative__ lien hypertexte vers le fichier `page2.html` situé dans le dosser `rep1` qui est dans le même dossier que le fichier actuel.

__Attribut `target`__ ([documentation](https://developer.mozilla.org/fr/docs/Web/HTML/Element/a)):

Cet attribut indique où afficher la ressource liée. 

* `target=_self` : charge la réponse dans le contexte de navigation courant pour nous : le même onglet
* `target=_blank` : charge la réponse dans un nouveau contexte de navigation. 

__Compléments__ : [Comprendre un URL](https://developer.mozilla.org/fr/docs/Learn/Common_questions/What_is_a_URL) : site developer.mozilla.org


### Insérer une image

__Syntaxe__ [documentation](https://developer.mozilla.org/fr/docs/Web/HTML/Element/Img)) :

> `<img src="URL_visé">`

__Exemples__ :

`<img src="mini-cakes-au-crabe.jpg" alt="image du plat terminé" width="300" />`

Inclut l'image avec `image du plat terminé` comme texte alternatif (si l'image ne peut être affichée) et définit re-dimensionnel l'image avec une largeur de 300 pixels.

__compléments__ :

La balise `<img>` possède l'attribut `align` dont les valeurs peuvent être `top`, `middle`, `bottom`, `left`, `right` :
* les trois premiers indiquent la manière dont l'image doit se positionner par rapport à la ligne d'écriture (haut de l'image aligné sur la ligne d'écriture, milieu de l'image ou bas) ;
* les deux derniers indiquent la manière dont l'image doit être positionnée sur la ligne par rapport au texte (à gauche ou à droite de la page)

### autres balises

Il existe nombre d'autres balises : pour insérer de l'audio, de la vidéo, lancer un téléchargement, ...

Une recherche sur le Web ou sur un site de référence permet de trouver son bonheur, d'avoir sa description, ses attribus, etc.

-----
## Travail à faire
-----

A partir du fichier [html_exercice_1_eleve.html](fichiers/html_exercice_1/html_exercice_1_eleve.html), effectuer les modifications nécessaires pour obtenir le résultat présenté ci-dessous.

_Remarques_ :

* le fichier image à utiliser est disponible [ici](fichiers/html_exercice_1)
* l'adresse du site du lycée est `https://lycee-beaupre.fr/`

<img src="fichiers/html_exercice_1/html_exercice_1_objectif.jpg" width="700">

