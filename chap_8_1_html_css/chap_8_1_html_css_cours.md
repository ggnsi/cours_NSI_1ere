# HTML et CSS

1. [Le Web](#le-web)
2. [La naissance du Web](#la-naissance-du-web)
3. [Ressources logicielles et documentations](#ressources-logicielles-et-documentations)
4. [Langage HTML](chap_8_1_HTML.md)
5. [Langage CSS](chap_8_1_CSS.md)


## Le Web

### Vue d'ensemble

>Le __Web__ ( _World Wide Web_ ou _toile d'araignée mondiale_) est un système permettant de consulter avec un __navigateur__ des __pages__ ou des __ressources__ mises à disposition par des __serveurs__.  
Les __pages__ peuvent contenir des __liens hyper-textes__ : d'où le nom de toile d'araignée (représentation de l'ensemble des liens).

Les __pages Web__ sont des fichiers textes hébergées sur des __serveurs web__ (programme fonctionnant sur des machines que par extension on appelle aussi __serveur__).  
Elles sont __interprétées__, c'est à dire lues et mises en forme, par des __navigateurs__ (Firefox, Chrome, Opera, ..) qui sont dits être des __clients__ (le client demande au serveur un ressource et celui-ci lui fournit).

![illustration client-serveur](img/client-server.png)

Le __Web__ est basé sur :

* le _protocole_ __[HTTP ou HTTPS](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol)__ (Hypertext Transfer Protocol) qui est utilise pour la communication entre client et serveur ;
* les __[URL](https://fr.wikipedia.org/wiki/Uniform_Resource_Locator)__ (Uniform Ressource Locator) qui est une  chaîne de caractères décrivant l'emplacement de la ressource ;
* le _langage_ __HTML__ qui décrit le contenu d'un document HTML et qui sera interprété par le navigateur.

Une page web peut être __statique__ ou __dynamique__ : 

* une page web est _statique_ lorque le contenu et la mise en forme sont fixés une fois pour toute. Toute personne demandant cette page recevra la même chose ;
* une page web est _dynamique_ lorsqu'elle est générée à la demande et que son contenu varie en fonction des caractéristiques de la demande (heure, adresse IP de l'ordinateur du demandeur, formulaire rempli par le demandeur, etc.) qui ne sont connues qu'au moment de sa consultation. (source Wikipédia)

_Remarque_ : d'autres langages viennent en complément pour pouvoir générer des pages _dynamiques_ : 

* __Javascript__ : Le Javascript sert principalement à agir sur le comportement du navigateur. Avec ce langage, vous pourrez par exemple afficher une fenêtre d’alerte lorsqu’on clique sur un lien, mettre le site web dans les favoris, imprimer la page …

* __PHP__ : Le PHP est un langage très complet avec de nombreuse fonctions. Il permet de faire le lien entre votre site et une base de données, d’afficher son contenue de façon dynamique, de gérer des variables, de traiter des formulaires …

* __SQL__ : SQL est le langage pour agir sur vos base de données. Si le PHP fais le lien entre elles et votre site web, il vous faudra coder en SQL pour effectué des actions (ajouter, supprimer, modifier, trier, rechercher …) sur celles-ci.


### Le Web n'est pas Internet

__Internet__ est l'ensemble du réseau permettant les communications.

Le __Web__ n'est qu'un des usage d'internet et on peut en citer d'autres comme :

* le courrier électronique ;
* le partage de fichiers (ftp) ;
* la connexion à distance (ssh) ;
* ...

## La naissance du Web

__En résumé__ :

>Le “World Wide Web”, plus communément appelé “Web” a été développé au CERN (Conseil Européen pour la Recherche Nucléaire) par le Britannique Sir Timothy John Berners-Lee et le Belge Robert Cailliau au début des années 90. À cette époque les principaux centres de recherche mondiaux étaient déjà connectés les uns aux autres, mais pour faciliter les échanges d’information Tim Berners-Lee met au point le système hypertexte. Le système hypertexte permet, à partir d’un document, de consulter d’autres documents en cliquant sur des mots clés. Ces mots “cliquables” sont appelés hyperliens et sont souvent soulignés et en bleu. Ces hyperliens sont plutôt connus aujourd’hui sous le simple terme de “liens”. (source : [Lycée des Flandes](https://qkzk.xyz)

__A écouter et lire__ :

* [Capsule audio à écouter](audio/le_web_tete_carre.mp4) (source : [France-Inter](https://www.franceinter.fr/emissions/l-edito-carre/l-edito-carre-12-mars-2019))

* [Article sur l'histoire du web](https://home.cern/fr/science/computing/birth-web/short-history-web) sur le site du CERN

__Image du premier serveur web au CERN__ :

<img  src="img/premier_serveur_cern.jpg" width="600">


_En complément_ :

* [autre article](https://interstices.info/les-debuts-du-web-sous-loeil-du-w3c/#1) sur le site [interstices](https://interstices.info)

* [vidéo sur la naissance du web](https://player.vimeo.com/video/207612470?color=f18215&amp;api=1&amp;player_id=video_Player_0) (source : [openclassrooms](https://openclassrooms.com/courses/connecter-le-reseau/assistez-a-la-naissance-du-web#/id/r-4344123)


