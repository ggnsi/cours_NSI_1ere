# Chapitre 8_1 : [HTML/CSS](./chap_8_1_html_css)

## Ressources logicielles et documentations

### Documentation

Quelques sites de référence sur lesquels vous pouvez trouver de la documentation, des tutoriels (et sinon, un recherche sur le web vous aidera à trouver des informations) :

* [http://www.w3schools.com/](http://www.w3schools.com/)
* [Référence HTML](https://developer.mozilla.org/fr/docs/Web/HTML/Reference) sur [developer.mozilla.org](https://developer.mozilla.org/fr)
* [Référence CSS](https://developer.mozilla.org/fr/docs/Web/CSS/Reference) sur [developer.mozilla.org](https://developer.mozilla.org/fr)
* [site de M.Willm](http://numerique.ostralo.net/html_css/)
* [tutoriel HTML](https://developer.mozilla.org/fr/docs/Learn/Getting_started_with_the_web) sur developer.mozilla.org.

### Logiciels utilisés

Nous utiliserons :

* Notepad ++ qui est installé sur les machines pour éditer les fichiers ;
* Firefox pour visualiser les fichiers produits.

## Cours et travail

* [Cours - introduction](./chap_8_1_html_css_cours.md)
* [Travail sur le HTML](./chap_8_1_HTML.md)
* [Travail sur le CSS](./chap_8_1_CSS.md) et un [court memento sur les propriétés CSS](./memento_css_3_gg_eleves.pdf)

Pour ceux qui veulent aller plus loin, vous pouvez par exemple travailler le cours situé sur [openclassroom](https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3)
