## Les listes : partie 1
---

* [Cours : généralités sur les listes](./4_1_les_listes_generalites.md)
* [TD sur les généralités sur les listes](./TD/4_1_TD_listes_generalites.md) et des [corrigés](./TD/corriges/corrige_td_generalites)
* [Cours : parcours de listes](./4_1_parcourir_une_liste.md) 
* [TD : parcours de listes](./TD/4_1_TD_listes_parcours_simples.md) et [des corrigés](./TD/corriges/corrige_td_parcours_listes)
* [Cours : méthodes sur les listes](./4_1_methodes_sur_les_liste.md)
* [TD : méthodes sur les listes](./TD/4_1_TD_listes_methodes.md) et des [corrigés](./TD/corriges/corriges_td_methodes)
* [Exercices de la BNS](./TD/4_1_TD_listes_BNS.md) et [corrige](./TD/corriges/4_1_TD_listes_BNS_corrige.md)
* [cours : les recopies de listes : Attention !](./4_1_recopie_de_liste.md)
* [mini projet pendu](./mini_projet_pendu/21_22_pendu_guide_guide.md)


