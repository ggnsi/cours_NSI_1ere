# Les listes en Python : premières notions

## Introduction

La sauvegarde de plusieurs valeurs nécessite autant de variables (et de noms de variables) que de valeurs. Cela peut être lourd (imaginez passer 30 variables en paramètre à une fonction).

De plus, il est de nombreuses situations où l'on ne connaît pas à l'avance le nombre de valeurs qui devront être traitées : imaginons une fonction qui calcule la moyenne des notes d'une classe.  
Chaque classe ayant un nombre d'élève différent, il n'est pas possible de savoir à l'avance le nombre de variables nécessaires.

Il serait plus intéressant de pouvoir regrouper ces valeurs dans une même structure associée à un seul nom de variable et dont les éléments seraient facilement accessibles.

C'est l'idée des _tableaux_ en informatique.

_Exemple_ : `tableau_notes = [10, 15, 7]`  (rien n'oblige à avoir les éléments ordonnées par ordre croissant !)

### En python

> En _Python_ , on parle de _listes_ et non de _tableaux_ . Pourquoi ?
>
>En informatique, les tableaux sont des structures ayant une taille (un nombre d'éléments) fixe.  
Ce n'est pas le cas en Python : on pourra augmenter ou réduire la taille de la liste. La structure n'est donc pas la même, le nom n'est pas le même.

_Remarque_ :

Dans de nombreux langages, le type des éléments contenus dans un tableau est défini dès le départ (tableau d'entiers, de chaînes de caractèrs, etc.).  
En Python, une liste peut contenir des éléments de types différents.

_Exemple_ : `ma_liste= [3, "Bonjour", True]`

>Le type d'une liste est `list`.

```Python
type (ma_liste)
<class 'list'>
```


## Comment définir une liste ?
---

>Pour définir une liste vide :

``` Python
ma_liste = []
```

>Pour définir une liste par énumération explicite des éléments, il suffit de les écrire entre crochets 

```Python
ma_liste = [3, 12, 5]
ta_liste = ["Rifi", "Fifi", "Loulou", "Le grand méchant loup"]
```

_Remarque_ : 

* pour plus de lisibilité dans le code, on laisse un espace après la virgule.
* En général, on écrit une expression Python sur une seule ligne. Une exception existe pour les listes (entre autres) et il est possible de l'écrire sur plusieurs lignes.  
_Exemple_ [^1] :

```Python
>>> ['janvier', 'février', 'mars', 'avril',
     'mai', 'juin', 'juillet', 'août',
     'septembre', 'octobre', 'novembre', 'décembre']
['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre']
```

>Pour définir une liste à partir d'un itérable (cad pour l'instant à partir d'une chaîne de caractère ou de l'itérable généré par `range()`, on utilisera la _fonction_ `list(itérable)`.


```Python
>>> list (range(5))
[0, 1, 2, 3, 4]

>>> list ('Bonjour')
['B', 'o', 'n', 'j', 'o', 'u', 'r']
```


## longueur d'une liste
---

>La taille d'une liste s'obtient par la fonction `len()`

_Exemple_ : 

```Python
>>> tableau_notes = [10, 15, 7]
>>> len(tableau_notes)
3
```

## Test d'appartenance à une liste
---

> `(elt in liste)` renvoie `True` si `elt` est un des élement de la liste, `False` sinon.

_Exemple_ : 

```Python
>>> 10 in tableau_notes
True

>>> 8 in tableau_notes
False
```

## Accés aux éléments d'une liste
---

> **Le principe est le même que celui utilisé pour les chaînes de caractères**.

> Tout élément d'une liste possède un indice.  
> Le premier élément est d'indice 0.
>
> On accède à une valeur d'un tableau en utilisant son indice.

_Exemple_ :

```python
    liste = [4, 7, 12]
```

Cela donne :  

| liste | 4 | 7 | 12 |
|:--------|:---:|:---:|:--:|  
| indice  | 0 | 1  | 2  |

```python
    >>> liste = [4, 7, 12]
    >>> liste[1]
    7
```

> On peut également utiliser des indices négatifs.
>
> L'indice $-1$ correspond au dernier élément de la liste.

Cela donne :  

| liste | 9 | 7 | 12 |
|:--------|:---:|:---:|:--:|  
| indice  | -3 | -2  | -1  |

```python
    >>> liste[-2]
    7
```

__Attention__ :

* avoir en tête que le dernier élément de la liste est d'indice `len(liste) - 1`

La longueur de la liste précédente est de 3 et son dernier élément est d'indice 2.

* _erreur classique_ : essayer d'atteindre un indice hors des valeurs permises.

_Exemple_ : 

```python
    >>> liste = [ 8, 10, 12]
    >>> liste[3]
       Traceback (most recent call last):
       File "<pyshell>", line 1, in <module>
       IndexError: list index out of range
```

## Une liste est mutable
---

> Cela signifie que l'**on peut modifier la valeur des éléments d'une liste.**
>
> Pour modifier l'élément d'indice `i` de la liste, on affecte une nouvelle valeur à `liste[i]`

_Exemple_ :

```python
    >>> liste = [4, 7, 12]
    >>> liste[2] = 25
    >>> liste
    [4, 7, 25]
```

```python
>>> ma_liste = ["toto", "bonjour", "écran", 'fumée']
>>> ma_liste[0] = 'maison'
>>> ma_liste
['maison', 'bonjour', 'écran', 'fumée']
```

## Opérations sur les listes
---

### Concaténation (opérateur `+`) : `liste_1 + liste_2`

_Exemple_ : 

```Python
>>> l_1 = [2, 4, 6]
>>> l_2 = [7, 8, 9]
>>> l_1 + l_2
[2, 4, 6, 7, 8, 9]
```

Le résultat est __une nouvelle liste__ constituée des éléments de la première suivi de ceux de la deuxième. (les listes `l_1` et `l_2` ne sont pas modifiées.)

### Concaténation multiple (opérateur `*`) : `n * liste_1`

_Exemple_ : 
```Python
>>> l1 = [3, 5]
>>> l1 * 4  # ou 4*l1
[3, 5, 3, 5, 3, 5, 3, 5]
```

Le résultat est une nouvelle liste, résultat de la concaténation de $n$ `liste_1`.

> Cette opération est pratique pour définir et initialiser une liste de taille donnée.

_Exemple_ : pour définir une liste de taille 5 avec des zéros :

```Python
>>> l_1 = [0] * 5
>>> l_1
[0, 0, 0, 0, 0]
```

[^1] https://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/seq5_listes.html
