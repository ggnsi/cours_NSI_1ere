## Danger lors de la recopie d'une liste
---

Il est possible de recopier une liste mais cela peut comporter des dangers qu'il faut connaître.

Un exemple pour comprendre le danger puis l'explication :

```Python
>>> l_1 = [2,3]
>>> l_2 = l_1 # on recopie la liste l_1 dans l_2
>>> l_2
[2, 3]
>>> l_1[0] = 5 # changeons le premier élément de l_1
>>> l_2 # problème : le premier élément de l_2 a aussi été modifié !!
[5, 3]
```

Pour bien comprendre, ce qui se passe, exécuter pas à pas les différentes lignes en ayant suivi le lien : [Python Tutor](http://pythontutor.com/visualize.html#code=l_1%20%3D%20%5B2,3%5D%0Al_2%20%3D%20l_1%0Aprint%28l_2%29%0Al_1%5B0%5D%20%3D%205%0Aprint%28l_2%29%0A&cumulative=false&curInstr=5&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) (appuyer sur la flèche `first` pour revenir au début du code).

Lors de l'évaluation de `l_2 = l_1`, Python ne recopie pas ici le contenu de la liste `l_1` mais associe au nom de variable `l_2` le même emplacement mémoire que celui du contenu de `l_1`.  
Ainsi, `l_1` et `l_2` pointent vers le même objet.  
Toute modification de cet objet est donc 'globale'.

> **Le même phénomène se passe lorsque l'on passe une liste en paramètre dans une fonction**

Considérons la fonction suivante qui prend une liste en argument et lui ajoute l'élément `3` à la fin :

```Python
def modif(liste):                              
    liste.append(3)
```



```python
def modif(liste):       #lors de l'appel de la fonction, une variable locale liste est
                        #créée et pointe vers la liste contenue dans l_1
    liste.append(3)     #on modifie en place liste. On modifie donc aussi l_1

l_1 = [1, 2]
modif(l_1)
print(l_1)
```

    [1, 2, 3]
    

On observe ci-dessus que la liste `l_1` a été modifiée.
En effet, comme on peut l'observer en  [exécutant le code pas à pas dans Python Tutor](http://pythontutor.com/visualize.html#code=def%20modif%28liste%29%3A%20%20%20%20%20%20%20%23lors%20de%20l'appel%20de%20la%20fonction,%20une%20variable%20locale%20liste%20est%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%23cr%C3%A9%C3%A9e%20et%20pointe%20vers%20la%20liste%20contenue%20dans%20l_1%0A%20%20%20%20liste.append%283%29%20%20%20%20%20%23on%20modifie%20en%20place%20liste.%20On%20modifie%20donc%20aussi%20l_1%0A%0Al_1%20%3D%20%5B1,%202%5D%0Amodif%28l_1%29%0Aprint%28l_1%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) :

Lors de l'appel de la fonction, une variable locale `liste` est crée et cette variable pointe vers le contenu de `l_1`.
Les modifications effectuées sur `liste` sont donc également appliquées à `l_1`.

On parle **d'effet de bord**.

> On dit qu'il y a un `effet de bord` dans une fonction, lorsqu'il y a une modification permanent de l'environement extérieur à la fonction après l'a fin de l'exécution de celle-ci.
> Par exemple lorsqu'une variable extérieure à la fonction est modifiée.

### Modification en place ou création d'une nouvelle liste ?

* la concaténation de liste crée une nouvelle liste ;
* l'utilation des méthodes `pop()`, `append()` et `extend()` modifient en place la liste

Exemples :


```python
l_1 = [1, 2]
l_2 = [3, 4]
l_3 = l_1 + l_2 #une nouvelle liste est crée et affectée à l_3
print(l_3)

l_1.append(7)
print(l_1)
```

### Pour aller plus loin : Comment faire alors si j'ai besoin d'une copie d'une liste

Une copie _superficielle_ peut s'obtenir :

- soit en utilisant `liste_2 = liste_1.copy()` ;
- soit en utilisant `liste_2 = liste_1[:]`.

La copie est _superficielle_ , c'est à dire qu'une nouvelle liste est crées. Mais si la liste d'origine contenait elle même des listes (ou des objets mutables), le même phénomère que ci-dessus peut se produire.

Deux exemples avec `Python Tutor` pour bien comprendre :

- exemple 1 : [utilisation de la méthode copy sur une liste sans itérables](http://pythontutor.com/visualize.html#code=liste_1%20%3D%20%5B1,%202,%203,%204%5D%0Aliste2%20%3D%20liste_1.copy%28%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)
- exemple 2 : [utilisation de la méthode copy sur une liste avec itérable (ici une liste) et mise en évidence du problème](http://pythontutor.com/visualize.html#code=li%20%3D%20%5B8,%209%5D%0Aliste_1%20%3D%20%5B1,%202,%203,%20li%5D%0Aliste_2%20%3D%20liste_1.copy%28%29%0Ali%5B0%5D%20%3D%2011%0Aprint%28liste_2%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)  
Ici, liste_1 et liste_2 contiennent une liste comme élément

