## Méthodes sur les listes

---

Les listes sont des **objets** sur lesquels on peut appliquer des **méthodes**.

> __Important__ :
>
> Les listes sont modifiées _en place_ par les méthodes suivantes.
>
> C'est à dire qu'on ne crée pas une nouvelle liste, mais que l'on modifie la liste actuelle

Prenez le temps de parcourir chacune des méthodes ci-dessous : elles vous seront très utiles 

### Ajouter une élément : `liste.append(elt)`, `liste.extend(iterable)`, `list.insert(indice, elt)`

| méthode | résultat |
|:-------:|:--------:|
|liste.append(elt) | Modifie `liste` en ajoutant `elt` en fin de liste. |
|liste.extend(iterable) | Modifie `liste` en ajoutant les éléments de `iterable` en fin de liste. |
|liste.insert(indice, elt) | Modifie la liste insérant `elt` à l'indice `indice` (et donc décale les suivants).|

_Exemples_ :

```Python
>>> liste = [1,2,3,4]
>>> liste.append(5)
>>> liste
[1, 2, 3, 4, 5]
```

```Python
>>> liste = [1,2,3,4]
>>> liste.extend('bonjour')
>>> liste
[1, 2, 3, 4, 'b', 'o', 'n', 'j', 'o', 'u', 'r']
```

```Python
>>> liste = [1,2,3,4]
>>> liste_2 = [5, 6]
>>> liste.extend(liste_2)    #équivalent de liste + liste_2
>>> liste
[1, 2, 3, 4, 5, 6]
```

```Python
>>> liste = [1,2,3,4]
>>> liste.insert(2,5)
>>> liste
[1, 2, 5, 3, 4]
```

### Supprimer un élément : `liste.pop()`, `liste.pop(i)`, `del(liste[i])`, `list.remove(elt)`

| méthode | résultat |
|:-------:|:--------:|
|liste.pop() | Modifie liste en supprimant le dernier élément et renvoie cet élément. |
|liste.pop(i) | Modifie liste en supprimant le i-ième élément et renvoie cet élément. |
|liste.remove(elt) | Modifie la liste en supprimant la première occurence de elt.|

_Exemple_ :

```Python
>>> liste = ["a", "b", "c", "d", "e", "f"]
>>> liste.pop()
'f'
>>> liste
['a', 'b', 'c', 'd', 'e']

>>> liste.pop(2)
'c'
>>> liste
['a', 'b', 'd', 'e']
```

```Python
liste = [2, 3, 4, 6, 3, 1]
>>> liste.remove(3)
>>> liste
[2, 4, 6, 3, 1]

>>> l.remove(7)  # Attention : une exception est levée si elt n'apparait pas dans la liste
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
ValueError: list.remove(x): x not in list
```

> La _fontion_ `del(liste[indice])` (ce n'est pas une méthode) supprime l'élément d'indice donné de la liste

```Python
>>> liste = ["a", "b", "c", "d", "e", "f"]
>>> del(liste[2])
>>> liste
['a', 'b', 'd', 'e', 'f']
```

## Après avoir fait les exercices : d'autres méthodes intéressantes rencontrées

| méthode | utilité |
|:--------|:--|
| sort() | Trie une liste selon les valeurs du codage des éléments |
| index(élément) | Renvoie l'indice de l'élément passé en paramètre et provoque une erreur si l'élément n'est pas dans la liste |  
| reverse() | Inverse les éléments de la liste |  
| split(élément séparateur) | Méthode appliquée à une chaine de caractères. Elle coupe la chaine dès qu'elle rencontre l'élément séparateur. Elle stocke les éléments dans une liste. |

[^1] https://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/seq5_listes.html
