# TD - les listes : partie 1
---

## Questions d'examen

__Conseil__ : si vous avez un doute sur certains algorithmes, les faire fonctionner sur des listes simples.

__Question 1__ :

On considère le script suivant :

```Python
t = [2, 8, 9, 2]
t[2] = t[2] + 5
```
Quelle est la valeur de t à la fin de son exécution ?

Réponses :

B- [2, 8, 14, 2]


__Question 2__ :

Après l'affectation suivante :

```Python
alphabet = [	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'  ]
```
Quelle est l'expression qui permet d'accéder à la lettre E ?

Réponses :

C- alphabet[4]


__Question 3__ :

On définit : `L = [10,9,8,7,6,5,4,3,2,1]`. Quelle est la valeur de `L[L[3]]` ?

Réponses :

A- 3 car `L[3] = 7` et `L[7] = 3`


__Question 4__ :

On définit en Python la fonction suivante :

```Python
def f(L):
	U = []
	for i in L:
		U.append(i**2 - 1)
	return U
```

Que vaut `f([-1, 0, 1, 2])` ?

Réponses :

C- [0, -1, 0, 3]

__Question 5__ :

Parmi les scripts suivants, un seul ne permet pas de générer le tableau `[0,2,4,6,8,10,12,14,16,18]` noté T.

Quel est ce script fautif ?

Réponses :


D-

```Python
T = [0] * 10
for k in range(0):
	T[k+1] = 2*T[k]
```

A cause du `range(0)`

__Question 6__ :

L est une liste d'entiers. On définit la fonction suivante :

```Python
def f(L):
  m = L[0]
  for x in L:
    if x > m:
      m = x
  return m
```
	

Que calcule cette fonction ?

Réponses :

A- le maximum de la liste L passée en argument

	

__Question 7__ :

On dispose d'une liste L d'entiers rangés en ordre croissant.

On désire connaître le nombre de valeurs distinctes contenues dans cette liste.

Parmi les quatre fonctions proposées, __lesquelles ne donne pas__ le résultat attendu ? ( _plus d'une réponse possible_ )


Réponses : C et D ne donnent pas le résultat attendu : Il  manquera 1 à la valeur retournée par ces fonctions.


C-

```Python
def nombreDistincts(L):
		n = 0
		for i in range(0,len(L)-1):
			if L[i] != L[i+1]:
				n = n + 1
		return n
```

D-

```Python
def nombreDistincts(L):
		n = 0
		for i in range(1,len(L)):
			if L[i] != L[i-1]:
				n = n + 1
		return n
```


		

__Question 8__ :

On définit en Python la fonction suivante :

```Python
def f(L):
	S = []
	for i in range(len(L)-1):
		S.append(L[i] + L[i+1])
	return S
```

		

Quelle est la liste renvoyée par `f([1, 2, 3, 4, 5, 6])` ?

Réponses :

C- [3, 5, 7, 9, 11]



__Question 9__ :

 Laquelle de ces listes de chaînes de caractères est triée en ordre croissant ?

Réponses :

A- ["112", "19", "27", "45", "8"]


__On compare d'abord les premiers caractères, puis les deuxièmes, etc__

__Question 10__ :

On définit la fonction suivante qui prend en argument un tableau non vide d'entiers.

```Python
def f(T):
    s = 0
    for k in T:
        if k == 8:
            s = s+1
    if s > 1:
        return True
    else:
        return False
```

		

Dans quel cas cette fonction renvoie-t-elle la valeur True ?

Réponses :


B- dans le cas où 8 est présent au moins deux fois dans le tableau T


