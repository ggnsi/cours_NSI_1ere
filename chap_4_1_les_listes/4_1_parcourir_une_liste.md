## Parcourir les éléments d'une liste
---

**Les deux méthodes possibles reprennent celles utilisées sur les chaînes de caractères**

### En parcourant via les indices

* le premier élément de la liste a pour indice 0 ;
* le dernier élément de la liste a pour indice `len(liste) - 1`

On va :

* utiliser une variable `indice` qui prendra comme valeur les entiers allant de 0 à `len(liste) - 1` (`for indice in range(len(liste))` ;
* pour chaque valeur, accéder au caractère via `liste[indice]`


```python
liste = [4, 7, 9, 12, 14, 15]
longueur = len(liste)
for indice in range(longueur):
    element = liste[indice]
    print(element)
```

    4
    7
    9
    12
    14
    15
    

### Parcourir la liste avec la boucle `for elt in liste`

> A chaque nouveau tout de boucle, la variable `elt` contiendra l'élément suivant de chaîne.

_Exemple_ : on affiche ici tous les éléments de la liste


```python
liste = [4, 7, 9, 12, 14, 15]
for elt in liste:
    print(elt)
```

    4
    7
    9
    12
    14
    15
    
