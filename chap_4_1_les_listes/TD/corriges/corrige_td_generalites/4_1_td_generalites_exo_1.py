# question 1
[ ]
# question 2
[0, 1, 2, 3, 4]
# question 3
L[2]
# question 4
L[3] = 'toto'
# question 5
[0] * 5
[0] + [0] + [0, 0, 0]
# question 6
1. ça dépend de ce que contient la variable a
2. True
3. False
4. True


