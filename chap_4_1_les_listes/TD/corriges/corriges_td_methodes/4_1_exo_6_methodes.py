def separe(liste):
    """Affiche à l'écran deux listes, l'une constituée des nombres pairs de liste, l'autre des nombres
    impairs
    
    param liste : (list of int)
    valeur retournée : aucune
    effet de bord : affichage à l'écran
    """
    
    l_pair = []
    l_impair = []
    for elt in liste:
        if elt % 2 == 0: #si elt est pair
            l_pair.append(elt)
        else:
            l_impair.append(elt)
    print(l_pair)
    print(l_impair)

    

    