# principe de l'algorithme :

# parcourir la liste de départ du dernier élément jusq'au premier
# Ajouter chaque élément à la liste de sortie

def verlan(liste):
    """Renvoie une nouvelle liste contenant les éléments de la `liste` d'origine en ordre inversé

    param l_eff : (list )
    valeur retournée : (list)
    
    Exemple:
    >>> verlan([1, 2, 3, 4, 5])
    [5, 4, 3, 2, 1]
    """
    
    l_sortie = []
    longueur = len(liste)
    for indice in range(longueur - 1, -1, -1): #première option ici : utiliser les possibilités de la fonction range
        l_sortie.append(liste[indice])
    return l_sortie

# autre méthode de parcourir la liste de droite à gauche
# prendre le temps de voir comment cela fonctionne, quitte à le faire tourner sur l'exemple à la main

def verlan_2(liste):
    """Renvoie une nouvelle liste contenant les éléments de la `liste` d'origine en ordre inversé

    param l_eff : (list )
    valeur retournée : (list)
    
    Exemple:
    >>> verlan_2([1, 2, 3, 4, 5])
    [5, 4, 3, 2, 1]
    """
    
    l_sortie = []
    longueur = len(liste)
    for indice in range(longueur): #première option ici : utiliser les possibilités de la fonction range
        l_sortie.append(liste[longueur - 1 - indice])
    return l_sortie


if __name__ == '__main__':
    import doctest
    doctest.testmod()
    

    