# principe de l'algorithme :

# l'élément d'indice 0 de l_eff correspond au nombre de 0 à ajouter à la liste de sortie
# l'élément d'indice 1 de l_eff correspond au nombre de 1 à ajouter à la liste de sortie
# ..
# l'élément d'indice k de l_eff correspond au nombre de k à ajouter à la liste de sortie

# l'incide de l'élément de l_eff correspond donc au chiffre à ajouter dans la liste de sortie.
# l_eff[indice] est la quantité de fois qu'il faut ajouter le chiffre indice à la liste de sortie.

# on parcourt l_eff via les indices puis on ajoute autant de fois que nécessaire (cad l_eff[indice]) (via une boucle)
# le nombre indice à la liste de sortie

def tableau(l_eff):
    """Renvoie une liste contenant successivement autant de 0, 1, 2, 3, 4, 5, ... qu'indiqué dans l_eff

    param l_eff : (list of int >=0)
    valeur retournée : (list of int)
    
    Exemple:
    >>> l_eff = [3, 2, 1, 0, 4, 2]
    >>> tableau(l_eff)
    [0, 0, 0, 1, 1, 2, 4, 4, 4, 4, 5, 5]
    """
    

    l_sortie = []
    for indice in range(len(l_eff)):
        nb_fois_a_ajouter = l_eff[indice]
        for _ in range(nb_fois_a_ajouter):
            l_sortie.append(indice)
    return l_sortie


if __name__ == '__main__':
    import doctest
    doctest.testmod()
    

    