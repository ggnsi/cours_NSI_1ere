def increment(liste):
    """renvoie une nouvelle liste dont les éléments sont ceux de liste augmentés de 1.
    
    param liste : (list of int or float)
    valeur retournée : (list of int or float)
    
    """

    l_sortie = []
    for elt in liste:
        l_sortie.append(elt + 1)
    return l_sortie

