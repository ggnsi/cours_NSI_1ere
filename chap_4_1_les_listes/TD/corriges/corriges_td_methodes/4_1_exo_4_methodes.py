from random import randint

def l_alea(n):
    """renvoie une liste de longueur n donc les éléments
    sont les entiers compris entre 0 et 9 inclus choisis au hasard.
    
    param n : (int)
    valeur retournée : (list of int avec 0 <= int <= 9)
    
    """

    

    l_sortie = []
    for _ in range(n):
        l_sortie.append(randint(0, 9))
    return l_sortie
    
# autre version qui fonctionne mais qui travaille avec les indices
# 
#     l_sortie = [0] * n
#         for indice in range(n):
#             l_sortie[indice] = randint(0,9)
#         return l_sortie
    

