def faire_liste(nombre):
    """renvoie une liste donc les éléments
    sont les entiers compris entre 0 et nombre rangés par ordre croissant.
    
    param nombre : (int)
    valeur retournée : (list of int)
    
    Exemples :
    >>> faire_liste(5)
    [0, 1, 2, 3, 4, 5]
    >>> faire_liste(0)
    [0]
    """
    
    l_sortie = []
    for nbr in range(nombre + 1):
        l_sortie.append(nbr)
    return l_sortie

#2 list(range(nombre + 1))
    
    

