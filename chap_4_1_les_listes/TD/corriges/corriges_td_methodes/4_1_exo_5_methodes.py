def majuscule(liste):
    """Renvoie une nouvelle liste constituée de tous les éléments de liste mis en
    majuscule.
        
    param liste : (list of strt)
    valeur retournée : (list of str)
    """

    l_sortie = []
    for mot in liste:
        l_sortie.append(mot.upper())
        
        # mot_maj = mot.upper()
        #l_sortie.append(mot_maj)
    return l_sortie

    