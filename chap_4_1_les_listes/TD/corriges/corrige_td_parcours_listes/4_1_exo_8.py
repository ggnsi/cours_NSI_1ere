# raisonnements identiques à l'exercice 7.

def minimum(liste):
    """
    Renvoie le plus petit élément de liste
    
    param liste : (list of int)
    valeur renvoyée : (int)
    
    CU : len(liste) >= 1
    """
    
    mini = liste[0]
    for elt in liste:
        if elt > mini:
            mini = elt
    return mini

def minimum_2(liste):
    """
    Renvoie le plus petit élément de liste
    
    param liste : (list of int)
    valeur renvoyée : (int)
    
    CU : len(liste) >= 1
    """
    
    mini = liste[0]
    i_mini = 0
    for indice in range(len(liste)):
        element = liste[indice]
        if element > mini:
            mini = element
            i_mini = indice
    
    return ['min', mini , 'rang', i_mini]

if __name__ == '__main__':
    import doctest
    doctest.testmod()