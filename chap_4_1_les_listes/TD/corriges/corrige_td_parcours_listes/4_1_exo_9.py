def moyenne(liste):
    """Retourne la moyenne des éléments de la liste
    
    param liste : (list of int or float)
    valeur retournée : (int or float)
    
    CU : len(liste) >= 1
    
    Exemples :
    
    >>> moyenne([-2, 7, 1, 3, 7, 8])
    4.0
    >>> moyenne([3, 3, 3, 3, 3])
    3.0
    """
    
    somme = 0
    for elt in liste:
        somme = somme + elt
    
    return somme / len(liste)

if __name__ == '__main__':
    import doctest
    doctest.testmod()