def increment(liste):
    """incrémente tous les élémens de liste de 1

    param liste : (list of int or float)
    valeur retournée : (list of int or float)
    
    Exemples :
    >>> increment([1, 2, 3, 4])
    [2, 3, 4, 5]
    >>> increment([])
    []
    """
    
    l_sortie = []
    for elt in liste:
        l_sortie.append(elt + 1)
    return l_sortie
    

if __name__ == '__main__':
    import doctest
    doctest.testmod()