def total(liste):
    """Renvoie le montant total du ticket de caisse où liste représente les quantités
    et l_prix les prix correspondants de chaque produit
    
    param liste : (list of int)
    valeur retournée : int
    
    Exemple :
    >>> total([1,2,0,2,3])
    35
    """
    
    somme = 0
    l_prix = [4, 7, 2, 1, 5]
    for indice in range(len(liste)):
        somme += l_prix[indice] * liste[indice]
    return somme
    

if __name__ == '__main__':
    import doctest
    doctest.testmod()