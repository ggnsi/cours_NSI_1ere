# première idée : on parcourt TOUTE la liste (obligé pour avoir la dernière occurence)
# si on rencontre *entier*, on modifie l'indice de l'occurence (ainsi, i_occurence contiendra bien
# l'indice de la dernière occurence rencontrée).
# on initialise i_occurence à -1, ainsi, si entier n'est pas rencontré, on renverra bien -1

def rang(liste, entier):
    """Renvoie le rang de la dernière occurence de entier dans liste, ou -1 si entier
    n'est pas dans liste

    param liste : (list of int)
    param entier : (int)
    valeur retournée : (int)
    
    Exemples :
    >>> ma_liste = [10, 7, -1, 4, 5, 9, 12, 4, 3, 65, 0, -5, -2]
    >>> rang(ma_liste, 4)
    7
    >>> rang(ma_liste, 10)
    0
    >>> rang(ma_liste, 35)
    -1
    """
    
    i_occurence = -1
    for indice in range(len(liste)):
        if liste[indice] == entier :
            i_occurence = indice
    return i_occurence

# deuxième idée : on parcourt la liste depuis la fin et on renvoie l'indice dès que l'on trouve *entier*
# si on a fini de parcourir la liste sans avoir rencontre *entier*, alors on renvoie -1


def rang_2(liste, entier):
    """Renvoie le rang de la dernière occurence de entier dans liste, ou -1 si entier
    n'est pas dans liste

    param liste : (list of int)
    param entier : (int)
    valeur retournée : (int)
    
    Exemples :
    >>> ma_liste = [10, 7, -1, 4, 5, 9, 12, 4, 3, 65, 0, -5, -2]
    >>> rang(ma_liste, 4)
    7
    >>> rang(ma_liste, 10)
    0
    >>> rang(ma_liste, 35)
    -1
    """
    
  
    for indice in range(len(liste), -1, -1):
        if liste[indice] == entier :
            return indice
    return -1


if __name__ == '__main__':
    import doctest
    doctest.testmod()

            