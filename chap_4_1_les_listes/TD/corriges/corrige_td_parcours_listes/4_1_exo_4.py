def somme(liste):
    """Renvoie la somme des éléments de liste

    param liste : (list of int or float)
    valeur retournée : (int or float)
    
    Exemple :
    >>> somme([4,2,-1,3])
    8
    >>> somme([])
    0
    """
    
    somme = 0
    for elt in liste:
        somme += elt
    return somme
    

if __name__ == '__main__':
    import doctest
    doctest.testmod()