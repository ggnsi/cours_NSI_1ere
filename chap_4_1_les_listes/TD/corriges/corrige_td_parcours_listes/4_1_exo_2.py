def affiche_mois():
    """affiche les mois de l'année contenus dans l_mois avec le numéro du mois devant

    param : aucun
    valeur retournée : aucune
    
    effet de bord : affichage à l'écran
    
    Exemple :
    >>> affiche_mois()
    1 janvier
    2 février
    3 mars
    4 avril
    5 mai
    6 juin
    7 juillet
    8 août
    9 septembre
    10 octobre
    11 novembre
    12 décembre
    """
    
    l_mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre']
    for indice in range(len(l_mois)):
        print(indice + 1, l_mois[indice]) #attention : indice commence à 0

if __name__ == '__main__':
    import doctest
    doctest.testmod()