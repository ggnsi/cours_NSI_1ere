# Le premier algorithme écrit ci-dessous n'est pas le plus efficace mais suit l'idée suivante :
# pour chaque noimbre entre 0 et 9, on compte le nombre de fois où il apparait dans l_entree
# puis on stocke ce nombre dans la liste de sortie à la bonne place (c'est à dire l'élément
# de l_sortie d'indice nombre)

def effectif(l_entree):
    """Renvoie une liste comporant le nombre d'occurences des entiers compris entre 0 et 9 de l_entree

    param l_entree : (list of int with 0 <= int <= 9)
    valeur retournée : (list of int) with len(list) = 10
    
    Exemple :
    >>> l_entree = [0, 5, 4, 7, 1, 4, 3, 0, 5, 6, 8, 1]
    >>> effectif(l_entree)
    [2, 2, 0, 1, 2, 2, 1, 1, 1, 0]
    """

    
    l_sortie = [0] * 10
    
    for nombre in range(10):
        effectif = 0
        for elt in l_entree:
            if elt == nombre:
                effectif += 1
        l_sortie[nombre] = effectif
    return l_sortie

# plus efficace (parce que l'on ne parcourt qu'une fois l_entree) :
# on parcourt l_entree. Pour chaque valeur rencontrée, on incrémente de 1 dans l_sortie
# le nombre de fois où cette valeur a déjà été rencontrée.
# on se base sur le fait que, par exemple, le nombre de 4 rencontrés est stocké dans l'élément
# d'indice 4 de l_sortie.


def effectif_2(l_entree):
    """Renvoie une liste comporant le nombre d'occurences des entiers compris entre 0 et 9 de l_entree

    param l_entree : (list of int with 0 <= int <= 9)
    valeur retournée : (list of int) with len(list) = 10
    
    Exemple :
    >>> l_entree = [0, 5, 4, 7, 1, 4, 3, 0, 5, 6, 8, 1]
    >>> effectif(l_entree)
    [2, 2, 0, 1, 2, 2, 1, 1, 1, 0]
    """
    
    l_sortie = [0] * 10
    for nombre in l_entree:
        l_sortie[nombre] = l_sortie[nombre] + 1
    return l_sortie

if __name__ == '__main__':
    import doctest
    doctest.testmod()