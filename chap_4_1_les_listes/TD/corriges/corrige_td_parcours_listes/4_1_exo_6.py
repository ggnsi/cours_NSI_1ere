# On parcourt les éléments de la liste via les indices
# si on trouve *entier*, alors on renvoie immédiatement l'indice où l'on se situe
# si *entier* n'est pas présent, on sera arrivé en bout de boucle et on renvoie -1

def rang(liste, entier):
    """
    Retourne l'indice de la première occurence de entier dans liste
    
    param liste : (list of int)
    param entier : (int)
    valeur retournée : (int)
    
    Exemples :
    
    >>> ma_liste = [10, 7, -1, 4, 5, 9, 12, 4, 3, 65, 0, -5, -2]
    >>> rang(ma_liste, 4)
    3
    >>> rang(ma_liste, 10)
    0
    """
    for indice in range(len(liste)):
        elt = liste[indice]
        if elt == entier:
            return indice
    return -1

if __name__ == '__main__':
    import doctest
    doctest.testmod()