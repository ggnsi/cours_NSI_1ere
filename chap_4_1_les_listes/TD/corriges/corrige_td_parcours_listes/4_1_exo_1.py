def affiche_liste(liste):
    """Affiche les uns en dessous des autres les éléments de liste

    param liste : (list)
    valeur retournée : aucune
    
    effet de bord : affichage sur l'écran
    
    Exemple :
    
    >>> ma_liste = [5, 'bonjour', 'toto', True]
    >>> affiche_liste(ma_liste)
    5
    bonjour
    toto
    True
    """
    
    for elt in liste:
        print(elt)


def affiche_liste_2(liste):
    """Affiche les uns en dessous des autres les éléments de liste

    param liste : (list)
    valeur retournée : aucune
    
    effet de bord : affichage sur l'écran
    
    Exemple :
    
    >>> ma_liste = [5, 'bonjour', 'toto', True]
    >>> affiche_liste(ma_liste)
    5
    bonjour
    toto
    True
    """
    
    for indice in range(len(liste)):
        print(liste[indice])

if __name__ == '__main__':
    import doctest
    doctest.testmod()