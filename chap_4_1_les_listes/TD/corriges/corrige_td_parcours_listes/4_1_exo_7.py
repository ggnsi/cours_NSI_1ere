# On initialise le maximum comme étant le premier élément de liste
# on parcourt la liste. Si on rencontre un élément plus grand que celui dans maxi
# on en fait notre nouveau maximum

def maximum(liste):
    """
    Renvoie le plus grand élément de liste
    
    param liste : (list of int)
    valeur renvoyée : (int)
    
    CU : len(liste) >= 1
    
    >>> maximum([-2, 7, 1, 3, 7, 4])
    7
    >>> maximum([3, 3, 3, 3, 3])
    3
    >>> maximum([1, 2, 3, 4])
    4
    >>> maximum([4, 3, 2, 1, 0])
    4

    """
    maxi = liste[0]
    for elt in liste:
        if elt > maxi:
            maxi = elt
    return maxi

# ici, on est obligé de faire un parcourt via les indices et d'avoir une variable
# supplémentaire où l'on stocke l'indice où se situe la valeur maximum rencontrée.
# le principe reste identique à la fonction précédente sinon

def maximum_2(liste):
    """
    Renvoie le plus grand élément de liste
    
    param liste : (list of int)
    valeur renvoyée : (int)
    
    CU : len(liste) >= 1
    """
    maxi = liste[0]
    i_maxi = 0
    for indice in range(len(liste)):
        element = liste[indice]
        if element > maxi:
            maxi = element
            i_maxi = indice
    
    return ['max', maxi , 'rang', i_maxi]

if __name__ == '__main__':
    import doctest
    doctest.testmod()