def nb_zeros_consecutifs(liste):
    """Renvoie le plus grand nombre de zéros consécutifs rencontrés dans liste

    param liste : (list of int)
    valeur retournée : (int)
    
    Exemples :
    >>> nb_zeros_consecutifs([1,0,2,0,0,4])
    2
    >>> nb_zeros_consecutifs([1,0,0,0,2,0,0,4,0,7])
    3
    """
    
    nb_zeros_cons_max = 0
    nb_zeros_cons_actuel = 0
    
    for nombre in liste:
        if nombre == 0:  # si on rencontre un zéro
            nb_zeros_cons_actuel += 1 # on augmente le nombre de zéros consécutifs actuels rencontrés de 1
        else: # si le nombre n'est pas zéro, c'est qu'on est au bout d'une séquence de 0
            # on regarde si notre nombre de zeros consécutifs actuel est plus grand que celui déjà rencontré
            # si oui : on modifie
            if nb_zeros_cons_actuel > nb_zeros_cons_max:
                nb_zeros_cons_max = nb_zeros_cons_actuel
            # on remet à zéro le nombre de zéros consécutifs actuel rencontrés
            nb_zeros_cons_actuel = 0
    
    # on est obligé de re-tester une fois à la fin à cause de la situation où la liste se termine par une liste de zéros.
    # en effet, on actualise le nombre de zéro maximum uniquement si on rencontre un nombre non nul.

    if nb_zeros_cons_actuel > nb_zeros_cons_max:
                nb_zeros_cons_max = nb_zeros_cons_actuel
    return nb_zeros_cons_max

if __name__ == '__main__':
    import doctest
    doctest.testmod()

            