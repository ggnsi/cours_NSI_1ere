# TD - les listes : partie 1


### Exercice 1 : parcourir une liste/accéder à un élément

1. On considère la liste `l = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre']`.  
  
    Créez une fonction `affiche_mois()` et une fonction `affiche_mois2()`qui affichent dans l'ordre chaque mois de l'année précédés de leur numéro ( janvier étant le mois numéro 1).  
    On utilisera les deux modes de parcours possible.

2. a. Créer une fonction `tirage()` qui retourne une liste contenant une valeur suivi d'une couleur (couleur signifiant `pique`, `coeur`, `carreau`, `trèfle`).

      _Exemple_ : 
        
      ```Python
      >>> tirage()
      [9, 'trèfle']
      ```
        
    On pourra définir dans cette fonction une liste avec les 4 enseignes : pique, coeur, carreau, trèfle et une autre avec les 13 valeurs : As, Roi, Dame, Valet, 10, 9, 8, 7, 6, 5, 4, 3 et 2.

    b. Créer une fonction `affichage(liste)` qui affiche sous la forme d'une chaine de caractère le résultat d'un tirage (ex : "9 de trèfle").
    
    c. Créer une fonction `bataille()` qui tire deux cartes au hasard, affiche les résultats obtenus et indique si il y a `bataille` ou `pas de bataille`.

    ➽ Variante (si le reste du TD terminé) : Modifier le programme pour qu'il propose deux cartes au hasard et indique quelle carte gagnerait à la bataille.

    ➽ Variante (si le reste du TD terminé) : Utiliser les symboles ♥, ♦, ♣ et ♠. (points de code unicode respectifs : U+2764, U+2666, U+2663 et U+2660) au lieu du texte.

4. On considère une variable liste contenant des chaines de caractères (ex : `liste = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]`).  
Créer une fonction `majuscule(liste)` qui renvoie une liste2 où toutes les chaines sont en majuscule.  

   _Aide_ : penser à utiliser la méthode `upper()` sur les chaînes de caractères.

### Exercice 2 : parcourir liste avec en plus accumulateur

1. Créez une fonction `somme(liste)` où liste est une liste de nombre (`int` ou `float`) et qui retourne la somme des éléments de la liste.

    _Exemple_ : 
    
     ```Python
    >>> somme([4,2,-1,3])
    8                         # 4 + 2 + (-1) + 3 = 8 
    ```
    

2. On considère la liste suivante `l_prix = [4, 7, 2, 1, 5]` correspondant aux prix de certains produits.

    Créez une fonction `total(liste)`, où `liste` représentera les quantités de chaque produit et qui renverra le montant total du ticket de caisse.

    _Exemple_ : 

    ```Python
    >>> total([1,2,0,2,3])
    35                         # 1 * 4 + 2 * 7 + 0 * 2 + 2 * 1 + 3 * 5 = 35 
    ```

3. Soit la liste `liste = [10, 7, -1, 4, 5, 9, 12, 4, 3, 65, 0, -5, -2]`

    Créez une fonction `rang(entier)` qui renvoie le rang de la première occurence de `entier` dans la liste (ie la première fois où on rencontre `entier` dans la liste.  
    Si `entier` n'est pas présent dans la liste, c'est $-1$ qui est retourné.
    
    _Exemples_ :
    
    ```Python
    >>> rang(4)
    3
    
    >>> rang(10)
    0
    >>> rang(35)
    -1
    ```
    
    Documentez vous ensuite sur la méthode `.index()`
    
    
4. a. Créer une fonction `maximum(liste)`, où `liste` est une liste d'entiers et qui renvoie le maximum des entiers présents dans la liste.

    _Exemple_ :

    ```Python
    >>> maximum([-2, 7, 1, 3, 7, 4])
    7
    ```

    Quelques listes pour des tests : `[-2, 7, 1, 3, 7, 4]`, `[3, 3, 3, 3, 3]`,  `[1, 2, 3, 4]`, `[4, 3, 2, 1, 0]`.

   b. Modifier la fonction précédente afin qu'elle retourne une liste contenant :
   
     * en premier élément la chaîne `"max"` ;
     * en deuxième élément la valeur du maximum ;
     * en troisième élément la chaîne `"rang"` ;
     * en quatrième élément la rang du maximum.
     
     _Exemples_ :
     
     ```Python
     >>> maximum2([-2, 7, 1, 3, 7, 4])
    ['max', 7, 'rang', 1]
    >>> maximum2([3, 3, 3, 3, 3])
    ['max', 3, 'rang', 0]
    ```
    
    c. En travail personnel, on pourra faire de la même manière une fonction `minimum(liste)`

5. Créer une fonction `moyenne(liste)` où liste est une liste d'entiers et qui renvoie la moyenne des éléments contenus dans la liste.

    _Exemples_ :
    
    ```Python
    >>> moyenne([-2, 7, 1, 3, 7, 8])
    4.0
    >>> moyenne([3, 3, 3, 3, 3])
    3.0
    ```

6. Ecrire la fonction `nb_zeros_consecutifs` qui, étant donnée une liste, renvoie le nombre maximum de zéros consécutifs dans la liste.

   Par exemple, `nb_zeros_consecutifs([1,0,2,0,0,4])` renvoie 2.


7. On considère une liste de départ dont les éléments sont des chiffres compris entre 0 et 9 (inclus).

    Créer une fonction `effectif(l_entree)`, où `l_entree` est une liste et qui renverra une liste comportant les effectifs de chacun des chiffres (le nombre de fois où chaque chiffre est apparu dans `l_entree`.
    
    _Exemple_ :
    
    ```Python
    >>> l_entree = [0, 5, 4, 7, 1, 4, 3, 0, 5, 6, 8, 1]
    >>> effectif(l_entree)
    [2, 2, 0, 1, 2, 2, 1, 1, 1, 0]    # 0 est présent 2 fois, 1 est présent 2 fois, 2 n'est pas présent, 3 est présent 1 fois, etc.
    ```

### Exercice 4 : création de liste/append

1. Créer maintenant une fonction qui prend comme paramètre un entier n. La fonction retournera une liste contenant par ordre croissant les n premiers entiers naturels (n exclus). On utilisera la boucle __for__.  

2. Créez une fonction `l_alea(n)`, où $n$ est entier et qui renvoie une liste de taille $n$ contenant des entiers choisis aléatoirement et compris entre $0$ et $9$ (inclus).


3. Créez une fonction `verlan(liste)` qui renvoie un la liste d'origine inversée.

    _Exemple_ :

    ```Python
    >>> verlan([1, 2, 3, 4, 5])
    [5, 4, 3, 2, 1]
    ```

    Après avoir crée votre fonction, documentez-vous sur la méthode `.reverse()`
    

4. On dispose d'une liste de nombre entiers positifs.

    Par exemple `L1 = [32, 5, 12, 8, 3, 75, 2, 15]`

    Écrire une fonction `separe(liste)` qui analyse un par un tous les éléments de la liste pour générer puis afficher deux nouvelles listes.  
    L’une contiendra seulement les nombres pairs de la liste initiale, et l’autre les nombres impairs.

    Par exemple, si la liste initiale est `L1`, le programme devra construire puis afficher les listes `[32, 12, 8, 2]` d'une part et `[5, 3, 75, 15]` d'autre part.
    
    ```Python
    >>> separe(L1)
    [32, 12, 8, 2]
    [5, 3, 75, 15]
    ```

5. On dispose d'une liste d'effectifs `l_eff = [3, 2, 1, 0, 4, 2]`.

    Créer une fonction `tableau(l_eff)` où `l_eff` est une liste d'effectif (exemple `l_eff = [3, 2, 1, 0, 4, 2]`) et qui renvoie une liste contenant successivement autant de 0, 1, 2, 3, 4, 5 qu'indiqué dans `l_eff`.
    
    _Exemple_ :
    
    ```Python
    >>> l_eff = [3, 2, 1, 0, 4, 2]
    >>> tableau(l_eff)
    [0, 0, 0, 1, 1, 2, 4, 4, 4, 4, 5, 5]
    ```



### Exercice 5 : Modifier une liste en place

_(extraits de : "Numériques et sciences informatiques", Balabonski, Conchon, Filiâtre, Nguyen)_

__1)__ Définissez une fonction `echange(liste, i, j)` qui échange dans la liste `liste` les éléments aux indices `i` et `j`. 
Cette fonction n'a pas de valeur de retour, mais un __effet de bord__ : elle modifie la liste passée en paramètre.
Pour le vérifier créer d'abord le tableau, puis appeler la fonction et enfin évaluer de nouveau le tableau.

Exemples :

```Python
    >>> liste = [1, 2, 3, 4, 5]
    >>> echange(liste, 1, 2)
    >>> print(liste)
    [1, 3, 2, 4, 5]
    >>> liste = [1, 2, 3, 4]
    >>> echange(liste, 0, 2)
    >>> print(liste)
    [3, 2, 1, 4]
```


__2)__ Définissez une fonction `miroir(liste)` qui accepte comme paramètre une liste et le modifie pour échanger le premier élément avec le dernier, le second avec l'avant dernier etc. Vous utiliserez la fonction `echange`.
Ici encore la transformation de la liste se fera en place.

Exemples :

```Python
    >>> liste = [1, 2, 3, 4, 5]
    >>> miroir(liste)
    >>> print(liste)
    [5, 4, 3, 2, 1]
    >>> liste = [1, 2, 3, 4]
    >>> miroir(liste)
    >>> print(liste)
    [4, 3, 2, 1]
```

### Exercice 6 : analyse de code

__Sans tester avec Thonny__, pour chacun des cas suivants expliquer l'erreur commise et corriger-la:

__a)__  

```python
liste = []
liste[0] = 'a'
```

__b)__  

```python
liste = [0, 1, 2, 3]
liste = liste + 4
```


__c)__  

```python
films = ['Un nouvel espoir', "L'Empire contre-attaque", 'Le Retour du Jedi']
for i in range(4):
    print(films[i])
```

__d)__  

```python
films = ['La Menace fantôme', "L'Attaque des clones", 'La Revanche des Sith']
for film in len(films):
    print(film)
```

__e)__  

```python
films = ['Le Réveil de la Force', "Les Derniers Jedi", "L'Ascension de Skywalker"]
print("Bientôt au cinéma :",films[len(film)])
```

### Exercice 7 : mini projet : le retour du code de César

_Des docstrings complets seront insérés dans toutes les fonctions._  
_Des tests seront insérés dans les fonctions `rang()` et `decalage()`_  
_Le lancement automatique des tests en tant que module principale sera également insérée_

C'est une méthode ancienne de cryptographie qui consiste à réaliser un décalage constant dans l'ordre alphabétique. Ce mode de cryptographie a été rapidement abandonné car une fois qu'on connait la méthode, le décryptage est très simple. Cependant, vous allez dans cet exercice reproduire le code de César.  

Dans le fichier python `code_cesar.py`, vous trouverez le canevas du TD.

 1) Une constante globale nommée alphabet a été créée.

 2) Créez une fonction `rang(liste, elt)` qui retourne le rang de `elt` dans la liste `liste`. La condition d'utilisation est que `elt` soit présent dans `liste` et que la liste ne soit pas vide.
 
 _Une `docstring` complète avec exemples devra être insérée dans cette fonction._
 
  Exemple :
     
  ```Python
  >>> tab = [1, 5, 4, 8, 6]
  >>> rang(tab, 5)
  1
  >>> tab = ['r', 'g', 'v', 'e']
  >>> rang(tab, 'v')
  2
  ```
 
 
 3) Créez une fonction __decalage(n)__ qui prendra en paramètres un entier n qui représente le décalage à effectuer et la variable alphabet. La fonction retournera une liste nommée __liste_decalee__ qui aura subi un décalage de n. 
``` python
    liste_decalee = decalage(2)
    print(liste_decallee)
    ['c', 'd', ... , 'a', 'b',]
```
Plusieurs méthodes existent. Ici je vous demande : 
* vous commencerez par créer __une copie__ de la liste `ALPHABET` (accessible car variable globale) ;
* puis vous chercherez une méthode qui utilise les méthodes `pop()` et `append()` (`insert()` est également possible).  

 4) Vous allez créer une fonction `demande_mot()` qui va demander à l'utilisateur le mot à crypter uniquement composé de minuscules. Cette fonction retournera une chaine de caractères. (On se contentera d'un mot pour le moment.)

 5) Vous allez créer une fonction `cle_cryptage()` qui va demander à l'utilisateur la clé de cryptage c'est à dire ici le décalage souhaité. La fonction retournera un entier.

 6) Créer une fonction `crypte_lettre(lettre, alphabet, alphabet_decale)` qui prendra trois paramètres, la lettre à crypter, la variable alphabet et la variable liste_decalee.  
 Elle retournera la valeur crypter du caractère passé en paramètre. (On évitera  d'utiliser la méthode .index())

 7) Créer une fonction `crypte_mot(mot, alphabet, alphabet_decale)` qui renvoie le mot crypté.
 
 8) Créer une fonction `Cesar()` qui :

 - Demander à l'utilisateur la clé de cryptage.
 - Créer une liste décalée en utilisant la clé de cryptage et la constante __alphabet__.  
 - Demander un mot à l'utilisateur.  
 - Affiche le mot crypté.

9) complément : on pourra créer une fonction `est_chaine(elt)` qui teste si `elt` est bien une chaîne de caractères composée uniquement de minuscules puis l'utiliser dans la fonction `demande_mot()`
 afin de poser la question à l'utilisateur tant que la réponse n'est pas du type attendu.



## Travail personnel sur France-IOI

Les exercices du chapitre sur les listes, en particulier les exercices 5 et 8.

## Travail personnel : petit QCM

Exercice sur [DocEval](https://doceval.dgpad.net/) code nfxP

<!--Questions GENUMSI : 

146;259;275;278;279;305;324;430 -->

## Questions d'examen

__Question 1__ :

On considère le script suivant :

```Python
t = [2, 8, 9, 2]
t[2] = t[2] + 5
```
Quelle est la valeur de t à la fin de son exécution ?

Réponses :

A- [2, 13, 9, 2]

B- [2, 8, 14, 2]

C- [7, 13, 14, 7]

D- [7, 13, 9, 2] 


__Question 2__ :

Après l'affectation suivante :

```Python
alphabet = [	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'  ]
```
Quelle est l'expression qui permet d'accéder à la lettre E ?

Réponses :

A- alphabet.E

B- alphabet['E']

C- alphabet[4]

D- alphabet[5] 


__Question 3__ :

On définit : `L = [10,9,8,7,6,5,4,3,2,1]`. Quelle est la valeur de `L[L[3]]` ?

Réponses :

A- 3

B- 4

C- 7

D- 8 

__Question 4__ :

On définit en Python la fonction suivante :

```Python
def f(L):
	U = []
	for i in L:
		U.append(i**2 - 1)
	return U
```

Que vaut `f([-1, 0, 1, 2])` ?

Réponses :

A- [0, 0, 1, 3]

B- [-1, 0, 0, 3]

C- [0, -1, 0, 3]

D- [-3, -1, 1, 3] 

__Question 5__ :

Parmi les scripts suivants, un seul ne permet pas de générer le tableau `[0,2,4,6,8,10,12,14,16,18]` noté T.

Quel est ce script fautif ?

Réponses :

A-

```Python
T = []
for k in range(10):
	T.append(2*k)
```

		

B-

```Python
T = [0] * 10
for k in range(9):
	T[k+1] = 2*(k+1)
```
		

D-

```Python
T = [0] * 10
for k in range(0):
	T[k+1] = 2*T[k]
```

__Question 6__ :

L est une liste d'entiers. On définit la fonction suivante :

```Python
def f(L):
  m = L[0]
  for x in L:
    if x > m:
      m = x
  return m
```
	

Que calcule cette fonction ?

Réponses :

A- le maximum de la liste L passée en argument

B- le minimum de la liste L passée en argument

C- le premier terme de la liste L passée en argument

D- le dernier terme de la liste L passée en argument 		

__Question 7__ :

On dispose d'une liste L d'entiers rangés en ordre croissant.

On désire connaître le nombre de valeurs distinctes contenues dans cette liste.

Parmi les quatre fonctions proposées, __lesquelles ne donne pas__ le résultat attendu ? ( _plus d'une réponse possible_ )

Réponses :

A-

```Python
def nombreDistincts(L):
		n = 1
		for i in range(1,len(L)):
			if L[i] != L[i-1]:
				n = n + 1
		return n
```

		

B-

```Python
def nombreDistincts(L):
		n = 1
		for i in range(0,len(L)-1):
			if L[i] != L[i+1]:
				n = n + 1
		return n
```

		

C-

```Python
def nombreDistincts(L):
		n = 0
		for i in range(0,len(L)-1):
			if L[i] != L[i+1]:
				n = n + 1
		return n
```

		

D-

```Python
def nombreDistincts(L):
		n = 0
		for i in range(1,len(L)):
			if L[i] != L[i-1]:
				n = n + 1
		return n
```

		

__Question 8__ :

On définit en Python la fonction suivante :

```Python
def f(L):
	S = []
	for i in range(len(L)-1):
		S.append(L[i] + L[i+1])
	return S
```

		

Quelle est la liste renvoyée par `f([1, 2, 3, 4, 5, 6])` ?

Réponses :

A- [3, 5, 7, 9, 11, 13]

B- [1, 3, 5, 7, 9, 11]

C- [3, 5, 7, 9, 11]

D- cet appel de fonction déclenche un message d'erreur 

__Question 9__ :

 Laquelle de ces listes de chaînes de caractères est triée en ordre croissant ?

Réponses :

A- ["112", "19", "27", "45", "8"]

B- ["8", "19", "27", "45", "112"]

C- ["8", "112", "19", "27", "45"]

D- ["19", "112", "27", "45", "8"] 

__Question 10__ :

On définit la fonction suivante qui prend en argument un tableau non vide d'entiers.

```Python
def f(T):
    s = 0
    for k in T:
        if k == 8:
            s = s+1
    if s > 1:
        return True
    else:
        return False
```

		

Dans quel cas cette fonction renvoie-t-elle la valeur True ?

Réponses :

A- dans le cas où 8 est présent au moins une fois dans le tableau T

B- dans le cas où 8 est présent au moins deux fois dans le tableau T

C- dans le cas où 8 est présent exactement une fois dans le tableau T

D- dans le cas où 8 est présent exactement deux fois dans le tableau T 


