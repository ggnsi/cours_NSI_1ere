# Questions issues de la BNS

Les questions suivantes sont issues de la **BNS** (Banque Nationale de Sujets) constituée à partir des QCM prévus pour les épreuves qui devaient avoir lieu en fin de première pour ceux abandonnant la spécialité NSI.

__Question 1__ :

On définit en Python la fonction suivante :

```Python
def f(L):
	U = []
	for i in L:
		U.append(i**2 - 1)
	return U
```

Que vaut `f([-1, 0, 1, 2])` ?

Réponses :

A- [0, 0, 1, 3]

B- [-1, 0, 0, 3]

C- [0, -1, 0, 3]

D- [-3, -1, 1, 3] 

__Question 2__ :

Parmi les scripts suivants, un seul ne permet pas de générer le tableau `[0,2,4,6,8,10,12,14,16,18]` noté T.

Quel est ce script fautif ?

Réponses :

A-

```Python
T = []
for k in range(10):
	T.append(2*k)
```

		

B-

```Python
T = [0] * 10
for k in range(9):
	T[k+1] = 2*(k+1)
```
		

D-

```Python
T = [0] * 10
for k in range(8):
	T[k+1] = 2*T[k]
```

__Question 3__ :

L est une liste d'entiers. On définit la fonction suivante :

```Python
def f(L):
  m = L[0]
  for x in L:
    if x > m:
      m = x
  return m
```
	

Que calcule cette fonction ?

Réponses :

A- le maximum de la liste L passée en argument

B- le minimum de la liste L passée en argument

C- le premier terme de la liste L passée en argument

D- le dernier terme de la liste L passée en argument 		

__Question 4__ :

On dispose d'une liste L d'entiers rangés en ordre croissant.

On désire connaître le nombre de valeurs distinctes contenues dans cette liste.

Parmi les quatre fonctions proposées, __lesquelles ne donne pas__ le résultat attendu ? ( _plus d'une réponse possible_ )

Réponses :

A-

```Python
def nombreDistincts(L):
		n = 1
		for i in range(1,len(L)):
			if L[i] != L[i-1]:
				n = n + 1
		return n
```

		

B-

```Python
def nombreDistincts(L):
		n = 1
		for i in range(0,len(L)-1):
			if L[i] != L[i+1]:
				n = n + 1
		return n
```

		

C-

```Python
def nombreDistincts(L):
		n = 0
		for i in range(0,len(L)-1):
			if L[i] != L[i+1]:
				n = n + 1
		return n
```

		

D-

```Python
def nombreDistincts(L):
		n = 0
		for i in range(1,len(L)):
			if L[i] != L[i-1]:
				n = n + 1
		return n
```

		

__Question 5__ :

On définit en Python la fonction suivante :

```Python
def f(L):
	S = []
	for i in range(len(L)-1):
		S.append(L[i] + L[i+1])
	return S
```

		

Quelle est la liste renvoyée par `f([1, 2, 3, 4, 5, 6])` ?

Réponses :

A- [3, 5, 7, 9, 11, 13]

B- [1, 3, 5, 7, 9, 11]

C- [3, 5, 7, 9, 11]

D- cet appel de fonction déclenche un message d'erreur 

__Question 6__ :

 Laquelle de ces listes de chaînes de caractères est triée en ordre croissant ?

Réponses :

A- ["112", "19", "27", "45", "8"]

B- ["8", "19", "27", "45", "112"]

C- ["8", "112", "19", "27", "45"]

D- ["19", "112", "27", "45", "8"] 

__Question 7__ :

On définit la fonction suivante qui prend en argument un tableau non vide d'entiers.

```Python
def f(T):
    s = 0
    for k in T:
        if k == 8:
            s = s+1
    if s > 1:
        return True
    else:
        return False
```

		

Dans quel cas cette fonction renvoie-t-elle la valeur True ?

Réponses :

A- dans le cas où 8 est présent au moins une fois dans le tableau T

B- dans le cas où 8 est présent au moins deux fois dans le tableau T

C- dans le cas où 8 est présent exactement une fois dans le tableau T

D- dans le cas où 8 est présent exactement deux fois dans le tableau T 
