# TD : méthodes sur les listes

### Exercice 1 : manipulations de base

Dans cet exercice, on utilisera **uniquement** des méthodes sur la liste `ma_liste` pour la modifier **en place**.

Soit `ma_liste = [5, 4, 3, 2, 1]`

1. Ajouter l'élément `0` en fin de liste. Alors `ma_liste = [5, 4, 3, 2, 1, 0]`.
2. Supprimer le dernier élément de la liste. Alors `ma_liste = [5, 4, 3, 2, 1]`.
3. Supprimez l'élément `2` de la liste. Alors `ma_liste = [5, 4, 3, 1]`.
4. Insérez un `3` comme élément entre le `5` et le `4`. Alors `ma_liste = [5, 3, 4, 3, 1]`.
5. Sans avoir à la chercher, supprimez la première occurence de la valeur `3` dans la liste. Alors `ma_liste = [5, 4, 3, 1]`.
6. Déterminer le rang de la valeur `3` dans la liste.
7. Effectuer un tri de la liste par ordre croissant. Alors `ma_liste = [1, 3, 4, 5]`.

### Exercice 2 : manipulations de base

Soit `ma_liste = ['ba', 'b', 'bac', 'atome', 'bb', 'B', 'Ba', 'bA']`

Effectuez **en place** le tri de `ma_liste` et observez la manière dont les chaînes de caractères sont ordonnées.

### Exercice 3

1. Définissez une fonction `faire_liste(nombre)` où `nombre` est un entier positif, qui retourne une liste donc les éléments sont les entiers compris entre `0` et `nombre` rangés par ordre croissant.  
On **utilisera obligatoirement une boucle** dans cette fonction.

    _Exemple_ :
```Python
>>> faire_liste(5)
[0, 1, 2, 3, 4, 5]
>>> faire_liste(0)
[0]
```

2. Comment faire la même chose en une seule ligne (sans boucle) ?



### Exercice 4

Créez une fonction `l_alea(n)`, où `n` est entier et qui renvoie une liste de taille `n` contenant des entiers choisis aléatoirement et compris entre `0` et `9` (inclus).

### Exercice 4bis

1. Créez une fonction `increment(liste)` où `liste` est une liste de nombres (`int` ou `float`) et qui retourne une nouvelle liste dont les éléments sont ceux de `liste` augmentés de `1`.

_Exemple_ :

```Python
>>> increment([1, 2, 3, 4])
[2, 3, 4, 5]
>>> increment([])
[]
```

2. Créez une fonction `increment_2(liste)` où `liste` est une liste de nombres (`int` ou `float`) et qui modifie **en place** la liste passée en paramètre en augmentant de 2 tous ses éléments.

### Exercice 5

On considère une variable liste contenant des chaines de caractères (ex : `liste = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]`).  
1. Créer une fonction `majuscule(liste)` qui renvoie une **nouvelle** liste où toutes les chaines sont en majuscule.  

   _Aide_ : penser à utiliser la méthode `upper()` sur les chaînes de caractères.
2. Créer une fonction `majuscule(liste)` qui modifie **en place** la liste passée en paramètre en mettant tous ses éléments en majuscule.
   
### Exercice 6

On dispose d'une liste de nombre entiers positifs.

Par exemple `L1 = [32, 5, 12, 8, 3, 75, 2, 15]`

Écrire une fonction `separe(liste)` qui analyse un par un tous les éléments de la liste pour générer puis afficher deux nouvelles listes.  
L’une contiendra seulement les nombres pairs de la liste initiale, et l’autre les nombres impairs.

Par exemple, si la liste initiale est `L1`, le programme devra construire puis afficher les listes `[32, 12, 8, 2]` d'une part et `[5, 3, 75, 15]` d'autre part.
    
```Python
>>> L1 = [32, 5, 12, 8, 3, 75, 2, 15]
>>> separe(L1)
[32, 12, 8, 2]
[5, 3, 75, 15]
```

### Exercice 7

On dispose d'une liste d'effectifs `l_eff = [3, 2, 1, 0, 4, 2]`.

Créer une fonction `tableau(l_eff)` où `l_eff` est une liste d'effectif et qui renvoie une liste contenant successivement autant de 0, 1, 2, 3, 4, 5 qu'indiqué dans `l_eff`.
    
_Exemple_ :
    
```Python
>>> l_eff = [3, 2, 1, 0, 4, 2]
>>> tableau(l_eff)
[0, 0, 0, 1, 1, 2, 4, 4, 4, 4, 5, 5]
```

### Exercice 8

1. Créez une fonction `verlan(liste)` qui renvoie une nouvelle liste contenant les éléments de la `liste` d'origine en ordre inversé.

    _Exemple_ :

```Python
>>> verlan([1, 2, 3, 4, 5])
[5, 4, 3, 2, 1]
```

2. Après avoir crée votre fonction, documentez-vous sur la méthode `.reverse()`

### Exercice 9 : Modifier une liste en place

_(extraits de : "Numériques et sciences informatiques", Balabonski, Conchon, Filiâtre, Nguyen)_

1. Définissez une fonction `echange(liste, i, j)` qui échange dans la liste `liste` les éléments aux indices `i` et `j`. 
Cette fonction n'a pas de valeur de retour, mais un __effet de bord__ : elle modifie la liste passée en paramètre.
Pour le vérifier créer d'abord le tableau, puis appeler la fonction et enfin évaluer de nouveau le tableau.

_Exemples_ :

```Python
    >>> liste = [1, 2, 3, 4, 5]
    >>> echange(liste, 1, 2)
    >>> liste
    [1, 3, 2, 4, 5]
    >>> liste = [1, 2, 3, 4]
    >>> echange(liste, 0, 2)
    >>> liste
    [3, 2, 1, 4]
```


2. Définissez une fonction `miroir(liste)` qui accepte comme paramètre une liste et le modifie pour échanger le premier élément avec le dernier, le second avec l'avant dernier etc. Vous utiliserez la fonction `echange`.
Ici encore la transformation de la liste se fera en place.

_Exemples_ :

```Python
    >>> liste = [1, 2, 3, 4, 5]
    >>> miroir(liste)
    >>> liste
    [5, 4, 3, 2, 1]
    >>> liste = [1, 2, 3, 4]
    >>> miroir(liste)
    >>> liste
    [4, 3, 2, 1]
```

### Exercice 10

On considère des listes d'entiers qui n'apparaissent qu'une fois dans la liste (un entier ne peut être présent deux fois).

Créez une fonction `destruction(entier, liste)` qui **modifie liste en place** en supprimant dans `liste` l'`entier` ainsi que (si c'est possible) les deux nombres contigus.

**On considère ici qu'une condition d'utilisation de la fonction est que `entier` soit présent dans `liste`**

On pourra utiliser la méthode `index()`.

_Exemples_ :
```Python
>>> liste = [1, 4, 3, 7, 9, 2]
>>> destruction(7, liste)
>>> liste
[1, 4, 2]
>>> liste = [1, 4, 3, 7, 9, 2]
>>> destruction(1, liste)
>>> liste
[3, 7, 9, 2]
>>> liste = [1, 4, 3, 7, 9, 2]
>>> destruction(2, liste)
>>> liste
[1, 4, 3, 7]
```

---
## Des exercices non corrigés 

### Exercice +1

Écrire une fonction qui prend en paramètre deux listes d'entiers `L1` et `L2` et qui les concatène sans doublons.

_Exemple :_

Si `L1 = [13, 15, 12, 17, 15 ]` et `L2 = [18, 15, 14, 13, 19, 20]` alors le programme crée la liste `L3 = [13, 15, 12, 17, 18, 14, 19, 20]` et la renvoie.

[source](https://isn-icn-ljm.pagesperso-orange.fr/NSI_exos/co/page_les_listes.html)

### Exercice +2

La valeur la plus souvent présente dans une liste s'appelle son mode.  
Écrire une fonction  qui prend en paramètre une liste d'entiers et qui renvoie son mode.

Exemple :

Si `L = [12, 15, 13, 15, 14, 12, 15, 13]`, son mode est `15`.

### Exercice +3

Écrire une fonction qui prend en paramètre une liste `L` et qui renvoie une liste `L1` qui contienne les mêmes éléments que `L`, mais sans que ce soit des listes.

Exemple :

si `L = [1,2,[3,4],5,6,[7,[8,[9,10,[11,12],13],14,15],16,17,18],19,[20,21,[22,23,24,[25,26,27],28],[29],30],31,32]`

Alors la liste renvoyée est :

`L1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32]`

