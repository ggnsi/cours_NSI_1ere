#  TD - les listes généralités


### Exercice 1

#### Question 1

Quelle est l'instruction qui crée une liste ?  

*  `liste()`
*  `[ ]`
*  `print(liste)`
*  `{ }`

#### Question 2 

Quel est le résultat de l'expression suivante ? `[0, 1, 2, 3] + [4]`  

* `[0, 1, 2, 3, [4]]`
* `[0, 1, 2, 7]`
* `[4, 0, 1, 2, 3]`
* `[4, 1, 2, 3]`
* `[0, 1, 2, 3, 4]  `

#### Question 3 

Soit `L = ['janvier', 'février', 'mars', 'avril', 'mai']`

Quelle instruction retourne `mars` ?

* L[1]
* L[2]
* L[3]
* L[4]

#### Question 4 

En utilise la liste `L` définie à la question 3, quelle instruction écrire pour modifier `L` en `['janvier', 'février', 'mars', 'toto', 'mai']`

#### Question 5  

Quelles instructions permet de générer la liste `[0, 0, 0, 0, 0]` ? (plusieurs réponses possibles)

* `[0 * 5]`
* `[0] * 5`
* `[0] + [0] + [0, 0, 0]`
* `[0, 0, 0, 0, 0, 4] - [4]`

#### Question 6

Soit `L = ['a', 'b', 'bac', 5]`.

Que renvoient chacune des instructions suivantes ?

1. `a in L`
2. `'b' in L`
3. `'ba' in L`
4. `4 + 1 in L`


### Exercice 2

Les questions suivantes sont issues de la **BNS** (Banque Nationale de Sujets) constituée à partir des QCM prévus pour les épreuves qui devaient avoir lieu en fin de première pour ceux abandonnant la spécialité NSI.

#### Question 1

On considère le script suivant :

```Python
t = [2, 8, 9, 2]
t[2] = t[2] + 5
```
Quelle est la valeur de t à la fin de son exécution ?

Réponses :

A- [2, 13, 9, 2]

B- [2, 8, 14, 2]

C- [7, 13, 14, 7]

D- [7, 13, 9, 2] 


#### Question 2

Après l'affectation suivante :

```Python
alphabet = [	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'  ]
```
Quelle est l'expression qui permet d'accéder à la lettre E ?

Réponses :

A- `alphabet.E`

B- `alphabet['E']`

C- `alphabet[4]`

D- `alphabet[5] `


#### Question 3

On définit : `L = [10,9,8,7,6,5,4,3,2,1]`. Quelle est la valeur de `L[L[3]]` ?

Réponses :

A- 3

B- 4

C- 7

D- 8 

---
### Exercice 3

1. Créer une fonction `tirage()` qui **renvoie** une liste contenant une valeur choisie _au hasard_ suivie d'une couleur choisie elle aussi _au hasard_ (couleur signifiant `pique`, `coeur`, `carreau`, `trèfle`).

      _Exemple_ : 
        
      ```Python
      >>> tirage()
      ['9', 'trèfle']
      ```
        
    _Aide_ :  
    On pourra définir dans cette fonction une liste avec les 4 enseignes : pique, coeur, carreau, trèfle et une autre avec les 13 valeurs : As, Roi, Dame, Valet, 10, 9, 8, 7, 6, 5, 4, 3 et 2.
    
```Python
l_enseignes = ['pique', 'coeur', 'carreau', 'trefle']
l_valeurs = ['As', 'Roi', 'Dame', 'Valet', '10', '9', '8', '7', '6', '5', '4', '3', '2']
```
    

2. Créer une fonction `affichage(liste)` qui affiche sous la forme d'une chaine de caractère le résultat d'un tirage (ex : "9 de trèfle").
    
3. Créer une fonction `bataille()` qui tire deux cartes au hasard, affiche les résultats obtenus et indique si il y a `bataille` ou `pas de bataille`.  
Penser à ré-utiliser la fonction `tirage()`

    ➽ Variante (si le reste du TD terminé) : Utiliser les symboles ♥, ♦, ♣ et ♠. (points de code unicode respectifs : U+2764, U+2666, U+2663 et U+2660) au lieu du texte.

### Exercice 4 : analyse de code

__Sans tester avec Thonny__, pour chacun des cas suivants expliquer l'erreur commise et corriger-la:

__a)__  

```python
liste = []
liste[0] = 'a'
```

__b)__  

```python
liste = [0, 1, 2, 3]
liste = liste + 4
```


__c)__  

```python
films = ['Un nouvel espoir', "L'Empire contre-attaque", 'Le Retour du Jedi']
for i in range(4):
    print(films[i])
```

__d)__  

```python
films = ['La Menace fantôme', "L'Attaque des clones", 'La Revanche des Sith']
for film in len(films):
    print(film)
```

__e)__  

```python
films = ['Le Réveil de la Force', "Les Derniers Jedi", "L'Ascension de Skywalker"]
print("Bientôt au cinéma :",films[len(film)])
```
