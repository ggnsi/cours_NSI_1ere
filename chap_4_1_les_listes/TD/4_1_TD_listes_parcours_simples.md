# TD - parcours de listes

**Remarque** :
> De nombreux exemples sont inclus dans les exercices.
>
> Utilisez-le dans les docstrings afin d'utiliser le module doctest pour tester vos fonctions (enleve bien sur les éléments avec un dièse qui sont des commentaires pour vous faire comprendre certains résultats).

### Exercice 1 

Créez une fonction `affiche_liste(liste)` et une fonction `affiche_liste_2(liste)` qui affichent(en utilisant chacune l'un des deux modes de parcours possible d'une liste) les uns en dessous des autres et dans l'ordre les éléments de la liste `liste` passée en argument. 'année .

_Exemple_ :

```Python
>>> ma_liste = [5, 'bonjour', 'toto', True]
>>> affiche_liste(ma_liste)
5
bonjour
toto
True
```

### Exercice 2

On considère la liste `l_mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre']`. 

Créer une fonction `affiche_mois()` qui affiche dans l'ordre chaque mois de l'année précédés de son numéro ( _Remarque_ : la liste `l_mois` sera donc ici incluse dans la définition de la fonction)

_Exemple_ :

```Python
>>> affiche_mois()
1 janvier
2 février
3 mars
4 avril
5 mai
6 juin
7 juillet
8 août
9 septembre
10 octobre
11 novembre
12 décembre
```

### Exercice 3

**supprimé**

### Exercice 4

Créez une fonction `somme(liste)` où `liste` est une liste de nombres (`int` ou `float`) et qui retourne la somme des éléments de la liste.

_Exemple_ : 
    
```Python
    >>> somme([4,2,-1,3])
    8   # 4 + 2 + (-1) + 3 = 8
    >>> somme([])
    0
```

### Exercice 5

On considère la liste suivante `l_prix = [4, 7, 2, 1, 5]` correspondant aux prix de certains produits.

Créez une fonction `total(liste)`, où `liste` représentera les quantités de chaque produit et qui renverra le montant total du ticket de caisse.  
La liste `l_prix` sera incluse dans la définition de la fonction `total`

_Exemple_ : 

```Python
>>> total([1,2,0,2,3])
35     # 1 * 4 + 2 * 7 + 0 * 2 + 2 * 1 + 3 * 5 = 35 
```

### Exercice 6 : classique

Créez une fonction `rang(liste, entier)` qui renvoie le rang de la **première occurence** de `entier` dans la liste (ie la première fois où on rencontre `entier` dans la liste.  
Si `entier` n'est pas présent dans la liste, c'est `-1` qui est retourné.
    
_Exemples_ :
    
```Python
    >>> ma_liste = [10, 7, -1, 4, 5, 9, 12, 4, 3, 65, 0, -5, -2]
    >>> rang(ma_liste, 4)
    3
    >>> rang(ma_liste, 10)
    0
    >>> rang(ma_liste, 35)
    -1
```
    
### Exercice 7 : classique

1. Créer une fonction `maximum(liste)`, où `liste` est une liste d'entiers et qui renvoie le maximum des entiers présents dans la liste.

    _Exemple_ :

    ```Python
    >>> maximum([-2, -4, -1])
    -1
    >>> maximum([-2, 7, 1, 3, 7, 4])
    7
    >>> maximum([3, 3, 3, 3, 3])
    3
    >>> maximum([1, 2, 3, 4])
    4
    >>> maximum([4, 3, 2, 1, 0])
    4
    ```

2. Créer une fonction `maximum_2(liste)` où `liste` est une liste d'entiers et qui retourne une liste contenant :
   
     * en premier élément la chaîne `"max"` ;
     * en deuxième élément la valeur du maximum ;
     * en troisième élément la chaîne `"rang"` ;
     * en quatrième élément la rang du maximum.
     
     _Exemples_ :
     
     ```Python
     >>> maximum_2([-2, 7, 1, 3, 7, 4])
    ['max', 7, 'rang', 1]
    >>> maximum_2([3, 3, 3, 3, 3])
    ['max', 3, 'rang', 0]
    ```

### Exercice 8 : classique

Reprendre l'exercice précédent avec le minimum à la place du maximum

### Exercice 9 : classique

Créer une fonction `moyenne(liste)` où liste est une liste d'entiers et qui renvoie la moyenne des éléments contenus dans la liste.

_Exemples_ :
    
```Python
    >>> moyenne([-2, 7, 1, 3, 7, 8])
    4.0
    >>> moyenne([3, 3, 3, 3, 3])
    3.0
```

### Exercice 10

On considère une liste de départ dont les éléments sont des chiffres compris entre 0 et 9 (inclus).

Créer une fonction `effectif(l_entree)`, où `l_entree` est une liste et qui renverra une liste comportant les effectifs de chacun des chiffres (le nombre de fois où chaque chiffre est apparu dans `l_entree`.
    
_Exemple_ :
    
```Python
    >>> l_entree = [0, 5, 4, 7, 1, 4, 3, 0, 5, 6, 8, 1]
    >>> effectif(l_entree)
    [2, 2, 0, 1, 2, 2, 1, 1, 1, 0]    # 0 est présent 2 fois, 1 est présent 2 fois, 2 n'est pas présent, 3 est présent 1 fois, etc.
```

### Exercice 11

Ecrire la fonction `nb_zeros_consecutifs(liste)`, où `liste` est une liste d'entiers, qui renvoie le nombre maximum de zéros consécutifs dans la liste.

Par exemple,

```Python
>>> nb_zeros_consecutifs([1,0,2,0,0,4])
2
>>> nb_zeros_consecutifs([1,0,0,0,2,0,0,4,0,7])
3
>>> nb_zeros_consecutifs([1,0,0,0,2,0,0,4,0,0,0,0])
4
```



### Exercice 12 : classique

Créez une fonction `rang(liste, entier)` qui renvoie le rang de la **dernière occurence** de `entier` dans la liste (ie la dernière fois où on rencontre `entier` dans la liste).  
Si `entier` n'est pas présent dans la liste, c'est `-1` qui est retourné.
    
_Exemples_ :
    
```Python
    >>> ma_liste = [10, 7, -1, 4, 5, 9, 12, 4, 3, 65, 0, -5, -2]
    >>> rang(ma_liste, 4)
    7
    >>> rang(ma_liste, 10)
    0
    >>> rang(ma_liste, 35)
    -1
```
