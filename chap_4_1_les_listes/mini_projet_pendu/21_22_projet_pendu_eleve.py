import random

def choix_mot(liste_de_mots):
    """
    Choisit un mot parmi la liste_de_mots
    
    param liste_de_mots : (list of strings)
    valeur retournée : (string)
    
    """
    
    pass

def liste_lettres_mot(mot):
    """
    Constitue la liste des lettes apparaissant dans mot
    
    param mot : (string) le mot de départ
    valeur retournée liste_lettres : (list) : liste contenant les différentes lettres du mot
    
    Exemples :
    >>> liste_lettres_mot('papi')
    ['p', 'a', 'i']
    >>> liste_lettres_mot('bonbon')
    ['b', 'o', 'n']
    >>> liste_lettres_mot('bonjour')
    ['b', 'o', 'n', 'j', 'u', 'r']
    >>> liste_lettres_mot('barbapapa')
    ['b', 'a', 'r', 'p']
    """
    
    pass


def choix_joueur(lettres_jouees):
    """
    Demande au joueur sa lettre choisie jusqu'à ce qu'elle ne fasse pas partie des lettres déjà jouées puis l'ajoute à la liste
    des lettres déjà jouées.
    amélioration : tester si la joueur ne propose bien qu'un caractère
    amélioration 2 : tester si ce caractères est bien une minuscule attendue
    
    param lettres_jouees : (list) liste des lettres déjà jouées
    valeur retournée : (string) la lettre choisie
    
    effet de bord : la liste lettres_jouees est modifiée en place
    """
    
    choix_joueur_valide = False
    while not choix_joueur_valide:
        
        pass
        
    # ajout de la lettre à la liste des lettres déjà jouées
    

def indices_lettre_dans_mot(chaine, lettre):
    """
    retourne la liste des indices correspondant aux positions de la lettre donnée dans la chaine
    
    parametre chaine : (string)
    parametre lettre : (string) contenant un seul caractère
    valeur retournee : liste
    
    Exemples :
    >>> indices_lettre_dans_mot('papi', 'p')
    [0, 2]
    >>> indices_lettre_dans_mot('bonjour', 'j')
    [3]
    >>> indices_lettre_dans_mot('bonjour', 'r')
    [6]
    >>> indices_lettre_dans_mot('bonjour', 't')
    []
    """

    pass
    

def modifie_liste_lettres_trouvees(lettre, liste_places, liste_decouvert):
    """
    Modifie list decouvert en y plaçant lettre aux indices contenus dans liste_places
    
    parametre lettre : (string) de 1 caractère
    parametre liste_places : (list of int)
    parametre liste_decouvert : (list of string)
    valeur retournee : aucune
    
    Effet de bord :  liste_decouvert est modifiée en place
    
    Exemples :
    >>> l = ['*', '*', '*', '*']
    >>> modifie_liste_lettres_trouvees('p', [0, 2], l)
    >>> l
    ['p', '*', 'p', '*']
    >>> l = ['*', '*', '*', '*']
    >>> modifie_liste_lettres_trouvees('p', [], l)
    >>> l
    ['*', '*', '*', '*']
    >>> l = ['a', '*', '*', '*', 'b', '*', '*']
    >>> modifie_liste_lettres_trouvees('e', [2, 5], l)
    >>> l
    ['a', '*', 'e', '*', 'b', 'e', '*']
    """
    
    pass   

def affiche_mot_decouvert(liste_decouvert):
    """Affiche à l'écran le mot constitué de lettres qui sont présentes dans liste_decouvert avec un
     passage à la ligne à la fin.
     
    param liste_decouvert : (list of str)
    valeur retournee : aucune
    Effet de bord : affichage à l'écran
    
    Exemples :
    >>> affiche_mot_decouvert(['*', '*', '*', '*'])
    ****
    >>> affiche_mot_decouvert(['p', '*', '*', 'i'])
    p**i
    >>> affiche_mot_decouvert(['*', 'o', '*', 'j', 'o', '*', '*'])
    *o*jo**
    """

    pass
    

def pendu():
    

    liste_mots = ['lenteur', 'glacons', 'miroirs', 'obliger', 'tirades', 'mongols']
    l_lettres_jouees = []
    nb_tour = 0
    nb_tour_maxi = 10
    gagne = False

    # choix du mot
    mot_cherche = choix_mot(liste_mots)

    # juste pour la mise au point
    print('mot cherché', mot_cherche) 
    
    # initialisation liste lettres trouvees
    l_lettres_trouvees = ['*'] * len(mot_cherche)
    
    # initialisation de la liste des lettres à découvrir
    # liste_lettres_a_decouvrir = 
     
    # boucle principale

    while (not gagne) and (nb_tour < nb_tour_maxi):
        nb_tour += 1
        print('Tour n° :', nb_tour, 'sur 10')
        
        #choix de la lettre par le joueur
        lettre_joueur = choix_joueur(l_lettres_jouees) 
        
#         # test si la lettre est dans le mot
#         #if #...
#             
#             #on enlève la lettre de la liste des lettres du mot
#             
#             
#             # récupérer les indices des lettres
#             # indices_lettres = 
#             
#             
#             # on affiche le nombre d'occurences rencontrées 
#             
#             
#             # on modifie la l_lettres_trouvees
# 
#                  
#             # a-t-on gagné ?
#             if ...
#                 
#         else:
#             print("La lettre n'est pas dans le mot")
        
        # On affiche le mot avec les lettres trouvées
        

    # sortie de boucle : on teste si l'on a gagné et on effectue l'affichage en fonction 

    if gagne == True:
        print('Bravo, vous avez trouvé', mot_cherche, ' en ', nb_tour, 'tour(s)')
    else:
        print('perdu, le mot était', mot_cherche)
    
if __name__ ==  '__main__':
    import doctest
    doctest.testmod()