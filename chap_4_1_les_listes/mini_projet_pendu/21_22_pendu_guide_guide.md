# Jeu de pendu


## Cahier des charges :

Le programme doit permettre de jouer suivant les règles ci-dessous :

* Un mot est tiré au hasard parmi une liste pré-définie de mots (de longueur variable)
* A chaque tour :
    * On indique le numéro du tour ;
    * Etpae : Le joueur propose une lettre ;
        * On lui demande une lettre et si la lettre a déjà été jouée, on lui indique et il doit en proposer une autre.
        * Ceci jusqu'à ce qu'il en donne une qu'il n'a jamais déjà proposée ;
    * Etape : analyse de la lettre proposée
        * Si la lettre proposée appartient au mot, on indique au joueur le nombre d’occurrence de la lettre dans le mot (le nombre de fois où elle apparait).
        * Sinon, on indique que la lettre n'est pas dans le mot.
        * On affiche le mot cherché avec les lettres découvertes placées aux bons endroits (y compris la nouvelle lettre trouvée si c'est le cas). Les lettres non encore trouvées sont remplacées par des *
    * Si toutes les lettres ont été trouvées, on indique que le joueur a gagné.
    * Sinon, tant que le nombre maximum de tour n’a pas été atteint et que le mot n’a pas été trouvé, on recommence
    * Si le nombre de tour maximum a été atteint sans gagner, on indique que joueur qu'il a perdu et le mot à découvrir.

### Exemples de partie

```Python
Tour n° : 1 sur 10
Quelle lettre choisissez vous ? : p
Il y a 2 lettres p
p*p*
Tour n° : 2 sur 10
Quelle lettre choisissez vous ? : u
La lettre n'est pas dans le mot
p*p*
Tour n° : 3 sur 10
Quelle lettre choisissez vous ? : a
Il y a 1 lettres a
pap*
Tour n° : 4 sur 10
Quelle lettre choisissez vous ? : p
Lettre déjà jouée
Quelle lettre choisissez vous ? : i
Il y a 1 lettres i
papi
Bravo, vous avez trouvé papi  en  4 tour(s)
```

## Principe de l'algorithme utilisé ici

Plusieurs listes seront utilisées : 

* la liste des lettres jouées (`l_lettres_jouees`) qui comprendra l'ensemble des lettres déjà proposées par le joueur (exemple : `['a', 'p', 't']`)

* la liste des lettres **restant** à découvrir (`liste_lettres_a_decouvrir`).

    Au début cette liste comportera toutes les lettres du mot puis dans laquelle on supprime au fur et à mesure les lettres trouvées par le joueur.
  
  *Exemple* : Si le mot à découvrir est `'bonjour'`, `liste_lettres_a_decouvrir` contiendra initialement `['b', 'o', 'n', 'j', 'u', 'r']`.
  
  Si le joueur a trouvé la lettre `b`, la liste devra alors être `['o', 'n', 'j', 'u', 'r']`.
  

* la liste `l_lettres_mot_mystere` qui comportera initialement autant de caractères étoiles que de lettres dans le mot à découvrir puis comportera les lettres découvertes et des étoiles pour celles non découvertes.

    _Exemple_ :
  
  Si le mot à découvrir est `'bonjour'` :
  * au départ `l_lettres_mot_mystere = ['*', '*', '*', '*', '*', '*', '*']`
  * si le joueur trouve la lettre `'b'`, `l_lettres_mot_mystere` est modifiée en `['b', '*', '*', '*', '*', '*', '*']`
  * si le joueur trouve la lettre `'o'`, `l_lettres_mot_mystere` est modifiée en `['b', 'o', '*', '*', 'o', '*', '*']`
  
---

On va segmenter ce problème en plusieurs fonctions :

* une principale : la fonction `pendu()` qui lance le jeu et gère son déroulement :
    * on va y initialiser la liste des lettres jouées (`l_lettres_jouees`) comme étant une liste vide ;
    * on va y choisir au hasard le `mot` à faire deviner ;
    * on va initialiser `liste_lettres_a_decouvrir` comme étant la liste comportant toutes les lettres contenues dans `mot` ;
    * on va y initialiser une `l_lettres_mot_mystere` comportant autant d'étoiles `*` que de lettres dans le mot à faire deviner.
     * puis : 
         * on demandera au joueur une lettre (en modifiant `l_lettres_jouees`) ;
         * on étudiera si la lettre est dans `liste_lettres_a_decouvrir` et si oui, on affichera le nombre d'occurence et on  modifiera cette liste ainsi que celle `l_lettres_mot_mystere` ;
         * on réalisera l'affichage du mot (avec les `*` ou les lettres découvertes) ;
         * On étudiera si le joueur peut encore jouer ou non.  
  
  

Dit autrement :

* Tant que l'on n'a pas trouvé toutes les lettres du mot (c'est à dire que `liste_lettres_a_decouvrir` n'est pas vide) et qu'on n'a pas dépassé le nombre maximum d'essais :
    * on augmente de 1 le numéro du tour ;
    * on demande au joueur une lettre (qu'il n'a pas encore proposée)
    * on l'ajoute à `l_lettres_jouees` ;
    
    * Si la lettre proposée est dans `liste_lettres_a_decouvrir` : 
        * on supprime la lettre proposée par le joueur de `liste_lettres_a_decouvrir` ;
        * on déterminer les places de cette lettre dans le mot ;
        * on affiche le nombre d'occurences rencontrées ;  
            _Exemple_ :
            
            ```Python
            Il y a 2 lettres p
            ```

        * on actualise `l_lettres_mot_mystere` en insérant aux bon endroits la lettre à la place des étoiles ;
        * on teste si l'on a gagné (cad si la liste des lettres à découvrir est vide)
    * Sinon on affiche que la lettre n'est pas dans le mot
    * on réalise l'affichage du mot en l'état actuel des lettres trouvées (ou pas).
        _Exemple_ : `t*t*`

* On teste si on a gagné ou pas.  
Si oui, on l'affiche avec le nombre de coups effectués.  
Si non, on écrit perdu et quel était le mot cherché


## Travail à faire

Un fichier de travail est donné : [fichier de travail](./21_22_projet_pendu_eleve.py).  
**Ce fichier n'est pas fonctionnel directement**. Il constitue une trame avec les différentes fonctions à coder comportant des docstrings.
Des parties ont été mises en commentaires, des instructions `pass`(à supprimer ensuite) ont été mises dans les fonctions/

Vous devez copier/coller dans **votre script** au fur et à mesure de votre travail les définitions de fonctions et les compléter afin de pouvoir les tester.

_Pour chaque fonction, une docstring est renseignée et des tests sont inclus._  
_Vous devez donc tester au fur et à mesure que vos fonctions réalisent bien le cahier des charges fixé._

Compléter les fonctions suivantes :

* fonction `choix_mot(liste_de_mots)`

    `liste_de_mots` est une liste de mots (chaînes de caractères).  
    La fonction doit retourner un mot choisit au hasard parmi `liste_de_mots`


* fonction `liste_lettres_mot(mot)`

    `mot` est une chaîne de caractères.  
    La fonction doit retourner une liste comportant **de manière unique** la liste des lettres contenues dans `mot`.
    
    _Exemples_ :
    
```Python
>>> liste_lettres_mot('papi')
['p', 'a', 'i']
>>> liste_lettres_mot('bonbon')
['b', 'o', 'n']
>>> liste_lettres_mot('bonjour')
['b', 'o', 'n', 'j', 'u', 'r']
>>> liste_lettres_mot('barbapapa')
['b', 'a', 'r', 'p']
```

* fonction `choix_joueur(lettres_jouees)`

    `lettres_jouees` est une liste comportant les lettres déjà jouées.  
    Cette fonction doit demander au joueur sa lettre choisie jusqu'à ce qu'elle ne fasse pas partie de `lettres_jouees`.  
        
    Ici, une variable booléenne `choix_joueur_valide` est initialisée à `False` et tant que le choix du joueur n'est pas valide (c'est à dire qu'il propose une lettre déjà jouée), il faut lui re-demander une lettre.
    
    Cette fonction doit :
        * **ajouter** à la liste `lettres_jouees` la nouvelle lettre proposée par le joueur (dès qu'elle n'a pas déjà été jouée) ;
        * **renvoyer** la lettre proposée qui n'a pas encore été jouée.
        
    Le liste `lettres_jouees` qui est passée en paramètre est alros di te **modifiée en place*.
    
    
* fonction `indices_lettre_dans_mot(mot, lettre)`

    `mot` est le mot à découvrir (`string` donc) et `lettre` un chaîne ne comportant qu'un élément.  
    Cette fonction doit **renvoyer** une **liste** comportant les indices correspondant aux  indices auxquels est situé `lettre` dans `mot`.
    
    _Exemples_ :
    
```Python
>>> indices_lettre_dans_mot('papi', 'p')
[0, 2]
>>> indices_lettre_dans_mot('bonjour', 'j')
[3]
>>> indices_lettre_dans_mot('bonjour', 'r')
[6]
>>> indices_lettre_dans_mot('bonjour', 't')
[]
```

* fonction `modifie_liste_lettres_trouvees(lettre, liste_places, liste_decouvert)`

    Cette fonction doit **modifier en place** `liste_decouvert` en y assignant `lettre` à tous les éléments dont indices contenus dans `liste_places`.  
    **Cette fonction ne renvoie donc rien.**
    
    _Exemples_ :
    
```Python
>>> modifie_liste_lettres_trouvees('p', [0, 2], ['*', '*', '*', '*'])
['p', '*', 'p', '*']
>>> modifie_liste_lettres_trouvees('p', [], ['*', '*', '*', '*'])
['*', '*', '*', '*']
>>> modifie_liste_decouvert('e', [2, 5], ['a', '*', '*', '*', 'b', '*', '*'])
['a', '*', 'e', '*', 'b', 'e', '*']
```

* fonction `affiche_mot_decouvert(liste_decouvert)`

    Cette fonction doit afficher à l'écran le mot constitué de lettres qui sont présentes dans `liste_decouvert` et inclure à la fin un passage à la ligne.  
    **Cette fonction ne renvoie rien**
    
    _Exemples_ :
    
```Python
>>> affiche_mot_decouvert(['*', '*', '*', '*'])
****
>>> affiche_mot_decouvert(['p', '*', '*', 'i'])
p**i
>>> affiche_mot_decouvert(['*', 'o', '*', 'j', 'o', '*', '*'])
*o*jo**
```

* fonction `pendu()`

    C'est la fonction principale du jeu.  
    Elle est déjà complétée en partie et des commentaires ont été mis pour vous guider.
    
    A vous de compléter les éléments manquants.

## Améliorations possibles

Tester si le joueur ne propose bien qu'un seul caractère et si celui-ci est bien une minuscule.
