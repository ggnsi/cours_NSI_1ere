# Les tuples : affectation multiples, retourner plusieurs valeurs dans une fonction
---

### Le mécanisme d'affectation multiple ou dit de déballage (un-packing)

>Le "déballage", consiste à pouvoir affecter simultanément les éléments d'un tuple aux variables écrites à gauche, séparées par une virgule.  

  _Exemple_ :
  
  Si `t = (2, 3, 5)`, 
  
  l'instruction `a, b, c = t` va "déballer" le tuple et affecter :
  * 2 à `a` ;
  * 3 à `b` ;
  * 5 à `c`.
  
  
  _Exemple_ :


```python
coord = (-3, 2) # coord est un tuple comportant les coordonnées d'un point
x, y = coord # affectation multiple
print(x)
print(y)
```

    -3
    2
    

_Remarques_ :

* Il faut autant de variables à gauche que d'éléments dans le tuple ;
* Il est d'usage d'utiliser `_` pour les variables inutilisées lors du déballage (la valeur inutilisée est affectée à la variable `_`.


```python
a, b, _ = t
```

_Conséquence_ :
* Les parenthèses étant facultatives lors de la déclaration d'un tuple (mais recommandées), il est possible de faire une affectation multiple rapidement.

  _Exemple_ :


```python
a, b, c = 4, 7, 12.0 #on crée un tuple qu'on unpack directement ensuite
print(a)
print(b)
print(c)
```

    4
    7
    12.0
    

* astuce pour l'échange de valeurs de deux variables


```python
a = 5
b = 7
b, a = a, b
```

A la dernière ligne, l'expression de droite est d'abord évaluée et un tuple `(5, 7)` est créé. Il y a ensuite affectation multiple aux variables `b` et `a`.  
Au final, il y a bien échange des deux valeurs.

_Remarque_ : ce mécanisme d'unpacking existe également pour les autres séquences : listes et chaînes de caractères (mais nous ne l'utiliseront pas avec ces objets cette année).

_pour les curieux_ : vous pouvez faire des recherches sur l'utilisation de l'étoile lors d'un unpacking (_exemple_ : `a, *b, c = (2, 'toto', 'bonjour', 4, 7)`

### Retourner plusieurs variables dans une fonction

Une fonction ne peut renvoyer qu'une seule valeur mais si cette valeur est un tuple, nous pouvons en renvoyer plusieurs et les affecter facilement grâce à l'affectation multiple

_Exemples_ :

* La fonction ci-dessous renvoie un tuple `(q, r)` constitué du quotient et du reste ;
* La ligne `quotient, reste = diveuclide(14, 4)` réalise :
    * d'abord l'appel de la fonction `diveuclide(14, 4)` qui renvoie le typle `(3, 2)` en sortie (car 14 = 4 x 3 + 2).  
    On a ainsi pu retourner les deux valeurs d'un coup.
    * ensuite l'affectation multiple `quotient, reste = (3, 2)`, ce qui permet d'affecter :
        * à la variable `quotient` la valeur 3 ;
        * à la variable `reste` la valeur 2.


```python
def diveuclide(a, b):
    
    q = a // b
    r = a % b
    
    return (q, r)

quotient, reste = diveuclide(14, 4)

print(quotient, reste)
```

    3 2
    

_Autre exemple_ :

Supposons que l'on dispose d'une fonction `min_max(liste)` qui détermine les valeurs minimales et maximales d'une liste passée en paramètre.

`return (min_liste, max_liste)` permet de renvoyer les deux valeurs d'un coup et de réaliser une affectation multiple lors de l'appel de la fonction : `min, max = min_max(ma_liste)`.


```python
def min_max(liste):
    
    #fonction qui cherche les valeurs minimals et maximales d'une liste d'entiers
    
    return (min_liste, max_liste)

min, max = min_max(ma_liste)
```

La fonction renvoie un tuple avec lequel il est facile de faire une affectation multiple.
