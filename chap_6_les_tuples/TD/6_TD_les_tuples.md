# TD - les tuples

### Exercice 1

Soit le tuple `t = (4, 8, 'trou', 'gagné', 7, True)` écrit en Python :

1. Quelle instruction permet d'obtenir la valeur `'gagné'` ?
2. **A partir de `t`**, créer un nouveau tuple `t_2` qui soit `('trou', True)`
3. A l'aide de `t` et de `t_2`, créez un nouveau tuple `t_3` : `(4, 8, 'trou', 'gagné', 7, True, 'trou', True )`

### Exercice 2

Créez une fonction `debut_fin(mot)` où `mot` est une chaîne de caractères non vide et qui renvoie un tuple constitué de la première et de la dernière lettre de `mot` (qui peuvent être la même si `mot` ne contient par exemple qu'un caractère).

### Exercice 3

1. En une seule ligne de code, affectez aux variables `a` et `b` les valeurs `7` et `3.0`.
2. soit le tuple `t = ('il', 'fait', 'beau')`.  
    En deux lignes de codes, affectez les 3 chaines de caractères composant le tuple à 3 variables différentes et faites afficher la phrase `'il fait beau'`
3. Compléter la fonction `modif(x)` suivante afin qu'en fin de script, `a` contienne la valeur `x + 1` et `b` contienne la valeur `x - 1`.  
    Une seule ligne est à écrire à la place des pointillés.

```Python
def modif(x):
    ...
    
a, b = modif(valeur) # valeur étant à remplacer par un nombre pour faire le test
```  

### Exercice 4

Soit le tuple : `t_prenoms = ('Rosalie', 'Pierre', 'Jean', 'Paul', 'Matthieu', 'Simon', 'André', 'Jacques', 'Philippe', 'Toto', 'Mathurin')`

Créer un prédicat `est_present(prenom)` où `prenom` est une chaîne de caractères et qui renvoie `True` si une personne portant le prénom `prenom` est dans ce tuple et `False` sinon.

_Remarque_ : on s'interdira bien sur ici d'utiliser : `return prenom in t_prenoms` !


### Exercice 5

On représente les coordonnées d'un point du plan par un tuple `(abscisse, ordonnée)`.

On rappelle que la distance entre deux points `A` et `B` est donnée par la formule : $`d(A,B)=\sqrt{(x_B - x_A)^2 + (y_B - y_A)^2}`$.

Compléter la fonction ci-dessous qui prend en paramètre deux points et qui renvoie la distance entre ces deux points :

```Python
def distance(coord1, coord2):
    '''
    Renvoie la distance entre les deux points dont on connait les coordonnées.

    :paramètre coord1: (tuple) coordonnées d'un point
    :paramètre coord2: (tuple) coordonnées d'un point
    :return: (float) distance entre les deux points

    Exemple
    >>> distance((0, 4), (3, 0))
    5.0
    '''
    
    
```

### Exercice 6 [^1]

On considère un tableau de tuples de la forme (prénom, nom, jour, mois, année) décrivant un élève et sa date de naissance:

```Python
eleves = [
    ("Sophie", "Jacquemin", 3, 6, 2003),
    ("Zoé", "Ledoux", 1, 2, 2004),
    ("Adrien", "Millet", 8, 6, 2003),
    ("Gilles", "Hebert", 11, 12, 2003),
    ("Andrée", "Millet", 14, 1, 2005),
    ("Françoise", "Auger", 30, 5, 2003),
    ("Margot", "Dias-Laurent", 30, 6, 2003),
    ("Thimothée", "Lebreton", 2, 10, 2004),
    ("Audrey", "Monnier", 30, 12, 2006),
    ("Élisabeth", "Lebrun", 27, 4, 2003)
]
```

Écrire une fonction `reduit_tuples(tableau_eleves)` prenant pour paramètre un tableau de tuples de la forme précédente, et renvoyant un autre tableau contenant des tuples de la forme `(NOM, PRENOM)` (en majuscules) construit à partir des données du premier, dans le même ordre.

_Exemple_ :

```Python
>>> eleves = [("Sophie", "Jacquemin", 3, 6, 2003), ("Zoé", "Ledoux", 1, 2, 2004), ("Adrien", "Millet", 8, 6, 2003), ("Gilles", "Hebert", 11, 12, 2003), ("Andrée", "Millet", 14, 1, 2005), ("Françoise", "Auger", 30, 5, 2003),("Margot", "Dias-Laurent", 30, 6, 2003), ("Thimothée", "Lebreton", 2, 10, 2004), ("Audrey", "Monnier", 30, 12, 2006), ("Élisabeth", "Lebrun", 27, 4, 2003)]
>>> reduit_tuples(eleves)
[('JACQUEMIN', 'SOPHIE'), ('LEDOUX', 'ZOÉ'), ('MILLET', 'ADRIEN'), ('HEBERT', 'GILLES'), ('MILLET', 'ANDRÉE'), ('AUGER', 'FRANÇOISE'), ('DIAS-LAURENT', 'MARGOT'), ('LEBRETON', 'THIMOTHÉE'), ('MONNIER', 'AUDREY'), ('LEBRUN', 'ÉLISABETH')]
```

### Exercice 7 : QCM

__Question 1__

Soit la définition suivante :

`t = ('foo', 'bar', 'baz')`

Laquelle des propositions suivantes permet de remplacer l'élément 'bar' par 'qux' ?

Réponses :

    A. t[1] = 'qux'
    B. t(1) = 'qux'
    C. t[1:1] = 'qux'
    D. Ce n'est pas possible de faire ce remplacement !



__Question 2__

Laquelle des propositions suivantes permet de créer un tuple nommé p contenant la chaîne `'foo'` ?

Réponses :

    A. p = puplet('foo')
    B. p = ('foo',)
    C. p = ('foo')
    D. On ne peut pas créer un tuple comportant 1 seul élément !



__Question 3__


Qu’affiche le programme suivant ?

```Python
tuple = (1,5,7,9,10,15)
print("tuple[5] = ",tuple[5])
```

Réponses :

    A. tuple[5] = 5
    B. tuple[5] = ‘5’
    C. tuple[5] = 15
    D. tuple[5] = 10

__Question 4__

Qu’affiche le programme suivant?

```Python
tuple = (1,5,7,9,10,15)
print("tuple=",Tuple)
```
Réponses :

    A. (1,5,7,9,10,15)
    B. NameError:name ‘Tuple’ is not defined
    C. Tuple
    D. 5

__Question 5__


Qu’affiche le programme suivant ?

```Python
tuple = (1,5,7,9,10,15)
tuple[5]=2
print("tuple=",tuple)
```

Réponses :

    A. (1,2,7,9,10,15)
    B. (1,5,7,9,10,2)
    C. TypeError :’tuple’ object does not support item assignment
    D. (1,5,7,9,2,15)

__Question 6__

On a saisi le code suivant :

```Python
t = (1, 2, 3, (2, 3, 4), 5, 6)
len(t)
```

Que renvoie l’exécution de ce script ?

Réponses :

    A. 6
    B. 8
    C. 5
    D. 3

__Question 7__

Que contient la variable `a` en fin d'éxécution du script suivant ?

```Python
def f(x):
    return x, x**2

b, a = f(4)
```

    A. un tuple ;
    B. l'entier 4
    C. l'entier 16
    D. une liste



__Question 8__

Une table d’un fichier client contient le nom, le prénom et l’identifiant des clients sous la forme :

```Python
clients = [("Dupont", "Paul", 1),
           ("Durand", "Jacques", 2),
           ("Dutronc", "Jean", 3),
           ...]
```

En supposant que plusieurs clients se prénomment Jean, que vaut la liste `x` après l’exécution du code suivant ?

```Python
x = []
for i in range(len(clients)):
    if clients[i][1] == "Jean":
       x = clients[i]
```


Réponses :

    A. Une liste de tuples des noms, prénoms et numéros de tous les clients prénommés Jean
    B. Une liste des numéros de tous les clients prénommés Jean
    C. Un tuple avec le nom, prénom et numéro du premier client prénommé Jean
    D. Un tuple avec le nom, prénom et numéro du dernier client prénommé Jean

__Question 9__

Quelle instruction écrire sur une seule ligne pour affecter aux variables `mini` et `maxi` le minimun et le maximum des valeurs 12 et 7 ?

```Python
def f(x,y):
   if x > y:
      return y,x
   else:
      return x,y
```


### Exercice 8

Réalisez une fonction nommée `insere(elt, rang, t_uple)` qui renvoie un tuple dans lequel l’élément `elt` a été placé à l’indice `rang` dans le tuple `t_uple`.

_Remarques_ :
* Faire attention à la syntaxe pour définir un tuple contenant un seul élément ;
* _Rappel_ : il n'existe pas de méthode `append()` pour les tuples puisque ceux-ci sont immuables.  Mais il est possible de concaténer des tuples pour en créer un nouveau.

_Exemple_ :

```Python
>>> insere(3,0,(1,4,1,5,9))
(3, 1, 4, 1, 5, 9)
>>> insere(4,2,(3,1,1,5,9))
(3, 1, 4, 1, 5, 9)
>>> insere(9,5,(3,1,4,1,5))
(3, 1, 4, 1, 5, 9)
```


### Exercice 9

Réalisez une fonction nommée `supprime(rang, t_uple)` qui renvoie un tuple donné en paramètre contenant tous les éléments de ce tuple sauf celui d’un indice `rang`. L'indice `rang` doit être compris entre 0 au sens large et `len(t_uple)` au sens strict.

_Exemple_ :

```Python
>>> supprime(0,(1,4,1,5,9))
(4, 1, 5, 9)
>>> supprime(2,(3,1,1,5,9))
(3, 1, 5, 9)
>>> supprime(4,(3,1,4,1,5))
(3, 1, 4, 1)
```

_Remarque_ : Si le reste est fini, créez une version modulaire de cette fonction en créant d'abord une fonction `extrait(rang_depart, rang_fin, t_uple)` qui renvoie un `tuple` contenant les élements de `t_uple` dont le rang est situé entre `rang_depart` (inclus) et `rang_fin` (exclus).

### Exercice 10

Ecrire un prédicat `est_rect(triangle)` où `triangle` est un tuple contenant les trois longueurs des côtés (pas forcément rangées par ordre croissant) d'un triangle et qui renvoie `True` si le triangle est rectangle et `False` sinon.  
On ne travaillera qu'avec des longueurs de côtés entières.

_Exemples_ :

```Python
>>> triangle_1 = (5, 3, 4)
>>> est_rect(triangle_1)
True
>>> triangle_2 = (5, 6, 4)
>>> est_rect(triangle_2)
False
```

### Liens utilisés et sitographie

* [1](http://isn.cassin.free.fr/Tuples_eleves.html)
