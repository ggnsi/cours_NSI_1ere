def est_rect(triangle):
    """Prédicat qui détermine si le triangle est rectangle

    param triangle : (tuple of int de longueur 3) : les dimensions des côtés
    valeur retournée : (True of False) selon que le triangle soit rectangle ou pas
    
    Exemples :
    
    >>> triangle_1 = (5, 3, 4)
    >>> est_rect(triangle_1)
    True
    >>> triangle_2 = (5, 6, 4)
    >>> est_rect(triangle_2)
    False
    """
    
    a, b, c = triangle # on fait une affectation multiple
    
    return (a **2 == b **2 + c **2) or (b **2 == a **2 + c **2) or (c **2 == a **2 + b **2)


if __name__ == '__main__':
    import doctest
    doctest.testmod()