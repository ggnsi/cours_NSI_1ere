def insere(elt, rang, t_uple):
    """insère elt dans le tuple t_uple afin qu'il soit au rang indiqué

    param elt : (non défini) l'élément à insérer
    param rang : (int)
    param t_uple : (tuple)
    
    CU : 0 <= rang < len(tuple)
    
    Exemples :
    >>> insere(3,0,(1,4,1,5,9))
    (3, 1, 4, 1, 5, 9)
    >>> insere(4,2,(3,1,1,5,9))
    (3, 1, 4, 1, 5, 9)
    >>> insere(9,5,(3,1,4,1,5))
    (3, 1, 4, 1, 5, 9)
    """
    
    t_sortie = ()
    for k in range(rang):
        t_sortie = t_sortie + (t_uple[k],)
    t_sortie = t_sortie + (elt,)
    for k in range(rang, len(t_uple)):
        t_sortie = t_sortie + (t_uple[k],)
    return t_sortie

def supprime(rang, t_uple):
    """Retourne t_uple où l'élément d'indice rang a été supprimé

    param rang : (int)
    param t_uple (tuple)
    
    CU : 0 <= rang <len(t_uple)
    
    Exemples :
    >>> supprime(0,(1,4,1,5,9))
    (4, 1, 5, 9)
    >>> supprime(2,(3,1,1,5,9))
    (3, 1, 5, 9)
    >>> supprime(4,(3,1,4,1,5))
    (3, 1, 4, 1)
    """
    
    t_sortie = ()
    for k in range(rang):
        t_sortie = t_sortie + (t_uple[k],)
    for k in range(rang + 1, len(t_uple)):
        t_sortie = t_sortie + (t_uple[k],)
    return t_sortie
    

# Version plus modulaire où l'on construit d'abord une fonction
# extrait qui permet d'obtenir une tranche d'un tuple (cela
# existe d'ailleurs nativement dans Python)
# puis on utilise cette fonction extrait pour construire
# la fontion insere2 et supprime2

def extrait(rang_depart, rang_fin, t_uple):
    """Retourne un un tuple contenant les élements de t_uple dont le rang est situé
    entre rang_depart (inclus) et rang_fin (exclus)
    
    param rang_depart, rang_fin : (int)
    param t_uple : (tuple)
    
    CU : 0 <= rang_depart < rang_fin < len(tuple)
    
    Exemples :
    >>> extrait(1,4,(1,4,1,5,9))
    (4, 1, 5)
    >>> extrait(4,5,(3,1,1,5,9))
    (9,)
    """
    
    t_sortie = ()
    for k in range(rang_depart, rang_fin):
        t_sortie = t_sortie + (t_uple[k],)
    return t_sortie

def insere_2(elt, rang, t_uple):
    """insère elt dans le tuple t_uple afin qu'il soit au rang indiqué

    param elt : (non défini) l'élément à insérer
    param rang : (int)
    param t_uple : (tuple)
    
    CU : 0 <= rang < len(tuple)
    
    Exemples :
    >>> insere_2(3,0,(1,4,1,5,9))
    (3, 1, 4, 1, 5, 9)
    >>> insere_2(4,2,(3,1,1,5,9))
    (3, 1, 4, 1, 5, 9)
    >>> insere_2(9,5,(3,1,4,1,5))
    (3, 1, 4, 1, 5, 9)
    """
    
    t_sortie = extrait(0, rang, t_uple)
    t_sortie = t_sortie + (elt,)
    t_sortie = t_sortie + extrait(rang, len(t_uple), t_uple)
    return t_sortie

def supprime_2(rang, t_uple):
    """Retourne t_uple où l'élément d'indice rang a été supprimé

    param rang : (int)
    param t_uple (tuple)
    
    CU : 0 <= rang <len(t_uple)
    
    Exemples :
    >>> supprime_2(0,(1,4,1,5,9))
    (4, 1, 5, 9)
    >>> supprime_2(2,(3,1,1,5,9))
    (3, 1, 5, 9)
    >>> supprime_2(4,(3,1,4,1,5))
    (3, 1, 4, 1)
    """
    
    return extrait(0, rang, t_uple) + extrait(rang + 1, len(t_uple), t_uple)

if __name__ == '__main__':
    import doctest
    doctest.testmod()