from math import sqrt

def distance(coord1, coord2):
    '''
    Renvoie la distance entre les deux points dont on connait les coordonnées.

    :paramètre coord1: (tuple) coordonnées d'un point
    :paramètre coord2: (tuple) coordonnées d'un point
    :return: (float) distance entre les deux points

    Exemple
    >>> distance((0, 4), (3, 0))
    5.0
    '''

    return sqrt((coord2[0] - coord1[0]) ** 2 + (coord2[1] - coord1[1]) ** 2)