def reduit_tuples(tableau_eleves):
    """Retourne un tableau de tuples contenant les noms et prénoms en majuscule des élèves présents dans tableau_eleves

    param tableau_eleves : (list of tuple) avec chaque tuple de la forme (string, string, int, int, int)
    correspondant à (nom, prénom, jour de naissance, mois de naissance, année de naissance)
    
    valeur retournée : (list of tuples)
    
    >>> eleves = [("Sophie", "Jacquemin", 3, 6, 2003), ("Zoé", "Ledoux", 1, 2, 2004), ("Adrien", "Millet", 8, 6, 2003), ("Gilles", "Hebert", 11, 12, 2003), ("Andrée", "Millet", 14, 1, 2005), ("Françoise", "Auger", 30, 5, 2003),("Margot", "Dias-Laurent", 30, 6, 2003), ("Thimothée", "Lebreton", 2, 10, 2004), ("Audrey", "Monnier", 30, 12, 2006), ("Élisabeth", "Lebrun", 27, 4, 2003)]
    >>> reduit_tuples(eleves)
    [('JACQUEMIN', 'SOPHIE'), ('LEDOUX', 'ZOÉ'), ('MILLET', 'ADRIEN'), ('HEBERT', 'GILLES'), ('MILLET', 'ANDRÉE'), ('AUGER', 'FRANÇOISE'), ('DIAS-LAURENT', 'MARGOT'), ('LEBRETON', 'THIMOTHÉE'), ('MONNIER', 'AUDREY'), ('LEBRUN', 'ÉLISABETH')]
    """
    
    l_sortie = []
    for eleve in tableau_eleves:
        l_sortie.append((eleve[1].upper(), eleve[0].upper()))
    return l_sortie

                    

if __name__ == '__main__':
    import doctest
    doctest.testmod()