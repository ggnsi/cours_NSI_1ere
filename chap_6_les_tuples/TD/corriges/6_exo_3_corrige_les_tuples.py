# question 1

a, b = (7, 3.0)

# question 2

t = ('il', 'fait', 'beau')
a, b, c = t
print(a, b, c)

#question 3

def modif(x):
    
    return (x - 1, x + 1)
    
a, b = modif(5) #5 est une valeur prise pour l'exemple

# alors a contient 4 et b contient 6 car la fonction renvoie le tuple (4, 6) qui est débalé.