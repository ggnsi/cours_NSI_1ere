# TD - les tuples - corrigé
---

### Exercice 7 : QCM

__Question 1__

Soit la définition suivante :

`t = ('foo', 'bar', 'baz')`

Laquelle des propositions suivantes permet de remplacer l'élément 'bar' par 'qux' ?

>Réponse : D. Ce n'est pas possible de faire ce remplacement !
>
> Un `tuple` est non mutable et toutes les commandes précédentes (enfin celles valides) tentaient de modifier `t`.



__Question 2__

Laquelle des propositions suivantes permet de créer un p-uplet nommé p contenant la chaîne `'foo'` ?

>Réponse :  B. `p = ('foo',)`
    
__Question 3__


Qu’affiche le programme suivant ?

```Python
tuple=(1,5,7,9,10,15)
print("tuple[5] = ",tuple[5])
```

>Réponse : C. `tuple[5] = 15`
   
__Question 4__

Qu’affiche le programme suivant?

```Python
tuple=(1,5,7,9,10,15)
print("tuple=",Tuple)
```
>Réponses : B. NameError:name ‘Tuple’ is not defined  
>`Tuple` n'est en effet pas défini : c'est `tuple` qui est défini.
   

__Question 5__


Qu’affiche le programme suivant ?

```Python
tuple=(1,5,7,9,10,15)
tuple[5]=2
print("tuple=",tuple)
```

>Réponses :C. TypeError :’tuple’ object does not support item assignment  
>On ne peut modifier un `tuple` (2ème ligne)


__Question 6__

On a saisi le code suivant :

```Python
t = (1, 2, 3, (2, 3, 4), 5, 6)
len(t)
```

Que renvoie l’exécution de ce script ?

>Réponse : A. 6
   

__Question 7__

Que contient la variable `a` en fin d'éxécution du script suivant ?

```Python
def f(x):
    return (x, x**2)

b, a = f(4)
```

>Réponse : C. l'entier 16  
> En effet, la fonction retourne le `tuple` `(4, 16)` et l'affectation multiple affecte
> 16 à `a`.
    



__Question 8__

Une table d’un fichier client contient le nom, le prénom et l’identifiant des clients sous la forme :

```Python
clients = [("Dupont", "Paul", 1),
           ("Durand", "Jacques", 2),
           ("Dutronc", "Jean", 3),
           ...]
```

En supposant que plusieurs clients se prénomment Jean, que vaut la liste `x` après l’exécution du code suivant ?

```Python
x = []
for i in range(len(clients)):
    if clients[i][1] == "Jean":
       x = clients[i]
```


> Réponse :D. Un tuple avec le nom, prénom et numéro du dernier client prénommé Jean  
> En effet, on parcours la liste et si l'élément d'indice 1 du `tuple` récupéré est `"Jean"` alors on à `x` le tuple correspondant.
>
>_Remarque_ : il est même bizarre d'avoir au départ défini `x` comme une `liste` vide.

__Question 9__

Quelle instruction écrire sur une seule ligne pour affecter aux variables `min` et `max` le minimun et le maximum des valeurs 12 et 7 ?

```Python
def f(x,y):
   if x > y:
      return y,x
   else:
      return x,y
```

> Réponse : `min, max = f(x,y)`
