# Les tuples compléments : quelques méthodes et fonctions.
---

* nombre d'occurence d'une valeur dans un tuple : la méthode `count()`


```python
t = (4, 5, 2, 4, 88, 6, 4)
print(t.count(4))
```

    3
    

* rang de la première apparition d'une valeur : la méthode `index()`


```python
print(t.index(88))
```

    4
    

* choisir au hasard un élément d'un tuple : `choice(tuple)` (avec module random)
* composer aléatoirement une _liste_ de $n$ éléments d'un tuple `sample(tuple, nb_elts)` (avec module random)

Ces deux fonctions ne sont à utiliser **QUE** si vous savez les programmer par vous même.


```python
import random
print(random.choice(t))
print(random.sample(t, 3))
```

    6
    [4, 4, 4]
    
