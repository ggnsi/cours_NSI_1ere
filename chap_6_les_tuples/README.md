## Les tuples
---

* [Cours : généralités sur les tuples](./6_les_tuples_definition_parcours_cours_1.md)
* [Exercices 1 et 2 du TD](./TD/6_TD_les_tuples.md)
* [Cours : affectation multiple, retourner plusieurs valeurs dans une fonction](./6_les_tuples_unpacking_cours_2.md) 
* [Autres exercices du TD](./TD/6_TD_les_tuples.md) et des corrigés
* [Cours : compléments](./6_les_tuples_methodes_cours_3.md)



