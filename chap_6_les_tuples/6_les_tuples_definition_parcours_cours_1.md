# Les tuples
---

Un _tuple_ est un objet **natif** ( _built-in_ ) qui est :

* __séquentiel__ : c'est une collection ordonnée d'objets qui peuvent être de différents types. On accède aux éléments par leur **rang**  (les listes et les chaînes de caractères sont aussi des objets séquentiels) ;

* __itérable__ : on peut le parcourir à l'aide d'une boucle _for_. A ce titre le tuple accepte __les tests__ :  `x in Tuple ` ou `x not in Tuple`. (les listes et les chaînes de caractères sont aussi des **itérables**);

* __non mutable__ : une fois défini, on ne peut modifier les valeurs affectées (une liste est mutable, une chaîne de caractères non).

**On peut voir les _tuples_ comme des listes non mutables**, c'est à dire dont **on ne peut pas modifier les éléments**.

Mais puisqu'on dispose déjà des lites, on peut se demander pourquoi introduire un tel objet.

Quelques éléments :

* on peut vouloir "protéger" en écriture les données qui n'ont pas besoin d'être modifiées. Utiliser un tuple à la place d'une liste revient à avoir une assertion implicite que les données sont constantes et que des mesures spécifiques sont nécéssaires pour modifier cette définition ;
* parcourir un tuple (voir plus loin) est plus rapide que de parcourir une liste (bon, pour nous ici, pas vraiment d'implication) ;
* nous aurons besoin d'éléments non mutables pour définir des clés de disctionnaire (plus tard dans l'année) et les listes sont mutables.

## Généralités
---

### Comment déclarer un _tuple_ ?

Un _tuple_ se **déclare à l'aide de parenthèses** (non obligatoire mais très recommandé) et **on sépare les éléments par une virgule**.

_Exemples_ :


```python
t1 = (25, 34, 150) #utilisation des parenthèses

t1 = 25, 34, 150 # t1 peut aussi se déclarer sans parenthèses mais c'est moins lisible
```


```python
print(type(t1))
```

    <class 'tuple'>
    

Un _tuple_ peut contenir des objets de différents types :


```python
t2 = ('toto', -25, 5.0, True)

t3 = ((4, 'rouge', 'tomate'), (2, 'vert', 'pomme'), (5, 'jaune', 'banane'))
```

t3 est un tuple de tuple, chacun de ces derniers contenant un entier et des chaînes de caractères.

### Cas particuliers

* le tuple vide :


```python
t_vide = () # les parenthèses sont ici nécessaires

```

* un tuple **ne contenant qu'un unique élément** : on écrit l'élément **suivi obligatoirement d'une virgule** (obligatoire sinon `t` contiendra un entier et non un tuple).

  On peut ne pas mettre de parenthèses mais c'est recommandé.


```python
t = 3,    # remarque : la virgule est nécessaire pour indiquer que l'on déclare un tuple
t_ = (3,)
```

### Longueur d'un tuple

> `len(tuple)` renvoie la _longueur_ du tuple, c'est à dire le nombre d'éléments le constituant.


```python
t_1 = (1, 2)
len(t_1)
```






### Accès aux éléments

> On accède aux éléments de manière indexée, comme pour les listes ou les chaînes de caractères.  
> Le premier élément est indexé 0.

> Pour $i\geqslant 0$, `t[i]` renvoie l'élément indexé $i$ ;
>
> Pour $i < 0$, `t[i]` renvoie le $i$-ème élément en partant de la fin, le dernier élément ayant la position $-1$.

_Exemples_ :


```python
t = ('zero', 'un', 'deux', 'trois', 'quatre', 'cinq')
print(t[2])
print(t[-2])
```

    deux
    quatre
    

### Les tuples ne sont pas mutables

Cela signifie qu'il est impossible de modifier un élément d'un tuple.



```python
t = (1, 2, 3)
t[1] = 4
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-15-60717e2b7ae9> in <module>
          1 t = (1, 2, 3)
    ----> 2 t[1] = 4
    

    TypeError: 'tuple' object does not support item assignment


Si _vous voulez modifier un tuple_ , il faudra en créer un autre, par exemple (mais pas uniquement) en lui concaténant des tuple :

_Exemple :_


```python
tuple1 = (1,2)
tuple2 = (3,4)
tuple_nouveau = tuple1 + tuple2
print(tuple_nouveau)
```

    (1, 2, 3, 4)
    

### Conversion en tuple

> la fonction `tuple(itérable)` permet de créer un tuple en parcourant l'itérable.

_Exemples_ :


```python
t1 = tuple('toto')
t2 = tuple([4, 7, 8])
print(t1)
print(t2)
```

    ('t', 'o', 't', 'o')
    (4, 7, 8)
    


```python
t = tuple(45)  #provoque une erreur car un int n'est pas itérable
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-14-4f66b85f9b69> in <module>
    ----> 1 t = tuple(45)  #provoque une erreur car un int n'est pas itérable
    

    TypeError: 'int' object is not iterable


_Remarque_ : il est aussi possible de créer des tuples en compréhension (hors programme).

### Les opérateurs + et *

> `t1 + t2` renvoie un tuple qui contient les éléments de `t1` avec à la suite ceux de `t2`
>
> `t1 * n` ou `n * t1` renvoie un tuple qui contient de manière répétée $n$ fois les éléments de `t1'


```python
t_1 = (1, 2)
print(t_1 * 3)
```

    (1, 2, 1, 2, 1, 2)
    

## Parcourir un tuple
---

On retrouve encore une fois ici les deux types de parcours possibles :

* via les indices ;
* avec l'utilisation d'une boucle `for` puisqu'un tuple est un itérable.

### parcours via les indices des éléments



```python
t = ('a', 'e', 'i', 'o', 'u', 'y')

for indice in range(len(t)):
    print(t[indice])
```

    a
    e
    i
    o
    u
    y
    

### parcours avec `for elt in tuple`


```python
t = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)

for elt in t:
    print(elt)
```

    0
    1
    2
    3
    4
    5
    6
    7
    8
    9
    
