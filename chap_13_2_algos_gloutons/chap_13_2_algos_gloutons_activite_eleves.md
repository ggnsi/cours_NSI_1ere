# Les algorithmes gloutons

<center> 

<img src="./img/mGlouton.jpeg" style="width:14em" >

</center>




---

# <span style= 'color:#FF4500'>1. késako un algorighme glouton ? </span>

---

**Un algorithme glouton est un algorithme d'optimisation, chargé de prendre une succession de décisions. Son principe de fonctionnement  est de faire le choix d'optimiser localement chacune des décisions, en espérant obtenir une solution optimale globalement.** 

<br/>

Les algorithmes gloutons produisent **rarement** des solutions optimales à un problème donné. <br>

On les utilise parfois pour résoudre de façon approximative (mais relativement convenable), et rapidement, un problème  dont la solution exacte serait bien trop longue à déterminer.

<br/>

### Définitions : algorithme glouton exact, heuristique gloutonne



- Lorsqu'un algorithme glouton est optimal globalement, on dit que c'est **un algorithme glouton exact.**
- **Sinon on parle d'heuristique gloutonne.**

<br/>

---

---

# <span style= 'color:#FF4500'>2. Un exemple : le voyageur de commerce ? </span>

---
_Présentation largement inspirée du livre NSI : 30 leçons (Balabonski, Conchon, Filliâtre, Nguyen)_

Supposons que nous sommes un représentant partant de Nancy et que nous devons visiter des entreprises situées à Metz, Paris, Troyes et Reims avant de revenir à Nancy (on ne visite qu'une seule fois les villes intermédiaires).

__Le but est de parcourir la plus petite distance possible.__

On a relevé (en utilisant par exemple [openstreetmap](https://www.openstreetmap.org/#map=9/50.0801/2.4005) les distances entre chaque ville et nous les avons reportées ci-dessous.

<br>

<img src="./img/tableauDistanceVoyageur.png" style="width:29em" />

<br>

### Première approche possible : la force brute

> L'idée de l'approche par force brute consiste à explorer tous les circuits possibles.

1. En utilisant un arbre de dénombrement (inutile de le dessiner en entier), déterminer combien de chemins sont possibles.
2. En remarquant que la moitié des chemins seront symétriques (c'est à dire un chemin déjà présent mais parcouru dans l'autre sens), déterminer celui qui aura la distance parcourue la plus courte (se répartir le travail).
3. Si le circuit comportait _N_ villes à visiter, quel serait le nombre de chemins à étudier ?
4. Quel sera ce nombre lorsque _N = 10_ ? lorsque _N = 13_ ? lorsque _N = 15_ ?

---

### Deuxième approche possible : un algorithme glouton
---

On ne cherchera plus ici __la meilleure solution__ mais une solution __qui soit bonne__ selon un certain critère (ici la distance parcourue). Peut être pas la meilleure mais acceptable.

> Lorsque la recherche d'une solution peut se ramener à une succession de choix qui produisent petit à petit une solution partielle, l'approche gloutonne consiste alors à construire une solution complète en choisissant à chaque étape la meilleure solution partielle.

Dans certains cas, cela donnera finalement la meilleure solution : on parlera d'algorithmes goutons exacts.

Dans d'autres non, on parlera d'heuristiques gloutonnes.


#### Traduction dans notre problème

Nous allons progressivement construire notre chemin en choisissant à chaque étape la ville qui minimisera la distance parcourue partielle.

Cela reviendra donc à choisir à chaque fois la ville la plus proche de la ville où l'on se situe (en étant sur qu'elle n'a pas déjà été visitée).

#### Réalisation à la main de la recherche gloutonne

Réaliser à la main la recherche gloutonne en vous aidant du tableau des distances situé plus haut.

#### Réalisation d'un d'algorithme

Nous aurons besoin :
* de la liste des noms des villes `l_villes` ;
* d'une matrice donnant les distances entre villes (les indices utilisés respectant ceux de `l_villes`) : `mat_dist` ;
* d'une liste  de "Drapeaux" indiquant si la ville a déjà été visitée : 
  * les indices correspondent à ceux de `l_villes` ;
  * le drapeau a la valeur `True` si la ville a déjà été visitée ;
  * le drapeau a la valeur `False` si la ville n'a pas déjà été visitée.
  
    _Exemple_ : 
      * si aucune ville n'a été visitée, `dejavisitee = [False, False, False, False, False, False]`
      * si `Nancy` et `Paris` ont déjà été visitées, `dejavisitee = [True, False, True, False, False, False]`


```python
# Exemple avec notre problème actuel

listeVilles = ['Nancy', 'Metz', 'Paris', 'Reims', 'Troyes'] 
#les villes sont indicées de 0 à 4

tableDistances = [ [None, 55, 303, 188, 183], 
                   [55, None, 306, 176, 203],
                   [303, 306, None, 142, 153],
                   [188, 176, 142, None, 123],
                   [183, 203, 153, 123, None] ]
```

Construisons deux fonctions :
* `plus_proche(i_ville, tabdist, dejavues)` qui prendra en paramètres :
  * `i_ville` : l'indice de la ville d'où part le trajet considéré ;
  * `tabdist` : la matrice donnant les distances entre villes ;
  * `dejavues` : liste de booléens indiquant si une ville a déjà été visitée ou non.
  
  et retournera un tuple `(indice_ville, distance)` correspondant à l'indice de la ville la plus proche de celle dont l'indice est `i_ville` et la la distance minimale par rapport à elle.

  Une condition d'utilisation de `plus_proche` est qu'il existe au moins une ville non encore visitée autre que `i_ville`.
  
* une fonction `it_glouton(liste_villes, tabdist, i_depart)` qui retournera la liste des villes à visiter ainsi que la distance totale parcourue lorsque l'on part de la ville d'indice `i_depart` en utilisant l'algorithme glouton.


```python
def plus_proche(i_ville, tabdist, dejavues):
    
    plus_proche = None
    l_distances = tabdist[i_ville]
    for i_town in range(len(dejavues)):
        if dejavues[i_town]:
            continue
        if (plus_proche == None) or (l_distances[i_town] < l_distances[plus_proche]):
                plus_proche = i_town
    return plus_proche

def it_glouton(liste_villes, tabdist, i_depart):
    
    nb_villes = len(liste_villes)
    dejavisitee = [False] * nb_villes
    chemin = []
    dist = 0
    i_ville_etape = i_depart
    for _ in range(nb_villes-1):
        dejavisitee[i_ville_etape] = True
        i_ville_suivante = plus_proche(i_ville_etape, tabdist, dejavisitee)
        chemin.append(liste_villes[i_ville_suivante])
        dist += tabdist[i_ville_etape][i_ville_suivante]
        i_ville_etape = i_ville_suivante
    dist += tabdist[i_ville_etape][i_depart]
    return dist, chemin

```


```python
it_glouton(listeVilles, tableDistances, 0) 
```




    (810, ['Metz', 'Reims', 'Troyes', 'Paris'])



#### Bilan

L'algorithme glouton n'est ici pas exact : il fournit une chemin de distance 810 km alors que la solution optimale obtenue en force brute était de 709 km.

__qualité de l'approximation__

Lorsque l'algorithme glouton n'est pas exact, la qualité de l'approximation donnée dépend fortement du problème considéré.

Il a été prouvé que pour le problème du voyageur, **lorsque le nombre de villes devient grand**, le rapport entre la solution gloutonne et la solution optimale est, dans le pire des cas, proportionnel au logarithme du nombre de villes :   $\frac{distance_{gloutonne}}{distance_{optimale}} = 0(log_2(nb_{villes}))$
