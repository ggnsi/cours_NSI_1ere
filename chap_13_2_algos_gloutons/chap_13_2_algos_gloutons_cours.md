# Les algorithmes gloutons

<center> 

<img src="./img/mGlouton.jpeg" style="width:14em" >

</center>




---

# <span style= 'color:#FF4500'>1. késako un algorighme glouton ? </span>

---

**Un algorithme glouton est un algorithme d'optimisation, chargé de prendre une succession de décisions. Son principe de fonctionnement  est de faire le choix d'optimiser localement chacune des décisions, en espérant obtenir une solution optimale globalement.** 

<br/>

Les algorithmes gloutons produisent **rarement** des solutions optimales à un problème donné. <br>

On les utilise parfois pour résoudre de façon approximative (mais relativement convenable), et rapidement, un problème  dont la solution exacte serait bien trop longue à déterminer.

<br/>

### Définitions : algorithme glouton exact, heuristique gloutonne



- Lorsqu'un algorithme glouton est optimal globalement, on dit que c'est **un algorithme glouton exact.**
- **Sinon on parle d'heuristique gloutonne.**

<br/>

---

---

# <span style= 'color:#FF4500'>2. Un exemple : le voyageur de commerce ? </span>

---
_Présentation largement inspirée du livre NSI : 30 leçons (Balabonski, Conchon, Filliâtre, Nguyen)_

Supposons que nous sommes un représentant partant de Nancy et que nous devons visiter des entreprises situées à Metz, Paris, Troyes et Reims avant de revenir à Nancy (on ne visite qu'une seule fois les villes intermédiaires).

__Le but est de parcourir la plus petite distance possible.__

On a relevé (en utilisant par exemple [openstreetmap](https://www.openstreetmap.org/#map=9/50.0801/2.4005) les distances entre chaque ville et nous les avons reportées ci-dessous.

<br>

<img src="./img/tableauDistanceVoyageur.png" style="width:29em" />

<br>

### Première approche possible : la force brute

> L'idée de l'approche par force brute consiste à explorer tous les circuits possibles.

#### Combien de chemins possibles ici ?

Nous pouvons réaliser un arbre de dénombrement. Partant de Nancy, nous avons :
* 4 choix possibles pour la première ville visitée ;
* puis, pour chacune, 3 choix possible pour la 2de ville visitée ;
* puis, pour chacune, 2 choix possible pour la 3-ième ville visitée ;
* puis, pour chacune, 1 choix possible pour la 4-ième ville visitée ;
* et enfin, retour à Nancy

<br>

<img src="./img/arbre_voyageur.png" style="width:29em" />

<br>



Nous avons donc _4 x 3 x 2 x 1 = 24_ chemins possibles.  
En fait, la moitié seront les mêmes chemins parcourus en sens contraîre. Il nous suffit donc d'en étudier 12.

__Pour chaque chemin, calulons la longueur du trajet__ :

<br>

<img src="./img/tableau2.png" style="width:10em" />

<br>

**Le meilleur circuit est donc : Nancy - Metz - Reims - Paris - Troyes - Nancy , ou bien celui en sens inverse Nancy - Troyes - Paris - Reims - Metz - Nancy , avec un total de 709 km.**

#### Est-il possible d'avoir la même approche pour un plus grand nombre de villes ?

Combien de chemins faudra-t-il explorer si _N_ villes sont à parcourir :

* N choix possibles pour la première ville visitée ;
* puis, pour chacune, N - 1 choix possible pour la 2de ville visitée ;
* puis, pour chacune, N - 2 choix possible pour la 3-ième ville visitée ;
* puis, pour chacune, N - 3 choix possible pour la 4-ième ville visitée ;
* ...
* enfin 1 choix pour la dernière.

On obtient donc _N * (N - 1) * (N - 2) * ... * 3 * 2 * 1_ possiblités (à diviser par 2).  
Ce nombre est noté _N!_ en mathématiques.  
Nous aurions donc _N!/2_ possibilités.

__Quelques valeurs de _N!/2_

| valeur de N | Valeur de _N!/2_ |
| :--: | :--: |
| 10 | 1 814 400 |
| 13 | environ 3 milliards |
| 15 | environ 654 milliards |

Pour un circuit de 71 villes, il y a plus de chemins possibles que d'atomes dans l'univers connu :  _5 * 10^{80}_.

> __Conclusion__ : Cette approche n'est pas réaliste dès que le nombre de villes augmente (même juste 15 villes !)

---

### Deuxième approche possible : un algorithme glouton
---

On ne cherchera plus ici __la meilleure solution__ mais une solution __qui soit bonne__ selon un certain critère (ici la distance parcourue). Peut être pas la meilleure mais acceptable.

> Lorsque la recherche d'une solution peut se ramener à une succession de choix qui produisent petit à petit une solution partielle, l'approche gloutonne consiste alors à construire une solution complète en choisissant à chaque étape la meilleure solution partielle.

Dans certains cas, cela donnera finalement la meilleure solution : on parlera d'algorithmes goutons exacts.

Dans d'autres non, on parlera d'heuristiques gloutonnes.


#### traduction dans notre problème

Nous allons progressivement construire notre chemin en choisissant à chaque étape la ville qui minimisera la distance parcourue partielle.

Cela reviendra donc à choisir à chaque fois la ville la plus proche de la ville où l'on se situe (en étant sur qu'elle n'a pas déjà été visitée).

#### traduction en terme d'algorithme

Nous aurons besoin :
* de la liste des noms des villes `l_villes` ;
* d'une matrice donnant les distances entre villes (les indices utilisés respectant ceux de `l_villes`) : `mat_dist` ;
* d'une liste de "Drapeaux" indiquant si la ville a déjà été visitée : 
  * les indices correspondent à ceux de `l_villes` ;
  * le drapeau a la valeur `True` si la ville a déjà été visitée ;
  * le drapeau a la valeur `False` si la ville n'a pas déjà été visitée.
  
Construisons deux fonctions :
* `plus_proche(i_ville, tabdist, dejavisitee)` qui retournera un tuple `(indice_ville, distance)` correspondant à l'indice de la ville la plus proche de celle dont l'indice est `i_ville` et la la distance minimale par rapport à elle.

Une condition d'utilisation de `plus_proche` est qu'il existe au moins une ville non encore visitée autre que `i_ville` (sinon, cela renvoie `None`)
* une fonction `it_glouton(liste_villes, tabdist, i_depart)` qui retournera la liste des villes à visiter ainsi que la distance totale parcourue lorsque l'on part de la ville d'indice `i_depart` en utilisant l'algorithme glouton.


```python
def plus_proche(i_ville, tabdist, dejavues):
    
    plus_proche = None
    l_distances = tabdist[i_ville]
    for i_town in range(len(dejavues)):
        if dejavues[i_town]:
            continue
        if (plus_proche == None) or (l_distances[i_town] < l_distances[plus_proche]):
                plus_proche = i_town
    return plus_proche

def it_glouton(liste_villes, tabdist, i_depart):
    
    nb_villes = len(liste_villes)
    dejavisitee = [False] * nb_villes
    chemin = []
    dist = 0
    i_ville_etape = i_depart
    for _ in range(nb_villes-1):
        dejavisitee[i_ville_etape] = True
        i_ville_suivante = plus_proche(i_ville_etape, tabdist, dejavisitee)
        chemin.append(liste_villes[i_ville_suivante])
        dist += tabdist[i_ville_etape][i_ville_suivante]
        i_ville_etape = i_ville_suivante
    dist += tabdist[i_ville_etape][i_depart]
    return dist, chemin

```


```python
# Exemple avec notre problème actuel

listeVilles = ['Nancy', 'Metz', 'Paris', 'Reims', 'Troyes'] 
#les villes sont indicées de 0 à 4

tableDistances = [ [None, 55, 303, 188, 183], 
                   [55, None, 306, 176, 203],
                   [303, 306, None, 142, 153],
                   [188, 176, 142, None, 123],
                   [183, 203, 153, 123, None] ]

it_glouton(listeVilles, tableDistances, 0) 
```




    (810, ['Metz', 'Reims', 'Troyes', 'Paris'])



#### Bilan

L'algorithme glouton n'est ici pas exact : il fournit une chemin de distance 810 km alors que la solution optimale obtenue en force brute était de 709 km.

__qualité de l'approximation__

Lorsque l'algorithme glouton n'est pas exact, la qualité de l'approximation donnée dépend fortement du problème considéré.

Il a été prouvé que pour le problème du voyageur, **lorsque le nombre de villes devient grand**, le rapport entre la solution gloutonne et la solution optimale est, dans le pire des cas, proportionnel au logarithme du nombre de villes :   $\frac{distance_{gloutonne}}{distance_{optimale}} = 0(log_2(nb_{villes}))$
