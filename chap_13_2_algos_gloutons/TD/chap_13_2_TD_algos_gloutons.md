# TD Algorithmes gloutons

### Exercice 1 : le problème du sac à dos

Un cambrioleur possède un sac à dos d'une capacité maximale de 30 Kg.  
Au cours d'un de ses cambriolages, il a la possibilité de dérober 4 objets A, B, C et D. Voici un tableau qui résume les caractéristiques de ces objets :

| Objet | A | B | C | D |
| :--: | :--: | :--: | :--: | :--: |
| Masse | 13 Kg | 12 Kg | 8 Kg | 10 Kg |
|Valeur | 700 € | 400 € | 300 € | 300 € |

Le but, ici, est de déterminer les objets que le cambrioleur aura intérêt à dérober en sachant que tous les objets dérobés devront tenir dans le sac à dos (30 Kg maxi) en fonction de l'objectif du voleur.

A l'aide d'algorithmes gloutons, déterminer la liste des objets que prendra le cambrioleur dans chacun des cas ci-dessous. __On se demandera à chaque fois si la solution fournie est optimale ou pas__.

1. il cherche à maximiser la masse dans le sac à dos ;
2. il cherche à maximiser le montant du butin.  
  Pour cette situation, on aura donc intêret à d'abord mettre dans le sac à dos les objets qui ont la plus grande valeur avec le plus petit poids possible, c'est à dire à s'intéresser aux rapports _valeur/poids_.

__Remarque__ :  
* Afin de faciliter le traitement, il sera judicieux de réaliser un tri du sac à dos en fonction du critère étudié.
* On créera des fonctions `glouton_1(sac_a_dos, poids_max)` et `glouton_2(sac_a_dos, poids_max)` où `poids_max` sera la masse maximum admise par le sac à dos (afin de pouvoir la faire varier).

_Eléments fournis_ :

Chaque objet est représenté par un dictionnaire et le sac à dos est représenté par la liste des objets (donc une liste de dictionnaires)


```python
obj_a = { 'Nom' : 'A', 'Masse' : 13, 'Valeur' : 700}
obj_b = { 'Nom' : 'B', 'Masse' : 12, 'Valeur' : 400}
obj_c = { 'Nom' : 'C', 'Masse' : 8, 'Valeur' : 300}
obj_d = { 'Nom' : 'D', 'Masse' : 10, 'Valeur' : 300}
sac_a_dos = [obj_a, obj_b, obj_c, obj_d]
```

### Exercice 2 : le rendu de monnaie

On cherche à rendre une certaine somme d'argent `somme` __en minimisant le nombre de pièce et de billets obtenus__.

1. Avec le système de monétaire européen

  <img src="img/monnaie_europeenne.png" style="width:42em" />

  On dispose des pièces et billets représentés ci-dessus représenté par une liste `liste_euro = [200, 100, 50, 20, 10, 5, 2, 1]`.  
  
  Ecrire une fontion `rendu_(somme)` qui retourne la liste des quantités respectives de billets/pièces à rendre.
  
  _Exemple_ :
  
  ```Python
  >>> rendu(248) # 248 = 1 x 200 + 0 x 100 + 0 x 50 + 2 x 20 + 0 x 10 + 1 x 5 + 1 x 2 + 1 x 1
  [1, 0, 0, 2, 0, 1, 1, 1]
  >>> rendu(517)
  [2, 1, 0, 0, 1, 1, 1, 0]
  >>> rendu(0)
  [0, 0, 0, 0, 0, 0, 0, 0]
  ```
  
>__Remarque__ :
>  
>  Cet algorithme glouton est __optimal__ pour ce système monétaire. On dit alors que ce système monétaire est __canonique__.
  
2. (question débranchée cad sans l'ordinateur)  
  Considérons mainetenant le système monétaire suivant : `list_bis = [30, 24, 12, 6, 3, 1]`.   
  Quelle sera la solution proposée par l'algorithme glouton pour une somme à rendre de 49 ? Est-elle optimale ? Ce système est-il canonique ?
  
  
3. (question débranchée cad sans l'ordinateur)  
  Considérons le système monétaire `liste_ter = [3, 2]`.
  Quelle sera le comportement de l'algorithme glouton pour une somme à rendre de 4 ? Que dire ?

### Exercice 3 : Problème de réservation de salles
_Source : document d'accompagnement NSI - Education Nationale_

Des conférenciers sont invités à présenter leurs exposés dans une salle. Mais leurs disponibilités ne leur permettent d’intervenir
qu’à des horaires bien définis. Le problème est de construire un planning d’occupation de la salle avec _le plus grand nombre de conférenciers_.

__Exemples__

<img src="img/conferencier_1.jpg" style="width:14em" />

Tous les conférenciers peuvent ici intervenir et la solution optimale sera `[C2,C4,C3,C1]`

<img src="img/conferencier_2.jpg" style="width:14em" />

Plusieurs solutions sont optimales ici : `[C2,C1]` ou `[C2,C3]` ou `[C4,C1]`.

<img src="img/conferencier_3.jpg" style="width:14em" />

La solution optimale est ici `[C2,C3]`.

Une situation plus compliquée : <img src="img/conferencier_4.jpg" style="width:28em" />



__Travail__

On vous demande ici de chercher des solutions au problème en envisageant deux algorithmes gloutons différents :

1. Règle de choix de l'algorithme glouton A : À chaque étape choisir, parmi les conférenciers, celui dont la conférence va commencer en premier. [Basée sur l'idée que moins on attend entre deux conférences, plus on verra de conférences.]


2. Règle de choix de l'algorithme glouton B : À chaque étape choisir, parmi les conférenciers, celui dont la conférence n'est pas encore commencée et qui va finir en premier. [Basée sur l'idée que plus un spectacle finit tôt, plus il y aura de la place pour les spectacles suivants.]

__Contraintes__

1. Chaque conférence sera représentée par un `tuple (int, int, string)` : `(heure_debut, heure_fin, nom_conférencier)`  et le programme par la liste constituée de ces tuples.

2. Vous écrirez des fonctions `glouton_A(programme)` et `glouton_B(programme)` qui retourneront la liste des conférenciers à aller voir dans l'ordre de passage.

__Esprit critique__ :

Vous étudierez à chaque fois la solution proposée par l'algorithme glouton et la comparerez à la solution optimale en étudiant ce qui se passe.

__Données__ :

* `programme_1 = [(3, 4, 'C1'), (0, 1, 'C2'), (2, 3, 'C3'), (1, 2, 'C4')]`
* `programme_2 = [(2, 4, 'C1'), (0, 1, 'C2'), (1, 3, 'C3'), (0, 2, 'C4')]`
* `programme_3 = [(0, 3, 'C1'), (1, 2, 'C2'), (2, 3, 'C3')]`
* `programme_4 = [(0,7,'C1'),(2,5,'C2'),(6,8,'C3'),(1,2,'C4'),(5,6,'C5'),(0,2,'C6'),(4,7,'C7'),(0,1,'C8'),(3,6,'C9'),(1,3,'C10'),(4,5,'C11'),(6,8,'C12'),(0,2,'C13'),(5,7,'C14'),(1,4,'C15')]`

__Eléments de vérification__ :
```Python
>>> glouton_A(programme_1)
['C2', 'C4', 'C3', 'C1']
>>> glouton_A(programme_2)
['C2', 'C3']
>>> glouton_A(programme_3)
['C1']
>>> glouton_A(programme_4)
['C1']
```

```Python
>>> glouton_B(programme_3)
['C2', 'C3']
>>> glouton_B(programme_4)
['C8', 'C4', 'C2', 'C5', 'C3']
```
__Remarque__ : On peut _démontrer_ que l'algorithme glouton B fournit systématiquement une solution optimale.

Une démonstration est faite dans l'article suivant : Bejian P. Matroïdes et algorithmes gloutons : une introduction, 2003. [http://pauillac.inria.fr/~quercia/documents-info/Luminy-2003/beijan/matroide_papier.pdf](http://pauillac.inria.fr/~quercia/documents-info/Luminy-2003/beijan/matroide_papier.pdf)

_Elements de vérification_ : `([20, 19, 16, 15, 14], 84)`

### Exercice 4 : chargeons les wagons

On souhaite charger des containers de marchandises sur des wagons qui peuvent transporter 60 tonnes chacun. Les masses (en tonnes) des containers sont représentées dans un tableau T de nombres entiers. On suppose qu'on peut charger autant de containers qu'on le souhaite sur un wagon tant que la masse des containers déposés ne dépasse pas 60 tonnes.

Par exemple avec le tableau `T` des 18 masses suivantes : `T = [32, 1, 4, 11, 16, 38, 30, 15, 40, 20, 26, 5, 25, 14, 44, 17, 7, 6]` on peut charger les 18 containers sur wagons en les répartissant ainsi dans sous-tableaux :  
`[32, 20, 4], [30, 26], [11, 44], [40, 15, 5], [38, 17], [14, 16, 25, 1], [6, 7]`

On cherche à trouver la répartition des containers de `T` qui permet d'utiliser le plus petit nombre de wagons.

1. Dans cet exemple la sélection effectuée est une "répartition" du tableau T en sous-tableaux. Quelles sont les contraintes ? Quelle est l'optimisation recherchée ?

2. Pour charger un wagon à partir des containers qui restent à charger, on considère la règle de choix suivante : Tant qu'on ne dépasse pas les 60 tonnes sur un wagon, on charge sur le wagon le container le moins lourd restant. Quand ce n'est plus possible, on prend un nouveau wagon. (Basée sur l'idée que plus on cherche à mettre de containers sur un wagon, moins on utilisera de wagons)

   Ecrire une fonction `chargement_1(l_charge)` qui retourne la répartition de charge sur les wagons.

   Ci-desssous, deux chargements à effectuer :


```python
T1 = [32, 1, 4, 11, 16, 38, 30, 15, 40, 20, 26, 5, 25, 14, 44, 17, 7, 6]
T2 = [4, 14, 32, 9, 31, 42, 12, 5, 31, 29, 39, 1, 3, 2, 5, 12, 38]
```

   _Elements de vérification_ :


```python
>>> T1 = [32, 1, 4, 11, 16, 38, 30, 15, 40, 20, 26, 5, 25, 14, 44, 17, 7, 6]
>>> chargement_1(T1)
[[1, 4, 5, 6, 7, 11, 14], [15, 16, 17], [20, 25], [26, 30], [32], [38], [40], [44]]
>>> T2 = [4, 14, 32, 9, 31, 42, 12, 5, 31, 29, 39, 1, 3, 2, 5, 12, 38]
>>> chargement_1(T2)
[[1, 2, 3, 4, 5, 5, 9, 12, 12], [14, 29], [31], [31], [32], [38], [39], [42]]
```

3. a) Pour le tableau T1, cette répartition est-elle une solution optimale ?

   b) Pourquoi cette règle de choix est-elle mauvaise lorsqu'il y a beaucoup de containers de plus de 30 tonnes ?

### Exercice 5

(Source : Julien de Villèle).

La problématique est la même que celle de l'exercice sur la réservation de salle (les conférenciers) mais on se place cette fois ci dans la situation où on dispose du planning d'un festival culturel qui propose des spectacles sur cinq scènes différentes.

Un festivalier, qui arrive à 10:00, cherche à choisir des spectacles dans ce planning (sélection) en cherchant à voir le plus grand nombre de spectacles possibles dans sa journée (maximiser une quantité) et en s'imposant de voir chaque spectacle en entier (contrainte).  
Il peut envisager deux algorithme gloutons différents.

_Règle de choix de l'algorithme glouton A_ :  
À chaque étape choisir, parmi les cinq scènes, le prochain spectacle qui commence en premier. [Basée sur l'idée que moins on attend entre deux spectacles, plus on verra de spectacles.]

_Règle de choix de l'algorithme glouton B_ :  
À chaque étape choisir, parmi les cinq scènes, le prochain spectacle qui finit en premier. [Basée sur l'idée que plus un spectacle finit tôt, plus il y aura de la place pour les spectacles suivants.]

<img src="img/spectacles_1.jpg" style="width:35em" />

_Remarques_ : 
* L'algorithme glouton A aboutit à une moins bonne solution que l'algorithme glouton B (10 spectacles contre 12 spectacles). En effet, le A choisit parfois de très longs spectacles (comme le 3A) ce qui est contradictoire avec le fait d'en voir beaucoup.
* (déjà dit lors du premier exercice) : On peut démontrer que l'algorithme glouton B fournit systématiquement une solution optimale (ainsi, sur cet exemple, on ne peut pas trouver de solution permettant de voir plus de 12 spectacles en entier).

### Exercice 6

On cherche à sélectionner __cinq__ nombres de la liste suivante (sélection) en cherchant à avoir __leur somme la plus grande possible__ (maximiser une grandeur) et en s'__interdisant de choisir deux nombres voisins__ (contrainte).

| 15 | 4 | 20 | 17 | 11 | 8 | 11 | 16 | 7 | 14 | 2 | 7 | 5 | 17 | 19 | 18 | 4 | 5 | 13 | 8 |

On écrira un algotithme qui renvoie la liste des nombres choisis ainsi que la somme obtenue

On donne ci-dessous la liste réprésentant les chiffres : 


```python
L = [15, 4, 20, 17, 11, 8, 11, 16, 7, 14, 2, 7, 5, 17, 19, 18, 4, 5, 13, 8]
```

## Complément

### exercice sur le problème du sac à dos

Pour chacune des stratégies gloutonnes de l'exercice 1, trouver des situations dans lesquelles la valeur emportée obtenue via l'algorithme glouton est aussi éloignée que possible de la valeur optimale.  
(Source : Balabanski)
6+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
