# Chap 2_2 : Représentation des entiers relatifs

---
Capacités attendues :

* savoir réaliser une addition binaire
* savoir ce qu'est le dépassement de capacité lors d'une addition binaire
* savoir quels sont les entiers relatifs représentables sur n bits
* savoir déterminer la représentation d'un entier relatif en utilisant le complément à 2
* savoir combien de bits seront nécessaires au minimin pour la représentation d'un entier relatif en utilisant le complément à 2
* connaissant la représentation avec le complément à 2 d'un entier relatif, savoir déterminer si cet entier est positif ou négatif et savoir déterminer la valeur de cet entier.

---



* [fichier pdf du cours](./21_22_cours_eleve_Représentation des entiers relatifs.pdf) 

* [TD](./TD/2_2_TD_representation_relatifs.md)
* [corrigés du TD](./TD/corriges/) : 4 fichiers pdf
