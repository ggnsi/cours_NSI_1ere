# Exercices sur la représentation des entiers relatifs

### Exercice 1

Réaliser en les posant les additions binaires suivantes. Indiquer pour chacune d'elle si, en travaillant sur 6 bits il se produirait un dépassement de capacité et si oui, quel serait le résultat retourné.

1. `100101` + `011010`
2. `110011` + `010100`
3. `101111` + `111110`
4. `011010` + `111101`


### Exercice 2

1. Quels sont les entiers relatifs qu'il est possible de représenter en utilisant le complément à deux :


    a. sur 7 bits ?
    b. sur 8 bits ?
    c. sur 9 bits ?

2. 256 est-il représentable en tant qu'entier relatif (en utilisant le complément à 2) :


    a. en travaillant sur 7 bits ?
    b. en travaillant sur 8 bits ?
    c. en travaillant sur 9 bits ?



### Exercice 3

Pour chaque entier relatif suivant, déterminer le nombre de bits minimum à utiliser afin qu'il soit représentable en utilisant le complément à 2 :

    1. -48
    2. -3504
    3. 1715
    4. 2048


### Exercice 4

On a représenté en machine sur un octet les entiers relatifs suivants en utilisant le  complément à 2 (l'espace mis au centre vise just à améliorer la lisibilité).

Préciser ceux qui sont :

    * positifs/négatifs ;
    * pairs/impairs

1. `1011 0111`
2. `0110 1010`
3. `1110 1110`

### Exercice 5

On travaille sur 8 bits.  
Déterminer la représentation en complément à deux des entiers relatifs suivants :

1. 53
2. -75
3. -85

### Exercice 6

On travaille sur 6 bits.  
Déterminer la valeur décimale des entiers relatifs dont la représentation en complément à deux est :

1. `101111`
2. `010111`
3. `110101`

### Exercice 7

Dans cet exercice, on travaille sur un octet et on utilise le complément à 2 pour représenter les entiers relatifs.

Réaliser à la main les opérations suivantes et donner le résultat obtenu.

1. `53 - 75`
2. `-27 + 61`
3. `-19 - 79`

_Aide_ : Pensez à ré-utiliser les résultats obtenus à l'exercice 5. Quelques résulats complémentaires sont donnés ci-dessous :

* 27 s'écrit `11011` en base 2
* 61 s'écrit `111101` en base 2
* 19 s'écrit `10011` en base 2
* 79 s'écrit `1001111` en base 2

### Exercice 8

En utilisant le complément à 2, un entier relatif `x` est stoqué sur 8 bits par `1011\ 0111`.

Il est (plusieurs réponses possibles) :

1. pair ;
2. négatif ;
3. son opposé `-x` est représenté par `0100\ 1000` ;
4. son opposé `-x` est représenté par `0100\ 1001`.


### Exercice 9

Quel est le complément à deux sur 8 bits des nombres suivants dont on donne la représentation :

1. `10110100` ;
2. `01101100` ;
3. `00000000`.

