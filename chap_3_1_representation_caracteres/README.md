# Représentation des caractères en machine
---

Les exercices s'intercalent entre les différentes parties de cours (voir les indications dans le cours)

* [introduction](./3_1_representation_caractere_machine_intro.md)
* [Cours](./3_1_representation_caractere_machine_cours.md)
* [TD](./TD/3_1_TD_representation_caracteres.md) et le [corrige](./TD/corrige/3_1_TD_representation_caractere_machine_corrige.md)
* [QCM](./TD/3_1_QCM_enonce_encodage_des_textes.pdf) et [corrigé du QCM](./TD/3_1_QCM_corrige_encodage_des_textes.pdf)
