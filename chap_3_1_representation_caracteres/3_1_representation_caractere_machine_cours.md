# Représentation d'un texte en machine

_Un éditeur hexadécimal est une interface (logiciel, application ou page web) qui permet de visualiser et éditer le contenu exact d'un fichier._

L'éditeur que nous utiliserons est une page web dont voici le lien : [Editeur hexadécimal en ligne](https://hexed.it/) : https://hexed.it/

## Premiers encodages : ASCII et ISO-8859-1
---

### L'encodage ASCII (ou latin1)

Le codage _ASCII_ (American Standard Code for Information Interchange) a été initialement
conçu en 1963, puis modifié en 1967 pour inclure les lettres minuscules. Il est devenu la norme
ISO 646 en 1983.

>Dans le codage ASCII, tous les codes sont des mots binaires de _7 bits_.  
>On peut donc représenter `2^7 = 128` caractères.

Dans la pratique les ordinateurs traitent les bits par 8 (octet).

>__Un bit toujours égal à 0 est ajouté en huitième position des codes précédents__ (la première dans le sens de lecture).  
Les codes ASCII correspondant alors aux nombres de 0 à 127 codés en binaire.

![Table Ascii](./fig/table_ascii.png)

_Remarque_ : _LSB_ : Low Significant Bit ou bit de poids faible et _MSB_ : Most Significant Bit ou bit de poids fort.

> _Exemple_ : Le caractère Z sera codé `101 1010` (sur 7 bits) ou `0101 1010` (sur 8 bits), soit `5A` en hexadécimal.

**Même table présentée autrement**

<img src="./fig/table_ascii_decimal.png" width="650">


Le code ASCII comprend :

* les 26 lettres latines en version majuscule et minuscule ;
* les 10 chiffres décimaux, l’espace (`SP`) ;
* les symboles de ponctuation et de parenthésage.
* Les 32 premiers caractères sont des codes de formatage, par exemple : 
    * code `0D` : retour chariot (CR : carriage return),
    * code `0A` : passage à la ligne (LF : Line Feed, nouvelle ligne), 
    * code `09` :tabulation horizontale (HT : horizontal tabulation), 
* des caractères non imprimables (tel `NUL` (code hexa `00` qui sert à marquer la fin d'une chaîne de caractères) ;
* des caractères correspondant à des actions : 
    * `BS` (code `08`) ; backspace, retour en arrière
    * `ESC` (escape, échappement),
    * `CAN` (cancel, annulation) ;
* des caractères de communication (ces derniers n'ont plus vraiment d'utilité actuellement mais demeurent...): début d’en-tête de transmission (SOH), début de texte transmis (STX), ... 

**Problème**

> Le codage _ASCII_ **ne permet pas** de coder les lettres latines accentuées ni les caractères propres à certains alphabets (arabe, syrillique, grec, hébreu).

---
### L'encodage [ISO-8859-1](https://fr.wikipedia.org/wiki/ISO/CEI_8859-1)
---

À la fin des années 1980, sont définis **les** encodages ISO-8859 qui sont _diverses extensions_ du codage ASCII.

Ces encodages utilisent **8 bits** par caractère, soit `2^8 = 256` codes possibles et donc  128 de plus que l'_ASCII_.

> __Chaque caractère est donc codé sur 1 octet.__

L'encodage **ISO-8859-1** :
* est une extension de l'encodage _ASCII_ (on y retrouve donc le code ASCII aux mêmes emplacements)
* ajoute des caractères supplémentaires utiles pour la langue française par exemple (lettres accentuées, \ae).


Attention : dans la table ci-dessous, le bit de poids forts est indiqué sur la ligne (et pas sur la colonne comme le tableau précédent)

<img src="./fig/table-iso8859-1_complet.png" width="600">

_Remarques_ :

* l'encodage dit **ANSI** (ou _Windows-1252_ ) est une extension de l'ISO-8859-1 introduite et diffusée par _Microsoft_ avec son utilisation dans _Windows 3.x_  
Les caractères autres que ceux entre les adresse 7A et A0 sont identiques à l'ISO-8859-1.
* Il existe d'autres variantes adaptées à d'autres langues :
    * ISO-8859-2 qui correspond à  la plupart des langues slaves d’Europe de l’Est utilisant l’alphabet latin : allemand,  croate,  hongrois,  polonais, roumain, slovaque, slovène et tchèque. ;
    * ISO-8859-5 qui inclut l’alphabet  cyrillique  est  utilisé  en  bulgare,   biélorusse, macédonien, russe, serbe et ukrainien. ;
    * ...

**Voici par exemple la table ISO-8859-5**

<img src="./fig/iso8859_5.jpg" width="400">


### Exercices

> Faire les exercices 1 à 4 de la [feuille de TD](./TD/3_1_TD_representation_caracteres.md)

---
### [Unicode](https://home.unicode.org/) et une de ses implémentation : UTF-8
---

**Les encodages ISO-8859-x posaient des problèmes** :

* la pagaille : chaque pays ou groupe linguistique avait sa propre page de code ;
* les échanges internationaux de fichiers posaient donc problème (bien sélectionner l'encodage utilisé par l'émetteur !) ;
* un même code pouvait signifier des caractèrs différents en fonction de l'encodage. Par exemple, pour le même code, le symbole `$` aux Etats-Unis était celui de la livre `£` au Royaume-Uni ... Dangereux !
* comment coder dans un même document un mot dans différentes langues n'utilisant pas le même encodage ISO-8859-x ?

Par exemple le mot `paix` dont les écritures sont :

* arabe : السلم 	
* français : paix 	
* grec : ειρήνη
* hébreu : שלום 	
* japonais : へいわ 	
* russe : мир
* symbole : ☮ 	
* tchèque : mír 	
* thai : ความสงบสุข

**La solution : UNICODE**

Grace à l'augmentation de la puissance des machines et de la mémoire disponible, l'idée d'un encodage universel est apparue au début des années 1990 : c'est le but de la norme _UNICODE_

**UNICODE** associe chaque caractère à un **point de code** qui est :
* en décimal un entier compris entre 0 et `1 114 111`
* en hexadécimal entre 0 et 10FFFF 
* en binaire entre 0 et 10000 11111111 11111111 (utilisation de 1 à 21 bits) 


> Les points de code sont noté `U+xxxx` où `xxxx` est l'écriture hexadécimale du point de code.

_Exemple_ : le point de code du symbole `€` est `U+20AC`, soit en écriture binaire `00100000 10101100`

>Les points de code des caractères de l'encodage ISO-8859-1 sont les mêmes que leur code ISO-8859-1. 

Par exemple le point de code du caractère `V` est `U+0056`.

Dans sa version 13 du 10 mars 2020, cet encodage compte près de $150\ 000$ caractères

* voir par exemple [ici](https://unicode-table.com/fr/blocks/) : https://unicode-table.com/fr/blocks/

* ou regarder [cela](https://unicode-table.com/fr/sets/) : https://unicode-table.com/fr/sets/

> **Attention** : __Unicode définit une liste de caractères et un code mais ne définit pas la manière donc ces codes seront représentés dans un fichier.__

__Quel encodage utiliser ?__

> **Première idée** : Utiliser systématiquement 3 octets.

_Exemple_ avec `€` de code `U+20AC` ou en binaire `00100000 10101100`.
On complèterait à 3 octets : `00000000 00100000 10101100`.

_Problème_ : on utilise systématiquement 3 octets là où 1 ou 2 pourraient suffire.

> **Deuxième idée**  utiliser **l'UTF-8**

L'encodage UTF-8 **utilise un nombre d'octet variable** pour les différents caractères en fonction de l'importance de l'utilisation du caractère.

**UTF-8 utilise entre 1 et 4 octets pour coder un caractère**.

Voici le principe de l'encodage : 


| Plage              | Suite d'octets (en binaire) | bits codants | Remarque   |
|:------------------:|:---------------------------:|:------------:|:----------:|
| U+0000 à U+007F    | `0xxxxxxx`                            | 7 bits | codage sur 1 octet, compatible ASCII |
| U+0080 à U+07FF    | `110xxxxx 10xxxxxx`                   | 11 bits | codage sur 2 octets |
| U+800 à U+FFFF     | `1110xxxx 10xxxxxx 10xxxxxx`          | 16 bits | codage sur 3 octets |
| U+10000 à U+10FFFF  | `11110xxx 10xxxxxx 10xxxxxx 10xxxxxx` | 21 bits | codage sur 4 octets |

Les bits codants correspondent à l'écriture binaire du point de code **éventuellement complété à gauche par des `0` pour obtenir le nombre de bits codants**.

_Exemple avec le symbole `÷` dont le point de code unicode est `F7`_ :

* `F7` est compris entre `0080` et `07FF` : on utilise la deuxième ligne du tableau.
* on aura besoin de 11 bits codants.
* on convertit `F7` en écriture binaire : `11110111`
* cette écriture ne contient que 8 bits, on en ajoute donc 3 à gauche ;
* bits codants : `00011110111`
* d'après le tableau, on sépare ces 11 bits codants en un groupe de 5 et un groupe de 6 : `00011 110111`
* L'encodag UTF-8 du symbole `÷` sera donc `11000011 10110111`, soit `C3B7` en hexadécimal

On peut retrouver cela la [page correspondante du site unicode-table.com](https://unicode-table.com/fr/00F7/)

_Exemple_ avec le symbole `€` :


* point de code : `U+20AC`
* `20AC` est compris entre `800` et `FFFF` : on utilise la troisième ligne du tableau et on encode ce caractère sur 3 octets. Il y a 16 bits codants ;
* écriture binaire de `20AC` : `00100000 10101100` : on a déjà ici 16 bits (car on a écrit les zéros à gauche du 1 lors de la converstion hexadécimal -> binaire) ;
* On utilise le codage indiqué (3ème ligne) : __1110__0010 __10__000010 __10__101100

**Remarque**

D'autres implémentation d'UNICODE existent, comme par exemple l'_UTF-16 (que Windows utilise maintenant en interne)_ ou l'_UTF-32_.

_Exemple_ : visualiser les différents encodages de la [Lettre majuscule latine A](https://unicode-table.com/fr/0041/) : https://unicode-table.com/fr/0041/



### Détection d'encodage

Un problème posé par la diversité des encodages existants est la détermination de l’encodage utilisé par un fichier. Les renseignements associés à un fichier particulier (sa date de création par exemple) n’indiquent rien sur son encodage. On doit donc tenter de le « deviner », au moyen d’algorithmes compliqués qui analysent le contenu du fichier. Ces algorithmes sont efficaces la plupart du temps, mais peuvent échouer, et sont compliqués.
C’est ce qui explique les affichages bizarres de certains fichiers ou pages web : le programme n’a pas réussi à déterminer le bon encodage.

Un moyen plus simple serait d’inclure cette indication directement dans le contenu du fichier, au tout début (afin de diminuer les risques de perturbation). On utilise pour cela le fait que tous les encodages actuels soient compatibles avec l’ASCII. [^1]

### Exercices

> Faire les exercices 5 et 6 de la [feuille de TD](./TD/3_1_TD_representation_caracteres.md)

---
## Un peu de Python
---

Python utilise l'UTF-8 comme encodage.

Il est possible d'utiliser les points de code Unicode en python en utilisant le caractère d'échappement `\u` suivi du code hexadécimal.

```Python
chaine = '\u00D7'
```
 
>La fonction ord() renvoie le code Unicode d'un caractère en base dix.
>
> `val_unicode = ord('x')`
>
>La fonction chr() renvoie le caractère correspondant à la valeur passée en paramètre.
>
>    `caract = chr(0xD7)`

_Exemple_ : 

* `ord('€')` renvoie 8364 ;
* `chr(8364)` renvoie la chaîne de caractère comportant le  symbole € ;
* `chr(0x20AC)` fait la même chose (on a écrit la valeur du point de code en hexadécimal)
* `'\u20AC'` est une chaîne de caractère comportant le  symbole €.

_Remarque_ : Parfois le caractère obtenu par la fonction chr se présente sous forme d’un carré : cela se produit lorsque le dispositif qui produit l’affichage ne connaît pas le glyphe associé au caractère. Un glyphe est une représentation graphique d’un signe typographique.

### Exercices

> Faire les exercices 7 et suivants la [feuille de TD](./TD/3_1_TD_representation_caracteres.md)

__Autres sites intéressants :__

* https://fr.wikipedia.org/wiki/ISO/CEI_8859-1

* https://fr.wikipedia.org/wiki/Windows-1252

* https://zestedesavoir.com/tutoriels/pdf/1114/comprendre-les-encodages.pdf

* https://www.fil.univ-lille1.fr/~L1S1Info/Doc/HTML/seq7_codage_caracteres.html

__Références :__

image du code Morse issu de wikipedia
