# Représentation d'un texte en machine

_Un éditeur hexadécimal est une interface (logiciel, application ou page web) qui permet de visualiser et éditer le contenu exact d'un fichier._

L'éditeur que nous utiliserons est une page web dont voici le lien : [Editeur hexadécimal en ligne](https://hexed.it/) : https://hexed.it/


## Introduction

Comment se transmettre des messages quand l'utilisation de l'écriture n'est pas possible ?

Par exemple quand l'on souhaite se transmettre un message sur de longues distances ?

[Claude Chappe](https://fr.wikipedia.org/wiki/Claude_Chappe) (1763-1805) invente le sémaphore, adopté pour transmettre rapidement des messages à l'aide de stations relais régulièrement situées.

<img src="./fig/126827685.jpg" width="200">

[Samuel Morse](https://fr.wikipedia.org/wiki/Samuel_Morse) (1791-1872) invente le télégraphe électrique.

<img src="./fig/morse_telegraphe.jpg" width="200">

>Pour fonctionner, ces systèmes nécessitent :
>
>* que soit décidée une représentation (un code) pour chaque caractère ou chiffre (système d'encodage) ;
>
>* que l'émetteur et le récepteur utilisent le même encodage (on peut en effet imaginer de multiples encodages)


Pour les sémaphores, le [code Chappe](https://fr.wikipedia.org/wiki/Code_Chappe) a été utilisé et pour le télégraphe électrique c'est le [code Morse](https://fr.wikipedia.org/wiki/Code_Morse_international).

<img src="./fig/code_chappe.svg" width="200"><img src="./fig/International_Morse_Code.png" width="200">


> **Le stockage d'un texte en machine pose les mêmes problématiques** :

>1. le besoin d'une représentation des caractères qui puisse être utilisée (système d'encodage) ;
> 2. Il y a besoin de connaître le système d'encodage utilisé pour pouvoir décoder correctement un fichier (plusieurs encodages peuvent exister).

**Définition issue de Wikipédia**

_« Le **codage des caractères** est une convention qui permet, à travers un codage connu de tous, de transmettre de l’information textuelle, là où aucun support ne permet l’écriture scripturale. Cela consiste à représenter chaque caractère, par un motif visuel, un motif sonore, ou une séquence abstraite. Les techniques des ordinateurs reposent sur l’association d’un caractère à un nombre. »_ (source Wikipedia)
