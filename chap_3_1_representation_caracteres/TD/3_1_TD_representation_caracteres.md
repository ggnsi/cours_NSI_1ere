# TD - Représentation d'un texte en machine
---

## Editeur hexadécimal utilisé pour le TD

_Un éditeur hexadécimal est une interface (logiciel, application ou page web) qui permet de visualiser et éditer le contenu exact d'un fichier._

L'éditeur que nous utiliserons est une page web dont voici le lien : [Editeur hexadécimal en ligne](https://hexed.it/) : https://hexed.it/


---
### Exercice 1 : encoder un texte

On s'intéresse au texte `Hello !` encodé à l'aide de la norme **ISO-8859-1**.

<img src="./img/table-iso8859-1_complet.png" width="600">

1. Combien d'octets et de bits seront nécessaires ?
2. Donner la représentation de ce texte en hexadécimal.
3. Donner la représentation de ce texte en binaire.

>Vérifier votre réponse avec l'[éditeur hexadécimal](https://hexed.it/) en inscrivant `Hello !` en haut à gauche de la colonne de droit (celle avec les points).  
Vous avez l'encodage en hexadécimal à gauche ainsi que les valeurs binaires correspondantes en bas dans la colonne de gauche. 

### Exercice 2 : décoder un fichier

On s'intéresse au texte encodé à l'aide de la norme **ISO-8859-1** dont la représentation en binaire est la suivante :

`00111010 00101101 00101001`

1. Combien de caractères contient ce texte ?
2. Retrouver ce texte.

Vous pouvez vérifier à l'aide de l'éditeur hexadécimal en entrant pour chaque caractère le code binaire (cliquer dans les cases rondes).

### Exercice 3 : décoder un fichier 

1. Téléchargez et sauvegardez dans votre répertoire personnel le fichier [codage_fichier_1.txt](./fichiers/codage_fichier_1.txt)
2. Ouvrir ce fichier à l'aide de l'éditeur `Notepad++` (bouton droit -> ouvrir avec) et, **sans rien modifier**, déterminer combien d'octets seront utilisés pour représenter ce texte.
3. A l’aide de l’Explorateur de votre système d'exploitation (clic droit Propriétés sous Windows) vérifiez si la taille exacte occupée (en octets) par ce fichier correspond bien à votre réponse précédente.
4. Ajoutez un saut de ligne (retour à la ligne) juste avant le mot `peut`, enregistrez le fichier modifié et __déterminez sa nouvelle taille sur le disque ?__  Combien d'octets supplémentaires ont-ils été utilisés ?
5. Ouvrez votre fichier modifié dans l'éditeur hexadécimal :
    * Quels sont les codes hexadécimaux des caractères ajoutés dans le fichier par le passage à la ligne ?
    * En cherchant dans la [table ASCII](./img/table_ascii_decimal.png), déterminer à quels caractères correspondent ces codes.
6. Dans le menu Affichage de Notepad++ sélectionnez : Symboles spéciaux > Afficher tous les caractères et retrouvez votre résultat précédent
7. Recherchez (en utilisant un moteur de recherche) l'origine de ces caractères (en fait ce sont des acronymes de mots anglais...) __et__ la manière dont sont codés les sauts de ligne sous 3 types de systèmes d'Exploitation (Windows, MacOS, Linux) 

### Exercice 4 : décoder un fichier 

Décoder le message suivant (codage ASCII):

```
01001100 01100101 00100000 01100010 01101001 01101110 01100001 01101001 01110010 01100101 00101100 00100000 01100011 00100111 01100101 01110011 01110100 00100000 01101001 01101110 01101000 01110101 01101101 01100001 01101001 01101110 00101110
```

### Exercice 5 [^2]

Le point de code du symbole `⤳` a la valeur décimale 10 547.

1. Convertir cette valeur en hexadécimal.
2. Combien d’octets seront utilisés en UTF-8 pour coder ce symbole ?
3. Donner le codage UTF-8 (en binaire).
4. Reprendre l'exercice pour déterminer le codage UTF-8 du symbole `ξ` (lettre grecque Xi minuscule) dont le point de code est `3BE`
5. Reprendre l'exercice pour déterminer l'encodage UTF-8 de l'émoticon 😂 dont le point de code est `U+1F602`

### Exercice 6

1. Ouvrir le **bloc-note de Windows**, y inscrire le mot `Codé` puis enregistrer le fichier (Enregistrer Sous) sous forme de fichier texte (.txt) en **sélectionnant** l'encodage **ANSI** (nom de fichier : `code_ansi.txt`), c'est à dire l'encodage ISO-8859-1 "enrichi".

Ré-enregistrer le fichier (Enregistrer Sous) sous forme de fichier texte avec l'encodage UTF-8 (nom de fichier : `code_utf8.txt`)

2. Ouvrir le fichier `code_ansi.txt` avec l'éditeur hexadécimal.  
Comment a été encodée la lettre `é`  ?  
Est-ce normal d'après la [table d'encodage ANSI](./img/table-iso8859-1_complet.png) ?

3.  * Le point de code Unicode de `é` correspond à son encodage ANSI. Quel est donc le point de code Unicode de `é` ?
    * Déterminer alors l'encodage UTF-8 de `é` en utilisant le principe d'encodage en UTF-8 (voir tableau du cours) et retrouver le résultat observé dans l'éditeur hexadécimal.
 
4. Supposons que l'on ouvre le fichier `code_utf8.txt` en pensant que l'encodage utilisé est ISO-8859-1 (ou ANSI). Le logiciel décode alors chaque octet comme un caractère ave la table ISO-8859-1.  
Déterminer quels seront les caractères affichés ?

5. Ouvrir le fichier `code_utf8.txt` avec l'éditeur hexadécimal et vérifier votre résultat.

5. Expliquer l'image suivante :

![Martine](./img/martine_utf_8.jpg)


6. Quels seront les caractères affichés à l'écran lorque mon navigateur ouvre en utilisant un encodage ISO-8859-1 un fichier qui a été encodé en UTF-8 et qui contient la lettre `à` ?

7. Dans `Notepad++`, créer un nouveau fichier et écrire `éàc`.  
Dans Encodage, sélectionner `ANSI` et retrouver vos résultats précédents.





### Exercice 7

1. En utilisant les fonctions python, déterminer quel est l'**écriture hexadécimale** du point de code Unicode :
* du caractère `ù`, 
* de la lettre majuscule cyrillique Elle : `Л` (faire un copier/coller du caractère)

2. En utilisant les fonctions python, déterminer quel est le caractère dont les codes Unicode sont donnés. Vérifier sur le site Unicode ensuite. 

* `1F607`
* `1F92A`
* `a21C`

### Exercice 8

[QCM à réaliser](./3_1_QCM_enonce_encodage_des_textes.pdf) et (ensuite seulement !), vérifier vos réponses avec [le corrigé](./3_1_QCM_corrige_encodage_textes.pdf)

### Exercice 9

L'espace Unicode est divisé en 17 zones de 65 536 points de codes. Ces zones sont appelées plans eux même divisés en sous-ensembles. Vous pouvez les retrouver à l’adresse suivante : [Unicode tables](http://www.unicodetables.com/)

Le sous-ensemble __Cyrillic__ est par exemple situé entre les points de code `U+0400` et `U+04FF`

1. Obtenez via la console l'affichage des deux premiers caractères du sous-ensemble Cyrillic.

2. Ecrivez une fonction `cyrillic()` qui affiche tous les symboles du sous ensemble Cyrillic les uns derrière les autres séparés par un espace. Ce sous ensemble se compose de 256 caractères.
  
_Remarque_ : il est bien sûr possible d'affecter une valeur hexadécimale à une variable, par exemple :
```Python
variable = 0xA0
```

**Alphabet Cytillic pour vérifier (il manque le dernier caractère sur cette image)**

<img src="./img/cyrillic.jpg" width="650">

### Exercice 10

Créer une fonction `codes(chaine)` qui affiche la liste des codes hexadécimaux ISO-8859-1 de chacun des caractères (séparés par un espace !)

_Exemple_ : `codes('Froid')` renvoie `0x46 0x72 0x6f 0x69 0x64`

On pourra s'intéresser à la fonction native `hex()`

### Exercice 11 (méthode _upper_ interdite)

**Observons la table ASCII ci-dessous**

* combien ajouter au code le la lettre `A` pour obtenir celui de la lettre `a` ?
* combien ajouter au code le la lettre `B` pour obtenir celui de la lettre `b` ?

On peut remarquer qu'il y a un décalage constant entre le code d'une lettre majuscule et celui d'une lettre minuscule.

<img src="./img/table_ascii_decimal.png" width="650">

1. Ecrire une fonction `minuscule(lettre)` où lettre est chaine de caractère contenant une lettre majuscule qui **renvoie** la lettre minuscule correspondante.
2. Ecrire une fonction `majuscule(lettre)` où lettre est chaine de caractère contenant une lettre minuscule qui **renvoie** la lettre majuscule correspondante.
3. Ecrire une fonction `mise_maj(mot)` où `mot` est une chaine de caratère contenant un mot qui renvoie la chaîne où toutes les lettres de `mot` ont été mises en majuscule.
3. Ecrire une fonction `mise_maj(mot)` où `mot` est une chaine de caratère contenant un mot qui renvoie la chaîne où toutes les lettres de `mot` ont été mises en majuscule.

_Elements de correction_

```Python
>>> mise_maj('bonjour')
'BONJOUR'
>>> mise_maj('BonjOur')
'BONJOUR'
```

_Remarque_ : pour tester si une chaine de caractères ne contenant qu'un élément est une lettre comprise entre `a` et `z`, on peut utiliser :
```Python
((lettre >= 'a') and (lettre <= 'z'))
```

_Exemples_ :

```Python
>>> (('g' >= 'a') and ('g' <= 'z'))
True
>>> (('B' >= 'a') and ('B' <= 'z'))
False
>>> (('(' >= 'a') and ('(' <= 'z'))
False
```

4. Quelle méthode des chaînes de caractères permet de réaliser la même chose ? Tester sur des exemples.

### Exercice 12 : Mini-projet 1 : Codage de César

Le code César est une méthode de cryptage qui consiste à décaler chaque lettre de l'alphabet d'un certain rang.  
Par exemple le décalage de César-3 remplace la lettre A par la lettre D, le B par le E, ..., le X devient A, le Z devient B.

Autre exemple, dans un décalage de César-13, le mot python deviendra clguba.

1. Créer une fonction `decalage(lettre, rang)` (où `lettre` est une chaîne contenant un caractère et `rang` un entier compris indiquant le décalage à réaliser) qui _retourne_ une chaîne de caractère contenant la lettre décalée.

_Aide_ : il sera peut être intéressant d'utiliser la fonction `%` déjà utilisée.

2. Créer une fonction `cod_cesar(texte, rang)` qui _retourne_ le texte codé avec le codage de césar dont `rang` est le décalage utilisé.

3. Créer dans le script un programme principal de façon à ce que via un menu :

    * l'utilisateur puisse choisir s'il souhaite chiffrer ou déchiffrer un texte,

    * l'utilisateur puisse entre le texte qu'il souhaite transformer,

    * le programme affiche le texte transformé.

_Prolongement_ : Vérifier que les caractères entrés sont uniquement des caractères minuscules de a à z et des espaces.

### Exercice 13 : Mini-projet 2 : Mise en forme  &#x1F3C6;&#x1F3C6;

Les données fournies par les utilisateurs à travers de nombreux formulaires présents sur les pages web sont souvent mises en forme pour être stockées ou réutilisées.

![Exemple de formulaire web](./fig/Sample_web_form.png)

[^1]

Cet exercice aborde ce problème et doit permettre à terme de réaliser une fonction `name_format` acceptant en paramètre une seule chaîne de caractères représentant le prénom et le nom d'une personne (séparés par un espace) `'prenom nom'` et renvoyant une chaîne mise en forme où seules la première lettre du prénom et toutes les lettres du nom seront en majuscules.  
On n'**utilisera pas** ici la méthode `split()`.

```python
>>> name_format('aLaN TUring')
'Alan TURING'
```

Nous allons décomposer le problème

__1)__ Définissez une fonction `firstname_format` :
* acceptant un paramètre un prénom sous forme de séquences de caractères  
* renvoyant le prénom mis en forme (uniquement la première lettre en majuscule)  

```python
>>> firstname_format('alAN')
'Alan'
```


__2)__ Définissez une fonction `lastname_format` :
* acceptant un paramètre un nom sous forme d'une séquence de caractères  
* renvoyant le nom mis en forme (toutes les lettres en majuscule) 

```python
>>> lastname_format('turing')
'TURING'
```

__3)__ Définissez une fonction `find_firstname` :
* acceptant un paramètre un nom complet sous forme d'une séquence de caractères  
* renvoyant uniquement le prénom  

```python
>>> find_firstname('aLaN TUring')
'aLaN'
```

__4)__ Définissez une fonction `find_lastname` :
* acceptant un paramètre un nom complet sous forme d'une séquence de caractères  
* renvoyant uniquement le nom  

```python
>>> find_firstname('aLaN TUring')
'TUring'
```

__5)__ Enfin, utilisez les fonctions précédentes pour définir la fonction `name_format` souhaitée.  

__6)__ 🥇 __Complément__   
 On peut améliorer la fonction `firstname_format` pour tenir compte des prénoms composés.

```python
>>> firstname_format_bis('marie-antoinette')
'Marie-Antoinette'
```


### Exercice 

Combien de caractères Unicode, encodés en UTF-8, chacune des séquences d’octets-ci dessous (en écriture décimale) représente-elle ?

* 107 110 045
* 234 133 188
* 212 184 88 120
* 226 190 133


__Références__

[^1] Issu de [ce site](http://sdz.tdct.org/sdz/comprendre-les-encodages.html)  
[^2] Issu sur [ce site](https://info.blaisepascal.fr/nsi-caracteres-et-textes)
