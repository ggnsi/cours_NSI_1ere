# j'ai mis plusieurs lignes pour expliquer.
# on peut bien évidemment faire plus court
def minuscule(lettre):
    point_code_depart = ord(lettre)
    point_code_final = point_code_depart + 32
    return chr(point_code_final)


def majuscule(lettre):
    point_code_depart = ord(lettre)
    point_code_final = point_code_depart - 32
    return chr(point_code_final)

def mise_maj(mot):
    sortie = ''
    for lettre in mot:
        if (lettre >= 'a') and (lettre <= 'z'):
            sortie += majuscule(lettre)
        else:
            sortie += lettre
    return sortie

# chaine.upper()