### Premières mises en oeuvre

#### Exercice 1 : encoder un texte

1) 7 octets donc 56 bits

2) `48 65 6C 6C 6F 20 21`

3) `01001000 01100101 01101100 01101100 01101111 00100000 00100001`

#### Exercice 2 : décoder un fichier

1) 3 caractères

2) `:-)`


#### Exercice 3 : décoder un fichier 

1. 
2. 31 octets
3. 
4. 33 octets
5.  
    * `0D 0A`
    * `CR LF` : CARRIER RETURN (retour chariot) et LINE FEED (saut de ligne)
6. 
7. Sous MacOS : CR seul et sous Linux : LF seul
    

#### Exercice 4 : décoder un fichier 

Décoder le message suivant (codage ASCII):

```
01001100 01100101 00100000 01100010 01101001 01101110 01100001 01101001 01110010 01100101 00101100 00100000 01100011 00100111 01100101 01110011 01110100 00100000 01101001 01101110 01101000 01110101 01101101 01100001 01101001 01101110 00101110
```

`4C 65 20 62 69 6E 61 69 72 65 2C 20 63 27 65 73 74 20 69 6E 68 75 6D 61 69 6E 2E`

`Le binaire c'est inhumain`

#### Exercice 5

1. `2933`
2. 3 octets et 16 bits codants
3. conversion de `2933` (hexadécimal) en binaire : `10100100110011`.  
  On complète à 16 bits `0010100100110011` que l'on sépare en groupes utilisés pour le codage : `0010` - `100100` - `110011`.  
  On encode : `11100010 10100100 10110011`
4. Réponse : `11001110 10111110`
5. Réponse :  `11110000 10011111 10011000 10000010`

#### Exercice 6

1.
2. `E9`
3. `E9`  
`E9 -> 11101001`  
On a besoin de 11 bits codants donc `00011101001` puis séparé en un groupe de 5 et un de 6 : `00011 101001`  
Le codage UTF-8 sera donc `11000011 10101001` soit en hexadécimal `C3 A9`
4. On obtiendra deux caractères qui seront (en regardant la table ISO-8859-1) : `Ã©`
5. En suivant la même démarche, le point de code unicode de `à` est `E0` (le même que celui en ISO-8859).  
`E0 -> 11100000` ce qui donne sur 11 bits : `00011100000` puis séparé en un groupe de 5 et un de 6 : `00011 100000`.  
Enfin l'encodage en UTF-8 : `11000011 10100000`.  
Si on décode en ISO-8859-1, on obtient 2 caractères dont les codes sont `C3 A0`, soit `Ã ` (le caractère de code `A0` correspond à un espace insécable (qui ne peut être coupé par un passage à la ligne)

#### Exercice 7

2. ```Python
>>> ord('ù')
249
```
3. ```Python
>>> ord('Л')
1051
```
5. ```Python
>>> chr(0x1F607)
'😇'
```
6. ```Python
>>> chr(0x1F92A)
'🤪'
```
7. ```Python
>>> chr(0xa21C)
'ꈜ'
```
