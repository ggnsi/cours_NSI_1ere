# Les chaînes de caractères : le type _string_

## Introduction

Les chaînes de caractères sont un cas particulier de _séquences_ , c'est à dire qu'elles sont constituées d'un ensemble fini et ordonné d'éléments.

Plus précisément, les chaînes de caractères sont des séquences _immuables_ de points de code Unicode.

Nous verrons d'autres séquences par la suite : listes, tuples.  
Les séquences ont un certain nombre de caractéristiques communes et certaines fonctions peuvent s'appliquer à différents types de séquence (ex : `len()`).

## I Généralités

### 1) Comment définir une chaîne de caractères ?

On peut les définir de plusieurs manières :

* soit par des _guillements simples_ :



```python
ch1 = 'ma chaine'
ch2 = 'Aller Lens, à bas Lille'
ch3 = 'Je suis le "meilleur" en maths.' #on peut utiliser des guillemets doubles à l'intérieur de guillemets simples
print(ch3)
```

    Je suis le "meilleur" en maths.
    

* soit par des _guillements doubles_ :


```python
ch1 = "ma chaine"
ch2 = "Aller Lens, à bas Lille"
ch3 = "Je suis le 'meilleur' en maths." #on peut utiliser des guillemets simples à l'intérieur de guillemets doubles
print(ch3)
```

    Je suis le 'meilleur' en maths.
    

* soit par des _guillements triples_ : """ ou '''  
Ces derniers peuvent contenir plusieurs lignes incluant des retours à la ligne.


```python
ch4 = """Voici un description de ce que fait la fonction
Elle prend comme paramètres deux entiers a et b
et renvoir leur somme"""
print(ch4)
```

**Utilisation de caratères d'échappement**

Certains caractères (on parle des caractères d'échappement) s'obtiennent à l'aide du symbole `\` (antislash) 

Python ne considère pas la contre-barre (ou anti-slash) comme un caractère mais comme une marque pour les caractères spéciaux.  
Employé dans une chaîne, il doit toujours être suivi d’un autre caractère.

| code utilisé | caractère obtenu |
|:--:|:--|
|`\n` | saut de ligne |
| `\t`| une tabulation |
|`\'` | une apostrophe (guillement simple) |
|`\"`| guillemet (guillemet double)|
|`\\`| un antislash|

* `\'` et `\"` ne ferment pas la chaîne.
* Pour le calcul des longueurs de chaînes, chacun de ces élément ne compte que pour _un seul caractère_.

_Exemples_ :


```python
a = "je suis \" bien\""
print(a)
```

    je suis " bien"
    


```python
b = 'je suis \'bien\''
print(b)
```

    je suis 'bien'
    


```python
a = "bonjour \n toto \t et tata"
print(a)
```

    bonjour 
     toto 	 et tata
    


```python
a = "bonjour \ntoto \tet tata"
print(a)
```

    bonjour 
    toto 	et tata
    

**Utilisation du point de code Unicode**

> _Syntaxe_ : `\u` suivi du point de code

_Exemple_ : 


```python
ch = "La lettre mu : \u00B5"
print(ch)
```

    La lettre mu : µ
    

### 2) longueur d'une chaine

>L'instruction `len(chaine)` renvoie la longueur de la chaine

_Rappel_ : les caractères d'échappement ne comptent que pour un seul caractère.

_Exemple_ :


```python
ch = "Toto !"
print(len(ch))
```

    6
    


```python
ch = "ab \n c"
print(ch)
print(len(ch))
```

    ab 
     c
    6
    

### 3) Comment accéder à un caractère d'une chaîne ?

>Si une chaîne comporte `n` caractères, ces derniers sont indicés (numérotés) de `0` à `n-1`.  
>`s[i]` renvoie le caractère de la chaine d'indice `i` (sous forme de chaîne).

> Attention : Si `i` est plus grand que l'indice du dernier caractère, une erreur survient

_Exemple_ : pour la chaîne `s = 'python'`

`len('python')` vaut `6` : les indices des caractères vont donc de 0 à `5`.

|Caractère|`'p'`|`'y'`|`'t'`|`'h'`|`'o'`|`'n'`| 
|:---:|:---:|:---:|:---:|:---:|:---:|:---:| 
|Indice|0|1|2|3|4|5|  

|instruction| résultat obtenu|
|:--:|:--:|
|`s[0]`| `'p'`|
|`s[1]`| `'y'`|
|`s[2]`| `'t'`|
|`s[3]`| `'h'`|
|`s[4]`| `'o'`|
|`s[5]`| `'n'`|




```python
s = 'python'
print(s[0])
print(s[3])
print(s[6])
```

    p
    h
    


    ---------------------------------------------------------------------------

    IndexError                                Traceback (most recent call last)

    <ipython-input-9-0a2db76cbf31> in <module>
          2 print(s[0])
          3 print(s[3])
    ----> 4 print(s[6])
    

    IndexError: string index out of range


### 4) Une chaîne de caractère est _immuable_

> Cela veut dire qu'il est impossible de modifier un caractère dans une chaine.  
> Essayer de le faire provoque une erreur.

_Exemple_ :


```python
s = 'python'
s[0] = 'G'
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-12-37565db0a3c1> in <module>
          1 s = 'python'
    ----> 2 s[0] = 'G'
    

    TypeError: 'str' object does not support item assignment


Si vous voulez "modifier" une chaîne, il faudra en re-créer une autre à partir de celle existante.

### 5) Opérations sur les chaînes : les opérateurs + et *

| opérateur | action |
|:--------:|:------:|
| ch1 + ch2| concatène (colle l'une à la suite de l'autre) les chaines ch1 et ch2 |
|n\*ch1 ou ch1\*n | concatène n fois ch1 |

_Exemples_ :


```python
a = 'Toto et'
b = 'Tata'
print(a + b)
print(a*3)
```

    Toto etTata
    Toto etToto etToto et
    

### 6) test d'appartenance : `in` et `not in`

> `'souschaine' in 'chaine'` renvoie `True` si la souchaine est présente dans la chaîne et `False` sinon.
> `'souschaine' not in 'chaine'` fait le contraire !

_Exemples_ :


```python
voyelles ="aeiouy"

print('o' in voyelles)
print('t' in voyelles)
print('t' not in voyelles)
```

    True
    False
    True
    


```python
voyelles = "aeiouy"

print('ou' in voyelles)
print('oy' in voyelles)
```

    True
    False
    


```python
voyelles = 'aeiouy'
mot = 'python'
premiere_lettre = mot[0]
if premiere_lettre in voyelles:
    print('La première lettre est une voyelle')
else:
    print('La première lettre est une consonne')
```

    La première lettre est une consonne
    

### 6) Conversions

On peut convertir une chaine de caractères ne contenant que des caractères numériques en un nombre entier ou flottant, en utilisant les fonctions int et float.

__Attention__ : il faut que la chaine de caractères initiale représente bien un nombre.

_Exemple_ :


```python
print(int("57"))
print(float("34.0"))
```

    57
    34.0
    


```python
print(int("57A"))
```


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-15-85b99ffc279e> in <module>
    ----> 1 print(int("57A"))
    

    ValueError: invalid literal for int() with base 10: '57A'


Inversement, il est possible de convertie un entier ou un flottant en chaîne de caractères à l'aide de l'instruction `str`.

_Exemple_ :


```python
print(str(34))

print(str(43.7))
```

    34
    43.7
    

### pour aller plus loin

* Les slices (ou extractions de fragments de chaîne).  
Voir le site [Ostralo](http://numerique.ostralo.net/python_AideMemoire/06a_ChainesDeCaracteres.htm)  
ou le site [Zestedesavoir](https://zestedesavoir.com/tutoriels/582/les-slices-en-python/)
