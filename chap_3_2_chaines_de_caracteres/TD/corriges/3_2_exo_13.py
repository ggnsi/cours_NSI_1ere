def remplace(chaine, char1, char2)
    sortie = ''
    for lettre in chaine:
        if lettre != char1:
            sortie = sortie + lettre
        else:
            sortie = sortie + char2
    return sortie