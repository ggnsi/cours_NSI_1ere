# solution 1 : ré-utiliser la fonction verlan

def verlan(chaine):
    sortie = ''
    for lettre in chaine:
        sortie = lettre + sortie
    return sortie

# on teste si la chaine est la même que la chaîne obtenue en verlan, c'est à dire si on a un palindrome

def palind1(chaine):
    return chaine == verlan(chaine)



# solution 2 : on teste la première lettre est la même que la dernière (celle d'incice 0 la même que celle d'incide longueur - 1 - 0)
# puis la deuxième avec l'avant dernière (celle d'incice 1 la même que celle d'incide longueur - 1 - 1)
# etc.
# de manière générale, si celle d'indice indice est la même que celle d'indice ongueur - 1 - indice
# pas la peine de parcourir toute la chaine, il suffit d'en examiner la moitié (d'où la variable dernier rang)


def palind(chaine):
    
    longueur = len(chaine)
    dernier_rang = longueur // 2 - 1
    for indice in range(dernier_rang + 1):
        lettre_1 = chaine[indice]
        indice_2 = longueur - 1 - indice
        lettre_2 = chaine[indice_2]
        if lettre_1 != lettre_2:
            return False
        
    return True


        