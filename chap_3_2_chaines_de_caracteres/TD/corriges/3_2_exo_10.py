#on parcourt toute la chaine et si on rencontre le caractere
# on stoque son indice dans indice_lettre
# si le caractère n'est pas rencontré, indice_lettre continuera
# à contenir -1

def rang3(chaine, caractere):
    
    longueur = len(chaine)
    indice_lettre = -1
    for indice in range(longueur):
        lettre = chaine[indice]
        if lettre == caractere:
            indice_lettre = indice
    return indice_lettre

# autre idée : on parcourt la chaîne depuis la fin
# pour cela on utilise les options de la fonction range
# on part du dernier élément (indice : longueur - 1)
# jusqu'à celui d'indice 0
# dès qu'on a le caractère, on sort

def rang(chaine, caractere):
    
    longueur = len(chaine)
    for indice in range(longueur - 1, -1, -1):
        lettre = chaine[indice]
        if lettre == caractere:
            return indice
    return -1

# même idée que la fonction précédente mais en travaillant différemment sur les indices

def rang2(chaine, caractere):
    
    longueur = len(chaine)
    dernier_indice = longueur - 1
    for indice in range(longueur):
        lettre = chaine[dernier_indice - indice]
        if lettre == caractere:
            return indice
    return -1

