# une idée est de parcourir la chaine et de compter +1 par parenthèse ouvrante et -1 par parenthèse fermante
# si à un moment le total est négatif c'est qu'il y a eu trop de parenthèses fermantes avant des ouvrantes
# à la fin, il faut tester si il y a le même nombre d'ouvrantes que de fermantes (cad si total = 0)

def bienparenthesee(machaine):
    
    total = 0
    for char in machaine:
        # on incrémente/décrémente total si on rencontre une parenthèse
        if char = '(':
            total += 1
        elif char = ')':
            total -= 1
        
        if total < 0: # trop de fermantes avant des ouvrantes
            return False
    
    return total == 0 #on renvoie le résultat du test : total == 0 (à savoir autant d'ouvrantes que de fermantes)