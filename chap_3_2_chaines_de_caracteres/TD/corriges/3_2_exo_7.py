# on est obligé ici de faire un parcours via les indices
# car tous les caractères su subiront pas le même "traitement" :
# le dernier caractère doit être traité de manière différente (on n'ajoute pas d'étoile)

def etoile(chaine):
    sortie = ''
    longueur = len(chaine)
    
    # Même principe que l'exercice précédent mais on s'arrête à l'avant dernier caractère
    for indice in range(longueur - 1):
        caractere = chaine[indice]
        sortie = sortie + caractere + '*'
    
    # on récupère le dernier caractère et on l'ajoute à la fin (sans mettre d'étoile)
    dernier_caractere = chaine[len(chaine) - 1]
    sortie = sortie + dernier_caractere
    print(sortie)
    