# on parcourt les éléments via leurs indices
def affiche(chaine):
    longueur = len(chaine)
    for indice in range(0, longueur):
        print(chaine[indice])

# on parcourt la chaîne avec la boucle for et le parcours de séquence
def affiche2(chaine):
    for lettre in chaine:
        print(lettre)