def verlan(chaine):
    sortie = ''
    for lettre in chaine:
        sortie = lettre + sortie
    return sortie

def verlan2(chaine):
    sortie = ''
    longueur = len(chaine)
    for indice in range(longueur - 1, -1, -1):
        caractere = chaine[indice]
        sortie = sortie + caractere
    return sortie

def verlan3(chaine):
    sortie = ''
    longueur = len(chaine)
    for compteur in range(longueur):
        indice = longueur - 1 - compteur
        caractere = chaine[indice]
        sortie = sortie + caractere
    return sortie
    
        
    