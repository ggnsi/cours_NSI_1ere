# on parcourt la chaîne via les indices (puisqu'on en a besoin)

def rang(chaine, caractere):
    
    longueur = len(chaine)
    for indice in range(longueur):
        lettre = chaine[indice]
        # si la lettre est celle choisie, on sort en renvoyant l'indice
        if lettre == caractere:
            return indice
    # si on a fini de parcourir la chaîne sans trouver la lettre (on a fini la boucle)
    # alors on renvoie -1
    return -1