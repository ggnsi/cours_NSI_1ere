# on parcourt la chaine via les indices
def nb_fois(chaine, caractere):
    
    nb_rencontre = 0
    longueur = len(chaine)
    for indice in range(longueur):
        lettre = chaine[indice]
        if lettre == caractere:
            nb_rencontre = nb_rencontre + 1
    return nb_rencontre

# on parcourt la chaine avec la boucle for
def nb_fois2(chaine, caractere):
    
    nb_rencontre = 0

    for lettre in chaine:
        if lettre == caractere:
            nb_rencontre = nb_rencontre + 1
    return nb_rencontre
    