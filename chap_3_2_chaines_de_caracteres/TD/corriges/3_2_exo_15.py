def presence_de_A(chaine):
    
    for lettre in chaine:
        if lettre == 'A':
            return True
    return False

def valide(sequence): # 'ATCGF'
    
    nucleotides = 'ATCG'
    for nucleo in sequence:
        if nucleo not in nucleotides:
            return False
    return True

def nbr_nucleo(sequence):
    
    nbr_A = 0
    nbr_T = 0
    nbr_C = 0
    nbr_G = 0
    
    for nucleo in sequence:
        if nucleo == 'A':
            nbr_A += 1
        elif nucleo == 'T':
            nbr_T += 1
        elif nucleo == 'C':
            nbr_C += 1
        else:
            nbr_G += 1
    print('A :', nbr_A)
    print('T :', nbr_T)
    print('C :', nbr_C)
    print('G :', nbr_G)
    
def position_de_AT(sequence):
    
    for indice in range(0, len(sequence) - 1):
        if sequence[indice] == 'A' and sequence[indice + 1] == 'T':
            return indice
    return None