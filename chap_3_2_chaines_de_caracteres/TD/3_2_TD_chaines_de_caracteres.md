# TD : Chaînes de caractères
---
## Définir une chaine de  caractères et accéder à ses éléments
---


### Exercice 1 : Définir une chaîne de caractères

Dans la console :
* définir chacune des phrases suivantes sous la forme d'une seule chaîne de caractères : `ch = ...` 
* utiliser `print(ch)` pour vérifier l'affichage attendu ;
* faites afficher la longueur de la chaîne de caractères.


1. Il l'aime.
2. Il a dit : "Bonjour !".
3. Il dit : "Je t'adore".
4. Le chemin est C:\User\toto.jpg.

### Exercice 2 : accéder aux éléments d'une chaîne

Via la console, définir la chaîne de caractères : `ch = "Le taboulé gris"`

Quelle instruction écrire dans la console pour  :

1. accéder au `a` contenue ds `ch` ;
2. accéder au `é` de la chaîne de caractères ;
3. accéder au `L` de la chaîne de caractères ;
4. obtenir la longueur de la chaîne ;


* Sans compter, quel est l'indice du dernier caractère de `ch` ?
* Comment l'obtient-on à partir de la longueur de la chaîne ?
* Quelle instruction utiliser afin d'accéder systématiquement au dernier caractère d'une chaîne `ch` (sans avoir à compter à la main le nombre de caractères dans la chaîne. Cette méthode doit fontionner quelque soit la chaîne contenue dans `ch`) ?

### Exercice 3 :

1. Soit `navigateur = 'Firefox'`. Quel est la chaîne renvoyée par l'instruction `navigateur[2]` ?
2. On considère les instructions suivantes :  
```Python
s_1 = "Système"
s_2 = "d'exploitation"
```

Que vaut `len(s_1) + len(s_2)` ?
3. Soit le script Python suivant  :
```Python
x = 3
s = 'est un nombre'
q = 'premier'
```

Quelle expression écrire pour obtenir la chaine : `'3 est un nombre premier'`

4. Soit `alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"`  
Quel est le résultat retourné par l'instruction `alphabet[len(alhpabet) - 2]` ?

### Exercice 3 bis

Définissez une fonction `caractere_alea(chaine)` qui devra renvoyer un caractère au hasard contenu dans `chaine`.

---
## Parcours de chaines de caractères
---

### Exercice 4

Créer une fonction `affiche(chaine)` qui affiche les uns en dessous des autres les caractères de la chaîne.  

  Réaliser deux versions de cette fonction :
  * en parcourant la chaîne via les indices ;
  * en utilisant le parcours de séquence.
  
  _Exemple_ :
  ```Python
  >>> affiche('Mardi')
M
a
r
d
i
```

### Exercice 5

Soit la fonction suivante :

```Python
def bizarre(chaine):
    sortie = ''
    for lettre in chaine:
        sortie = lettre + sortie + lettre
    return sortie
```

En faisant fonctionner cette fonction **à la main**, déterminer le résultat retourné par l'appel `bizarre('NSI')`

### Exercice 6 : étoiles version 1
  
Créer une fonction `etoile(chaine)` qui __renvoie__ la chaine de caractère entrée avec une étoile `*` insérée après chaque caractére.

**Contrainte** : interdit d'utiliser le paramètre optionnel `end= ...` de la fonction `print()`

_Exemple_ :
```Python
>>> etoile("papa")
p*a*p*a*
```

### Exercice 7 : étoiles version 2
  
Créer une fonction `etoile(chaine)` qui __renvoie__ la chaine de caractère entrée avec une étoile `*` insérée **entre** chaque caractére.

**Contrainte** : interdit d'utiliser le paramètre optionnel `end= ...` de la fonction `print()`

_Exemple_ :
```Python
>>> etoile("papa")
p*a*p*a
```

### Exercice 8

Créer une fonction `verlan(chaine)` qui __renvoie__ à l'envers la chaine donnée.

_Exemple_ :

```Python
>>> verlan("Bonjour")
ruojnoB
```

On cherchera trois manières de réaliser cette fonction.

_Aide_ :
* première manière : utiliser les possibilités offertes par la fonction `range()`
* deuxième manière : 
    * si `indice = 0` comment obtenir le rang du dernier caractère en utilisant `indice` ?
    * si `indice = 1` comment obtenir le rang de l'avant-dernier caractère en utilisant `indice` ? 
    * si `indice = 2` comment obtenir le rang de l'avant-avant-dernier caractère en utilisant `indice` ?
    * généraliser.
* troisième manière : en parcourant la chaîne, comment construire le mot de sortie en verlan ?


### Exercice 9

1. Créer une fonction `rang(chaine, caractere)` qui __renvoie__ le rang de la **première** occurence (apparition) du caractère donné et _retourne_ `-1` si la caractère n'est pas présent (utilisation de `in` interdite (sauf pour `for ... in ...`)).

    _Exemples_ : 

    * `rang('Bonjour', 'j')` renvoie 3 ;
    * `rang('Bonjour', 'O')` renvoie -1 ;
    * `rang('Bonjour', 't')` renvoie -1.


2. Si `ch = 'Bonjour'`, quelle méthode appliquée à la chaine de caractère réalise la même chose que cette fonction ? Tester.

### Exercice 10

1. Créer une fonction `rang(chaine, caractere)` qui __renvoie__ le rang de la **dernière**  occurence (apparition) du caractère donné et _retourne_ `-1` si la caractère n'est pas présent (utilisation de `in` interdite (sauf pour `for ... in ...`)).

    _Exemples_ : 

    * `rang('Bonjour', 'j')` renvoie 3 ;
    * `rang('Bonjour', 'O')` renvoie -1 ;
    * `rang('Bonjour', 'o')` renvoie 4.

### Exercice 11

1. Créer une fonction `nb_fois(chaine, caractère)` qui __renvoie__ le nombre de fois où le caractère est présent dans la chaîne.

    On fera deux versions de cette fonction en utilisant les deux méthodes de parcours de chaine possible.

    _Exemples_ : 
    
```Python
>>> nb_fois('Bonjour', 'o')
2
>>> nb_fois('Bonjour', 't')
0
```

2. Si `ch = 'Bonjour'`, quelle méthode appliquée à la chaine de caractère réalise la même chose que cette fonction ? Tester.

### Exercice 12

Créer une fonction `suppression(chaine, caractere)` qui __renvoie__ la chaine initiale à l'intérieur de laquelle toutes les occurences du caractère donné auront-été supprimées.

_Exemples_ :

```Python
>>> suppression("Bonjour", 'o')
`Bnjur`
>>> suppression("Bonjour", 't')
`Bonjour`
```

### Exercice 13

Créer une fonction `remplace(chaine, char1, char2)` qui __renvoie__ la chaîne entrée dans laquelle tous les caractères `char1` auront été remplacés par des `char2`.

_Exemples_ :

```Python
>>> remplace("Bonjour", 'o', 'u')
`Bunjuur`
>>> remplace("Bonjour", 't', 'r')
`Bonjour`
```

### Exercice 14 : Palindrome 

> Un palindrome est une chaîne de caractères qui est identique qu'on la lise de gauche à droite ou de droite à gauche.  
> _Par exemple_: "été", "ressasser", "radar", "kayak" sont des palindromes.


__Ecrire un prédicat `palind(chaine)` qui vérifie si une chaîne de caractère est un palindrome.__

On considèrera qu'une chaîne de caractères vide est un palindrome.

_Remarques_ :
* une solution très rapide consiste à utiliser une fonction déjà définie dans ce TD ;
* on cherchera à en programmer une autre avec une méthode différente.

_Exemples_ :

```Python
>>> palind('ressasser')
True
>>> palind('manotam')
False
>>> palind('a')
True
>>> palind('')
True
```

### Exercice 15 : un peu d'ADN

Une molécule d'ADN est formée d'environ six milliards de nucléotides. L'ordinateur est donc un outil indispensable pour l'analyse de l'ADN.

Dans un brin d'ADN il y a seulement quatre types de nucléotides qui sont notés A,C,T ou G.

Une séquence d'ADN est donc un long mot de la forme :TAATTACAGACCTGAA...

1. Écrire un prédicat `presence_de_A(sequence)` qui teste la présence du nucléotide A dans une séquence d'ADN. Interdiction dans cette question d'utiliser la fonction `in`.

_Exemple_ :
```Python
>>> presence_de_A("CTTGCT")
False
>>> presence_de_A('TCGTCGTCGAT')
True
```

2. Écrire un prédicat `valide(sequence)` qui teste si la séquence est valide, c'est à dire si elle ne comprend que les lettres A,C,T ou G.

3. Ecrire une fonction `nbr_nucleo(sequence)` qui **affiche** le nombre de fois où chaque nucléotide apparait.

_Exemple_ : 

```Python
>>> nbr_nucleo('TCGTCGTCGAT')
A : 1
T : 4
C : 3
G : 3
```

4. Écrire une fonction `position_de_AT(sequence`) qui teste si une séquence contient le nucléotide A suivi du nucléotide T et renvoie la position de la première occurrence trouvée et renvoie `None` (mot clé défini dans Python) si la `AT` n'apparaît pas dans la séquence.

_Exemple_ :

```Python
>>>position_de_AT("CTTATGCT")
3
>>> position_de_AT("GATATAT")
1
>>> position_de_AT("GACCGTA")
None
```


### Exercice 16

Une chaine de caractères est bien parenthésée si elle contient autant de parenthèses ouvrantes que de parenthèses fermantes, et qu’en la lisant de gauche à droite, il y a toujours au moins autant de parenthèses ouvrantes que de parenthèses fermantes.

1. Parmi les chaines suivantes, lesquelles sont bien parenthésées ?
* `t=’abcd’`
* `u=’(abc(d(ef))’`
* `v=’()a()b’ , w=’’` 
* `x=’(ab))c(‘`
* `y=’((abc(d)))’`
* `z=’(abc))’`

2. On veut coder la fonction `bienparenthesee(machaine)`, qui revoie `True` si la chaîne est bien parenthésée, `False` sinon.

[^1]: [Sample web form.png by Cburnett sur Wikipédia anglais](https://commons.wikimedia.org/wiki/File:Sample_web_form.png?uselang=fr)
