# Parcourir une chaîne de caractères

**Important**

Les méthodes vues ici sont très importantes car nous les retrouverons dans les parcours d'autres séquences.  
Il est donc important de bien les travailler pour bien les maitriser.

>Il existe deux manières de parcourir une chaîne de caractères (ou une séquence) ;
> * par les indices des éléments ;
> * en utilisant la boucle `for` (parcours de séquence)

## Parcourir un chaine via les indices

**Illustration du principe sur un exemple**

Soit la chaîne `ch = 'python'`.  
Sa longueur est de `6` et les indices des caractères vont donc de `0` à `5`.

Nous allons :
* utiliser une variable `indice` qui prendra comme valeur les entiers allant de `0` à `5` ;
* pour chaque valeur, accéder au caractère via `ch[indice]`

En pratique (on affiche chaque caractère ici):


```python
ch = 'python'
longueur = len(ch) # longueur contient 6
for indice in range(longueur): #indice varie donc entre 0 et 5
    caractere = ch[indice]
    print(caractere)
```

    p
    y
    t
    h
    o
    n
    

Version légèrement plus compacte du code (sans utiliser la variable `caractere`) :


```python
ch = 'python'
longueur = len(ch) # longueur contient 6
for indice in range(longueur): #indice varie donc entre 0 et 5
    print(ch[indice])
```

### Parcourir une chaine avec la boucle `for ... in chaîne`

> Les chaînes de caractères sont _itérables_ , c'est à dire qu'on peut les parcourir avec une boucle `for`.

> Syntaxe :
>```Python
>for variable in chaine:
>    ...
>```
> A chaque nouveau tout de boucle, `variable` contiendra le caractère suivant de `chaine`

>Cette fonctionnalité permet de ne pas avoir à revenir aux indices pour parcourir la chaîne et offre un code plus lisible.

_Illustration du fonctionnement sur un exemple_ :


```python
mot = 'Python'

for lettre in mot:
    print(lettre)
```

    P
    y
    t
    h
    o
    n
    

Pour bien comprendre, [visualisez l'exécution du code prépécent dans PythonTutor](https://pythontutor.com/render.html#code=mot%20%3D%20'Python'%0A%0Afor%20lettre%20in%20mot%3A%0A%20%20%20%20print%28lettre%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) (Appuyez sur le bouton next pour faire évoluer l'exécution du code et observez l'évolution du contenu des variables sur la droite)

|tour de boucle n°| `lettre` contient |
|:--:|:--:|
|1| `'P'`|
|2| `'y'`|
|3| `'t'`|
|4| `'h'`|
|5| `'o'`|
|6| `'n'`|

_Autre exemple simple_ :


```python
def nature(lettre):
    """Renvoie 'voyelle' ou 'consonne' en fonction de la nature de lettre"""
    voyelles = 'aeiouy'
    if lettre in voyelles:
        return 'voyelle'
    else:
        return 'consonne'

def liste_lettres(mot):
    for lettre in mot:
        print(lettre, 'est une', nature(lettre))

liste_lettres('Python')
```

    P est une consonne
    y est une voyelle
    t est une consonne
    h est une consonne
    o est une voyelle
    n est une consonne
    

---
## Des méthodes sur les chaînes
---

Les chaînes de caractères sont des objets sur lesquels ont peut appliquer des méthodes.

Pour illustrer ceci, nous pouvons imaginer que l'objet est un crayon 4 couleurs. Vous pouvez par exemple lui appliquer la méthode : "changer la couleur en noir", ou "mettre le capuchon", ou "le mettre dans la trousse".

> Remarques :
> * Attention à la syntaxe qui est de la forme `chaine.methode_a_appliquer()`
> * Sauf précision dans le TD, vous ne devez PAS utiliser directement ces méthodes mais être capable de les programmer vous-même.

Quelques méthodes usuelles et quelques exemples :

| méthode | effet |
|:-------:|:-----:|
|ch.lower() |	Renvoie une chaine identique avec des minuscules|
|ch.upper() |	Renvoie une chaine identique avec des majuscules|
|ch.capitalize() |	Renvoie une chaine identique avec seulement la première lettre en majuscule|
|ch.split() |	Renvoie une liste dont les éléments sont les sous-chaines de ch|
|ch.join(liste) |	Renvoie une chaine à partir des éléments de liste en utilisant ch comme séparateur|
|ch.find(sch) |	Renvoie la position de sch dans ch ou -1 si sch n'est pas trouvée|
|ch.count(sch) |	Renvoie le nombre d'occurence de sch dans ch|
|ch.replace(ch1,ch2) |	Renvoie une chaine où les occurences de ch1 sont remplacés par ch2|

_Exemples_ :


```python
chaine = "Bonjour Gaëtan"

print(chaine.lower())
print(chaine.upper())
print(chaine.split())
print(chaine.split(' '))
print(chaine.split('o'))
```

    bonjour gaëtan
    BONJOUR GAËTAN
    ['Bonjour', 'Gaëtan']
    ['Bonjour', 'Gaëtan']
    ['B', 'nj', 'ur Gaëtan']
    


```python
print(chaine.find('a'))
print(chaine.count('o'))
print(chaine.replace('o','t'))
```

    9
    2
    Btnjtur Gaëtan
    
