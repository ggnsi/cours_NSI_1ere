## Chaînes de caractères
---

* [Généralités sur les chaînes de caractères](./3_2_chaines_de_caracteres_generalites.md)
* Exercices 1 à 3bis de la [feuille de TD](./TD/3_2_TD_chaines_de_caracteres.md)
* [Parcourir une chaîne de caractères](./3_2_parcours de_chaines_et_methodes.md)
* [suite des exercices du TD](./TD/3_2_TD_chaines_de_caracteres.md)
* [corrigés des exercices](./TD/corriges) au fur et à mesure dans le répertoire corrigé du répertoire TD

