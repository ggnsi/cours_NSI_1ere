__preliminaire__ :

Ce sujet traite du jeu Mastermind, et permet de programmer à la fois le calcul des bien placés et des mal placés, mais aussi de faire jouer la machine (et de lutter contre elle). 


## Le jeu Mastermind

Le mastermind est un jeu de société qui se joue à deux. Les joueurs disposent de pions colorés (nous considérerons les couleurs violet, jaune, rouge, orange, rose, bleu foncé, bleu clair, vert et le vide). Un joueur choisit un code secret de quatre pions parmi ces couleurs. Le second joueur va essayer de trouver le code. Il a droit à dix essais. À chaque proposition qu'il fait, le premier joueur lui donne deux indications :
* le nombre de pions de la bonne couleur qui sont bien placés (BP) ;
* et le nombre de pions de la bonne couleur qui sont mal placés (MP). 

Pour tout savoir sur l'historique de ce jeu : [wikipedia](https://fr.wikipedia.org/wiki/Mastermind).

L'objectif de ce sujet est d'arriver à un jeu dont le déroulement pourrait ressembler à ceci :

```
Tour 1
Votre proposition ? 0,1,2,3
La réponse est  1 bien placés et  2 mal placés.
Ma proposition est : [0, 4, 2, 0]
Mon score est : 0  bien placés,  2  mal placés.
Tour 2
Votre proposition ? 0,2,1,4 
La réponse est  1 bien placés et  1 mal placés.
Ma proposition est : [1, 0, 0, 1]
Mon score est : 1  bien placés,  0  mal placés.
Tour 3
Votre proposition ? 0,5,5,1
La réponse est  0 bien placés et  1 mal placés.
Ma proposition est : [4, 2, 3, 1]
Mon score est : 2  bien placés,  2  mal placés.
Tour 4
Votre proposition ? 3,2,0,3 
La réponse est  1 bien placés et  1 mal placés.
Ma proposition est : 3, 2, 4, 1
Mon score est : 1  bien placés,  3  mal placés.
Tour 5
Votre proposition ? 3,1,1,2
La réponse est  4 bien placés et  0 mal placés.
Bravo, la partie est finie, vous avez gagné.
```

## Une première version avec un seul joueur

Le code secret comme les propositions peuvent être représentés par des listes d'entiers. Nous coonsidèrerons le paramétrage habituel du jeu : il y a 9 couleurs possibles et le code secret a une longueur de 4. Les listes d'entiers manipulées seront donc de longueur 4 et les valeurs possibles dans les listes seront les entiers de l'intervalle $[0;8]$.

### Algorithme principal

L'idée est simple : on génère un code secret, et puis on demande à l'utilsateur sa proposition, on calcule et on l'informe de son nombre de valeurs bien placées et mal placées, jusqu'à ce qu'il ait trouvé le code secret ou dépassé le nombre maximal de propositions. L'algorithme ressemble à :

*entrée* : la taille du code secret, le nombre de couleurs utilisées et le nombe maximal de propositions acceptées

*pas de sortie* - affichages sur la console. 

*Algo* : 

* afficher les règles du jeu
* générer un code secret
* initialiser le numéro du tour à 1
* répèter au maximum 10 fois (on pourra s'arrêter plus tôt si le joueur a gagné)
  * afficher le numéro du tour
  * demander la proposition du joueur
  * calculer le nombre de bien placés et le nombre de mal placés
  * afficher le nombre de bien pacés et le nombre de mal placés
  * si le nombre de bien placés n'est pas égal à la taille du code secret, augmenter le numéro du tours et recommencer
* afficher un message (gagné/perdu) et afficher le code secret.


On décide de séparer dans deux modules différents les fonctions strictement relatives au jeu (`modele`{.py}) et les fonctions relatives aux interactions avec le joueur (`vue`{.py}). 
Dans un premier temps, nous allons créer toutes les fonctions du module `modele`{.py}


### Générer un code secret 

(@) Proposez une fonction `generer_secret()`{.py} qui prend en paramètre une longueur et un nombre de couleurs et qui génère un code secret correspondant aux contraintes.

```{.py}
>>> generer_code(4,9)
[0,5,4,0]
>>> generer_code(4,9)
[6, 8, 2, 2]
```
Cette fonction (comme les suivantes) est à écrire dans le fichier `modele.py`{.py}

(@) Proposez une fonction `generer_secret2()`{.py} qui effectue la même chose que la précédente mais en utilisant une liste en compréhension.

### Compter les bien placés et les mal placés

Le coeur du problème est de compter le nombre de bien placés et le nombre de mal placés. Nous proposons ici deux versions, à vous de faire votre choix (mais vous pouvez implémenter les deux versions !).
	
 
#### Première version - distribution des valeurs

Cette solution, inspirée d'un exercice donné à un concours d'Agro-Véto, utilise un calcul de distribution des valeurs. C'est un algorithme qu'on utilise pour tester si deux mots sont des anagrammes.

(@) Proposez une fonction `calculer_bp1()`{.py} qui prend en paramètre le code secret et une proposition (soit deux listes d'entiers comme décrites ci-dessus) et qui renvoie le nombre de bien placés, c'est-à-dire le nombre d'éléments qui ont la même valeur à la même position sur chacune des listes.

Exemples :

```{.py}
>>> calcul_bp1([1,2,2,4],[2,1,2,4])
2
>>> calcul_bp1([1,2,2,4],[3,1,3,4])
1
```

_Remarques_ :
* vous inclurez ces exemples comme jeu de test ;
* vous pourrez introduire des assertions permettant de vérifier les pré-conditions indiquées (conditions d'utilisation)

(@) Proposez une fonction `distribution()`{.py} qui prend un code (ou une proposition) en paramètre et  une valeur maximale. Le code ne doit contenir que des valeurs entre $0$ et la valeur maximale. La fonction construit une liste de longueur de la valeur maximale dont les valeurs représentent le nombre d'occurrences de la valeur dans la liste. Dans les exemples suivants, la valeur maximale est $9$ comme le nombre de couleurs du mastermind :


```{.py}
>>> distribution([1,2,2,4],9)
[0, 1, 2, 0, 1, 0, 0, 0, 0]
>>> distribution([3,1,3,4],9)
[0, 1, 0, 2, 1, 0, 0, 0, 0]
>>> distribution([8,8,6,8],9)
[0, 0, 0, 0, 0, 0, 1, 0, 3]
```

(@) Proposez une fonction `calculer_mp1`{.py} qui, pour deux listes passées en paramètre (un code et une proposition), ainsi qu'un nombre de bien placés, renvoie le nombre de mal placés.

```{.py}
>>> calculer_mp1([1,2,2,4],[2,1,2,4],2)
2
>>> calculer_mp1([1,2,2,4],[2,1,3,4],1)
2
>>> calculer_mp1([4,3,2,2],[2,2,1,5],0)
2
>>> calculer_mp1([1,2,3,4],[4,3,2,1],0)
4
>>> calculer_mp1([1,2,3,4],[4,3,3,1],1)
2
```

Le principe est le suivant : 
* On calcule d'abord les distributions de chaque liste, ce qui nous donne pour chaque couleur le nombre de pions de cette couleur présents (dans la proposition et dans le code) ;
* Puis on compare ces deux distributions pour déterminer le nombre de pions de couleurs en commun entre les deux listes. Pour cela, il faut ajouter, pour les valeurs non nulles d'incice commun, la plus petite des deux valeurs (on peut utiliser la fonction `min`). 
* Le nombre de mal placés est la différence entre le nombre d'éléments en commun et le nombre de bien placés.

_Ne pas oublier d'inclure un jeu de test pour vérifier votre fonction_.


#### Au final

(@) Proposez une fonction `reponses()`{.py} qui prend en paramètres une proposition et un code secret et qui retourne le nombre de bien placés et de mal placés. Vous choisirez l'une des deux versions précédentes pour effecture les calculs.

```{.py}
>>> reponses([1,2,2,4],[2,1,2,4])
2, 2
>>> reponses([1,2,2,4],[3,1,3,4])
1, 1
```

#### Deuxième version possible

A vous de trouver une deuxième manière de déterminer les bien et mal placés.

### Lire la proposition d'un joueur

Nous passons maintenant aux fonctions du module `vue`{.py}, qui devront être écrites dans un fichier `vue.py`{.py}.  

(@) Proposez une fonction `est_chiffre(car)` qui prend une chaine contenant un caractère en paramètre et renvoie `True` si ce caractère est un nombre entier compris entre 0 et 9 (inclus)

_Remarque_ : il est possible d'utiliser l'opérateur `<=` pour comparer des chaînes de caractères (faites des essais dans la console).

(@) Proposez une fonction `split_int`{.py} qui prend une chaîne de caractères en paramètre et convertit celle-ci en une liste d'entiers. La chaîne de caractères doit être constituée de valeurs entières séparées par des espaces. Pour la conversion des fragments de la chaîne de caractères qui représenteront des entiers, on pourra utiliser la fonction `int()`{.py} comme dans l'exemple ci-dessus.

On n'utilisera pas ici la méthode `split()` mais ce serait de manière générale possible (essayer de coder une deuxième version de la fonction demandée en l'utilisant ensuite).

Exemple d'utilisation :

```{.py}
>>> rep = input("Votre proposition (4 entiers entre 0 et 8 séparés par des espaces) ? ")
Votre proposition (4 entiers entre 0 et 8 séparés par des espaces) ? 6 4 0 1
>>> prop = split_int(rep)
>>> prop
[6, 4, 0, 1]
```

(@) Proposez une fonction `lire_proposition(nb_prop, longueur)` qui doit :
* afficher le numéro du tour (qui correspond au paramètre `nb_prop`) ;
* demander au joueur une proposition jusqu'à ce que celle-ci soit correcte ;
* renvoyer la liste des entiers correspondant à cette proposition.

### D'autres fonctions d'affichage déjà définies dans la module `vue`

* `afficher_mp(mp)` : affiche un message sur le nombre de mal placés (mp) ;
* `afficher_bp(bp)` : affiche un message sur le nombre de bien placés (bp);
* `afficher_gagnant(nbprop,secret)` : affiche un message pour le joueur qui a gagné avec le  code secret ;
* `afficher_perdant(nbprop,secret)` : affiche un message de fin du jeu - partie perdue avec le code secret.


### Fonction principale du jeu

(@) C'est le moment d'écrire la fonction principale dans un nouveau fichier `master.py`{.py}

```{.py}
import modele
import vue

def main1(nb_props_max,nb_couleurs,longueur) :
    ''' Jeu de mastermind à un seul joueur.
	param nb_props_max : nombre maximal de propositions acceptées
	param nb_couleurs : nombre de couleurs (de valeurs) avec lesquels on joue.
	param longueur : longueur du code secret 
	'''
	# à écrire
```

Vous devrez :

* afficher les règles ;
* générer un code secret ;
* tant que le nombre de proposition maximum n'a pas été atteint et que le joueur n'a pas trouvé le code, lui demander ses propositions, afficher le nombre de bien placés et de mal placés ;
* afficher si le joueur a gagné ou perdu.

