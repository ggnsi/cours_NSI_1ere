# module pour les entrées sorties du mastermind en mode texte

def afficher_les_regles(nb_couleurs, nb_props_max, longueur) :
    '''Affiche les règles du mastermind avec les informations sur les paramètres utilisés.
    param nb_couleurs : int, nbre de couleurs pour le jeu
    param nb_props_max : int, nbre maximal de propositions acceptées
    param longueur : int, taille du code secret
    return None - affichages sur la console
    '''
    print("Bienvenue dans le jeu du Mastermind:")
    print("L'objectif est de découvrir un code secret constitué de",longueur,"valeurs dans l'intervalle 0 à",nb_couleurs - 1," inclus.")
    print("À chause proposition, deux indices vous seront donnés : le nombre de bonnes valeurs bien placées et le nombre de bonnes valeurs mal placées.")
    print("Vous devez trouver le code en maximum",nb_props_max,"propositions.")
    print("Vous saisirez en séparant les valeurs par des virgules (exemple :[0, 1, 2, 1]).")

def est_chiffre(car) :
    '''teste si car représente un chiffre compris entre 0 et 8 inclus -> boolean'''
   
   

def split_int(chaine, longueur) :
    '''Convertit (si possible) une chaîne de caractères en liste d'entiers.
    Paramètre : 
    - chaine : str 
    Retour : list(int) ou False si la chaine entrée n'est pas constituée de 4 entiers compris entre 0 et 8 séparés par des espaces
    
    Post-condition : longueur de la liste retournée doit être de 4.
    
    Exemples :
    >>> split_int('6 4 0 1')
    [6, 4, 0, 1]
    >>> split_int('6 4 a 1')
    False
    >>> split_int('6 4 4 1 7')
    False
    '''
    
   
            

def lire_proposition(nb_prop, longueur) :
    """Doit demander la proposition du joueur jusqu'à ce que celle-ci soit correcte (chaine de caractère composée d'entiers séparés
    par des espaces). Le proposition doit être de la longueur spécifiée en paramètre (nombre d'enties attendus).
    Renvoie la liste des entiers composée à partir de cette proposition.
    Doit afficher le numéro du tour (qui correspond à nb_prop)
    
    Paramètre nb_prop : (int) numéro du tour
    paramètre longueur : (int>=1) nombre d'entiers attendus dans la proposition du joueur
    
    valeur retournée : list(int) de longueur égale au paramètre longueur
    
    """
    
    
   


def afficher_mp(mp) :
    '''affiche un message sur le nombre de mal placés (mp).'''
    print("Vous avez",mp," bonne(s) valeur(s) mal placée(s).")

def afficher_bp(bp) :
    '''affiche un message sur le nombre de bien placés (bp).'''
    print("Vous avez",bp," bonne(s) valeur(s) bien placée(s).")

def afficher_gagnant(nbprop,secret) :
    '''affiche un message pour le joueur qui a gagné.'''
    print("Bravo, vous avez trouvé le code secret",secret,"en",nbprop,"proposition(s).")


def afficher_perdant(nbprop,secret) :
    '''affiche un message de fin du jeu - partie perdue.'''
    print("Dommage, vous avez atteint le nombre maximal de propositions.")
    print("Le code secret était",secret,".")
    