import random

def generer_secret(nb_couleurs, longueur) :
    '''génère un code secret qui est une liste d'entiers de dimension longueur 
    dont chaque élément prend une valeur aléatoirement dans l'intervalle [0;nb_couleurs[
    param nb_couleurs : int > 0
    param longueur : int > 0
    return : list(int>=0)
    '''
   
   


def generer_secret2(nb_couleurs, longueur) :
    '''génère un code secret qui est une liste d'entiers de dimension longueur 
    dont chaque élément prend une valeur aléatoirement dans l'intervalle [0;nb_couleurs[
    param nb_couleurs : int > 0
    param longueur : int > 0
    return : list(int>=0)
    '''
    
  

def calculer_bp1(prop, secret) :
    '''calcule le nombre de bien placés dans la proposition prop par rapport au code secret.
    
    param prop, secret : list(int>=0) - non modifiés
   
    return : int >= 0
    CU : len(prop) == len(secret) 
    '''
   

def distribution(prop, maxi):
    """construit une liste de longueur la valeur maxi dont les valeurs représentent le nombre
    d'occurences de leur indice dans la liste
    
    param prop : list(int>=0)
    param maxi : int >= 1
    
    return : list(int>=0)
    CU : chaque élément de prop doit être inférieur à maxi
    
    Exemples :
    >>> distribution([1,2,2,4],9)
    [0, 1, 2, 0, 1, 0, 0, 0, 0]
    >>> distribution([3,1,3,4],9)
    [0, 1, 0, 2, 1, 0, 0, 0, 0]
    >>> distribution([8,8,6,8],9)
    [0, 0, 0, 0, 0, 0, 1, 0, 3]
    """
    
   

def calculer_mp1(prop, secret, bp) :
    '''calcule le nombre de mal placés dans la proposition prop par rapport au code secret et le nombre de bp.
    
    param prop, secret : list(int>=0) - non modifiés
    param bp : int >=0
    
    return : int >= 0
    CU : len(prop) == len(secret)
    CU : bp>=0:
    
    >>> calculer_mp1([1,2,2,4],[2,1,2,4],2)
    2
    >>> calculer_mp1([1,2,2,4],[2,1,3,4],1)
    2
    >>> calculer_mp1([4,3,2,2],[2,2,1,5],0)
    2
    >>> calculer_mp1([1,2,3,4],[4,3,2,1],0)
    4
    >>> calculer_mp1([1,2,3,4],[4,3,3,1],1)
    2
    '''
    

def reponses(prop,secret) :
    '''retourne le nombre de bien placés et de mal placés entre la proposition et le code secret.
    prop et secret ne sont pas modifiés.
    param prop : list(int>=0)
    param secret : list(int>=0)
    return : int>=0,int>=0
    CU : len(prop) == len(secret) > 0
    '''
    

######################

def calculer_bp2(prop,bprop,secret,bsecret) :
    '''calcule le nombre de bien placés dans la proposition prop par rapport au code secret.
    met à jour dans bprop et dans bsecret les positions concernées.
    param prop, secret : list(int>=0) - non modifiés
    param : bprop, bsecret : list(boolean) - modifiés
    return : int >= 0
    CU : len(prop) == len(secret) == len(bprop) == len(bsecret)
    '''
    assert len(prop) == len(secret) == len(bprop) == len(bsecret)
    bp = 0
    for i in range(len(prop)) :
        if prop[i] == secret[i] :
            bprop[i] = True
            bsecret[i] = True
            bp += 1
    return bp


def calculer_mp(prop,bprop,secret,bsecret) :
    '''calcule le nombre de mal placés dans la proposition prop par rapport au code secret.
    met à jour dans bprop et dans bsecret les positions concernées.
    param prop, secret : list(int>=0) - non modifiés
    param : bprop, bsecret : list(boolean) - modifiés
    return : int >= 0
    CU : len(prop) == len(secret) == len(bprop) == len(bsecret)
    '''
    assert len(prop) == len(secret) == len(bprop) == len(bsecret)
    mp = 0
    for i in range(len(prop)) :
        if not bprop[i] :
            if marque_dans(prop[i],secret,bsecret) :
                bprop[i] = True
                mp += 1
    return mp


def marque_dans(val,secret,bsecret) :
    '''marque par true dans bsecret la position dans secret qui contient val si elle existe et si elle n'est pas déjà marquée.
    param val : int>=0
    param secret : list(int>=0) - non modifié
    param : bsecret : list(boolean) - modifié
    return : boolean
    CU : len(secret) == len(bsecret)
    '''
    assert len(bsecret) == len(secret)
    for i in range(len(secret)) :
        if not bsecret[i] and secret[i] == val :
            bsecret[i]= True
            return True
    return False


def genere_prop(les_props,les_reps,nb_couleurs,longueur) :
    '''calcule la première proposition suivante valide par rapport aux indices fournis.
    param les_props : list(list(int))  -- toutes les propositions déjà faites par la machine
    param les_reps : list(tuple:(int,int))  -- toutes les réponses qui ont été données (les indices)
    param nb_couleur : int, nbre de couleurs pour le jeu
    param longueur : int, taille du secret et des propositions
    return : list(int)
    '''
    if len(les_props) == 0 :
        prop = generer_secret(nb_couleurs, longueur)
    else :
        prop = suivant(les_props[-1],nb_couleurs)
    print("essai de prop : ",prop)
    while not est_valide(prop,les_props,les_reps) :
        prop = suivant(prop,nb_couleurs)
        print("essai de prop : ",prop)
    return prop

def est_valide(secret,les_props,les_reps) :
    '''vérifie qu'une proposition est valide par rapport aux indices fournis, i.e. que cette proposition pourrait être le secret.
    param secret : list(int) -- la proposition qu'on veut tester par rapport aux propositions déjà faites et leur réponse
    param les_props : list(list(int))  -- les propositions déjà faites
    param les_reps : list(tuple:(int,int)) -- les réponses qui ont été faites aux propositions
    return : boolean
    '''
    for i in range(len(les_props)) :
        bp,mp = reponses(les_props[i],secret)
        print("on compare à ",les_props[i], "tour :",i)
        b,m = les_reps[i]
        print("La réponse est",(bp,mp)," - la réponse était",(b,m))
        if not(bp == b and mp == m) :
            return False
    return True

def suivant(prop,nb_couleurs) :
    '''retourne la proposition suivant immédiatement prop en base nb_couleurs.
    param prop : list(int), la proposition de départ
    param nb_couleurs : int, le nombre de couleurs du jeu
    return : list(int)
    '''
    suiv = [prop[i] for i in range(len(prop))]  # si on utilise les slices suiv = prop[:]
    # ou encore suiv = prop.copy() ou passage par une boucle
    i = len(prop) - 1
    suiv[i] = (suiv[i] + 1)%nb_couleurs
    while i > 0 and suiv[i] == 0 :
        i = i - 1
        suiv[i] = (suiv[i] + 1)%nb_couleurs
    return suiv
    # remarque : on pourrait tout aussi bien faire passer i de 0 à len(prop) et ajouter 1 à partir de la gauche
    
        

    
    
    

    
