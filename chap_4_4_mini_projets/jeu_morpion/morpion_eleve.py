def affiche_plateau(plateau):
    """
    Affiche le plateau sur 3 lignes en affichant :
    * le caractère _ si la case est vide ;
    * le caractère O si le joueur 1 y a posé un pion ;
    * le caractère X si le joueur 2 y a posé un pion.
    
    Exemple :
    Si plateau = [ [1, 0, -1], [0, 1, 1], [-1, -1, 0]], l'affichage doit être :
    
    O _ X
    _ O O
    X X _
    """
    
    

def coord_case(num_case):
    """Renvoie le numéro de ligne et celui de colonne correspondant au numéro de la case num_case

    param num_case : int
    valeur retournee : (tuple) : (num_lign, num_colonne)
    
    CU : 1<= num_case <= 9
    """
    
    
def joue(plateau, num_joueur):
    """Fais jouer un joueur en vérifiant que l'emplacement souhaité est libre et y insère son chiffre_joueur

    param (plateau) : list(list)
    chiffre_joueur : int  (1 pour joueur 1, 2 pour joueur 2)
    valeur retournee :  aucune
    
    effet de bord : liste plateau modifiée
    """
    
    

def gagnant(plateau, num_joueur):
    """définit si un joueur a gagné et retourne son numéro si oui et False si non

    parametre  plateau : list(list) (non modifiée)
    valeur retournée : int ou False
    """
    
   


# programme principal


    