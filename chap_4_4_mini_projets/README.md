# Minis projets :

Certains projest existent en version guidée et en version non guidée.
Voir avec moi pour sélectionner la version qui vous convient.

Voir dans les répertoires concernés pour certains fichiers annexes.

### avec des listes

* [jeu de pendu guidée](./pendu_guide/projet_jeu_pendu_guidé.pdf) (Difficulté 1);
* [jeu de pendu non guidé](./pendu_non_guide/projet_jeu_pendu_non_guide.pdf) (Difficulté ..);
* [jeu de mastermind](./mini_projet_guide_mastermind/mini_projet_mastermind.md) (Difficulté 5)

### avec des listes de listes

* [jeu de morpion version guidée](./jeu_morpion/jeu_morpion_guide.md) (Difficulté ..); (mais une version non guidée est possible)
* [jeu de la vie](./jeu_de_la_vie)
* [Ice Walker](./ice_walker)

### avec listes et dictionnaire

* [adn](../chap_7_les_dictionnaires/mini_projet_adn)

