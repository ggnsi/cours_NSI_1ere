# 1_NSI Lycée Beaupré

plateforme de fichiers pour les élèves de première NSI au lycée Beaupré d'Haubourdin 

Toutes les ressources sont sous licence CC BY-NC-SA
---

## Ressources :

* [docs.python.org](https://docs.python.org/fr/3/) le site de la documentation de Python
* [documentation Python NSI première] (http://nsi.ostralo.net/1_premiere/index.htm) : site de M. Willm

* lien vers le réseau du lycée : https://194.167.100.5:8443/owncloud/index.php/login

* voir la page [ressources](ressources.md)

---
## chapitres précédents

Vous pouvez naviguer dans les répertoires pour y retrouver les cours/TD des chapitres précédents.  
Les fichiers _markdown_ (extension .md) correspondent aux versions statiques des notebooks.  
J'ai laissé les notebooks pour ceux qui voudraient les utiliser chez eux.  
Pour _utiliser_ les notebooks : le sauvegarder sur votre ordinateur puis l'ouvrir avec par exemple [le serveur proposée par Basthon](https://notebook.basthon.fr/)


---

* [Chapitre 1_1 : types et opérations](./chap_1_initiation/1_1_types_operations/)
* [Chapitre 1_2 : variables et affectation](./chap_1_initiation/1_2_variables_affectation/)
* [Chapitre 1_3 : les fonctions](./chap_1_initiation/1_3_fonctions/)
* [Chapitre 1_4 : les structures conditionnelles et les opérateurs booléens](./chap_1_initiation/1_4_structures_conditionnelles/)
* [Chapitre 2_1 : représentation des entiers naturels](./chap_2_1_representation_entiers_naturels) : binaire, hexadécimal
* [Chapitre 1_5 : structures itérative : l boucle `for` ](./chap_1_initiation/1_5_structure_iterative_FOR/)
* [Chapitre 2_2 : représentation des entiers relatifs](./chap_2_2_representation_relatifs)
* [Chapitre 1_6 : la boucle While](./chap_1_initiation/1_6_structure_iterative_while)
* [chapitre 3_2 : les chaînes de caractères](./chap_3_2_chaines_de_caracteres)
* [chapitre 3_1 : représentation des caractères en machine](./chap_3_1_representation_caracteres)
* [chapitre 5_1 : Fonctions, spécifications et tests](./chap_5_1_fonctions_specification)
* [chapitre 4_1 : listes, partie 1](./chap_4_1_les_listes)
* [chapitre 9_1 : Modèle de Von Neumann](./chap_9_1_modele_van_neumann)
* [chapitre 5_2 : Variables locales et globales](./chap_5_2_variables_locales_globales)
* [chapitre 4_2 : Listes définies en compréhension](./Chap_4_2_listes_en_comprehension)
* [chapitre 4_3 : listes de listes](./chap_4_3_listes_de_listes)
* [chapitre 9_4 : systèmes d'exploitations](./chap_9_4_systemes_exploitation)
* [chapitre 6 : les tuples](./chap_6_les_tuples)
* [chapitre 7 : les dictionnaires](./chap_7_les_dictionnaires)
* [chapitre 9_5 : interaction d'un objet avec son environnement](./chap_9_5_interaction_objet_avec_environnement)
* [chapitre 11_1 : notion de complexité - recherche séquentielle](./chap_11_1_recherche_sequentielle)
* [chapitre 11_2 : recherche dichotomique](./chap_11_2_recherche_dichotomique)
* [chapitre 8_1 : HTML/CSS](./chap_8_1_html_css)
* [chap_12_donnees_en_tables](./chap_12_donnees_en_tables)
* [chap_11_3 : quelques tris de listes](./chap_11_3_tris)
* [chapitre 8_2 : Interactions avec l'utilisateur](./chap_8_2_interaction_utilisateur)
* [chapitre 13_2 : algos_gloutons](./chap_13_2_algos_gloutons)
* [chapitre 8_3 : Le modèle client-serveur et le protocole HTTP](./chap_8_3_modele_client_serveur_http)
* [chapitre 8_4 : Formulaires dans une page web](./chap_8_4_formulaires_page_web)
* [chapitre 13_1 : algorithme des k plus proches voisins](./chap_13_1_k_plus_proches)
* [chapitre 10 : réseaux et protocoles de communication](./chap_10_reseaux)






---

* [DM sur les boucles](./chap_1_initiation/1_5_structure_iterative_FOR/DM/DM_boucles_for_france_ioi.md)

---

* Projet shifoumi : [fichiers élèves](./projets/1_shifoumi/fichiers_eleves/) et des [corrigés profs](./projets/1_shifoumi/corrige_prof)



