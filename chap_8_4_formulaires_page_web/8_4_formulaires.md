## Interaction avec l'utilisateur

Une page web statique va renvoyer la même page à tous les utilisateurs.

Afin d'obtenir des contenus personnalisés en fonction de l'internaute, il est nécessaire de pouvoir utiliser des éléments dépendant du contexte (de l'internaute, de l'heure, de l'adresse IP, de la requette, d'éléments transmis).

Ceci nécessite l'utilisation de langages supplémentaires à l'HTML (Javascript, PHP, Python, ...)

On va s'intéresser ici à la transmission d'éléments via des formulaires, ce qui arrive relativement souvent.

Quelques exemples :

* lors de l'authentification via un mot de passe ;
* lors d'une recherche (exemple : moteur de recherche)
* lors du filtrage de résultats en fonction de choix réalisés par l'utilisateur (ex : filtrage selon des critères sur un site d'annonces en ligne, etc.)

**Interaction entre le client et le serveur**

![image interaction client_serveur](img/interaction_formulaire_serveur.png)

## Ce que nous allons voir ici :

* Comment créer un formulaire simple ;
* 2 méthodes de transmission des informations au serveur.

## Création d'un formulaire : la balise `<form ...>`

_Exemple de formulaire avec son code HTML en dessous_ :

![exemple_1](img/exemple_1.jpg)

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <form method="get" action="http://guillonprof.free.fr/nsi/chap_8_4/action_1b.php">
    <p style="text-align: center;">Votre nom : <input type="text" name="nom" value=""/></p>
	<p style="text-align: center;">Votre prénom : <input type="text" name="prenom" value=""/></p>
		<input type="submit" value="Envoyer"/>
	</p>
  </form>
</html>
```

> Décrivons les éléments constituants ce formulaire :

### La balise `<form>` permet de définir un formulaire.

Elle comporte deux attributs :

* `method`: la méthode à utiliser lors de l'échange des éléments saisis ave le serveur (ici dans l'exemple, méthode `GET`) ;
* `action`: l'URL à laquelle envoyer les paramètres (ici : `http://guillonprof.free.fr/nsi/chap_8_4/action_1b.php`)

### les balises `<input>` 

Elles permettent de créer des champs afin de prendre les entrées de l'utilisateur.

L'attribut `name` définit le nom du champ.

L'attribut `type` permet de définir le type de champ d'entrée.
Des valeurs possibles (chercher dans la documentation pour d'autres types possibles) :
* text (champ de texte) ;
* password (idem text mais les caractères entrées sont remplacés à l'affichage par des étoiles);
* email ;
* checkbox ;
* ...

L'attribut `value` permet d'afficher en fonction du type :
* ce qui est écrit sur un boutton ;
* un texte entré par défaut dans une zone de texte;
* ...

La balise `<input type="submit" ...>` permet d'ajouter un bouton de soumission du formulaire.

---

**A faire** :

* téléchargez [la page exemple_1.html](./fichiers/exemple_1.html)
* ouvrez-là dans votre navigateur ;
* Remplissez le formulaire et validez.

Les données ont été envoyées à la page `action1b.php` qui contient les éléments ci-dessous. La langage php permet de générer des éléments de manière dynamique.

_Remarque_ : le `.` permet la concaténation de chaînes de caractères.

```php
<!DOCTYPE html>
<?php
  $nom = $_GET["nom"];
  $prenom = $_GET["prenom"];
  ?>
  
<html>
  <head>
    <meta charset="utf-8">
  </head>

	<body>
	<p> Vous êtes <?php echo $nom . ' ' . $prenom; ?>
	</p>
	</body>
</html>
```
---

### autres types de balises `input`

**Ci dessous quelques exemples de rendus et le code associé**

![exemple_2](img/exemple_2.png)

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <form method="get" action="action.php">
    <p>Votre identifiant :
		<input type="text" name="identifiant" value="xxxxx"/> </p>
	<p> Mot de passe : <input type="password" name="mot_passe" value = "toto"></p>
		
	<p> Rester connecté ? <input type="checkbox" name="rester_co"></p>
	<p><input type="submit" value="Envoyer"/></p>
	</p>
  </form>
</html>
```

## Les méthodes de passage des paramètres

### La méthode `GET`

Reprenons l'exemple 1 où c'est la méthode **GET** qui est utilisé lors de la validation du formulaire.

![exemple_1](img/exemple_1.jpg)

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <form method="get" action="http://guillonprof.free.fr/nsi/chap_8_4/action_1b.php">
    <p style="text-align: center;">Votre nom : <input type="text" name="nom" value=""/></p>
	<p style="text-align: center;">Votre prénom : <input type="text" name="prenom" value=""/></p>
		<input type="submit" value="Envoyer"/>
	</p>
  </form>
</html>
```

**A faire** :

* Rechargez le fichier de l'exemple 1 dans votre navigateur;
* Entrer un non et un prénom puis valider.
* Observez la barre d'adresse du navigateur. Comment sont passés les valeurs incluses dans les champs ?
* Appuyez sur la touche F12 (sur Firefox) pour faire apparaître les outils de développement Web.
    * Allez dans l'onglet réseau.  
    Vous pouvez observez que la méthode utilisée est bien GET
    * Observez à droite l'entête de la requête (mettez en texte brut).  
    On voit que la méthode utilisée est clairement indiquée dans la requête, tout comme le nom des champs et les valeurs envoyées.

### La méthode `POST`

Remplaçons dans le fichier précédent la méthode **GET** par la méthode **POST**.

On obtient :

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <form method="post" action="http://guillonprof.free.fr/nsi/chap_8_4/action_1b_post.php">
    <p style="text-align: center;">Votre identifiant :
		<input type="text" name="identifiant" value="xxxxx"/>
		<input type="submit" value="Envoyer"/>
	</p>
  </form>
</html>
```

**A faire** :

* Télécharger le fichier [exemple_1_post.html](./fichiers/exemple_1_post.html) ;
* Ouvrez-le dans votre navigateur ;
* Remplissez puis validez le formulaire.
* Quelle différence constatez-vous avec l'utilisation de la méthode `GET` ?
* Avec les outils de développement Web, examinez l'entête de la requête :
    * le type de méthode utilisée y-est-elle indiquée ?
    * les noms des champs et les valeurs associées y sont-ils indiqués ?
* examinez le  corps de la requête (onglet Requète à droite).

**A faire**

* Quels sont les dangers de la méthode `GET` ?
* Donnez des exemples où il vaut mieux utiliser la méthode `POST`.
* L'utilisation de `POST` donne-t-elle une sécurité totale à l'utilisateur ?

### Validation des données du formulaire

Les données envoyées sont traitées au niveau du serveur. Mais pour diminuer sa charge il est possible de valider certaines données (ou leur format du côté client en **javascript**) avant l'envoi.

