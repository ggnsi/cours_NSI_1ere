# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 15:13:47 2019
class game2048 GUI
@author: Martin
"""
from tkinter import *
from tkinter.messagebox import *


 
class Game2048GUI(Tk):
    
    colortable={0 : "grey" ,\
                2 : "#eee4ba" ,\
                4 : "#ede0c8" ,\
                8 : "#f2b175" ,\
                16 : "#f59563" ,\
                32 : "#f67c5f" ,\
                64 : "#f65e3b" ,\
                128 : "#edcf72" ,\
                256 : "#edcc61" ,\
                512 : "#edc850" ,\
                1024 : "#edc53f" ,\
                2048 : "#edc22e" ,\
                4096 : "#f00f0f" }
                
    def clavier(self,event):
        global coords

        touche = event.keysym

        if touche == "Up":
            self.game.command("U")
       #     self.displaytable(self.game.table)        
        elif touche == "Down":
            self.game.command("D")
        #    self.displaytable(self.game.table)        
        elif touche == "Right":
          
            self.game.command("R")
         #   self.displaytable(self.game.table)
        elif touche == "Left": 
            self.game.command("L")
          #  self.displaytable(self.game.table)        
    def initialize(self):
        return
    def restart(self):
        nrow=int(self.spinrow.get())
        ncol=int(self.spincolumn.get())
        self.zonedessin.delete(ALL)
        self.game.restart(nrow,ncol)
        
    def setZonedessin(self):
        self.xsize, self.ysize =self.game.getsize()
        self.zonedessin.config(width=self.xsize*42+2, height=self.ysize*42+2)
        
        
    def on_closing(self):
        showwarning(title="Jeu 2048",message = "Programmation par les élèves du lycée H. Loritz ")
        self.destroy()
        
          
    def __init__(self, game):
         
        self.game=game
        self.xsize, self.ysize =self.game.getsize()
         
        Tk.__init__(self)  
        self.protocol("WM_DELETE_WINDOW", self.on_closing)    
        frame=Frame(self,bg="light grey", bd=1)
        frame.pack()
        lbl = Label(frame, text="Jeu 2048")
        lbl.pack()
        
         
        
        frame1 = Frame(frame,bd=2)
        frame1.focus_set()
        frame1.bind("<Key>", self.clavier)
        frame1.pack(side=LEFT, padx=10, pady=10)
        
        # On crée le canvas
        self.zonedessin=Canvas(frame1,width=self.xsize*42+2, height=self.ysize*42+2,background="grey")
        self.zonedessin.pack()
        
        #self.zonedessin.config(width=500,height=500)
        
 
        frame2=Frame(frame, padx=5, pady=5,bg="light grey")
        frame2.pack(side=RIGHT)
        
        framescore=LabelFrame(frame2,padx=5,pady=5,\
                              background="lightgrey",text="score",\
                              width=150,height=70,bd=1)   
        framescore.grid_propagate(0)
        self.s_scoreString=StringVar()
        self.s_maxString=StringVar()
        
        lblscore1= Label(framescore,text="0",width=4,bd=2,padx=10,\
                         bg="lightgrey",textvariable=self.s_scoreString)
        lblscore2= Label(framescore,text="0",width=4,bd=2,padx=10,\
                         bg="lightgrey",textvariable=self.s_maxString)

        lblscore1.pack(side=LEFT)
        lblscore2.pack(side=LEFT)
        framescore.grid(row=0)
        
        framesize=LabelFrame(frame2,padx=5,pady=5,\
                             bg="light grey",text="dimensions grille",\
                             width=150,height=150,bd=1)
        framesize.grid_propagate(0)
        
        framesize.columnconfigure(0,minsize=70)
        framesize.columnconfigure(1,minsize=70)
        framesize.rowconfigure(0,minsize=30)
        framesize.rowconfigure(1,minsize=30)
        framesize.rowconfigure(1,minsize=50)
    
        lblrow=Label(framesize,text="lignes  ",bg="lightgrey")
        lblcolumn=Label(framesize,text="colonnes",bg="lightgrey")
        spinrow=Spinbox(framesize,from_=4,to=8,width=2,bg="lightgrey")
        spincolumn=Spinbox(framesize,from_=4,to=8,width=2,bg="lightgrey")
        buttonsize=Button(framesize,text="Restart",command=self.restart)
        
        lblrow.grid(row=0,column=0)
        lblcolumn.grid(row=0,column=1)
        spinrow.grid(row=1,column=0)
        spincolumn.grid(row=1,column=1)
        buttonsize.grid(row=2,column=0,columnspan=2,sticky='s')
        
        framesize.grid(row=1)
        
        self.spinrow=spinrow
        self.spincolumn=spincolumn
        
        
    def mainLoop(self):
        self.mainloop()
        
    def displaytable(self,table):

        can=self.zonedessin
        nrow=len(table)
        ncol=len(table[0])
        for i in range(nrow) :
            for j in range(ncol):
                color= self.colortable[table[i][j]]
                if table[i][j] ==0 :
                    t=""
                else :
                    t=str(table[i][j])
                    
                can.create_rectangle(j*42+3,i*42+3,j*42+41,i*42+41,fill=color,state="disabled")
                can.create_text(j*42+21,i*42+24,text=t)

    def displayscore(self,m,s):
        self.s_scoreString.set(str(s))
        self.s_maxString.set(str(m))
        
    def displaymessageofEnd(self) :
        showwarning(title="Jeu 2048",message = " jeu terminé")
        
    def displaymessageADIOS(self) :
        print("Au revoir")
        #print ("Programme by M. Canals & les eleves de Prépa Loritz")
    
    def quitUI(self):
        self.quit()
        
        