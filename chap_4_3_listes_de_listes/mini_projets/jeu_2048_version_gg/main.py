# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 15:22:47 2019

@author: Martin
"""

from gui2048 import *
from moteurjeu import *

class game2048():
    

    
    def __init__(self) :
        print("Creating application")
       
    def defautvalues(self) :
        self.xsize=4
        self.ysize=4
 
    def initialize(self) :
        self.scorem=0
        self.scores=0
        self.table=startTable(self.xsize,self.ysize)
        
    
    def setUI(self,ui):
        self.ui=ui
        
    def initializeUI(self) :
         self.ui.setZonedessin()
         self.ui.displaytable(self.table)    
    
    def quit(self):
        self.ui.displaymessageADIOS()
        self.ui.quitUI()
       
    def newgame(self):
        return
    def getsize(self):
        return self.xsize, self.ysize
    
    def restart(self,nrow, ncol):
        self.xsize = ncol
        self.ysize = nrow
        self.table=startTable(self.xsize,self.ysize)
        self.ui.setZonedessin()
        self.ui.displaytable(self.table)
        
    
    def command(self,command):   
    # l'application répond à des commandes
        if command=="QUIT" :
            self.quit()
        elif command=="R" :
            newtable=playRight(self.table) 

        elif command=="L" :
            newtable=playLeft(self.table)

        elif command=="U" :
            newtable=playUp(self.table)

        elif command=="D" :
            newtable=playDown(self.table)

        if newtable != self.table :
            self.table=newtable
            addRandomTile(self.table)   
            self.ui.displaytable(self.table)
        
        m,s = score(self.table)
        self.ui.displayscore(m,s)
        
        if noPossibleMove(self.table):
            self.ui.displaymessageofEnd()
           
            
        
myApp= game2048()
myApp.defautvalues()
myApp.initialize()
myUI=Game2048GUI(game=myApp) 
myApp.setUI(myUI)
myUI.setZonedessin()
myApp.initializeUI()
myUI.mainLoop()

