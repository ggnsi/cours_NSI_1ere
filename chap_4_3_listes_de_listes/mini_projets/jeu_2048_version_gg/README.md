# Le jeu 2048

## Présentation du sujet.

2048 est un jeu vidéo de réflexion qui est apparu sur le WEB en 2014.  
Le jeu comporte une grille, de 4x4 cases dans la version classique, sur laquelle sont disposées des tuiles numérotées par des puissances de deux.

On déplace l’ensemble des tuiles dans les quatre directions à l’aide des touches directionnelles du clavier.  
Des tuiles de même valeur peuvent fusionner pour former une tuile de valeur double.

Le but du jeu est d’arriver à former une tuile de valeur 2048. Lorsque plus aucun mouvement n’est possible le jeu est terminé.

Dans l’exemple ci-dessous si l’on déplace vers la gauche, toutes les tuiles vont de déplacer le plus à gauche possible puis les 8 vont fusionner et une nouvelle tuile numérotée 2 ou 4 va apparaître aléatoirement parmi les cases vides.

![images du plateau](./img/image_2048_1.jpg)

![images du plateau](./img/image_2048_2.jpg)

Lors d’un déplacement une tuile ne peut fusionner qu’une fois (pas de fusion en cascade).

Le but du DM est la programmation de ce jeu en Python. L’interface graphique est déjà réalisée et votre travail consiste à programmer la logique du jeu.  
Vous allez donc écrire un module contenant toutes les fonctions nécessaires au calcul des déplacements et des fusions.  
Vous devrez donc respecter scrupuleusement les spécifications des fonctions à réaliser.

* **Tester bien vos fonctions.**
** **Toutes les fonctions doivent être écrites dans un fichier nommé `moteurjeu.py`.**

**Préambule** : Découvrez le jeu : [https://jeu2048.fr/](https://jeu2048.fr/) et comprenez les règles de déplacement et fusion.  
Attention le jeu est addictif !

## Travail à réaliser

On modélise une grille par un tableau d’entiers, qui en Python sera une liste de listes.  
Dans tout le devoir on désignera par table, tableau ou grille un objet Python qui est une liste de listes. Une valeur 0 dans ce tableau représente une case vide. Une valeur k modélise une tuile de valeur k.

Dans l’exemple précédent, la grille à gauche sera implantée en Python par la liste : `[[0,0,0,0],[0,0,0,2],[0,2,0,4],[0,2,8,8]]`.

**Les règles de déplacement et fusion étant les mêmes dans toutes les directions il est plus simple de les écrire pour une seule direction puis de travailler ensuite par rotation.**

Afin d’adapter le jeu à des grilles de tailles diverses, vos fonctions devront travailler sur des grilles de tailles quelconques.

### Question 1

1. Compléter la fonction nommée `rotateLeft(table`) ayant les spécifications suivantes :  
    * Entrée : table est un tableau d’entiers non vide.
    * Sortie : la fonction retourne un **nouveau** tableau d’entiers obtenu par rotation d’un quart de tour vers la gauche de table.
    * Le tableau d’entrée n’est pas modifié.
    
    _Remarque_ : Dans le code déjà rempli, un tableau de sortie à la bonne taille et ne contenant que des `0` est défini. vous pouvez travailler comme cela ou supprimer cette ligne si vous voulez utiliser une autre méthode.
    
    _Exemples_ :
    
    ![images rotation_gauche](./img/image_2048_3.png)
    
    _Une idée de méthode : ce n'est pas la seule_ :
    * si la table de départ comporte `n` lignes et `m` colonnes, combien la table tournée comportera-t-elle de lignes et de colonnes ?
    * Définir une table de sortie remplie de zéros et de la taille déterminée.
    * On considère un élément situé dans la matrice de départ donc la position est donnée par `(ind_l, ind_c)`.
    * A quel endroit tous les éléments de la première ligne de la matrice de départ vont-ils se retrouver dans la matrice tournée ?  
    En déduire l'expression de `nouvel_ind_c` en fonction de `ind_l`.
    * Quelles sont les coordonnées du nombre 0 de l'exemple ci-dessous ?  
    Quelles seront ses coordonnées dans la matrice tournée ?
    * Reprendre les questions du point précédent avec les noimbres 2, 4, 8.
    * En déduire l'expression de `nouvel_ind_l` en fonction de `nb_colonnes` et `ind_c`
    
    _deuxième idée__ :
    * Comment obtient-on la première ligne de la matrice tournée ?
    
    _Exemple_ :
    
    ```Python
    >>> rotateLeft([[1,2,3],[4,5,6]])
    [[3,6],[2,5],[1,4]]
    >>> rotateLeft([[0, 2, 4, 8],[16, 32, 64, 128], [256, 512, 1024, 2048], [2048, 4096, 8192, 16384]])
    [[8, 128, 2048, 16384], [ 4, 64, 1024, 8192], [2, 32, 512, 4096], [0, 16, 256, 2048]]
    ```


2. Ecrire de même des fonctions `rotateHalf(table)` et `rotateRight(table)`qui renvoient respectivement le tableau obtenu par rotation d'un quart de tour vers le droite et d'un demi-tour (Vous pouvez re-coder des fonctions ou penser éventuellement à utiliser des fonctions déjà codées).

    _Exemples_ : 
     ```Python
    >>> rotateRight([[1,2,3],[4,5,6]])
    [[4, 1],[5, 2],[6, 3]]
    >>> rotateHalf([[0, 2, 4, 8],[16, 32, 64, 128], [256, 512, 1024, 2048], [2048, 4096, 8192, 16384]])
[[16384, 8192, 4096, 2048], [2048, 1024, 512, 256], [128, 64, 32, 16], [8, 4, 2, 0]]
    ```

### Question 2

Dans cette question nous allons réaliser la fonction qui ajoute une tuile aléatoire de valeur 2 ou 4 à la place
d’un emplacement vide. La méthode proposée consiste à établir la liste des cellules vides sous forme d’une
liste de couples `(i , j)` où `i` désigne l’indice de la ligne et `j` l’indice de la colonne de la cellule vide. On choisit ensuite un couple au hasard dans cette liste et un nombre au hasard parmi `[2, 4]`. 
La cellule `table[i][j]` est modifiée.

1. Ecrire une fonction `emptyCells(table)` qui renvoie la liste de couples `[(i₀ , j₀) , (i₁ , j₁) , …, (in , jn) ]` tels que `table[ik][jk] = 0`.

_Exemple:
```Python
 >>> emptyCells([[0, 2 , 2, 4],[0, 0, 4, 8]])
 [(0, 0), (1, 0), (1, 1)]
   ```

2. Ecrire une fonction `addRandomTile(table)` qui choisit au hasard une cellule vide dans la table et modifie la table en écrivant 2 ou 4 dans cette cellule, la probabilité de choix de 2 étant de 2/3.  
Si aucune cellule n'est vide on ne modifie pas la table. 


   Cette fonction **ne renvoie rien** mais modifie la liste table passée en argument.
   
   _Aide_ : pour simuler l'obtention d'un élément (ici la tuile 2) avec une probabilité de 2/3, il suffit par exemple : 
   * choisir un nombre entier au hasard entre 1 et 3. Il y a donc 3 possibilités : 1, 2, 3.
   * considérer que si le nombre obtenu est 1 ou 2 (soit 2 chances sur 3), alors la tuile sera de valeur 2 ;
   * considérer que si le nombre obtenu est 3 (soit 1 chance sur 3), alors la tuile sera de valeur 4.
   
   _Remarque_ : Une méthode plus générale peut consister à  :
   * choisir au hasard un nombre entre 0 et 1 (voir la fonction random du module random) ;
   * etudier si ce nombre est inférieur à 2/3.  
   En effet, la probabilité qu'un nombre choisi au hasard dans [0 ; 1] appartienne à l'intervalle [0; 2/3] est de 2/3.
   
### Question 3

1. Ecrire une fonction `startTable (nb_lig = 4, nb_col = 4)` qui crée et renvoie une table de `nb_lig` lignes et `nb_col` colonnes
contenant des 0 à l’exception d’une cellule choisie au hasard contenant un 2 ou un 4 suivant le même processus qu'à la question précédente.

_Remarque_ : la fait d'écrire `nb_lig = 4` dans la définition de la fonction fait que si les paramètres ne sont pas spécifiés lors de l'appel de la fonction, la valeur par défaut 4 lui sera attribuée.



2. Ecrire un prédicat `compare(table1,table2)` qui renvoie `True` si les deux tables ont des valeurs
toutes identiques et `False` sinon.  
On suppose ici que les deux tables ont même dimension et on évitera d'utiliser `table1 == table2`.

_Exemples_ :
```Python
>>> compare([[1,2,3],[4,5,6]], [[1,2,3],[4,5,6]])
True
>>> compare([[1,2,3],[4,5,6]], [[1,2,3],[4,8,6]])
False
```


### Question 4

Dans cette question nous allons réaliser la fonction qui calcule le déplacement des tuiles vers la droite. On commence par programmer le mouvement pour une rangée.

1. Ecrire une fonction `moveRight(row)` qui prend en entrée une liste d’entiers et qui renvoie une liste d’entiers de même taille obtenue en déplaçant les valeurs nulles de row en tête de la liste.  
La liste en entrée n’est pas modifiée.  
Cette fonction doit avoir avoir l’effet suivant :

![images projectin droite](./img/image_2048_4.png)

_Exemples_ :
```Python
>>> moveRight([0, 4, 2, 0, 0, 2, 0, 8])
[0, 0, 0, 0, 4, 2, 2, 8]
>>> moveRight([0, 0, 0, 0])
[0, 0, 0, 0]
```

2. Écrire une fonction `stackRight(row)` qui prend en entrée une liste d’entiers et qui renvoie une liste de même taille obtenue en remplaçant deux cellules voisines de même valeur par une cellule vide suivie d’une cellule contenant la valeur double.  
La liste en entrée n’est pas modifiée.

  _Exemples_ :
  
  ![images projectin droite](./img/image_2048_5.png)
  
_Exemples_ :
```Python
>>> stackRight([2, 2])
[0, 4]
>>> stackRight([2, 2, 4, 4])
[0, 4, 0, 8]
>>> stackRight([8, 8, 2, 4])
[0, 16, 2, 4]
``` 

3. Écrire une fonction `playRight(table)` ayant les spécifications suivantes :
  * table est un tableau d’entiers représentant la grille de jeu à un instant donné.
  * La fonction renvoie un nouveau tableau d’entiers qui correspond à la grille de jeu après un déplacement vers la droite.
  
  Vous travaillerez rangée par rangée et utiliserez les deux fonctions précédentes.
  
  ![images projectin droite](./img/image_2048_6.png)

_Exemples_ :
```Python
>>> table = [[0, 2, 2, 4], [2, 2, 4, 4], [8, 8, 2, 4], [0, 2, 0, 2]]
>>> playRight(table)
[[0, 0, 4, 4], [0, 0, 4, 8], [0, 16, 2, 4], [0, 0, 0, 4]]
``` 

4. Voici un morceau de code Python écrit à partir des fonctions déjà présentes dans le module.

```Python
def mystere(table) :
# table est un tableau d’entiers.
return rotateLeft(playRight(rotateRight(table)))
```

  * Que fais ce code ?
  * En déduire l’écriture des fonctions `playUp(table)`, `playDown(table)` et `playLeft(table)` qui  réalisent les tableaux correspondants à la grille de jeu après déplacement vers le haut, le bas et la gauche.

### Question 5

Nous allons traiter quelques questions utilitaires.

1. Écrire une fonction `score(table)` qui renvoie un tuple `(m,s)`, où `m` est la valeur maximale de
table et `s` est la somme de tous les éléments de table.  

On évitera l'utilisation de la fonction `max()`.

_Exemple_ :
```Python
>>> table = [[0, 0, 4, 4], [0, 0, 4, 8], [0, 16, 2, 4], [0, 0, 0, 4]]
>>> score(table)
(16, 46)
``` 

2. Écrire une fonction `noPossibleMove(table)` qui renvoie un booléen égal à `True` si le jeu n’est
plus possible et `False` sinon.  
Au départ écrivez une fonction qui renvoie simplement `True`.
3. Copier dans le même dossier les fichiers `main.py`, `gui2048.py` et `moteurjeu.py`.
L’exécution de `main.py` lancera le jeu.

---
### Crédits :
* M. Canals & les eleves de Prépa Loritz
* énoncé modifié par G.Guillon
