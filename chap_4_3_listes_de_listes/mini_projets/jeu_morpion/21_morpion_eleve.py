def affiche_plateau(plateau):
    """
    Affiche le plateau sur 3 lignes en affichant :
    * le caractère _ si la case est vide ;
    * le caractère O si le joueur 1 y a posé un pion ;
    * le caractère X si le joueur 2 y a posé un pion.
    
    Exemple :
    >>> plateau = [ [1, 0, -1], [0, 1, 1], [-1, -1, 0]]
    >>> affiche_plateau(plateau)
    O _ X 
    _ O O 
    X X _ 
    """
    
   pass

def coord_case(num_case):
    """Renvoie le numéro de ligne et celui de colonne correspondant au numéro de la case num_case

    param num_case : int
    valeur retournee : (tuple) : (num_lign, num_colonne)
    
    CU : 1<= num_case <= 9
    """
    
     pass

def joue(plateau, num_joueur):
    """Fais jouer un joueur en vérifiant que l'emplacement souhaité est libre et y insère son chiffre_joueur

    param (plateau) : list(list)
    chiffre_joueur : int  (1 pour joueur 1, 2 pour joueur 2)
    valeur retournee :  aucune
    
    effet de bord : liste plateau modifiée
    """
    
     
   pass

def est_gagnant(plateau, num_joueur):
    """définit si un joueur a gagné et retourne son numéro si oui et False si non

    parametre  plateau : list(list) (non modifiée)
    valeur retournée : int ou False
    
    Exemples :
    >>> plateau = [ [1, 0, -1], [0, 1, 1], [-1, -1, -1]] # joueur 2 gagne en dernière ligne
    >>> est_gagnant(plateau, 1)
    False
    >>> est_gagnant(plateau, 2)
    True
    >>> plateau = [ [1, 0, -1], [1, 1, 1], [-1, 0, -1]] # joueur 1 gagne en ligne du milieu
    >>> est_gagnant(plateau, 1)
    True
    >>> est_gagnant(plateau, 2)
    False
    >>> plateau = [ [1, 0, -1], [1, -1, 1], [1, 0, -1]] # joueur 1 gagne en première colonne
    >>> est_gagnant(plateau, 1)
    True
    >>> est_gagnant(plateau, 2)
    False
    >>> plateau = [ [1, 0, -1], [1, 1, -1], [-1, 0, -1]] # joueur 2 gagne en dernière colonne
    >>> est_gagnant(plateau, 1)
    False
    >>> est_gagnant(plateau, 2)
    True
    >>> plateau = [ [1, 0, -1], [0, 1, 1], [-1, -1, 1]] # joueur 1 gagne en diagonale
    >>> est_gagnant(plateau, 1)
    True
    >>> est_gagnant(plateau, 2)
    False
    """
    
      
   pass


def morpion():
# programme principal

      
   pass

if __name__ == '__main__':
    import doctest
    doctest.testmod()
    