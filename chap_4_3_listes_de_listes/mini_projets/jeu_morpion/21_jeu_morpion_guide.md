# Jeu de morpion

Un [fichier est fourni](./21_morpion_eleve.py) avec les noms de fonctions et les docstrings.

Le jeu de morpion se joue dans une grille de 3 cases sur 3.
Chaque joueur a un symbole (une croix et un rond) et les joueurs inscrivent à tour de rôle leur symbole dans une case vide.

Un joueur gagne s'il arrive à aligner 3 symboles (horizontalement, verticalement ou en diagonale).
Si la grille est complétée sans vainqueur, il y a égalité.

![plateau morpion](./img/regle-morpion-1-375x210.jpg)



On considèrera ici :
* que le joueur n°1 joue avec le symbole `O` et que le joueur 2 joue avec le symbole `X` ;
* que les cases du plateau sont numérotées de 1 à 9, la case 1 étant en haut à gauche, la case 2 en haut au milieu, ..., la case 9 en bas à droite ;
* le joueur 1 commence toujours la partie.

### Le plateau

Chaque ligne du plateau est modélisée par une liste de 3 éléments.
Le plateau est modélisé par une liste composée de 3 des listes précédentes.
On convient que :
* une case qui ne contient rien est codée par 0 dans la représentation du plateau ;
* une case où le joueur 1 a posé un pion `O` est codée par `+1` dans la représentation du plateau;
* une case où le joueur 2 a posé un pion `X` est codée par `-1` dans la représentation du plateau.

1. Dans la fonction `morpion()`, initialisez une variable `plateau` correspondant à une représentation du plateau et ne contenant que des `0`.

2. Compléter la fonction `affiche_plateau(plateau)` qui affiche le plateau de jeu sur 3 lignes avec :

    * le caractère _ si la case est vide ;
    * le caractère O si le joueur 1 y a posé un pion ;
    * le caractère X si le joueur 2 y a posé un pion.
    * un espace est situé au bout de chaque ligne (pour simplifier la fonction)

    _Exemple_ : 
```Python
    >>> plateau = [ [1, 0, -1], [0, 1, 1], [-1, -1, 0]]
    >>> affiche_plateau(plateau)
    O _ X 
    _ O O 
    X X _ 
```

### Un joueur joue

(@) Compléter la fonction `coord_case(num_case)` qui doit retourner un tuple (ou une liste si le chapitre tuple n'a pas encore été fait) composé du numéro de la ligne (entre 0 et 2) et du numéro de la colonne (entre 0 et 2) du plateau correspondant au numéro de case choisi.



_Correspondance entre le numéro de la case et ses coordonnées_ :

![image plateau](./img/morpion_plateau.jpg)

![image plateau](./img/morpion_plateau_coord.jpg)

_Exemples_ :

```Python
>>> coord_case(1)
(0, 0)
>>> coord_case(3)
(0, 2)
>>> coord_case(5)
(1, 1)
>>> coord_case(9)
(2, 2)
```

_Aide_ : 

* On pourra commencer par enlever 1 au numéro de la case ;
* Se demander ensuite comment obtenir le numéro de la ligne à partir du résultat obtenu ;
* Se demander comment obtenir le numéro de la colonne à partir du résultat obtenu
* (et .... penser à faire des essais à la main et les fonctions `//` et `%` peuvent vous être ... très utiles.)

![image plateau](./img/morpion_plateau_coord_2.jpg)



(@) Compléter la fonction `joue(plateau, num_joueur)` qui doit :

* Demander au joueur un chiffre entre 1 et 9 où il veut jouer ;
* vérifier si cet emplacement est libre.  
Si non, on l'indique au joueur et on recommence
* Lorsque la proposition est valide, modifier le plateau de jeu en conséquence (la variable `plateau` est donc modifiée en place).


### Le programme principal (pour l'instant sans test pour savoir qui gagne)

Compléter le programme principal afin que :

* on affiche le plateau au départ ;
* A chaque tour :
    * on affiche le numéro du joueur qui joue (n° 1 ou n°2) ;
    * le joueur joue ;
    * on affiche le plateau modifié.
    
### Savoir qui a gagné

(@) Compléter la fonction `est_gagnant(plateau, num_joueur)` qui renvoie `True` si le joueur numéro `num_joueur` a gagné et `False` sinon.

On rappelle que chaque case contient le chiffre 1 si le joueur `1` y a posé un pion et le chiffre `-1` si c'est le joueur 2 qui y a posé le sien.

_Exemples_ :
```Python
>>> plateau = [ [1, 0, -1], [0, 1, 1], [-1, -1, -1]] # joueur 2 gagne en dernière ligne
>>> est_gagnant(plateau, 1)
False
>>> est_gagnant(plateau, 2)
True
>>> plateau = [ [1, 0, -1], [1, 1, 1], [-1, 0, -1]] # joueur 1 gagne en ligne du milieu
>>> est_gagnant(plateau, 1)
True
>>> est_gagnant(plateau, 2)
False
>>> plateau = [ [1, 0, -1], [1, -1, 1], [1, 0, -1]] # joueur 1 gagne en première colonne
>>> est_gagnant(plateau, 1)
True
>>> est_gagnant(plateau, 2)
False
>>> plateau = [ [1, 0, -1], [1, 1, -1], [-1, 0, -1]] # joueur 2 gagne en dernière colonne
>>> est_gagnant(plateau, 1)
False
>>> est_gagnant(plateau, 2)
True
>>> plateau = [ [1, 0, -1], [0, 1, 1], [-1, -1, 1]] # joueur 1 gagne en diagonale
>>> est_gagnant(plateau, 1)
True
>>> est_gagnant(plateau, 2)
False
```

(@) modifier le programme principal pour
* qu'il teste après chaque tour si le joueur qui vient de jouer a gagné et arrête la partie en affichant le gagnant
* ou bien qu'il affiche égalité si la partie est terminée sans gagnant.

(@) La proposition de travail écrite ci-dessus entraine l'écriture de fonctions comportant de multiples lignes.  
Chercher à créer des fonctions auxiliaires permettant de réduire la taille de chaque fonction.
