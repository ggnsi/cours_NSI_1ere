# Listes de listes

Chaque élément d'une liste peut lui-même être une liste.

## Premières manipulations
---

_Exemple_ :

```Python
L = [[1, 2, 3], [4, 5, 6]]
```


```python
L = [[1, 2, 3], [4, 5, 6]]
```

La longueur de `L` est le nombre de sous-listes :


```python
len(L)
```




    2



Chaque élément de la liste est une liste :


```python
L[0]
```




    [1, 2, 3]




```python
L[1]
```




    [4, 5, 6]



Pour accéder à l'élément d'indice `1` de `L[0]`, on utilise la syntaxe habituelle (attention à l'ordre) :


```python
L[0][1]
```




    2



`L[1][2]` est donc l'élément d'indice 2 de la deuxième liste (d'indice 1) contenue dans `L`.


```python
L[1][2]
```




    6



## Une représentation visuelle (lorsque toutes les listes ont même longueur)
---
### Définition

>En Mathématiques, une _matrice_ de taille n x m est un **tableau de nombres** comportant $n$ lignes et $m$ colonnes.

![matrice](./fig/matrice_n_m.png)

>Chacun de ces nombres est un des coefficients de cette matrice.
> On note `$a_{i,j}$` le coefficient situé à la la `$i$`-ième ligne et à la `$j$`-ième colonne.
> Attention : en mathématiques, on numérote les lignes et les colonnes à partir de 1.

_Exemple_ : Soit ![matrice_1](./fig/listes_de_listes_cours_fig_2.jpg)

<!-- $$A=\begin{pmatrix}
5 & 4 & -1 \\ 
2 & 7 & -3 \\ 
0 & 1 & 6
\end{pmatrix} $$.-->

Quelles sont les valeurs de a(2,3)  ? a(1, 2) ? a(2, 2) ? a(3, 1) ? (réponses en bas de la page.)

### Lien avec les listes de listes

> Une liste de liste peut se représenter par une matrice (et inversement).

_Exemple_ : `L = [[1, 2, 3], [4, 5, 6]]` se représente par

![matrice_1](./fig/listes_de_listes_cours_fig_3.jpg)
<!-- $$L = \begin{pmatrix}
1 & 2 & 3 \\ 
4 & 5 & 6
\end{pmatrix} $$ -->

> Chaque élément de `L` (une liste donc) correspond à une ligne de la matrice

**Attention** :
* La **première** ligne de la matrice est l'élément **d'indice 0** de `L` ;
* La **première** colonne de la matrice est constituée des éléments **d'indice 0** des sous-listes de `L`.

### Intérêt des listes de listes ?

Un plateau de jeu, une image rectangulaire peuvent se représenter par une matrice et donc par une liste de liste.

## Comment parcourir tous les éléments d'une liste de liste ?
---

**Attention** : toutes les sous-listes n'ont pas forcément même longueur.

### Principe général  :

* On va parcourir la liste "principale" avec une boucle
* On va parcourir chaque "sous-liste" avec une autre boucle

Nous aurons donc deux boucles imbriquées.

### en utilisant les parcours de listes



```python
L = [[1, 2, 3], [4, 5, 6, 7]]

for liste in L: # On parcourt L
    for elt in liste: # On parcourt chaque sous-liste
        print(elt)
```

    1
    2
    3
    4
    5
    6
    7
    

### en utilisant les indices

Il faut à chaque élément de la liste pricipale (soit pour chaque sous liste) récupérer sa longueur.  
On retrouve les deux boucles imbriquées.


```python
L = [[1, 2, 3], [4, 5, 6, 7]]

lgr_1 = len(L) # nombre d'éléments de L
for ind_1 in range(lgr_1):   # on parcourt la liste L
    lgr = len(L[ind_1])  # lgr est la longueur de la liste de L d'indice ind_1
    for ind_2 in range(lgr): # on parcourt la sous-liste d'indice ind_1
        print(L[ind_1][ind_2]) # et on affiche tous ses éléments.
```

    1
    2
    3
    4
    5
    6
    7
    

## Attention : Comment bien initialiser une matrice ?
---

_Exemple_ : On veut générer une matrice de taille `3 x 4` (3 lignes et 4 colonnes) remplie de zéros.

### Première idée (mauvaise !) : concaténer en série



```python
L = [0] * 4     # on génère une liste de taille 4 contenant des 0
LL = [L] * 3      # on concatène 3 de ces listes
LL
```




    [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]



Où est le problème alors ?

Si on modifie la liste `L` :


```python
LL[0][0] = 1 #on modifie le premier élément de la première sous-liste
LL
```




    [[1, 0, 0, 0], [1, 0, 0, 0], [1, 0, 0, 0]]



__Problème__ : _tous les premiers éléments des sous-listes ont été modifiés !_

Ceci est encore du à la manière dont python construit les objets.

**Visualisez ce qui se passe avec [python Tutor](http://www.pythontutor.com/visualize.html#code=L%20%3D%20%5B0%5D%20*%204%20%20%20%20%20%23on%20g%C3%A9n%C3%A8re%20une%20liste%20de%20taille%204%20contenant%20des%200%0ALL%20%3D%20%5BL%5D%20*%203%20%20%20%20%20%20%23on%20concat%C3%A8ne%203%20de%20ces%20listes%0AL%5B0%5D%20%3D%201&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)**

En mémoire, chaque élément de la liste `LL` correspond à la liste `L`.  
La modification `LL[0][0] = 1` modifie donc le premier élément de `L` et donc tous les autres éléments de `L`.

Même problème si on écrit :


```python
LL = [ [0] * 4] * 3
print(LL)
LL[0][0] = 1 #on modifie le premier élément de la première sous-liste
print(LL)
```

    [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    [[1, 0, 0, 0], [1, 0, 0, 0], [1, 0, 0, 0]]
    

**Visualisez ce qui se passe avec [python Tutor](http://www.pythontutor.com/visualize.html#code=LL%20%3D%20%5B%20%5B0%5D%20*%204%5D%20*%203&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)**

### Deuxième idée : la bonne

* On crée une liste vide,
* On lui ajoute 3 fois une liste de taille 4 ne contenant que des 0.

Ici, __on créée une nouvelle liste à chaque itération__. Les problèmes précédents ont donc disparus.

Visualisation de l'exécution du code ci-dessous avec [python Tutor](http://pythontutor.com/visualize.html#code=LL%20%3D%20%5B%5D%0Afor%20i%20in%20range%283%29%3A%20%23%20on%20r%C3%A9p%C3%A8te%203%20fois%0A%20%20%20%20ligne%20%3D%20%5B%5D%20%23cr%C3%A9ation%20d'une%20liste%20vide%0A%20%20%20%20for%20j%20in%20range%284%29%3A%20%23%20ajout%20de%204%20z%C3%A9ros%0A%20%20%20%20%20%20%20%20ligne.append%280%29%0A%20%20%20%20LL.append%28ligne%29%20%23ajout%20de%20la%20liste%20avec%204%20z%C3%A9ros%20%C3%A0%20LL%0ALL&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)


```python
LL = []
for i in range(3): # on répète 3 fois
    ligne = [] #création d'une liste vide
    for j in range(4): # ajout de 4 zéros à ligne
        ligne.append(0)
    LL.append(ligne) #ajout de la liste avec 4 zéros à LL
LL
```




    [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]



Qui peut se condenser en : (visualisation de l'exécution avec [python Tutor](http://pythontutor.com/visualize.html#code=LL%20%3D%20%5B%5D%0Afor%20i%20in%20range%284%29%3A%0A%20%20%20%20LL.append%28%5B0%5D*3%29%20%20%20%20%23%20la%20liste%20%5B0%5D%20*%205%20est%20cr%C3%A9%C3%A9e%20%C3%A0%20chaque%20it%C3%A9ration%20ici.%0ALL&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false))


```python
LL = []
for i in range(4):
    LL.append([0]*3)    # la liste [0] * 3 est créée à chaque itération ici.
LL
```




    [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]



> __La manière la plus concise avec une liste en compréhension__ (celle à retenir): 
>
>```Python
>L = [[0]*4 for i in range(3)]
>```

a(2, 3) = -3, a(1, 2) = 4, a_(2, 2) = 7, a(3, 1) = 0.

Liens et sources utilisées

* [source de l'image de matrice](https://boowiki.info/art/matrices/matrice.html)
