# TD listes de listes définies en compréhension
---

### Exercice 1 : avec des listes en compréhension

__Question  1__ :

Quelle est la valeur de l'expression `[[i for i in range(5)] for j in range(3)]` ?

Réponses :

B- [[0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4]]


__Question 2__ :

De quelle expression la liste suivante est-elle la valeur ?


`[[0,0,0,0], [1,1,1,1], [2,2,2,2]]`
		

Réponses :

C- [[i] * 4 for i in range(3)] 


__Question  3__ :

Quelle est la valeur de l'expression `[[n,n+2] for n in range(3)]` ?

Réponses :

C- [[0,2],[1,3],[2,4]] 


__Question  4__ :

On construit une matrice par compréhension :


`M = [ [i*j for j in range(4)] for i in range(4) ]`
		

Laquelle des conditions suivantes est-elle vérifiée ? (on pourra d'abord écrire la liste `M`.)

Réponses :

A- M[4][4] == 16 #impossible : on dépasse les index des listes

B- M[0][1] == 1 # impossible la première liste n'est composée que de 0

C- M[2][3] == 6 # OUI, c'est celle là

D- M[1][2] == 3 # non, `M[1][2] = 2`

__Question  5__ :

Quelle est la valeur de l'expression `[(a,b) for a in range(3) for b in range(3) if a > b]` ?

Réponses : C- `[(1,0),(2,0),(2,1)]`

La définition de la liste est équivalente à 

```Python
M = []
for a in range(3):
    for b in range(3):
        if a > b:
            M.append((a,b))
```

__Question  6__ :

Quelle est la valeur de l'expression `[[i,2*i] for i in range(3)]` ?

Réponses :

On définit une liste de liste donc la proposition A est impossible.  
Le premier élément inclus est pour `i = 0` et on obtient `[0, 0]`.  
C'est donc la proposition B

B- [[0,0],[1,2],[2,4]]


__Question  7__ :

Quelle est la valeur de l'expression `[[0] * 3 for i in range(2)]` ?

Réponses :

B- [[0,0,0], [0,0,0]]


__Question 8__ :

On représente un plateau de jeu d'échec par une liste de listes dans laquelle on place des 1 pour représenter une case où se trouve une tour et des 0 pour représenter les cases vides.

Par exemple le code

```Python
echiquier = [ [ 0 for i in range(8) ] for j in range(8) ]
echiquier[2][0] = 1
echiquier[3][1] = 1
```

		

représente la situation de la figure ci-dessous.

![équiquier](./fig/bns_4.jpg)

Deux tours sont en prise si elles se trouvent sur une même ligne ou sur une même colonne.

Parmi les codes suivants, lequel permet de vérifier que la tour placée en ligne i et en colonne  j n'est en prise avec aucune tour placée dans les colonnes à sa gauche ?

Réponses :

A- NON : il faudrait mettre `for col in range(j)` (et pas `i`)

```Python
def ok(echiquier,i,j):
		for col in range(i):
			if echiquier[i][col] == 1:
				return False
			return True
```

		

B- NON : on teste ici si aucune tour au dessus de la case `(i, j)`

```Python
def ok(echiquier,i,j):
		for lig in range(i):
			if echiquier[lig][j] == 1:
				return False
			return True
```

		

C- OUI : c'est la bonne réponse

```Python
def ok(echiquier,i,j):
		for col in range(j):
			if echiquier[i][col] == 1:
				return False
		return True
```

		

D-  NON : confusion entre les indices de ligne et de colonne

```Python
def ok(echiquier,i,j):
		for lig in range(j):
			if echiquier[lig][j] == 1:
				return False
		return True
```

		


__Question  9__ :

Laquelle de ces expressions a pour valeur la liste `[[0,1,2],[3,4,5],[6,7,8]]` ?

Réponses :

C- [[i+j*3 for i in range(3)] for j in range(3)] 


