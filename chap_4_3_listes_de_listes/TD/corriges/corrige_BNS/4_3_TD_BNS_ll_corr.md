# TD BNS listes de listes : Corrigé
---

### Exercice 1 : questions BNS

__Question 1__ :

On exécute le code suivant :

```Python
A = [ [1,2,3], [4,5,6], [7,8,9] ]
B = [ [0,0,0], [0,0,0], [0,0,0] ]
for i in range(3):
  for j in range(3):
    B[i][j] = A[j][i]
```

Exécutons la première instruction :

`B[0][0] = A[0][0]` donc `B[0][0] = 1` et c'est forcément la réponse C

Si l'on exécute la deuxième : `B[0][1] = A[1][0]` donc `B[0][1] = 4`
		
C- [ [1,4,7], [2,5,8], [3,6,9] ]

__Question 2__ :


On exécute le script suivant.

```Python
m = []
for i in range(5):
  n = []
  for j in range(3):
    n.append(i*j)
  m.append(n)
```

pour `i = 0`, on va créer la liste `[0 * 0, 0 * 1, 0 * 2]` et l'ajouter à `m`.

Le premier élément de `m` est donc `[0, 0, 0]`.  
Donc réponse B

Quelle est la valeur de m à la fin de son exécution ?

B- [ [0, 0, 0], [0, 1, 2], [0, 2, 4], [0, 3, 6], [0, 4, 8] ]


__Question 3__ :

On considère deux entiers strictement positifs L et C. On note n = L*C leur produit et on écrit la fonction suivante, qui construit un tableau de L lignes et C colonnes, contenant les entiers consécutifs de 0 à n-1 :

```Python
def construitTable(L,C):
  t = []
  for i in range(L):
    ligne = []
    for j in range(C):
      ......
    t.append(ligne)
  return t
```

Ecrivons au brouillon un exemple avec `L = 2` et `C = 3`.

```Python
i = 0 [[0, 1, 2],
i = 1  [3, 4, 5]]
```

On voit donc qu'il faut utiliser l'instruction D `ligne.append(C*i + j)`

__Question 4__ :

On définit une grille G remplie de 0, sous la forme d'une liste de listes, où toutes les sous-listes ont le même nombre d'éléments.


`G =[[0, 0, 0, …, 0],
    [0, 0, 0, …, 0],
    [0, 0, 0, …, 0]
     ….
    [0, 0, 0, …, 0]]`
		

On appelle hauteur de la grille le nombre de sous-listes contenues dans G et largeur de la grille le nombre d'éléments dans chacune de ces sous-listes. Comment peut-on les obtenir ?

Réponses :

B- hauteur = len(G)

largeur = len(G[0])

__Question 5__ :

On dispose d'une table tab constituée d'une liste de trois sous-listes contenant chacune quatre caractères.


`tab=[['A', 'B', 'C', 'D'],
     ['E', 'F', 'G', 'H'],
     ['I', 'J', 'K', 'L'] ]`
		

Parmi les propositions suivantes, laquelle permet de convertir cette table en une liste L contenant dans l'ordre, ligne par ligne, les 12 caractères de tab ?

```Python
# à la fin, on a l'égalité :
L == [  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L' ]
```
		

Réponses :

A-

```Python
L = []
for i in range(3):
  for j in range(4):
    L.append(tab[i][j])
```		

__Question 6__ :

On dispose d'une liste L constituée de 12 caractères.


`L = [  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L' ]`
		

Parmi les propositions suivantes, laquelle permet de convertir cette liste en une table tab constituée d'une liste de trois sous-listes contenant chacune quatre caractères contenant dans l'ordre, et contenant les 12 caractères de L dans l'ordre.

```Python
# à la fin, on a l'égalité :
tab == [['A', 'B', 'C', 'D'],
        ['E', 'F', 'G', 'H'],
        ['I', 'J', 'K', 'L'] ]
```
D-

```Python
tab = []
for i in range(3):
  temp = []
  for j in range(4):
    temp.append(L[4*i + j])
  tab.append(temp)
```

		

__Question 7__ :

On souhaite construire une table de 4 lignes de 3 éléments que l’on va remplir de 0. Quelle syntaxe Python utilisera-t-on ?

Réponses :

A- [ [ 0 ] * 3 for i in range (4) ]


__Question 8__ :

Quelle est la valeur de la variable image après exécution du programme Python suivant ?

```Python
image = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
for i in range(4):
    for j in range(4):
        if (i+j) == 3:
            image[i][j] = 1
```

On met un 1 dans les "cases" `image[i][j]` pour lesquelles `i + j = 3`n soit les couples (0, 3), (1, 2), (2, 1) et (3, 0).
		

Quelle est la valeur de la variable a à la fin de cette exécution ?

Réponses :

C- [[0, 0, 0, 1], [0, 0, 1, 0], [0, 1, 0, 0], [1, 0, 0, 0]]



__Question 9__ :

On a défini :


`mendeleiev = [['H','.', '.','.','.','.','.','He'],
              ['Li','Be','B','C','N','O','Fl','Ne'],
              ['Na','Mg','Al','Si','P','S','Cl','Ar'],
								...... ]`
		

Une erreur s'est glissée dans le tableau, car le symbole du Fluor est F et non Fl. Quelle instruction permet de rectifier ce tableau ?

Réponses :


B- mendeleiev[1][6] = 'F'


__Question 10__ :

On exécute le script suivant :

```Python
asso = []
L =  [	['marc','marie'], ['marie','jean'], ['paul','marie'], ['marie','marie'], ['marc','anne']   ]
for c in L :
     if c[1]==’marie’:
           asso.append(c[0])
```

		

Que vaut asso à la fin de l'exécution ?

On ajoute l'élément d'indice 0 de chaque sous-liste dont l'élément d'indice 1 est `'marie'`.

Réponses :

C- ['marc', 'paul', 'marie'] 



__Question 11__ :

 On a défini :


`mendeleiev = [['H','.', '.','.','.','.','.','He'],
              ['Li','Be','B','C','N','O','Fl','Ne'],
              ['Na','Mg','Al','Si','P','S','Cl','Ar'],
								...... ]`
		

Comment construire la liste des gaz rares, c'est-à-dire la liste des éléments de la dernière colonne ?

Réponses :

A- gaz_rares = [ periode[7] for periode in mendeleiev]


__Question 12__ :


Quelle est la valeur de la variable t1 à la fin de l'exécution du script suivant :

```Python
t1 = [['Valenciennes', 24],['Lille', 23],['Laon', 31],['Arras', 18]]
t2 = [['Lille', 62],['Arras', 53],['Valenciennes', 67],['Laon', 48]]

for i in range(len(t1)):
    for v in t2:
        if v[0] == t1[i][0]:
            t1[i].append(v[1])
```

		

Réponses :

B- [['Valenciennes', 24, 67], ['Lille', 23, 62], ['Laon', 31, 48], ['Arras', 18, 53]]


__Question 13__ :

Quelle est la valeur de x après exécution du programme ci-dessous ?

```Python
t = [[3,4,5,1],[33,6,1,2]]
x = t[0][0]
for i in range(len(t)):
	for j in range(len(t[i])):
		if x < t[i][j]:
			x = t[i][j]
```

On parcour le tableau et si l'élément rencontré est strictement plus grand que `x`, on remplace `x` par cet élément.  
On est donc en train de chercher le maximum du tableau.
		

Réponses : D- 33 
