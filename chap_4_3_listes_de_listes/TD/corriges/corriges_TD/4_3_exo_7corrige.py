def somme_l(matrice,num_ligne):
    
    somme = 0
    nb_col = len(matrice)
    for num_c in range(nb_col):
        somme += matrice[num_ligne][num_c]
        
def somme_l(matrice,num_ligne):
    
    ligne = matrice[num_ligne]
    return sum(ligne)

def somme_c(matrice, num_col):
    
    somme = 0
    nb_lignes = len(matrice)
    for ind_l in range(nb_lignes):
        somme += matrice[ind_l][num_col]
    return somme

def somme_diag_1(matrice):
    somme = 0
    nb_lignes = len(matrice)
    for ind in range(nb_lignes):
        somme += matrice[ind][ind]
    return somme

def somme_diag_2(matrice):
    somme = 0
    nb_l = len(matrice)
    for ind_l in range(nb_l):
        somme += matrice[ind_l][nb_l - 1 - ind_l]
    return somme

def magique(matrice):
    """
    >>> mat_1 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    >>> magique(mat_1)
    False
    >>> mat_2 = [[2, 7, 6], [9, 5, 1], [4, 3, 8]]
    >>> magique(mat_2)
    True
    """

    total_vise = somme_l(matrice, 0)
    nb_l = len(matrice)
    for ind_l in range(nb_l):
        if somme_l(matrice, ind_l) != total_vise:
            return False
        
    for ind_c in range(nb_l):
        if somme_c(matrice, ind_c) != total_vise:
            return False
        
    if (somme_diag_1(matrice) != total_vise) or (somme_diag_2(matrice) != total_vise):
        return False
    
    return True
    
if __name__ == '__main__':
    import doctest
    doctest.testmod()
