def somme_mat(mat_1, mat_2):
    """
    >>> mat_1 = [[1, 2, 3], [4, 5, 6]]
    >>> mat_2 = [[3, -1, 2], [0, 1, 7]]
    >>> somme_mat(mat_1, mat_2)
    [[4, 1, 5], [4, 6, 13]]
    """
    nb_l = len(mat_1)
    nb_c = len(mat_1[0])
    
    mat_somme = [ [0] * nb_c for _ in range(nb_l)]
    
    for ind_l in range(nb_l):
        for ind_c in range(nb_c):
            elt1 = mat_1[ind_l][ind_c]
            elt2 = mat_2[ind_l][ind_c]
            somme = elt1 + elt2
            mat_somme[ind_l][ind_c] = somme
    return mat_somme
            
def somme_mat2(mat_1, mat_2):
    """
    >>> mat_1 = [[1, 2, 3], [4, 5, 6]]
    >>> mat_2 = [[3, -1, 2], [0, 1, 7]]
    >>> somme_mat(mat_1, mat_2)
    [[4, 1, 5], [4, 6, 13]]
    """
    nb_l = len(mat_1)
    nb_c = len(mat_1[0])
    
    mat_somme = []
    
    for ind_l in range(nb_l):
        l_somme = []
        for ind_c in range(nb_c):
            elt1 = mat_1[ind_l][ind_c]
            elt2 = mat_2[ind_l][ind_c]
            somme = elt1 + elt2
            l_somme.append(somme)
        mat_somme.append(l_somme)  

    return mat_somme


if __name__ == '__main__':
    import doctest
    doctest.testmod()