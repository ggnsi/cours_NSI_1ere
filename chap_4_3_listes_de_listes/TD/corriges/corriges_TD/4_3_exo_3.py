def increment(matrice):
    """
    >>> matrix = [[1, 5, -3, 4], [5, 1, 7, 2], [9, 10, 0, 1]]
    >>> increment(matrix)
    >>> matrix
    [[2, 6, -2, 5], [6, 2, 8, 3], [10, 11, 1, 2]]
    """

    nb_lignes = len(matrice)
    nb_col = len(matrice[0])
    for ind_l in range(nb_lignes):
        for ind_c in range(nb_col):
            matrice[ind_l][ind_c] = matrice[ind_l][ind_c] + 1

    
            
 




if __name__ == '__main__':
    import doctest
    doctest.testmod()