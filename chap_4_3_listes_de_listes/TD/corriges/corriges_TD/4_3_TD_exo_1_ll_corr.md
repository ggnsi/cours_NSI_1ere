# TD listes de listes


### Exercice 1 :

__Question 1__ :

On considère la liste de listes suivante :


`tictactoe = [['X', 'O', 'O'],
             ['O', 'O', 'O'],
             ['O', 'O', 'X']]`
		

Quelle instruction permet d'obtenir une diagonale de 'X' ?

Réponse :

C- tictactoe[1][1] = 'X' : on affecté un `X` à l'élément d'indice 1 de la liste d'indice 1

__Question 2__ :

On définit ainsi une liste M :


`M = [['A','B','C','D'], ['E','F','G','H'], ['I','J','K','L']]`
		

Quelle expression vaut la chaîne de caractères 'H' ?

Réponse : `A- M[1][3]`

__Question 3__ :

On a défini : `T = [[1,2,3], [4,5,6], [7,8,9]]`.

Quelle expression parmi les suivantes a pour valeur le nombre 8 ?

Réponse : D- `T[2][1]`

__Question 4__ :

On définit :
`L = [ ["lundi",10,0.87], ["mardi",11,0.82], ["mercredi",12,0.91] ]`

Quel est le type de la variable a définie par `a = L[1][2]` ?

Réponses : C- nombre flottant

__Question 5__ :

Quelle est la valeur de la variable S à la fin de l'exécution du script suivant ?

```Python
res =  [ [1,2,3], [4,5,6], [7,8,9] ]
S = 0
for i in range(3):
	S = S + res[i][2]
```

>On cumule dans la variable `S` la somme de éléments d'indice 2 (les derniers) de chaque liste contenue dans `res`.
> `S = 3 + 6 + 9 = 18`
		

Réponse : C- 18
