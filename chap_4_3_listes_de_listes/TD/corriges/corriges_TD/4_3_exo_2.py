def affichage_l(matrice):
    """
    >>> matrix = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
    >>> affichage_l(matrix)
    1 2 3 4 
    5 6 7 8 
    9 10 11 12 
    """
    
    for ligne in matrice:
        for elt in ligne:
            print(elt, end = ' ')
        print()

def affichage_l_2(matrice):
    """
    >>> matrix = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
    >>> affichage_l_2(matrix)
    1 2 3 4 
    5 6 7 8 
    9 10 11 12 
    """
    nb_lignes = len(matrice)
    nb_colonnes = len(matrice[0])
    for num_l in range(nb_lignes):
        for num_c in range(nb_colonnes):
            print(matrice[num_l][num_c], end = ' ')
        print()

def affichage_l_3(matrice):
    nb_lignes = len(matrice)
    nb_colonnes = len(matrice[0])
    for num_l in range(nb_lignes):
        ligne = matrice[num_l]
        for num_c in range(nb_colonnes):
            print(ligne[num_c], end = ' ')
        print()    

def affichage_c(matrice):
    """
    >>> matrix = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
    >>> affichage_c(matrix)
    1 5 9 
    2 6 10 
    3 7 11 
    4 8 12 
    """
    
    nb_lignes = len(matrice)
    nb_col = len(matrice[0])
    for ind_c in range(nb_col):
        for ind_l in range(nb_lignes):
            print(matrice[ind_l][ind_c], end = ' ')
        print()
        
def affichage_c2(matrice):
    """
    >>> matrix = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
    >>> affichage_c(matrix)
    1 5 9 
    2 6 10 
    3 7 11 
    4 8 12 
    """
    
    nb_lignes = len(matrice)
    nb_col = len(matrice[0])
    for ind_c in range(nb_col):
        for ligne in matrice:
            print(ligne[ind_c], end = ' ')
        print()

def affichage_coeff(matrice):
    """
    >>> matrix = [[1, 2], [3, 4]]
    >>> affichage_coeff(matrix)
    ligne 1 colonne 1 : 1
    ligne 1 colonne 2 : 2
    ligne 2 colonne 1 : 3
    ligne 2 colonne 2 : 4
    """
    
    nb_lignes = len(matrice)
    nb_col = len(matrice[0])
    for ind_l in range(nb_lignes):
        for ind_c in range(nb_col):
            print('ligne', ind_l + 1, 'colonne', ind_c + 1,':', matrice[ind_l][ind_c])
    
def affichage(tableau):
    """
    >>> tableau = [[1, 2 ], [3, 4, 5 ], [6, 7, 8, 9]]
    >>> affichage(tableau)
    1 2 
    3 4 5 
    6 7 8 9 
    """
    
    for ligne in tableau:
        for elt in ligne:
            print(elt, end = ' ')
        print()
        
def affichage2(tableau):
    """
    >>> tableau = [[1, 2 ], [3, 4, 5 ], [6, 7, 8, 9]]
    >>> affichage(tableau)
    1 2 
    3 4 5 
    6 7 8 9 
    """
    
    nb_ligne = len(tableau)
    for ind_l in range(nb_ligne):
        nb_col = len(tableau[ind_l])
        for ind_c in range(nb_col):
            print(tableau[ind_l][ind_c], end = ' ')
        print()
    
    
            
 




if __name__ == '__main__':
    import doctest
    doctest.testmod()