import random

def mat_alea(m,n):
    
    tab_vide = [[0] * n for _ in range(m)]
    
    for ind_l in range(m):
        for ind_c in range(n):
            nb = random.randint(1, 100)
            tab_vide[ind_l][ind_c] = nb
    return tab_vide

def mat_alea2(m,n):
    
    l_sortie = []
    for _ in range(m):
        ligne = []
        for _ in range(n):
            # ajouter un nombre choisi au hasard
            nb = random.randint(1, 100)
            ligne.append(nb)
        # ajouter la ligne à la liste de sortie
        l_sortie.append(ligne)
    return l_sortie

def mat_alea3(m,n):
    
    l_sortie = []
    for _ in range(m):
        # avec une liste en compréhension
        ligne = [random.randint(1, 100) for _ in range(n)]
        # ajouter la ligne à la liste de sortie
        l_sortie.append(ligne)
    return l_sortie
    
