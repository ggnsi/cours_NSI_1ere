# TD listes de listes - questions de la BNS
---

__Question 1__ :

On exécute le code suivant :

```Python
A = [ [1,2,3], [4,5,6], [7,8,9] ]
B = [ [0,0,0], [0,0,0], [0,0,0] ]
for i in range(3):
  for j in range(3):
    B[i][j] = A[j][i]
```

		

Que vaut B à la fin de l'exécution ?

Réponses :

A- rien du tout, le programme déclenche une erreur d'exécution

B- `[ [3,2,1], [6,5,4], [9,8,7] ]`

C- `[ [1,4,7], [2,5,8], [3,6,9] ]`

D- `[ [7,8,9], [4,5,6], [1,2,3] ]` 

__Question 2__ :


On exécute le script suivant.

```Python
m = []
for i in range(5):
  n = []
  for j in range(3):
    n.append(i*j)
  m.append(n)
```


Quelle est la valeur de m à la fin de son exécution ?

Réponses :

A- `[ [0, 0, 0, 0, 0], [0, 1, 2, 3, 4], [0, 2, 4, 6, 8] ]`

B- `[ [0, 0, 0], [0, 1, 2], [0, 2, 4], [0, 3, 6], [0, 4, 8] ]`

C- `[ [1, 1, 1], [2, 4, 6], [3, 6, 9], [4, 8, 12], [5, 10, 15] ]`

D- `[ [1, 1, 1, 1, 1], [2, 4, 6, 8, 10], [3, 6, 9, 12, 15], [4, 8, 12, 16, 20], [5, 10, 15, 20, 25] ]` 

__Question 3__ :

On considère deux entiers strictement positifs L et C. On note n = L*C leur produit et on écrit la fonction suivante, qui construit un tableau de L lignes et C colonnes, contenant les entiers consécutifs de 0 à n-1 :

```Python
def construitTable(L,C):
  t = []
  for i in range(L):
    ligne = []
    for j in range(C):
      ......
    t.append(ligne)
  return t
```
	

Par exemple, l'appel construitTable(2,3) doit renvoyer la table : [[0, 1, 2],[3, 4, 5]]

Que faut-il écrire à la place des points de suspension pour obtenir ce résultat ?

Réponses :

A- `ligne.append(i + C*j)`

B- `ligne.append(L*i + j)`

C- `ligne.append(i + L*j)`

D- `ligne.append(C*i + j)` 

__Question 4__ :

On définit une grille G remplie de 0, sous la forme d'une liste de listes, où toutes les sous-listes ont le même nombre d'éléments.


`G =[[0, 0, 0, …, 0],
    [0, 0, 0, …, 0],
    [0, 0, 0, …, 0]
     ….
    [0, 0, 0, …, 0]]`
		

On appelle hauteur de la grille le nombre de sous-listes contenues dans G et largeur de la grille le nombre d'éléments dans chacune de ces sous-listes. Comment peut-on les obtenir ?

Réponses :

A- `hauteur = len(G[0])`

`largeur = len(G)`

B- `hauteur = len(G`)

`largeur = len(G[0])`

C- `hauteur = len(G[0])`

`largeur = len(G[1])`

D- `hauteur = len(G[1])`

`largeur = len(G[0])` 

__Question 5__ :

On dispose d'une table tab constituée d'une liste de trois sous-listes contenant chacune quatre caractères.


`tab=[['A', 'B', 'C', 'D'],
     ['E', 'F', 'G', 'H'],
     ['I', 'J', 'K', 'L'] ]`
		

Parmi les propositions suivantes, laquelle permet de convertir cette table en une liste L contenant dans l'ordre, ligne par ligne, les 12 caractères de tab ?

```Python
# à la fin, on a l'égalité :
L == [  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L' ]
```

		

Réponses :

A-

```Python
L = []
for i in range(3):
  for j in range(4):
    L.append(tab[i][j])
```

		

B-

```Python
L = []
for i in range(4):
  for j in range(3):
    L.append(tab[i][j])
		
```


C-
```Python
L = []
for i in range(3):
  L.append(tab[i])
```


		

D-

```Python
L = []
for i in range(4):
  L.append(tab[i])
```
		

__Question 6__ :

On dispose d'une liste L constituée de 12 caractères.


`L = [  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L' ]`
		

Parmi les propositions suivantes, laquelle permet de convertir cette liste en une table tab constituée d'une liste de trois sous-listes contenant chacune quatre caractères contenant dans l'ordre, et contenant les 12 caractères de L dans l'ordre.

```Python
# à la fin, on a l'égalité :
tab == [['A', 'B', 'C', 'D'],
        ['E', 'F', 'G', 'H'],
        ['I', 'J', 'K', 'L'] ]
```

		

Réponses :

A-

```Python
tab = []
for i in range(4):
  temp = []
  for j in range(3):
    temp.append(L[4*i + j])
	tab.append(temp)
```

		

B-
```Python
tab = []
for i in range(4):
  temp = []
  for j in range(3):
    temp.append(L[3*i + j])
  tab.append(temp)
```


		

C-

```Python
tab = []
for i in range(3):
  temp = []
  for j in range(4):
    temp.append(L[3*i + j])
  tab.append(temp)
```

		

D-

```Python
tab = []
for i in range(3):
  temp = []
  for j in range(4):
    temp.append(L[4*i + j])
  tab.append(temp)
```

		

__Question 7__ :

On souhaite construire une table de 4 lignes de 3 éléments que l’on va remplir de 0. Quelle syntaxe Python utilisera-t-on ?

Réponses :

A- [ [ 0 ] * 3 for i in range (4) ]

B- for i in range (4) [ 0 ] * 3

C- [ 0 ] * 3 for i in range (4)

D- [ for i in range (4) [ 0 ] * 3 ] 

__Question 8__ :

Quelle est la valeur de la variable image après exécution du programme Python suivant ?

```Python
image = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
for i in range(4):
    for j in range(4):
        if (i+j) == 3:
            image[i][j] = 1
```

		

Quelle est la valeur de la variable a à la fin de cette exécution ?

Réponses :

A- `[[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 1]]`

B- `[[0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1]]`

C- `[[0, 0, 0, 1], [0, 0, 1, 0], [0, 1, 0, 0], [1, 0, 0, 0]]`

D- `[[0, 0, 0, 1], [0, 0, 1, 1], [0, 1, 1, 1], [1, 1, 1, 1]]` 

__Question 9__ :

On a défini :


`mendeleiev = [['H','.', '.','.','.','.','.','He'],
              ['Li','Be','B','C','N','O','Fl','Ne'],
              ['Na','Mg','Al','Si','P','S','Cl','Ar'],
								...... ]`
		

Une erreur s'est glissée dans le tableau, car le symbole du Fluor est F et non Fl. Quelle instruction permet de rectifier ce tableau ?

Réponses :

A- `mendeleiev.append('F')`

B- `mendeleiev[1][6] = 'F'`

C- `mendeleiev[6][1] = 'F'`

D- `mendeleiev[-1][-1] = 'F'` 

__Question 10__ :

On exécute le script suivant :

```Python
asso = []
L =  [	['marc','marie'], ['marie','jean'], ['paul','marie'], ['marie','marie'], ['marc','anne']   ]
for c in L :
     if c[1]==’marie’:
           asso.append(c[0])
```

		

Que vaut asso à la fin de l'exécution ?

Réponses :

A- `['marc', 'jean', 'paul']`

B- `[['marc','marie'], ['paul','marie'], ['marie','marie']]`

C- `['marc', 'paul', 'marie']` 

D- `['marie', 'anne']` 

__Question 11__ :

 On a défini :


`mendeleiev = [['H','.', '.','.','.','.','.','He'],
              ['Li','Be','B','C','N','O','Fl','Ne'],
              ['Na','Mg','Al','Si','P','S','Cl','Ar'],
								...... ]`
		

Comment construire la liste des gaz rares, c'est-à-dire la liste des éléments de la dernière colonne ?

Réponses :

A- `gaz_rares = [ periode[7] for periode in mendeleiev]`

B- `gaz_rares = [ periode for periode in mendeleiev[7]]`

C- `gaz_rares = [ periode for periode[7] in mendeleiev]`

D- `gaz_rares = [ periode[8] for periode in mendeleiev]` 

__Question 12__ :


Quelle est la valeur de la variable t1 à la fin de l'exécution du script suivant :

```Python
t1 = [['Valenciennes', 24],['Lille', 23],['Laon', 31],['Arras', 18]]
t2 = [['Lille', 62],['Arras', 53],['Valenciennes', 67],['Laon', 48]]

for i in range(len(t1)):
    for v in t2:
        if v[0] == t1[i][0]:
            t1[i].append(v[1])
```

		

Réponses :

A- `[['Valenciennes', 67], ['Lille', 62], ['Laon', 48], ['Arras', 53]]`

B- `[['Valenciennes', 24, 67], ['Lille', 23, 62], ['Laon', 31, 48], ['Arras', 18, 53]]`

C- `[['Arras', 18, 53],['Laon', 31, 48], ['Lille', 23, 62], ['Valenciennes', 24, 67]]`

D- `[['Valenciennes', 67, 24], ['Lille', 62,23], ['Laon', 48, 31], ['Arras', 53, 18]]` 


__Question 13__ :

Quelle est la valeur de x après exécution du programme ci-dessous ?

```Python
t = [[3,4,5,1],[33,6,1,2]]
x = t[0][0]
for i in range(len(t)):
	for j in range(len(t[i])):
		if x < t[i][j]:
			x = t[i][j]
```

		

Réponses :

A- 3

B- 5

C- 6

D- 33 

__Question 14__ :

Soit une liste nommée `bizarre = [1,[2,3],[4,5],6,7]`.
Que vaut `len(bizarre)` ?

Réponses :

    A. 1
    B. 3
    C. 5
    D. 7

## Plus dur

__Question 1__ :

t1 est un tableau à n lignes et n colonnes. On souhaite remplir un tableau t2 de mêmes dimensions que t1 avec les contraintes suivantes : les lignes de t2 sont les colonnes de t1 et les colonnes de t2 sont les lignes de t1.

Par quelle instruction faut-il remplacer la ligne en pointillées du code suivant ?

```Python
for i in range(n):
	for j in range(n):
		......
```
	

Réponses :

A- t1[i][j] = t2[j][i]

B- t2[j][i] = t1[j][i]

C- t1[j][i] = t2[i][j]  

D- t2[i][j] = t1[j][i] 

__Question 2__ :

Soient n et p deux entiers au moins égaux à 2. On définit une liste de listes t par le code suivant :

```Python
# n et p sont initialisés dans les lignes précédentes

t = [ [ 0 for j in range(p) ] for i in range(n) ]

for k in range(n*p):
  t[k%n][k%p] = k
```


Une et une seule des affirmations suivantes est fausse. Laquelle ?

Réponses :

A- La liste t contient des entiers tels que 0 <= k < n x p

B- Pour tout j tel que 0 <= j < n-1, t[j][0] est un multiple de p.

C- La liste t[0] contient des entiers qui sont tous multiples de n.

D- Pour tout j tel que 0 <= j < n-1, t[0][j] est un multiple de p. 
