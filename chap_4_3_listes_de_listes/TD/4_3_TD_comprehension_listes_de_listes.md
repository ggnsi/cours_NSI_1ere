# TD listes de listes définies en compréhension

---

### Exercice 1

__Question  1__ :

Quelle est la valeur de l'expression `[[i for i in range(5)] for j in range(3)]` ?

Réponses :

A- `[[0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2], [0, 1, 2]]`

B- `[[0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4]]`

C- `[[0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3]]`

D- `[[0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5]]`

**Question 2** :

De quelle expression la liste suivante est-elle la valeur ?


`[[0,0,0,0], [1,1,1,1], [2,2,2,2]]`
		

Réponses :

A- `[[i] * 4 for i in range(4)]`

B- `[[i] * 3 for i in range(4)]`

C- `[[i] * 4 for i in range(3)]` 

D- `[[i] * 3 for i in range(3)]` 


__Question  3__ :

Quelle est la valeur de l'expression `[[n,n+2] for n in range(3)]` ?

Réponses :

A- `[0,2,1,3,2,4]`

B- `[1,3,2,4,3,5]`

C- `[[0,2],[1,3],[2,4]]` 

D- `[[1,3],[2,4],[3,5]]` 

__Question  4__ :

On construit une matrice par compréhension :


`M = [ [i*j for j in range(4)] for i in range(4) ]`
		

Laquelle des conditions suivantes est-elle vérifiée ? (on pourra d'abord écrire la liste `M`.)

Réponses :

A- `M[4][4] == 16`

B- `M[0][1] == 1`

C- `M[2][3] == 6` 

D- `M[1][2] == 3`

__Question  5__ :

Quelle est la valeur de l'expression `[(a,b) for a in range(3) for b in range(3) if a > b]` ?

Réponses :

A- `[(a,b),(a,b),(a,b),(a,b),(a,b),(a,b),(a,b),(a,b),(a,b)]`

B- `[(0,0),(0,1),(0,2),(1,0),(1,1),(1,2),(2,0),(2,1),(2,2)]`

C- `[(1,0),(2,0),(2,1)]` 

D- `[(0,0),(0,1),(0,2),(1,0),(1,1),(1,2),(1,0),(1,1),(1,2)]` 



__Question  6__ :

Quelle est la valeur de l'expression `[[i,2*i] for i in range(3)]` ?

Réponses :

A- `[0,0,1,2,2,4]`

B- `[[0,0],[1,2],[2,4]]`

C- `[1,2,2,4,3,6]`

D- `[[1,2],[2,4],[3,6]]` 


__Question  7__ :

Quelle est la valeur de l'expression `[[0] * 3 for i in range(2)]` ?

Réponses :

A- `[[0,0], [0,0], [0,0]]`

B- `[[0,0,0], [0,0,0]]`

C- `[[0.000], [0.000]]`

D- `[[0.00], [0.00], [0.00]]` 

__Question 8__ :

On représente un plateau de jeu d'échec par une liste de listes dans laquelle on place des 1 pour représenter une case où se trouve une tour et des 0 pour représenter les cases vides.

Par exemple le code

```Python
echiquier = [ [ 0 for i in range(8) ] for j in range(8) ]
echiquier[2][0] = 1
echiquier[3][1] = 1
```

		

représente la situation de la figure ci-dessous.

![équiquier](./fig/bns_4.jpg)

Deux tours sont en prise si elles se trouvent sur une même ligne ou sur une même colonne.

Parmi les codes suivants, lequel permet de vérifier que la tour placée en ligne i et en colonne  j n'est en prise avec aucune tour placée dans les colonnes à sa gauche ?

Réponses :

A-

```Python
def ok(echiquier,i,j):
		for col in range(i):
			if echiquier[i][col] == 1:
				return False
			return True
```

		

B-

```Python
def ok(echiquier,i,j):
		for lig in range(i):
			if echiquier[lig][j] == 1:
				return False
			return True
```

		

C-

```Python
def ok(echiquier,i,j):
		for col in range(j):
			if echiquier[i][col] == 1:
				return False
		return True
```

		

D-

```Python
def ok(echiquier,i,j):
		for lig in range(j):
			if echiquier[lig][j] == 1:
				return False
		return True
```

		


__Question  9__ :

Laquelle de ces expressions a pour valeur la liste `[[0,1,2],[3,4,5],[6,7,8]]` ?

Réponses :

A- `[[i+j for i in range(3)] for j in range(3)]`

B- `[[i]*3 for i in range(3)]*3`

C- `[[i+j*3 for i in range(3)] for j in range(3)]` 

D- `[[i+j for i in range(3)] for j in range(3)]*3` 





### Exercice 2 :

Créer une liste en compréhension en deux dimensions `4 x 5` qui ne contient que des `0` (4 lignes et 5 colonnes).

### Exercice 3 : listes en compréhension niveau 2

**Niveau plus difficile pour ceux qui veulent s'amuser.**

---



1) Créer une liste en compréhension à deux dimensions 4x4 qui comporte uniquement les 16 premiers entiers impairs. 

2) Créer une liste en compréhension à deux dimensions 3x3 qui comportent un 0 si la somme de l'indice de ligne et l'indice de colonne est paire sinon un 1.    

3) Créer une liste par compréhension qui a pour affichage:  
(On créera une fonction pour créer la liste et on utilisera le cours pour créer une fonction qui affichera cette liste.)  

1 0 0 0 0 0 0 0 0 0  
0 1 0 0 0 0 0 0 0 0  
0 0 1 0 0 0 0 0 0 0  
0 0 0 1 0 0 0 0 0 0  
0 0 0 0 1 0 0 0 0 0  
0 0 0 0 0 1 0 0 0 0  
0 0 0 0 0 0 1 0 0 0  
0 0 0 0 0 0 0 1 0 0  
0 0 0 0 0 0 0 0 1 0  
0 0 0 0 0 0 0 0 0 1   

4) Créer une liste par compréhension qui a pour affichage:

1 0 0 0 0 0 0 0 0 0  
1 1 0 0 0 0 0 0 0 0  
1 1 1 0 0 0 0 0 0 0  
1 1 1 1 0 0 0 0 0 0  
1 1 1 1 1 0 0 0 0 0  
1 1 1 1 1 1 0 0 0 0  
1 1 1 1 1 1 1 0 0 0  
1 1 1 1 1 1 1 1 0 0  
1 1 1 1 1 1 1 1 1 0  
1 1 1 1 1 1 1 1 1 1  

5) Créer une liste par compréhension qui a pour affichage:  

1 0 0 0 0 0 0 0 0 1  
0 1 0 0 0 0 0 0 1 0  
0 0 1 0 0 0 0 1 0 0  
0 0 0 1 0 0 1 0 0 0  
0 0 0 0 1 1 0 0 0 0  
0 0 0 0 1 1 0 0 0 0  
0 0 0 1 0 0 1 0 0 0  
0 0 1 0 0 0 0 1 0 0  
0 1 0 0 0 0 0 0 1 0  
1 0 0 0 0 0 0 0 0 1  



### Exercice 4 :

__Question  1__ :

Soient n et p deux entiers au moins égaux à 2. On définit une liste de listes t par le code suivant :

```Python
# n et p sont initialisés dans les lignes précédentes

t = [ [ 0 for j in range(p) ] for i in range(n) ]

for k in range(n*p):
  t[k%n][k%p] = k
```


Une et une seule des affirmations suivantes est fausse. Laquelle ?

Réponses :

A- La liste t contient des entiers tels que 0 <= k < n x p

B- Pour tout j tel que 0 <= j < n-1, t[j][0] est un multiple de p.

C- La liste t[0] contient des entiers qui sont tous multiples de n.

D- Pour tout j tel que 0 <= j < n-1, t[0][j] est un multiple de p. 

__Question  2__ :

t1 est un tableau à n lignes et n colonnes. On souhaite remplir un tableau t2 de mêmes dimensions que t1 avec les contraintes suivantes : les lignes de t2 sont les colonnes de t1 et les colonnes de t2 sont les lignes de t1.

Par quelle instruction faut-il remplacer la ligne en pointillées du code suivant ?

```Python
for i in range(n):
	for j in range(n):
		......
```
	

Réponses :

A- `t1[i][j] = t2[j][i]`

B- `t2[j][i] = t1[j][i]`

C- `t1[j][i] = t2[i][j] ` 

D- `t2[i][j] = t1[j][i] `
