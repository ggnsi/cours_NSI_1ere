# TD listes de listes


### Exercice 1 :

__Question 1__ :

On considère la liste de listes suivante :


`tictactoe = [['X', 'O', 'O'],
             ['O', 'O', 'O'],
             ['O', 'O', 'X']]`
		

Quelle instruction permet d'obtenir une diagonale de 'X' ?

Réponses :

A- `tictactoe[3] = 'X'`

B- `tictactoe[4] = 'X'`

C- `tictactoe[1][1] = 'X'`

D- `tictactoe[2][2] = 'X'`

__Question 2__ :

On définit ainsi une liste M :


`M = [['A','B','C','D'], ['E','F','G','H'], ['I','J','K','L']]`
		

Quelle expression vaut la chaîne de caractères 'H' ?

Réponses :

A- `M[1][3]`

B- `M[3][1]`

C- `M(7)`

D- `M(8)` 

__Question 3__ :

On a défini : `T = [[1,2,3], [4,5,6], [7,8,9]]`.

Quelle expression parmi les suivantes a pour valeur le nombre 8 ?

Réponses :

A- `T[1,2]`

B- `T[1][2]`

C- `T[2,1]`

D- `T[2][1]`

__Question 4__ :

On définit :
`L = [ ["lundi",10,0.87], ["mardi",11,0.82], ["mercredi",12,0.91] ]`

Quel est le type de la variable a définie par `a = L[1][2]` ?

Réponses :

A- nombre entier

B- liste

C- nombre flottant

D- chaîne de caractères 

__Question 5__ :

Quelle est la valeur de la variable S à la fin de l'exécution du script suivant ?

```Python
res =  [ [1,2,3], [4,5,6], [7,8,9] ]
S = 0
for i in range(3):
	S = S + res[i][2]
```

		

Réponses :

A- 12

B- 15

C- 18

D- 24 



### Exercice 2 : je sais parcourir

1. Créez une fonction `affichage_l(matrice)` où `matrice` est une liste de listes représentant une matrice (donc toutes les sous-listes ont même longueur) et qui affiche cette matrice sous forme de tableau, c'est à dire chaque ligne de la matrice (chaque sous-liste) affichée sur une ligne différente avec un espace entre ses éléments (il peut y avoir un espace (invisible) au bout de chaque ligne).

   **Coder 2 versions : l'une n'utilisant pas les indices, l'autre oui.**

    _Exemple_ : 

```Python
>>> matrix = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
>>> affichage_l(matrix)
1 2 3 4 
5 6 7 8 
9 10 11 12 
```

2. Créez une fonction `affichage_c(matrice)` qui affiche la matrice en affichant chaque colonne sur une ligne.

    _Exemple_ : 

```Python
>>> matrix = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
>>> affichage_c(matrix)
1 5 9 
2 6 10 
3 7 11 
4 8 12 
```

3. Créez une fonction `affichage_coeff(matrice)` qui affiche chaque coefficient de la matrice précédé de son numéro de ligne et de colonne. La matrice est parcourue ligne par ligne.

    **Attention ici : on considère les numéros de lignes et de colonnes 'mathématiques' et donc commençant à 1.**

    _Exemple_ : 

```Python
>>> matrix = [[1, 2], [3, 4]]
>>> affichage_coeff(matrix)
ligne 1 colonne 1 : 1
ligne 1 colonne 2 : 2
ligne 2 colonne 1 : 3
ligne 2 colonne 2 : 4
```

4. Créez une fonction `affichage(tableau)` où `tableau` est une liste de listes (pas forcément toutes de la même longueur) et qui, comme à la question 1, affiche le contenu de chaque ligne du tableau les unes en dessous des autres.

    _Exemple_ : 

```Python
>>> tableau = [[1, 2 ], [3, 4, 5 ], [6, 7, 8, 9]]
>>> affichage(tableau)
1 2 
3 4 5 
6 7 8 9 
```

### Exercice 3 :

Créez une fonction `increment(matrice)` (où `matrice` est une liste de listes de même longueur contenant des entiers) qui incrémente de `1` tous les éléments de matrice.

_Remarque_ : la matrice passée en paramètre sera modifiée en place.

_Exemple_ : 

```Python
>>> matrix = [[1, 5, -3, 4], [5, 1, 7, 2], [9, 10, 0, 1]]
>>> affichage_l(matrix)
>>> matrix
[[2, 6, -2, 5], [6, 2, 8, 3], [10, 11, 1, 2]]
```


### Exercice 4 :

Ecrire une fonction `mat_alea(m,n)`, où `m` et `n` sont deux entiers supérieurs ou égaux à 1, qui renvoie une matrice à `m` lignes et `n` colonnes remplie d'entiers aléatoires compris entre `0` et `100` (inclus).

### Exercice 5 : somme de deux matrices

En mathématiques, l'addition de deux matrices de même taille se fait coefficient par coefficient.

_Exemple_ :

<img src="./fig/listes_de_listes_td_fig1.jpg" alt= "somme de deux matrices" width="500">

<!--- Si $ A = \begin{pmatrix}
1 & 2 & 3 \\ 
4 & 5 & 6
\end{pmatrix} $ et $B = \begin{pmatrix}
3 & -1 & 2 \\ 
0 & 1 & 7
\end{pmatrix}$,

alors $A + B = \begin{pmatrix}
4 & 1 & 5 \\ 
4 & 6 & 13
\end{pmatrix} $ -->


On additionne les coefficients des deux matrices situés aux mêmes endroits.

Ecrire une fonction `somme_mat(mat_1, mat_2)`, où `mat_1` et `mat_2` représentent des matrices de même taille, et qui renvoie la matrice somme représentée par une liste de liste.

_Exemple_ : 

```Python
>>> mat_1 = [[1, 2, 3], [4, 5, 6]]
>>> mat_2 = [[3, -1, 2], [0, 1, 7]]
>>> somme_mat(mat_1, mat_2)
[[4, 1, 5], [4, 6, 13]]
```


### Exercice 6

1. On dispose d'une liste L constituée de 12 caractères.  
  `L = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L' ]`
  
    Ecrire une fonction `transform()` qui produit (et renvoie) à partir de cette liste une table constituée d'une liste de trois sous-listes contenant chacune quatre caractères obtenus successivement à partir des 12 caractères de L dans l'ordre.

    _Remarque_ : la liste `L` sera donc ici incluse dans la fonction `transform()`.

```Python
>>> transform()
[['A', 'B', 'C', 'D'], ['E', 'F', 'G', 'H'], ['I', 'J', 'K', 'L'] ]
```

2. On dispose d'un tableau constituée d'une liste listes (pas forcément de même longueur).

    Ecrire une fonction `transform2(tableau)` qui permet de convertir ce tableau en une liste  contenant dans l'ordre, ligne par ligne, les éléments de `tableau` ?

    _Exemple_ : 

```Python
>>> tab = [['A', 'B', 'C', 'D'], ['E', 'F', 'G'], ['H', 'I']]
>>> transform2(tab)
['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I']
```

### Exercice 7 : Carré magique

>En mathématiques, un carré magique d'ordre `n` est composé de `n^2` entiers strictement positifs, écrits sous la forme d'un tableau __carré__. Ces nombres sont disposés de sorte que leurs sommes sur chaque rangée, sur chaque colonne et sur chaque diagonale principale soient égales. (source : Wikipédia)

<img src="./fig/2560px-Magicsquareexample.svg.png" alt="Carré magique" style="width: 350px;"/>


1. Ecrire une fonction `somme_l(matrice,num_ligne)`, où `matrice` représente une matrice et `num_ligne` est un entier,  qui renvoie la somme des éléments contenus dans la ligne d'indice `num_ligne` de la matrice.

    _Remarque_ : on pourra éventuellement utiliser ici la fonction `sum` (se documenter)
    
    _Exemple_ :
    
    ```Python
    >>> mat_1 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    >>> somme_l(mat_1, 1)
    15
    ```
    
    
2. Ecrire une fonction `somme_c(matrice, num_col)`,  où `matrice` représente une matrice et `num_col` est un entier,  qui renvoie la somme des éléments contenus dans la colonne d'indice `num_col` de la matrice.

    _Exemple_ :
    
    ```Python
    >>> mat_1 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    >>> somme_c(mat_1, 0)
    12
    ```

3. Ecrire une fonction `somme_diag_1(matrice)`,  où `matrice` représente une matrice, qui renvoie la somme des éléments contenus sur diagonale principale de la matrice (celle qui part d'en haut à gauche jusqu'en bas à droite)

    _Exemple_ :
    
    ```Python
    >>> mat_1 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    >>> somme_diag_1(mat_1)
    15 # 1 + 5 + 9
    ```

3. Ecrire une fonction `somme_diag_2(matrice)`,  où `matrice` représente une matrice, qui renvoie la somme des éléments contenus sur l'anti-diagonale de la matrice (celle qui part d'en haut à droite jusqu'en bas à gauche).

    _Exemple_ :
    
    ```Python
    >>> mat_1 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    >>> somme_diag_2(mat_1)
    15  # 3 + 5 + 7
    ```

4. Ecrire une fonction `magique(matrice)` qui renvoie `True` si la matrice est magique et `False` sinon.

    _Exemples_ :
    
    ```Python
    >>> mat_1 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    >>> magique(mat_1)
    False
    >>> mat_2 = [[2, 7, 6], [9, 5, 1], [4, 3, 8]]
    >>> magique(mat_2)
    True    
    ```

### Autres idées d'exercices

* Construction du triangle de Pascal
* Jeu Quattro
* 
