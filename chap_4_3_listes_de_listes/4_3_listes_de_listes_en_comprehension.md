## Listes de listes en compréhension

### Principe des listes en compréhension avec imbrication

>Des listes en compréhension peuvent être imbriquées.
>
>Attention à l'ordre dans lequel les boucles `for` sont effectuées

_Exemple_ : [^ 1]

Soit à créer une liste formée de tous « mots » commençant par une lettre A ou B et suivie d’un chiffre parmi 1, 2 ou 3. On peut utiliser une liste en compréhension :

```Python
L = [x+y for x in "AB" for y in "123"]
print(L)
```

```Python
['A1', 'A2', 'A3', 'B1', 'B2', 'B3']
```

Le résultat ligne 3 montre que l’on fixe d’abord la lettre, c’est-à-dire les éléments du `for` lexicalement le plus interne (ici `for x`) puis que `y` varie. Ce n’est pas forcément très intuitif.

> **A retenir**
>
>Le résultat est plus facilement compréhensible si la liste en compréhension est interprétée par deux boucles for imbriquées exactement dans l’ordre où on lit les apparitions des `for` dans la liste en compréhension, ce qui donne ici :



```Python
>>> L = []
>>> for x in "AB":
>>>     for y in "123":
>>>         L.append(x+y)
>>> print(L)

['A1', 'A2', 'A3', 'B1', 'B2', 'B3']
```

> La boucle `for x` apparaît avant la boucle `for y`, dans le même ordre que la liste en compréhension `[x+y for x in "AB" for y in "123"]`.

_Remarque_ : ci-dessous, pour chaque valeur de `y`, une liste est créée par itération sur `x`.

```Python
>>> L = [[x+y for x in "AB"] for y in "123"]

[['A1', 'B1'], ['A2', 'B2'], ['A3', 'B3']]
```


#### Initialisation d'une matrice en compréhension

Initialisation d'une matrice comportant 4 lignes et 5 colonnes remplies de 0 :

```Python
LL = [[0 for i in range(4)] for j in range(5)]
```

ou, comme 0 est immuable, comme déjà vu plus haut :

```Python
LL = [[0]*4 for j in range(5)]
```



### Listes en compréhension avec imbrication et test

Soit à placer dans une liste L tous les couples d’entiers $(x,y)$

tel que `0 ≤ x,y ≤ 3` et `x + y ≤ 3`

Voici un code utilisant une liste en compréhension :


```Python
>>> L =[[x,y] for x in range(0,4) for y in range(0,4) if x+y <= 3]
>>> print(L)

[[0, 0], [0, 1], [0, 2], [0, 3], [1, 0], [1, 1], [1, 2], [2, 0], [2, 1], [3, 0]]
```

La syntaxe de la liste en compréhension a été prévue pour que la traduction depuis des boucles `for` imbriquées soit immédiate : on place les `for` et `if` dans l’ordre de gauche à droite où ils apparaissent dans la liste en compréhension :

```Python
L =[]
for x in range(0,4):
    for y in range(0,4):
        if x+y <= 3:
            L.append([x,y])
print(L)
```

Liens et sources utilisées

* [^1] http://pascal.ortiz.free.fr/contents/python/listes_comprehension/listes_comprehension.html
* [source de l'image de matrice](https://boowiki.info/art/matrices/matrice.html)
