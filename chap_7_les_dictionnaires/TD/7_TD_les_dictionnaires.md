# Les dictionnaires - TD
---

### Exercice 1 : premières opérations

__Les opérations sont ici à effectuer via la console__

* Créez un dictionnaire nommé `placard_1` dont les clés sont :
  * `'chemise'` avec une valeur 6 ;
  * `'pantalon'` avec une valeur 4
* Affichez votre dictionaire ;
* Ajoutez la clé `'chaussette'` avec une valeur de 10 ;
* Ajoutez la clé `'pull'` avec une valeur de 4 ;
* Affichez votre dictionnaire ;*
* Quelle instruction entrer pour savoir combien il y a de chemises dans votre placard ?
* Quelle instruction entrer pour savoir combien il y a de chaussettes dans votre placard ?
* modifiez la valeur associée à la clé `'chemise'` pour qu'elle soit maintenant de 12 puis affichez votre dictionnaire pour vérifier.
* Suppimez l'entrée `'pantalon'` de votre dictionnaire.

   


### Exercice 2 : Décompte des points d'un mot au Scrabble [^2]

Au Scrabble, chaque lettre est affectée d'un nombre de points.  
Le total des point d'un mot se fait en additionnant les points de chaque lettre.  
Compléter la fonction `points(mot)` ci-dessous afin qu'elle réponde au spécifications indiquées dans la docstring.

```Python
def points(mot):
    """
    Compte le nombre de points d'une chaine de caractères au jeu de Scrabble
    :param mot: une chaine de caractères composée de lettres de l'alphabet en majuscules
    :return: le nombre de points au Scrabble (un entier)
    >>> points("BONJOUR")
    16
    >>> points("ANTICONSTITUTIONNELLEMENT")
    28
    """
    points_lettre = {"A":1,"B":3,"C":3,"D":2,"E":1,"F":4,"G":2,"H":4,"I":1,"J":8,"K":10,"L":1,
                     "M":2,"N":1,"O":1,"P":3,"Q":8,"R":1,"S":1,"T":1,"U":1,"V":4,"W":10,"X":10,"Y":10,"Z":10}
        
    pass
```

### Exercice 2 bis : les différents parcours

Dans un script, définissez le dictionnaire `PLACARD` comme constante globale  `PLACARD = { 'chemise' : 6, 'pantalon' : 4, 'chaussette' : 10, 'pull' : 4, 'chaussures' : 3}`
  * Ecrire une fonction `affich1(dico)`  permettant d'afficher toutes les clés du dictionnaire `dico` ;
  * Ecrire une fonction `affich2(dico)`  permettant d'afficher toutes les valeurs du dictionnaire `dico` (et uniquement les valeurs) ;
  * Ecrire une fonction `affich3(dico)` permettant d'afficher tous les couples clés, valeurs du dictionnaire sous la forme :
  ```Python
  Il y a 12 chemise
  Il y a 4 pantalon
  Il y a 10 chaussette
  ...
  ```
 
 _Complément à l'exercice_ : On pourra chercher à ajouter un 's' lorsqu'il y a plus d'une quantité.
   


### Exercice 3 : Anagrammes [^1]

_Les fonctions comporteront des docstrings complètes_.

Tester si un mot est un anagramme d'un autre est un algorithme classique. Il se résoud particulièrement bien avec les dictionnaires.

La première idée est de parcourir le premier mot, et de compter le nombre d'occurrences de chaque lettre présente dans le mot. Puis on fait cette même opération sur le deuxième mot et on regarde si les deux dictionnaires sont égaux. 

(@) Écrivez la fonction `alphabet(mot)` qui prend un mot en paramètre et retourne un dictionnaire `dict(str:int)` dont les clés sont les lettres du mot et les valeurs le nombre d'occurences. On devrait obtenir le résultat suivant :

```Python
def alphabet(mot):
    '''
    >>> alphabet("abracadabra")
    {'a': 5, 'b': 2, 'r': 2, 'c': 1, 'd': 1}
    >>> alphabet("bonjour")
    {'b': 1, 'o': 2, 'n': 1, 'j': 1, 'u': 1, 'r': 1}
    >>> alphabet("")
    {}
    >>> alphabet("aaa")
    {'a': 3}
    '''
    
    pass
```

(@) Écrivez un **prédicat** `anagramme(mot_1, mot_2)` qui prend deux mots en paramètres et teste s'il sont un anagramme l'un de l'autre. Cette fonction doit donc renvoyer `True` si `mot_1` et `mot_2` sont des anagrammes et `False` sinon.

L'égalité entre deux dictionnaires s'écrit facilement mais néanmoins elle reste coûteuse en opérations.

> **La méthode à utiliser ici** est donc de parcourir le deuxième mot, de d'enlever une occurrence de chaque lettre rencontrée dans l'alphabet du premier. On pourra alors tester s'ils sont ou pas deux anagrammes.

_Exemples_ : 

```Python
def anagramme(mot_1, mot_2):
    '''
    >>> anagramme('parisien', 'aspirine')
    True
    >>> anagramme('sommeil', 'immoles')          
    True
    >>> anagramme('bonjour', 'bateau')              
    False
    '''
    
    pass
```

### Exercice 4 [^2]

Ecrire une fonction `echange(dico)` qui prend un dictionnaire en paramètre et renvoie un nouveau dictionnaire où les clés et les valeurs auront été interverties.


_Exemple_ :

```Python
>>> mon_dico = { 'maison' : 'house', 'canard' : 'duck', 'voiture' : 'car'}
>>> dico2 = echange(mon_dico)
>>> dico2
{'house': 'maison', 'duck': 'canard', 'car': 'voiture'}
```

### Exercice 5 : Des élèves et des notes [^1]

On veut mémoriser les notes qu'un élève a obtenu dans chaque matière. On utilise pour cela un dictionnaire dont chaque clé sera le nom d'une matière, et la valeur la liste des notes obtenues dans cette matière.

```
# Un releve de notes est représenté par un dictionnaire :  dict(string : liste(nombre))
# la clé est le nom d'une matière
# la valeur est la liste des notes obtenues dans cette matière
```

On prendra comme exemples :

```
>>> r1 = {'maths': [12,15.5], 'anglais': [13,8,9.5], 'physique': [13,8,12,10]}
>>> r2 = {'philo': [12,15.5], 'anglais': [11,7,12.5], 'svt': [13,8,12,10,16,15.5]}
```


1. Proposez la fonction `ajoute_note(eleve, matiere, note)` qui ajoute une note dans une matière à un relevé de notes.

    > **Attention** : si la matière n'existe pas, il faut l'ajouter !

    **Cette fonction comporte un effet de bord** puisqu'elle modifie le dictionnaire `eleve`.

```
>>> ajoute_note(r1,'musique',18)
>>> ajoute-note(r1,'maths',9)
>>> r1
{'maths': [12, 15.5, 9], 'anglais': [13, 8, 9.5], 'physique': [13, 8, 12, 10], 'musique': [18]}
```


2. Proposez la fonction `moyenne_dans_une_matiere(eleve, matiere)` qui calcule la moyenne d'un étudiant dans une matière (on considère que toutes les notes ont le même coefficient et que matiere est une des clé du dictionnaire eleve).

    > On commencera par créer une fonction `moyenne(liste)` qui renvoie la moyenne des éléments de `liste`.

    _Exemple_ :

```Python
>>> moyenne_dans_une_matiere(r1, 'physique')
10.75
```


3. Proposez la fonction `moyenne_generale(eleve)` qui calcule la moyenne générale d'un élève (i.e. la moyenne des moyennes dans chaque matière, chaque matière ayant le même coefficient).

    On cherchera ici à ré-utilise la fonction `moyenne()` déjà définie.

    _Exemples_ :

```Python
>>> r1 = {'maths': [12,15.5], 'anglais': [13,8,9.5], 'physique': [13,8,12,10]}
>>> moyenne_generale(r1)
11.555555555555555
```

4. __premiere version__ : Proposez la fonction `meilleure_note(eleve)` qui retourne la meilleure note obtenue par un étudiant.

    _Remarque_ : on pourra ici utiliser la fonction native `max()` qui renvoie le maximum des éléments d'une liste.

    _Exemple_ :

    ```Python
    >>> max([15, 3, 18, 7])
    18
    ```
    
    **Plus dur** :  Proposez la fonction `meilleure_note(eleve)` qui retourne la meilleure note obtenue par un étudiant, toutes matières confondues, et la liste du ou des noms de matière dans laquelle ou lesquelles il a obtenu cette note.


```Python
>>> r1 = {'maths': [12,15.5], 'anglais': [13,8,9.5], 'physique': [13,8,12,10]}
>>> meilleure_note(r1)
(15.5, ['maths'])
>>> r2 = {'philo': [12,18], 'anglais': [11,7,12.5], 'svt': [13,8,12,10,18,15.5]}
>>> meilleure_note(r2)
(18, ['philo', 'svt'])
```

------
__Pour aller plus loin__ :

On considère maintenant une promotion d'étudiants : 


```
# L'identité d'un étudiant est donné par un couple (nom, prénom) : (str,str)

# Une promotion est un dictionnaire dont les clés sont les noms et
# prénoms des étudiants, et les valeurs les notes de ces étudiants.
# dict((str,str) : dict(str:list(nombre)))
```
Par exemple :

```
>>> promo1 = {("Kaczmarek","Anissa"): r1, ('Dupont','Kévin'): r2}
```



5. Proposez la fonction `ajoute_etudiant(promotion, nom, prenom, releve)` d'ajout d'un étudiant à une promotion. Les paramètres sont la promotion, le nom, le prénom de l'étudiant et un relevé de notes (dictionnaire défini comme précédemment).

    **Si l'étudiant existe déjà, on ne l'ajoute pas.**

    _Exemple_ :

```Python
>>> r1 = {'maths': [12,15.5], 'anglais': [13,8,9.5], 'physique': [13,8,12,10]}
>>> r2 = {'philo': [12,18], 'anglais': [11,7,12.5], 'svt': [13,8,12,10,18,15.5]}
>>> r3 = {'maths': [2, 4]}
>>> promo1 = {("Kaczmarek","Anissa"): r1, ('Dupont','Kévin'): r2}
>>> ajoute_etudiant(promo1, 'Durand', 'Pierre', r3)
>>> promo1
{('Kaczmarek', 'Anissa'): {'maths': [12, 15.5], 'anglais': [13, 8, 9.5], 'physique': [13, 8, 12, 10]}, ('Dupont', 'Kévin'): {'philo': [12, 18], 'anglais': [11, 7, 12.5], 'svt': [13, 8, 12, 10, 18, 15.5]}, ('Durand', 'Pierre'): {'maths': [2, 4]}}
>>> ajoute_etudiant(promo1, 'Durand', 'Pierre', r3)
{('Kaczmarek', 'Anissa'): {'maths': [12, 15.5], 'anglais': [13, 8, 9.5], 'physique': [13, 8, 12, 10]}, ('Dupont', 'Kévin'): {'philo': [12, 18], 'anglais': [11, 7, 12.5], 'svt': [13, 8, 12, 10, 18, 15.5]}, ('Durand', 'Pierre'): {'maths': [2, 4]}}
```

6. Proposez la fonction `supprimer_etudiant(promotion, nom, prenom)` qui supprime un étudiant d'une promotion si il est présent dans la promotion.

    _Exemples_ :

```Python
>>> promo1 = {('Kaczmarek', 'Anissa'): {'maths': [12, 15.5], 'anglais': [13, 8, 9.5], 'physique': [13, 8, 12, 10]}, ('Dupont', 'Kévin'): {'philo': [12, 18], 'anglais': [11, 7, 12.5], 'svt': [13, 8, 12, 10, 18, 15.5]}, ('Durand', 'Pierre'): {'maths': [2, 4]}}
>>> supprimer_etudiant(promo1, 'Durand', 'Pierre')
>>> promo1
>>> {('Kaczmarek', 'Anissa'): {'maths': [12, 15.5], 'anglais': [13, 8, 9.5], 'physique': [13, 8, 12, 10]}, ('Dupont', 'Kévin'): {'philo': [12, 18], 'anglais': [11, 7, 12.5], 'svt': [13, 8, 12, 10, 18, 15.5]}}
>>> supprimer_etudiant(promo1, 'Durand', 'Pierre')
>>> {('Kaczmarek', 'Anissa'): {'maths': [12, 15.5], 'anglais': [13, 8, 9.5], 'physique': [13, 8, 12, 10]}, ('Dupont', 'Kévin'): {'philo': [12, 18], 'anglais': [11, 7, 12.5], 'svt': [13, 8, 12, 10, 18, 15.5]}}
```

7. Proposez la fonction `moyenne_promo_matiere(promotion, matiere)` de calcul de la moyenne d'une promotion dans une matière (moyenne des moyennes des élèves) si celle-ci existe. Retourne -1 sinon.
 
    _Exemples_ :

```Python
>>> promo1 = {('Kaczmarek', 'Anissa'): {'maths': [12, 15.5], 'anglais': [13, 8, 9.5], 'physique': [13, 8, 12, 10]}, ('Dupont', 'Kévin'): {'philo': [12, 18], 'anglais': [11, 7, 12.5], 'svt': [13, 8, 12, 10, 18, 15.5]}, ('Durand', 'Pierre'): {'maths': [2, 4]}}
>>> moyenne_promo_matiere(promo1, 'maths')
>>> 8.375
>>> moyenne_promo_matiere(promo1, 'anglais')
10.166666666666666
>>> moyenne_promo_matiere(promo1, 'musique')
-1
```

### Exercice 6 : mini-projet : Jeu par tour [^1]
 
L'objectif ici est de manipuler des dictionnaires qui ont une structure précise : les clés sont définies à l'avance, ainsi que le type des valeurs associées. 

Dans un jeu de stratégie au tour par tour, les joueurs dirigent des
équipes de guerriers qui vont se combattre. Un guerrier est connu par
son nom et est caractérisé par~:

* `ptsVie` : ses points de vie, qu'il perd au fur et à mesure des combats; 
* `force` : sa force, qui détermine la puissance des coups qu'il porte à ses adversaires; 
* `adresse` : son adresse, qui détermine son habileté à atteindre son adversaire, (valeur entre 0 et 100); 
* `nbCoups` : le nombre de coups qu'il peut porter lors d'une attaque; 
* `armure` : son armure, qui le protège et atténue les coups qu'il reçoit, (valeur entre 0 et 100). 


Nous modéliserons un guerrier en python par un dictionnaire dont les clés prendront les valeurs `ptsVie`, `force`,
`adresse`, `nbCoups`, `armure`. 

Exemples :

```Python
>>> helmut={'ptsVie':100,'force':6,'adresse':75,'nbCoups':3,'armure':30}
>>> olga={'ptsVie':98,'force':8,'adresse':90,'nbCoups':2,'armure':35}
>>> irina={'ptsVie':100,'force':2,'adresse':55,'nbCoups':2,'armure':20}
>>> boris={'ptsVie':100,'force':3,'adresse':30,'nbCoups':3,'armure':15}
```

#### Fonction `est_vivant(guerrier)`
---
Proposez la fonction `est_vivant(guerrier)` qui
teste si un guerrier donné en paramètre est vivant ou non.

```Python
>>> helmut={'ptsVie':100,'force':6,'adresse':75,'nbCoups':3,'armure':30}
>>> est_vivant(helmut)
True
>>> captain = {'ptsVie':0,'force':6,'adresse':75,'nbCoups':3,'armure':30}
>>> est_vivant(captain)
False
```

---
#### Fonction `donne_un_coup(guerrier)`
---
Lorsqu'un guerrier donne un coup à un adversaire, la valeur
du coup est celui de sa force. Néanmoins, il faut d'abord déterminer
si l'adversaire sera atteint. Cette possibilité est tirée au hasard,
avec une probabilité de réussite indiquée par l'adresse qui est un
pourcentage de chance de réussir. Un guerrier mort, ou un guerrier qui
n'atteint pas son adversaire, porte un coup d'une force nulle.

(@) Proposez la fonction `donne_un_coup(guerrier)` qui retourne la
valeur du coup porté par un guerrier donné.


```Python
>>> donne_un_coup(olga)
0                     # olga a raté sa cible
>>> donne_un_coup(olga)
8                     # olga a atteint sa cible  
```

---
#### Fonction `prend_un_coup(guerrier, force_coup)`
---
Lorsqu'un guerrier reçoit un coup, son armure le protège. La valeur de son armure est appliquée comme un
pourcentage de coefficient de protection sur la force du coup reçu. Ainsi, un guerrier qu reçoit un 
coup de 5 points, mais qui a une armure avec un coefficient de
protection de 20%, voit diminuer ses points de vie de 4 points seulement.  

On prendra la partie entière du résulat si besoin (utiliser la fonction `floor()` du module `math` (à importer donc).

(@) Proposez la fonction `prend_un_coup(guerrier, force_coup)` qui, pour un
guerrier et une force de coup donnés, diminue les points de vie du
guerrier (donc modifie en place le dictionnaire guerrier). Un guerrier ne peut jamais avoir des points de vie négatifs.

_Exemple_ :

```Python
>>> helmut={'ptsVie':100,'force':6,'adresse':75,'nbCoups':3,'armure':30}
>>> prend_un_coup(helmut,8)
>>> helmut
{'ptsVie': 94, 'force': 6, 'adresse': 75, 
              'nbCoups': 3, 'armure': 30}
          # 30% de 8 est arrondi à 2, 8-2 : 6 points sont enlevés
```
---
#### Fonction `attaque(guerrier_1, guerrier_2)`
---
Lors d'une attaque d'un guerrier sur un autre, le premier
guerrier porte ses `nbCoups` au deuxième, puis, si le deuxième est
encore vivant, c'est à son tour de donner des coups au premier.

(@) Proposez la fonction `attaque(guerrier_1, guerrier_2)` qui gère l'attaque
de deux guerriers donnés.

----
#### Des équipes de guerriers
---
On constitue maintenant des équipes de guerriers. Une équipe de guerriers est représentée par un dictionnaire dont les clés sont les noms des guerriers et les valeurs des guerriers comme définis ci-dessus.

Exemples :

```Python
>>> winners = {'Helmut':helmut, 'Olga':olga}
>>> loosers = {'Irina':irina, 'Boris':boris}
```

(@) Proposez une fonction `moyenne_caracteristique(equipe, caracteristique)` qui
calcule la moyenne des valeurs des guerriers d'une équipe sur une caractéristique.


```Python
>>> moyenne_caracteristique(winners,'ptsVie')
99.0
>>> moyenne_caracteristique(winners,'force')
7.0
```


(@) Proposez la fonction `les_plus(equipe, caracteristique)` qui
retourne la liste des noms des guerriers qui ont la plus grande valeur
pour une caractéristique et une équipe données.

```Python
>>> les_plus(loosers,'ptsVie')
['Irina', 'Boris']
>>> les_plus(winners,'armure')
['Olga']
```



#### Références des sites utilisés

* [1] : Auteur : Anne Parain : université de Lens, modifié par G.Guillon
* [2] : idée de M. Willm modifiée par G.Guillon

