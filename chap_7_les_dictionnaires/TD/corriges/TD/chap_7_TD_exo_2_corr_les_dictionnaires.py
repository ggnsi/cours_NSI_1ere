#principe de l'algorithme :
# on parcourt le mot
# * si la lettre est déjà une clé du dictionnaire, on ajoute 1 à la valeur correspondante ;
# * sinon, on l'ajoute comme nouvelle clé avec une valeur de 1

def alphabet(mot):
    """Retourne un dictionnaire avec pour clés les lettres apparaissant dans le mot
    et pour valeur le nombre d'occurences de ces lettres dans le mot
    
    param mot : (string)
    valeur retournée : (dict of strings:int)
    
    Exemple :
    >>> alphabet("abracadabra")
    {'a': 5, 'b': 2, 'r': 2, 'c': 1, 'd': 1}
    """
    
    dico = {} #création d'un dictionnaire vide
    for lettre in mot:
        if lettre in dico.keys():
            dico[lettre] +=1
        else:
            dico[lettre] = 1
    return dico

# principe de l'algorithme

# on crée le dictionnaire dico_1 du mot 1 avec la fonction alphabet
# on parcourt le mot_2 et :
# * si la lettre n'est pas dans le dico_1 : on retourne False
# * si la lettre est avec une valeur 1 : on supprime la clé correspondante
# * si la lettre est avec une valeur > 1, on enlève 1 à la valeur
# Enfin au final, on teste si le dictionnaire est vide (cad que toutes les lettres ont été utilisées)

def anagramme(mot_1, mot_2):
    """Prédicat qui teste si mot_1 et mot_2 sont des anagrammes

    param mot_1, mot_2 : (strings)
    valeur retournée : (booléen)
    
    Exemples :
    >>> anagramme('parisien', 'aspirine')
    True
    >>> anagramme('sommeil', 'immoles')          
    True
    >>> anagramme('bonjour', 'bateau')              
    False
    """
    
    dico_1 = alphabet(mot_1)
    for lettre in mot_2:
        if lettre not in dico_1.keys():
            return False
        elif dico_1[lettre] == 1:
            del(dico_1[lettre])
        else:
            dico_1[lettre] = dico_1[lettre] - 1
    
    return dico_1 == {}
    
if __name__ == '__main__':
    import doctest
    doctest.testmod()