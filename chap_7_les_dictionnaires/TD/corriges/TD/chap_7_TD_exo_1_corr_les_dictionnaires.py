# ici les commandes du début de l'exercice les unes derrière les autres (comme si on travaillait dans la console)

#placard_1 = {'chemise' : 6, 'pantalon' : 4}
#placart_1
#placard_1['chaussette'] = 10
#placard_1['pull'] = 4
#placard_1
#del(placard_1['pantalon'])

PLACARD = { 'chemise' : 6, 'pantalon' : 4, 'chaussette' : 10, 'pull' : 4, 'chaussures' : 3}

def affich1(dico):
    for cle in dico.keys():
        print(cle)

def affich2(dico):
    for cle in dico.values():
        print(cle)

def affich3(dico):
    for (cle, valeur) in dico.items():
        print('Il y a', valeur, cle)

def affich3_bis(dico): #variante avec le pluriel
    for (cle, valeur) in dico.items():
        pluriel = ''
        if valeur > 1:
            pluriel = 's'
        print('Il y a', valeur, cle + pluriel)