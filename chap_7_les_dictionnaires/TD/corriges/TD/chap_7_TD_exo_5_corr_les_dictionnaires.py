import random, math

def est_vivant(guerrier):
    """Prédicat qui indique si le guerrier est vivant

    param guerrier : dict(string : int)
    valeur retournée : booléen
    
    Exemples :
    >>> helmut={'ptsVie':100,'force':6,'adresse':75,'nbCoups':3,'armure':30}
    >>> est_vivant(helmut)
    True
    >>> captain = {'ptsVie':0,'force':6,'adresse':75,'nbCoups':3,'armure':30}
    >>> est_vivant(captain)
    False
    """
    
    return guerrier['ptsVie'] != 0


def donne_un_coup(guerrier):
    """Retourne la valeur du coup porté par le guerrier

    param guerrier : dict(string : int)
    
    """
    
    if not est_vivant(guerrier):
        return 0
    
    nbr_alea = random.randint(0, 99)
    if nbr_alea < guerrier['adresse']:
        return guerrier['force']
    else:
        return 0

def prend_un_coup(guerrier, force_coup):
    """Modifie guerrier en fonction de la force_coup et de son armure

    param guerrier : dict('string' : int)
    param force_coup : int
    valeur retournée : aucune
    
    Effet de bord : modifie en place le dictionnaire guerrier
    
    Exemples :
    >>> helmut={'ptsVie':100,'force':6,'adresse':75,'nbCoups':3,'armure':30}
    >>> prend_un_coup(helmut,8)
    >>> helmut
    {'ptsVie': 94, 'force': 6, 'adresse': 75, 'nbCoups': 3, 'armure': 30}
    >>> helmut={'ptsVie':3,'force':6,'adresse':75,'nbCoups':3,'armure':30}
    >>> prend_un_coup(helmut,8)
    >>> helmut
    {'ptsVie': 0, 'force': 6, 'adresse': 75, 'nbCoups': 3, 'armure': 30}
    """
    
    valeur_coup = force_coup - math.floor(force_coup * guerrier['armure']/100)
    vie_restante = max(guerrier['ptsVie'] - valeur_coup, 0)
    guerrier['ptsVie'] = vie_restante
    
    
def attaque(guerrier_1, guerrier_2):
    """Gère l'attaque de guerrier_1sur guerrier_2, puis si guerrier_2 est encore vivant, de guerrier_2
    sur guerrier_1
    
    param guerrier_1, guerrier_2 : dict('string' : int)
    valeur retournée : aucune
    
    Effet de bord : guerrier_1 et guerrier_2 sont modifiés en place
    """
    
    nb_coups_1 = guerrier_1['nbCoups']
    for _ in range(nb_coups_1):
        coup_guerrier_1 = donne_un_coup(guerrier_1)
        prend_un_coup(guerrier_2, coup_guerrier_1)
        
    nb_coups_2 = guerrier_2['nbCoups']
    for _ in range(nb_coups_2):    
        coup_guerrier_2 = donne_un_coup(guerrier_2)
        prend_un_coup(guerrier_1, guerrier_2)

def moyenne_caracteristique(equipe, caracteristique):
    """Retourne la valeur moyenne des guerriers de l'équipe selon le caractéristique donnée

    param equipe : dict(string : dict_guerrier) avec dict_guerrier : dict('string' : int)
    param caracteristique : string
    valeur retournée : float
    
    CU : caracteristique in keys of dict_guerrier
    CU : len(equipe) != 0
    
    Exemples:
    >>> helmut={'ptsVie':100,'force':6,'adresse':75,'nbCoups':3,'armure':30}
    >>> olga={'ptsVie':98,'force':8,'adresse':90,'nbCoups':2,'armure':35}
    >>> irina={'ptsVie':100,'force':2,'adresse':55,'nbCoups':2,'armure':20}
    >>> boris={'ptsVie':100,'force':3,'adresse':30,'nbCoups':3,'armure':15}
    >>> winners = {'Helmut':helmut, 'Olga':olga}
    >>> loosers = {'Irina':irina, 'Boris':boris}
    >>> moyenne_caracteristique(winners,'ptsVie')
    99.0
    >>> moyenne_caracteristique(winners,'force')
    7.0
    """
    
    total = 0
    nb_guerriers = len(equipe)
    for guerrier in equipe.values(): # on parcourt ici les valeurs du dictionnaire qui sont les dictionnaires des caractéristiques des guerriers
        total = total + guerrier[caracteristique]
    return total / nb_guerriers
    
def les_plus(equipe, caracteristique):
    """Retourne la liste des guerriers de equipe qui ont la plus grande valeur pour la caractéristique donnée

    param equipe : dict(string : dict_guerrier) avec dict_guerrier : dict('string' : int)
    param caracteristique : string
    valeur retournée : list of string
    
    CU : caracteristique in keys of dict_guerrier
    CU : len(equipe) != 0
    
    Exemples :
    >>> helmut={'ptsVie':100,'force':6,'adresse':75,'nbCoups':3,'armure':30}
    >>> olga={'ptsVie':98,'force':8,'adresse':90,'nbCoups':2,'armure':35}
    >>> irina={'ptsVie':100,'force':2,'adresse':55,'nbCoups':2,'armure':20}
    >>> boris={'ptsVie':100,'force':3,'adresse':30,'nbCoups':3,'armure':15}
    >>> winners = {'Helmut':helmut, 'Olga':olga}
    >>> loosers = {'Irina':irina, 'Boris':boris}
    >>> les_plus(loosers,'ptsVie')
    ['Irina', 'Boris']
    >>> les_plus(winners,'armure')
    ['Olga']
    """
    
    l_sortie = []
    max = 0 # impossible d'avoir moins, on peut initialiser à 0 le maximum
    
    for (nom_guerrier, guerrier) in equipe.items(): # on a besoin ici à la fois du nom et du dico guerrier
        valeur_guerrier = guerrier[caracteristique]
        if valeur_guerrier > max:
            max = valeur_guerrier
            l_sortie = [nom_guerrier]
        elif valeur_guerrier == max:
            l_sortie.append(nom_guerrier)
    return l_sortie

        
        
if __name__ == '__main__':
    import doctest
    doctest.testmod()