
# principe de l'algorithme :
# on crée un dictionnaire vide
# on parcourt le dictionnaire par (clé, valeur)
# on ajoute l'entrée correspondant à (valeur, clé) au dictionnaire de sortie

def echange(dict):
    """Modifie le dictionnaire en échangeant les clés et les valeurs

    param dict : dict
    valeur retournée : aucune
    
    Effet de bord : modifie dict en place
    
    Exemple :
    >>> mon_dico = { 'maison' : 'house', 'canard' : 'duck', 'voiture' : 'car'}
    >>> echange(mon_dico)
    {'house': 'maison', 'duck': 'canard', 'car': 'voiture'}
    """
    new_dico = {}
    for (cle, valeur) in dict.items():
        new_dico[valeur] = cle
    return new_dico


    
    
    

if __name__ == '__main__':
    import doctest
    doctest.testmod()