def alphabet(mot):
    '''
    A faire
    
    Exemples:
    >>> alphabet("abracadabra")
    {'a': 5, 'b': 2, 'r': 2, 'c': 1, 'd': 1}
    >>> alphabet("bonjour")
    {'b': 1, 'o': 2, 'n': 1, 'j': 1, 'u': 1, 'r': 1}
    >>> alphabet("")
    {}
    >>> alphabet("aaa")
    {'a': 3}
    '''
    
    dico = {}
    for lettre in mot:
        if lettre in dico.keys():
            dico[lettre] +=1
        else:
            dico[lettre] = 1
    return dico

def anagramme(mot_1, mot_2):
    '''
    A faire
    
    >>> anagramme('parisien', 'aspirine')
    True
    >>> anagramme('sommeil', 'immoles')          
    True
    >>> anagramme('bonjour', 'bateau')              
    False
    '''
    
    dico_1 = alphabet(mot_1)
    for lettre in mot_2:
        if lettre not in dico_1.keys():
            return False
        else:
            dico_1[lettre] -= 1
    
    # tester si toutes les valeurs sont à 0
    for effectif in dico_1.values():
        if effectif != 0:
            return False
    
    return True

def anagramme2(mot_1, mot_2):
    '''
    A faire
    
    >>> anagramme2('parisien', 'aspirine')
    True
    >>> anagramme2('sommeil', 'immoles')          
    True
    >>> anagramme2('bonjour', 'bateau')              
    False
    '''
    
    dico_1 = alphabet(mot_1)
    for lettre in mot_2:
        if lettre not in dico_1.keys():
            return False
        else:
            dico_1[lettre] -= 1
        if dico_1[lettre] == 0:
            del(dico_1[lettre])
     
    return len(dico_1) == 0

if __name__ == '__main__':
    import doctest
    doctest.testmod()
        
        
        
