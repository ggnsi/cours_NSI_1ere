
# principe de l'algo :
# on ajoute à la liste correspondant à l'entrée matière du dictionnaire eleve la note.

def ajoute_note(eleve, matiere, note):
    """Ajoute la note note dans l'entrée matiere du dictionnaire eleve

    param eleve : (dict string:(list of int))
    param matiere : (string)
    param note : (int)
    
    Effet de bord : modifie le dictionnaire eleve en place
    
    Exemple :
    >>> r1 = {'maths': [12,15.5], 'anglais': [13,8,9.5], 'physique': [13,8,12,10]}
    >>> ajoute_note(r1,'musique',18)
    >>> ajoute_note(r1,'maths',9)
    >>> r1
    {'maths': [12, 15.5, 9], 'anglais': [13, 8, 9.5], 'physique': [13, 8, 12, 10], 'musique': [18]}
    """
    if matiere in eleve:
        l_notes = eleve[matiere]
        l_notes.append(note)
        eleve[matiere] = l_notes # ou bien ces 3 lignes en une seule : eleve[matiere].append(note)
    else:
        eleve[matiere] = [note]

# principe de l'algorithme :
# on récupère la liste des notes
# on calcul la moyenne des notes de cette liste.
# a cet effet, pour la modularité du code, on peut créer une fonction moyenne à part

def moyenne(liste):
    """Retourne la moyenne des éléments d'une liste

    param liste : (list of int or float)
    valeur retournée : float
    
    Exemple :
    >>> moyenne([8, 10, 12])
    10.0
    """
    
    total = 0
    for val in liste:
        total += val
    return total / len(liste)
    
def moyenne_dans_une_matiere(eleve, matiere):
    """Retourne la moyenne des notes de eleve dans matiere

    param eleve : (dict(string:(list of int))
    param matiere : (string)
    
    CU : matiere in eleve.keys()
    
    Exemple :
    >>> r1 = {'maths': [12,15.5], 'anglais': [13,8,9.5], 'physique': [13,8,12,10]}
    >>> moyenne_dans_une_matiere(r1, 'physique')
    10.75
    """
    
    notes = eleve[matiere]
    return moyenne(notes)

# principe de l'algorithme 
# on crée une liste l_moyennes vide (qui contiendra ensuite les moyennes de chaque matière)
# on parcourt les valeurs du dictionnaire eleve, pour chacune on calcule la moyenne qu'on ajoute dans la liste l_moyennes
# on calcule la moyenne de cette liste (avec la fonction moyenne)

def moyenne_generale(eleve):
    """Retourne la moyenne générale des notes de eleve

    param eleve : (dict(string:(list of int))
    valeur retournée : (float)
    
    Exemple :
    >>> r1 = {'maths': [12,15.5], 'anglais': [13,8,9.5], 'physique': [13,8,12,10]}
    >>> moyenne_generale(r1)
    11.555555555555555
    """
    
    l_moyennes = []
    for notes in eleve.values():
        moy_matiere = moyenne(notes)
        l_moyennes.append(moy_matiere)
    
    moy_generale = moyenne(l_moyennes)
    return moy_generale

# principe de l'algorithme :
# on prend une des clé et sa valeur associée
# on initialise une variable max_note avec la première note de cette clé et une variable matiere_max avec [clé]
# on parcourt les couples clés, valeurs du dictionnaire
# a chaque fois, on recherche le maximum de la liste :
# s'il est égal à max note : on ajoute la clé à la liste ;
# s'il est strictement plus grand que max_note, on remplace max_note par [cle]
# et on stoque la clé dans la variable matiere_max

def meilleure_note(eleve):
    """Retourne la meilleure note obtenue par un étudiant, toutes matières
    confondues, et la liste du ou des noms de matière dans laquelle ou
    lesquelles il a obtenu cette note.
    
    param eleve : (dict(string:(list of int))
    valeur retournée : (float, (list of string))
    
    Exemple :
    >>> r1 = {'maths': [12,15.5], 'anglais': [13,8,9.5], 'physique': [13,8,12,10]}
    >>> meilleure_note(r1)
    (15.5, ['maths'])
    >>> r2 = {'philo': [12,18], 'anglais': [11,7,12.5], 'svt': [13,8,12,10,18,15.5]}
    >>> meilleure_note(r2)
    (18, ['philo', 'svt'])
    """
    
    #initialisation
    l_cles = list(eleve.keys())
    matiere = l_cles[0]
    max_matiere = [matiere]
    max_note = eleve[matiere][0]
    #parcours du dictionnaire par (clé, valeur)
    for (matiere, l_notes) in eleve.items():
        note_max_matiere = max(l_notes)
        if note_max_matiere == max_note:
            max_matiere.append(matiere)
        elif note_max_matiere > max_note:
            max_note = note_max_matiere
            max_matiere = [matiere]
    return (max_note, max_matiere)
    
def ajoute_etudiant(promotion, nom, prenom, releve):
    """Ajoute un étudiant à la promotion au cas où il n'existe pas déjà

    param promotion : dict((str,str) : dict(str:list(nombre)))
    param nom, prenom: (string)
    param releve : dict(str:list(nombre))
    
    Effet de bord : modifie le dictionnaire promotion en place
    
    Exemple :
    >>> r1 = {'maths': [12,15.5], 'anglais': [13,8,9.5], 'physique': [13,8,12,10]}
    >>> r2 = {'philo': [12,18], 'anglais': [11,7,12.5], 'svt': [13,8,12,10,18,15.5]}
    >>> r3 = {'maths': [2, 4]}
    >>> promo1 = {("Kaczmarek","Anissa"): r1, ('Dupont','Kévin'): r2}
    >>> ajoute_etudiant(promo1, 'Durand', 'Pierre', r3)
    >>> promo1
    {('Kaczmarek', 'Anissa'): {'maths': [12, 15.5], 'anglais': [13, 8, 9.5], 'physique': [13, 8, 12, 10]}, ('Dupont', 'Kévin'): {'philo': [12, 18], 'anglais': [11, 7, 12.5], 'svt': [13, 8, 12, 10, 18, 15.5]}, ('Durand', 'Pierre'): {'maths': [2, 4]}}
    >>> ajoute_etudiant(promo1, 'Durand', 'Pierre', r3)
    >>> promo1
    {('Kaczmarek', 'Anissa'): {'maths': [12, 15.5], 'anglais': [13, 8, 9.5], 'physique': [13, 8, 12, 10]}, ('Dupont', 'Kévin'): {'philo': [12, 18], 'anglais': [11, 7, 12.5], 'svt': [13, 8, 12, 10, 18, 15.5]}, ('Durand', 'Pierre'): {'maths': [2, 4]}}
    """
    
    if (nom, prenom) not in promotion:
        promotion[(nom, prenom)] = releve


    
def supprimer_etudiant(promotion, nom, prenom):
    """Supprime l'étudiant de clé (nom, prenom) s'il est présent dans promotion

    param promotion : dict((str,str) : dict(str:list(nombre)))
    param nom, prenom: (string)
    
    Effet de bord : modifie le dictionnaire promotion en place
    
    >>> promo1 = {('Kaczmarek', 'Anissa'): {'maths': [12, 15.5], 'anglais': [13, 8, 9.5], 'physique': [13, 8, 12, 10]}, ('Dupont', 'Kévin'): {'philo': [12, 18], 'anglais': [11, 7, 12.5], 'svt': [13, 8, 12, 10, 18, 15.5]}, ('Durand', 'Pierre'): {'maths': [2, 4]}}
    >>> supprimer_etudiant(promo1, 'Durand', 'Pierre')
    >>> promo1
    >>> {('Kaczmarek', 'Anissa'): {'maths': [12, 15.5], 'anglais': [13, 8, 9.5], 'physique': [13, 8, 12, 10]}, ('Dupont', 'Kévin'): {'philo': [12, 18], 'anglais': [11, 7, 12.5], 'svt': [13, 8, 12, 10, 18, 15.5]}}
    >>> supprimer_etudiant(promo1, 'Durand', 'Pierre')
    >>> {('Kaczmarek', 'Anissa'): {'maths': [12, 15.5], 'anglais': [13, 8, 9.5], 'physique': [13, 8, 12, 10]}, ('Dupont', 'Kévin'): {'philo': [12, 18], 'anglais': [11, 7, 12.5], 'svt': [13, 8, 12, 10, 18, 15.5]}}
    """

    if (nom, prenom) in promotion:
        del(promotion[(nom, prenom)])

def moyenne_promo_matiere(promotion, matiere):
    """
    Calcule la moyenne des élèves de promotion dans matiere (
    
    param promotion : dict((str,str) : dict(str:list(nombre)))
    param matiere : string
    valeur retournée : float
    
        """
    
    l_moy_eleves = []
    for releve_eleve in promotion.values():
        if matiere in releve_eleve:
            l_moy_eleves.append(moyenne(releve_eleve[matiere]))
    if len(l_moy_eleves) != 0:
        return moyenne(l_moy_eleves)
    else:
        return -1


        
r1 = {'maths': [12,15.5], 'anglais': [13,8,9.5], 'physique': [13,8,12,10]}
r2 = {'philo': [12,18], 'anglais': [11,7,12.5], 'svt': [13,8,12,10,18,15.5]}
r3 = {'maths': [2, 4]}

promo1 = {("Kaczmarek","Anissa"): r1, ('Dupont','Kévin'): r2}    
    
    
    
    
    
    
    
    
    

if __name__ == '__main__':
    import doctest
    doctest.testmod()