# Les dictionnaires - Questions de la BNS (Banque Nationale de Sujets)

Les ressources écrites ci-dessous sont propriétés du ministère de l'éducation Nationale et de la jeunesse, licence CC BY SA NC.  
Sélection de questions issue du site [pixees](https://pixees.fr)

__Question 1__

On définit un dictionnaire :

```Python
d = {'couleur': 'vert', 'taille': 42, 'marque': 'le coq sportif'}
```
Quelle est la valeur de l'expression d.keys() ?

Réponses : A- `['couleur', 'taille', 'marque']`

__Question 2__

Comment peut-on accéder à la valeur associée à une clé dans un dictionnaire ?

Réponses : B- on peut y accéder directement à partir de la clé

__Question 3__

On définit le dictionnaire `d = {'a': 1, 'b': 2, 'c': 3, 'z': 26}`. Quelle expression permet de récupérer la valeur de la clé 'z' ?

Réponses : D- `d['z']`

__Question 4__

Quel est le type de la variable billes définie par : `billes = {'vert': 6, 'rouge': 15, 'bleu': 11, 'jaune': 2, 'orange': 17 }`

Réponses : D- c'est un dictionnaire

__Question 5__

Considérons le dictionnaire suivant : `resultats = {'Paul':5 , 'Amina':1 , 'Léon' : 9 , 'Benoit':3}`

Quelle affirmation est correcte ?

Réponses : A- `resultats['Amina']` vaut 1

__Question 6__

Après avoir défini : `d = { 'tigre': 'félin', 'tortue': 'reptile', 'renard': 'canidé' }`

laquelle des quatre expressions suivantes est correcte ?

Réponses : A- `d['tortue']`

__Question 7__

On exécute le script suivant :

```Python
inventaire = {'pommes': 430, 'bananes': 312,'oranges' : 274, 'poires' : 137}

stock = 0
for fruit in inventaire.keys():
  if fruit != 'bananes':
    stock = stock + inventaire[fruit]
```


Que contient la variable stock à la fin de cette exécution ?

Réponses : C- 841

__Question 8__

On définit ainsi une liste P :


`P = [{"nom":"Turing","prénom":"Alan","âge":28},{"nom":"Lovelace","prénom":"Ada","âge":27}]`
		
Que fait alors l'instruction `P[1]["âge"] = 25` ?

Réponses : B- elle modifie la valeur de la clé âge du premier élément de la liste P

__Question 9__

La variable sequence contient une liste de lettres, éventuellement répétées, choisies parmi `'A', 'B', 'C', 'D'`. On veut créer un dictionnaire effectifs associant à chaque lettre le nombre de fois qu'elle apparaît dans la liste sequence.

Par exemple si sequence contient `['A', 'B', 'B', 'D', 'B', 'A']`, effectifs doit contenir `{'A':2, 'B':3, 'C':0, 'D':1}`.

Parmi les scripts suivants, lequel réalise cet objectif ?

Réponses :

A-

```Python
effectifs = {'A':0, 'B':0, 'C':0, 'D':0}
for lettre in sequence:
  effectifs[lettre] = effectifs[lettre] + 1
```
		
__Question 10__

On a défini `dico = { 'a': (1,2,3), 'b': (4,5,6) }`
		
Quelle est la valeur de l'expression `dico['a'][1]`?

Réponses : B- 2


__Question 11__

Quelle est la valeur affichée à l'exécution du programme Python suivant ?

```Python
ports = { 'http': 80, 'imap': 142, 'smtp': 25 }
ports['ftp'] = 21
print(ports['ftp'])
```

Réponses : B- 21

__Question 12__

On dispose du dictionnaire regions ci-dessous :


`regions = {'Mayotte': 376, 'Pays de la Loire': 32082,'La Réunion': 2504, 'Grand Est': 57441,'Martinique': 1128, 'Corse': 8680,'Bretagne': 27208, 'Nouvelle-Aquitaine': 84036}`
		

Parmi les instructions suivantes, laquelle permet d'ajouter une nouvelle région ?

Réponses : D- `regions['Hauts de France'] = 31806`

__Question 13__

On définit ainsi une liste P :

`P = [ {"nom":"Turing","prénom":"Alan","âge":28},{"nom":"Lovelace","prénom":"Ada","âge":27} ]`
		

Comment accéder à la chaîne de caractères "Alan" ?

Réponses : C- `P[0]["prénom"]`

__Question 14__

On exécute le script suivant :

```Python
def ajoute(stock,element,quantite):
  if element in stock:
    stock[element] = stock[element] + quantite
  else:
    stock[element] = quantite

stock = { 'clous': 14, 'vis': 27, 'boulons': 8, 'écrous': 24 }
ajoute(stock,'vis',5)
ajoute(stock,'chevilles',3)
```

Quelle est la valeur de la variable stock à la fin de cette exécution ?

Réponses :  D- `{‘clous’: 14, ‘vis’: 32, ‘boulons’: 8, ‘écrous’: 24, 'chevilles': 3}`

__Question 15__

On considère le code suivant : `D = { 'a': '1', '2': 'a', 'b': 'a', 'c': '3'}`
		

Que vaut `D['a']` à la fin de son exécution ?

Réponses : A- `'1'`

__Question 16__

On a défini un dictionnaire : `contacts = {'Paul': '0601010182', 'Jacques': '0602413824', 'Claire': '0632451153'}`
		

Quelle instruction écrire pour ajouter à ce dictionnaire un nouveau contact nommé Juliette avec le numéro de téléphone `0603040506` ?

Réponses : C- `contacts['Juliette'] = '0603040506'`

__Question 17__

On considère le script suivant :

```Python
billes = {'vert': 6, 'rouge': 15, 'bleu': 11, 'jaune': 2, 'orange': 17 }
total = 0
for n in billes.XXXXXXX():
  total = total + n
```

Par quoi faut-il remplacer `XXXXXXX` dans ce script pour qu'à la fin de son exécution la variable total contienne le nombre total de billes ?

Réponses : B- values

__Question 18__

On définit : `dico = {"Herve": 15, "Kevin":17, "Fatima":16}` qui associe nom et âge de trois élèves.

Comment accéder à l'âge de Kevin ?

Réponses : C- `dico["Kevin"]`

__Question 19__

On exécute le code suivant :

```Python
placard = { 'chemise': 3, 'pantalon': 6, 'tee shirt': 7 }
placard['chaussette'] = 4
placard['chemise'] = 5

L = list(placard.values())
```

Quelle est la valeur de la variable L à l'issue de cette exécution ?

Réponses : D- `[ 5, 6, 7, 4 ]`

__Question 20__

Pour gérer certaines données EXIF de photographies, on a utilisé le code suivant pour stocker dans une liste L de dictionnaires quelques données :

```Python
L = []
L.append({'marque': 'Canon', 'modele': 'EOS 7D', 'focale': '19mm', 'flash': False})
L.append({'marque': 'Nikon', 'modele': 'CoolPix A1000', 'focale': '19mm', 'flash': True})
L.append({'marque': 'Sony', 'modele': 'HK 350', 'focale': '24mm', 'flash': False})
L.append({'marque': 'Sony', 'modele': 'HK 350', 'focale': '19mm', 'flash': True})
# ……
# et ainsi de suite, d'autres informations ont été ajoutées
# ……
```

On veut extraire de ces informations la liste Z des photographies obtenues avec un Canon ou un Nikon et une distance focale de 19 mm.

Quelle instruction permet de réaliser cette extraction ?

Réponses :

D-
```Python
Z = [ p for p in L if (p['marque'] == 'Canon' and p['focale'] == '19mm') or (p['marque'] == 'Nikon' and p['focale'] == '19mm') ]
```

		
__Question 21__

On définit : `contacts = {'Toto': 'toto@nsi.fr', 'Chloé': 'chloe@nsi.com','Paul': 'paul@nsi.net', 'Clémence': 'clemence@nsi.org' }`
		

Parmi les propositions suivantes, laquelle est exacte ?

Réponses : B- 'Chloé' est une clé de la variable contacts

__Question 22__

On considère la table suivants :

`t = [{'type': 'marteau', 'prix': 17, 'quantité': 32},{'type': 'scie', 'prix': 24, 'quantité': 3},{'type': 'tournevis', 'prix': 8, 'quantité': 45}]`
		

Quelle expression permet d'obtenir la quantié de scies ?

Réponses : B- `t[1]['quantité']`

__Question 23__

On définit ainsi une liste t :

```Python
t = [ {'id':1, 'age':23, 'sejour':'PEKIN'},{'id':2, 'age':27, 'sejour':'ISTANBUL'},{'id':3, 'age':53, 'sejour':'LONDRES'},{'id':4, 'age':41, 'sejour':'ISTANBUL'},{'id':5, 'age':62, 'sejour':'RIO'},{'id':6, 'age':28, 'sejour':'ALGER'}]
```


Quelle affirmation est correcte ?

Réponses : B- t est une liste de dictionnaires

__Question 24__

On exécute le code suivant :

```Python
dict = {"alexandre" : 17, "mehdi" : 18,  "jeanne" : 16,"charlotte" : 19, "celina" : 18, "noé" : 19}

def f(dic):
  for cle, valeur in dic.items() :
    if valeur > 18:
      return cle
```
	
Que renvoie l'appel `f(dict)` ?

Réponses : C- "charlotte"

__Question 25__

On définit la variable suivante : `lettres = {"a": 1, "b": 2, "c": 3}`.

Quelle est la valeur de l'expression list(lettres.keys()) ?

Réponses : C- `["a","b","c"]`

__Question 26__

On exécute le script suivant :

```Python
notes = {"Paul": 12, "Jean": 16, "Clara": 14, "Aïssa": 18}
t = list(notes.keys())
```

Quelle est la valeur de t à la fin de cette exécution ?

Réponses : B- `["Paul", '"Jean", "Clara", "'Aïssa']`

__Question 27__

Par quelle expression remplacer les pointillés dans le programme Python suivant, pour que son exécution affiche le numéro de Dupond ?

```Python
repertoire = [{'nom':'Dupont', 'tel':'5234'},{'nom':'Tournesol', 'tel':'5248'}, {'nom':'Dupond', 'tel':'3452'}]
for i in range(len(repertoire)):
  if ......   :
    print(repertoire[i]['tel'])
```
	

Réponses : D- `repertoire[i]['nom'] == 'Dupond'`

__Question 28__

On définit :
```Python
T = [{'fruit': 'banane', 'nombre': 25}, {'fruit': 'orange', 'nombre': 124}, {'fruit': 'pomme', 'nombre': 75}, {'fruit': 'kiwi', 'nombre': 51}]
```


Quelle expression a-t-elle pour valeur le nombre de pommes ?

Réponses : A- `T[2]['nombre']`

__Question 29__

Quelle expression Python permet d’accéder au numéro de téléphone de Tournesol, sachant que le répertoire a été défini par l’affectation suivante :

```Python
repertoire = [{'nom':'Dupont', 'tel':'5234'}, {'nom':'Tournesol', 'tel':'5248'}, {'nom':'Dupond', 'tel':'3452'}]
```
		

Réponses : C- `repertoire[1]['tel']`

__Question 30__

On définit ainsi une liste t puis une liste r :

```Python
t = [ {'id':1, 'age':23, 'sejour':'PEKIN'},{'id':2, 'age':27, 'sejour':'ISTANBUL'},{'id':3, 'age':53, 'sejour':'LONDRES'},{'id':4, 'age':41, 'sejour':'ISTANBUL'},{'id':5, 'age':62, 'sejour':'RIO'},{'id':6, 'age':28, 'sejour':'ALGER'}]

r = [ c for c in t if c['age']>30 and c['sejour']=='ISTANBUL' ]
```

Combien la liste r contient-elle d'éléments ?

Réponses : B- 1 

__Question 31__

On définit ainsi une liste t :

```Python
t = [ {'id':1, 'age':23, 'sejour':'PEKIN'},{'id':2, 'age':27, 'sejour':'ISTANBUL'},{'id':3, 'age':53, 'sejour':'LONDRES'},{'id':4, 'age':41, 'sejour':'ISTANBUL'},{'id':5, 'age':62, 'sejour':'RIO'},{'id':6, 'age':28, 'sejour':'ALGER'}]
```
	

Quelle expression vaut-elle 'RIO' parmi les suivantes ?

Réponses : A- `t[4]['sejour']`

__Question 32__

On considère des dictionnaires comme `{ 'nom': 'Jérôme', 'NSI': 16.2, 'maths': 11.4, 'physique': 13.0 }`
		

pour retenir les notes d'un élève.

On définit :

```Python
def somme(notes):
  return notes['NSI'] + notes['maths'] + notes['physique']

def plusPetit(n1, n2):
  if n1['NSI'] < n2['NSI']:
    return True
  if n1['NSI'] == n2['NSI']:
    if somme(n1) < somme(n2):
      return True
    elif somme(n1) == somme(n2) and n1['nom'] < n2['nom']:
      return True
  return False
```

pour définir un ordre croissant sur ces dictionnaires.

Ranger dans l'ordre croissant les dictionnaires suivants :

```Python
n1 = { 'nom': "Albert", 'NSI': 12.3, 'maths': 14.0, 'physique': 8.7 }
n2 = { 'nom': "Béatrice", 'NSI': 12.3, 'maths': 11.0, 'physique': 12.5 }
n3 = { 'nom': "Colin", 'NSI': 12.3, 'maths': 7.0, 'physique': 15.7 }
n4 = { 'nom': "Daniel", 'NSI': 13.4, 'maths': 9.0, 'physique': 5.2 }
n5 = { 'nom': "Emilie", 'NSI': 16.1, 'maths': 5.3, 'physique': 14.4 }
```

Réponses : C- n1, n3, n2, n4, n5

__Question 33__

Par quoi faut-il remplacer les pointillés dans le script suivant :

```Python
relevé = [{'matière':'EPS','moyenne':11}, {'matière':'Sciences','moyenne':6}, {'matière':'LV1','moyenne':14}, {'matière':'Histoire','moyenne':9}, {'matière':'LV2','moyenne':15}]
a = ......
b = ......
for i in relevé :
  if i[a] > 10:
    print(i[b])
```

pour qu'il affiche

```Python
EPS
LV1
LV2
```


Réponses :

A-

```Python
a = 'moyenne'
b = 'matière'
```

		
__Question 34__

Q34 - 
```Python
stock = [{'nom': 'flageolets', 'quantité': 50, 'prix': 5.68},{'nom': 'caviar', 'quantité': 0, 'prix': 99.99},.........,.........,{'nom': 'biscuits', 'quantité': 100, 'prix': 7.71}]
```
		
Quelle expression permet d'obtenir la liste des noms des produits effectivement présents dans le stock (c'est-à-dire ceux dont la quantité n'est pas nulle) ?

Réponses : D- `[p['nom'] for p in stock if p['quantité'] != 0]`

__Question 35__

On considère le code suivant :

```Python
def clearfield(f):
  for i in range(len(f)):
    fiche[i]['code'] = None
  return f

fiche = [{"nom": "pierre", "note": 5.99, "code": 125},{"nom": "pol", "note": 2.99, "code": 82},{"nom": "jack", "note": 7.99, "code": 135}]
```

Que renvoie `clearfield(fiche)` ?

Réponses :

D-
```Python
[{"nom": "pierre", "note": 5.99, "code": None},{"nom": "pol", "note": 2.99, "code": None},{"nom": "jack", "note": 7.99, "code": None}]
```

		
__Question 36__

On a défini
```Python
repertoire = [{'nom': 'Francette', 'poste': 412},{'nom': 'Jeanne', 'poste': 222},{'nom': 'Éric', 'poste': 231}]
```		

Quelle expression permet d'accéder au poste d'Éric ?

Réponses : A- `repertoire[2]['poste']`
