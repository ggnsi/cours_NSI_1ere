# Les dictionnaires

* [Introduction et premiers exemples](./7_les_dictionnaires_intro_exemple_cours_1.md)
* [Cours : généralités et parcours de dictionnaires](./7_les_dictionnaires_cours_parcours_2.md)
* [le TD](./TD/7_TD_les_dictionnaires.md) et [des corrigés](./TD/corriges/TD)
* [Des questions de la Banque Nationale de Sujets](./TD/7_TD_questions_BNS_les_dictionnaires.md) et [un corrigé](./TD/corriges/BNS/7_TD_questions_BNS_corr_les_dictionnaires.md)
* [un mini projet sur l'ADN](./mini_projet_adn)
* [un mini projet sur les données EXIF des photos](./mini_projet_adrien_exif)
