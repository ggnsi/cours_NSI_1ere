# TP sur les séquences nucléiques

Dans cet exercice, les fonctions programmées vont permettre de reproduire
le dogme central de la biologie moléculaire :

![Dogme central de la biologie](https://upload.wikimedia.org/wikipedia/commons/6/68/Central_Dogma_of_Molecular_Biochemistry_with_Enzymes.jpg "Dhorspool at en.wikipedia [CC BY-SA 3.0 (https://creativecommons.org/licenses/by-sa/3.0)]")

 Les molécules représentées dans ce schéma sont :
* l'**ADN** : support stable et transmissible de l'information génétique.
Il est composé des 4 nucléotides suivants (appelés aussi bases) :
A = adénine, T = thymine, G = guanine et C = cytosine.
Dans les cellules vivantes, l'ADN est sous la forme double brin,
c'est-à-dire que 2 séquences ADN se font face. Une séquence est lue
de gauche à droite et l'autre de droite à gauche. De plus, les bases
complémentaires l'une de l'autre se font face (A et T sont complémentaires,
G et C sont complémentaires). Un brin est donc complémentaire et inversé
par rapport à l'autre.

![Structure d'ADN](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/DNA_structure_and_bases_FR.svg/200px-DNA_structure_and_bases_FR.svg.png "derivative work: Dosto (talk)DNA_structure_and_bases_PL.svg: MesserWoland [CC BY-SA 2.5 (https://creativecommons.org/licenses/by-sa/2.5)]")

* l'**ARN** : support temporaire permettant l'expression de l'information
génétique. Il est composé des 4 nucléotides suivants :
A = adénine, U = uracile, G = guanine et C = cytosine

* les **protéines** : outils de la cellule (enzymes, transporteurs, etc.).
Elles sont composées de 20 acides aminés différents.

Les processus du dogme central de la biologie moléculaire, réalisés par
les cellules sont les suivants :

* la **transcription** : certaines parties spécifiques de l'ADN sont
transcrites en ARN. La transcription consiste en l'assemblage de nucléotides
ARN en suivant le modèle ADN et en prenant les bases complémentaires à savoir :
le A de l'ADN est remplacé par un U dans l'ARN, le T par un A,
le G par un C et le C par un G. Il n'y a pas d'inversion lors de la transcription,
telle qu'elle est modélisée dans ce TP.

* la **traduction** : les ARN messagers sont traduits en
protéines. Le passage d'une séquence ARN composée de 4 nucléotides à une
séquence protéique composée de 20 acides aminés, se fait à l'aide du
[code génétique](https://fr.wikipedia.org/wiki/Code_g%C3%A9n%C3%A9tique#Table_des_codons_d'ARN_messager "Contenu soumis à la licence CC-BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0/deed.fr) Source : Article Code génétique de Wikipédia en français").
Dans ce code, chaque mot de 3 bases, appelé codon,
correspond à un acide aminé. Il est possible de construire 4^3 = 64 codons
différents à l'aide des 4 bases. Ce code est donc dégénéré : plusieurs
codons correspondent au même acide aminé. Les codons sont lus sans
chevauchement, les uns à la suite des autres.

* la réplication : l'ADN de chaque brin d'une double hélice est recopié
de telle sorte que deux nouvelles doubles hélices sont produites,
identiques à l'unique double hélice qui a servi de modèle.

De simples chaînes de caractères permettent de représenter les séquences
biologiques et les fonctions programmées vont reproduire les processus.

**A faire**

>* vous écrirez des docstrings pour chacune des fonctions demandées ;
>* des exemples sont fournis dans l'énoncé. Vous **les utiliserez a minima comme jeu de test en utilisant le module `doctest`** (à part pour `genereADN` qui renvoie une chaîne de caractères aléatoire).

## Les séquences ADN

### Question 1 

Réalisez une fonction nommée `estADN` qui vérifie si la chaîne de caractères
passée en paramètre ne contient aucun autre caractère que les quatre bases
A, C, G et T. Cette fonction renvoie la valeur `True` si tel est le cas,
et la valeur `False` dans le cas contraire. De plus, elle renvoie `True`
si la chaîne est vide.

Exemples :
```python
>>> estADN('ATGCGATC')
True
>>> estADN('ACKT')
False
>>> estADN('ACTK')
False
>>> estADN("")
True
```

### Question 2

Il est possible de générer aléatoirement une séquence ADN. La version
naïve suppose que les 4 bases ont la même probabilité d'apparaître à une
position donnée.

Réalisez une fonction nommée `genereADN` qui renvoie une séquence ADN
générée aléatoirement et dont la taille est passée en paramètre.

Exemple :
```python
>>> genereADN(10)
'ACGCCGACTA'
```

> Rappel : En python, le module `random` fournit des outils pour générer des valeurs aléatoires. Ainsi la fonction `random.randrange(max_exclu)` produit une valeur entière aléatoire `N` telle que  `0 <= N < max_exclu`  la fonction `random.randint(a,b)` fournit une valeur entière aléatoire `N` telle que `a <= N <= b`.

```python
>>> from random import randrange, randint
>>> randrange(5)
2
>>> randint(0,5)
4
```

## La transcription

Pour rappel :
* dans une hélice d'ADN, les bases A et T sont complémentaires, ainsi que les bases G et C.  
* Lors du passage de l'ADN à l'ARN, le A est transformé en U, le T en A, le G en C et le C en G. Il n'y a pas d'inversion.

> Je vous conseille ici de relire l'introduction tout en haut du projet (si, celle que vous n'avez certainement pas vraiment le temps de bien lire !)


### Question 3

Réalisez une fonction nommée `baseComplementaire(base, type_voulu)` qui renvoie la base
complémentaire de la base `base` passée en paramètre, selon le type `type` de séquence dont on veut la base associée et  
qui peut être soit `'ADN'`, soit `'ARN'`.

Ainsi :
* si le type est `'ARN'`, cela signifie que l'on passe de la `base` d'ADN à celle d'`'ARN'` associée ;
* si le type est `'ADN'`, cela signifie que l'on passe de la `base` d'ADN à celle d'`'ADN'` associée.

Les contraintes d'utilisation de cette fonction sont que le premier paramètre est bien un seul caractère qui est l'une des quatre bases de l'ADN (`A`, `T`, `G` ou `C`) et le deuxième est la chaîne soit `'ADN'`, soit `'ARN'`.

Exemples : 
```python
>>> baseComplementaire('G', 'ADN')
'C'
>>> baseComplementaire('A', 'ARN')
'U'
```

### Question 4

Réalisez une fonction nommée `transcrit(seq_adn, i_debut_ i_fin)` qui renvoie l'ARN construit
à partir de la sous-séquence d'ADN comprise entre les deux positions
passées en paramètre, incluses. **La première base de la séquence étant
numérotée 1**. 
Pour mémoire, cet ARN est la séquence complémentaire de la portion
d'ADN transcrite.
La fonction `baseComplementaire` doit être utilisée.

Exemple : 
```python
>>> transcrit('TTCTTCTTCGTACTTTGTGCTGGCCTCCACACGATAATCC', 4, 23)
'AAGAAGCAUGAAACACGACC'
```

## La traduction

Voici un dictionnaire qui représente le code génétique en association
à tous les codons possibles l'acide aminé correspondant, ou le caractère *
lorsqu'il s'agit d'un codon Stop (marquant l'arrêt de la traduction).
Vous pouvez copier-coller ce dictionnaire afin de faciliter l'écriture du code.

```python
code_genetique = {
'UUU' : "F", 'UUC' : 'F',
'UUA' : 'L', 'UUG' : 'L', 'CUU' : 'L', 'CUC' : 'L', 'CUA' : 'L', 'CUG' : 'L',
'AUU' : 'I', 'AUC' : 'I', 'AUA' : 'I',
'AUG' : 'M',
'GUU' : 'V', 'GUC' : 'V', 'GUA' : 'V', 'GUG' : 'V',
'UCU' : 'S', 'UCC' : 'S', 'UCA' : 'S', 'UCG' : 'S', 'AGU' : 'S', 'AGC' : 'S',
'CCU' : 'P', 'CCC' : 'P', 'CCA' : 'P', 'CCG' : 'P',
'ACU' : 'T', 'ACC' : 'T', 'ACA' : 'T', 'ACG' : 'T',
'GCU' : 'A', 'GCC' : 'A', 'GCA' : 'A', 'GCG' : 'A',
'UAU' : 'Y', 'UAC' : 'Y',
'UAA' : '*', 'UAG' : '*', 'UGA' : '*',
'CAU' : 'H', 'CAC' : 'H',
'CAA' : 'Q', 'CAG' : 'Q',
'AAU' : 'N', 'AAC' : 'N',
'AAA' : 'K', 'AAG' : 'K',
'GAU' : 'D', 'GAC' : 'D',
'GAA' : 'E', 'GAG' : 'E',
'UGU' : 'C', 'UGC' : 'C',
'UGG' : 'W',
'CGU' : 'R', 'CGC' : 'R', 'CGA' : 'R', 'CGG' : 'R', 'AGA' : 'R', 'AGG' : 'R',
'GGU' : 'G', 'GGC' : 'G', 'GGA' : 'G', 'GGG' : 'G'}
```

### Question 5

Réalisez une fonction nommée `traduit(seq_arn)` qui construit la séquence protéique
obtenue par la traduction de la séquence ARN passée en paramètre (de type `string`). Cette
traduction se fait à partir du premier nucléotide de la séquence ARN, par blocs de 3 nucléotides, et
utilise le dictionnaire `code_genetique`.

On suppose ici que la longueur de la séquence d'ARN `seq_arn` passée en paramètre est bien un multiple de 3.

**attention** : ne pas oublier que rencontrer uncodon Stop arrète la transcription (et ce même si la séquence d'ARN contient encore des nucléotides)

Dans le deuxième exemple ci-dessous, le codon Stop `UAA` arrète la transcription alors même qu'il y a encore ensuite les nucléotides `CGACGCAGAAGG` ensuite

```python
>>> traduit('AUGCGAAGCCGAAAGAACACCGGCUAA')
'MRSRKNTG*'
>>> traduit('AUGCGAAGCCGAAAGAACACCGGCUAACGACGCAGAAGG')
'MRSRKNTG*'
```

## La réplication

### Question 6

Réalisez une fonction nommée `replique(seq_adn)` qui
construit la séquence ADN complémentaire **et inversée** de celle passée
en paramètre. Pour cela, cette fonction fait appel à la fonction
`baseComplementaire`. Une contrainte d’utilisation de cette fonction
est que son paramètre est bien une séquence ADN.


```python
>>> replique('ACTG')
'CAGT'
```

## Pour aller plus loin

Il est également possible de finir cet exercice par l'écriture d'un menu
interactif qui permet d'utiliser les fonctions programmées. Ce menu
sera programmé dans le main et comportera des vérifications de saisie.
