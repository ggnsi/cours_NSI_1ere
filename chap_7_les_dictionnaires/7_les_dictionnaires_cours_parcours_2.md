# <center>Les dictionnaires </center>
---


## 2 Dictionnaires : notions élémentaires

### 2.1 généralités

* Les dictionnaires sont des ensembles de paires clé-valeur.

* Les clés permettent d'accéder aux valeurs.

* Les clés doivent être toutes différentes et doivent être récursivement non mutables ;

* Les valeurs peuvent être de tous les types.

* Les éléments (paires) d'un dictionnaire n'ont pas d'ordre.

### 2.2. Création d'un dictionnaire

On peut ajouter des associations *(clé,valeur)*, modifier une *valeur*, supprimer une *clé*, ...   
* __création d'un dictionnaire vide__ : 


```python
mon_dico = {} # initialisation d'un dictionnaire vide
```

* __création d'un dictionnaire en extension__ :  

  entre accolades, les couples clé-valeur sont séparés par des virgules et chaque clé est séparée de sa valeur par un double point.


```python
mon_dico = {"nom": "Durant", "prenom": "Jean"} # initialisation  
```

* __création d'un dictionnaire en compréhension__

C'est également possible


```python
carres = {k : k**2 for k in range(5)}
carres
```




    {0: 0, 1: 1, 2: 4, 3: 9, 4: 16}




```python
carres[4]
```




    16



### 2.3 longueur d'un dictionnaire

Cela correspond au nombre de paies clé-valeur contenues dans le dictionnaire.

> On utilise la fonction `len(dictionnaire)`


```python
mon_dico = {"nom": "Durant", "prenom": "Jean"} # initialisation  
len(mon_dico)
```




    2



### 2.4 lecture d'une valeur à partir d'une clé

> on utilise la syntaxe `dictionnaire[clé]`


```python
mon_dico['nom'] # attention : la clé est la chaîne nom
```




    'Durant'



### 2.5 ajout et suppression d'un élément

* On peut ajouter une paire valeur-clé ou modifier une valeur facilement :


```python
mon_dico["age"] = 23  # créé une clé "age" associée à la valeur 23 - mon_dico doit déjà exister
```


```python
mon_dico["age"] = 18  # on modifie la valeur associée à "age"
```


```python
mon_dico
```




    {'nom': 'Durant', 'prenom': 'Jean', 'age': 18}



* on peut supprimer une clé avec la méthode `del()`


```python
smoothie = {'bananes':2,'oranges':1, 'litchis': 4 } 

```


```python
smoothie
```




    {'bananes': 2, 'oranges': 1, 'litchis': 4}




```python
del(smoothie['litchis'])
```


```python
smoothie
```




    {'bananes': 2, 'oranges': 1}



## 3 Parcourir un dictionnaire


__Un dictionnaire n'est pas une structure ordonnée contrairement aux listes ou aux tuples__ : chaque valeur est associée à une clé, pas à un indice. On ne peut donc pas parcourir un dictionnaire suivant un ordre donné ce qui n'aurait aucun sens.

>**Il existe 3 types de parcours de dictionnaires** :
>
>* par ses clés
>* par ses valeurs
>* par ses couples clé:valeur


C'est particulièrement utile quand on manipule un dictionnaire dont on ne connaît pas à l'avance les clés.

**Des méthodes bien utiles** :

Chacune des méthodes suivantes retourne un itérateur (Objet qui permet de parcourir tous les éléments contenus dans une collection)

>* La méthode `keys()`  retourne un itérateur sur les clés,
>* la fonction `values()` retourne un itérateur sur les valeurs,
>* la fonction `items()` retourne un itérateur sur les couples **(clé, valeur)**.

### 3.1  __Parcourir les clés d'un dictionnaire__

>La méthode `keys()` appliquée à un dictionnaire renvoie un itérateur (Objet qui permet de parcourir tous les éléments contenus dans une collection) sur les clés.
>
> **situation typique :**
>
>```Python
>for cle in dictionnaire.keys():
>    ...
>```

_Exemple_ :

A Toubouctou il y a 2 salons de coiffure, 2 boulangeries, 1 bureau de poste :


```python
toubouctou =  {"salon_coiffure" : 2, "boulangerie" : 2, "poste" : 1}
for cle in toubouctou.keys():
    print(cle)
```

    salon_coiffure
    boulangerie
    poste
    

**Remarque** :

Il est possible de créer :
* une liste des clés avec la fonction `list()`
* un tuple des clés avec la fonction `tuple()`


```python
list(toubouctou.keys())
```




    ['salon_coiffure', 'boulangerie', 'poste']




```python
tuple(toubouctou.keys())
```




    ('salon_coiffure', 'boulangerie', 'poste')



**Remarque** :

>Si l'on parcourt un dictionnaire avec une boucle `for`, on parcourt les clés
>
>`for elt in dictionnaire:` est équivalent à `for elt in dictionnaire.keys():`



```python
toubouctou =  {"salon_coiffure" : 2, "boulangerie" : 2, "poste" : 1}
for elt in toubouctou:
    print(elt)
```

    salon_coiffure
    boulangerie
    poste
    

### 3.2 Parcourir les valeurs d'un dictionnaire

>La méthode `values()` appliquée à un dictionnaire renvoie un itérateur sur les clés.
>
> **situation typique :**
>```Python
>for valeur in dictionnaire.values():
>    ...
>```


```python
for val in toubouctou.values():
    print(val)
```

    2
    2
    1
    

Il est également possible de créer une liste ou un tuple à partir de cet itérateur.


```python
list(toubouctou.values())
```




    [2, 2, 1]




```python
tuple(toubouctou.values())
```




    (2, 2, 1)



### 3.3 Parcourir les couples (clé : valeur)

>La méthode `items()` appliquée à un dictionnaire renvoie un itérateur sur les couples (clé : valeur) donnés sous forme de tuples `(cle, valeur)`.
>

_Exemple_ :


```python
for couple in toubouctou.items():
    print(couple)
```

    ('salon_coiffure', 2)
    ('boulangerie', 2)
    ('poste', 1)
    

Il est souvent intéressant d'utiliser l'unpacking afin d'avoir deux variables : l'une contenant la clé et l'autre la valeur associée.

_Exemple_ :


```python
for (cle, valeur) in toubouctou.items():
    print("clé :", cle, "et valeur :", valeur)
```

    clé : salon_coiffure et valeur : 2
    clé : boulangerie et valeur : 2
    clé : poste et valeur : 1
    


```python
dico1 = {'bleu': 'blue', 'rouge': 'red', 'vert': 'green'}
for c, v in dico1.items():
    print(c + ' se traduit, en anglais, par ' + v)
```

    bleu se traduit, en anglais, par blue
    rouge se traduit, en anglais, par red
    vert se traduit, en anglais, par green
    

### 2.4.  Tester si une clé ou une valeur existe déjà

#### Pour tester si une clé existe déjà

> On peut utiliser la fonction `in` (ou `not in`):
> * soit en utilisant `cle in dico` ;
> * soit en utilisant `cle in dico.keys()`

#### Pour tester si une valeur existe déjà

> On peut utiliser la fonction `in` (ou `not in`) : `valeur in dico.values()`

_Exemples_ :


```python
dico = {'bleu': 'blue', 'rouge': 'red', 'vert': 'green'}
print('bleu' in dico)   # affiche True
print('bleu' in dico.keys()) # affiche True
print('violet' in dico.keys()) # affiche False

print('blue' in dico.values())   # affiche True
print('yellow' in dico.values()) # affiche False
```

    True
    True
    False
    True
    False
    

## 3. Pour aller plus loin

* [Quelques éléments sur la manière dont sont implémentés les dictionnaires](https://qkzk.xyz/docs/nsi/cours_terminale/structures_donnees/dictionnaire/) : site d'un collègue du Lycée des Flandres.


Sources : 
    * cours Anne Parrain faculté de Lens
    * NSI ellipses ISBN 9782340-031722
    
