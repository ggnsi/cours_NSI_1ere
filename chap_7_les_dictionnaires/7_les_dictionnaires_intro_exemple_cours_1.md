# <center>Les dictionnaires </center>

<img src="./img/bo.png" width=50%/>

Une [vidéo sur Lumni](https://www.lumni.fr/video/les-dictionnaires) reprend la notion de dictionnaire


## 1.  La structure de données dictionnaires ?
---

### 1.1 tuples et listes parfois inadaptés

Les listes sont une structure de données modifiables qui permet de représenter une suite ordonnée d'éléments. 
Mais pour un certain nombre de situations, la notion d'indice n'est pas pertinente.

Par exemple, pour représenter un élève de 1ère, on pourrait avoir envie de conserver : 

* son nom,
* son prénom,
* sa classe,
* ses trois spécialités
* sa moyenne générale à chaque trimestre

Si on utilise une liste pour représenter un élève, on aurait :


```python
anissa = ["Kaczmarek", "Anissa", "1F", ["nsi","mathématiques","SVT"],[15, 16.8, 15.3]]
```


```python
kevin = ["Dupont", "Kévin", "1G", ["SVT", "mathématiques", "biologie-écologie"],[8.9, 10.3, 9.8]]
```


```python
les_eleves = [anissa, kevin]  ## et sûrement beaucoup d'autres 
```

Supposons qu'on veuille écrire maintenant une fonction qui retourne la listes des élèves (nom et prénom sous forme d'une chaîne de caractères) qui ont choisi une certaine spécialité :

* on doit parcourir la liste `les_elvs` ;
* Pour chaque élève tester si la spécialité `spec` est dans `eleve[3]` (donc savoir que la liste des spécialités est située à l'indice 3) ;
* et si oui, concaténer `eleve[0]` et `eleve[1]` (donc savoir que le nom est à l'indice 0 et le prénom à l'indice 1) et l'ajouter à la liste de sortie.


```python
def eleves_specialite(spec, les_elvs) :
    """ retourne la liste des noms et prénoms des élèves qui ont choisi la spécialité spec.
    param spec : str - la spécialité
    param les_elvs : list(eleve)
    avec eleve de type liste : str, str, str, list(str), list(nombre)
    return : list(str)
    """
    res = []
    for eleve in les_elvs : # on parcourt la liste des élèves
        if spec in eleve[3] : # eleve[3] est la liste des spécialités
            res.append(eleve[0] + " " + eleve[1]) # on ajoute la chaîne 'nom prénom' à la liste des résultats
    return res
```


```python
eleves_specialite("nsi",les_eleves)
```




    ['Kaczmarek Anissa']



Ici, le fait de numéroter les différentes caractéristiques d'un élève

* conduit à un code qui n'est pas clair (quel peut être le sens de `eleve[3]` ?) 
* __n'est pas facilement maintenable__ :  
Par exemple, si on décide d'ajouter l'âge de l'élève après son nom et son prénom, il faut réécrire la fonction car la liste des spécialités est maintenant en indice `4` !

> On a donc inventé une autre structure de données permettant de pallier ce problème : les dictionnaires.  
> Ils apportent une généralisation de la notion d'indice

### 1.2 Utilisation d'un dictionnaire


>Un dictionnaire est un ensemble de couple clé - valeur.
> 
>* Chaque *clé* peut être de n'importe quel type mais non mutable récursivement (entier, flottant, chaîne de >caractères, tuple qui ne contient lui même pas d'élément mutable comme les listes) ;
>* Chaque valeur peut être de n'importe quel type ;
>* les clés doivent être uniques (impossible de définir deux clés de même noms.

Si on reprend l'exemple de l'élève, on pourrait utiliser comme clés le nom des différentes caractéristiques qui nous intéressent :

* "nom" : la chaîne de caractères représentant le nom
* "prenom" : la chaîne de caractères représentant le prénom
* "classe" : la chaîne de caractères représentant la classe
* "specs" : la liste des spécialités choisies
* "moyennes" : la liste des moyennes de l'élève

On reprend notre exemple :

> __Syntaxe__ : 
>
>Un dictionnaire peut se définir entre accolades, les couples clé-valeur sont séparés par des virgules et chaque clé est séparée de sa valeur par un double point.


```python
anissa = {"nom" : "Kazcmarek", "prenom" : "Anissa", "classe" : "1F",
          "specs" : ["nsi","mathématiques","SVT"], "moyennes" : [15, 16.8, 15.3]}
```


```python
anissa
```




    {'nom': 'Kazcmarek',
     'prenom': 'Anissa',
     'classe': '1F',
     'specs': ['nsi', 'mathématiques', 'SVT'],
     'moyennes': [15, 16.8, 15.3]}




```python
type(anissa)
```




    dict



On peut facilement accéder une valeur en utilisant sa clé :


```python
anissa["classe"]
```




    '1F'



Ci dessous, `anissa["specs"]` retourne la valeur associé à la clé `specs`, soit la liste des spécialités.


```python
anissa["specs"]
```




    ['nsi', 'mathématiques', 'SVT']



Donc `anissa["specs"][0]` retourne la première spécialité située dans la liste.


```python
anissa["specs"][0]
```




    'nsi'



__Les valeurs contenues dans un dictionnaire sont mutables__ : on peut les modifier

Par exemple : si `anissa` change de classe :


```python
anissa["classe"] = "1A"
```


```python
anissa
```




    {'nom': 'Kazcmarek',
     'prenom': 'Anissa',
     'classe': '1A',
     'specs': ['nsi', 'mathématiques', 'SVT'],
     'moyennes': [15, 16.8, 15.3]}



__Il est possible d'ajouter (et même de supprimer) de nouvelles paires clé - valeur__

Ci-dessous on ajoute une nouvelle clé `"spe_abandonnee"` et on lui associe la valeur `'SVT'`.  
Pour cela il suffit de faire une affectation.  
Comme la clé `"spe_abandonnee"` n'existe pas, elle est ajoutée.


```python
anissa["spe_abandonnee"] = 'SVT'
```


```python
anissa
```




    {'nom': 'Kazcmarek',
     'prenom': 'Anissa',
     'classe': '1A',
     'specs': ['nsi', 'mathématiques', 'SVT'],
     'moyennes': [15, 16.8, 15.3],
     'spe_abandonnee': 'SVT'}




```python
del(anissa["spe_abandonnee"])
```


```python
anissa
```




    {'nom': 'Kazcmarek',
     'prenom': 'Anissa',
     'classe': '1A',
     'specs': ['nsi', 'mathématiques', 'SVT'],
     'moyennes': [15, 16.8, 15.3]}



__Quelques exemples supplémentaires de manipulations__ :


```python
kevin = {} # création d'un dictionnaire vide
```


```python
kevin["nom"] = "Dupont"   # on créé directement une entrée pour la clé "nom" à 
#laquelle on associe "Dupont"
```


```python
kevin["prenom"] = "Jason"
```


```python
kevin
```




    {'nom': 'Dupont', 'prenom': 'Jason'}




```python
kevin["prenom"] = "Kévin"  # on écrase la valeur associée à "prenom"
```


```python
kevin
```




    {'nom': 'Dupont', 'prenom': 'Kévin'}




```python
kevin["classe"] = "1G"
```


```python
kevin["specs"] = ["SVT", "mathématiques", "biologie-écologie"]
```


```python
kevin["moyennes"] = [8.9, 10.3, 9.8]
```


```python
kevin
```




    {'nom': 'Dupont',
     'prenom': 'Kévin',
     'classe': '1G',
     'specs': ['SVT', 'mathématiques', 'biologie-écologie'],
     'moyennes': [8.9, 10.3, 9.8]}




```python
les_eleves = [anissa, kevin]  ## et sûrement beaucoup d'autres 
```

On peut maintenant modifier la fonction `eleves_specialite()` qui devient beaucoup plus lisible.


```python
def eleves_specialite(spec, les_elvs) :
    """ docstring
    """
    res = []
    for eleve in les_elvs : 
        if spec in eleve["specs"] :
            res.append(eleve["nom"] + " " + eleve["prenom"])
    return res
```


```python
eleves_specialite("nsi",les_eleves)
```




    ['Kazcmarek Anissa']



#### Dans quels cas utilise-t-on un dictionnaire ?
<div style="color:red">

> * Comme dans l'exemple des élèves, lorsqu'on veut manipuler des données qui ont toutes une même structure, avec des caractéristiques de nature différentes. On est capable pour ces données de fixer à l'avance quelles seront les clés;
> * Pour manipuler des données qui associent à un élément d'un certain type A une valeur d'un certain type B. Par exemple, on veut conserver les notes d'un contrôle : on associe une note à un élève ;
    </div>


```python
interro = {"Kévin": 12, "Anissa": 15, "Mohamed": 12, "Sophie": 8}
```

* Pour agréger dans une seule variable de nombreux paramètres qu'on a souvent besoin de transmettre ensemble à des fonctions.
