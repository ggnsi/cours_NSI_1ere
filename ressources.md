# Quelques ressources

* __[Basthon](https://basthon.fr)__ (Bac à sable pour Python) qui permet :
  * d'utiliser Python en ligne [avec une console et une zone de script](https://console.basthon.fr/) ;
  * [d'afficher des notebooks](https://notebook.basthon.fr/) : il suffit d'ouvrir le notebook d'abord sauvegardé sur votre disque 

* 