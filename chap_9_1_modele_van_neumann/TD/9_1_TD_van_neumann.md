# TD - Modèle de Van Neumann

## Exercice 1

Déterminer la ou les réponses correctes.

### question 1

Quel est le rôle de l'Unité Arithmétique et Logique dans un processeur?

Réponses :

1. La gestion interne du processeur
2. Faire les calculs
3. Interfacer les différents périphériques
4. Définir la base arithmétique

### question 2

Parmi les affirmations suivantes, laquelle est vraie ?

Réponses :

1. La mémoire RAM est une mémoire accessible en lecture seulement
2. La mémoire RAM est une mémoire accessible en écriture seulement
3. La mémoire RAM est une mémoire accessible en lecture et en écriture
4. La mémoire RAM permet de stocker des données après extinction de la machine

### question 3

Dans l'architecture de Von Neumann, le processeur contient : 

Réponses :

1. L'unité de commande et l'unité arithmétique et logique
2. L'unité de commande et la RAM
3. Seulement l'unité arithmétique et logique.
4. Seulement l'unité de commande

### question 4

Que dit la loi de MOORE ?

Réponses :

1. Que le nombre de transistors sur une puce va doubler tous les deux ans.
2. Que la vitesse des horloges va doubler tous les deux ans.
3. Que la puissance des ordinateurs va doubler tous les deux ans.
4. Que la taille des ordinateurs va être divisée par deux tous les deux ans.

### question 5

A quoi servent les mémoires cache ?

Réponses :

1. Elles contrôlent les droits d'écritures sur le disque dur.
2. Elles servent à accélerer les traitements.
3. Elles masquent les données avec des ET logiques.
4. Elles permettent de cacher les données inutiles.

### question 6

Parmi les 4 composants informatiques ci-dessous, lequel est le plus rapide ?

Réponses :

1. RAM
2. disque dur
3. mémoire cache
4. registre

### question 7

Le processeur est composé ...

Réponses :

1. de l'unité de contrôle et de la mémoire principale
2. d'une mémoire principale et de l'unité arithmétique et logique
3. de l'unité de contrôle et de l'unité arithmétique et logique
4. d'un accumulateur et d'entrées/sorties

### question 8

L’unité qui permet de séquencer, décoder, traduire chaque instruction et générer les signaux d’activation nécessaires pour l'ALU et d’autres unités s'appelle ...

Réponses :

1. l'unité arithmétique
2. le CPU
3. l'unité logique
4. l'unité de contrôle

### question 9

Physiquement, les registres sont :

Réponses :

1. Des composants sur la carte mère de l'ordinateur
2. A l'intérieur du processeur
3. Une partie séparée de la RAM
4. De la mémoire cache

### question 10

Laquelle des mémoires suivantes est non volatile, c'est à dire ne s'efface pas quand on éteint l'ordinateur ?

Réponses :

1. RAM
2. cache
3. ROM
4. Toutes les réponses sont vraies

### question 11

« BUS » est le nom que l'on donne en informatique :

Réponses :

1. Au Board Unit System
2. Aux registres du processeur
3. Aux fils qui permettent de transmettre les données d'un composant à un autre
4. A l'horloge de la carte mère

### question 12

Dans quel langage les instructions sont formées de suites de 0 et de 1 ?

Réponses :

1. Le langage assembleur
2. Le langage machine
3. Un langage de haut niveau
4. Un langage orienté objets

### question 13

Quelle affirmation est exacte:

Réponses :

1. La mémoire RAM ne fonctionne qu’en mode lecture
2. La mémoire RAM permet de stocker des données et des programmes
3. Une mémoire morte ne peut plus être utilisée
4. La mémoire RAM permet de stocker définitivement des données

### question 14

Quel est le composant de base des microprocesseurs intégrés ?

Réponses :

1. La résistance
2. La diode
3. Le thyristor
4. Le transistor


### question 15

Les informations entre le processeur et la RAM (mémoire vive) circulent ?

Réponses :

1. Du processeur vers la mémoire RAM uniquement.
2. De la mémoire RAM vers le processeur uniquement.
3. Aucune information n'est échangée entre ces deux composants.
4. De la mémoire RAM vers le processeur et du processeur vers la RAM.


### question 16

Le BIOS appelé aussi mémoire morte :

Réponses :

1. est une partie du processeur.
2. contient les informations nécessaires pour démarrer l'ordinateur.
3. est effacé lorsque l'ordinateur est éteint.
4. est une mémoire accessible en écriture uniquement;


### question 17

Soit une machine basée sur une architecture de Von Neumann, disposant d'un Accumulateur et du jeu d'instructions suivant :

Dans le tableau ci-dessous, xx représente une adresse mémoire et (xx) son contenu. 

| Mnémonique | Action correspondante|
|:--:|:--:|
|LDA xx  |LoaD (xx) in Accumulator  |
|STA xx  |STore Accumulator value at address xx  |
|ADD xx  |ADD (xx) to accumulator value  |
|SUB xx  |SUBstract (xx) from accumulator value  |
|BRZ xx  |BRanch to the instruction at address xx if Accumulator value equal Zero  |
|BRP xx  |BRanch to the instruction at address xx if Accumulator value is Positive (or zero)  |
|BRA xx  |BRanch Always to the instruction at address xx   |
|END 	 |Stop program  |

Que contient l'accumulateur une fois le programme ci-dessous exécuté ?

01 LDA 09  
02 SUB 08  
03 BRP 06  
04 LDA 08  
05 BRA 07  
06 LDA 09  
07 HLT  
08 25  
09 41  

Réponses :

1. 08
2. 09
3. 41
4. 25





