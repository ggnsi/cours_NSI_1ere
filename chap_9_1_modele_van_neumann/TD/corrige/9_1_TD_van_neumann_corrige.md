# TD - Modèle de Van Neumann

## Exercice 1

Déterminer la ou les réponses correctes.

### question 1

Quel est le rôle de l'Unité Arithmétique et Logique dans un processeur?

Réponses :

2. Faire les calculs

### question 2

Parmi les affirmations suivantes, laquelle est vraie ?

Réponses :

3. La mémoire RAM est une mémoire accessible en lecture et en écriture

### question 3

Dans l'architecture de Von Neumann, le processeur contient : 

Réponses :

1. L'unité de commande et l'unité arithmétique et logique

### question 4

Que dit la loi de MOORE ?

Réponses :

1. Que le nombre de transistors sur une puce va doubler tous les deux ans.

### question 5

A quoi servent les mémoires cache ?

Réponses :

2. Elles servent à accélerer les traitements.


### question 6

Parmi les 4 composants informatiques ci-dessous, lequel est le plus rapide ?

Réponses :

4. registre

### question 7

Le processeur est composé :

Réponses :

3. de l'unité de contrôle et de l'unité arithmétique et logique

### question 8

L’unité qui permet de séquencer, décoder, traduire chaque instruction et générer les signaux d’activation nécessaires pour l'ALU et d’autres unités s'appelle ...

Réponses :

4. l'unité de contrôle

### question 9

Physiquement, les registres sont :

Réponses :

2. A l'intérieur du processeur

### question 10

Laquelle des mémoires suivantes est non volatile, c'est à dire ne s'efface pas quand on éteint l'ordinateur ?

Réponses :

3. ROM

### question 11

« BUS » est le nom que l'on donne en informatique :

Réponses :

3. Aux fils qui permettent de transmettre les données d'un composant à un autre


### question 12

Dans quel langage les instructions sont formées de suites de 0 et de 1 ?

Réponses :

2. Le langage machine

### question 13

Quelle affirmation est exacte:

Réponses :

2. La mémoire RAM permet de stocker des données et des programmes


### question 14

Quel est le composant de base des microprocesseurs intégrés ?

Réponses :

4. Le transistor


### question 15

Les informations entre le processeur et la RAM (mémoire vive) circulent ?

Réponses :

4. De la mémoire RAM vers le processeur et du processeur vers la RAM.

### question 16

Le BIOS appelé aussi mémoire morte :

Réponses :

2. contient les informations nécessaires pour démarrer l'ordinateur.


### question 17

Soit une machine basée sur une architecture de Von Neumann, disposant d'un Accumulateur et du jeu d'instructions suivant :

LDA xx  //LoaD (xx) in Accumulator
STA xx  //STore Accumulator value at address xx
ADD xx  //ADD (xx) to accumulator value
SUB xx  //SUBstract (xx) from accumulator value
BRZ xx  //BRanch to the instruction at address xx if Accumulator value equal Zero
BRP xx  //BRanch to the instruction at address xx if Accumulator value is Positive (or zero)
BRA xx  //BRanch Always to the instruction at address xx 
END 	//Stop program
où xx représente une adresse mémoire et (xx) son contenu.

Que contient l'accumulateur une fois le programme ci-dessous exécuté ?

01 LDA 09
02 SUB 08
03 BRP 06
04 LDA 08
05 BRA 07
06 LDA 09
07 HLT
08 25
09 41

Réponses :

3. 41






