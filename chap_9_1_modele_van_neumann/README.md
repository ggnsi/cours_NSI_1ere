## Modèle de Van Neumann - Langage machine
---

* [Bref historique de l'histoire de l'informatique](./9_1_historique_rapide.md)
* [Partie cours sur le modèle de Van Neumann](./9_1_modele_van_neumann.pdf)
* [TD langage machine](./9_1_TD_machine_10_eleve.pdf) et un corrigé
* [QCM](./TD/9_1_TD_van_neumann.md) et [son corrigé](./TD/corrige/9_1_TD_van_neumann_corrige.md)

