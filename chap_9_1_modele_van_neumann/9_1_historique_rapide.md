# bref historique 
---
_source_ : [site qkzk.xyz](https://qkzk.xyz/docs/nsi/cours_premiere/architecture/1_intro/)


## Historique de la machine 

### Préhistoire informatique 

* Compter : les doigts, les orteils et des outils.
* Concevoir des machines pour réaliser des calculs (_calculi_ mot latin qui signifie “cailloux”). 

**Exemple** : Le boulier, découvert indépendamment par de nombreuses civilisations

![boublier](./img2/boulier3.jpg)

### Antiquité : naviguer

![machine](./img2/boulier3.jpg)

![anticythere](./img2/anticythere_4.jpg)

![anticythere](./img2/anticythere_5.png)

La machine d’Anticythère, découverte en 1900 et datant de -87 avant J.-C. servait à calculer les positions astronomiques et donc à naviguer

Elle démontre que :

* Calculer a toujours été une entreprise importante
* L’homme s’est toujours montré d’une très grande ingéniosité pour y parvenir

### Renaissance : Blaise Pascal 

**Blaise Pascal** : Scientifique et penseur français du XVIIe siècle

* En physique : le théorème de Pascal qui exprime les variations de pression dans un fluide
* En mathématiques : calcul infinitésimal, raisonnement par récurrence etc.
* En philosophie et littérature : les Pensées (1669)
* En ingénierie : la Pascaline. Première machine à calculer. Inventée à 19 ans pour aider son père devant remettre en ordre les recettes fiscales d’une province.

![La pascaline](./img2/pascaline_2.jpg)


### Gottfried Wilhelm Leibniz 

**Leibniz (1646 - 1716)** est un penseur allemand fait progresser la philosophie, les mathématiques, la physique et l’ingénierie autant que Pascal.

Il améliore la Pascaline et redécouvre le système binaire.

Néanmoins on n’utilisera réellement le binaire qu’après 1945.

### Premières machines programmables : métiers à tisser 

**Joseph Marie Jacquard (1752-1834)** améliore des principes déjà existants pour concevoir une machine à tisser utilisant les cartes perforées de Jean-Baptiste Falcon.

ON S’EN FOUT C’EST VIEUX ! 

Les métiers Jacquard sont encore utilisés dans le médical pour réaliser des coudières, genouillères et prothèses d’artères. Et c’est produit en France.

![Cartes perforées](./img2/cartes_perforees_2.jpg)

## Machine à calculer programmable #

**Charles Babbage** constatant que les erreurs de calculs dans les tables conduisent à de nombreuses catastrophes invente en 1833 le concept de machine (Difference Engine 1) permettant d’automatiser le calcul.

Il correspond ensuite avec **Ada Lovelace** (comtesse et fille du poète Lord Byron). Elle conçoit les premiers programmes pour cette machine. Elle est vue comme la première programmeuse du monde.

Trop complexe, trop coûteuse la machine de Babbage ne verra jamais le jour.

![Nombres de Bernoulli](./img2/ada_lovelace_2.jpg)

![Ada Lovelace](./img2/lovelace_bernoulli_2.jpg)


### La révolution électricité 

La révolution industrielle de la fin du XIXe siècle conduit à l’apparition de l’électricité et des moteurs qui améliorent les machines à calculer.

Par exemple, aux USA, **Herman Hollerith**, conçoit une machine qui divise par deux le temps nécessaire au recensement de la population. Sa société fusionnera pour devenir IBM.

### XXe siècle : un essor fulgurant 

* Avant 1936 : première machine capable de parallélisme
* 1936 : Alonzo Church et Kurt Gödel fournissent un cadre théorique et Alan Turing propose un concept théorique de machine qui est encore utilisé.
* Seconde guerre mondiale. L’information est chiffrée et circule alors massivement via les ondes radio et le télégraphe. Le déchiffrement devient un enjeu mondial. Citons Enigma, utilisée par les allemands, déchiffrée par Turing grâce à la bombe.

### Enigma 

![Enigma](./img2/enigma_2.jpg)

### La bombe

### ENIAC 

Les machines sont encore colossales !

* ENIAC : 30 tonnes, 167 m2, 160 kW pour 100 kHz et 100 000 additions par secondes.
* Programme sur cartes perforées, entièrement électronique. Servant au calcul balistique. Programmé par six femmes.
* Calcul d’une trajectoire d’une table de tir. Comparatif :
    * Moyen                         Temps
    * Homme à la main               2,6 j
    * Avec une machine à calculer    12 h
    * Model 5 (concurrent ENIAC)     40 min
    * ENIAC                           3 s
    * PC ~2000                       30 µs

### l’ENIAC : un monstre 

![eniac_2](./img2/eniac_2.jpg)
